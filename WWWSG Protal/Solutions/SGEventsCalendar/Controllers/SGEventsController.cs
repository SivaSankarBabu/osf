﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGEventsCalendar.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Search;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;

namespace SGEventsCalendar.Controllers
{
    public class SGEventsController : Controller
    {
        SGEventsDAL objDal = new SGEventsDAL();

        Database _web = Database.GetDatabase("web");

        [HttpGet]
        public ActionResult Header()
        {


            Header head = new Header();
            try
            {
                head = objDal.Get_Header_Content(Constants.rootItemId);
                return View(head);
            }
            catch (Exception ex)
            {
                SGEventslogger.WriteException(ex, "Exception Occured" + ex.InnerException);
                return View(head);
            }
        }

        [HttpPost]
        public ActionResult Header(Header head)
        {
            try
            {
                head = objDal.Get_Header_Content(Constants.rootItemId);
                return View(head);
            }
            catch (Exception ex)
            {
                SGEventslogger.WriteException(ex, "Exception Occured" + ex.InnerException);
                return View(head);
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            Home obj = new Home();
            try
            {
                Item item = _web.GetItem(Constants.rootItemId);

                if (item != null)
                {

                    string[] pageImgAlt = CommonFun.GetImage(item, "Background Image");
                    obj.PageTitle = Convert.ToString(item.Fields["Title"]);
                    obj.PageBackImage = pageImgAlt[0];
                    obj.PageBackImageAlt = pageImgAlt[1];

                    obj.PageShortDesc = Convert.ToString(item.Fields["Description"]);

                    Item _eventItem = _web.GetItem(Constants.eventsItemId);

                    if (_eventItem != null)
                    {
                        obj.EventsTitle = Convert.ToString(_eventItem.Fields["Introduction Title"]);
                        obj.EventsDesc = Convert.ToString(_eventItem.Fields["Introduction Content"]);
                    }

                    obj.RevisitTitle = Convert.ToString(item.Fields["SG50 Title"]);
                    obj.RevisitDesc = Convert.ToString(item.Fields["SG50 Description"]);

                    string[] deskImg = CommonFun.GetImage(item, "SG50 Desktop Background"); string[] mobImg = CommonFun.GetImage(item, "SG50 Mobile BgImage");
                    obj.RevisitDeskImg = deskImg[0];
                    obj.RevisitDeskImgAlt = deskImg[1];
                    obj.RevisitMobImg = mobImg[0];
                    obj.RevisitMobImgAlt = mobImg[1];

                    obj.RevisitUrl = Convert.ToString(CommonFun.LinkUrl(item, "Navigate Url"));
                    obj.IsVisibleSG50 = ((CheckboxField)item.Fields["IsVisible"]).Checked;

                    /* Calling functions */
                    obj.Banners = objDal.GetBanners(Constants.bannersItemId, 4);
                    obj.Events = objDal.GetEvents(Constants.eventsItemId, Constants.EventsTiles, "L");
                    obj.Stories = objDal.GetStories(Constants.storiesItemId, Constants.StoriesTiles, null);

                    return View(obj);
                }
                else return View(obj);
            }
            catch (Exception msg)
            {
                SGEventslogger.WriteException(msg, "Exception Occured" + msg.InnerException);
                throw msg;
            }
        }

        [HttpGet]
        public ActionResult Footer()
        {
            Footer _footer = new Footer();
            try
            {
                _footer = objDal.Get_Footer_Content(Constants.rootItemId);

                Item itemConfig = _web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");
                if (itemConfig != null)
                {
                    _footer.HostName = Convert.ToString(itemConfig.Fields["Host Name"]);
                    _footer.FBAppId = Convert.ToString(itemConfig.Fields["Facebook AppID"]);
                }
                return View(_footer);
            }
            catch (Exception msg)
            {
                SGEventslogger.WriteException(msg, "Exception Occured" + msg.InnerException);
                return View(_footer);
            }
        }

        [HttpPost]
        public ActionResult Footer(Footer _footer)
        {
            try
            {
                _footer = objDal.Get_Footer_Content(Constants.rootItemId);

                Item itemConfig = _web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");
                if (itemConfig != null)
                {
                    _footer.HostName = Convert.ToString(itemConfig.Fields["Host Name"]);
                    _footer.FBAppId = Convert.ToString(itemConfig.Fields["Facebook AppID"]);
                }

                return View(_footer);
            }
            catch (Exception msg)
            {
                SGEventslogger.WriteException(msg, "Exception Occured" + msg.InnerException);
                return View(_footer);
            }
        }

        [HttpGet]
        public ActionResult EventsListing()
        {
            SGEvent _objEvents = new SGEvent();
            try
            {
                Item _conItem = Sitecore.Context.Item;

                Item eventsItem = _web.GetItem(Constants.eventsItemId);

                if (eventsItem != null)
                {
                    _objEvents.IntroTitle = Convert.ToString(eventsItem.Fields["Event Introduction Title"]);
                    _objEvents.IntroContent = Convert.ToString(eventsItem.Fields["Event Introduction Content"]);
                    _objEvents.TandCContent = Convert.ToString(eventsItem.Fields["Content"]);
                    _objEvents.TandCLink = Convert.ToString(CommonFun.LinkUrl(eventsItem, "link"));

                    _objEvents.OveryLayContent = objDal.GetOverLayContent(eventsItem);
                }

                if (_conItem != null)
                {
                    _objEvents.ConItemTempId = _conItem.TemplateID.ToString();
                    _objEvents.ConItemId = _conItem.ID.ToString();
                }

                _objEvents.GetPost = "Get";
                _objEvents.Categories = objDal.GetCategories(Constants.categoryTypes);
                _objEvents.EventTypes = objDal.GetCategories(Constants.eventTypes);
                _objEvents.AudienceTypes = objDal.GetCategories(Constants.audiTypes);

                _objEvents.NewEvent = objDal.GetNewEventData(Constants.eventsItemId);

                if (GetSelectedItemsCount(Constants.eventsItemId, "List Page Events") > 0)
                {
                    _objEvents.ListItems = objDal.GetEvents(Constants.eventsItemId, new string[1] { "List Page Events" }, "m");
                }
                _objEvents.ListEvents = objDal.GetEvents(Constants.eventsItemId, null, "L");


                return View(_objEvents);
            }
            catch (Exception msg)
            {
                SGEventslogger.WriteException(msg, "Exception Occured" + msg.InnerException);
                return View(_objEvents); throw msg;
            }
        }

        [HttpPost]
        public ActionResult EventsListing(string categoryName, string type, string period, string audiType, SGEvent eventObj)
        {
            List<Item> _items = new List<Item>();
            categoryName = categoryName.Split(':')[0].Trim();
            type = type.Split(':')[0].Trim();
            audiType = audiType.Split(':')[0].Trim();

            if (!string.IsNullOrEmpty(period))
            {
                period = period.Split(':')[0].Trim();
            }

            Item _conItem = Sitecore.Context.Item;

            Item eventsItem = _web.GetItem(Constants.eventsItemId);

            if (eventsItem != null)
            {
                eventObj.IntroTitle = Convert.ToString(eventsItem.Fields["Event Introduction Title"]);
                eventObj.IntroContent = Convert.ToString(eventsItem.Fields["Event Introduction Content"]);
                eventObj.TandCContent = Convert.ToString(eventsItem.Fields["Content"]);
                eventObj.TandCLink = Convert.ToString(CommonFun.LinkUrl(eventsItem, "link"));
                eventObj.OveryLayContent = objDal.GetOverLayContent(eventsItem);
            }

            if (_conItem != null)
            {
                eventObj.ConItemTempId = _conItem.TemplateID.ToString();
                eventObj.ConItemId = _conItem.ID.ToString();
            }

            Item eveRootItem = _web.GetItem(Constants.eventsItemId);

            try
            {
                if (eveRootItem != null)
                {
                    _items = eveRootItem.GetChildren().OrderBy(x => x.Fields["Event Start Date"].Value).ToList();
                    eventObj.GetPost = "Post";
                    eventObj.PostStartMonth = 0;
                }

                if (categoryName != "0" && _items.Count > 0)
                {
                    eventObj.GetPost = "Post";
                    _items = _items.Where(x => ((LookupField)x.Fields["Event Category"]).TargetItem != null && Convert.ToString(((LookupField)x.Fields["Event Category"]).TargetItem.Fields["Value"]) == categoryName.Trim()).ToList();
                }
                if (type != "0" && _items.Count > 0)
                {
                    eventObj.GetPost = "Post";
                    _items = _items.Where(x => ((LookupField)x.Fields["Event Type"]).TargetItem != null && Convert.ToString(((LookupField)x.Fields["Event Type"]).TargetItem.Fields["Value"]) == type.Trim()).ToList();
                }
                if (audiType != "0" && _items.Count > 0)
                {
                    eventObj.GetPost = "Post";

                    //_items = _items.Where(x => x.Fields["Audience Type"] != null && (LookupField)x.Fields["Audience Type"] != null && ((LookupField)x.Fields["Audience Type"]).TargetItem != null && ((LookupField)x.Fields["Audience Type"]).TargetItem.Fields["Value"] != null && !string.IsNullOrEmpty(Convert.ToString(((LookupField)x.Fields["Audience Type"]).TargetItem.Fields["Value"])) && Convert.ToString(((LookupField)x.Fields["Audience Type"]).TargetItem.Fields["Value"]) == audiType.Trim()).ToList();
                    _items = _items.Where(x => ((LookupField)x.Fields["Audience Type"]).TargetItem != null && Convert.ToString(((LookupField)x.Fields["Audience Type"]).TargetItem.Fields["Value"]) == audiType.Trim()).ToList();
                }
                if (!string.IsNullOrEmpty(period) && period != "0")
                {
                    eventObj.GetPost = "Post with filter";
                    int[] dates = period.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                    eventObj.PeriodStartMonth = dates[0];
                    eventObj.PeriodStartYear = dates[1];
                }


                eventObj.ListEvents = _items.Select(x => new SGEvent()
                {
                    ThumbImg = Convert.ToString(CommonFun.GetImage(x, "Event Image")[0]),
                    ThumbImgAlt = Convert.ToString(CommonFun.GetImage(x, "Event Image")[1]),

                    DetailLargeImg = Convert.ToString(CommonFun.GetImage(x, "Event Overlay Image")[0]),
                    DetailLargeImgAlt = Convert.ToString(CommonFun.GetImage(x, "Event Overlay Image")[1]),

                    FromDate = ((DateField)x.Fields["Event Start Date"]).DateTime,
                    ToDate = ((DateField)x.Fields["Event End Date"]).DateTime,
                    Name = Convert.ToString(x.Fields["Event Name"]),
                    Description = Convert.ToString(x.Fields["Event Description"]),
                    AllDay = ((CheckboxField)x.Fields["All Day"]).Checked,

                    Category = ((LookupField)x.Fields["Event Category"]).TargetItem != null ? Convert.ToString(((LookupField)x.Fields["Event Category"]).TargetItem.Name) : string.Empty,
                    Type = ((LookupField)x.Fields["Event Type"]).TargetItem != null ? Convert.ToString(((LookupField)x.Fields["Event Type"]).TargetItem.Name) : string.Empty,
                    AudienceType = ((LookupField)x.Fields["Audience Type"]).TargetItem != null ? Convert.ToString(((LookupField)x.Fields["Audience Type"]).TargetItem.Name) : string.Empty,
                    EntryType = Convert.ToString(x.Fields["Event Entry Type"]),

                    Location = Convert.ToString(x.Fields["Event Location"]),
                    Url = Convert.ToString(CommonFun.LinkUrl(x, "Event URL")),
                    GoogleMapLink = Convert.ToString(CommonFun.LinkUrl(x, "Google Map Link")),
                    PurchaseTicketURL = CommonFun.LinkUrl(x, "Purchase Ticket URL"),

                    SocShareTitle = Convert.ToString(x.Fields["Social Share Title"]),
                    SocShareDesc = Convert.ToString(x.Fields["Social Share Description"]),
                    SocShareImg = Convert.ToString(CommonFun.GetImage(x, "Social Share Image")[0]),

                    /* Details Footer Content */
                    NoteTitle = Convert.ToString(x.Fields["Got a Similar Title"]),
                    NoteContent = Convert.ToString(x.Fields["Got a Similar Content"]),
                    TermsTitle = Convert.ToString(x.Fields["Terms Title"]),
                    EventDetailsMail = Convert.ToString(x.Fields["Email"]),
                    TermsCondExternalLink = Convert.ToString(CommonFun.LinkUrl(x, "Terms Link")),
                    item = x
                }).ToList();

                eventObj.Categories = objDal.GetCategories(Constants.categoryTypes);
                // eventObj.GetPost = "Post";
                eventObj.EventTypes = objDal.GetCategories(Constants.eventTypes);
                eventObj.AudienceTypes = objDal.GetCategories(Constants.audiTypes);
                eventObj.NewEvent = objDal.GetNewEventData(Constants.eventsItemId);
                return View(eventObj);
            }
            catch (Exception msg)
            {
                SGEventslogger.WriteException(msg, "Exception Occured" + msg.InnerException);
                throw msg;
            }
        }

        [HttpGet]
        public ActionResult FeaturedStories()
        {
            IEnumerable<Story> objSg;

            objSg = objDal.GetStories(Constants.storiesItemId, null, null);

            return View(objSg);
        }

        [HttpGet]
        public ActionResult ArticleDetails()
        {
            Item _item = Sitecore.Context.Item;

            Story _obj = new Story();

            if (_item != null)
            {
                _obj = objDal.GetFeaturedStoryDetails(_item);
                ViewBag.LatestNews = objDal.GetStories(Constants.storiesItemId, null, _item);

                Item _config = _web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");
                if (_config != null)
                {
                    _obj.HostName = Convert.ToString(_config.Fields["Host Name"]);
                    _obj.FBAppId = Convert.ToString(_config.Fields["Facebook AppID"]);
                }

                return View(_obj);
            }
            return View(_obj);
        }

        [HttpGet]
        public ActionResult MetaData()
        {
            MetaContent metaCon = new MetaContent();
            try
            {
                Item itemconfiguration = _web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");

                if ("{BAEBC2BC-71C9-4AA9-AA95-C58A8B0DEBF9}" == Convert.ToString(Sitecore.Context.Item.ID.ToString()))
                {
                    metaCon.MetaTitle = "SG";
                    metaCon.MetaDescription = "The Singapore Spirit lives in all of us. It carries us forward and unites us. Discover the ones who are shaping the future and learn how you can make a difference too.";
                    metaCon.MetaKeywords = "SG, Singapore, SGfuture, Singapore Spirit, SG50, SG PULSE, OSF, Our Singapore Fund, Singapore future, Singapore story";
                    metaCon.PageTitle = "SG";

                    metaCon.OGImage = "Image Path";
                    metaCon.FBID = itemconfiguration["Facebook AppID"];

                }
                else if ("{BE043EA9-BB60-49B4-95AF-9E1A3E5CF21A}" == Convert.ToString(Sitecore.Context.Item.ID.ToString()))
                {

                    metaCon.MetaTitle = "SG Pulse";
                    metaCon.MetaDescription = "Be inspired by stories of Singapore and fellow Singaporeans who are shaping the future of Singapore today.";
                    metaCon.MetaKeywords = "Singapore Spirit, SG Pulse, Singapore story, profile, idea, community, place, food, feature, project, event, spirit of partnership, Kampung Spirit, Fighting Spirit, Trailblazing Spirit";
                    metaCon.PageTitle = "SG | Pulse";
                }
                else if ("{5433A3B4-9A3B-4674-87B5-CADC90AADE63}" == Convert.ToString(Sitecore.Context.Item.TemplateID.ToString()))
                {
                    metaCon.MetaTitle = "SG Pulse";
                    metaCon.MetaDescription = "Go behind the scenes to discover the stories and people who are shaping the future of Singapore today.";
                    metaCon.MetaKeywords = "Singapore Spirit, SG Pulse, Singapore story, profile, idea, community, place, food, feature, project, event, spirit of partnership, Kampung Spirit, Fighting Spirit, Trailblazing Spirit";
                    metaCon.PageTitle = "SG | Pulse";
                }
                else if ("{0CB3EA7B-3E71-4264-A974-4301F03ED693}" == Convert.ToString(Sitecore.Context.Item.ID.ToString()))
                {
                    metaCon.PageTitle = "SG | Events Calendar";
                    metaCon.OGImage = "Image Path";
                    metaCon.FBID = itemconfiguration["Facebook AppID"];

                    metaCon.MetaTitle = "What’s On | Local events that foster the Singapore Spirit";
                    metaCon.MetaDescription = "Every event that helps to unite us as Singaporeans finds its way to this calendar. Sign up for an event to help build our Singapore Spirit.";
                    metaCon.MetaKeywords = "Our SG, Our Singapore, What’s on, Featured events, Singapore events, Singapore calendar, Community activities, MCCY, Ministry of Culture, Community and Youth, Singapore Spirit";


                }
                else if ("{10ADBEF4-75F0-4E8E-9187-3DB1F2D1B03A}" == Convert.ToString(Sitecore.Context.Item.TemplateID.ToString()))
                {
                    metaCon.PageTitle = "SG | Events Calendar";
                    metaCon.OGImage = "Image Path";
                    metaCon.FBID = itemconfiguration["Facebook AppID"];

                    metaCon.MetaTitle = "What’s On | Local events that foster the Singapore Spirit";
                    metaCon.MetaDescription = "Every event that helps to unite us as Singaporeans finds its way to this calendar. Sign up for an event to help build our Singapore Spirit.";
                    metaCon.MetaKeywords = "Our SG, Our Singapore, What’s on, Featured events, Singapore events, Singapore calendar, Community activities, MCCY, Ministry of Culture, Community and Youth, Singapore Spirit";


                    bool SocialShareImage = false;
                    if (Sitecore.Context.Item.Fields["Social Share Image"] != null)
                    {
                        Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Social Share Image"];
                        if (img != null)
                        {
                            MediaItem mItem = img.MediaItem;
                            if (mItem != null)
                            {
                                string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                                metaCon.OGImage = itemconfiguration["Host Name"] + imgSrc;
                                SocialShareImage = true;
                            }
                        }

                    }

                    if (SocialShareImage == false)
                    {
                        if (Sitecore.Context.Item.Fields["Event Overlay Image"] != null)
                        {
                            Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Event Overlay Image"];
                            if (img != null)
                            {
                                MediaItem mItem = img.MediaItem;
                                if (mItem != null)
                                {
                                    string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                                    metaCon.OGImage = itemconfiguration["Host Name"] + imgSrc;
                                }
                            }

                        }
                    }
                }

                if ("{5433A3B4-9A3B-4674-87B5-CADC90AADE63}" == Convert.ToString(Sitecore.Context.Item.TemplateID.ToString()))
                {

                    metaCon.FBID = itemconfiguration["Facebook AppID"];

                    if (Sitecore.Context.Item.Fields["Social Share Image"] != null)
                    {
                        Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Social Share Image"];
                        if (img != null)
                        {
                            MediaItem mItem = img.MediaItem;
                            if (mItem != null)
                            {
                                string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                                metaCon.OGImage = itemconfiguration["Host Name"] + imgSrc;
                            }
                        }

                    }
                }
                return View(metaCon);
            }
            catch (Exception msg)
            {
                SGEventslogger.WriteException(msg, "Exception Occured" + msg.InnerException);
                return View(metaCon);
            }
        }

        [HttpPost]
        public ActionResult MetaData(MetaContent metaCon)
        {
            metaCon = new MetaContent();
            Item itemconfiguration = _web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");

            if ("{BAEBC2BC-71C9-4AA9-AA95-C58A8B0DEBF9}" == Convert.ToString(Sitecore.Context.Item.ID.ToString()))
            {
                metaCon.MetaTitle = "SG";
                metaCon.MetaDescription = "The Singapore Spirit lives in all of us. It carries us forward and unites us. Discover the ones who are shaping the future and learn how you can make a difference too.";
                metaCon.MetaKeywords = "SG, Singapore, SGfuture, Singapore Spirit, SG50, SG PULSE, OSF, Our Singapore Fund, Singapore future, Singapore story";
                metaCon.PageTitle = "SG";

                metaCon.OGImage = "Image Path";
                metaCon.FBID = itemconfiguration["Facebook AppID"];
            }
            else if ("{BE043EA9-BB60-49B4-95AF-9E1A3E5CF21A}" == Convert.ToString(Sitecore.Context.Item.ID.ToString()))
            {

                metaCon.MetaTitle = "SG Pulse";
                metaCon.MetaDescription = "Be inspired by stories of Singapore and fellow Singaporeans who are shaping the future of Singapore today.";
                metaCon.MetaKeywords = "Singapore Spirit, SG Pulse, Singapore story, profile, idea, community, place, food, feature, project, event, spirit of partnership, Kampung Spirit, Fighting Spirit, Trailblazing Spirit";
                metaCon.PageTitle = "SG | Pulse";
            }
            else if ("{5433A3B4-9A3B-4674-87B5-CADC90AADE63}" == Convert.ToString(Sitecore.Context.Item.TemplateID.ToString()))
            {
                metaCon.MetaTitle = "SG Pulse";
                metaCon.MetaDescription = "Go behind the scenes to discover the stories and people who are shaping the future of Singapore today.";
                metaCon.MetaKeywords = "Singapore Spirit, SG Pulse, Singapore story, profile, idea, community, place, food, feature, project, event, spirit of partnership, Kampung Spirit, Fighting Spirit, Trailblazing Spirit";
                metaCon.PageTitle = "SG | Pulse";
            }
            else if ("{0CB3EA7B-3E71-4264-A974-4301F03ED693}" == Convert.ToString(Sitecore.Context.Item.ID.ToString()))
            {
                metaCon.PageTitle = "SG | Events Calendar";
                metaCon.OGImage = "Image Path";
                metaCon.FBID = itemconfiguration["Facebook AppID"];

                metaCon.MetaTitle = "What’s On | Local events that foster the Singapore Spirit";
                metaCon.MetaDescription = "Every event that helps to unite us as Singaporeans finds its way to this calendar. Sign up for an event to help build our Singapore Spirit.";
                metaCon.MetaKeywords = "Our SG, Our Singapore, What’s on, Featured events, Singapore events, Singapore calendar, Community activities, MCCY, Ministry of Culture, Community and Youth, Singapore Spirit";

            }
            else if ("{10ADBEF4-75F0-4E8E-9187-3DB1F2D1B03A}" == Convert.ToString(Sitecore.Context.Item.TemplateID.ToString()))
            {
                metaCon.PageTitle = "SG | Events Calendar";
                metaCon.OGImage = "Image Path";
                metaCon.FBID = itemconfiguration["Facebook AppID"];

                metaCon.MetaTitle = "What’s On | Local events that foster the Singapore Spirit";
                metaCon.MetaDescription = "Every event that helps to unite us as Singaporeans finds its way to this calendar. Sign up for an event to help build our Singapore Spirit.";
                metaCon.MetaKeywords = "Our SG, Our Singapore, What’s on, Featured events, Singapore events, Singapore calendar, Community activities, MCCY, Ministry of Culture, Community and Youth, Singapore Spirit";


                bool SocialShareImage = false;
                if (Sitecore.Context.Item.Fields["Social Share Image"] != null)
                {
                    Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Social Share Image"];
                    if (img != null)
                    {
                        MediaItem mItem = img.MediaItem;
                        if (mItem != null)
                        {
                            string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                            metaCon.OGImage = itemconfiguration["Host Name"] + imgSrc;
                            SocialShareImage = true;
                        }
                    }

                }

                if (SocialShareImage == false)
                {
                    if (Sitecore.Context.Item.Fields["Event Overlay Image"] != null)
                    {
                        Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Event Overlay Image"];
                        if (img != null)
                        {
                            MediaItem mItem = img.MediaItem;
                            if (mItem != null)
                            {
                                string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                                metaCon.OGImage = itemconfiguration["Host Name"] + imgSrc;
                            }
                        }

                    }
                }
            }


            if ("{5433A3B4-9A3B-4674-87B5-CADC90AADE63}" == Convert.ToString(Sitecore.Context.Item.TemplateID.ToString()))
            {


                metaCon.FBID = itemconfiguration["Facebook AppID"];

                if (Sitecore.Context.Item.Fields["Social Share Image"] != null)
                {

                    Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Social Share Image"];
                    if (img != null)
                    {
                        MediaItem mItem = img.MediaItem;
                        if (mItem != null)
                        {
                            string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                            metaCon.OGImage = itemconfiguration["Host Name"] + imgSrc;
                        }
                    }

                }

                //if (string.IsNullOrEmpty(metaCon.OGImage))
                //{
                //    if (Sitecore.Context.Item.Fields["Desktop Banner"] != null)
                //    {
                //        Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Desktop Banner"];
                //        if (img != null)
                //        {
                //            MediaItem mItem = img.MediaItem;
                //            if (mItem != null)
                //            {
                //                string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                //                metaCon.OGImage = itemconfiguration["Host Name"] + imgSrc;
                //            }
                //        }
                //    }
                //}

            }

            ViewData["Meta"] = metaCon;
            return View(metaCon);
        }

        [HttpGet]
        public ActionResult SearchResults()
        {
            List<SearchResults> results = new List<SearchResults>();
            SearchResults _res = new SearchResults();

            try
            {
                List<MyResultItem> objResult = new List<MyResultItem>();
                MyResultItem obj = new MyResultItem();

                if (Request.QueryString["searchStr"] != null && !string.IsNullOrEmpty(Request.QueryString["searchStr"].ToString()))
                {
                    string _queryStr = Request.QueryString["searchStr"].ToString();


                    if (!string.IsNullOrEmpty(_queryStr))
                    {
                        var index = ContentSearchManager.GetIndex("sitecore_web_index");


                        using (var context = index.CreateSearchContext())
                        {
                            var res = context.GetQueryable<MyResultItem>().Where(x => x.Name == "events-calender").ToList();

                            if (res != null)
                            {
                                if (res.Count() > 0)
                                {
                                    foreach (MyResultItem item in res)
                                    {
                                        obj.Name = item.Name;
                                        obj.ItemId = item.ItemId;
                                        objResult.Add(obj);
                                        return View(objResult);
                                    }
                                }
                                else
                                {
                                    ViewBag.dat = "count 0";
                                }
                            }
                            else
                            {
                                ViewBag.dat = "epty";

                            }
                            return View();

                            //return View(res);
                        }
                    }
                    else
                    {
                        obj.PageTitle = "Not empty Query string";
                        obj.PageTitle = "Title from QS";
                        objResult.Add(obj);
                        return View(objResult);
                    }
                }
                else
                {
                    obj.PageTitle = "raju";
                    obj.PageTitle = "title";
                    objResult.Add(obj);

                    return View(objResult);
                }

            }
            catch (Exception msg) { throw msg; }
        }

        [HttpGet]
        public ActionResult Notfound()
        {
            try
            {
                return View();
            }
            catch (Exception msg)
            {
                throw msg;
            }
        }

        /* Get the Selected items count from multi list field in root item */
        int GetSelectedItemsCount(string itemId, string fieldName)
        {
            try
            {
                if (!string.IsNullOrEmpty(itemId) && !string.IsNullOrEmpty(fieldName))
                {
                    Item rootItem = _web.GetItem(itemId);

                    if (rootItem != null)
                    {
                        ID[] ids = ((MultilistField)rootItem.Fields[fieldName]).TargetIDs;

                        if (ids != null && ids.Count() > 0)
                        {
                            return ids.Count();
                        }
                    }
                } return 0;
            }
            catch (Exception msg) { throw msg; }
        }

        public ActionResult Test()
        {

            Item[] items = _web.GetItem("{0CB3EA7B-3E71-4264-A974-4301F03ED693}").GetChildren().
                     Where(x => ((DateField)x.Fields["Event Start Date"]).DateTime.Date != new DateTime(0001, 01, 01)
                         && ((DateField)x.Fields["Event Start Date"]).DateTime.Date <= DateTime.Now.Date
                         && ((DateField)x.Fields["Event End Date"]).DateTime.Date >= DateTime.Now.Date
                         && ((DateField)x.Fields["Event End Date"]).DateTime.Date != new DateTime(0001, 01, 01)).Select(x => x).ToArray();



            ViewBag.Count = items.Length.ToString();

            return View();
        }
    }

    /// <summary>
    /// Search results object for binding to frontend
    /// </summary>
    public class SearchResult
    {
        public string Title { get; set; }

        public string Summary { get; set; }

        public string Link { get; set; }
    }

    /// <summary>
    /// Custom sitecore search results item with required fields
    /// </summary>
    public class MyResultItem : SearchResultItem
    {
        [IndexField("Title")]
        public string PageTitle { get; set; }

        [IndexField("Content")]
        public string PageContent { get; set; }
    }
}


