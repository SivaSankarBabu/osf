﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace SGEventsCalendar.Controllers
{
    public static class SGEventslogger
    {
         static string ErrorLogFile, TraceLogFile;
         static SGEventslogger()
        {
            string fpath = HttpContext.Current.Server.MapPath("\\SGEvents\\LogFiles\\");

            ErrorLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_ErroLogFile.txt";
            TraceLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_TraceLogFile.txt";
        }

        private static string GetExceptionMessage(Exception ex)
        {
            string retValue = string.Format("Message: {0}\r\nStackTrace: {1}", ex.Message, ex.StackTrace);
            if (ex.InnerException != null)
            {
                retValue = retValue + string.Format("\r\n\r\nInner Exception: {0}", GetExceptionMessage(ex.InnerException));
            }
            return retValue;
        }

        public static void WriteException(Exception ex, string AdditionalInfo)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n\r\nAdditional Info: {2}\r\n{3}\r\n\r\n", DateTime.Now, GetExceptionMessage(ex), AdditionalInfo, new string('-', 60));

            File.AppendAllText(ErrorLogFile, finalMessage);
        }

        public static void WriteLine(string Message)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n", DateTime.Now, Message);

            File.AppendAllText(TraceLogFile, finalMessage);
        }
    }
}
