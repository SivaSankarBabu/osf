﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data;
using SGEventsCalendar.Models;
using Sitecore.Data.Fields;
using Sitecore.Links;
using System.IO;

namespace SGEventsCalendar.Models
{

    public class SGEventsDAL
    {
        Database web = Database.GetDatabase("web");

        /* Get Header Data */
        public Header Get_Header_Content(string itemId)
        {
            Header objHeader = new Header();

            if (!string.IsNullOrEmpty(itemId))
            {
                Item item = web.GetItem(itemId);

                if (item != null)
                {
                    string[] logoAlt = CommonFun.GetImage(item, "Logo"); string[] guideLinesLogoAlt = CommonFun.GetImage(item, "download-icon");
                    objHeader.Logo = logoAlt[0];
                    objHeader.LogoAlt = logoAlt[1];
                    objHeader.GuidelinesLogo = guideLinesLogoAlt[0];
                    objHeader.GuidelinesLogoAlt = guideLinesLogoAlt[1];
                    objHeader.GuidelinesCTA = Convert.ToString(item.Fields["Download SG logo and Guidelines Button Text"]);
                    objHeader.Menu = GetMenu(item);

                    return objHeader;
                }
                else return new Header();
            }
            else return new Header();
        }

        /* Get Header Data */
        public Footer Get_Footer_Content(string itemId)
        {
            Footer objFooter = new Footer();

            if (!string.IsNullOrEmpty(itemId))
            {
                Item item = web.GetItem(itemId);

                if (item != null)
                {
                    objFooter.FooterContent = Convert.ToString(item.Fields["Footer"]);
                    objFooter.GuideLines = GetDownloadGuidelinesContent(Constants.brandAssetsItemId);
                    objFooter.FilePath = CommonFun.GetFilePath(item, "Download File");

                    return objFooter;
                }
                else return new Footer();
            }
            else return new Footer();
        }

        public DownloadLogoGuideLines GetDownloadGuidelinesContent(string itemId)
        {
            DownloadLogoGuideLines obj = new DownloadLogoGuideLines();

            if (!string.IsNullOrEmpty(itemId))
            {
                Item item = web.GetItem(itemId);

                if (item != null)
                {
                    string[] logoAlt = CommonFun.GetImage(item, "Logo");
                    obj.Logo = logoAlt[0];
                    obj.LogoAlt = logoAlt[1];

                    obj.Title = Convert.ToString(item.Fields["Title"]);
                    obj.ShortDesc = Convert.ToString(item.Fields["Short Description"]);
                    obj.FullDesc = Convert.ToString(item.Fields["Full Description"]);
                    obj.CTAText = Convert.ToString(item.Fields["ButtonText"]);
                    obj.filePath = CommonFun.GetFilePath(item, "Download File");
                }
            }
            return obj;
        }

        /* Get Menu */
        List<string> GetMenu(Item item)
        {
            List<string> _menu = new List<string>();
            string[] fieldNames = new string[] { "Whats On", "Futured Stories Menu Text", "Singapore Cares Menu Text", "SGFuture Menu Text", "OSF Menu Text", "Our SG Menu Text" };

            if (item != null)
            {
                for (int index = 0; index < fieldNames.Length; index++)
                {
                    _menu.Add(Convert.ToString(item.Fields[fieldNames[index]]));
                }
                return _menu;
            }
            else { return new List<string>(); }
        }

        /* Get Banners */
        public List<Banner> GetBanners(string itemId, int count)
        {
            List<Banner> _banners = new List<Banner>(); Banner banObj = null;
            List<Item> _items = new List<Item>();

            if (!string.IsNullOrEmpty(itemId))
            {
                Item item = web.GetItem(itemId);

                if (item != null)
                    _items = item.GetChildren().Take(count).ToList();

                if (_items != null && _items.Count > 0)
                {
                    foreach (Item _item in _items)
                    {
                        banObj = new Banner();

                        string[] deskImgAlt = CommonFun.GetImage(_item, "Desktop Image"); string[] mobImgAlt = CommonFun.GetImage(_item, "Mobile Image");
                        banObj.Title = Convert.ToString(_item.Fields["Banner Overlay Text"]);
                        banObj.DeskImage = deskImgAlt[0];
                        banObj.DeskImageAlt = deskImgAlt[1];

                        banObj.MobImage = mobImgAlt[0];
                        banObj.MobImageAlt = mobImgAlt[1];

                        banObj.Description = Convert.ToString(_item.Fields["Banner Overlay Description"]);
                        //banObj.Url = Convert.ToString(_item.Fields["Link"]);

                        Sitecore.Data.Fields.LinkField linkField = _item.Fields["Link"];
                        if (linkField != null && !string.IsNullOrEmpty(linkField.Url))
                        {
                            if (linkField.IsInternal)
                                banObj.Url = linkField.Url;
                            else if (linkField.IsMediaLink)
                                banObj.Url = "/" + "~/media/" + linkField.Url;
                            else
                            {
                                if (linkField.Url.Contains("http://") || linkField.Url.Contains("https://"))
                                    banObj.Url = linkField.Url;
                            }
                        }

                        //banObj.Url = Convert.ToString(CommonFun.LinkUrl(_item, "Link"));

                        _banners.Add(banObj);
                    }
                    return _banners;
                }
                else { return _banners; }
            }
            else { return _banners; }
        }

        /* Get Stories and Events Tiles in Landing Page */
        List<Item> GetStoriesEventsLanding(Item _item, string[] fields, string type)
        {
            if (_item != null && fields.Length > 0)
            {
                List<Item> _items = new List<Item>();
                if (type.ToLower() == "m")
                {
                    ID[] ids = ((MultilistField)_item.Fields[fields[0]]).TargetIDs;

                    if (ids.Count() > 0)
                    {
                        for (var index = 0; index < ids.Length; index++)
                        {
                            Item _tarItem = web.GetItem(ids[index]);

                            if (_tarItem != null)
                                _items.Add(_tarItem);
                        }
                    }
                }
                else
                {
                    for (var index = 0; index < fields.Length; index++)
                    {
                        Item _tarItem = ((InternalLinkField)_item.Fields[fields[index]]).TargetItem;

                        if (_tarItem != null)
                            _items.Add(_tarItem);
                    }
                }
                return _items;
            }
            return new List<Item>();
        }

        /* Get SG Events */
        public List<SGEvent> GetEvents(string itemId, string[] fieldNames, string type)
        {
            List<SGEvent> _sgEvents = new List<SGEvent>(); SGEvent _sgEve = null;

            List<Item> _items = new List<Item>();

            DateTime now = DateTime.Now;

            if (!string.IsNullOrEmpty(itemId))
            {
                Item item = web.GetItem(itemId);

                if (item != null)
                {
                    if (fieldNames != null)
                    {
                        if (type.ToLower() == "m")
                        {
                            _items = GetStoriesEventsLanding(item, fieldNames, "M");
                        }
                        else
                        {
                            _items = GetStoriesEventsLanding(item, fieldNames, "L").ToList();
                        }
                    }
                    else
                        _items = item.GetChildren().Distinct().OrderBy(x => x.Fields["Event Start Date"].Value).ToList();
                }

                if (_items != null && _items.Count > 0)
                {
                    foreach (Item _item in _items)
                    {
                        _sgEve = new SGEvent();
                        string[] thumbImgAlt = CommonFun.GetImage(_item, "Event Image"); string[] eventLargeImgAlt = CommonFun.GetImage(_item, "Event Overlay Image");

                        _sgEve.ThumbImg = thumbImgAlt[0];
                        _sgEve.ThumbImgAlt = thumbImgAlt[1];
                        _sgEve.DetailLargeImg = eventLargeImgAlt[0]; _sgEve.DetailLargeImgAlt = eventLargeImgAlt[1];
                        _sgEve.FromDate = ((DateField)_item.Fields["Event Start Date"]).DateTime;
                        _sgEve.ToDate = ((DateField)_item.Fields["Event End Date"]).DateTime;
                        _sgEve.Name = Convert.ToString(_item.Fields["Event Name"]);
                        _sgEve.Description = Convert.ToString(_item.Fields["Event Description"]);

                        _sgEve.Category = ((LookupField)_item.Fields["Event Category"]).TargetItem != null ? Convert.ToString(((LookupField)_item.Fields["Event Category"]).TargetItem.Name) : string.Empty;
                        _sgEve.Type = ((LookupField)_item.Fields["Event Type"]).TargetItem != null ? Convert.ToString(((LookupField)_item.Fields["Event Type"]).TargetItem.Name) : string.Empty;
                        _sgEve.AudienceType = ((LookupField)_item.Fields["Audience Type"]).TargetItem != null ? Convert.ToString(((LookupField)_item.Fields["Audience Type"]).TargetItem.Name) : string.Empty;
                        _sgEve.EntryType = Convert.ToString(_item.Fields["Event Entry Type"]);

                        _sgEve.Location = Convert.ToString(_item.Fields["Event Location"]);
                        _sgEve.Url = Convert.ToString(CommonFun.LinkUrl(_item, "Event URL"));
                        _sgEve.GoogleMapLink = Convert.ToString(CommonFun.LinkUrl(_item, "Google Map Link"));
                        _sgEve.PurchaseTicketURL = CommonFun.LinkUrl(_item, "Purchase Ticket URL");

                        _sgEve.SocShareTitle = Convert.ToString(_item.Fields["Social Share Title"]);
                        _sgEve.SocShareDesc = Convert.ToString(_item.Fields["Social Share Description"]);
                        _sgEve.SocShareImg = Convert.ToString(CommonFun.GetImage(_item, "Social Share Image"));

                        /* Details Footer Content */
                        _sgEve.NoteTitle = Convert.ToString(_item.Fields["Got a Similar Title"]);
                        _sgEve.NoteContent = Convert.ToString(_item.Fields["Got a Similar Content"]);
                        _sgEve.TermsTitle = Convert.ToString(_item.Fields["Terms Title"]);
                        _sgEve.EventDetailsMail = Convert.ToString(_item.Fields["Email"]);
                        _sgEve.TermsCondExternalLink = Convert.ToString(CommonFun.LinkUrl(_item, "Terms Link"));

                        if (((CheckboxField)_item.Fields["All Day"]) != null)
                            _sgEve.AllDay = ((CheckboxField)_item.Fields["All Day"]).Checked;



                        _sgEve.item = _item;

                        _sgEvents.Add(_sgEve);
                    }
                    return _sgEvents;
                }
                else return _sgEvents;
            }
            else return _sgEvents;
        }

        /* Get Stories or Featured News  */
        public List<Story> GetStories(string itemId, string[] fieldNames, Item _conItem)
        {
            List<Story> _stories = new List<Story>(); Story _story = null;

            if (!string.IsNullOrEmpty(itemId))
            {
                List<Item> _items = null;

                try
                {
                    Item storiesItem = web.GetItem(Constants.storiesItemId);

                    if (storiesItem != null)
                    {
                        if (_conItem != null)
                        {
                            _items = storiesItem.GetChildren().Where(x => x.ID.ToString() != _conItem.ID.ToString()).OrderByDescending(x => new DateField(x.Fields["Event Date"]).DateTime).ToList();
                        }
                        else
                        {
                            if (fieldNames != null && fieldNames.Length > 0)
                                _items = GetStoriesEventsLanding(storiesItem, fieldNames, "L");
                            else
                                _items = storiesItem.GetChildren().ToList();
                        }
                    }

                    if (_items != null && _items.Count > 0)
                    {
                        foreach (Item _item in _items)
                        {
                            _story = new Story();

                            string[] thumbImgAlt = CommonFun.GetImage(_item, "Image Tile for Box 1 or 2"); string[] wideImgAlt = CommonFun.GetImage(_item, "Image Tile for Box3");
                            _story.ThumbImg = thumbImgAlt[0];
                            _story.ThumbImgAlt = thumbImgAlt[1];

                            _story.WideImage = wideImgAlt[0];
                            _story.WideImageAlt = wideImgAlt[1];

                            _story.Title = Convert.ToString(_item.Fields["Title"]);
                            _story.EventDate = ((DateField)_item.Fields["Event Date"]).DateTime;
                            _story.IsEventDateHide = ((CheckboxField)_item.Fields["HideEventDate"]).Checked;
                            _story.ShortDesc = Convert.ToString(_item.Fields["Short Description"]);
                            _story.ReadMoreCTA = Convert.ToString(_item.Fields["CTA Button Text"]);
                            _story.YoutubeURL = Convert.ToString(_item.Fields["Youtube URL"]);
                            _story.ExternalLink = Convert.ToString(_item.Fields["External Link"]);
                            _story.Categories = GetCategories(_item, "Categories", "Category Name");
                            _story.item = _item;

                            _stories.Add(_story);
                        }
                        return _stories;
                    }

                    else return _stories;
                }
                catch (Exception mg)
                {
                    throw mg;
                }
            }
            else return _stories;
        }

        public Story GetFeaturedStoryDetails(Item item)
        {
            try
            {
                Story _objDetails = new Story();

                if (item != null)
                {
                    string[] deskBanner = CommonFun.GetImage(item, "Desktop Banner"); string[] mobBanner = CommonFun.GetImage(item, "Mobile Banner");
                    _objDetails.DeskBanner = deskBanner[0];
                    _objDetails.DeskBannerAlt = deskBanner[1];
                    _objDetails.MobBanner = mobBanner[0];
                    _objDetails.MobBannerAlt = mobBanner[1];
                    _objDetails.EventDate = ((DateField)item.Fields["Event Date"]).DateTime;
                    _objDetails.IsEventDateHide = ((CheckboxField)item.Fields["HideEventDate"]).Checked;
                    _objDetails.Title = Convert.ToString(item.Fields["Title"]);
                    _objDetails.TopDesc = Convert.ToString(item.Fields["Top Description"]);
                    _objDetails.BottomDesc = Convert.ToString(item.Fields["Bottom Description"]);
                    _objDetails.QuoteText = Convert.ToString(item.Fields["Quote Text"]);
                    _objDetails.QuoteAuthor = Convert.ToString(item.Fields["Quote Author"]);
                    _objDetails.Categories = GetCategories(item, "Categories", "Category Name");
                    _objDetails.CarouselItems = GetCarouselBanners(item, "Gallery");
                    _objDetails.SocTitle = Convert.ToString(item.Fields["Social Share Title"]);
                    _objDetails.SocShareDesc = Convert.ToString(item.Fields["Social Share Content"]);
                    _objDetails.SocShareImg = Convert.ToString(CommonFun.GetImage(item, "Social Share Image")[0]);
                    _objDetails.item = item;

                    return _objDetails;
                }
                return new Story();
            }
            catch (Exception msg)
            { throw msg; }
        }

        /* Get Carousel Bannres from Tree list field in Story item */
        public List<Carousel> GetCarouselBanners(Item item, string fldName)
        {
            List<Carousel> carousels = new List<Carousel>();

            if (item != null && item.Fields[fldName] != null)
            {
                MultilistField mlGallery = item.Fields[fldName];

                if (mlGallery != null)
                {
                    Carousel _objCar = null;

                    foreach (ID id in mlGallery.TargetIDs)
                    {
                        MediaItem mediaitem = web.GetItem(id.ToString());

                        if (mediaitem != null)
                        {
                            _objCar = new Carousel();

                            _objCar.ItemImage = Sitecore.Resources.Media.MediaManager.GetMediaUrl(web.GetItem(id));
                            _objCar.ImgAltText = mediaitem.Alt;
                        }
                        else
                        {
                            if (mediaitem.Alt == string.Empty)
                                _objCar.ImgAltText = mediaitem.Name;
                        }

                        carousels.Add(_objCar);
                    }
                }
                else { return carousels; }
            }
            return carousels;
        }

        public List<string> GetCategories(Item item, string fldName, string tarItemFld)
        {
            List<string> _cats = new List<string>();

            if (item != null && !string.IsNullOrEmpty(fldName))
            {
                MultilistField mlGallery = item.Fields[fldName];

                if (mlGallery != null)
                {
                    foreach (ID id in mlGallery.TargetIDs)
                    {
                        _cats.Add(Convert.ToString(web.GetItem(id).Fields[tarItemFld]));
                    }
                    return _cats;
                }
                return _cats;
            }
            return _cats;
        }

        /* Get Categories */
        public List<Category> GetCategories(string _catsItemId)
        {
            try
            {
                if (!string.IsNullOrEmpty(_catsItemId))
                {
                    Item item = web.GetItem(new ID(_catsItemId));

                    if (item != null)
                    {
                        return item.GetChildren().Select(x => new Category
                        {
                            Text = Convert.ToString(x.Name.Trim()),
                            Value = Convert.ToString(x.Fields["Value"]),
                            AllOption = Convert.ToString(item.Fields["All Option"])

                        }).ToList();
                    }
                }
                return new List<Category>();
            }
            catch (Exception ex) { return new List<Category>(); throw ex; }
        }

        /* Get New Event Data */
        public NewEvent GetNewEventData(string itemId)
        {
            NewEvent _new = new NewEvent();

            if (!string.IsNullOrEmpty(itemId))
            {
                Item item = web.GetItem(itemId);

                if (item != null)
                {
                    _new.TermsUrl = Convert.ToString(CommonFun.LinkUrl(item, "Terms and Conditions URL"));
                    _new.TermsConditionsCTA = Convert.ToString(item.Fields["Terms and Conditions CTA"]);
                    _new.NewBlockTitle = Convert.ToString(item.Fields["Title"]);
                }
            }
            return _new;
        }

        /* Get Overlay Content in Events Lising page i.e. *Tell us* popup in page introduciton desc */
        public OveryLayContent GetOverLayContent(Item item)
        {
            try
            {
                OveryLayContent objCon = new OveryLayContent();

                if (item != null)
                {
                    objCon.OverlayTitle = Convert.ToString(item.Fields["OverlayTitle"]);
                    objCon.OverlayContent = Convert.ToString(item.Fields["OverlayContent"]);
                    objCon.OverlayDescription = Convert.ToString(item.Fields["OverlayDescription"]);
                    objCon.Email = Convert.ToString(item.Fields["Email"]);
                    objCon.CTAText = Convert.ToString(item.Fields["CTA Text"]);

                    return objCon;
                }
                else
                    return new OveryLayContent();
            }
            catch (Exception msg) { throw msg; }
        }

    }
}
