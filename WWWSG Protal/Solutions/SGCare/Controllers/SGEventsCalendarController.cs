﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGCare.Models;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace SGCare.Controllers
{
    public class SGEventsCalendarController : Controller
    {
        SGEventsCalendarDAL _sgEvents = new SGEventsCalendarDAL();

        Database web = Database.GetDatabase("web");

        public ActionResult SGHome()
        {
            Home home = new Home();

            try
            {
                return View(home);
            }
            catch
            {
                return View(home);
            }
        }

        public string Header()
        {
            Header _head = new Header();

            try
            {
                Item item = web.GetItem(SGEventsConstants.sgItemId);
                return Convert.ToString(item.Fields["Download SG logo and Guidelines"]);

                _head.Logo = Convert.ToString(CommonFun.GetImage(item, "Logo"));
                _head.DownloadIconImg = Convert.ToString(CommonFun.GetImage(item, "download-icon"));
                _head.DownloadLogoCTA = Convert.ToString(item.Fields["Download SG logo and Guidelines"]);
                _head.Menu = _sgEvents.Get_Menu(item, SGEventsConstants.Menu);



               //return View(_head);
            }
            catch (Exception msg) { throw msg; }
        }

        public ActionResult Footer()
        {
            return View();
        }

    }
}
