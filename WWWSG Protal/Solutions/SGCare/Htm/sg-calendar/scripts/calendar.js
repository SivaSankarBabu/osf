'use strict';

var SGF = window.SGF || {};

SGF.calendar = {
	init: function init() {
		SGF.calendar.initCustomSelect();
		SGF.calendar.initSubmitEventPopup();
		SGF.calendar.initMonthFilter();
		SGF.calendar.initFilter();
		SGF.calendar.initMonthFilterMobile();
		SGF.calendar.initEventDetailPopup();
		SGF.calendar.initHomeSlider();
		SGF.calendar.initEvalHeight();
	},

	initFilter: function initFilter() {
		$('.btn-filter').on('click', function (e) {
			e.preventDefault();
			$(this).parents('.section-filter').toggleClass('active');
			$('.filter-bg').toggleClass('active');
		});

		$('.filter-bg').on('click', function (e) {
			e.preventDefault();
			$('.section-filter').removeClass('active');
			$(this).removeClass('active');
		});
	},

	initCustomSelect: function initCustomSelect() {
		function initSelect() {
			if ($(window).width() > 767 && $('select').length) {
				$('select').niceSelect();
				if ($('select').length) {
					$(document).on('click.nice_select', '.nice-select .option:not(.disabled)', function (e) {
						var $option = $(this);
						var $dropdown = $option.closest('.nice-select');
						var current = $option.data('value');
						var first = $dropdown.prev('select').find('option:first-child').val();

						if (current !== first) {
							$dropdown.addClass('active');
						} else {
							$dropdown.removeClass('active');
						}
					});
				}
			} else {
				if ($('select').length) {
					$('select').niceSelect('destroy');
				}
			}
		}

		initSelect();

		$(window).on('resize', function () {
			initSelect();
		});
	},

	initSubmitEventPopup: function initSubmitEventPopup() {
		$('#submitEventGuides').hide();
		$('.block-event.block-add .btn-add, .section-head .link').click(function () {
			$('#submitEventGuides').fadeIn(300);
			$('body').addClass('hideScrool');
		});
		$('.submitEventHeader a').click(function () {
			$('#submitEventGuides').fadeOut(300);
			$('body').removeClass('hideScrool');
		});
		$('#submitEventGuides').on('click', function (e) {
			if (e.target == this) {
				$('#submitEventGuides').fadeOut(300);
				$('body').removeClass('hideScrool');
			}
		});
	},

	initMonthFilter: function initMonthFilter() {
		if ($('.section-calendar').length) {
			$('.section-calendar').on('click', '.block-event.month .btn-action', function (e) {
				e.preventDefault();
				e.stopPropagation();
				$(this).toggleClass('collapsed');
				$(this).find('.text').html($(this).is('.collapsed') ? 'collapse' : 'expand');
				//
				var wrap = $(this).parents('.month');
				var target = wrap.attr('data-month');
				var list = wrap.siblings('[data-month="' + target + '"]');
				list.toggleClass('hidden');
			});
			$('.section-calendar').on('click', '.block-event.month', function (e) {
				e.preventDefault();
				$(this).find('.btn-action').trigger('click');
			});
		}
	},

	initMonthFilterMobile: function initMonthFilterMobile() {
		var slider = null;

		function initSlider() {
			if (!slider) {
				if ($('.wrap-mobile-month:visible').length) {
					var display = 3;
					var width = $('.wrap-mobile-month:visible').width() / display;
					slider = $('.wrap-mobile-month:visible .list').bxSlider({
						minSlides: display,
						maxSlides: display,
						moveSlides: display,
						slideWidth: width,
						controls: true,
						pager: false,
						prevText: '<i class="fa fa-caret-left"></i>',
						nextText: '<i class="fa fa-caret-right"></i>'
					});
				}
			} else {
				if ($(window).width() > 767) {
					slider.destroySlider();
					slider = null;
				} else {
					slider.reloadSlider();
				}
			}
		}

		if ($('.wrap-mobile-month:visible').length > 0) {
			initSlider();
		}

		$(window).on('resize', function () {
			initSlider();
		});

		//handle click month on slider
		$('.wrap-mobile-month').on('click', '.item a', function (e) {
			e.preventDefault();
			$('.item a').removeClass('active');
			$(this).addClass('active');
			var target = $(this).attr('data-month');
			var list = $('.wrap-calendar .block-event').filter('[data-month="' + target + '"]');
			$('.wrap-calendar .block-event').filter(':not(.block-add)').filter(':not(.block-more)').filter(':not(.month)').addClass('hidden');
			if (list.length) {
				$('.wrap-calendar .block-event.month').filter('[data-month="' + target + '"]').find('.btn-action').trigger('click');
			}
		});

		//handle sticky
		$(window).on('scroll', function () {
			if ($('.wrap-mobile-month:visible').length > 0) {
				var top = $(window).scrollTop();
				var sticky = $('.wrap-mobile-month');
				var anchor = $('.wrap-calendar').offset().top - sticky[0].clientHeight - $('header')[0].clientHeight;
				if (top > anchor && !sticky.is('.sticky')) {
					sticky.addClass('sticky');
					$('.wrap-calendar').addClass('anchor');
				}
				if (top < anchor && sticky.is('.sticky')) {
					sticky.removeClass('sticky');
					$('.wrap-calendar').removeClass('anchor');
				}
			}
		});
	},

	initEventDetailPopup: function initEventDetailPopup() {
		if ($('.section-calendar').length) {
			$('.section-calendar').on('click', '.block-event a.inner', function (e) {
				e.preventDefault();
				$("#eventDetail").fadeIn(300);
				$("body").addClass("hideScrool");
				// get data
				var wrap = $(this).parents('.block-event');
				var isDisable = wrap.is('.disable');
				var imagePath = wrap.attr('data-img');
				// bind data
				if (isDisable) {
					$("#eventDetail").find('.eventDetailPopupContainer').addClass('disable');
				} else {
					$("#eventDetail").find('.eventDetailPopupContainer').removeClass('disable');
				}
				if (imagePath) {
					$("#eventDetail").find('.wrap-thumb .thumb').attr('src', imagePath);
				}
			});

			$('.eventDetailHeader a, .eventDetailHeaderMobile .close').on('click', function () {
				$('#eventDetail').fadeOut(300);
				$('body').removeClass('hideScrool');
			});

			$('#eventDetail').on('click', function (e) {
				if (e.target == this) {
					$('#eventDetail').fadeOut(300);
					$('body').removeClass('hideScrool');
				}
			});
		}
	},

	initHomeSlider: function initHomeSlider() {
		if ($('.page-event-listing').length) {
			return;
		}

		var slider = null;

		function initSlider() {
			if (!slider) {
				if ($('.wrap-calendar').length && $(window).width() < 767) {
					var width = $(window).width() - 50;
					width = width > 280 ? 280 : width;
					slider = $('.wrap-calendar').bxSlider({
						minSlides: 1,
						maxSlides: 1,
						moveSlides: 1,
						controls: false,
						slideWidth: width,
						infiniteLoop: false,
						pager: true
					});
				}
			} else {
				if ($(window).width() > 767) {
					slider.destroySlider();
					slider = null;
				} else {
					var width = $(window).width() - 50;
					width = width > 280 ? 280 : width;
					slider.reloadSlider({
						minSlides: 1,
						maxSlides: 1,
						moveSlides: 1,
						controls: false,
						slideWidth: width,
						infiniteLoop: false,
						pager: true
					});
				}
			}

			var height = $('.block-event:not(.block-more)')[0].clientHeight;
			$('.block-event.block-more').css({ height: height });
		}

		$(window).on('load', function () {
			if ($('.wrap-calendar').length > 0) {
				initSlider();
			}
		});

		var lastWidth = 0;
		$(window).on('resize', function () {
			var currentWidth = $(window).width();
			if (currentWidth != lastWidth) {
				initSlider();
			}
			lastWidth = currentWidth;
		});
	},

	initEvalHeight: function initEvalHeight() {
		function evalHeight() {
			if ($('.wrap-calendar').length) {
				var stdHeight = $('.block-event').filter(':not(.hidden):not(.month):not(.comming):not(.block-more):not(.block-add)').get(0).getBoundingClientRect().height;
				$('.block-event.month').css({ height: stdHeight });
				$('.block-event.comming').css({ height: stdHeight });
				$('.block-event.block-more').css({ height: stdHeight });
				$('.block-event.block-add').css({ height: stdHeight });
			}
		}
		evalHeight();

		$(window).on('resize', function () {
			evalHeight();
		});
	}
};

$(function () {
	SGF.calendar.init();
});
//# sourceMappingURL=calendar.js.map
