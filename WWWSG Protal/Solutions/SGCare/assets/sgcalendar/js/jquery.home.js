$(document).ready(function() {

    var data = '';

    $('.pagiNation').on("click", "a", function(event) {

        var option = $(this).data('option');
        var cnt = $('#tot_rcds_cnt').val();
        //console.log(option, cnt);
        if (option == 'last' || option == cnt) {
            option = cnt;
            // $('#next').attr( 'class', 'disabled' );
        }
        if (option != cnt) {
            $('#next').attr('data-option', option + 1);
        }

        $(" ul.pagiNation li a").removeClass('active');
        $('.storyGrid').html('<div class="loader" align="center"><img src="images/loader.gif" /></div>');
        $("a[data-option=" + option + " ]").addClass('active');
        $("#next").removeClass('active');
        $.ajax({
            url: "ajax/getFeaturedStories" + option + ".html?q=" + option,
            success: function(data) {
                $('.storyGrid').html(data);
            }
        });
    });

});