﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Publishing;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web;

//namespace SGCare.Models
//{
//    public class SGCareTrace
//    {
//        static string ErrorLogFile, TraceLogFile;

//        private static Database master = Sitecore.Configuration.Factory.GetDatabase("master");
//        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");

//        string sgCareItemId = "{C24A1BAA-3ED0-4AA4-BD68-A6A7E8EA7299}", contentItemId = "{0DE95AE4-41AB-4D01-9EB0-67441B7C2450}";

//        public void OnPublishEnd(object sender, EventArgs args)
//        {
//            try
//            {
//                Publisher publisher = Sitecore.Events.Event.ExtractParameter(args, 0) as Publisher;
//                var lan = publisher.Options.Language.Name;
//                Item masterItem = master.GetItem(sgCareItemId), webItem = web.GetItem(sgCareItemId);

//                if (lan == "en")
//                {
//                    DataSet ds = new DataSet();
//                    WebClient webClient = new WebClient();

//                    var rootItemId = publisher.Options.RootItem.ID.ToString();

//                    if (rootItemId.ToString() == sgCareItemId || rootItemId.ToString() == contentItemId)
//                    {
//                        WriteLine(Convert.ToString("****************************************************" + DateTime.Now.ToString() + "****************************************************"));
//                        WriteLine(Sitecore.Context.User.Name);
//                        WriteLine(Convert.ToString(masterItem.Fields["Offline Pledge Count"]));
//                        WriteLine(Convert.ToString(webItem.Fields["Offline Pledge Count"]));

//                        return;
//                    }
//                    else
//                    {
//                        Item sgCareItem = master.GetItem(sgCareItemId);
//                    }
//                }
//            }
//            catch (Exception msg)
//            {
//                WriteException(msg, "Error ouccured while publising the Item or in Trace log.");
//            }
//        }

//        static SGCareTrace()
//        {
//            string fpath = HttpContext.Current.Server.MapPath("\\SGCare\\TraceLog\\");

//            ErrorLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_ErrorLogFile.txt";
//            TraceLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_TraceLogFile.txt";

//        }

//        public void WriteLine(string Message)
//        {
//            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n", DateTime.Now, Message);
//            File.AppendAllText(TraceLogFile, finalMessage);
//        }

//        public void WriteException(Exception ex, string AdditionalInfo)
//        {
//            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n\r\nAdditional Info: {2}\r\n{3}\r\n\r\n", DateTime.Now, GetExceptionMessage(ex), AdditionalInfo, new string('-', 60));

//            File.AppendAllText(ErrorLogFile, finalMessage);
//        }

//        private static string GetExceptionMessage(Exception ex)
//        {
//            string retValue = string.Format("Message: {0}\r\nStackTrace: {1}", ex.Message, ex.StackTrace);
//            if (ex.InnerException != null)
//            {
//                retValue = retValue + string.Format("\r\n\r\nInner Exception: {0}", GetExceptionMessage(ex.InnerException));
//            }
//            return retValue;
//        }
//    }
//}

namespace SGCaresOfflineCounter
{
    class CounterLog
    {
        static string TraceLogFile = @"D:\sg\Website\SGCare\TraceLog\Counter.txt";
        Database web = Database.GetDatabase("web");

        public void OnPublishEnd(object sender, EventArgs args)
        {
            Publisher publisher = Sitecore.Events.Event.ExtractParameter(args, 0) as Publisher;
            var lan = publisher.Options.Language.Name;

            if (lan == "en")
            {
                string sgCareItemId = "{C24A1BAA-3ED0-4AA4-BD68-A6A7E8EA7299}";

                var rootItemId = publisher.Options.RootItem.ID.ToString();

                if (rootItemId.ToString() == sgCareItemId)
                {
                    string strauthor = Sitecore.Context.User.Name;
                    Item item = web.GetItem(rootItemId);

                    string CounterVal = Convert.ToString(item.Fields["Offline Pledge Count"]);

                    if (!string.IsNullOrEmpty(CounterVal))
                        WriteLine("Author: " + strauthor + " and Offline Count: " + CounterVal);
                }
            }
        }

        public static void WriteLine(string Message)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n", DateTime.Now, Message);
            File.AppendAllText(TraceLogFile, finalMessage);
        }
    }
}