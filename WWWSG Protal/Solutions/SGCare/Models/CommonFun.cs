﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using System.Text;
using System.Text.RegularExpressions;

namespace SGCare.Models
{
    public class CommonFun
    {
        string[] replacedByFB = { "'", "\"", "&" }, replacedWithCharsFB = { "\\'", "%22", "" };

        public const string faceBook = "FaceBook";

        /* Get url based on type of link from sitecore admin */
        public static String LinkUrl(Item item, string fldName)
        {
            if (item != null && !string.IsNullOrEmpty(fldName))
            {
                LinkField _objLF = (LinkField)item.Fields[fldName];

                switch (_objLF.LinkType.ToLower())
                {
                    case "internal":
                        return _objLF.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(_objLF.TargetItem) : "javascript:void(0)";
                    case "media":
                        return _objLF.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(_objLF.TargetItem) : "javascript:void(0)";
                    case "external":
                    case "mailto":
                    case "javascript":
                        return _objLF.Url;
                    case "anchor":
                        return !string.IsNullOrEmpty(_objLF.Anchor) ? "#" + _objLF.Anchor : "javascript:void(0)";
                    default:
                        return _objLF.Url;
                }
            }
            return string.Empty;
        }

        /* Get Image Path from sitecore */
        public static string GetImage(Item item, string fldName)
        {
            if (item != null && !string.IsNullOrEmpty(fldName))
            {
                ImageField imgField = ((ImageField)item.Fields[fldName]);

                MediaItem mediaItem = null;

                if (imgField != null)
                    mediaItem = imgField.MediaItem;

                if (mediaItem != null) return MediaManager.GetMediaUrl(mediaItem); else return string.Empty;
            }
            else return string.Empty;
        }

        /* Get Page Count */
        public static int GetPageCount(int itemCount, string type)
        {
            if (type == "S")
            {
                if (itemCount > Constants.StoriesPageSize)
                {
                    return itemCount % Constants.StoriesPageSize == 0 ? itemCount / Constants.StoriesPageSize : (itemCount / Constants.StoriesPageSize) + 1;
                }
                return 0;
            }
            else
            {
                if (itemCount > Constants.PageSize)
                {
                    return itemCount % Constants.PageSize == 0 ? itemCount / Constants.PageSize : (itemCount / Constants.PageSize) + 1;
                }
                return 0;
            }
        }

        /* Get Item Path */
        public static string GetItemPath(Item item, string fieldName)
        {
            if (item != null)
            {
                Item tarItem = ((InternalLinkField)item.Fields[fieldName]).TargetItem;

                if (tarItem != null)
                {
                    return Convert.ToString(LinkManager.GetItemUrl(tarItem));
                }
                else { return string.Empty; }
            }
            else { return string.Empty; }
        }

        /* Build String */
        public string BuildString(string value, string type)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (type == "FaceBook")
                    return ModifyString(value, type);
                else return string.Empty;
            }
            else return string.Empty;
        }

        string ModifyString(string value, string type)
        {
            if (type == faceBook)
            {
                for (int index = 0; index < replacedByFB.Length; index++)
                {
                    value = value.Replace(replacedByFB[index], replacedWithCharsFB[index]);
                }
            }
            return value;
        }

        /* Encode and Decode String*/
        public static string Encode_Decode(string _text, string type)
        {
            if (!string.IsNullOrEmpty(_text))
            {
                if (type == Constants.encode)
                {
                    var _value = Encoding.UTF8.GetBytes(_text);
                    return Convert.ToBase64String(_value);
                }
                else if (type == Constants.decode)
                {

                    if ((_text.Length % 4 == 0) && Regex.IsMatch(_text, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                    {
                        var _value = Convert.FromBase64String(_text);
                        return Encoding.UTF8.GetString(_value);
                    }
                    return Constants.AllCats;
                }
                else { return string.Empty; }
            }
            else { return string.Empty; }
        }

        /* Truncate word */
        public static string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length <= length)
                return input;
            int iNextSpace = input.LastIndexOf(" ", length);
            return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }
    }
}