﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCare.Models
{
    public class Header
    {
        public string Logo { get; set; }
        public string DownloadLogoCTA { get; set; }
        public string DownloadIconImg { get; set; }
        public List<string> Menu { get; set; }
    }

    public class Footer
    {

    }

    public class Home
    {

    }
}