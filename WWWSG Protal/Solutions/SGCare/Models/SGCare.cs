﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;

namespace SGCare.Models
{
    public class SGCareLandData
    {
        public string Title { get; set; }
        public WhatHappening WhatsHappening { get; set; }
        public List<Event> Events { get; set; }
        public List<Category> Categories { get; set; }
        public List<Story> Stories { get; set; }
        public List<Partner> Partners { get; set; }
        public List<Movement> Movements { get; set; }

        public Quote Quote { get; set; }
        public string DeskMastheadBanner { get; set; }
        public string MobMastheadBanner { get; set; }
        public string MasterHeadVideoUrl { get; set; }
        public string StoriesTitle { get; set; }
        public string StoriesDesc { get; set; }
        public string GetStartedCTA { get; set; }
        public string GetStartedCTAUrl { get; set; }
        public PartnerData PartnerData { get; set; }

        public string MeterTitle { get; set; }
        public string ActionPledgedText { get; set; }
        public string MeterDescription { get; set; }
        public string ActofKindnessText { get; set; }
        public string VolunteerforCauseText { get; set; }
        public string GroupUpActivityText { get; set; }
        public string InvolvedMastheadBanner { get; set; }
        public string InvolvedMasterHeadVideoUrl { get; set; }
        public string PledgenowImage { get; set; }

        public string ActofKindnessPledgeimage { get; set; }
        public string VolunteerforCausePledgeImage { get; set; }
        public string GroupUpActivityPledgeImage { get; set; }
        public int PledgeCount { get; set; }
    }

    /* Header Content */
    public class HeadContent
    {
        public string Logo { get; set; }
        public string ProgramesMenu { get; set; }
        public string StoriesMenu { get; set; }

        public string Kindnessmenu { get; set; }
        public string ActivityMenu { get; set; }

        public string ThirdMenuText { get; set; }
        public string CorporatePartnerShip { get; set; }
        public string LogoAndGuidelinesText { get; set; }
        public string GuidelinesFile { get; set; }
        public string ShareText { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        public SocialShare SocData { get; set; }
        public Item ConItem { get; set; }
    }

    public class MetaContent
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        public Item ConItem { get; set; }

        public string FBAppId { get; set; }
        public string HostName { get; set; }

        public string OGTitle { get; set; }
        public string OGDec { get; set; }
        public string OGImage { get; set; }
    }

    /* Footer Content */
    public class FooterContent
    {
        public string FooterLogo { get; set; }
        public string SocialMediaContent { get; set; }
        public string Copyrights { get; set; }

        public string StickyTitle { get; set; }
        public string StickyDesc { get; set; }
        public string PledgeNowImage { get; set; }
        public ConfigDetails ConfigDetails { get; set; }
    }

    /* Events */
    public class Event
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Location { get; set; }
        public string Url { get; set; }
    }

    /* What Happening */
    public class WhatHappening
    {
        public string Title { get; set; }
        public string ViewmoreCTA { get; set; }
        public string ViewmoreCTAUrl { get; set; }
    }

    /* Programme */
    public class Programme : ItemDetails
    {
        public string Description { get; set; }
        public string FindOutMoreCTA { get; set; }
        public string FindOutMoreCTAUrl { get; set; }
        public string VideoUrl { get; set; }
        public string RelatedStory { get; set; }
        public string StoryCat { get; set; }
        public List<Event> Events { get; set; }
        public string ProgramCategory { get; set; }
        public string Type { get; set; }
        public string Time { get; set; }
        public string StoryName { get; set; }

    }

    /* List of Programmes */
    public class Programmes
    {
        public Item Programme { get; set; }
        public Item ThumbnailImage { get; set; }
        public string ListDeskImg { get; set; }
        public string ListMobImg { get; set; }
        public string HelpText { get; set; }
        public List<Programme> ListProgrammes { get; set; }
        public List<Category> ListCategories { get; set; }
        public List<Movement> ListMovements { get; set; }
        public int PageCount { get; set; }
        public string QueryString { get; set; }
        public string AllCategories { get; set; }
        public List<Time> ListTime { get; set; }
        public List<Type> ListType { get; set; }
        public List<Story> Stories { get; set; }
        public string Logo1 { get; set; }
        public string Logo2 { get; set; }
        public string CTAUrl1 { get; set; }
        public string CTAUrl2 { get; set; }
    }

    public class Stories
    {
        public Item Story { get; set; }
        public string ThumbNailImage { get; set; }
        public string ListDeskImg { get; set; }
        public string ListMobImg { get; set; }
        public List<Story> ListStories { get; set; }
        public List<Category> ListCategories { get; set; }
        public int PageCount { get; set; }
        public string QueryString { get; set; }
        public string AllCategories { get; set; }

    }

    /* Story Details */
    public class Story : ItemDetails
    {
        public string TopContent { get; set; }
        public string BottomContent { get; set; }
        public string VideoUrl { get; set; }
        public List<Carousel> CarouseItems { get; set; }

        public string GetStartedCTA { get; set; }
        public string GetStartedUrl { get; set; }
    }

    public class Category
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string CategoryIcon { get; set; }
        public string ShortDesc { get; set; }
        public string CTA { get; set; }
        public string VideoUrl { get; set; }
        public string CatImage { get; set; }
        public Item StoriesItem { get; set; }
        public Item CatItem { get; set; }
        public string GetStartedCTA { get; set; }
        public string GetStartedUrl { get; set; }
        public string Url { get; set; }
    }

    public class Carousel
    {
        public string ItemImage { get; set; }
        public string Url { get; set; }
        public string ItemType { get; set; }
    }

    /*Checkbox list*/
    public class Time
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public Item TimeItem { get; set; }
    }
    public class Type
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public Item TypeItem { get; set; }
    }

    /* Item Details */
    public class ItemDetails
    {
        public string ItemPath { get; set; }
        public string ThumbnailImage { get; set; }
        public string DeskThumbImage { get; set; }
        public string MobThumbImage { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string HelpContent { get; set; }
        public string CategoryIconPath { get; set; }
        public string ShortDesc { get; set; }
        public string CTA { get; set; }
        public string SocShareTitle { get; set; }
        public string SocShareDesc { get; set; }
        public string SocShareImage { get; set; }
        public string FindOutHowUrl { get; set; }
        public string FBAppId { get; set; }
        public string HostName { get; set; }
        public int PageCount { get; set; }
        public string Metatags { get; set; }
        public Item Item { get; set; }
        public string ItemName { get; set; }

    }

    public class Partner
    {
        public string Image { get; set; }
        public string PartnerUrl { get; set; }
    }

    public class Quote
    {
        public string QuoteImg1 { get; set; }
        public string QuoteImg2 { get; set; }
    }

    public class Movement
    {
        public string ThumbImage { get; set; }
        public string URL { get; set; }
    }

    public class PartnerData
    {
        public string MovementTitle { get; set; }
        public string ExpandText { get; set; }
        public string PartnerTitle { get; set; }
    }

    /* Newly Created by Suryam */
    public class ActofKindnessDTO
    {
        public string DeskMastheadImg { get; set; }
        public string MobMastHeadImg { get; set; }
        public string IntroImg { get; set; }
        public List<ActofKindnessItem> LandingTiles { get; set; }
        public List<ActofKindnessItem> AllExceptTiles { get; set; }
        public int ItemCount { get; set; }
        public Item Item { get; set; }
        public string CTAUrl { get; set; }

    }

    public class ActofKindnessItem
    {
        public string DeskBanner { get; set; }
        public string MobileBanner { get; set; }
        public string Author { get; set; }
        public string TopDesc { get; set; }
        public List<Carousel> CarouselItems { get; set; }
        public string BottomDesc { get; set; }

        public string Title { get; set; }
        public string ShortDesc { get; set; }
        public string TileforBox1_2 { get; set; }
        public string TileforBox3 { get; set; }
        public string Readmore { get; set; }
        public List<ActofKindnessItem> RelatedStories { get; set; }
        public SocialShare SocData { get; set; }

        public Item Item { get; set; }
    }

    public class SocialShare
    {
        public string SocTitle { get; set; }
        public string SocDesc { get; set; }
        public string SocImg { get; set; }
       
    }

    public class ConfigDetails
    {
        public string HostName { get; set; }
        public string FBAppId { get; set; }
    }

    public class GroundUpActivity : ActofKindnessDTO
    {

    }
}
