﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public class CommonMethods
    {
        string[] replacedByFB = { "'", "\"" }, replacedWithCharsFB = { "\\'", "%22" };
        string[] replacedByTwitter = { "\"", "%", "#", ";" }, replacedWithCharsTwitter = { "%22", "%25", "%23", "%3B" };

        public const string faceBook = "FaceBook", tweet = "Twitter";

        public string BuildString(string value, string type)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (type == "FaceBook")
                    return ModifyString(value, type);
                //else if (type.Equals("Twitter"))
                //    return ModifyString(value, type);
                else return string.Empty;
            }
            else return string.Empty;
        }

        string ModifyString(string value, string type)
        {
            if (type == faceBook)
            {
                for (int index = 0; index < replacedByFB.Length; index++)
                {
                    value = value.Replace(replacedByFB[index], replacedWithCharsFB[index]);
                }
            }
            //else if (type == tweet)
            //{
            //    for (int index = 0; index < replacedByTwitter.Length; index++)
            //    {
            //        value = value.Replace(replacedByTwitter[index], replacedWithCharsTwitter[index]);
            //    }
            //}
            return value;
        }
    }
}
