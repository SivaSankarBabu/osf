﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using sgfuture.SGFuture_Revamp.sublayouts;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public partial class AboutUsSublayout : System.Web.UI.UserControl
    {
        private static Database web = Database.GetDatabase("web");
        Item itemContext = Sitecore.Context.Item;

        Item GaleryItem = web.GetItem("{857ADABC-8335-415B-B452-7DE605A81654}");
        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
              //  Logger.WriteLine("About Us Form is Started");
                BindTable("{857ADABC-8335-415B-B452-7DE605A81654}");
            }

        }

        string GetMediaImagePath(Item item, string colName)
        {

            return MediaManager.GetMediaUrl(((ImageField)item.Fields[colName]).MediaItem).ToString();
        }

        public string GetImagePath(Item item, string colName, string imagePathOrAlt)
        {
            try
            {
                if (!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Image")
                {
                    ImageField img = (ImageField)item.Fields[colName];
                    if (img != null)
                    {
                        MediaItem mItem = img.MediaItem;
                        if (mItem != null)
                            return Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                        else { return string.Empty; }
                    }
                    else { return string.Empty; }
                }
                else if ((!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Alt"))
                    return ((ImageField)item.Fields[colName]).Alt.ToString();
                else { return string.Empty; }
            }
            catch (Exception exception)
            {
              //  Logger.WriteException(exception, "Exception Occurred " + exception.InnerException);
                throw exception;
            }
        }

        public void BindTable(string itemId)
        {
            try
            {
                DataTable dtTabs = new DataTable();
                dtTabs.Columns.AddRange(new DataColumn[11] { new DataColumn("Categoryname", typeof(string)), new DataColumn("Categoryimage", typeof(string)), new DataColumn("TopDescription", typeof(string)), new DataColumn("GalleryImage", typeof(string)), new DataColumn("Bottomdescription", typeof(string)), new DataColumn("Quotation", typeof(string)), new DataColumn("Id", typeof(string)), new DataColumn("tabid", typeof(string)), new DataColumn("Count", typeof(string)), new DataColumn("GalleryStatus", typeof(bool)), new DataColumn("QuoteStatus", typeof(bool)) });
                DataRow drTabs;
                List<Item> TabItems = web.GetItem(itemId).GetChildren().ToList();
                if (TabItems != null && TabItems.Count > 0)
                {
                    int tabid = 1;
                    foreach (Item item in TabItems)
                    {
                        //tab-1

                        drTabs = dtTabs.NewRow();
                        drTabs["tabid"] = "tab-" + tabid.ToString();
                        drTabs["Count"] = "0" + tabid.ToString();
                        drTabs["Categoryname"] = item.Fields["Category Name"].ToString();
                        drTabs["Categoryimage"] = GetImagePath(item, "Category Image", "Image");
                        drTabs["TopDescription"] = item.Fields["Top Description"].ToString();

                        StringBuilder sbGallery = new StringBuilder();
                        int GalleryCount = 0;
                        if (item.Fields["Gallery"] != null && !item.Fields["Gallery"].Equals(""))
                        {
                            Sitecore.Data.Fields.MultilistField mlGallery = item.Fields["Gallery"];
                            if (mlGallery != null)
                            {

                                foreach (ID id in mlGallery.TargetIDs)
                                {
                                    MediaItem itemTab = Sitecore.Context.Database.GetItem(id.ToString());
                                    if (itemTab != null && GalleryCount < 5)
                                    {
                                        sbGallery.Append("<li class='item'> <figure class='wrap-thumb'>");
                                        sbGallery.Append("<img class='thumb' src='" + MediaManager.GetMediaUrl(itemTab) + "'></figure>");
                                        sbGallery.Append("<figcaption class='thumb-desc'> " + itemTab.Alt + " </figcaption></li>");
                                        GalleryCount++;
                                    }
                                }
                            }
                        }
                        if (GalleryCount == 0)

                            drTabs["GalleryStatus"] = false;
                        else
                            drTabs["GalleryStatus"] = true;

                        drTabs["GalleryImage"] = sbGallery.ToString();
                        List<Item> quoteItems = web.GetItem(item.ID).Axes.GetDescendants().OrderByDescending(a => a.Statistics.Created).Take(2).ToList();

                        StringBuilder sbQuotes = new StringBuilder();
                        int quotecount = 0;
                        if (quoteItems != null && quoteItems.Count > 0)
                        {
                            if (quoteItems != null)
                            {
                                foreach (Item quote in quoteItems)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(quote.Fields["Name"])) && !string.IsNullOrEmpty(Convert.ToString(quote.Fields["Designation"])) && !string.IsNullOrEmpty(Convert.ToString(quote.Fields["Quote"])))
                                    {
                                        sbQuotes.Append("<li class='item-quote'><a class='item-link' href='#'><figure class='avatar'>");
                                        if (!string.IsNullOrEmpty(GetImagePath(quote, "Quote Image", "Image")))
                                        {
                                            sbQuotes.Append("<img class='thumb' src='" + GetImagePath(quote, "Quote Image", "Image") + "'></figure>");
                                        }
                                        else
                                        {
                                            sbQuotes.Append("<img class='thumb' src='/SGFuture Revamp/assets/images/dummy/avatar-default.jpg'></figure>");
                                        }
                                        sbQuotes.Append("<figcaption class='info'>");
                                        sbQuotes.Append("<span class='author-name'>" + quote.Fields["Name"] + "</span>");
                                        sbQuotes.Append("<span class='author-title'>" + quote.Fields["Designation"] + "</span>");
                                        sbQuotes.Append("<p class='quote'>" + quote.Fields["Quote"] + "</p>");
                                        sbQuotes.Append("</figcaption></a></li>");
                                        quotecount++;
                                    }
                                    else if (!string.IsNullOrEmpty(Convert.ToString(quote.Fields["Name"])) || !string.IsNullOrEmpty(Convert.ToString(quote.Fields["Designation"])) || !string.IsNullOrEmpty(Convert.ToString(quote.Fields["Quote"])))
                                    {
                                        sbQuotes.Append("<li class='item-quote'><a class='item-link' href='#'><figure class='avatar'>");
                                        if (!string.IsNullOrEmpty(GetImagePath(quote, "Quote Image", "Image")))
                                        {
                                            sbQuotes.Append("<img class='thumb' src='" + GetImagePath(quote, "Quote Image", "Image") + "'></figure>");
                                        }
                                        else
                                        {
                                            sbQuotes.Append("<img class='thumb' src='/SGFuture Revamp/assets/images/dummy/avatar-default.jpg'></figure>");
                                        }
                                        sbQuotes.Append("<figcaption class='info'>");
                                        sbQuotes.Append("<span class='author-name'>" + quote.Fields["Name"] + "</span>");
                                        sbQuotes.Append("<span class='author-title'>" + quote.Fields["Designation"] + "</span>");
                                        sbQuotes.Append("<p class='quote'>" + quote.Fields["Quote"] + "</p>");
                                        sbQuotes.Append("</figcaption></a></li>");
                                        quotecount++;

                                    }
                                }

                            }
                        }


                        if (quotecount == 0)

                            drTabs["QuoteStatus"] = false;
                        else
                            drTabs["QuoteStatus"] = true;

                        drTabs["Quotation"] = sbQuotes.ToString();

                        drTabs["Bottomdescription"] = item.Fields["Bottom Description"].ToString();
                        dtTabs.Rows.Add(drTabs);
                        tabid++;
                    }
                    repSection.DataSource = repTabs.DataSource = dtTabs;
                    repSection.DataBind();
                    repTabs.DataBind();

                }
            }
            catch (Exception ex)
            {
                //Logger.WriteException(ex, "Exception Occurred " + ex.InnerException);

            }
        }
    }
}