﻿using System;
using System.Data;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Collections;
using Sitecore.SecurityModel;
using System.Linq;
using Sitecore.Links;
using System.Collections.Generic;
using SDF = Sitecore.Data.Fields;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Resources.Media;
using Sitecore.Data.Fields;
using sgfuture.SGFuture_Revamp.sublayouts;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public partial class PastEngagementsSublayout : System.Web.UI.UserControl
    {
        public static string ProjectshowcaseItemId = "{0E35F486-1BDC-4586-87FD-14413610B816}";
        public static Database webDb = Database.GetDatabase("web");
        Item itemContext = Sitecore.Context.Item;
        static int strLnkLast = 0;
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // Logger.WriteLine("PastEngagement Form is started");
                BindProjectShowcaseList();
            }
        }
        void BindProjectShowcaseList()
        {
            try
            {
                DataTable dtProjects = new DataTable();
                dtProjects.Columns.AddRange(new DataColumn[6] { new DataColumn("href", typeof(string)), new DataColumn("ProjectTitle", typeof(string)), new DataColumn("ProjectShortDescription", typeof(string)), new DataColumn("ListThumbImage", typeof(string)), new DataColumn("Date", typeof(string)), new DataColumn("ReadMore", typeof(string)) });

                using (new SecurityDisabler())
                {
                    List<Item> ProjectShowcase = new List<Item>();

                    ProjectShowcase = (from t in webDb.GetItem(ProjectshowcaseItemId).GetChildren().OrderByDescending(c => new DateField(c.Fields["Project date"]).DateTime) select t).ToList();

                    if (ProjectShowcase != null && ProjectShowcase.Count > 0)
                    {
                        foreach (Item item in ProjectShowcase)
                        {
                            DataRow drProjects = dtProjects.NewRow();
                            drProjects["Date"] = !string.IsNullOrEmpty(Convert.ToString(item.Fields["Project date"])) ? (new DateField(item.Fields["Project date"]).DateTime.ToString("dd MMM yyyy")) : string.Empty;
                            drProjects["href"] = Convert.ToString(LinkManager.GetItemUrl(item));
                            drProjects["ProjectTitle"] = Convert.ToString(item.Fields["Project Title"]);
                            drProjects["ProjectShortDescription"] = Convert.ToString(item.Fields["Project Short Description"]);
                            drProjects["ListThumbImage"] = GetImagePath(item, "Thumb Image", "Image");
                            drProjects["ReadMore"] = Convert.ToString(item.Fields["Read More Button Text"]);
                            dtProjects.Rows.Add(drProjects);

                        }
                    }
                }
                PagedDataSource pgsource = new PagedDataSource();
                pgsource.DataSource = dtProjects.DefaultView;
                pgsource.AllowPaging = true;
                pgsource.PageSize = 8;
                pgsource.CurrentPageIndex = PageNumber;
                if (PageNumber == 0)
                {
                    lnkbPrevious.Visible = lnkbFirst.Visible = false;
                }
                else
                {
                    lnkbPrevious.Visible = lnkbFirst.Visible = true;
                }

                if (PageNumber == pgsource.PageCount - 1)
                {
                    lnkbNext.Visible = lnkbLast.Visible = false;
                }
                else
                {
                    lnkbNext.Visible = lnkbLast.Visible = true;
                }

                DataTable dtPS = new DataTable();
                dtPS.Columns.Add("ID", typeof(string));
                dtPS.Columns.Add("Enable", typeof(Boolean));
                dtPS.Columns.Add("class", typeof(string));

                if (pgsource.PageCount > 1)
                {
                    DataRow drPS;
                    rptPaging.Visible = true;
                    System.Collections.ArrayList pages = new System.Collections.ArrayList();
                    strLnkLast = pgsource.PageCount - 1;
                    for (int i = 0; i <= pgsource.PageCount - 1; i++)
                    {
                        drPS = dtPS.NewRow();
                        drPS["ID"] = (i + 1).ToString();
                        if (pgsource.CurrentPageIndex + 1 == i + 1)
                        {
                            drPS["Enable"] = false;
                            drPS["class"] = "pager active";
                        }
                        else
                        {
                            drPS["Enable"] = true;
                            drPS["class"] = "pager";
                        }
                        dtPS.Rows.Add(drPS);
                    }

                    rptPaging.DataSource = dtPS;
                    rptPaging.DataBind();
                }
                else
                {
                    rptPaging.Visible = false;
                    if (pgsource.PageCount <= 1)
                    {
                        lnkbNext.Visible = lnkbLast.Visible = lnkbFirst.Visible = lnkbPrevious.Visible = false;
                    }
                }
                if (PageNumber == 0)
                {
                    lnkbFirst.Visible = lnkbPrevious.Visible = false;
                }
                else
                {
                    lnkbFirst.Visible = lnkbPrevious.Visible = true;
                }

                rptProjectShowcase.DataSource = pgsource;
                rptProjectShowcase.DataBind();
            }
            catch (Exception ex)
            {
               // Logger.WriteException(ex, "Exception Occurred " + ex.InnerException);

            }
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return System.Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set { ViewState["PageNumber"] = value; }
        }

        /* Getting Image Path or Alt Text based on Item and Field Name */
        string GetImagePath(Item item, string colName, string imagePathOrAlt)
        {
            try
            {
                if (!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Image")
                {
                    SDF.ImageField img = (SDF.ImageField)item.Fields[colName];
                    if (img != null)
                    {
                        MediaItem mItem = img.MediaItem;
                        if (mItem != null)
                            return Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                        else { return string.Empty; }
                    }
                    else { return string.Empty; }
                }

                else if ((!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Alt"))
                    return ((Sitecore.Data.Fields.ImageField)item.Fields[colName]).Alt.ToString();
                else { return string.Empty; }
            }
            catch (Exception exception)
            {
              //  Logger.WriteException(exception, "Exception Occurred " + exception.InnerException);
                throw exception;
            }
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = System.Convert.ToInt32(e.CommandArgument) - 1;
            BindProjectShowcaseList();
        }

        protected void lnkbFirst_Click(object sender, EventArgs e)
        {
            //Logger.WriteLine("First button is clicked");
            PageNumber = 0;
            BindProjectShowcaseList();
        }

        protected void lnkbPrevious_Click(object sender, EventArgs e)
        {
            if (PageNumber != 0)
            {
                //Logger.WriteLine("Previous button is clicked");
                PageNumber = PageNumber - 1;
                BindProjectShowcaseList();
            }
        }

        protected void lnkbNext_Click(object sender, EventArgs e)
        {
           // Logger.WriteLine("Next button is clicked");
            PageNumber = PageNumber + 1;
            BindProjectShowcaseList();
        }

        protected void lnkbLast_Click(object sender, EventArgs e)
        {
            //Logger.WriteLine("LastLink button is clicked");
            PageNumber = strLnkLast;// ((Convert.ToInt32(ViewState["TotalPages"]) / 1) - 1);
            BindProjectShowcaseList();
        }
    }
}