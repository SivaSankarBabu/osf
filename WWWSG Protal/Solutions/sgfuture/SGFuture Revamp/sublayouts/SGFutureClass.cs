﻿using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sitecore.Resources.Media;
using Sitecore.Collections;
using sgfuture.SGFuture_Revamp.sublayouts;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public class SGFutureClass
    {
        public static Database webDb = Database.GetDatabase("web");
        public static string projectshowcaseItemId = "{7F755F74-00C9-4604-8DA2-248ACFA90967}";
        Item itemContext = Sitecore.Context.Item;

        /* Getting Image Path or Alt Text based on Item and Field Name */
        public string GetImagePath(Item item, string colName, string imagePathOrAlt)
        {
            try
            {
                if (!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Image")
                {
                    ImageField img = (ImageField)item.Fields[colName];
                    if (img != null)
                    {
                        MediaItem mItem = img.MediaItem;
                        if (mItem != null)
                            return Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                        else { return string.Empty; }
                    }
                    else { return string.Empty; }
                }

                else if ((!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Alt"))
                    return ((ImageField)item.Fields[colName]).Alt.ToString();
                else { return string.Empty; }
            }
            catch (Exception exception)
            {
                //Logger.WriteException(exception, "Exception Occurred " + exception.InnerException);
                throw exception;
            }
        }

        /* Getting Landing tiles data in fo Data table */
        public DataTable GetLandingTiles()
        {
            try
            {
                DataTable dtTiles = new DataTable();
                dtTiles.Columns.AddRange(new DataColumn[16] { new DataColumn("Name", typeof(string)), new DataColumn("Title", typeof(string)), new DataColumn("Event Date", typeof(string)), new DataColumn("Thumb Image", typeof(string)), new DataColumn("Banneroverlay Text", typeof(string)), new DataColumn("Banner Alt Text", typeof(string)), new DataColumn("Short Description", typeof(string)), new DataColumn("Top Desc", typeof(string)), new DataColumn("Bottom Desc", typeof(string)), new DataColumn("Facebook Title", typeof(string)), new DataColumn("Facebook Content", typeof(string)), new DataColumn("Facebook Image", typeof(string)), new DataColumn("Item Id", typeof(string)), new DataColumn("Gallery", typeof(List<GalleryItem>)), new DataColumn("Item Url", typeof(string)), new DataColumn("Read More", typeof(string)) });
                List<Item> items = webDb.GetItem("{7F755F74-00C9-4604-8DA2-248ACFA90967}").GetChildren().OrderByDescending(x => x.Statistics.Created).ToList();
                foreach (Item eachItem in items)
                {
                    DataRow tileNewrow = dtTiles.NewRow();
                    BuidDataTable(tileNewrow, eachItem);
                    dtTiles.Rows.Add(tileNewrow);
                }
                return dtTiles;
            }
            catch (Exception msg)
            {
               // Logger.WriteException(msg, "Exception Occurred " + msg.InnerException);
                throw msg;
            }
        }

        /* Common Method for Build Data table */
        public void BuidDataTable(DataRow drNewRow, Item item)
        {
            try
            {
                if (item != null)
                {
                    drNewRow["Name"] = item.Name.ToString();
                    drNewRow["Title"] = Convert.ToString(item.Fields["Project Title"]);
                    drNewRow["Event Date"] = Convert.ToString(item.Fields["Project Number"]);
                    drNewRow["Thumb Image"] = GetImagePath(item, "Thumb Image", "Image");
                    drNewRow["Banneroverlay Text"] = Convert.ToString(item.Fields["Banner Overlay Text"]);
                    drNewRow["Short Description"] = Convert.ToString(item.Fields["Project Short Description"]);
                    drNewRow["Top Desc"] = Convert.ToString(item.Fields["Top Description"]);
                    drNewRow["Bottom Desc"] = Convert.ToString(item.Fields["Bottom Description"]);
                    drNewRow["Banner Alt Text"] = GetImagePath(item, "Thumb Image", "Alt");
                    drNewRow["Facebook Title"] = Convert.ToString(item.Fields["Social Share Title"]);
                    drNewRow["Facebook Content"] = Convert.ToString(item.Fields["Social Share Content"]);
                    drNewRow["Facebook Image"] = Convert.ToString(item.Fields["Social Share Image"]);
                    drNewRow["Item Id"] = item.ID.ToString();
                    drNewRow["Item Url"] = LinkManager.GetItemUrl(item);
                    drNewRow["Read More"] = Convert.ToString(item.Fields["Read More Button Text"]);

                }
            }
            catch (Exception msg)
            {
                //Logger.WriteException(msg, "Exception Occurred " + msg.InnerException);
                //throw msg;
            }
        }

        public class GalleryItem
        {
            public string BannerImage { get; set; }
            public string BannerCaption { get; set; }
        }

        public string GetItemLink(Item item, string colName)
        {
            if (item != null)
            {
                LinkField lnkField = item.Fields[colName];
                if (lnkField.LinkType == "external" || lnkField.LinkType == "anchor" || lnkField.LinkType == "mailto" || lnkField.LinkType == "javascript")
                    return lnkField.Url;
                if (lnkField.LinkType == "internal")
                    return lnkField.TargetItem != null ? LinkManager.GetItemUrl(lnkField.TargetItem) : string.Empty;
                else if (lnkField.LinkType == "media")
                    return Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(lnkField.TargetItem));
                else { return string.Empty; }
            }
            else { return string.Empty; }
        }
    }
}