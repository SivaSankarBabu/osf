﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlipBookSublayout.ascx.cs" Inherits="sgfuture.SGFuture_Revamp.sublayouts.FlipBookSublayout" %>
<asp:HiddenField ID="hdnFlipbookUrl" runat="server" ClientIDMode="Static" />

<header id="page-header">

    <div class="wrap-top">
        <div class="wrap-left pull-left">
            <a href="javascript:void(0);" class="icon-nav">
                <span class="icon icon-line"></span>
                <span class="icon icon-line"></span>
                <span class="icon icon-line"></span>
            </a>

            <a href="/SGFuture" class="logo">
                <sc:Image ID="imgLogo" runat="server" Field="Header Logo" CssClass="thumb" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
            </a>
        </div>


        <div id="divDownloadTextShow" runat="server" class="wrap-right pull-right hidden-xs">
            <a href="/SGFuture/FlipBook" target="_blank" id="DownloadFile" runat="server" class="button-download">
                <span class="download-text">
                    <sc:Text ID="txtDownloadText" runat="server" Field="Download SGFuture Report text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
                <span class="icon icon-download">download</span>
            </a>
            <asp:HiddenField ID="hdnDownload" runat="server" />
        </div>
        <asp:HiddenField ID="hdnDownloadbtnimage" runat="server" />
    </div>
    <div class="wrap-nav">
        <nav class="text-center">

            <%if (!String.IsNullOrEmpty(new Sitecore.Data.Database("web").GetItem("{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}").Fields["Home Text for mobile"].ToString()))
              { %>
            <a href="/SGFuture" class="nav-item visible-xs first">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="txtHome" runat="server" Field="Home Text for mobile" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a><%} %>

            <%if (!String.IsNullOrEmpty(new Sitecore.Data.Database("web").GetItem("{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}").Fields["About SGFuture Text"].ToString()))
              { %>
            <a href="/SGFuture/About-SGFuture" class="nav-item">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="txtAbout" runat="server" Field="About SGFuture Text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a><%} %>

            <%if (!String.IsNullOrEmpty(new Sitecore.Data.Database("web").GetItem("{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}").Fields["Project Showcase Text"].ToString()))
              { %>
            <a href="/SGFuture/Project-Showcase" class="nav-item" id="PjectShowcase">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="Text1" runat="server" Field="Project Showcase Text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a><%} %>

            <%if (!String.IsNullOrEmpty(new Sitecore.Data.Database("web").GetItem("{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}").Fields["Past Engagements Text"].ToString()))
              { %>
            <a href="/SGFuture/Past-Engagements" class="nav-item last" id="PstEngagement">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="Text2" runat="server" Field="Past Engagements Text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a><%} %>

            <a id="DownloadFileMenu" target="_blank" runat="server" class="nav-item visible-xs" href="/SGFuture/FlipBook">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="txtDownload" runat="server" Field="Download SGFuture Report text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a>
        </nav>
    </div>
    <div class="nav-fade"></div>
</header>
<main class="page page-about-us">
			<div class="container-fluid">
				<!-- begin section hero -->
				<section class="section-hero text-center">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-push-1">
								<h2 class="title">
									<sup class="number">04</sup>
									<span class="text"><sc:Text ID="txtAboutTitle" runat="server" Field="Title Text" /> </span>
								</h2>
                                <p class="desc">
                                    <sc:Text ID="Text3" runat="server" Field="Description Text" />
								</p>
							</div>
						</div>
					</div>
				</section>
                <div class="iframe-cont">
    <iframe id="flipbook-file" width="100%" height="600px" src="javascript:void(0)" frameborder="0" allowfullscreen></iframe>
</div>
                	</div>

    </main>

<div id="nocontent" style="display: none">
    <div class="nocontent-cell">
        <p>There is no content to Display</p>
    </div>
</div>
<script type="text/javascript">
    $(window).load(function () {
        var path = $('#header_0_hdnDownloadbtnimage').val();
        $('.icon.icon-download').css({ 'background': 'url("' + path + '") center center no-repeat', 'background-size': ' cover' });
    })

    var i = 0;
    $('.sup').each(function () {
        $(this).text('0' + i);
        i++;
    });

    $(".wrap-nav > .text-center a").filter(function () {
        return this.href == location.href.replace(/#.*/, "");
    }).addClass("active")
</script>
<script>



    if ($('#hdnFlipbookUrl').val() != '') {

        $('#flipbook-file').attr('src', $('#hdnFlipbookUrl').val());
        // $('#flipbook-file').attr('src', 'https://www.sg+');

    }
    else {
        $('#nocontent').show();
        $('.iframe-cont').remove();


    }

</script>
<style>
    #nocontent
    {
        display: table;
        width: 100%;
    }

        #nocontent > .nocontent-cell
        {
            display: table-cell;
            width: 100%;
            height: 600px;
            text-align: center;
            vertical-align: middle;
        }

            #nocontent > .nocontent-cell p
            {
                color: #fff;
                font-size: 16px;
            }
</style>