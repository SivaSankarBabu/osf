﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutUsSublayout.ascx.cs" Inherits="sgfuture.SGFuture_Revamp.sublayouts.AboutUsSublayout" %>
<main class="page page-about-us">
			<div class="container-fluid">
				<!-- begin section hero -->
				<section class="section-hero text-center">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-push-1">
								<h2 class="title">
									<sup class="number">01</sup>
									<span class="text"><sc:Text ID="txtAboutTitle" runat="server" Field="About SGFuture Title Text" /></span>
								</h2>
                                <p class="desc">
								<sc:Text ID="txt" runat="server" Field="About SGFuture Description Text" /></p>
							</div>
						</div>
					</div>
				</section>
				<!-- end section hero -->
				<!-- begin section main -->
				<section class="section-main">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-push-1">
								<div class="tabs-top">
									<ul class="list-tabs-top">
                                        <asp:Repeater ID="repTabs" runat="server">
                                            <ItemTemplate>
                                                <li>
                                                    <a class="item-tab" href="#<%#Eval("tabid") %>">
                                                        <span class="inner">
                                                            <sup class="number"><%#DataBinder.Eval(Container.DataItem,"Count") %></sup>
                                                            <span class="text"><%#DataBinder.Eval(Container.DataItem,"Categoryname")%>
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
									</ul>
									<a class="icon-tab-nav visible-xs" href="javascript:void(0);"></a>
								</div>
								<div class="bg-fade"></div>
							</div>
						</div>
					</div>
					<div class="tabs-wrapper text-center">
                        <asp:Repeater ID="repSection" runat="server">
                            <ItemTemplate>
                        <section id='<%#Eval("tabid") %>' class="tab-content">
							<!-- begin top content -->
							<div class="container">
								<div class="row">

									<div class="col-xs-12 col-sm-10 col-sm-push-1">
										<h3 class="title">
											<img class="thumb" src="<%#DataBinder.Eval(Container.DataItem,"Categoryimage") %>" />
											<br/>
											<span class="text"><%#DataBinder.Eval(Container.DataItem,"Categoryname") %></span>
										</h3>
										<div class="content">
											<%#DataBinder.Eval(Container.DataItem,"TopDescription") %>
										</div>
									</div>
								</div>
							</div>
							<!-- end top content -->
							<!-- begin article thumb slider -->
							<div class="block-article-thumb" runat="server" id="divQuote" visible='<%#Eval("GalleryStatus")%>'>
								<div class="wrap-content">
									<ul class="list-article-thumb" id="Slideshow" runat="server">	
                                        <%#DataBinder.Eval(Container.DataItem,"GalleryImage") %>							
							</ul>
								</div>
							</div>
							<!-- end article thumb slider -->
							<!-- begin section quotes -->
							<section class="section-quote">
								<!-- begin block quotes -->                              
								<div class="block-quotes" id="divblockquotes" runat="server" visible='<%#Eval("QuoteStatus")%>'>
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1">
												<div id="Div1" class="wrap-quote" runat="server">
													<ul class="list-quote">
                                                        <%#DataBinder.Eval(Container.DataItem,"Quotation") %>														
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
                                       
								<!-- end block quotes -->
							</section>
							<!-- end section quotes -->
							<!-- begin section info -->
							<section class="section-info text-center">
								<div class="container">
									<div class="row">
										<div class="col-xs-12 col-sm-10 col-sm-push-1">
                                            <%#DataBinder.Eval(Container.DataItem,"Bottomdescription") %>
										</div>
									</div>
								</div>
							</section>
							<!-- end section info -->
						</section>
                            </ItemTemplate>
                        </asp:Repeater>
						<!-- begin tab 1 content -->
					</div>
				</section>
				<!-- end section main -->

			</div>
    <a class="btn-back-top" href="javascrip:;"></a>

        <div id="divHiddenTab" style="display: none">
           <%-- <asp:Repeater ID="repQuotes" runat="server">
             <ItemTemplate>
                 <div>
                     <input type="hidden" class="QuoteName" value="<%#DataBinder.Eval(Container.DataItem,"QuoteName") %>" />
                     <input type="hidden" class="QuoteImage" value="<%#DataBinder.Eval(Container.DataItem,"QuoteImage") %>" />
                     <input type="hidden" class="QuoteDEsig" value="<%#DataBinder.Eval(Container.DataItem,"QuoteDesignation") %>" />
                     <div style="display:none" class="Topdesc" > 
                    
                 </div>
             </ItemTemplate>

            </asp:Repeater>--%>
        </div>
		</main>
<!-- main -->

<script type="text/javascript">
    $(function () {
        $(".tab-content:eq(0)").addClass("active");
        $('.list-tabs-top li:eq(0)').addClass("active");

        $('h3.title').each(function () {
            if ($(this).find('img').attr('src') == '') {
                $(this).find('img').hide();
            }
            else { $(this).find('img').show(); }
        });

        $('.inner').click(function () {
            var Title = $(this).find('.text').text();
            ga('send', 'event', {
                eventCategory: 'SGFuture Homepage',
                eventAction: 'Click',
                eventLabel: Title
            });
        });

        /*For Bottom text Empty*/
        $('.section-info').find('.col-xs-12').each(function () {
            if ($(this).html().trim() == '') {
                $(this).parents('.section-info').remove();
            }
            else { $(this).parents('.section-info').show(); }
        });

        if ($('.block-article-thumb').find('img').attr('src') == '') { $(this).hide(); }
        else { $(this).show(); }


        $(window).load(function () {
            if ($('.list-quote').length) {
                if ($('.list-quote').length > 0) {
                    $('.list-quote:visible').bxSlider();
                    if ($('#content_0_repSection_divblockquotes_0').find('li').length > 3) {
                        $('.block-quotes').find('.bx-pager-item').show();
                    }
                    else { $('#content_0_repSection_divblockquotes_0').find('.bx-pager-item').remove(); }
                }
                else { $('.block-quotes').hide(); }
            }
        });
    });
</script>
