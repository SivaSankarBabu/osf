﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using sgfuture.SGFuture_Revamp.sublayouts;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public partial class FlipBookSublayout : System.Web.UI.UserControl
    {
        private static Database web = Database.GetDatabase("web");
        Item itemContext = Sitecore.Context.Item;

        private void Page_Load(object sender, EventArgs e)
        {
            hdnFlipbookUrl.Value = Convert.ToString(itemContext.Fields["Iframe Url"]);
        }
    }
}