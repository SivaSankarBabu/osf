﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using System.IO;
using System.Web;
using sgfuture.SGFuture_Revamp.sublayouts;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public partial class HeaderSublayout : System.Web.UI.UserControl
    {
        public static Database WebDB = Database.GetDatabase("web");
        Item HeaderItem = WebDB.GetItem("{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}");
        Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

              //  hdnDownload.Value = ((FileField)HeaderItem.Fields["Download File"]).Src.ToString();
                GetDownloadIcon();
                ShowHideDownloadText(HeaderItem);
                Sitecore.Data.Fields.LinkField linkField = HeaderItem.Fields["Download File"];
                if (linkField != null && !string.IsNullOrEmpty(linkField.Url))
                {
                    if (linkField.IsMediaLink)
                    {
                        DownloadFile.HRef = DownloadFileMenu.HRef = "/" + "~/media/" + linkField.Url;
                    }
                    else if (linkField.IsInternal)
                        DownloadFile.HRef = DownloadFileMenu.HRef = linkField.Url;
                    else
                    {
                        
                        if (!string.IsNullOrEmpty(linkField.Url) && (linkField.Url.Contains("http://") || linkField.Url.Contains("https://")))
                        {
                           
                            DownloadFile.HRef = DownloadFileMenu.HRef = linkField.Url;
                        }
                        else
                        {
                            DownloadFile.Attributes.Remove("href");
                            DownloadFile.Style.Add("text-decoration", "none");
                        }
                    }
                }
                else
                {
                    DownloadFile.Attributes.Remove("href");
                    DownloadFile.Style.Add("text-decoration", "none");
                }


                Item itemsgfuture = web.GetItem("{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}");
                if (itemsgfuture != null)
                {
                    if (itemsgfuture.Fields["Home Text for mobile"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(itemsgfuture.Fields["Home Text for mobile"])))
                            aSgfuture.Visible = true;
                    }

                    if (itemsgfuture.Fields["About SGFuture Text"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(itemsgfuture.Fields["About SGFuture Text"])))
                            aAbout.Visible = true;
                    }

                    if (itemsgfuture.Fields["Project Showcase Text"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(itemsgfuture.Fields["Project Showcase Text"])))
                            PjectShowcase.Visible = true;
                    }

                    if (itemsgfuture.Fields["Past Engagements Text"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(itemsgfuture.Fields["Past Engagements Text"])))
                            PstEngagement.Visible = true;
                    }


                }



            }
        }

        private void GetDownloadIcon()
        {
            ImageField imgDownload = (ImageField)HeaderItem.Fields["Download Button Image"];
            if (imgDownload != null && imgDownload.MediaItem != null)
            {
                MediaItem mItem = imgDownload.MediaItem;
                if (mItem != null)
                {
                    hdnDownloadbtnimage.Value = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                }
            }
            else { hdnDownloadbtnimage.Value = ""; }
        }

        /* Download Logo and Guide */
        //protected void DownloadFile_ServerClick(object sender, EventArgs e)
        //{
        //    if (Page.IsValid)
        //    {
        //        //try
        //        //{



        //            //FileField file = HeaderItem.Fields["Download File"];

        //            //if (file != null)
        //            //{
        //            //    Response.Write(HeaderItem.Fields["Download File"]);
        //            //    Response.End();
        //            //    DownloadFileMenu.HRef = "/" + HeaderItem.Fields["Download File"];
        //            //    //MediaItem mi = file.MediaItem;//web.GetItem(file.ID);
        //            //    //if (mi != null)
        //            //    //{
        //            //    //    Stream stream = mi.GetMediaStream();
        //            //    //    if (stream != null)
        //            //    //    {
        //            //    //        long fileSize = stream.Length;
        //            //    //        byte[] buffer = new byte[(int)fileSize];
        //            //    //        stream.Read(buffer, 0, (int)stream.Length);
        //            //    //        stream.Close();

        //            //    //        Response.ContentType = String.Format(mi.MimeType);
        //            //    //        string fileName = (mi.Name + "." + mi.Extension).ToString().Replace(' ', '_');
        //            //    //        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
        //            //    //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            //    //        Response.BinaryWrite(buffer);
        //            //    //        Response.End();
        //            //    //    }
        //            //    //    else { Response.Write("No download available."); }
        //            //    //}
        //            //    //else { Response.Write("No download available"); }

        //            //}
        //        //}
        //        //catch (Exception ex) { throw ex; }
        //    }
        //}

        public void ShowHideDownloadText(Item item)
        {
            divDownloadTextShow.Visible = !string.IsNullOrEmpty(item.Fields["Download SGFuture Report text"].Value) ? true : false;
        }
    }
}