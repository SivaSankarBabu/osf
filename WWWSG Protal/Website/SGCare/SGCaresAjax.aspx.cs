﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGCare.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using System.Web.Services;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Publishing;
using System.Text.RegularExpressions;

public partial class SGCaresAjax : System.Web.UI.Page
{
    static Database web = Database.GetDatabase("web");
    static Database master = Database.GetDatabase("master");

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /* Get Programmes */
    [WebMethod]
    public static List<Programme> GetProgrames(string catName, string type, string time, string skipNumber)
    {
        try
        {

            if (!string.IsNullOrEmpty(Constants.programmesItemId) && !string.IsNullOrEmpty(skipNumber))
            {
                List<Item> items = null; int itemCount = 0;
                items = web.GetItem(Constants.programmesItemId).GetChildren().ToList();

                if (!string.IsNullOrEmpty(catName))
                {
                    items = items.Where(x => ((LookupField)x.Fields["Programme Filter"]).TargetItem != null && catName.Contains(((LookupField)x.Fields["Programme Filter"]).TargetItem.Name) == true).ToList();
                }
                if (!string.IsNullOrEmpty(type))
                {
                    items = items.Where(x => ((LookupField)x.Fields["Type"]).TargetItem != null && type.Contains(((LookupField)x.Fields["Type"]).TargetItem.Name) == true).ToList();
                }
                if (!string.IsNullOrEmpty(time))
                {
                    items = items.Where(x => ((LookupField)x.Fields["Time"]).TargetItem != null && time.Contains(((LookupField)x.Fields["Time"]).TargetItem.Name) == true).ToList();
                }

                itemCount = items.Count;
                items = items.Skip(Convert.ToInt16(skipNumber)).Take(6).ToList();


                return items.Select(item => new Programme
                {
                    ThumbnailImage = CommonFun.GetImage(item, "Thumbnail Image"),
                    Title = Convert.ToString(item.Fields["Name of Programme"]),
                    ShortDesc = Convert.ToString(item.Fields["Short Description"]),
                    CategoryIconPath = CommonFun.GetImage(((LookupField)item.Fields["Programme Filter"]).TargetItem, "Icon"),
                    CTA = Convert.ToString(item.Fields["CTA"]),
                    RelatedStory = CommonFun.GetItemPath(item, "Related Story"),
                    StoryCat = new SGCareDAL().GetStoryTitle(((InternalLinkField)item.Fields["Related Story"]).Path),
                    ItemPath = LinkManager.GetItemUrl(item),
                    PageCount = CommonFun.GetPageCount(itemCount, "P"),
                    Type = ((LookupField)item.Fields["Type"]).TargetItem != null ? Convert.ToString(((LookupField)item.Fields["Type"]).TargetItem.Name) : string.Empty,
                    Time = ((LookupField)item.Fields["Time"]).TargetItem != null ? Convert.ToString(((LookupField)item.Fields["Time"]).TargetItem.Name) : string.Empty,
                    StoryName = ((InternalLinkField)item.Fields["Related Story"]).TargetItem != null ? Convert.ToString(((InternalLinkField)item.Fields["Related Story"]).TargetItem.Name) : "Raju",
                    ItemName = Convert.ToString(item.Name.Trim())
                }).ToList();
            }
            else { return new List<Programme>(); }
        }
        catch (Exception msg) { throw msg; }
    }

    /* Get Stories */
    [WebMethod]
    public static List<Story> GetStories(string catName, string skipNumber)
    {
        try
        {
            if (!string.IsNullOrEmpty(Constants.storiesItemId) && !string.IsNullOrEmpty(skipNumber))
            {
                List<Item> items = null; int itemCount = 0;

                if (!string.IsNullOrEmpty(catName) && catName.ToLower() != Constants.AllCats)
                {
                    items = web.GetItem(Constants.storiesItemId).GetChildren().Where(x => ((LookupField)x.Fields["Story Category"]).TargetItem != null && catName.Contains(((LookupField)x.Fields["Story Category"]).TargetItem.Name) == true).ToList();
                    itemCount = items.Count;
                    items = items.Skip(Convert.ToInt16(skipNumber)).Take(4).ToList();
                }
                else
                {
                    items = web.GetItem(Constants.storiesItemId).GetChildren().ToList();
                    itemCount = items.Count;
                    items = items.Skip(Convert.ToInt16(skipNumber)).Take(4).ToList();
                }

                return items.Select(story =>
                       new Story
                       {
                           /* Left Pane Data */
                           ThumbnailImage = CommonFun.GetImage(story, "Image Thumbnail"),
                           VideoUrl = CommonFun.LinkUrl(story, "Video Url"),
                           GetStartedCTA = Convert.ToString(story.Fields["Get Started CTA"]),
                           GetStartedUrl = CommonFun.LinkUrl(story, "Get Started CTA Url"),

                           /* Right Pane Data */
                           CategoryIconPath = CommonFun.GetImage(((LookupField)story.Fields["Story Category"]).TargetItem, "Icon"),
                           Title = Convert.ToString(story.Fields["Title"]),
                           ShortDesc = Convert.ToString(story.Fields["Description"]).Length <= 200 ? Convert.ToString(story.Fields["Description"]) : CommonFun.TruncateAtWord(Convert.ToString(story.Fields["Description"]), 200),
                           CTA = System.Convert.ToString(story.Fields["CTA"]),
                           ItemPath = LinkManager.GetItemUrl(story),
                           ItemName = story.Name.Trim(),
                           PageCount = CommonFun.GetPageCount(itemCount, "S"),
                       }).ToList();
            }
            else { return new List<Story>(); }
        }
        catch (Exception msg) { throw msg; }
    }

    /* Update Pledge Count when form is submiited */
    [WebMethod]
    public static string UpdateCount()
    {
        try
        {
            Item item = master.GetItem(Constants.sgCareItemId);

            string exitsValue = string.Empty;

            if (item != null)
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    
                    exitsValue = Convert.ToString(item.Fields["Pledge Count"].Value);
                    bool numberValid = Regex.IsMatch(exitsValue, "^[0-9]+$");
                    if (!string.IsNullOrEmpty(exitsValue) && numberValid)
                    {
                        item.Editing.BeginEdit();
                        item.Fields["Pledge Count"].Value = Convert.ToString((Convert.ToInt32(exitsValue) + 1));
                        item.Editing.EndEdit();
                        return Convert.ToString(item.Fields["Pledge Count"].Value.ToString());
                    }
                    else
                    {
                        item.Editing.BeginEdit();
                        item.Fields["Pledge Count"].Value = "1";
                        item.Editing.EndEdit();
                        return "1";
                    }
                }
            }
            return "Fail";
        }
        catch (Exception msg) { throw msg.InnerException; }
    }




}
