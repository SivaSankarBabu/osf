﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SGFutureLandingSublayout.ascx.cs" Inherits="sgfuture.SGFuture_Revamp.sublayouts.SGFutureLandingSublayout" %>
<%@ Register Src="~/SGFuture Revamp/sublayouts/ProjectShowcaseSublayout.ascx" TagPrefix="uc1" TagName="ProjectShowcaseSublayout" %>


<main class="page page-landing">
    		<div class="container-fluid">
				<!-- begin section hero -->
				<section class="section-hero">
                    <div id="divMaintenance" runat="server" class="maintenance">
                          <span><i class="fa fa-close"></i>
                         <sc:Text ID="txtMaintenanceMessage" runat="server" Field="Maintenance Text" />
                        </span>
                    </div>  
                     
					<div class="wrap-hero">
                        <sc:Image ID="imgBanner" runat="server" Field="Desktop Banner Image" CssClass="bg-thumb hidden-xs DskImg "/>
                        <sc:Image ID="imgMobBanner" runat="server" Field="Mobile Banner Image" CssClass="bg-thumb visible-xs MbImg"/>                         
						<div class="hero-info text-center">
							<h2 class="hero-title">
								<%--<sc:Text ID="txtSGFuture" runat="server" Field="Banner Text" />--%>
							</h2>
                            <asp:HiddenField ID="hdnoverlayImage" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hdnOverlayImageformobile" runat="server" ClientIDMode="Static" />
							<div class="hoz-line"></div>
                           <%  if (!String.IsNullOrEmpty(Sitecore.Context.Item["Watch Video Button Text"]))
                               {%>
							<div class="wrap-button">
								<span class="border-ver left"></span>
								<span class="border-ver right"></span>
								<span class="border-hoz top"></span>
								<span class="border-hoz bottom"></span>
								<a class="button button-watch-video" href="#modalVideo" title="watch video" onClick="ga('send', 'event', { eventCategory: 'SGFuture Homepage', eventAction: 'Click', eventLabel: 'Watch video'});">
                                    <sc:Text ID="txtBtnText" runat="server" Field="Watch Video Button Text" /> </a>
							</div><%} %>
                            <asp:HiddenField ID="hdnVideo" runat="server" />
                            
						</div>
					</div>
				</section>
				<!-- end section hero -->
				<!-- begin section about -->              
				<section class="section-about">
					<div class="container">
						<div class="row">
                            <div class="col-md-12 col-xs-12 text-center">
                                <h2 class="title-right">
								 <sc:Text ID="txttitleright" runat="server" Field="Introduction Title right" />
								</h2>
                            </div>
							<div id="Div1" class="col-xs-12 col-sm-5 col-sm-push-1" runat="server">
								
								<p class="desc">
                                    <sc:Text ID="txtDescLeft" runat="server" Field="Introduction Description Left" />									
								</p>
							</div>
							<div class="col-xs-12 col-sm-5 col-sm-push-1">
								
								<p class="desc">
									<sc:Text ID="txtdescriptionRight" runat="server" Field="Introduction Description Right" />
								</p>
								
							</div>
                            <div class="col-md-12 col-xs-12 col-sm-12 wrap-custom">
                                <div class="wrap-action text-center">
                                      <%  if (!String.IsNullOrEmpty(Sitecore.Context.Item["Get Started Button Text"].Trim()))
                                          {%>
									<div class="wrap-button">
										<span class="border-ver left"></span>
										<span class="border-ver right"></span>
										<span class="border-hoz top"></span>
										<span class="border-hoz bottom"></span>
										<%--<a class="button button-start" href="/" title="get started" target="_blank"><sc:Text ID="txtGetstarted" runat="server" Field="GetStarted Text" /></a>--%>
                                         <asp:HyperLink ID="lnkUrl"  runat="server" Target="_blank" CssClass="button button-start" onClick="ga('send', 'event', { eventCategory: 'SGFuture Homepage', eventAction: 'Click', eventLabel: 'Get started'});"><sc:Text ID="txtGetstarted" runat="server" Field="Get Started Button Text" /></asp:HyperLink>
									</div>
                                    <%} %>

                                    <%if (!String.IsNullOrEmpty(Sitecore.Context.Item["Be a volunteer Text"].Trim()))
                                      { %>
									<div class="wrap-button">
										<span class="border-ver left"></span>
										<span class="border-ver right"></span>
										<span class="border-hoz top"></span>
										<span class="border-hoz bottom"></span>
										<%--<a class="button button-volunteer" href="#" title="be a volunteer"><sc:Text ID="Text1" runat="server" Field="Be a volunteer Text" /></a>--%>
                                        <asp:HyperLink ID="lnkUrl1" runat="server" Target="_blank" CssClass="button button-volunteer" onClick="ga('send', 'event', { eventCategory: 'SGFuture Homepage', eventAction: 'Click', eventLabel: 'Be a volunteer'});"><sc:Text ID="TxtVolunteer" runat="server" Field="Be a volunteer Text" /></asp:HyperLink>
									</div><%} %>
								</div>
                            </div>
						</div>
					</div>
				</section>               
				<!-- end section about -->

				<!-- begin section main -->
				<section class="section-main">
					<!-- begin block inspired -->
					<div class="block-inspired text-center">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<h3 class="title">
										<%--<sup class="number"></sup>--%>
										<span class="text"> <sc:Text ID="txtGetInspired" runat="server" Field="Get Inspired Title" /></span>
									</h3>
									<p class="desc">
										<sc:Text ID="txtGetstartedDesc" runat="server" Field="Get Inspired Description" />
									</p>
								</div>
							</div>
						</div>
					</div>
					<!-- end block inspired -->
					<!-- begin block projects -->
					<div class="block-projects">
						<div class="row">
							<div class="col-xs-12 col-sm-6 no-padding">
								<article class="project-item item-large">
									<figure class="wrap-thumb">
										<img class="thumb" src="#" />
									</figure>
									<div class="fade"></div>
									<figcaption class="wrap-info">
										<%--<span class="project-no">project 03</span>--%>
										<span class="project-date"></span>
										<!-- <span class="project-date">03 Mar 2016</span> -->
										<h2 class="project-title"></h2>
                                        <input type="hidden" class="hdnTitle" />
										<p class="project-desc"></p>
										<span class="project-link" ></span>
									</figcaption>
									<div class="wrap-line">
										<div class="line line-ver"></div>
										<div class="line line-hoz"></div>
										<div class="cross cross-top"></div>
										<div class="cross cross-bottom"></div>
										<div class="cross-bg"></div>
									</div>
									<a class="wrap-link" href="#"></a>
								</article>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="row">
									<div class="col-xs-6 no-padding">
										<article class="project-item">
											<figure class="wrap-thumb">
												<img class="thumb" src="#" />
											</figure>
											<div class="fade"></div>
											<figcaption class="wrap-info wrap-landing-custom">
												<%--<span class="project-no">project 07</span>--%>
												<span class="project-date"></span>
												<!-- <span class="project-date">03 Mar 2016</span> -->
												<h2 class="project-title"></h2>
                                                <input type="hidden" class="hdnTitle" />
												<p class="project-desc">
													
												</p>
												<span class="project-link " ></span>
											</figcaption>
											<div class="wrap-line">
												<div class="line line-ver"></div>
												<div class="line line-hoz"></div>
												<div class="cross cross-top"></div>
												<div class="cross cross-bottom"></div>
												<div class="cross-bg"></div>
											</div>
											<a class="wrap-link" href="#"></a>
										</article>
									</div>
									<div class="col-xs-6 no-padding">
										<article class="project-item">
											<figure class="wrap-thumb">
												<img class="thumb" src="#" />
											</figure>
											<div class="fade"></div>
											<figcaption class="wrap-info wrap-landing-custom">
												<%--<span class="project-no">project 02</span>--%>
												<span class="project-date">project 02</span>
												<!-- <span class="project-date">03 Mar 2016</span> -->
												<h2 class="project-title"></h2>
                                                <input type="hidden" class="hdnTitle" />
												<p class="project-desc"></p>
												<span class="project-link" ></span>
											</figcaption>
											<div class="wrap-line">
												<div class="line line-ver"></div>
												<div class="line line-hoz"></div>
												<div class="cross cross-top"></div>
												<div class="cross cross-bottom"></div>
												<div class="cross-bg"></div>
											</div>
											<a class="wrap-link" href="#"></a>
										</article>
									</div>
									<div class="col-xs-6 no-padding">
										<article class="project-item">
											<figure class="wrap-thumb">
												<img class="thumb" src="#" />
											</figure>
											<div class="fade"></div>
											<figcaption class="wrap-info wrap-landing-custom">
												<%--<span class="project-no">project 06</span>--%>
												<span class="project-date"></span>
												<!-- <span class="project-date">03 Mar 2016</span> -->
												<h2 class="project-title"></h2>
                                                <input type="hidden" class="hdnTitle" />
												<p class="project-desc"></p>
												<span class="project-link"></span>
											</figcaption>
											<div class="wrap-line">
												<div class="line line-ver"></div>
												<div class="line line-hoz"></div>
												<div class="cross cross-top"></div>
												<div class="cross cross-bottom"></div>
												<div class="cross-bg"></div>
											</div>
											<a class="wrap-link" href="#"></a>
										</article>
									</div>
									<div class="col-xs-6 no-padding">
										<a class="project-item-all project-item-all-cus" href="/sgfuture/Project-Showcase" onClick="ga('send', 'event', { eventCategory: 'SGFuture Homepage', eventAction: 'Click', eventLabel: 'View all projects'});">
											<div class="wrap-info">
												<div class="wrap-button">
													<span class="border-ver left"></span>
													<span class="border-ver right"></span>
													<span class="border-hoz top"></span>
													<span class="border-hoz bottom"></span>
													<span class="button">INSPIRE ME</span>
												</div>
												<%--<h3 class="project-title">all projects</h3>--%>
											</div>
											<div class="wrap-line">
												<div class="line line-ver"></div>
												<div class="line line-hoz"></div>
												<div class="cross cross-top"></div>
												<div class="cross cross-bottom"></div>
												<div class="cross-left"></div>
												<div class="cross-right"></div>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
                    
					<!-- end block projects -->
					<!-- begin block quotes -->
					<div class="block-quotes" runat="server" id="divQuote">
						<div class="container">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1">
									<div class="wrap-quote">
										<ul class="list-quote">
                                            <asp:Repeater ID="rptQuotes" runat="server">
                                                <ItemTemplate>
											<li class="item-quote">
												<a class="item-link" href="#">
													<figure class="avatar">
                                                        <img class="thumb" src='<%#DataBinder.Eval(Container.DataItem,"QuoteImage") %>' />
													</figure>
													<figcaption class="info">
														<span class="author-name">
                                                            <asp:Label ID="lblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QuoteName") %>'></asp:Label>
														</span>
														<span class="author-title">
                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QuoteDesignation") %>'></asp:Label>
														</span>
														<p class="quote">
                                                            <asp:Label ID="lblQuotation" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation") %>'></asp:Label>
														</p>
													</figcaption>
												</a>
											</li>
                                            </ItemTemplate>
                                            </asp:Repeater>											
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end block quotes -->
					<!-- begin block engagement -->
					<div class="block-engagement text-center">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<h3 class="title">
									<%--	<sup class="number"></sup>--%>
										<span class="text">
											<sc:Text ID="txtPEngagement" runat="server" Field="Past Engagement Title" />
										</span>
									</h3>
                                   <p class="desc">
									<sc:Text ID="txtPEngDesc" runat="server" Field="Past Engagement Description" /></p>
								<%if (!String.IsNullOrEmpty(Sitecore.Context.Item["View All Button Text"]))
          { %>
                                    <div class="wrap-button button-blue">
										<span class="border-ver left"></span>
										<span class="border-ver right"></span>
										<span class="border-hoz top"></span>
										<span class="border-hoz bottom"></span>
										<a class="button button-volunteer" href="/sgfuture/Past-Engagements" title="view all" onClick="ga('send', 'event', { eventCategory: 'SGFuture Homepage', eventAction: 'Click', eventLabel: 'View all'});">
                                            <sc:Text ID="txtViewAllBtn" runat="server" Field="View All Button Text" />
										</a>
                                        <asp:HiddenField ID="hdnPastEngagement" runat="server" ClientIDMode="Static" />
                                        <asp:HiddenField ID="hdnPastEngagementforMobile" runat="server" ClientIDMode="Static" />
									</div>
                                    <%} %>
								</div>
							</div>
						</div>
					</div>
					<!-- end block engagement -->
				</section>
				<!-- end section main -->
			</div>
     	<a class="btn-back-top" href="javascrip:;"></a>

		</main>
<!-- main -->

<!-- [begin modal]-->
<div class="wrap-modal">
    <div rol="dialog" id="modalVideo" class="modal fade modal-fullscreen">
        <div class="modal-dialog">
            <div class="modal-content text-center">
                <div class="modal-body">
                    <iframe width="560" height="315" id="popup-youtube-player" frameborder="0" allowfullscreen></iframe>
                    <!-- <a data-dismiss="modal" href="javascript:void(0);" class="close-button" aria-label="Close"></a> -->
                    <br />
                    <div class="wrap-button">
                        <span class="border-ver left"></span>
                        <span class="border-ver right"></span>
                        <span class="border-hoz top"></span>
                        <span class="border-hoz bottom"></span>
                        <a class="button button-watch-video" data-dismiss="modal" href="javascript:void(0);" title="close">close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- [end modal]-->

<div id="divHideItems" style="display: none">
    <asp:Repeater ID="repFeaturedNews" runat="server">
        <ItemTemplate>
            <div>
                <input type="hidden" class="ItemImage" value='<%# DataBinder.Eval(Container.DataItem, "Thumb Image") %>' />
                <input type="hidden" class="Title" value='<%# DataBinder.Eval(Container.DataItem, "Title") %>' />
                <input type="hidden" class="Date" value='<%# DataBinder.Eval(Container.DataItem, "Event Date") %>' />
                <input type="hidden" class="ShortDescription" value='<%# DataBinder.Eval(Container.DataItem, "Short Description") %>' />
                <input type="hidden" class="ItemUrl" value='<%# DataBinder.Eval(Container.DataItem, "Item Url") %>' />
                <input type="hidden" class="ItemId" value='<%# DataBinder.Eval(Container.DataItem, "Item Id") %>' />
                <input type="hidden" class="Readmore" value='<%# DataBinder.Eval(Container.DataItem, "Read More") %>' />
                <input type="hidden" class="hdnItemName" value='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>



<script type="text/javascript">

    /* function to Bind 4 tiles by default */
    function BindImages(targetCtrlId, sourceCtrlId) {
        if ($(targetCtrlId).length > 0 && $(sourceCtrlId).length > 0) {
            $(targetCtrlId).each(function () {
                var index = $(this).index(targetCtrlId), src = '';
                src = $(sourceCtrlId).eq(index);
                $(this).find('.wrap-thumb > img').attr('src', src.find('.ItemImage').val());
                $(this).find('.wrap-info > .project-date ').text(src.find('.Date').val());
                $(this).find('.wrap-info > .project-title').html(src.find('.Title').val());
                $(this).find('.wrap-info p').html(src.find('.ShortDescription').val());
                $(this).find('.wrap-link').attr('href', src.find(".ItemUrl").val());
                $(this).find('.wrap-info > .project-link').html(src.find(".Readmore").val());
                $(this).find(".hdnTitle").val(src.find('.hdnItemName').val());
            });
        } else { return false; }
    }

    BindImages('.project-item', '#divHideItems div');

    $('.project-item').click(function () {
        var Title = $(this).find('.wrap-info .hdnTitle').val();
        ga('send', 'event', {
            eventCategory: 'SGFuture Homepage',
            eventAction: 'Click',
            eventLabel: Title
        });
    });


    /* Watch button Text is empty show hide line above watch video button */
    if ($('.wrap-hero .button-watch-video').text().trim() == '') {
        $('.hoz-line').hide();
    } else { $('.hoz-line').show(); }

    /*For maintance Message close button*/
    $('#content_0_divMaintenance').find('.fa-close').click(function () {
        $('.maintenance').hide();
    });

    $('#popup-youtube-player').attr('src', $('#content_0_hdnVideo').val());

    if ($(window).width() >= 768) {
        $('.block-engagement').css({ 'background': 'url("' + $('#hdnPastEngagement').val() + '")' });
        $('.hero-title').css({ 'background': 'url("' + $('#hdnoverlayImage').val() + '")' });
    } else {
        $('.block-engagement').css({ 'background': 'url("' + $('#hdnPastEngagementforMobile').val() + '")' });
        $('.hero-title').css({ 'background': 'url("' + $('#hdnOverlayImageformobile').val() + '")' });
    }



    if ($('#content_0_hdnVideo').val() == null || $('#content_0_hdnVideo').val() == '') {
        $('#modalVideo').remove(); $('.modal-backdrop').removeClass('in').addClass('');
    }

    /* Truncate item description if it is more than 35 chars only in mobile divices */
    function TrimDesc_Devices(ctrl) {
        if ($(ctrl).length > 0) {
            $(ctrl).each(function () {
                //$(this).parent().find('.project-link').hide();
                if ($(this).text().length > 35)
                    $(this).text($(this).text().substr(0, 35) + '...');

            });
        } else { return false; }
    }

    /* Truncate item title, If it is more than 25 chars only in mobile divices */
    function TrimTitle_Devices(ctrl) {
        if ($(ctrl).length > 0) {
            $(ctrl).each(function () {
                if ($(this).text().length > 25)
                    $(this).text($(this).text().substr(0, 25) + '...');
            });
        } else { return false; }
    }

    if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

        TrimDesc_Devices('.project-desc'), TrimTitle_Devices('.project-item h2');
    }

    $(window).load(function () {
        $('.button-watch-video').on('click', function () {
            $('#popup-youtube-player').attr('src', '');
        })

        $('.button-watch-video:eq(0)').click(function () {
            $('#popup-youtube-player').attr('src', $('#content_0_hdnVideo').val());
        })

        /*remove image width and height*/
        $('.DskImg,.MbImg').removeAttr("height").removeAttr("width");

    });

</script>
