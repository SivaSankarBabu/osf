﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Galleryvideolist.GalleryvideolistSublayout" CodeFile="~/SG50/layouts/GalleryVideoList.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<%--<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript" language="javascript"></script>--%>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/jquery.bxslider.min.js"></script>
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<script>
    $(function () {

        var api_key = 'AIzaSyB4VFYXvYG4_hzoupeI0ZYueTQvjbcr3QQ'; //For Live
       // var api_key = 'AIzaSyBi7B2buWEpUaWgto-puq5L2nbbEjeqFwg';
        $.getJSON("https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UCqQZD3jRc3jkaVwRWvBoSmw&maxResults=20&order=date&key=" + api_key + "&alt=json", function (data) {
            // + converts the string to int
            var feedInfo = data;
            var pages = Math.round(feedInfo.pageInfo.totalResults / feedInfo.pageInfo.resultsPerPage);
            var video = feedInfo.items;
            var content = '';
            var video_album_full_description = '';
            var result = $.map(video, function (value, key) {

                if (value['snippet']['description'].length >= 180) {
                    des = value['snippet']['description'].substr(0, 170) + "...";
                } else {
                    des = value['snippet']['description'];
                }
                video_album_full_description = value['snippet']['description'];
                if (des == '')
                    video_album_full_description = des = '';
                content += '<li id="' + value['id'] + '">' +
                    '<div class="photo-cont">' +
                        '<img src="' + value['snippet']['thumbnails']['medium']['url'] + '"/>' +
                        '<h1>' + value['snippet']['title'] + '</h1>' +
                        '<span>' + new Date(value['snippet']['publishedAt']).toDateString() + '</span>' +
                         "<p data-rel='" + escape(video_album_full_description) + "'>" + des + '</p>' +
                    '</div>' +
                '</li>';
            });
            $("#YouTubePlayList").html(content);
            $(".popup-gallery.videos.popup-youtube .youtube").colorbox({ iframe: true, innerWidth: 640, innerHeight: 390 });
            $(".SG50Loader").hide();
        });
    });
    $(document).on("click", "#YouTubePlayList li", function (event) {
        var v_album_id = $(this).attr('id');
        localStorage.setItem('youtube_video_play_list_id', jQuery(this).attr('id'));
        var vt = $('#' + v_album_id + ' h1').html();
        var vd = $('#' + v_album_id + ' p').data('rel');
        localStorage.setItem('vtitle', vt);
        localStorage.setItem('vdescription', vd);
        window.location = "/SG50/GalleryLanding/GalleryVideoList/GalleryVideos.aspx";
    });
</script>


<div class="masthead">

    <!-- Photos Header -->
    <div class="photo-header">
        <div class="photo-row">
            <div class="cell-12">
                <div class="group-text-button">
                    <a href="/SG50/GalleryLanding.aspx" class="title-button"><sc:Text ID="txtButtonText" runat="server" Field="Back Button Text" /></a>
                    <h1><sc:Text ID="txtTitle" runat="server" Field="Title" /></h1>
                </div>
                <p class="mrg-b40"><sc:Text ID="txtDescription" runat="server" Field="Description" /></p>
            </div>

        </div>
    </div>
    <!-- Photos Header -->
    <div class="photo-gallery gallery-VRow">
        <div class="photo-row">
            <div class="popup-gallery videos popup-youtube">
                <div class="SG50Loader">
                    <img src="/SG50/images/Gallery/SG50Loader.gif" /></div>
                <ul id="YouTubePlayList"></ul>
            </div>
        </div>
        <!--<div class="cell-12">
            <div class="pagination">
                <ul>
                    <li><a href="#" class="pre"></a></li>
                    <li><a href="#" class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#" class="next"></a></li>
                </ul>
            </div>
        </div> -->
    </div>
</div>
<script src="/SG50/include/js/jquery.colorbox.js" type="text/javascript"></script>
<script type="text/javascript">var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>
