﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Timecapsulenew.TimecapsulenewSublayout" CodeFile="~/SG50/layouts/TimeCapsuleNew.ascx.cs" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SG50 Time Capsule Campaign</title>
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
    <%--<script src="/SG50/include/js/jquery.min_Home.js"></script>
    <script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>--%>
    <link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
    <link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css" />
    <link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css" />
    <link href="/SG50/include/css/thematic-campaign-responsive.css" rel="stylesheet"
        type="text/css" />
    <!--//new css SG50 Time Capsule Campaign-->
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/campaign.css" />
    <%--// new JS for SG50 Time Capsule Campaign--%>

    <script src="/SG50/include/js/jquery.timeCapsuleCampaign.js" type="text/javascript"
        charset="utf-8"></script>

    <%--<script src="/SG50/include/js/jquery.timeCapsuleCampaign.min.js" type="text/javascript" charset="utf-8"></script>--%>

    <script type="text/javascript" src="/SG50/include/js/Custom%20Script/TimeCapsuleCustom.js"></script>

    <script type="text/javascript">
        var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
        window.fbAsyncInit = function () {
            FB.init({
                appId: FBID, status: true, cookie: true, version: 'v2.2'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $(function () {
            /* Facebook and Twitter share GA Tracking  */
            function GATrackingFBTwitterShare(ctrld, shareType, GATrackTitle) {
                if (shareType != '' && shareType != undefined && shareType != null && $(ctrld).length > 0) {
                    $(ctrld).click(function () {
                        if (shareType == 'Facebook') {
                            ga('send', 'event', 'Landing page Time Capsule', 'Click', GATrackTitle);
                        } else if (shareType == 'Twitter') {
                            ga('send', 'event', 'Landing page Time Capsule', 'Click', GATrackTitle);
                        } else { return false; }
                    });
                }
            }

            GATrackingFBTwitterShare('.campaignDescRight .facebookShare', 'Facebook', 'Time Capsule - Post - Facebook Share');
            GATrackingFBTwitterShare('.campaignDescRight .twitterShare', 'Twitter', 'Time Capsule - Post - Twitter Share');
        })

        function postToFeed(Title, Description, img, HostName) {
            var fbtitle = Title, fbdes = Description, fbimg = img == '' || img == undefined || img == undefined ? $('#spanHostName').text() + $('#fbShareIcon').attr('src') : img;
            var obj = {
                method: 'feed', link: HostName, picture: fbimg, name: fbtitle.toString(), description: fbdes.toString()
                // caption: fbdes,
            }; FB.ui(obj);
        }


        function newPopup(urlasp, url1, HostName, AccessToken) {
            var accessToken = AccessToken, result;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
            var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
            $.getJSON(
                url, {},
                function (response) {
                    result = response.data.url;
                    var tempurl = 'https://twitter.com/intent/tweet?text=' + escape(urlasp) + ' ' + result;
                    popupWindow = window.open(
                tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')
                }
            );
        }
    </script>

</head>
<form id="">
</form>
<div id="homePage" class="container rew relative">
    <div class="BannerCampaign">
        <sc:image id="desktopBanner" field="Banner Image" runat="server" cssclass="ShowOnDesktop" />
        <sc:image id="mobileBanner" field="Mobile Banner Image" runat="server" cssclass="ShowOnMobile" />
    </div>
    <div class="campaignDesc">
        <div class="category_container">
            <div class="campaignDescLeft">
                <p>
                    <sc:text id="txtSummary" runat="server" field="Summary Of Campaign" />
                </p>
            </div>
            <div class="campaignDescRight">
                <div class="shareTxt">
                    <h4>
                        Share on
                    </h4>
                </div>
                <asp:Repeater ID="repsocialSharingTop" runat="server">
                    <ItemTemplate>
                        <ul class="socioShare">
                            <li><a class="facebookShare" href="JavaScript:postToFeed('<%# Eval("SocialShareTitle").ToString()%>','<%# Eval("SocialShareContent").ToString()%>','<%# Eval("SocialSharingThumbNail")%>','<%# Eval("EventUrl")%>')">
                            </a></li>
                            <li><a class="twitterShare" href="JavaScript:newPopup('<%# Eval("TwitterSharingDescription").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')">
                            </a></li>
                        </ul>
                        <div id="hostDetails" style="display: none">
                            <span id="spanEventUrl">
                                <%# Eval("EventUrl")%></span> <span id="spanHostName">
                                    <%# Eval("HostName")%></span> <span id="spanAccessToken">
                                        <%# Eval("AccessToken")%></span>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <!-- // campaign desc -->
    <div class="arrowDown">
        <img src="/SG50/images/Capsule/arrow_down.png" alt="" title="" border="0" />
    </div>
    <div class="howitworks">
        <div class="category_container">
            <img src="/SG50/images/Capsule/how_it_works.png" alt="" title="" border="0" />
            <p>
                <sc:text id="txtDescription" runat="server" field="Description" />
            </p>
        </div>
    </div>
    <div class="level2">
        <div class="category_container">
            <div class="filterHolderNew">
                <div class="mobileFilter">
                    <div class="viewcategories">
                        <a href="javascript:void(0)" class="viewcate" id="showDiv"><span>
                            <img src="/SG50/images/Capsule/viewcategory.png" alt="" title="" border="0" class="nonactive" />
                            <img src="/SG50/images/Capsule/viewcategory_active.png" alt="" title="" border="0"
                                class="active2" />
                        </span></a>
                    </div>
                    <div class="suggestfilter">
                        <a href="javascript:void(1)" class="suggestcate"><span>
                            <img src="/SG50/images/Capsule/suggest_item.png" alt="" title="" border="0" class="nonactive" />
                            <img src="/SG50/images/Capsule/suggest_item_active.png" alt="" title="" border="0"
                                class="active2" />
                        </span></a>
                    </div>
                </div>
                <h2>
                    Category view:
                </h2>
                <ul class="filterSection">
                    <li><a data-option="all" class="current">All</a></li>
                    <li runat="server" id="listIDNew"><a data-option="all">New</a></li>
                    <li><a data-option="family">One People</a> </li>
                    <li><a data-option="technology">Our Community</a></li>
                    <li><a data-option="design">Global Connections</a></li>
                    <li><a data-option="mobile">Can-do Spirit</a></li>
                </ul>
                <span id="spanSelectedCategories" style="display: none"></span>
            </div>
            <div class="listItems">
                <ul>
                    <li id="HideSuggestOnMobile">
                        <div class="boxcontainer_new">
                            <div class="suggestIcon">
                                <a href="javascript:void(0)" id="ShowSuggestAnItem">
                                    <img border="0" title="" alt="" src="/SG50/images/Capsule/suggest.png"></a>
                            </div>
                            <h4 class="suggestIcon">
                                Suggest a
                                <br>
                                new item</h4>
                        </div>
                    </li>
                    <asp:Repeater ID="repeaterTimeCapusuleItems" runat="server">
                        <ItemTemplate>
                            <li>
                                <div class="boxcontainer">
                                    <div class="thumbIcon">
                                        <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                                    </div>
                                    <h4>
                                        <%#Eval("ItemTitle")%></h4>
                                    <div class="caption">
                                        <%#Eval("ItemDescription")%>
                                    </div>
                                    <div class="votesblock">
                                        <div class="votes">
                                            <a href="javascript:void(0)">&nbsp;</a>
                                            <div class="votecount">
                                                <%#Eval("Itemvotes")%>
                                                likes
                                            </div>
                                        </div>
                                        <div class="grabItem">
                                            <a href="javascript:void(0)" id="ShowVote">
                                                <img id="imgIwnatThisIn" alt="Image" src="/SG50/images/Capsule/iwantthis.png" /></a>
                                        </div>
                                        <div id="divItemDetails" style="display: none">
                                            <input type="hidden" value='<%#Eval("ID")%>' id="itemId" />
                                            <span id="itemName">
                                                <%#Eval("Name")%></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <%--  <div id="divLoader" style="width: 100%; text-align: center; display: table;">--%>
            <div class="animation_image" style="margin: 0 auto;">
                <img src="/SG50/images/ajax-loader1.gif">
                Loading...
            </div>
            <%--</div>--%>
            <div class="loadMoreBox load_more" id="load_more_button">
                <a class="LoadMoreBtn" data-option="all">
                    <img src="/SG50/images/Capsule/load_more.png" />
                </a>
            </div>
        </div>
        <div>
            <div class="suggestAnItem suggestAnItem_popup">
                <div class="suggest-wrapper category_container">
                    <a href="javascript:void(0)" class="clostBtn">
                        <img src="/SG50/images/Capsule/close_btn.png" alt="" title="" border="0" />
                    </a>
                    <h2>
                        <img src="/SG50/images/Capsule/suggest_an_item_header.png" alt="" title="" border="0" />
                    </h2>
                    <div>
                        <p class="SelectedItemTxt SelectedItemTxt_one">
                            Could not find your favourite item on the list?
                            <br />
                            Feel free to leave us your suggestion below!
                        </p>
                    </div>
                    <div class="formBlock">
                        <div class="formRight">
                            <form id="suggestanitemform" name="suggestAnItemForm" method="POST">
                            <div class="formItem">
                                <span class="errortxt">Please insert your name </span><span class="errortxt">Your name
                                    must not be exceeds 100 characters long</span>
                                <input type="text" placeholder="Name*" id="name" name="name" autocomplete="off" />
                            </div>
                            <div class="formItem">
                                <span class="errortxt">Please enter a valid email address</span> <span class="errortxt">
                                    Please enter a valid email address</span>
                                <input type="text" placeholder="Email*" id="email" name="email" autocomplete="off" />
                            </div>
                            <div class="formItem">
                                <strong>Your suggestion: </strong>
                            </div>
                            <div class="formItem">
                                <span class="errortxt">Please insert name of item </span><span class="errortxt">Name
                                    of the item must not be exceeds 20 characters long</span>
                                <input type="text" placeholder="Name of Item*" id="name_item" name="name_item" autocomplete="off" />
                            </div>
                            <div class="formItem">
                                <span class="errortxt">Please insert your story </span>
                                <textarea placeholder="Tell us why this item should be in the SG50 Time Capsule"
                                    name="your_story" id="your_story"></textarea>
                                <span class="char"><span id="counter"></span>&nbsp;characters</span> <span class="char">
                                    * Mandatory fields</span>
                            </div>
                            <div class="formItem">
                                <input type="button" value="" class="submitBtn2" id="btnSuggestNewItem" />
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- suggest an item overlay ends here -->
            <div class="suggestAnItem SuggestThankYou_popup" id="SuggestThankYou">
                <div class="suggest-wrapper category_container">
                    <span class="clostBtn"><a href="javascript:void(0)">
                        <img src="/SG50/images/Capsule/close_btn.png" alt="" title="" border="0" />
                    </a></span>
                    <div class="thankyouHolder">
                        <img src="/SG50/images/Capsule/thank_you_suggest.png" alt="" title="" border="0" />
                        <p id="pSuggestItemDescription">
                            <sc:text id="txtSuggestThankYouMessage" runat="server" field="Suggest Thankyou Message" />
                        </p>
                        <div class="sharecapsule">
                            <img src="/SG50/images/Capsule/share_capsule.png" alt="" title="" border="0" class="sharecapsuletitle" />
                            <ul>
                                <li><a href="javascript:void(0)">
                                    <img id="imgFbShareSuggestNewItem" src="/SG50/images/Capsule/facebook_share_capsule.png"
                                        alt="" title="" border="0" />
                                </a></li>
                                <li><a href="javascript:void(0)">
                                    <img id="imgTweetShareSuggestNewItem" src="/SG50/images/Capsule/twitter_capsule_share.png"
                                        alt="" title="" border="0" />
                                </a></li>
                            </ul>
                        </div>
                        <div id="divSuggestSocialShareDetails" style="display: none">
                            <p id="pSuggestFBTitle">
                                <sc:text id="scSuggestFBTitle" runat="server" field="Suggest Facebook Title" />
                            </p>
                            <p id="pSuggestFbContent">
                                <sc:text id="scSuggestFbContent" runat="server" field="Suggest Facebook Content" />
                            </p>
                            <p id="pSuggestTwitterContent">
                                <sc:text id="scSuggestTwitterContent" runat="server" field="Suggest Twitter Content" />
                            </p>
                        </div>
                    </div>
                    <img style="display: none" id="fbShareIcon" src="/SG50/images/Capsule/fb-share.jpg"
                        alt="Image can't be displayed." />
                </div>
            </div>
            <!-- Thank you for Suggest an item ends here -->
            <div class="suggestAnItem VoteForItem_popup" id="divVoteForItemPopup">
                <div class="suggest-wrapper category_container">
                    <span class="clostBtn"><a href="javascript:void(0)">
                        <img src="/SG50/images/Capsule/close_btn.png" alt="" title="" border="0" />
                    </a></span>
                    <h2>
                        <img src="/SG50/images/Capsule/vote_baby_icon.png" alt="" title="" border="0" />
                    </h2>
                    <p class="SelectedItemTxt">
                        You have selected the following item for the SG50 Time Capsule
                    </p>
                    <div class="ribbon">
                        <div class="middle">
                            <span class="left"></span><span id="spanTitle"></span><span class="right"></span>
                        </div>
                    </div>
                    <div style="text-align: center;">
                        Thank you for choosing this item. If you wish to, you can leave a message for our future generations.
                    </div>
                    <span class="clearfix">&nbsp;</span>
                    <div class="formBlock">
                        <div class="formRight">
                            <form id="VoteForItemForm" name="VoteForItemForm" method="POST">
                            <div class="formItem">
                                <span class="errortxt">Please insert your name </span>
                                <input type="text" placeholder="Name*" id="txtPersonName" name="name" autocomplete="off" />
                            </div>
                            <div class="formItem">
                                <span class="errortxt">Please insert your email </span>
                                <input type="text" placeholder="Email*" id="txtEMail" name="email" autocomplete="off" />
                            </div>
                            <div class="formItem">
                                <span class="errortxt">Please insert your story </span>
                                <textarea placeholder="Tell us why this item should be in the SG50 Time Capsule*"
                                    name="v_your_story" id="v_your_story"></textarea>
                                <span class="char"><span id="my_counter"></span>&nbsp;characters</span> <span class="char">
                                    * Mandatory fields</span>
                            </div>
                            <div class="formItem">
                                <%--<input type="submit" value="Submit" class="submitBtn" id="btnVoteForAnItem" />--%>
                                <input type="submit" value="" class="submitBtn2" id="btnVoteForAnItem" />
                                <span id="spanVoteForanItemId" style="display: none"></span>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- vote for your item overlay ends here -->
            <div class="suggestAnItem vus_popup" id="VoteThankYou">
                <div class="suggest-wrapper category_container">
                    <span class="clostBtn"><a href="">
                        <img src="/SG50/images/Capsule/close_btn.png" alt="" title="" border="0" />
                    </a></span>
                    <div class="thankyouHolder2">
                        <img src="/SG50/images/Capsule/thank_you_vote.png" alt="" title="" border="0" />
                        <span class="clearfix">&nbsp;</span>
                        <img src="/SG50/images/Capsule/vote_baby_icon_thankyou.png" alt="" title="" border="0" />
                        <div class="ribbon">
                            <div class="middle">
                                <span class="left"></span><span id="spanThankyouItemName"></span><span class="right">
                                </span>
                            </div>
                        </div>
                        <p id="pThankyouDesc">
                            <sc:text id="txtVoteThankYouMessage" runat="server" field="Thankyou Message" />
                            <%--Thank you for your selection. The final SG50 Time Capsule items will be revealed in the later part of the year. Meanwhile, you can continue to select or suggest more of your favourite items! Do encourage your friends to do so too!--%>
                        </p>
                        <div class="sharecapsule">
                            <img src="/SG50/images/Capsule/share_capsule.png" alt="" title="" border="0" class="sharecapsuletitle" />
                            <ul>
                                <li><a href="javascript:void(0)">
                                    <img id="imgVoteThankyouFbShare" src="/SG50/images/Capsule/facebook_share_capsule.png"
                                        alt="" title="" border="0" />
                                </a></li>
                                <li><a href="javascript:void(0)">
                                    <img id="imgVoteThankyouTwitter" src="/SG50/images/Capsule/twitter_capsule_share.png"
                                        alt="" title="" border="0" />
                                </a></li>
                            </ul>
                        </div>
                        <div id="divVoteItemSocialSharingDetails" style="display: none">
                            <p id="pVoteFbContent">
                                <sc:text id="scVoteFbContent" runat="server" field="Vote Facebook Content" />
                            </p>
                            <p id="pVoteTwitterContent">
                                <sc:text id="scVoteTwitterContent" runat="server" field="Vote Twitter Content" />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <%--<span id="spanEvalHiddenField" runat="server">'<%# Eval("EventUrl")%>'</span>--%>
            <!-- Thank you for vote an item ends here -->
            <div class="overlaybg">
            </div>
        </div>
        <span id="spanTotalNuberOfItems" runat="server" style="display: none"></span>
    </div>
    <%--<div id="divHi">
            <div id="suggestItemExtra" class="listItemsExtra" style="display: none">
                <ul>
                    <li id="HideSuggestOnMobileExtra">
                        <div class="boxcontainer">
                            <div class="suggestIcon">
                                <a href="javascript:void(0)" id="showSuggestAnItemExtra">
                                    <img border="0" title="" alt="" src="/SG50/images/Capsule/suggest.png"></a>
                            </div>
                            <h4 class="suggestIcon">Suggest a
                                    <br>
                                new item</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>--%>
    <div class="level2 level2_one" style="display: none">
        <div class="category_container">
            <div id="appendDiv" class="listItems_one">
                <ul>
                    <li id="Li1">
                        <div class="boxcontainer_new" style="">
                            <div class="suggestIcon">
                                <a href="javascript:void(0)" id="A1">
                                    <img border="0" title="" alt="" src="/SG50/images/Capsule/suggest.png"></a>
                            </div>
                            <h4 class="suggestIcon">
                                Suggest a
                                <br>
                                new item</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<%--<script src="/SG50/include/js/jquery.min.js"></script>--%>
<!--// new JS for SG50 Time Capsule Campaign-->
<!--jQuery plugin that provides a character counter for any text input or textarea-->

<script type="text/javascript" charset="utf-8" src="/SG50/include/js/jquery.simplyCountable.js"></script>

<script type="text/javascript" charset="utf-8" src="/SG50/include/js/jquery.charactercounter.js"></script>

<script type="text/javascript" charset="utf-8" src="/SG50/include/js/jquery.validate.min.js"></script>

<!--// End of new JS for SG50 Time Capsule Campaign-->

<script>
    function ShowMenu() {
        $(".menu").toggle()
    }
    $(function() {
        $(window).scroll(function() {
            var e = $(this).scrollTop();
            $(".section ").each(function() {
                if (e > $(this).offset().top - 100) {
                    $(".section").removeClass("current");
                    $(this).addClass("current");
                    return
                }
            })
        });
        $("div.section").first();
        $("a.display").on("click", function(e) {
            e.preventDefault();
            var t = $(this).text(),
                    n = $(this);
            if (t === "next" && $(".current").next("div.section").length > 0) {
                var r = $(".current").next(".section");
                var i = r.offset().top;
                $(".current").removeClass("current");
                $("body,html").animate({
                    scrollTop: i
                }, function() {
                    r.addClass("current")
                })
            } else if (t === "prev" && $(".current").prev("div.section").length > 0) {
                var s = $(".current").prev(".section");
                var i = s.offset().top;
                $(".current").removeClass("current");
                $("body,html").animate({
                    scrollTop: i
                }, function() {
                    s.addClass("current")
                })
            }
        })
    });
    var a;
    var animation = true;
    $(document).ready(function() {
        $("#nav div").hover(function() {
            $("div", this).fadeIn(700);
            $(this).find("a:first").addClass("active_nav")
        }, function() {
            $("div", this).hide();
            $(this).find("a:first").removeClass("active_nav")
        });
        $("#nav a").each(function() {
            if ($(this).attr("href") == window.location.pathname) {
                $(this).addClass("selected")
            }
        })
    });
</script>

<script type="text/javascript">
    var catchMsg = 'An Error Occured While Voting an Item', newCat = 'New', allCat = 'All', currClass = 'current', failure = 'Failure', error = 'Error';

    $(function () {

        /* Function for Select Multiple Categories */
        function SelectMultipleCategories(divId) {
            var allAnchorTags = $(divId).find('li a');
            if ($(divId).length > 0) {
                allAnchorTags.click(function () {
                    allAnchorTags.removeClass(currClass); $(this).addClass(currClass);
                });
            } else { return false; }
        }

        /* Calling function for Selecting Multiple Categories */
        SelectMultipleCategories('.filterSection');

        /* Validate the vote for existing item form and vote the item */
        function VoteFornItem(btnId) {
            if ($(btnId).length > 0) {
                $(btnId).click(function () {
                    $("#VoteForItemForm").validate({
                        ignore: [], rules: { 'email': { required: true, email: true }, 'name': { required: true, maxlength: 100 }, 'v_your_story': { required: true, maxlength: 500 } },
                        errorPlacement: function (error, element) { error.insertBefore(element); },
                        messages: {
                            'email': { required: "Please enter a valid email address", email: "Please enter a valid email address" },
                            'name': { required: "Please enter your name", maxlength: "Your name must not be exceeds 100 characters long" },
                            'v_your_story': { required: "Please enter information", maxlength: "Information provided must not exceed 500 characters" }
                        },
                        submitHandler: function (VoteForItemForm) {
                            var dtop = parseInt($(window).scrollTop()) - 54;
                            VoteForItem($('#txtPersonName').val().trim(), $('#txtEMail').val().trim(), $('#v_your_story').val().trim(), $('#spanVoteForanItemId').text());
                            $("input[type=submit]").attr("disabled", "disabled");
                            $('.VoteForItem_popup').removeClass('productactive'); $('.vus_popup').addClass('thankyouactive').css('top', dtop);
                            AssignValuesForThankyouDivInVoteForItem('#VoteThankYou');
                        }
                    });
                })
            }
        }

        /* Get and Set Values */
        function AssignValuesForThankyouDivInVoteForItem(divId) {
            if ($(divId).length > 0) {
                $(divId).find('.thankyouHolder2').find('img:eq(1)').attr('src', $('#divVoteForItemPopup').find('img:eq(1)').attr('src'));
                $(divId).find('.middle #spanThankyouItemName').text($('div.middle #spanTitle').text());
                <%--$(divId).find('#pThankyouDesc').text($('#v_your_story').val());--%>
            } else return false;
        }

        /* Calling the vote for an item function */
        VoteFornItem('#btnVoteForAnItem');

        /* Vote for an existing item */
        function VoteForItem(name, eMail, description, itemId) {
            try {
          name=name.replace(/'/g, '');
          eMail=eMail.replace(/'/g, "\\'");
          description=description.replace(/'/g, "\\'");
                if (itemId != '' && itemId != undefined && itemId != null) {
                    $.ajax({
                        type: 'post', url: '/SG50/ajax/ajax_content.aspx/VoteForExistingItem', data: "{'name':'" + name + "','eMail':'" + eMail + "','description':'" + description + "','parentItemId':'" + itemId + "'}", contentType: 'application/json; charset=utf-8', dataType: 'json', success: function (data) {
                            if (data.d != null && data.d != '') {
                                if (parseInt(data.d) == 1) {
                                    $("input[type=submit]").attr("disabled", "disabled"); $('.VoteForItem_popup').removeClass('productactive'); $('.vus_popup').addClass('thankyouactive');
                                } else { alert(catchMsg); return false; }
                            }
                        }, failure: function () { alert(failure); return false; }, error: function (err) { alert(err.message); return false; }
                    })
                } else { return false; }
            } catch (msg) { alert(catchMsg); return false; }
        }

        /* function to add new item to the gallery of items */
        function SuggestItem() {
        var name=$("#name").val().replace(/'/g, "\\'");
        var email=$('#email').val().replace(/'/g, "\\'");
        var itemName=$('#name_item').val().replace(/'/g, '');
        var itemDesc=$('#your_story').val().replace(/'/g, "\\'");
            $.ajax({
                type: 'post', url: '/SG50/ajax/ajax_content.aspx/SuggestNewItem', data: "{'personName':'" + name + "', 'eMail':'" +  email+ "','itemName':'" + itemName + "','description':'" + itemDesc + "'}", contentType: 'application/json; charset=utf-8', dataType: 'json', success: function (data) {
                    if (data.d != null && data.d != '') {
                        if (parseInt(data.d) == 1) {
                            var dtop = parseInt($(window).scrollTop()) - 55;
                            $("input[type=submit]").attr("disabled", "disabled"); $('.suggestAnItem_popup').removeClass('productactive'); $('.SuggestThankYou_popup').addClass('thankyouactive').css('top', dtop);
                        } else { alert(catchMsg); return false; }
                    }
                }, failure: function () { alert(failure); return false; }, error: function (err) { alert(err.responseText + " " + err.statusCode); return false; }
            })
        }
        var formValid = false;

        /* Suggest New gallery Item */
        function SuggestNewItem(btnId) {
            //if ($(btnId).length > 0) {
            //    $(btnId).click(function () {
            //        $("#suggestanitemform").validate({
            //            ignore: [], rules: {
            //                'email': { required: true, email: true }, 'name': { required: true, maxlength: 100 }, 'name_item': { required: true, maxlength: 20 }, /*'your_story': { required: true, maxlength: 100 }*/
            //            },
            //            errorplacement: function (error, element) { error.insertBefore(element); },
            //            messages: {
            //                'email': { required: "Please enter your email address", email: "Please enter a valid email address" },
            //                'name': { required: "Please enter your name", maxlength: "your name must not be exceeds 100 characters long" },
            //                'name_item': { required: "Please enter name of item", maxlength: "name of the item must not be exceeds 20 characters long" },
            //                'your_story': { required: "Please enter information", maxlength: "information must not be exceeds 500 characters long" }
            //            },
            //            submitHandler: function (suggestanitemform) { }
            //        });
            //    });
            //} else { return false; }
        }

        /*Add New item to Gallery of Items in button click - > SuggestNewItem('#btnSuggestNewItem')*/

        /* Check Length Validation */
        function CheckLength(ctrlId, maxLength, formItemIndex) {
            if ($(ctrlId).length > 0) {
                $(ctrlId).keyup(function () {
                    var formCtrl = $('#suggestanitemform .formItem:eq(' + formItemIndex + ')'); formCtrl.find('span.errortxt').hide();
                    if ($(this).val().length > 0) {
                        formCtrl.find('span.errortxt:eq(0)').hide(); formCtrl.find('input[type=text]').removeClass('error');
                        if ($(this).val().length > maxLength) {
                            formCtrl.find('span.errortxt:eq(1)').show(); formCtrl.find('input[type=text]').addClass('error');
                        } else {
                            formCtrl.find('span.errortxt:eq(1)').hide(); formCtrl.find('input[type=text]').removeClass('error');
                        }
                    } else {
                        formCtrl.find('span.errortxt:eq(0)').show(); formCtrl.find('input[type=text]').addClass('error');
                    }
                });
            } else { return false; }
        }

        CheckLength('#name', 100, 0); CheckLength('#name_item', 20, 3); CheckEMail('#email', 1);

        /* Check E - Mail Validation */
        function CheckEMail(ctrlId, Index) {
            $(ctrlId).keyup(function () {
                var formCtrl = $('#suggestanitemform .formItem:eq(' + Index + ')'); formCtrl.find('span.errortxt').hide();
                if ($(this).val().length > 0) {
                    formCtrl.find('span.errortxt:eq(0)').hide();
                    formCtrl.find('input[type=text]').removeClass('error');
                    if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test($(this).val())) {
                        formCtrl.find('span.errortxt:eq(1)').hide(); formCtrl.find('input[type=text]').removeClass('error');
                    } else {
                        formCtrl.find('span.errortxt:eq(1)').show(); formCtrl.find('input[type=text]').addClass('error');
                    }
                } else {
                    formCtrl.find('span.errortxt:eq(0)').show(); formCtrl.find('input[type=text]').addClass('error');
                }
            });
        }


        var isValidate = [];
        /* Validate the suggest item form */
        function ValidateSuggestItemForm(divId) {
            var indexes = [0, 1, 3]; isValidate = [];
            for (var index = 0; index < indexes.length; index++) {
                var formCtrl = $(divId).find('.formItem:eq(' + indexes[index] + ')');
                if (formCtrl.find('input[type=text]').val() != '') {
                    formCtrl.find('span.errortxt:first').hide(); formCtrl.find('input[type=text]').removeClass('error'); isValidate.push(true);
                } else { formCtrl.find('span.errortxt:first').show(); formCtrl.find('input[type=text]').addClass('error'); isValidate.push(false); }
            }
        }

        /* Validate and add new item */
        $('#btnSuggestNewItem').click(function () {
            ValidateSuggestItemForm('#suggestanitemform');
            if (jQuery.inArray(false, isValidate) < 0) {
                $('#suggestanitemform').find('.formItem:eq(1)').find('input[type=text]').removeClass('error');
                if ($('#name').val().length <= 100 && $('#name_item').val().length <= 20 && /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test($('#email').val())) {
                    $('#suggestanitemform').find('.formItem:eq(1)').find('input[type=text]').removeClass('error'); SuggestItem();
                } else { $('#suggestanitemform').find('.formItem:eq(1)').find('input[type=text]').addClass('error'); return false; }
            } else { $('#suggestanitemform').find('.formItem:eq(1)').find('input[type=text]').addClass('error'); return false; }
        });
    })

</script>

<style type="text/css">
    .filterSection li a, .LoadMoreBtn
    {
        cursor: pointer;
    }
</style>
</html>
