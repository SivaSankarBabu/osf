﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using Sitecore.Data.Fields;

namespace Layouts.Acknowledgementsublayout
{

    /// <summary>
    /// Summary description for AcknowledgementsublayoutSublayout
    /// </summary>
    public partial class AcknowledgementsublayoutSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillGoldPartners();
                FillBronzePartners();
                FillSupporter();
                FillSliverPartners();
            }
            // Put user code to initialize the page here
        }

        private void FillGoldPartners()
        {
            try
            {
                DataTable dtGoldPartners = new DataTable();
                dtGoldPartners.Columns.Add("item", typeof(Item));
                dtGoldPartners.Columns.Add("imgSrc", typeof(string));
                dtGoldPartners.Columns.Add("href", typeof(string));
                DataRow drGoldPartners;


                //Item itmGoldPartners = web.GetItem("{4A9FFA72-A23F-4E1A-AE04-C408486E0914}");
                //Sitecore.Collections.ChildList GoldPartnerschild = itmGoldPartners.GetChildren();

                Item itmPress = SG50Class.web.GetItem("{4A9FFA72-A23F-4E1A-AE04-C408486E0914}");

                if (itmPress != null)
                {

                    IEnumerable<Item> GoldPartnerschild = from a in itmPress.Axes.GetDescendants()
                                                          where
                                                          a.TemplateID.ToString().Equals("{400CBFBD-E3E2-4263-A0C7-D6A5735A2200}")
                                                          orderby
                                                          a.Name
                                                          select a;

                    foreach (Item a in GoldPartnerschild)
                    {
                        drGoldPartners = dtGoldPartners.NewRow();
                        drGoldPartners["item"] = a;
                        Sitecore.Data.Fields.LinkField lnk = a.Fields["Link"];
                        drGoldPartners["href"] = lnk.Url;
                        Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                        if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
                        {
                            drGoldPartners["imgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mainImgField.MediaItem);
                        }
                        dtGoldPartners.Rows.Add(drGoldPartners);
                    }
                    repGoldPartners.DataSource = dtGoldPartners;
                    repGoldPartners.DataBind();
                }
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }

        private void FillBronzePartners()
        {
            try
            {
                DataTable dtBronzePartners = new DataTable();
                dtBronzePartners.Columns.Add("item", typeof(Item));
                dtBronzePartners.Columns.Add("imgSrc", typeof(string));
                dtBronzePartners.Columns.Add("href", typeof(string));
                DataRow drBronzePartners;
                //Item itmBronzePartners = web.GetItem("{B82D0440-1E15-4529-8997-9FF8A36E44C9}");
                //Sitecore.Collections.ChildList BronzePartnerschild = itmBronzePartners.GetChildren();
                Item itmPress = SG50Class.web.GetItem("{B82D0440-1E15-4529-8997-9FF8A36E44C9}");

                if (itmPress != null)
                {
                    IEnumerable<Item> BronzePartnerschild = from a in itmPress.Axes.GetDescendants()
                                                            where
                                                            a.TemplateID.ToString().Equals("{400CBFBD-E3E2-4263-A0C7-D6A5735A2200}")
                                                            orderby
                                                            a.Name
                                                            select a;
                    foreach (Item a in BronzePartnerschild)
                    {
                        drBronzePartners = dtBronzePartners.NewRow();
                        drBronzePartners["item"] = a;
                        Sitecore.Data.Fields.LinkField lnk = a.Fields["Link"];
                        drBronzePartners["href"] = lnk.Url;
                        Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                        if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
                        {
                            drBronzePartners["imgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mainImgField.MediaItem);
                        }
                        dtBronzePartners.Rows.Add(drBronzePartners);
                    }
                    repBronzePartners.DataSource = dtBronzePartners;
                    repBronzePartners.DataBind();
                }
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }

        private void FillSupporter()
        {
            try
            {
                DataTable dtSupporter = new DataTable();
                dtSupporter.Columns.Add("item", typeof(Item));
                dtSupporter.Columns.Add("imgSrc", typeof(string));
                dtSupporter.Columns.Add("href", typeof(string));
                DataRow drSupporter;


                Item itmPress = SG50Class.web.GetItem("{1BFC7F3B-C474-4431-8E00-0E8124DBA7EB}");

                if (itmPress != null)
                {
                    IEnumerable<Item> Supporterchild = from a in itmPress.Axes.GetDescendants()
                                                       where
                                                       a.TemplateID.ToString().Equals("{400CBFBD-E3E2-4263-A0C7-D6A5735A2200}")
                                                       orderby
                                                       a.Name
                                                       select a;

                    //Item itmSupporter = web.GetItem("{1BFC7F3B-C474-4431-8E00-0E8124DBA7EB}");
                    // Sitecore.Collections.ChildList Supporterchild = itmSupporter.GetChildren();
                    foreach (Item a in Supporterchild)
                    {
                        drSupporter = dtSupporter.NewRow();
                        drSupporter["item"] = a;
                        Sitecore.Data.Fields.LinkField lnk = a.Fields["Link"];
                        drSupporter["href"] = lnk.Url;
                        Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                        if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
                        {
                            drSupporter["imgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mainImgField.MediaItem);
                        }
                        dtSupporter.Rows.Add(drSupporter);
                    }
                    repSupporter.DataSource = dtSupporter;
                    repSupporter.DataBind();
                }
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }
        private void FillSliverPartners()
        {
            try
            {
                DataTable dtSupporter = new DataTable();
                dtSupporter.Columns.Add("item", typeof(Item));
                dtSupporter.Columns.Add("imgSrc", typeof(string));
                dtSupporter.Columns.Add("href", typeof(string));
                DataRow drSupporter;


                Item itmPress = SG50Class.web.GetItem("{04CCD1C7-6762-4866-AE78-43633FCA2211}");

                if (itmPress != null)
                {

                    IEnumerable<Item> Supporterchild = from a in itmPress.Axes.GetDescendants()
                                                       where
                                                       a.TemplateID.ToString().Equals("{400CBFBD-E3E2-4263-A0C7-D6A5735A2200}")
                                                       orderby
                                                       a.Name
                                                       select a;

                    //Item itmSupporter = web.GetItem("{1BFC7F3B-C474-4431-8E00-0E8124DBA7EB}");
                    // Sitecore.Collections.ChildList Supporterchild = itmSupporter.GetChildren();
                    foreach (Item a in Supporterchild)
                    {
                        drSupporter = dtSupporter.NewRow();
                        drSupporter["item"] = a;
                        Sitecore.Data.Fields.LinkField lnk = a.Fields["Link"];
                        drSupporter["href"] = lnk.Url;
                        Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                        if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
                        {
                            drSupporter["imgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mainImgField.MediaItem);
                        }
                        dtSupporter.Rows.Add(drSupporter);
                    }
                    repSliverPartners.DataSource = dtSupporter;
                    repSliverPartners.DataBind();
                }
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }
    }
}