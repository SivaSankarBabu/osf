﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Projectdetails.ProjectdetailsSublayout" CodeFile="~/SG50/layouts/ProjectDetails.ascx.cs" %>
<link href="/SG50/include/Css/style.css" rel="stylesheet" />
<link href="/SG50/include/Css/responsive_Grid.css" rel="stylesheet" />
<style type="text/css">
    .pressBannerCaption a {
        border: 2px solid #fff;
        color: #fff;
        margin: 0 7px;
        padding: 10px 25px;
    }

        .pressBannerCaption a:hover {
            border: 2px solid #fff;
            background: rgba(255, 255, 255, 0.2);
        }

    #oneColumn {
        width: 100%;
        margin: 1% auto;
    }

        #oneColumn img {
            width: 100%;
        }

        #oneColumn div {
            width: 100%;
            background: #CCC;
        }

    #twoColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }

        #twoColumn img {
            width: 100%;
        }

        #twoColumn div {
            width: 49%;
            margin: 0 1% 0 0;
            background: #CCC;
            float: left;
        }

            #twoColumn div + div {
                width: 49%;
                margin: 0 0 0 1%;
                background: #CCC;
                float: left;
            }

    #threColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }

        #threColumn img {
            width: 100%;
        }

        #threColumn div {
            width: 72%;
            margin: 0 1% 0 0;
            background: #CCC;
            float: left;
        }

            #threColumn div + div, #threColumn div + div + div {
                width: 25%;
                margin: 0 0 0 1%;
                background: #CCC;
                float: left;
                ;
            }

                #threColumn div + div + div {
                    margin-top: 2%;
                }

    #fourColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }

        #fourColumn img {
            width: 100%;
        }

        #fourColumn div, #fourColumn div + div + div {
            width: 49%;
            margin: 0 1% 0 0;
            background: #CCC;
            float: left;
        }

            #fourColumn div + div, #fourColumn div + div + div + div {
                margin: 0 0 0 1%;
            }

                #fourColumn div + div + div, #fourColumn div + div + div + div {
                    margin-top: 2%;
                }

    #fiveColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }

        #fiveColumn img {
            width: 100%;
        }

        #fiveColumn div, #fiveColumn div + div + div {
            width: 49%;
            margin: 0 1% 0 0;
            background: #CCC;
            float: left;
        }

            #fiveColumn div + div, #fiveColumn div + div + div + div {
                margin: 0 0 0 1%;
            }

                #fiveColumn div + div + div, #fiveColumn div + div + div + div {
                    margin-top: 2%;
                    margin-bottom: 2%;
                }

                    #fiveColumn div + div + div + div + div {
                        float: inherit;
                        margin: 0 auto;
                        width: 49%;
                        display: flex;
                    }

    #sixColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }

        #sixColumn img {
            width: 100%;
        }

        #sixColumn div, #sixColumn div + div + div + div {
            width: 64%;
            margin: 0 1% 0 0;
            background: #CCC;
            float: left;
        }

            #sixColumn div + div, #sixColumn div + div + div, #sixColumn div + div + div + div + div, #sixColumn div + div + div + div + div + div {
                width: 30%;
                margin: 0 0 0 1%;
                background: #CCC;
                float: left;
            }

            #sixColumn div + div {
                margin-bottom: 2%;
            }

                #sixColumn div + div + div + div, #sixColumn div + div + div + div + div, #sixColumn div + div + div + div + div + div {
                    margin-top: 2%;
                }

    .video-player {
        height: 0;
        overflow: hidden;
        padding-bottom: 50%;
        padding-top: 30px;
        position: relative;
    }

        .video-player iframe, .video-player object, .video-player embed {
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%;
        }
</style>

<p>
    <a onclick='postToFeed();'></a>
</p>
<p id='msg'>
</p>

<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...


        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            // caption: fbdes,
            description: fbdes
        };
        FB.ui(obj);

    }

    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken; //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function (response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }

</script>

<section class="main-content">
    <!---main-content start-------->
    <section class="grid-content">
        <!---grid-content start-------->
        <div class="head-title">
            <div class="project-title">
                <a href="/SG50/Celebration Fund and Ideas/Ground-Up Projects.aspx" class="top-btn">back to list</a>
                <h1>
                    <sc:Text ID="txtTitle" Field="Title" runat="server" />
                </h1>
                <a href="/SG50/Celebration Fund and Ideas/Ground-Up Projects.aspx" class="bottom-btn">back to list</a>

            </div>
            <p>
                <sc:FieldRenderer ID="txtShortDescription" runat="server" FieldName="Short Description" />
            </p>
        </div>

    </section>
    <div class="" id="DIVVideo" runat="server" visible="false">
        <div id="dvVideo" runat="server" style="text-align: center">
            <div class="video-player">
                <a id="refVideo" href=''>
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <iframe src="<%#Eval("href")%>" frameborder="0"></iframe>
                        </ItemTemplate>
                    </asp:Repeater>

                </a>
            </div>
        </div>
    </div>
    <!---grid-content ends-------->
    <section class="grid-content">
        <!---grid-content start-------->

        <div class="grid-container">


            <div id="ImageCount" runat="server">
            </div>
            <div class="head-title ">
                <div class="project-title">

                    <h1>
                        <sc:FieldRenderer ID="txtInterviewTitle" runat="server" FieldName="Interview_Title" />
                    </h1>
                </div>
                <p class="caps-font">
                    <sc:FieldRenderer ID="txtInterviewContent" runat="server" FieldName="Interview_Content" />
                </p>
                <span class="auth-name">
                    <sc:Text ID="txtCreatedBy" Field="By Whom" runat="server" />
                </span>
            </div>
            <div class="question-container">
                <sc:FieldRenderer ID="txtQuestion" runat="server" FieldName="Interview_Story" />
            </div>
            <div class="social-contianer">
                <asp:Repeater ID="repsocialSharing" runat="server">
                    <ItemTemplate>
                        <div>
                            <ul>
                                <li><a href="JavaScript:postToFeed('<%# Eval("FaceBookTitle").ToString()%>','<%# Eval("FaceBookContent").ToString()%>','<%# Eval("FaceBookThumbNail")%>','<%# Eval("EventUrl")%>')">
                                    <img src="/SG50/images/facebook.png" /></a>

                                </li>
                                <li><a href="JavaScript:newPopup('<%# Eval("TwitterContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')">
                                    <img src="/SG50/images/twitter.png" /></a>

                                </li>

                            </ul>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div>
                    <asp:Button ID="btnPrevious" runat="server" Text="Previous" OnClick="btnPrevious_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" />
                </div>
            </div>


        </div>


    </section>
    <div class="press">
        <div class="pressBanner">
            <div id="pressBannerInner">
                <div class="pressBannerImage desktop">
                    <sc:Image ID="Image1" runat="server" Field="Banner Image" DataSource="{62A20A5B-8FC8-40E2-AB05-84C94AE82C85}" />
                </div>
                <div class="pressBannerCaption desktop">
                    <h1>TURN YOUR IDEAS FOR SINGAPORE'S 50TH INTO A REALITY</h1>
                    <%--<a href="/sg50/Apply For Funding.aspx">APPLY FOR FUNDING</a>--%>
                </div>
                <div class="pressBannerCaption mobile">
                    <h1>MARK YOUR CALENDARS AND PENCIL US IN.</h1>
                   <%-- <a class="banner-btn" href="/sg50/Apply For Funding.aspx">APPLY FOR FUNDING</a>--%>
                </div>
            </div>
        </div>
    </div>
    <!---grid-content ends-------->
</section>
<!---main-content end-------->
