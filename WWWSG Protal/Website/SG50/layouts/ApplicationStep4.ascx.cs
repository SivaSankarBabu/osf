﻿using System;

namespace Layouts.Applicationstep4 {
  
	/// <summary>
	/// Summary description for Applicationstep4Sublayout
	/// </summary>
  public partial class Applicationstep4Sublayout : System.Web.UI.UserControl 
	{
    private void Page_Load(object sender, EventArgs e) {
        Session["Step2"] = null;
        Session["Step3"] = null;
        Session["SEA"] = null;
        Session["Local"] = null;
        if (Session["ApplicationName"] != null)
        {
            AppId.Text = Session["ApplicationName"].ToString(); 
        }
        Session["ApplicationName"] = null;
      // Put user code to initialize the page here
    }
  }
}