﻿using System;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.IO;
using System.Text;
using Sitecore.Collections;
using Sitecore.Resources.Media;
using Sitecore;
using System.Text.RegularExpressions;
using System.Configuration;
namespace Layouts.Projectdetails
{

    /// <summary>
    /// Summary description for ProjectdetailsSublayout
    /// </summary>
    public partial class ProjectdetailsSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmContext = Sitecore.Context.Item;

        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                Sitecore.Collections.ChildList ccc = web.GetItem("{DEFF9D32-481F-454D-A125-E0E46131822B}").GetChildren();
                string curntItem = Sitecore.Context.Item.ID.ToString();

                string path = Sitecore.Context.Item.Paths.Path.ToString();
                Item ImageItem = web.GetItem(path + "/Images");
                ChildList ImagesCount = null;
                if (ImageItem != null && ImageItem.Versions.Count > 0)
                {


                    ImagesCount = ImageItem.GetChildren();
                    string[] stringArray = new string[ImagesCount.Count];
                    int availableimage = 0;
                    for (int i = 0; i < ImagesCount.Count; i++)
                    {
                        Sitecore.Data.Fields.ImageField image = ImagesCount[i].Fields["Image"];

                        if (image.MediaItem != null)
                        {
                            MediaUrlOptions options = new MediaUrlOptions();
                            options.BackgroundColor = System.Drawing.Color.White;
                            string url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                            stringArray[i] = url;
                            availableimage = availableimage + 1;
                        }
                    }


                    StringBuilder sbBannerText = new StringBuilder();
                    if (availableimage == 1)
                    {
                        // Response.Write("one");
                        sbBannerText.Append("<div id='oneColumn'> <div> <img src='" + stringArray[0] + "'/></div> </div>");

                    }
                    else if (availableimage == 2)
                    {

                        sbBannerText.Append("<div id='twoColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append("<div> <img src='" + stringArray[1] + "'/></div>");

                    }
                    else if (availableimage == 3)
                    {
                        // Response.Write("three");
                        sbBannerText.Append("<div id='threColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div> </div>");
                    }
                    else if (availableimage == 4)
                    {
                        // Response.Write("four");
                        sbBannerText.Append("<div id='fourColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[3] + "'/></div> </div>");
                    }
                    else if (availableimage == 5)
                    {
                        // Response.Write("five");
                        sbBannerText.Append("<div id='fiveColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[3] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[4] + "'/></div> </div>");
                    }
                    else if (availableimage == 6)
                    {
                        // Response.Write("six");
                        sbBannerText.Append("<div id='sixColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[3] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[4] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[5] + "'/></div> </div>");
                    }


                    ImageCount.InnerHtml = sbBannerText.ToString();


                }


                if (ccc[0].ID.ToString() == curntItem)
                {
                    btnPrevious.Enabled = false;
                    btnPrevious.CssClass = "deactive";
                    //CssClass="deactive"
                }
                if (ccc[ccc.Count - 1].ID.ToString() == curntItem)
                {
                    btnNext.Enabled = false;
                    btnNext.CssClass = "deactive";
                }
                if (Microsoft.Security.Application.Encoder.HtmlEncode(Request.QueryString["buttons"]) != null)
                {
                    btnPrevious.Visible = false;
                    btnNext.Visible = false;
                }
                SocialSharing();

            }

            VideoBinding();
        }
        public void VideoBinding()
        {

            DataTable dtMonthlyHiglights = new DataTable();
            dtMonthlyHiglights.Columns.Add("href", typeof(string));
            DataRow drMonthlyHiglights;

            drMonthlyHiglights = dtMonthlyHiglights.NewRow();
            drMonthlyHiglights["href"] = Sitecore.Context.Item["Video Link1"].ToString();
            dtMonthlyHiglights.Rows.Add(drMonthlyHiglights);




            if (!string.IsNullOrEmpty(Sitecore.Context.Item["Video Link1"].ToString()) && !string.IsNullOrWhiteSpace(Sitecore.Context.Item["Video Link1"].ToString()))
            {
                DIVVideo.Visible = true;
            }

            if (dtMonthlyHiglights.Rows.Count > 0)
            {

                Repeater1.DataSource = dtMonthlyHiglights;
                Repeater1.DataBind();
            }
            else
            {
                DIVVideo.Visible = false;
            }

        }
        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Sitecore.Collections.ChildList ccc = web.GetItem("{DEFF9D32-481F-454D-A125-E0E46131822B}").GetChildren();
                string curntItem = Sitecore.Context.Item.ID.ToString();

                for (int i = 0; i < ccc.Count; i++)
                {
                    if (ccc[i].ID.ToString() == curntItem)
                    {
                        string url = Sitecore.Links.LinkManager.ExpandDynamicLinks(Sitecore.Links.LinkManager.GetItemUrl(ccc[i - 1]));
                        if (!(url.Contains("://")))
                            Response.Redirect(url);
                    }
                }
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Sitecore.Collections.ChildList ccc = web.GetItem("{DEFF9D32-481F-454D-A125-E0E46131822B}").GetChildren();
                string curntItem = Sitecore.Context.Item.ID.ToString();

                for (int i = 0; i < ccc.Count; i++)
                {
                    if (ccc[i].ID.ToString() == curntItem)
                    {
                        string url = Sitecore.Links.LinkManager.ExpandDynamicLinks(Sitecore.Links.LinkManager.GetItemUrl(ccc[i + 1]));
                        if (!(url.Contains("://")))
                            Response.Redirect(url);
                    }
                }
            }
        }
        private void SocialSharing()
        {
            string noHTML = string.Empty;
            string inputHTML = string.Empty;
            DataTable dtrepEvents = new DataTable();
            dtrepEvents.Columns.Add("Id", typeof(string));
            dtrepEvents.Columns.Add("FaceBookTitle", typeof(string));
            dtrepEvents.Columns.Add("FaceBookContent", typeof(string));
            dtrepEvents.Columns.Add("FaceBookThumbnail", typeof(string));
            dtrepEvents.Columns.Add("TwitterTitle", typeof(string));
            dtrepEvents.Columns.Add("TwitterContent", typeof(string));
            dtrepEvents.Columns.Add("href", typeof(string));
            dtrepEvents.Columns.Add("HostName", typeof(string));
            dtrepEvents.Columns.Add("AccessToken", typeof(string));
            dtrepEvents.Columns.Add("EventUrl", typeof(string));
            DataRow drrepEvents;
            try
            {
                drrepEvents = dtrepEvents.NewRow();
                drrepEvents["HostName"] = hostName + "/";
                drrepEvents["AccessToken"] = accessToken;
                drrepEvents["href"] = LinkManager.GetItemUrl(itmContext).ToString();
                drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(itmContext).ToString();
                if (!string.IsNullOrEmpty(itmContext.Fields["Facebook Title"].ToString()))
                {
                    drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["Facebook Title"].ToString(), CommonMethods.faceBook);
                }
                else
                {
                    drrepEvents["FaceBookTitle"] =cmObj.BuildString(itmContext.Fields["Title"].ToString(), CommonMethods.faceBook);
                }

                if (!string.IsNullOrEmpty(itmContext.Fields["Facebook Content"].ToString()))
                {

                    drrepEvents["FaceBookContent"] = cmObj.BuildString(itmContext.Fields["Facebook Content"].ToString(), CommonMethods.faceBook);//+ " " + hostName + LinkManager.GetItemUrl(itmContext);
                }
                else
                {
                    inputHTML = Sitecore.Web.UI.WebControls.FieldRenderer.Render(itmContext, "Short Description"); //itmContext.Fields["Short Description"].ToString();
                    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                    drrepEvents["FaceBookContent"] = cmObj.BuildString(noHTML, CommonMethods.faceBook);//+ " " + hostName + LinkManager.GetItemUrl(itmContext);
                }

                Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Facebook Image"]);
                string fbimgSrc = fbimgField.Src;
                if (!fbimgSrc.Equals(""))
                {
                    drrepEvents["FaceBookThumbNail"] = hostName + "/" + fbimgSrc;
                }
                else
                {
                    Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Thumb Image"]);
                    string staticimgSrc = staticimgField.Src;
                    if (!string.IsNullOrEmpty(staticimgSrc))
                    {
                        drrepEvents["FaceBookThumbNail"] = hostName + "/" + staticimgField.Src;
                    }
                    else
                    {
                        drrepEvents["FaceBookThumbNail"] = "";
                    }

                }

                if (!string.IsNullOrEmpty(itmContext.Fields["Twitter Title"].ToString()))
                {
                    drrepEvents["TwitterTitle"] = itmContext.Fields["Twitter Title"].ToString();
                }
                else
                {
                    drrepEvents["TwitterTitle"] = itmContext.Fields["Title"].ToString();
                }

                if (!string.IsNullOrEmpty(itmContext.Fields["Twitter Content"].ToString()))
                {
                    drrepEvents["TwitterContent"] = cmObj.BuildString(itmContext.Fields["Twitter Content"].ToString(), CommonMethods.faceBook);
                }
                else
                {
                    inputHTML = Sitecore.Web.UI.WebControls.FieldRenderer.Render(itmContext, "Short Description");// itmContext.Fields["Short Description"].ToString();
                    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                    if (noHTML.Length > 120)
                        drrepEvents["TwitterContent"] = cmObj.BuildString(noHTML.Substring(0, 119), CommonMethods.faceBook);
                    else
                        drrepEvents["TwitterContent"] = cmObj.BuildString(noHTML.Substring(0, noHTML.Length), CommonMethods.faceBook);

                }

                drrepEvents["Id"] = itmContext.ID;
                dtrepEvents.Rows.Add(drrepEvents);
                repsocialSharing.DataSource = dtrepEvents;
                repsocialSharing.DataBind();

            }
            catch (Exception exSub1)
            {
                // Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }
    }
}