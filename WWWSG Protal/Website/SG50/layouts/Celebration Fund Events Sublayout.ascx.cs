﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
namespace Layouts.Celebration_fund_events_sublayout
{

    /// <summary>
    /// Summary description for Celebration_fund_events_sublayoutSublayout
    /// </summary>
    public partial class Celebration_fund_events_sublayoutSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        int latestCount = 5;

        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                FillrepUpcomingEvents();
            }

        }

        private void FillrepUpcomingEvents()
        {
            try
            {
                DataTable dtrepEvents = new DataTable();
                dtrepEvents.Columns.Add("cssClass", typeof(string));
                dtrepEvents.Columns.Add("item", typeof(Item));
                dtrepEvents.Columns.Add("href", typeof(string));
                dtrepEvents.Columns.Add("imgSrc", typeof(string));
                dtrepEvents.Columns.Add("Id", typeof(string));
                dtrepEvents.Columns.Add("videoURL", typeof(string));
                dtrepEvents.Columns.Add("FacebookTitle", typeof(string));
                dtrepEvents.Columns.Add("FacebookContent", typeof(string));
                dtrepEvents.Columns.Add("FacebookImage", typeof(string));
                dtrepEvents.Columns.Add("TwitterTitle", typeof(string));
                dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                dtrepEvents.Columns.Add("title", typeof(string));
                dtrepEvents.Columns.Add("ShorttDescription", typeof(string));
                dtrepEvents.Columns.Add("Count", typeof(string));
                dtrepEvents.Columns.Add("HostName", typeof(string));
                dtrepEvents.Columns.Add("AccessToken", typeof(string));
                dtrepEvents.Columns.Add("EventUrl", typeof(string));

                DataRow drrepEvents;

                Item itmPress = SG50Class.web.GetItem(SG50Class.str_Celebrations_Funds_Item_ID);

                IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                          where
                                                          a.TemplateID.ToString().Equals(SG50Class.str_CelebrationFund_Details_Page_Template_ID)
                                                          select a;
                int count = 0;
                foreach (Item a in itmIEnumPressReleases)
                {
                    try
                    {

                        drrepEvents = dtrepEvents.NewRow();
                        drrepEvents["HostName"] = hostName + "/";
                        drrepEvents["AccessToken"] = accessToken;
                        drrepEvents["cssClass"] = "a";
                        drrepEvents["item"] = a;
                        drrepEvents["href"] = LinkManager.GetItemUrl(a).ToString();
                        drrepEvents["Count"] = count + 1;
                        drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(a).ToString();
                        string FacebookTitle = string.Empty;
                        string FacebookContent = string.Empty;
                        string TwitterContent = string.Empty;
                        string TwitterTitle = string.Empty;
                        string videourl = string.Empty;
                        string noHTML = string.Empty;
                        string inputHTML = string.Empty;
                        if (!string.IsNullOrEmpty(a.Fields["Facebook Title"].ToString()))
                            FacebookTitle = a.Fields["Facebook Title"].ToString();

                        else
                            FacebookTitle = a.Fields["Title"].ToString();

                        if (!string.IsNullOrEmpty(a.Fields["Facebook Content"].ToString()))
                            FacebookContent = a.Fields["Facebook Content"].ToString();// + " " + hostName + LinkManager.GetItemUrl(a);
                        else
                        {
                            inputHTML = a.Fields["Short Description"].ToString();
                            noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                            FacebookContent = noHTML; //+ " " + hostName + LinkManager.GetItemUrl(a);
                        }

                        if (!string.IsNullOrEmpty(a.Fields["Twitter Title"].ToString()))
                            TwitterTitle = a.Fields["Twitter Title"].ToString();
                        else
                            TwitterTitle = a.Fields["Title"].ToString();


                        if (!string.IsNullOrEmpty(a.Fields["Twitter Content"].ToString()))
                            TwitterContent = a.Fields["Twitter Content"].ToString();
                        else
                        {
                            inputHTML = a.Fields["Short Description"].ToString();
                            noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                            if (noHTML.Length > 120)
                                TwitterContent = noHTML.Substring(0, 119);
                            else
                                TwitterContent = noHTML.Substring(0, noHTML.Length);
                        }


                        drrepEvents["FacebookTitle"] = cmObj.BuildString(FacebookTitle, CommonMethods.faceBook);
                        drrepEvents["FacebookContent"] = cmObj.BuildString(FacebookContent, CommonMethods.faceBook);
                        drrepEvents["TwitterContent"] = cmObj.BuildString(TwitterContent, CommonMethods.faceBook);
                        drrepEvents["TwitterTitle"] = TwitterTitle;

                        Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Facebook Image"]);
                        string fbimgSrc = fbimgField.Src;
                        if (!string.IsNullOrEmpty(fbimgSrc))
                        {
                            drrepEvents["FacebookImage"] = hostName + "/" + fbimgSrc;
                        }
                        else
                        {
                            Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                            string staticimgSrc = staticimgField.Src;
                            if (!string.IsNullOrEmpty(staticimgSrc))
                            {
                                drrepEvents["FacebookImage"] = hostName + "/" + staticimgField.Src;
                            }
                            else
                            {
                                drrepEvents["FacebookImage"] = "";
                            }

                        }


                        drrepEvents["videoURL"] = a.Fields["Video Link1"].ToString();


                        Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                        string imgSrc = imgField.Src;

                        Sitecore.Data.Fields.ImageField VideoimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Video Image1"]);
                        string VideoImgsrc = VideoimgField.Src;

                        if (a.Fields["Thumb Image"] != null && !a["Thumb Image"].ToString().Equals(""))
                        {
                            drrepEvents["imgSrc"] = hostName + "/" + imgSrc;
                        }
                        else if (a.Fields["Video Image1"] != null && !a["Video Image1"].ToString().Equals(""))
                        {
                            drrepEvents["imgSrc"] = hostName + "/" + VideoImgsrc;
                        }
                        drrepEvents["Id"] = a.ID;
                        dtrepEvents.Rows.Add(drrepEvents);
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
                    }
                }

                repUpcomingEvents.Visible = true;
                repUpcomingEvents.DataSource = dtrepEvents;
                repUpcomingEvents.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
            }
        }
        public void DownloadCalendar(object sender, EventArgs e)
        {
            try
            {
                string str = "";
                Sitecore.Data.Fields.FileField objcalendar = itmcontext.Fields["Calendar"];
                if (!string.IsNullOrEmpty(objcalendar.Src.ToString()))
                {
                    str = objcalendar.MediaItem.ID.ToString();
                    if (str.Length > 0)
                    {
                        MediaItem mi = web.GetItem(str);
                        if (mi != null)
                        {
                            Stream stream = mi.GetMediaStream();
                            long fileSize = stream.Length;
                            byte[] buffer = new byte[(int)fileSize];
                            stream.Read(buffer, 0, (int)stream.Length);
                            stream.Close();
                            Response.ContentType = String.Format(mi.MimeType);
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + mi.Name + "." + mi.Extension);
                            Response.End();
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(buffer);
                            Response.End();
                        }
                        else { Response.Write("no mi"); }
                    }
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataRow ComFunList(DataRow drrepEvents, Item a, string buttons)
        {
            try
            {

                //drrepEvents = dtrepEvents.NewRow();
                drrepEvents["HostName"] = hostName + "/";
                drrepEvents["AccessToken"] = accessToken;
                drrepEvents["cssClass"] = "a";
                drrepEvents["item"] = a;
                if (buttons == "NO")
                    drrepEvents["href"] = LinkManager.GetItemUrl(a).ToString() + "?buttons=NO";
                else
                    drrepEvents["href"] = LinkManager.GetItemUrl(a).ToString();
                drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(a).ToString();
                string FacebookTitle = string.Empty;
                string FacebookContent = string.Empty;
                string TwitterContent = string.Empty;
                string TwitterTitle = string.Empty;
                string videourl = string.Empty;
                string noHTML = string.Empty;
                string inputHTML = string.Empty;
                if (!string.IsNullOrEmpty(a.Fields["Facebook Title"].ToString()))
                    FacebookTitle = a.Fields["Facebook Title"].ToString();

                else
                    FacebookTitle = a.Fields["Title"].ToString();

                if (!string.IsNullOrEmpty(a.Fields["Facebook Content"].ToString()))
                    FacebookContent = a.Fields["Facebook Content"].ToString(); //+ " " + hostName + LinkManager.GetItemUrl(a);
                else
                {
                    inputHTML = a.Fields["Short Description"].ToString();
                    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                    FacebookContent = noHTML; //+ " " + hostName + LinkManager.GetItemUrl(a);
                }

                if (!string.IsNullOrEmpty(a.Fields["Twitter Title"].ToString()))
                    TwitterTitle = a.Fields["Twitter Title"].ToString();
                else
                    TwitterTitle = a.Fields["Title"].ToString();


                if (!string.IsNullOrEmpty(a.Fields["Twitter Content"].ToString()))
                    TwitterContent = a.Fields["Twitter Content"].ToString();
                else
                {
                    inputHTML = a.Fields["Short Description"].ToString();
                    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                    if (noHTML.Length > 120)
                        TwitterContent = noHTML.Substring(0, 119);
                    else
                        TwitterContent = noHTML.Substring(0, noHTML.Length);
                }


                drrepEvents["FacebookTitle"] = cmObj.BuildString(FacebookTitle, CommonMethods.faceBook);
                drrepEvents["FacebookContent"] = cmObj.BuildString(FacebookContent, CommonMethods.faceBook);
                drrepEvents["TwitterContent"] = cmObj.BuildString(TwitterContent, CommonMethods.faceBook); ;
                drrepEvents["TwitterTitle"] = TwitterTitle;

                Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Facebook Image"]);
                string fbimgSrc = fbimgField.Src;
                if (!string.IsNullOrEmpty(fbimgSrc))
                {
                    drrepEvents["FacebookImage"] = hostName + "/" + fbimgSrc;
                }
                else
                {
                    Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                    string staticimgSrc = staticimgField.Src;
                    if (!string.IsNullOrEmpty(staticimgSrc))
                    {
                        drrepEvents["FacebookImage"] = hostName + "/" + staticimgField.Src;
                    }
                    else
                    {
                        drrepEvents["FacebookImage"] = "";
                    }

                }


                drrepEvents["videoURL"] = a.Fields["Video Link1"].ToString();

                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                string imgSrc = imgField.Src;

                Sitecore.Data.Fields.ImageField VideoimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Video Image1"]);
                string VideoImgsrc = VideoimgField.Src;

                if (a.Fields["Thumb Image"] != null && !a["Thumb Image"].ToString().Equals(""))
                {
                    drrepEvents["imgSrc"] = hostName + "/" + imgSrc;
                }
                else if (a.Fields["Video Image1"] != null && !a["Video Image1"].ToString().Equals(""))
                {
                    drrepEvents["imgSrc"] = hostName + "/" + VideoImgsrc;
                }
                drrepEvents["Id"] = a.ID;
                //dtrepEvents.Rows.Add(drrepEvents);
            }
            catch (Exception exSub1)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
            return drrepEvents;
        }
        protected void ddlFundedSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (ddlFundedSearch.SelectedValue == "0")
                {
                    try
                    {
                        DataTable dtrepEvents = new DataTable();
                        dtrepEvents.Columns.Add("cssClass", typeof(string));
                        dtrepEvents.Columns.Add("item", typeof(Item));
                        dtrepEvents.Columns.Add("href", typeof(string));
                        dtrepEvents.Columns.Add("imgSrc", typeof(string));
                        dtrepEvents.Columns.Add("Id", typeof(string));
                        dtrepEvents.Columns.Add("videoURL", typeof(string));
                        dtrepEvents.Columns.Add("FacebookTitle", typeof(string));
                        dtrepEvents.Columns.Add("FacebookContent", typeof(string));
                        dtrepEvents.Columns.Add("FacebookImage", typeof(string));
                        dtrepEvents.Columns.Add("TwitterTitle", typeof(string));
                        dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                        dtrepEvents.Columns.Add("title", typeof(string));
                        dtrepEvents.Columns.Add("ShorttDescription", typeof(string));
                        dtrepEvents.Columns.Add("HostName", typeof(string));
                        dtrepEvents.Columns.Add("AccessToken", typeof(string));
                        dtrepEvents.Columns.Add("EventUrl", typeof(string));
                        DataRow drrepEvents;

                        Item itmPress = SG50Class.web.GetItem(SG50Class.str_Celebrations_Funds_Item_ID);

                        IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                                  where
                                                                  a.TemplateID.ToString().Equals(SG50Class.str_CelebrationFund_Details_Page_Template_ID)
                                                                  orderby a.Statistics.Updated descending
                                                                  select a;
                        foreach (Item a in itmIEnumPressReleases)
                        {
                            drrepEvents = dtrepEvents.NewRow();
                            dtrepEvents.Rows.Add(ComFunList(drrepEvents, a, "YES"));
                        }
                        repUpcomingEvents.Visible = true;
                        repUpcomingEvents.DataSource = dtrepEvents;
                        repUpcomingEvents.DataBind();
                    }
                    catch (Exception exMain)
                    {
                        Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
                    }
                }
                else if (ddlFundedSearch.SelectedValue == "1")
                {
                    try
                    {
                        DataTable dtrepEvents = new DataTable();
                        dtrepEvents.Columns.Add("cssClass", typeof(string));
                        dtrepEvents.Columns.Add("item", typeof(Item));
                        dtrepEvents.Columns.Add("href", typeof(string));
                        dtrepEvents.Columns.Add("imgSrc", typeof(string));
                        dtrepEvents.Columns.Add("Id", typeof(string));
                        dtrepEvents.Columns.Add("videoURL", typeof(string));
                        dtrepEvents.Columns.Add("FacebookTitle", typeof(string));
                        dtrepEvents.Columns.Add("FacebookContent", typeof(string));
                        dtrepEvents.Columns.Add("FacebookImage", typeof(string));
                        dtrepEvents.Columns.Add("TwitterTitle", typeof(string));
                        dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                        dtrepEvents.Columns.Add("title", typeof(string));
                        dtrepEvents.Columns.Add("ShorttDescription", typeof(string));
                        dtrepEvents.Columns.Add("HostName", typeof(string));
                        dtrepEvents.Columns.Add("AccessToken", typeof(string));
                        dtrepEvents.Columns.Add("EventUrl", typeof(string));
                        DataRow drrepEvents;

                        Item itmPress = SG50Class.web.GetItem(SG50Class.str_Celebrations_Funds_Item_ID);

                        IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                                  where
                                                                  a.TemplateID.ToString().Equals(SG50Class.str_CelebrationFund_Details_Page_Template_ID)
                                                                  orderby
                                                                   a.Statistics.Created descending
                                                                  select a;
                        foreach (Item a in itmIEnumPressReleases)
                        {
                            drrepEvents = dtrepEvents.NewRow();
                            dtrepEvents.Rows.Add(ComFunList(drrepEvents, a, "NO"));
                        }
                        repUpcomingEvents.Visible = true;
                        repUpcomingEvents.DataSource = dtrepEvents;
                        repUpcomingEvents.DataBind();
                    }
                    catch (Exception exMain)
                    {
                        Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
                    }
                }
            }
        }
        protected void imgSearchButton_Click(object sender, ImageClickEventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    DataTable dtrepEvents = new DataTable();
                    dtrepEvents.Columns.Add("cssClass", typeof(string));
                    dtrepEvents.Columns.Add("item", typeof(Item));
                    dtrepEvents.Columns.Add("href", typeof(string));
                    dtrepEvents.Columns.Add("imgSrc", typeof(string));
                    dtrepEvents.Columns.Add("Id", typeof(string));
                    dtrepEvents.Columns.Add("videoURL", typeof(string));
                    dtrepEvents.Columns.Add("FacebookTitle", typeof(string));
                    dtrepEvents.Columns.Add("FacebookContent", typeof(string));
                    dtrepEvents.Columns.Add("FacebookImage", typeof(string));
                    dtrepEvents.Columns.Add("TwitterTitle", typeof(string));
                    dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                    dtrepEvents.Columns.Add("title", typeof(string));
                    dtrepEvents.Columns.Add("ShorttDescription", typeof(string));
                    dtrepEvents.Columns.Add("HostName", typeof(string));
                    dtrepEvents.Columns.Add("AccessToken", typeof(string));
                    dtrepEvents.Columns.Add("EventUrl", typeof(string));
                    DataRow drrepEvents;

                    Item itmPress = SG50Class.web.GetItem(SG50Class.str_Celebrations_Funds_Item_ID);

                    IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                              where
                                                              a.TemplateID.ToString().Equals(SG50Class.str_CelebrationFund_Details_Page_Template_ID)
                                                              select a;
                    foreach (Item a in itmIEnumPressReleases)
                    {
                        try
                        {
                            string Title = a.Fields["Title"].ToString().ToLower();

                            string Name = a.Fields["Short Description"].ToString().ToLower();

                            if (Title.ToString().Contains(SG50Class.StripUnwantedString(txtSearch.Text.ToLower())) || Name.ToString().Contains(SG50Class.StripUnwantedString(txtSearch.Text.ToLower())))
                            {
                                drrepEvents = dtrepEvents.NewRow();
                                drrepEvents["HostName"] = hostName + "/";
                                drrepEvents["AccessToken"] = accessToken;
                                drrepEvents["cssClass"] = "a";
                                drrepEvents["item"] = a;
                                drrepEvents["href"] = LinkManager.GetItemUrl(a).ToString() + "?buttons=NO";
                                drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(a).ToString();
                                string FacebookTitle = string.Empty;
                                string FacebookContent = string.Empty;
                                string TwitterContent = string.Empty;
                                string TwitterTitle = string.Empty;
                                string videourl = string.Empty;
                                string noHTML = string.Empty;
                                string inputHTML = string.Empty;
                                if (!string.IsNullOrEmpty(a.Fields["Facebook Title"].ToString()))
                                    FacebookTitle = a.Fields["Facebook Title"].ToString();

                                else
                                    FacebookTitle = a.Fields["Title"].ToString();

                                if (!string.IsNullOrEmpty(a.Fields["Facebook Content"].ToString()))
                                    FacebookContent = a.Fields["Facebook Content"].ToString(); //+ " " + hostName + LinkManager.GetItemUrl(a);
                                else
                                {
                                    inputHTML = a.Fields["Short Description"].ToString();
                                    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                                    FacebookContent = noHTML;//+ " " + hostName + LinkManager.GetItemUrl(a);
                                }

                                if (!string.IsNullOrEmpty(a.Fields["Twitter Title"].ToString()))
                                    TwitterTitle = a.Fields["Twitter Title"].ToString();
                                else
                                    TwitterTitle = a.Fields["Title"].ToString();


                                if (!string.IsNullOrEmpty(a.Fields["Twitter Content"].ToString()))
                                    TwitterContent = a.Fields["Twitter Content"].ToString();
                                else
                                {
                                    inputHTML = a.Fields["Short Description"].ToString();
                                    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                                    if (noHTML.Length > 120)
                                        TwitterContent = noHTML.Substring(0, 119);
                                    else
                                        TwitterContent = noHTML.Substring(0, noHTML.Length);
                                }


                                drrepEvents["FacebookTitle"] = cmObj.BuildString(FacebookTitle, CommonMethods.faceBook);
                                drrepEvents["FacebookContent"] = cmObj.BuildString(FacebookContent, CommonMethods.faceBook);
                                drrepEvents["TwitterContent"] = cmObj.BuildString(TwitterContent, CommonMethods.faceBook);
                                drrepEvents["TwitterTitle"] = TwitterTitle;

                                Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Facebook Image"]);
                                string fbimgSrc = fbimgField.Src;
                                if (!string.IsNullOrEmpty(fbimgSrc))
                                {
                                    drrepEvents["FacebookImage"] = hostName + "/" + fbimgSrc;
                                }
                                else
                                {
                                    Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                                    string staticimgSrc = staticimgField.Src;
                                    if (!string.IsNullOrEmpty(staticimgSrc))
                                    {
                                        drrepEvents["FacebookImage"] = hostName + "/" + staticimgField.Src;
                                    }
                                    else
                                    {
                                        drrepEvents["FacebookImage"] = "";
                                    }

                                }

                                drrepEvents["videoURL"] = a.Fields["Video Link1"].ToString();

                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                                string imgSrc = imgField.Src;

                                Sitecore.Data.Fields.ImageField VideoimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Video Image1"]);
                                string VideoImgsrc = VideoimgField.Src;

                                if (a.Fields["Thumb Image"] != null && !a["Thumb Image"].ToString().Equals(""))
                                {
                                    drrepEvents["imgSrc"] = hostName + "/" + imgSrc;
                                }
                                else if (a.Fields["Video Image1"] != null && !a["Video Image1"].ToString().Equals(""))
                                {
                                    drrepEvents["imgSrc"] = hostName + "/" + VideoImgsrc;
                                }

                                drrepEvents["Id"] = a.ID;
                                dtrepEvents.Rows.Add(drrepEvents);
                            }
                        }
                        catch (Exception exSub1)
                        {
                            Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
                        }
                    }

                    repUpcomingEvents.Visible = true;
                    repUpcomingEvents.DataSource = dtrepEvents;
                    repUpcomingEvents.DataBind();
                }
                catch (Exception exMain)
                {
                    Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
                }
            }
        }
    }
}