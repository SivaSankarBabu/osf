﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Idea_details_sublayout.Idea_details_sublayoutSublayout" CodeFile="~/SG50/layouts/Idea Details Sublayout.ascx.cs" %>

<style>
    .icon_content_inner {
        float: left;
        width: 65%;
        position: relative;
        clear: both;
    }
	
    .backButton
    {
        position: relative;
        top: -5px;
        left: -5px;
    }	

    .facebookLike {
        width: 35%;
        float: left;
        background-color: #ffffff;
    }

    .conversationStarter {
        float: left;
        width: 35%;
        text-align: left;
        background-color: #ffffff;
    }

    .Facebook {
        width: 33%;
        float: left;
        background-color: #ffffff;
    }

    .fb-comments, .fb-comments * 
    {
        width:100% !important;
    }

    .statement p {
        font-weight: bold;
        color: black;
        padding-top: 50px;
        padding-left: 10px;
    }

    .facebookLike div {
        padding: 10px;
    }

    .idea_details {
        width: 95%;
        float: left;
        background-color: #F01334;
        min-height: 768px;
    }

    .ideaId {
        margin-top: 50px;
        font-size: 50px;
        font-weight: bold;
        margin-left: 50px;
        color: #fff;
        margin-bottom: 50px;
		font-size:64px;
    }

    .ideaDetails {
        color: #fff;
        margin-left: 50px;
        margin-right: 30%;
        font-size: 26px;
        line-height: 45px;
		font-style:italic;

    }
    .ideaContributor {
        margin-top:50px;
        color: #fff;
        margin-left: 50px;
        font-size: 20px;
        line-height: 45px;
		font-style:italic;
    }
    .idea_details a img {
        margin:5px;
    }
    .ideaPage {
        min-height:768px;
        background-color:white;
    }
	
    @media screen and (min-width: 300px) and (max-width: 768px)
    {
        .icon_content_inner, .idea_details
        {
            width: 100%;
            height: auto;
        }

        #bgCircle
        {
            display: none;
        }

        .conversationStarter
        {
            width: 100%;
        }
		
		.Facebook
		{
			width: 95%;
			margin-left: 2%;
		}
    }	
	
</style>
<div class="ideaPage">


<div class="icon_content_inner">
    <div class="idea_details">
        <div class="backButton"><a href="<%= getCelebrationIdeasLink() %>" ><img id="ideabackbtn" src="/SG50/images/submission/submission_return.jpg" /></a></div>
        <canvas id="bgCircle" width="790" height="768" style="position: absolute; top: 0px; left: 0px;"></canvas>
        
        
        <div class="ideaId">
            #<sc:Text Field="Index" runat="server" />
        </div>
        <div class="ideaDetails"><sc:Text ID="Text1" Field="Idea" runat="server" /></div>
        <div class="ideaContributor"><sc:Text ID="Text2" Field="Name" runat="server" />, <sc:Text ID="Text3" Field="Age" runat="server" /></div>
    </div>

</div>

<div class="conversationStarter">
    <div class="statement">
        <p class="Normal1">
            Think you can build upon this idea?
            <br />
            Share your thoughts!
        </p>
    </div>
</div>

<div class="facebookLike">
    <div class="fb-like" data-href="<%= getCurrentLink() %>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
</div>

<script type="text/javascript">


    /*-------------------FacebookCommentScrollbar-----------------------------*/
    $(".facebookComment").mCustomScrollbar({
        theme: "dark",
        advanced: {
            updateOnContentResize: true
        }
    });

    /*-------------------ReloadFacebookCommentAfterComment-----------------------------*/
    var pathname = window.location.pathname;

    $(".fbCommentButton").click(function () {
        $(".wrap .innerWrap").load(pathname);
    });

    /*-------------------MiniSlideShow-----------------------------*/
    $(".slideShow > div:gt(0)").hide();

    setInterval(function () {
        $('.slideShow > div:first')
          .fadeOut(1000)
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('.slideShow');
    }, 3000);

    /*-------------------Facebook-----------------------------*/
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/all.js#xfbml=1&appId=1427510060800861";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    /*-------------------ResponsiveTab-----------------------------*/
    $('#tabs li a').click(function (e) {

        $('#tabs li, #content .current').removeClass('current').removeClass('fadeInLeft');
        $(this).parent().addClass('current');
        var currentTab = $(this).attr('href');
        $(currentTab).addClass('current fadeInLeft');
        e.preventDefault();

    });

    /*-------------------ResponsiveLayout-----------------------------*/

    var responsive_viewport1 = $(window).width();

    if (responsive_viewport1 < 769) {
        var icon_tab_banner = $('.iconBanner').clone();
        (icon_tab_banner).insertAfter("#iconBanner");
    }

    var responsive_viewport2 = $(window).width();

    if (responsive_viewport2 < 769) {
        var icon_tab_description = $('.descriptionContent').clone();
        (icon_tab_description).insertAfter("#descriptionContent");
    }

    var responsive_viewport3 = $(window).width();

    if (responsive_viewport3 < 769) {
        var conversation_tab_facebook = $('.facebookComment').clone();
        (conversation_tab_facebook).insertAfter("#facebookComment");
    }

    var responsive_viewport4 = $(window).width();

    if (responsive_viewport4 < 769) {
        var facebook_like = $('.facebookLike').clone();
        (facebook_like).insertAfter(".roundImage");
    }

    var responsive_viewport5 = $(window).width();

    if (responsive_viewport5 < 769) {
        var conversation_starter = $('.conversationStarter').clone();
        (conversation_starter).insertAfter("#conversationStarter");
    }

    /*-------------------FacebookCommentScrollbarForResponsive-----------------------------*/
    $("#tabwrap .facebookComment").mCustomScrollbar({
        theme: "dark",
        advanced: {
            updateOnContentResize: true
        }
    });

    var responsive_viewport = $(window).width();
    count = 0;

    $(window).resize(function () {
        var new_responsive_viewport = $(window).width();

        if (new_responsive_viewport < 769 && responsive_viewport > 769 && count == 0) {
            var icon_tab_banner = $('.iconBanner').clone();
            (icon_tab_banner).insertAfter("#iconBanner");

            var icon_tab_description = $('.descriptionContent').clone();
            (icon_tab_description).insertAfter("#descriptionContent");

            var conversation_tab_facebook = $('.facebookComment').clone();
            (conversation_tab_facebook).insertAfter("#facebookComment");

            var facebook_like = $('.facebookLike').clone();
            (facebook_like).insertAfter(".roundImage");

            var conversation_starter = $('.conversationStarter').clone();
            (conversation_starter).insertAfter("#conversationStarter");
            count++;
        }

        responsive_viewport = new_responsive_viewport;

    });


</script>

<script type="text/javascript">
    var canvas = document.getElementById('bgCircle');
    var context = canvas.getContext('2d');
    var centerX = 250;
    var centerY = 100;
    var radius = 500;

    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.lineWidth = 2;
    context.strokeStyle = '#ffffff';
    context.stroke();

    </script>

<div class="Facebook">
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=565151546893256";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <div class="facebookComment">
        <div class="fb-comments" data-colorscheme="light" colorscheme="light" data-numposts="10" data-href="<%= getCurrentLink() %>" data-order-by="reverse_time" data-width="400px"></div>
    </div>
    <div style="clear:both; width:100%;height:0px;">&nbsp;</div>
</div>
</div>