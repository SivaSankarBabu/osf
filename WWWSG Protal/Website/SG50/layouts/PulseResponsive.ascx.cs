﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Layouts.Pulseresponsive
{

    /// <summary>
    /// Summary description for PulseresponsiveSublayout
    /// </summary>
    public partial class PulseresponsiveSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();

        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                string SearchValue = Request.QueryString["Search"];
                if (SearchValue != null)
                {
                    //PulseSearch(SearchValue);
                    FillPulse(string.Empty, SearchValue);
                }
                else
                {
                    FillPulse(string.Empty,string.Empty);
                }
            }
        }
        private void FillPulse(string value, string Word)
        {
            try
            {
                DataTable dtPulse = new DataTable();
                dtPulse.Columns.Add("item", typeof(Item));
                dtPulse.Columns.Add("imgSrc", typeof(string));
                dtPulse.Columns.Add("Title", typeof(string));
                dtPulse.Columns.Add("Sub Title", typeof(string));
                dtPulse.Columns.Add("Description", typeof(string));
                dtPulse.Columns.Add("Id", typeof(string));
                dtPulse.Columns.Add("href", typeof(string));
                dtPulse.Columns.Add("EventUrl", typeof(string));
                dtPulse.Columns.Add("width", typeof(int));


                DataRow drPulse;
                Item itmPulses = web.GetItem("{83DBF609-220C-45B2-9A27-D5411E98B8E3}");
                IEnumerable<Item> NewPulsechild = itmPulses.GetChildren();

                Item itmPress = SG50Class.web.GetItem("{83DBF609-220C-45B2-9A27-D5411E98B8E3}");

                IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                          where
                                                          a.TemplateID.ToString().Equals("{60AD0385-5E02-492A-B358-F14177348860}")
                                                          orderby a.Statistics.Updated descending
                                                          select a;

                IEnumerable<Item> itmIEnumPressReleases1 = from a in itmPress.Axes.GetDescendants()
                                                           where
                                                           a.TemplateID.ToString().Equals("{60AD0385-5E02-492A-B358-F14177348860}")
                                                           orderby a.Statistics.Updated
                                                           select a;

                IEnumerable<Item> itmIEnumPressReleases2 = from a in itmPress.Axes.GetDescendants()
                                                           where
                                                           a.TemplateID.ToString().Equals("{60AD0385-5E02-492A-B358-F14177348860}")
                                                           orderby a.Statistics.Created descending
                                                           select a;

                IEnumerable<Item> itmIEnumPressReleases3 = from a in itmPress.Axes.GetDescendants()
                                                           where
                                                           a.TemplateID.ToString().Equals("{60AD0385-5E02-492A-B358-F14177348860}")
                                                           orderby a.Statistics.Created
                                                           select a;

                foreach (Item a in NewPulsechild)
                {
                    if (a.Fields["Title"].ToString().ToLower().Contains(Word.ToLower()) || a.Fields["Description"].ToString().ToLower().Contains(Word.ToLower()))
                    {
                        // if (a.Name != "" && a.Name != "Pulse10" && a.Name != "Pulse12")
                        //  {
                        // Response.Write("name"+a.Name);
                        drPulse = dtPulse.NewRow();
                        drPulse["item"] = a;
                        drPulse["title"] = a.Fields["Title"].ToString();
                        drPulse["description"] = a.Fields["Description"].ToString();
                        drPulse["Id"] = a.ID.ToString();
                        drPulse["Sub Title"] = a.Fields["Sub Heading"].ToString();

                        drPulse["href"] = LinkManager.GetItemUrl(a).ToString();// +"/" + a.Name;
                        Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                        drPulse["width"] = mainImgField.Width;

                        if (mainImgField != null)
                        {
                            string mainimgSrc = mainImgField.Src;
                            if (!string.IsNullOrEmpty(mainimgSrc))
                            {
                                drPulse["imgSrc"] = mainimgSrc;
                            }
                        }
                        dtPulse.Rows.Add(drPulse);
                        // }
                    }
                }
                PagedDataSource pgsource = new PagedDataSource();
                pgsource.DataSource = dtPulse.DefaultView;
                pgsource.AllowPaging = true;
                pgsource.PageSize = 30;
                pgsource.CurrentPageIndex = PageNumber;

                if (dtPulse.Rows.Count == 0)
                {
                    lblResult.Visible = true;
                    lblResult.Text = "No search records found..";
                }
                else
                {
                    lblResult.Visible = false;
                }

                if (PageNumber == 0)
                    PrevControl.Enabled = false;
                else
                    PrevControl.Enabled = true;

                //int pageNationCount = 0;
                //pageNationCount = (dtPulse.Rows.Count / pgsource.PageSize) - 1;
                if (PageNumber == pgsource.PageCount - 1)
                    NextControl.Enabled = false;
                else
                    NextControl.Enabled = true;

                DataTable dtNewCollectiblePaging = new DataTable();
                dtNewCollectiblePaging.Columns.Add("ID", typeof(string));
                dtNewCollectiblePaging.Columns.Add("style", typeof(string));
                dtNewCollectiblePaging.Columns.Add("Enable", typeof(Boolean));

                if (pgsource.PageCount > 1)
                {
                    DataRow drNewCollectiblePaging;
                    //Style="padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important"
                    string css = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;";
                    string Activecss = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important";
                    rptPaging.Visible = true;
                    System.Collections.ArrayList pages = new System.Collections.ArrayList();
                    for (int i = 0; i <= pgsource.PageCount - 1; i++)
                    {
                        drNewCollectiblePaging = dtNewCollectiblePaging.NewRow();
                        drNewCollectiblePaging["ID"] = (i + 1).ToString();
                        if (pgsource.CurrentPageIndex + 1 == i + 1)
                        {
                            drNewCollectiblePaging["style"] = Activecss;
                            drNewCollectiblePaging["Enable"] = false;
                        }
                        else
                        {
                            drNewCollectiblePaging["style"] = css;
                            drNewCollectiblePaging["Enable"] = true;
                        }

                        dtNewCollectiblePaging.Rows.Add(drNewCollectiblePaging);
                    }
                    rptPaging.DataSource = dtNewCollectiblePaging;
                    rptPaging.DataBind();
                }
                else
                {
                    rptPaging.Visible = false;

                    if (pgsource.PageCount <= 1)
                    {
                        PrevControl.Visible = false;
                        NextControl.Visible = false;
                    }

                }

                repNewCollectibles.DataSource = pgsource;
                repNewCollectibles.DataBind();
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }

        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return System.Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["PageNumber"] = value; }
        }

        protected void rptPaging_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = System.Convert.ToInt32(e.CommandArgument) - 1;
            FillPulse(string.Empty, string.Empty);
        }
        protected void PrevControl_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;
            FillPulse(string.Empty, string.Empty);
        }
        protected void NextControl_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;
            FillPulse(string.Empty, string.Empty);
        }

        protected void imgSearchButton_Click(object sender, ImageClickEventArgs e)
        {
            //FillPulse(string.Empty,SG50Class.StripUnwantedHtml(txtSerachPulse.Text));
            Response.Redirect("/SG50/SearchResult.aspx?searchWord=" + txtSerachPulse.Text.Trim());
        }

       
    }
}