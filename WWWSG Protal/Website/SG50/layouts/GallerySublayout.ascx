﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Gallerysublayout.GallerysublayoutSublayout" CodeFile="~/SG50/layouts/GallerySublayout.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript"
    language="javascript"></script>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<script src="/SG50/include/js/b6NoKG6GEMQqshcL22y.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/jquery.bxslider.min.js"></script>
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<%--<style>
    .embed-container
    {
        position: relative;
        padding-bottom: 56.25%;
        height: 0;
        overflow: hidden;
        max-width: 100%;
        height: auto;
    }

        .embed-container iframe,
        .embed-container object,
        .embed-container embed
        {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
</style>--%>
<script type="text/javascript" lang="ja">

    


</script>
<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...

        //alert(Img);
        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle.toString(),
            // caption: fbdes,
            description: fbdes.toString()
        };
        FB.ui(obj);

    }
    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function (response) {

                result = response.data.url;
                var tempurl = '//twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }



</script>
<asp:HiddenField ID="isMobile" runat="server" />
<div class="masthead">
    <!-- Gallery Banner -->
    <div class="banner-gallery">
        <div class="banner-gallery-text">
            <div class="banner-gt-middle">
                <div class="gt-middle">
                    <div class="photo-row">
                        <%--<span>RELIVE THE<br />
                            EXCITEMENT</span>--%>
                    </div>
                </div>
            </div>
        </div>
        <%--<img src="/SG50/images/Gallery/banner-gallery01.jpg" class="banner-image" />--%>
        <sc:text id="txtBannerTitle" runat="server" field="Banner Image" />
    </div>
    <!-- Gallery Banner -->
    <!-- Photos Header -->
    <div class="photo-header">
        <div class="photo-row">
            <div class="cell-12 GalleryT">
                <div class="group-text-button">
                    <h1>
                        <sc:text id="txtTitle" runat="server" field="Title" />
                    </h1>
                    <%--<div class="si-title">
                        <a href="#" class="facebook"></a>
                        <a href="#" class="twitter"></a>
                    </div>--%>
                    <asp:Repeater ID="repsocialSharingTop" runat="server">
                        <ItemTemplate>
                            <div class="si-title">
                                <a class="facebook" href="JavaScript:postToFeed('<%# Eval("SocialShareTitle").ToString()%>','<%# Eval("SocialShareContent").ToString()%>','<%# Eval("SocialSharingThumbNail")%>','<%# Eval("EventUrl")%>')"></a><a class="twitter" href="JavaScript:newPopup('<%# Eval("SocialShareContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')"></a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <p>
                    <sc:text id="txtDescription" runat="server" field="Description" />
                </p>
            </div>
        </div>
    </div>
    <!-- Photos Header -->
    <div class="photo-gallery gallery-VRow">
        <div class="photo-row">
            <div class="LineTitle">
                <h1>Videos</h1>
                <span class="line"></span>
            </div>
            <div class="popup-gallery videos popup-youtube">
                <div class="SG50Loader">
                    <img src="/SG50/images/Gallery/SG50Loader.gif" />
                </div>
                <ul id="YouTubeList">
                </ul>
            </div>
            <div class="ViewAll">
                <a href="/SG50/GalleryLanding/GalleryVideoList.aspx">View All Videos</a>
            </div>
        </div>
    </div>
    <div class="photo-gallery gallery-VRow gallery-PRow">
        <div class="photo-row">
            <div class="LineTitle">
                <h1>Photos</h1>
                <span class="line"></span>
            </div>
            <div class="SG50Loader">
                <img src="/SG50/images/Gallery/SG50Loader.gif" />
            </div>
            <ul id="FaceBookList">
            </ul>
            <div class="ViewAll">
                <a href="/SG50/GalleryLanding/GalleryPhotoList.aspx">View All Photos</a>
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
    <div class="photo-gallery gallery-VRow" runat="server" id="APPSection">
        <div class="photo-row">
            <div class="galleryApps">
                <div class="AppsLeft">
                    <div class="cell-12 padding-none mobile-scroll">
                        <div class="cell-6 padding-Lnone">
                            <div class="LineTitle">
                                <h1>Playlist</h1>
                                <span class="line"></span>
                            </div>
                            <div class="embed-container">
                                <%--<iframe src="https://embed.spotify.com/?uri=spotify:user:mccysingapore:playlist:3lgLJVjkF1sTDo8K9xqVNe&theme=white" width="500" height="550" frameborder="0" allowtransparency="true"></iframe>--%>
                                <iframe src="https://embed.spotify.com/?uri=spotify:user:mccysingapore:playlist:3lgLJVjkF1sTDo8K9xqVNe&theme=white"
                                    width="500" height="555" frameborder="0" allowtransparency="true" class="embeded_iframe_50"></iframe>
                            </div>
                        </div>
                        <div class="cell-6 padding-Rnone">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                            <asp:UpdatePanel ID="upCountry" runat="server">
                                <ContentTemplate>
                                    <div class="LineTitle">
                                        <h1>Apps</h1>
                                        <span class="line"></span>
                                    </div>
                                    <div class="AppsList radius-3p">
                                        <table>
                                            <asp:Repeater runat="server" ID="rpApp">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="SNO">
                                                            <%# Eval("Sno") %>
                                                        </td>
                                                        <td class="Middel">
                                                            <img src="<%# Eval("imgSrc") %>" class="IconImage" />
                                                            <h4>
                                                                <%# Eval("Title") %></h4>
                                                            <p>
                                                                <%# Eval("Description") %>
                                                            </p>
                                                            <span>
                                                                <%-- <%# Eval("Version") %>--%></span>
                                                        </td>
                                                        <td class="AppsCont">
                                                            <a href='<%# Eval("IphoneURL") %>' runat="server" id="RIphone" target="_blank" class="AppsIcon Apple"></a><a href='<%# Eval("AndroidURL") %>' runat="server" id="RAndroid" target="_blank"
                                                                class="AppsIcon Google"></a>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                        <div class="cell-12">
                                            <div class="pagination">
                                                <ul>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="btnPage" />
                                                                    </Triggers>
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnPage" Enabled='<%# Eval("Enable") %>' Style='<%# Eval("style") %>'
                                                                            CommandName="Page" CommandArgument='<%# Eval("ID") %>' runat="server" ForeColor="White"
                                                                            Font-Bold="True"><%# Eval("ID") %>
                                                                        </asp:LinkButton>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <table id="mobileDiv" style="display: none">
                                            <tr>
                                                <td class="SNO"></td>
                                                <td class="Middel">
                                                    <img src="#" class="IconImage">
                                                    <h4></h4>
                                                    <p>
                                                    </p>
                                                    <span></span>
                                                </td>
                                                <td class="AppsCont">
                                                    <a href="#" id="content_0_rpApp_RIphone_0" target="_blank" class="AppsIcon Apple"></a><a href="#" id="content_0_rpApp_RAndroid_0" target="_blank" class="AppsIcon Google"></a>
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="mobileData" style="display: none">
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--     <div class="photo-gallery gallery-VRow gallery-PRow">
        <div class="photo-row">
            <div class="LineTitle">
                <h1>
                    Playlist</h1>
                <span class="line"></span>  <br />  <br />  <br />  <br />
                <div class="embed-container" >
                  
                        <iframe src="https://embed.spotify.com/?uri=spotify:user:mccysingapore:playlist:3lgLJVjkF1sTDo8K9xqVNe&theme=white"   width="1020" height="1020" frameborder="0" allowtransparency="true"></iframe>
                        
                </div>
                   
            </div>
        </div>
    </div>--%>
</div>

<script src="/SG50/include/js/jquery.colorbox.js" type="text/javascript"></script>
<script src="/SG50/include/js/b6NoG6GMQqshcL22.js" type="text/javascript"></script>
<script>
    function MobileApps() {
        $.ajax({
            type: 'post', url: '/SG50/ajax/ajax_content.aspx/MobileAPPS', contentType: 'application/json; charset=utf-8', dataType: 'json', success: function (data) {
                if (data.d != null && data.d != '') {
                    var div = '<div class=AppsList radius-3p><table>'; //AppsList radius-3p
                    for (var index = 0; index < data.d.length; index++) {
                        div += '<tr>';
                        var row = $('#mobileDiv').find('tr');
                        row.find('td:eq(0)').text(data.d[index].SNO);
                        row.find('td:eq(1) img').attr('src', data.d[index].ImageSrc);
                        row.find('td:eq(1) h4').text(data.d[index].Title);
                        row.find('td:eq(1) p').text(data.d[index].Description);
                        //row.find('td:eq(1) span').text(data.d[index].Version);
                        // row.find('td:eq(2) a').attr('href', data.d[index].iPhoneUrl);
                        row.find('td:eq(2) a:eq(0)').attr('href', data.d[index].iPhoneUrl);
                        row.find('td:eq(2) a:eq(1)').attr('href', data.d[index].AndroidURL);
                        //  row.find('td:eq(2) a').attr('href', data.d[index].AndroidURL);
                        div += row.html();
                        div += '</tr>';
                    }
                    div += '</table></div>';
                    $('#mobileData').html(div).show();
                }
            }, failure: function () { alert(failure); return false; }, error: function (err) { alert(err.responseText + " " + err.statusCode); return false; }
        })
    }

    if (/Android|iPhone/i.test(navigator.userAgent)) {
        MobileApps();
        $('.AppsList.radius-3p:eq(0)').hide();

    }



</script>
