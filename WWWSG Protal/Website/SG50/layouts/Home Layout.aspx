﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    Culture="en-GB" %>

<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>SG50</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
     <meta property="fb:admins" content="100007298373580" />
    <meta property="fb:app_id" content="505158409601896" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="-1" />
    <sc:sublayout runat="server" renderingid="{6D5E4173-FA8F-44CC-9B34-EDE491E0ECF5}"
        path="/SG50/layouts/Meta Tag Sublayout.ascx" id="sublayoutMetaTag" placeholder="contentHeader"></sc:sublayout>
    
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />    
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
    
      
    
    <script src="/SG50/include/js/jquery.min_Home.js"></script>
    
    <script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>

    <script type="text/javascript">

        if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
            alert('Please use Internet Explorer Version 9 or higher version for best viewing.');
        }
         
    </script>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-46719765-2', 'singapore50.sg');
        ga('send', 'pageview');
    </script>

    <sc:visitoridentification runat="server" />
</head>
<body>
    <form method="post" runat="server" id="mainform">
    <div class="container" id="homePage">
        <div id="header">
            <sc:placeholder id="plhHeader" runat="server" key="Header"></sc:placeholder>
        </div>
        <div class="content">
            <sc:placeholder id="plhContent" runat="server" key="Content"></sc:placeholder>
        </div>
        <div class="footer">
            <sc:placeholder id="plhFooter" runat="server" key="Footer"></sc:placeholder>
        </div>
    </div>
    </form>
<script async src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" src="/sg50/include/js/common.js"></script>
</body>
</html>

 

