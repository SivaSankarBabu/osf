﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Pgsharequote_sublayout.Pgsharequote_sublayoutSublayout" CodeFile="~/SG50/layouts/PG/sublayouts/PGShareQuote Sublayout.ascx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="text/javascript">
    var domainName = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Host Name"].Value.ToString() %>";
    $(function () {

        $('#shareOnTwitter').click(function () {
            newPopup($('#content_0_lbloverlay').text());
        });

        $('#aChangeImage').click(function () {
            $('#divChangeBanner').fadeToggle(1000);
        });

        $('#btnCancel').click(function () {
            $('#content_0_lblResult,#content_0_lblSprt').text(''); $('#divChangeBanner').toggle();
        });





    });

    function previewFile() {
        var preview = document.querySelector('#<%=imgFirst.ClientID %>');
        var previewTest = document.querySelector('#<%=Image1.ClientID %>');
        var overlaytxt = document.querySelector('#<%=lbloverlay.ClientID %>');
        var overlayNametxt = document.querySelector('#<%=lbloverlayName.ClientID %>');
        var file = document.querySelector('#<%=fileChnageBanner.ClientID %>').files[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            previewTest.setAttribute('style', 'display:inline');
            previewTest.src = reader.result;

            setTimeout(function () {


                if (file.size <= 1048576) {
                    //if (previewTest.clientWidth >= 1280 && previewTest.clientHeight >= 740) {
                    overlaytxt.setAttribute('style', 'display:inline');
                    overlaytxt.setAttribute('style', 'Font-Size:28px');
                    overlayNametxt.setAttribute('style', 'display:inline');
                    preview.src = reader.result;
                    previewTest.src = "";
                    previewTest.setAttribute('style', 'display:none');
                    //}
                    //else {
                    //    alert('Image file dimentions must above 1280*740');
                    //    previewTest.src = "";
                    //    previewTest.setAttribute('style', 'display:none');

                    //}
                }
                else {
                    alert('Uploaded images size exceeded 1mb');
                    previewTest.src = "";
                    previewTest.setAttribute('style', 'display:none');
                }
            }, 200);
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
    }
    function FacebookCalling() {
        var img = $('#contentshare_0_imgFirst');
        if (img != null || img != undefined) {
            var imgSRC = $(img).attr('src');
            var imagePath = domainName + imgSRC
            var sharequote = $('#contentshare_0_lbloverlay').text();
            postToFeed(imagePath);
        }
        return true;
    }
	
	var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
        window.fbAsyncInit = function () {
            FB.init({
                appId: FBID, status: true, cookie: true,
                version: 'v2.2'
            });
        };

	 (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Img) {
        var image1 = Img.replace("?bc=White", "");
        var SessionID = '<%= Session["StoryRedirecturl"] %>';
        var obj = {
            method: 'feed',
            picture: image1,
            link: SessionID
        };
        

       
        FB.ui(obj);
        return true;
    }
</script>

<script type="text/javascript">
    function newPopup() {
        var img = $('#contentshare_0_imgFirst');
        if (img != null || img != undefined) {
            var imgSRC = $(img).attr('src');
            var imagePath = domainName + imgSRC
            var sharequote = $('#contentshare_0_lbloverlay').text();
            var SessionID = '<%= Session["StoryRedirecturl"] %>';
            var SessionID2 = '<%= Session["StoryRedirecturl2"] %>';
            var urls = encodeURIComponent(SessionID);
            var tempurl = 'https://twitter.com/intent/tweet?text=' + urls
            popupWindow = window.open(
                tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')
        }
        else {
            alert("Image Not Found");
        }
    }
    function TwitterTweet() {
        newPopup();
    }
</script>

<style>
    .detailsbanner .slide-caption p {
        bottom: 0;
        font-size: 28px;
        line-height: normal;
        text-align: center !important;
        text-shadow: 2px 4px 2px #000;
    }

    .slider-container .slide-caption p {
        color: #fff;
        font-size: 68px;
        font-weight: 600;
        line-height: 68px;
        padding-bottom: 20px;
    }

    .sgContainer p {
        font-family: "Open Sans",sans-serif;
    }

    .sortlist-container .caption p span, .slide-caption p span {
        line-height: 1.2em;
    }
</style>
<asp:Literal ID="PGlitMetaTag" runat="server"></asp:Literal>
<div class="slider-container details-slider">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <div id="DetailsImgBanner" runat="server" class="carousel-inner detailsbanner">
            <div class='item active'>
                <asp:Image ID="imgFirst" runat="server" />
                <asp:Image ID="Image1" runat="server" runat="server" Style="display: none" />
                <div class='carousel-caption'>
                    <div class='slide-caption'>
                        <p>
                            <asp:Label ID="lbloverlay" runat="server" Font-Size="28px"></asp:Label>
                            <span>
                                <asp:Label ID="lbloverlayName" runat="server" Font-Size="14px"></asp:Label></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
            href="#carousel-example-generic" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>

<script src="/SG50/include/PG/js/jquery-filestyle.min.js"></script>

<link href="/SG50/include/PG/css/jquery-filestyle.css" rel="stylesheet" />

<script>
    $(document).ready(function () {
        $(":file").jfilestyle({ buttonText: "CHANGE THE IMAGE", icon: false });

    });


</script>

<style>
    div.jquery-filestyle input[type="text"][disabled] {
        height: 50px !important;
        margin: 0 -10px 10px 0 !important;
        width: 470px !important;
        color: #000 !important;
        display: none;
    }
</style>
<section class="pioneer-bg photo-buttons">
    <asp:ScriptManager ID="scMgr" runat="server">
    </asp:ScriptManager>
    <div class="pioneer-container">
        
        <div id="divChangeBanner">
            <div style="color: white">
                <p id="pChangeBanner">
                    
                                <asp:FileUpload CssClass="jfilestyle" ID="fileChnageBanner"    onchange="previewFile()"  runat="server" /><br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic" runat="server" ControlToValidate="fileChnageBanner" ValidationGroup="FileUpload" ErrorMessage="Pleae select Image"></asp:RequiredFieldValidator></td>
                                
                </p>
            </div>
        </div>
        <div class="choose"><a id="aChangeImage" style="display:none" runat="server" ValidationGroup="FileUpload"   class="sg-button">Change the image</a></div>
        <div class="photo-buttons"><span><a id="shareOnFacebook" runat="server" onserverclick="FaceBookSharing" class="sg-button">Share on Facebook</a></span> 
        <span><a id="shareOnTwitter" runat="server" onserverclick="TwitterSharing" class="sg-button">Share on Twitter</a></span></div>
        <div class="clearfix"></div>
    </div>
</section>
<section class="events-section" style="display: none">
    <div class="custom-container">
        <div class="events-block">
            <!--<h3>Latest Events</h3> -->
            <div class="events-img">
                <asp:Image ID="imgEvent" runat="server" />
            </div>
            <div class="description">
                
                <h5>
                    <sc:Text ID="txtTitle" Field="Title" runat="server" DataSource="{2547DDFB-044A-4011-BCC4-5A16FEED3D1C}" />
                </h5>
               
                <p>
                    <sc:Text ID="txtDescription" Field="Description" runat="server" DataSource="{2547DDFB-044A-4011-BCC4-5A16FEED3D1C}" />
                </p>

                <a href="#" class="event-button">
                    <sc:Text ID="btnText" Field="Group Button Text" runat="server" DataSource="{2547DDFB-044A-4011-BCC4-5A16FEED3D1C}" />
                </a>
            </div>

        </div>
        <div class="clearfix"></div>
    </div>
</section>
