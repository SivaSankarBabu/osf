﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;

namespace Layouts.Pghomesublayout_top
{

    /// <summary>
    /// Summary description for Pghomesublayout_topSublayout
    /// </summary>
    public partial class Pghomesublayout_topSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmContext = Sitecore.Context.Item;
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        public string vidurl = "";
        
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
                getPGbannerimages();
        }

        public void NextStory(object sender, EventArgs e)
        {
            Response.Redirect("/SG50/Pioneer Generation/PG Story.aspx");
        }
        private void getPGbannerimages()
        {
            try
            {

                Item HomeBannerItem = web.GetItem(SG50Class.str_PioneerHome_Banner_Item_ID);

                if (HomeBannerItem != null)
                {
                    ChildList homePageBanners;
                    StringBuilder sbBannerText = new StringBuilder();

                    // sbBannerText.Append("<div id='slider'>");
                    StringBuilder pagerText = new StringBuilder();
                    homePageBanners = HomeBannerItem.GetChildren();

                    if (homePageBanners!=null && homePageBanners.Count >= 0)
                    {

                        int BannerCount = 0;
                        sbBannerText.Append("<ul class='bxslider'>");
                        for (int i = 0; i < 5; i++)
                        {

                            //if (homePageBanners[i].Versions.Count > 0 && homePageBanners[i]["Show Banner"] == "1" && BannerCount < 3)
                            //{
                                BannerCount++;


                                string videourl = homePageBanners[i]["Video URL"];
                                if (string.IsNullOrEmpty(videourl))
                                {
                                    try
                                    {

                                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)homePageBanners[i].Fields["Image"]);

                                    string mediaUrl5 ="";
					if(imgField != null)
					mediaUrl5 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                             
                                    sbBannerText.Append("<li>");
                                    sbBannerText.Append("<img src='" + mediaUrl5 + "'/>");
                                    sbBannerText.Append("<div class='home-caption'>");
                                    sbBannerText.Append("<div class='slide-caption'>");
                                    //sbBannerText.Append("<p>");
                                    //Sitecore.Data.Fields.ImageField imgField1 = ((Sitecore.Data.Fields.ImageField)homePageBanners[i].Fields["Slider Title"]);
                                    //string mediaUrl1 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField1.MediaItem);
                                    //sbBannerText.Append("<img src='" + mediaUrl1 + "'/>");
                                    //sbBannerText.Append("</p>");
                                    sbBannerText.Append("<p><span>" + homePageBanners[i]["Slider Title"] + "</span></p><div class='slide-para'>");
                                    string text = homePageBanners[i]["Slider Content"];
                                    sbBannerText.Append(text);
                                    sbBannerText.Append("</div>");
                                    string url = homePageBanners[i]["Button URL"];
                                    sbBannerText.Append("<a href='" + url + "' class='sg-button'>" + homePageBanners[i]["Button Text"] + "</a>");
                                    sbBannerText.Append("</div>");
                                    sbBannerText.Append("</div>");
                                    sbBannerText.Append("</li>");
                                    }
                                    catch (Exception ex)
                                    {
                                        throw;
                                    }
                                }
                                else
                                {

                                    sbBannerText.Append("<li class='Video-section'>");
                                    sbBannerText.Append("<div id='howToVideo"+ i +"' class='video'>");
                                    //sbBannerText.Append("<iframe src='" + videourl + "' width='560' height='315' frameborder='0' allowfullscreen></iframe>");
                                    sbBannerText.Append("</div>");

                                    sbBannerText.Append("<div class='home-caption videocon'>");
                                    sbBannerText.Append("<div class='slide-caption'>");
                                    sbBannerText.Append("<div class='slide-para'>" + homePageBanners[i]["Slider Content"] + "</div>");
                                    sbBannerText.Append("</div></div>");

                                    sbBannerText.Append("</li>");
                                    vidurl += "{ id: 'howToVideo" + i + "', height: 'auto', width: '100%', videoId: '" + videourl + "' },";
                                    //[{ id: 'howToVideo1', height: 'auto', width: '100%', videoId: 'ObaLqHHTiJ0' }, { id: 'howToVideo2', height: 'auto', width: '100%', videoId: 'ObaLqHHTiJ0'}]

                                }
                          //  }
                        }




                        //sbBannerText.Append("</div>");                                         
                    }
                    sbBannerText.Append("</ul>");
                    slidercontent.InnerHtml = sbBannerText.ToString();
                }
            }
            catch (Exception ex)
            {
                throw ;
            }
        }
    }
}