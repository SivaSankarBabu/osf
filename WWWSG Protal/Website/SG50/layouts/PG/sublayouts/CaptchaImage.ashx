﻿<%@ WebHandler Language="C#" Class="CaptchaImage" %>

using System;
using System.Web;
using System.Drawing;
using Microsoft.Security.Application;
public class CaptchaImage : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        string path;
        if (context.Request.QueryString["id"] != null)
            path = Encoder.HtmlEncode(context.Request.QueryString["id"].ToString());
        else
            throw new ArgumentException("No parameter specified");
        if (System.IO.File.Exists(path))
        {
          Bitmap newBmp = new Bitmap(path);
           
            if (newBmp != null)
            {

                newBmp.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
                newBmp.Dispose();
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}