﻿using Sitecore;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Publishing;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using System.Web.Services;
using System.Net;
using ImageProcessor;
using ImageProcessor.Imaging;
using ImageProcessor.Imaging.Formats;
namespace Layouts.Pgsharequote_sublayout
{

    /// <summary>
    /// Summary description for Pgsharequote_sublayoutSublayout
    /// </summary>
    public partial class Pgsharequote_sublayoutSublayout : System.Web.UI.UserControl
    {
        Item itmContext = Sitecore.Context.Item;
        Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"] + "/SG50/Pioneer Generation";
        string[] allowedExtensions = new string[] { ".Jpg, .Jpeg, .Bmp, .Gif, .Tif, .Png" };
        string currentItemId = string.Empty, url = "";
        Item itmPress = null;
        private void Page_Load(object sender, EventArgs e)
        {

            if (Session["Share"] == "Y")
            {
                Placeholder plShare = (Placeholder)this.Parent.FindControl("PlhShare");
                Placeholder plDetails = (Placeholder)this.Parent.FindControl("PlhDetails");

                plDetails.Visible = false;
                plShare.Visible = true;
            }

            if (Session["ShareQuoteId"] == null)
            {
                Response.Redirect("/SG50/Pioneer Generation.aspx");
            }
            //if (Request.QueryString["Share"] != null)
            //{
            //    if (plShare != null)
            //        plShare.Visible = true;
            //    if (plDetails != null)
            //        plDetails.Visible = false;
            //}
            //else
            //{
            //    if (plShare != null)
            //        plShare.Visible = false;
            //    if (plDetails != null)
            //        plDetails.Visible = true;
            //}



            if (!IsPostBack)
            {

                if (Session["ShareQuoteId"] != null)
                {
                    currentItemId = Session["ShareQuoteId"].ToString();
                    itmPress = SG50Class.web.GetItem(currentItemId);
                    if (itmPress != null)
                    {
                        Session["StoryRedirecturl"] = hostName.Replace(" ", "%20") + LinkManager.GetItemUrl(itmPress).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "").Replace(" ", "%20");
                        Session["StoryRedirecturl2"] = hostName.Replace(" ", "%20") + LinkManager.GetItemUrl(itmContext).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "").Replace(" ", "%20");
                        //Session["ShareQuoteId"] = null;
                    }
                    else
                    {
                        Response.Redirect("/SG50/Pioneer Generation.aspx");
                    }


                }
                else
                {
                    if (currentItemId != null && currentItemId != "")
                    {
                        itmPress = SG50Class.web.GetItem(currentItemId);
                        if (itmPress != null)
                        {
                            Session["StoryRedirecturl"] = hostName.Replace(" ", "%20") + LinkManager.GetItemUrl(itmPress).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "").Replace(" ", "%20");

                        }
                        else
                        {
                            Response.Redirect("/SG50/Pioneer Generation.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("/SG50/Pioneer Generation.aspx");
                    }
                }

                lbloverlay.Visible = true;
                lbloverlayName.Visible = true;
                Item itemCmcBanner = SG50Class.web.GetItem("{2547DDFB-044A-4011-BCC4-5A16FEED3D1C}");
                if (itemCmcBanner.Fields["Image"] != null)
                {
                    Sitecore.Data.Fields.ImageField image = itemCmcBanner.Fields["Image"];
                    if (image.MediaItem != null)
                    {
                        url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem));

                        imgEvent.ImageUrl = url;
                    }
                }


                lbloverlay.Text = itmPress.Fields["Pioneers Description"].ToString();
                lbloverlayName.Text = itmPress.Fields["Pioneers Name"].ToString();


                Item ImageItem = web.GetItem(currentItemId);



                string path = Sitecore.Context.Item.Paths.Path.ToString();

                Item ImageItemTop = web.GetItem(path + "/Images/Original");

                if (ImageItemTop != null)
                {
                    Sitecore.Data.Fields.ImageField image = ImageItemTop.Fields["Image"];
                    if (image.MediaItem != null)
                    {

                        url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem));
                        imgFirst.ImageUrl = url;
                    }
                }


            }
        }

        public void ShareImage()
        {
            DataTable dtrepEvents = new DataTable();
            dtrepEvents.Columns.Add("socialThumbnail", typeof(string));
            dtrepEvents.Columns.Add("HostName", typeof(string));
            DataRow drrepEvents = dtrepEvents.NewRow();

            string currentItemId = string.Empty, url = string.Empty;
            if (Session["ShareQuoteId"] != null)
                currentItemId = Session["ShareQuoteId"].ToString();

            Item itmPress = SG50Class.web.GetItem(currentItemId), ImageItem = web.GetItem(currentItemId);

            ChildList childList = ImageItem.GetChildren();

            for (int index = 0; index < childList.Count; index++)
            {
                if (index == 0)
                {
                    ChildList childItems = childList[0].GetChildren();
                    for (int itemIndex = 0; itemIndex < childItems.Count; itemIndex++)
                    {
                        if (itemIndex == 0)
                        {
                            Sitecore.Data.Fields.ImageField image = childItems[itemIndex].Fields["Image"];
                            if (image.MediaItem != null)
                            {
                                string fbimgsrc = image.Src;
                                url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem));
                                imgFirst.ImageUrl = url;
                            }
                        }
                    }
                }
            }

            drrepEvents["socialThumbnail"] = hostName + "/" + imgFirst.ImageUrl;
            drrepEvents["HostName"] = hostName;
        }

        protected void GenImageWithOverLayText()
        {
            try
            {
                Bitmap myBitmap = new Bitmap("C:\\myImage.jpg");
                Graphics gra = Graphics.FromImage(myBitmap);

                gra.DrawString("My over lay Text ", new Font("Tahoma", 40), Brushes.White, new PointF(0, 0));

                gra.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                gra.DrawString("My\nText", new Font("Tahoma", 20), Brushes.White, new PointF(0, 0));

                StringFormat strFormat = new StringFormat();
                strFormat.Alignment = StringAlignment.Center;
                strFormat.LineAlignment = StringAlignment.Center;
                gra.DrawString("My\nText", new Font("Tahoma", 20), Brushes.White,
                    new RectangleF(0, 0, 500, 500), strFormat);
            }
            catch (Exception) { throw; }
        }

        /* Validate file before upload */
        protected bool ValidateFile(FileUpload fileControl, Label lblMsg, double minSize, double maxSize, List<string> allowedExtesnsions)
        {
            try
            {
                string ext = Path.GetExtension(fileChnageBanner.FileName);
                double originalSize; originalSize = fileChnageBanner.FileContent.Length;

                if (fileControl.HasFile)
                    if (allowedExtesnsions.IndexOf(ext) >= 0)
                        if (originalSize >= minSize * 1024 * 1024 && originalSize <= maxSize * 1024 * 1024)
                            return true;
                        else { lblMsg.Text = "File size should be between " + minSize + " and " + maxSize + " MB."; return false; }
                    else { lblMsg.Text = "Invalid file format (" + ext + "), Please upload valid file."; return false; }
                else { lblMsg.Text = "Please select the file."; return false; }
            }
            catch (Exception msg)
            {
                lblMsg.Text = msg.StackTrace + "" + msg.InnerException + "" + msg.Message;
                return false;
            }
        }
        /// <summary>
        /// Change Banner of PG Share Quote Layout Page in button click
        /// </summary>


        public void GetBitImageFromImage(string waterMarkText, string completeFileName, string expectedFileFormatExtension, string quotationTextFontStyle, int quotationFontSize, Color quoteForeColor, string newfile)
        {
            try
            {

                waterMarkText = waterMarkText.Replace("<br/>", "\n");
                string serverpath = Server.MapPath("/SG50/layouts/PG/Share Quote Images/");
                //Response.Write(waterMarkText);

                Bitmap bmp = new Bitmap(serverpath + completeFileName, true);

                //  Graphics grp = Graphics.FromImage

                byte[] photoBytes = File.ReadAllBytes(serverpath + completeFileName);
                ISupportedImageFormat format = new JpegFormat { Quality = 70 };
                Size size = new Size(1280, 740);
                TextLayer text = new TextLayer();
                Font font = new System.Drawing.Font("Open Sans", 54);
                text.Text = waterMarkText;
                text.FontColor = Color.White;
                text.FontFamily = font.FontFamily;
                text.DropShadow = true;
                text.FontSize = 54;
                text.Style = FontStyle.Bold;

                TextLayer textName = new TextLayer();
                Font fontName = new System.Drawing.Font("Open Sans", 34);

                if (waterMarkText.Length >= 55)
                    textName.Text = "\n" + "\n" + "\n" + "\n" + "\n" + lbloverlayName.Text;
                else
                    textName.Text = "\n" + "\n" + "\n" + lbloverlayName.Text;
                textName.FontColor = Color.White;
                textName.FontFamily = fontName.FontFamily;
                textName.DropShadow = true;
                textName.FontSize = 34;
                textName.Style = FontStyle.Bold;

                using (MemoryStream inStream = new MemoryStream(photoBytes))
                {
                    using (MemoryStream outStream = new MemoryStream())
                    {
                        // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                        using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                        {
                            // Load, resize, set the format and quality and save an image.
                            imageFactory.Load(inStream)
                                        .Resize(size)
                                        .Format(format)
                                        .Watermark(text)
                                        .Watermark(textName)
                                        .Save(outStream);
                        }
                        System.Drawing.Image img = System.Drawing.Image.FromStream(outStream);
                        img.Save(serverpath + newfile + "." + expectedFileFormatExtension);

                    }
                }





                //using (Bitmap bmp = new Bitmap(serverpath + completeFileName, true))
                //{
                //    using (Graphics grp = Graphics.FromImage(bmp))
                //    {
                //        grp.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                //        StringFormat strFormat = new StringFormat();
                //        strFormat.Alignment = StringAlignment.Center;
                //        Brush brush = new SolidBrush(quoteForeColor);
                //        Font font = new System.Drawing.Font(quotationTextFontStyle, quotationFontSize, FontStyle.Bold, GraphicsUnit.Pixel);
                //        Font font1 = new System.Drawing.Font("Open Sans", 34, FontStyle.Bold, GraphicsUnit.Pixel);
                //        SizeF textSize = new SizeF();

                //        textSize = grp.MeasureString(waterMarkText, font);

                //        int textYPosition = 5;
                //        Point position1;
                //        if (textSize.Width > bmp.Width)
                //        {
                //            textYPosition = (int)(textSize.Width / bmp.Width) + 5;
                //            position1 = new Point(10, (bmp.Height - ((int)textSize.Height + font.Height * textYPosition)) + (int)(textSize.Height + font.Height));
                //        }
                //        else
                //        {
                //            position1 = new Point(10, (bmp.Height - ((int)textSize.Height + font.Height * textYPosition)) + (int)(textSize.Height));
                //        }

                //        Point position = new Point(10, (bmp.Height - ((int)textSize.Height + font.Height * textYPosition)));
                //        RectangleF drawRect = new RectangleF(position.X, position.Y, bmp.Width, font.Height * textYPosition);

                //        grp.DrawString(waterMarkText, font, brush, drawRect, strFormat);

                //        //Point position1 = new Point(10, (bmp.Height - ((int)textSize.Height + font.Height * textYPosition)) + (int)(textSize.Height + font.Height));
                //        RectangleF drawRect1 = new RectangleF(position1.X, position1.Y, bmp.Width, font.Height * textYPosition);
                //        grp.DrawString(lbloverlayName.Text, font1, brush, drawRect1, strFormat);

                //        bmp.Save(serverpath + newfile + "." + expectedFileFormatExtension);

                //    }
                //}
            }
            catch (Exception) { throw; }
        }

        public void FaceBookSharing(object sender, EventArgs e)
        {
            ChangeShareImage();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "FacebookCalling", "FacebookCalling();", true);

        }
        public void TwitterSharing(object sender, EventArgs e)
        {
            ChangeShareImage();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "newPopup", "newPopup();", true);
        }

        public void ChangeShareImage()
        {
            if (fileChnageBanner.HasFile)
            {
                Item parentItem = master.GetItem(Session["ShareQuoteId"].ToString());
                Item Folder = null;
                Item ImagenewForm = null;

                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    if (System.Convert.ToInt32(parentItem.GetChildren().Count.ToString()) < 2)
                    {
                        Folder = master.GetItem("/sitecore/content/SG50/Pioneer Generation/Images");
                        Folder.CopyTo(parentItem, "Share Quote Images");
                    }


                    if (parentItem != null)
                    {

                        var source = master;
                        var target = Sitecore.Data.Database.GetDatabase("web");

                        var options = new PublishOptions(source, target, PublishMode.SingleItem, parentItem.Language, DateTime.Now)
                        {
                            RootItem = parentItem,
                            Deep = true,
                        };

                        var publisher = new Publisher(options);
                        publisher.Publish();

                    }
                }

                string[] allowedExtension = new string[] { ".jpg", ".png", ".gif" };
                bool Contains = true;
                string filename = string.Empty;

                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    if (fileChnageBanner.HasFile)
                    {
                        string ext = System.IO.Path.GetExtension(fileChnageBanner.FileName);

                        Sitecore.Resources.Media.MediaCreatorOptions optionsM = new Sitecore.Resources.Media.MediaCreatorOptions();
                        optionsM.Database = master;
                        optionsM.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                        optionsM.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                        filename = fileChnageBanner.FileName.Replace(".jpg", "").Replace(".png", "").Replace(".gif", "");
                        filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                        filename += "_Quote" + DateTime.Now.ToString("yyMMdd") + DateTime.Now.ToString("hhmmss");

                        optionsM.Destination = "/sitecore/media library/SG50/PG/Share Quote Images/" + filename;
                        string Destination = "/sitecore/media library/SG50/PG/Share Quote Images/" + filename;
                        fileChnageBanner.SaveAs(Server.MapPath("/SG50/layouts/PG/Share Quote Images/" + fileChnageBanner.FileName));


                        GetBitImageFromImage(lbloverlay.Text, fileChnageBanner.FileName, "png", "Open Sans", 54, System.Drawing.Color.White, filename);


                        optionsM.FileBased = false;
                        Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                        Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/layouts/PG/Share Quote Images/" + filename + ".png"), optionsM);

                        string path = parentItem.Paths.Path.ToString();
                        Item ImageItem1 = web.GetItem(path + "/Share Quote Images");

                        Sitecore.Data.Items.TemplateItem Imagetemplate = master.GetItem("/sitecore/templates/SG50/Pioneer Generation/PGStrory Image Banner");
                        Item ImageItem = master.GetItem(ImageItem1.ID.ToString());
                        string ImageItemName = filename;
                        ImagenewForm = ImageItem.Add(ImageItemName.ToString(), Imagetemplate);
                        ImagenewForm.Editing.BeginEdit();



                        Sitecore.Data.Items.MediaItem image = parentItem.Database.GetItem("/sitecore/media library/SG50/PG/Share Quote Images/" + filename);
                        if (image != null)
                        {
                            Sitecore.Data.Fields.ImageField imagefield = ImagenewForm.Fields["Image"];
                            imagefield.Alt = image.Alt;
                            imagefield.MediaID = image.ID;
                            imagefield.Src = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image);

                        }
                        ImagenewForm.Editing.EndEdit();

                    }
                }

                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    if (parentItem != null)
                    {

                        var source = master;
                        var target = Sitecore.Data.Database.GetDatabase("web");

                        var options = new PublishOptions(source, target, PublishMode.SingleItem, parentItem.Language, DateTime.Now)
                        {
                            RootItem = parentItem,
                            Deep = true
                        };

                        var publisher = new Publisher(options);
                        publisher.Publish();

                        var itemMedia = master.GetItem("{276C6895-9758-48FE-927C-4B1642E2A6D4}");//{FC566152-B83E-447D-9312-55C583A9575D}
                        if (itemMedia != null)
                        {
                            var mediaOptions = new PublishOptions(source, target, PublishMode.SingleItem, itemMedia.Language, DateTime.Now)
                            {
                                RootItem = itemMedia,
                                Deep = true
                            };

                            var mediaPublisher = new Publisher(mediaOptions);
                            mediaPublisher.Publish();
                        }

                    }
                }

                Sitecore.Data.Items.MediaItem imageUpdate = parentItem.Database.GetItem("/sitecore/media library/SG50/PG/Share Quote Images/" + filename);
                imgFirst.ImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageUpdate);
                //lbloverlay.Visible = false;
                lbloverlay.Attributes.Add("Style", "Display:none");
                lbloverlayName.Attributes.Add("Style", "Display:none");
                Session["StoryRedirecturl"] = hostName.Replace(" ", "%20") + LinkManager.GetItemUrl(parentItem).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "").Replace(" ", "%20") + "?mitem=" + SG50Class.StripUnwantedString(imageUpdate.ID.ToString().Replace("{", "").Replace("}", ""));
                //string twitterimg = "<meta property=\"twitter:image\" content=\"" + hostName + imgFirst.ImageUrl.Replace(" ", "%20") + "\" />";
                //PGlitMetaTag.Text = twitterimg;
            }
            else
            {
                //try
                //{


                string domainName = Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Host Name"].Value.ToString();
                string url = string.Empty;

                string path = Sitecore.Context.Item.Paths.Path.ToString();

                Item ImageItemTop = web.GetItem(path + "/Images/Original");

                if (ImageItemTop != null)
                {
                    Sitecore.Data.Fields.ImageField image = ImageItemTop.Fields["Image"];
                    if (image.MediaItem != null)
                    {
                        url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem));
                    }
                }



                string serverpath = Server.MapPath("/SG50/layouts/PG/Share Quote Images/");

                byte[] b = null;

                WebClient c = new WebClient();
                b = c.DownloadData("http://delivery-app.singapore50.sg:81" + url);
                string newfile = "originalShare" + "-" + DateTime.Now.ToString("yyMMdd") + "-" + DateTime.Now.ToString("hhmmss") + ".jpeg";
                //System.Drawing.Text.PrivateFontCollection col = new System.Drawing.Text.PrivateFontCollection();
                //col.AddFontFile(Server.MapPath("/SG50/include/PG/css/OpenSans-Semibold.ttf"));
                ISupportedImageFormat format = new JpegFormat { Quality = 70 };
                Size size = new Size(1280, 740);
                TextLayer text = new TextLayer();
                Font font = new System.Drawing.Font("Open Sans", 54, FontStyle.Bold, GraphicsUnit.Pixel);
                text.Text = lbloverlay.Text;
                text.FontColor = Color.White;
                text.FontFamily = font.FontFamily;
                text.DropShadow = true;
                text.Style = FontStyle.Bold;
                TextLayer textName = new TextLayer();
                Font fontName = new System.Drawing.Font("Open Sans", 34);

                if (lbloverlay.Text.Length >= 55)
                    textName.Text = "\n" + "\n" + "\n" + "\n" + "\n" + lbloverlayName.Text;
                else
                    textName.Text = "\n" + "\n" + "\n" + lbloverlayName.Text;
                textName.FontColor = Color.White;
                textName.FontFamily = fontName.FontFamily;
                textName.DropShadow = true;
                textName.FontSize = 34;
                textName.Style = FontStyle.Bold;
                using (MemoryStream inStream = new MemoryStream(b))
                {

                    using (MemoryStream outStream = new MemoryStream())
                    {
                        // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                        using (ImageFactory imageFactory = new ImageFactory(preserveExifData:true))
                        {
                            // Load, resize, set the format and quality and save an image.
                            imageFactory.Load(inStream)
                                        .Resize(size)
                                        .Format(format)
                                        .Watermark(text)
                                        .Watermark(textName)
                                        .Save(outStream);
                        }
                        System.Drawing.Image img = System.Drawing.Image.FromStream(outStream);
                        img.Save(serverpath + newfile);
                        lbloverlay.Attributes.Add("Style", "Display:none");
                        lbloverlayName.Attributes.Add("Style", "Display:none");

                    }
                }

                               

                Item parentItem = master.GetItem(Session["ShareQuoteId"].ToString());
                string filename = string.Empty;
                Sitecore.Resources.Media.MediaCreatorOptions optionsM = new Sitecore.Resources.Media.MediaCreatorOptions();
                optionsM.Database = master;
                optionsM.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                optionsM.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                filename = newfile.Replace(".jpg", "").Replace(".png", "").Replace(".gif", "").Replace(".jpeg", "");
                filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                optionsM.Destination = "/sitecore/media library/SG50/PG/Share Quote Images/" + filename;
                optionsM.FileBased = false;
                Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/layouts/PG/Share Quote Images/" + newfile), optionsM);

                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    if (parentItem != null)
                    {

                        var source = master;
                        var target = Sitecore.Data.Database.GetDatabase("web");

                        var options = new PublishOptions(source, target, PublishMode.SingleItem, parentItem.Language, DateTime.Now)
                        {
                            RootItem = parentItem,
                            Deep = true
                        };

                        var publisher = new Publisher(options);
                        publisher.Publish();

                        var itemMedia = master.GetItem("{276C6895-9758-48FE-927C-4B1642E2A6D4}");//{FC566152-B83E-447D-9312-55C583A9575D}
                        if (itemMedia != null)
                        {
                            var mediaOptions = new PublishOptions(source, target, PublishMode.SingleItem, itemMedia.Language, DateTime.Now)
                            {
                                RootItem = itemMedia,
                                Deep = true
                            };

                            var mediaPublisher = new Publisher(mediaOptions);
                            mediaPublisher.Publish();
                        }

                    }
                }

                Sitecore.Data.Items.MediaItem imageUpdate = parentItem.Database.GetItem("/sitecore/media library/SG50/PG/Share Quote Images/" + filename);
                imgFirst.ImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imageUpdate);

                Session["StoryRedirecturl"] = hostName.Replace(" ", "%20") + LinkManager.GetItemUrl(parentItem).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "").Replace(" ", "%20") + "?mitem=" + SG50Class.StripUnwantedString(imageUpdate.ID.ToString().Replace("{", "").Replace("}", ""));

                //}
                //catch (Exception) { throw; }
            }
        }
    }
}