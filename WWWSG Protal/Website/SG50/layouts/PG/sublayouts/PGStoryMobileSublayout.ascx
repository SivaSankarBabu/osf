﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Pgstorymobilesublayout.PgstorymobilesublayoutSublayout" CodeFile="~/SG50/layouts/PG/sublayouts/PGStoryMobileSublayout.ascx.cs" %>


<script src="/SG50/include/PG/js/jquery.validate.min.js" type="text/javascript" language="javascript"></script>

<script src="/SG50/include/js/jquery-filestyle.min.js"></script>

<link href="/SG50/include/css/jquery-filestyle1.css" rel="stylesheet" />

<script>
    $(document).ready(function() {
        $(":file").jfilestyle({ buttonText: "Browse", icon: false });

    });

    function previewFile() {
        var preview = document.querySelector('#<%=imgFirst.ClientID %>');


        var file = document.querySelector('#<%=fileOriginalImage.ClientID %>').files[0];
        var reader = new FileReader();

        reader.onloadend = function() {
            preview.src = reader.result;
            // PageMethods.ChangeShareImage();
        }

        if (file) {
            document.getElementById("imgdv").style.display = "inline";
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
    }
</script>

<style>
   
    div.jquery-filestyle input[type="text"][disabled]
    {
        background: none repeat scroll 0 0 #fff;
        height: 39px !important;
        margin: 0 -10px 10px 0 !important;
        width: 470px !important;
        color: #000 !important;
    }
    div.jquery-filestyle label
    {
        height: 40px !important;
        line-height: 27px !important;
    }
    .error
    {
        color: #FF0000;
        padding-top: 5px;
        display: inline-block;
    }
    #t_c-error
    {
        float: left;
        display: block;
        position: absolute;
        margin-top: 19px;
    }
    #form_of_agreement-error
    {
        float: left;
        display: block;
        position: absolute;
        margin-top: 19px;
    }
     @media screen and (min-width: 300px) and (max-width: 768px)
    {
        div.jquery-filestyle input[type="text"][disabled]
        {
            width: 265px !important;
        }
        div.jquery-filestyle label
        {
            margin: 0px 4px 10px 3px;
        }
    }
</style>

<script type="text/javascript">
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57 || keyCode >= 65 && keyCode <= 90 || keyCode >= 97 && keyCode <= 122 || keyCode == 9 || keyCode == 32) || specialKeys.indexOf(keyCode) != -1);
        return ret;
    }
</script>
<section class="share-story-section">
    <div class="custom-container">
        <h3>
            <asp:HiddenField runat="server" ID="_repostcheckcode" />
            <sc:Text ID="txtTitle" Field="Story Title" runat="server" />
        </h3>
        <p>
            <sc:Text ID="txtDescription" Field="Story Description" runat="server" />
        </p>

        <h5 class="h5">
            <sc:Text ID="txtPioneerTitle" Field="pioneer Title" runat="server" />
        </h5>

        <div class="fields-group">
            <label>
                <sup>*</sup><sc:Text ID="txtQuestionsHeading" Field="Questions Heading" runat="server" />
            </label>
            <ul class="questions-list">
                <sc:Text ID="txtQuestions" Field="Questions" runat="server" />
            </ul>
            <asp:TextBox ID="txtStoryArea" placeholder="Max 2000 characters" TextMode="MultiLine" runat="server" CssClass="form-textarea ValidationRequired"></asp:TextBox>

        </div>
        <div class="fields-group">
            <label><sup>*</sup>What’s the one thing he/she would tell the next generation of Pioneers?</label>
            <asp:TextBox ID="txtPioneersArea" placeholder="Max 140 characters" TextMode="MultiLine" runat="server" CssClass="form-textarea form-textarea140 ValidationRequired"></asp:TextBox>

        </div>

        <div class="fields-group" style="display: none">
            <div class="form-col2">
                <label><sup>*</sup>Categories</label>
                <asp:DropDownList ID="ddlCategories" runat="server" CssClass="form-select ValidationRequired">

                    <asp:ListItem Value="2" Text="Kampong Chief"></asp:ListItem>
                    <asp:ListItem Value="3" Text="The Rocksteady"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Trailblazer"></asp:ListItem>
                    <asp:ListItem Value="5" Text="The Giver"></asp:ListItem>
                    <asp:ListItem Value="6" Text="The Die-Hard"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="fields-group">
            <div class="form-col2">
                <label><sup>*</sup>Pioneer's Name</label>
                <asp:TextBox ID="txtPioneerName" onkeypress="return IsNumeric(event);" AutoCompleteType="Disabled" autocomplete="off" placeholder="100 characters" runat="server" CssClass="form-select ValidationRequired"></asp:TextBox>
            </div>
            <div class="form-col2-right">
                <label><sup>*</sup>Age</label>
                <asp:DropDownList ID="ddlAge" runat="server" CssClass="form-select">
                    <asp:ListItem Value="1" Text="50-60"></asp:ListItem>
                    <asp:ListItem Value="2" Text="60-70"></asp:ListItem>
                    <asp:ListItem Value="3" Text="70-80"></asp:ListItem>
                    <asp:ListItem Value="4" Text="80 & above"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="fields-group browse-group">
            <div class="form-col2">
                <label><sup>*</sup>Make it real, attach a photo of your pioneer</label>

            </div>

        </div>
        <asp:FileUpload ID="fileOriginalImage" onchange="previewFile()" CssClass="jfilestyle" runat="server" />
         <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                ErrorMessage="Please select Image" Style="font-size: 14px; font-weight: bolder" ForeColor="Red" ValidationGroup="submit" ControlToValidate="fileOriginalImage"></asp:RequiredFieldValidator>
        <div class="fields-group paddingbtm0">
            <div class="form-col2 recommend">Recommended dimension 1280x740 pixels.</div>
        </div>
        <div id="imgdv" style="display: none;" >
            <asp:Image ID="imgFirst" Width="150px" Height="150px" runat="server" />
        </div>
        <br />
        <br />
        <br />
        <asp:FileUpload ID="filename1" CssClass="jfilestyle" runat="server" />
        <br />
        <asp:FileUpload ID="filename2" CssClass="jfilestyle" runat="server" />
        <br />
        <asp:FileUpload ID="filename3" CssClass="jfilestyle" runat="server" />


        <h5 class="h5 show-lg">Your Profile</h5>
        <h5 class="h5 show-sm ">Your Profile</h5>
        <div class="fields-group">
            <div class="form-col2">
                <label><sup>*</sup>Your Name</label>
                <asp:TextBox ID="txtName" AutoCompleteType="Disabled" autocomplete="off" placeholder="100 characters" runat="server" CssClass="form-select ValidationRequired"></asp:TextBox>
            </div>
            <div class="form-col2-right">
                <label><sup>*</sup>Email Address</label>
                <asp:TextBox ID="txtEmail" AutoCompleteType="Disabled" autocomplete="off" placeholder="100 characters" runat="server" CssClass="form-select"></asp:TextBox>

            </div>
        </div>

        <hr class="hr">

        <div class="fields-group">
            <div class="form-chk" id="DivTerms" runat="server" visible="false">
                <input class="form-checkbox" type="checkbox" name="t_c">

                <div class="chk-label">
                    <sup>*</sup><sc:Text ID="Conditions" Field="TC Text" runat="server" />
                    <sc:Link Field="TC URL" runat="server" ID="hyplknTC">
                        <sc:Text Field="TC URL Text" runat="server" ID="txtterms" />
                    </sc:Link>
                </div>
            </div>
            <div class="form-chk" id="DivAgreement" runat="server" visible="false">
                <input class="form-checkbox" type="checkbox" name="form_of_agreement">
                <div class="chk-label">
                    <sup>*</sup><sc:Text ID="Text1" Field="Agreement Text" runat="server" />

                    <sc:Link Field="Agreement URL" runat="server" ID="Link1">
                        <sc:Text Field="Agreement URL Text" Style="color: red" runat="server" ID="Text2" />
                    </sc:Link>
                </div>
            </div>

        </div>
        <br />
        <br />
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txt_CaptchaVal" runat="server" Visible="false"></asp:TextBox>
                    <asp:Image ID="Image1" runat="server" />

                    <asp:TextBox ID="txt_Captcha" MaxLength="4" Style="vertical-align: top" AutoCompleteType="Disabled" autocomplete="off" placeholder="Enter Captcha" Width="200px" runat="server" CssClass="form-select"></asp:TextBox>

                    <asp:ImageButton ID="Button1" Style="vertical-align: top" runat="server" ImageUrl="/SG50/images/PG/refreshIcon.gif"
                        CausesValidation="false" Text="refresh" CssClass="cancel" OnClick="Button1_Click1" />
                    <br />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button1" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                ErrorMessage="Please enter Captcha" Style="font-size: 14px;  font-weight: bolder" ForeColor="Red" ValidationGroup="submit" ControlToValidate="txt_Captcha"></asp:RequiredFieldValidator>
        </div>
        <div class="fields-group form-submit">

            <asp:Button CssClass="submit-button" ID="btnSubmit" ValidationGroup="submit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />

        </div>

        <div class="clearfix"></div>
    </div>
</section>

<script type="text/javascript" language="javascript">



    $(document).ready(function() {


        $("#mainform").validate({
            ignore: [],
            rules: {
                '<%=txtEmail.UniqueID%>': {
                    required: true,
                    email: true
                },
                '<%=txtName.UniqueID%>': {
                    required: true,
                    maxlength: 100
                },
                '<%=txtPioneerName.UniqueID%>': {
                    required: true,
                    maxlength: 100
                },
                '<%=txtStoryArea.UniqueID%>': {
                    required: true,
                    maxlength: 2000
                },
                '<%=txtPioneersArea.UniqueID%>': {
                    required: true,
                    maxlength: 140
                },
                t_c: {
                    required: true
                },
                form_of_agreement: {
                    required: true
                }
                //                ,
                //                cropped_image_name: {
                //                    required: true
                //                }
                //                ,
                //                cropped_image_name_mobile: {
                //                    required: true
                //                }

            }, // Specify the validation error messages
            messages: {
                '<%=txtEmail.UniqueID%>': {
                    required: "Please enter Email Address",
                    email: "Please enter a valid Email Address"
                },
                '<%=txtName.UniqueID%>': {
                    required: "Please enter Your Name",
                    maxlength: "Your name must not be exceeds 100 characters long"
                },
                '<%=txtPioneerName.UniqueID%>': {
                    required: "Please enter Pioneer's Name",
                    maxlength: "Pioneer's must not be exceeds 100 characters long"
                },
                '<%=txtStoryArea.UniqueID%>': {
                    required: "Please enter information",
                    maxlength: "Information must not be exceeds 2000 characters long"
                },
                '<%=txtPioneersArea.UniqueID%>': {
                    required: "Please enter information",
                    maxlength: "Information must not be exceeds 140 characters long"
                },

                //                cropped_image_name: {
                //                    required: "Please Select Image"
                //                }
                // ,
                //                cropped_image_name_mobile: {
                //                    required: "Please Select Image"
                //                },
                submitHandler: function(mainform) {

                    mainform.submit();
                }

            }
        });
        // delete code for image delete for desktop
        $('#deleteImage').click(function() {
            $('#Test').empty();
            $('#crop_image_hidden').val('');
            $("#filetype").empty();
            $('#fileName').empty().html('<span class="format">Format acceptable: JPG, PNG; File size limit: 5MB</span>');
            $('#Test').hide();
            $('#deleteImage').hide();
        });
        // delete code for image delete for mobile
        $('#deleteImageMobile').click(function() {
            $('#area img').remove();
            $('#crop-image-hidden-mobile').val('');
            $("#filetype").empty();
            $('#fileName').empty().html('<span class="format">Format acceptable: JPG, PNG; File size limit: 5MB</span>');
            $('#deleteImageMobile').hide();
        });
        $('#area u').click(function() {
            $('input[name=photo]').trigger('click');
        });

        $('input[name=photo]').change(function(e) {
            var file = e.target.files[0];
            var reader = new FileReader();

            reader.onloadend = function() {
                $('#crop-original-image-hidden-mobile').val(reader.result);
                var txtOriginalb5 = $("[id$='txtOriginalb4']");
                txtOriginalb5.val(reader.result);

            }
            reader.readAsDataURL(file);


            $("#filetype").empty();
            $('#fileName').empty().append(file.name);


            //alert(reader.result);
            //image format and size validation
            if (file.type != "image/jpeg" && file.type != "image/png") {
                $("#filetype").empty().append('Please Enter Valid Image File (jpeg, png)');
                $('#area img').remove();
                $('#crop-image-hidden-mobile').val('');
                $('#deleteImageMobile').hide();
                return false;
            }
            if (file.size > 5000000) {
                $("#filetype").empty().append('Uploaded file size not permitted (max 5 MB)');
                $('#area img').remove();
                $('#crop-image-hidden-mobile').val('');
                $('#deleteImageMobile').hide();
                return false;
            }


            // RESET
            $('#area p span').css('width', 0 + "%").html('');
            $('#area img, #area canvas').remove();
            $('#area i').html(JSON.stringify(e.target.files[0]).replace(/,/g, ", <br/>"));

            // CANVAS RESIZING
            canvasResize(file, {
                width: 376,
                height: 376,
                crop: true,
                quality: 100,
                rotate: 0,
                callback: function(data, width, height) {

                    // SHOW AS AN IMAGE
                    // =================================================
                    var img = new Image();
                    img.onload = function() {

                        $(this).css({
                            'margin': '10px auto',
                            'width': width,
                            'height': height
                        }).appendTo('#area div');

                    };

                    $(img).attr('src', data);
                    $('#crop-image-hidden-mobile').val(data);
                    var txtb5 = $("[id$='txtb4']");
                    // $('#cropped_image_name').val("jpg");
                    txtb5.val(data);

                    $('#crop-image-hidden-mobile-error').hide();
                    $('#crop_image_hidden-error').hide();
                    $('#deleteImageMobile').show();


                    $('#mainform').submit(function() {
                        if (Page_IsValid)
                            $('input[type=submit]', this).prop('disabled', true);
                    });
                    // /SHOW AS AN IMAGE
                    // =================================================


                    // IMAGE UPLOADING
                    // =================================================

                    // Create a new formdata
                    //                    var fd = new FormData();
                    //                    // Add file data
                    //                    var f = canvasResize('dataURLtoBlob', data);
                    //                    f.name = file.name;
                    //                    fd.append($('#area input').attr('name'), f);



                    // /IMAGE UPLOADING
                    // =================================================               
                }
            });

        });
    });
</script>

