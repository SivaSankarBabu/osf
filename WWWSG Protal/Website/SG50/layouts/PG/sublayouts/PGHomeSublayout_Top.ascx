﻿<%@ Control Language="c#" AutoEventWireup="true" Inherits="Layouts.Pghomesublayout_top.Pghomesublayout_topSublayout"
    CodeFile="~/SG50/layouts/PG/sublayouts/PGHomeSublayout_Top.ascx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="application/javascript">

var Myslider = {};
    $(document).ready(function() {
        if ($(window).width() <= 1024) {
            Myslider.slider = $('.bxslider').bxSlider({
                pause: 7000,
                touchEnabled: false,
                autoStart: false,
                useCSS:false,
                auto: false
            });
        } else {
            Myslider.slider = $('.bxslider').bxSlider({
                pause: 7000,
                useCSS:false,
                auto: false
            });
        }
        $('.bx-next, .bx-prev, .bx-pager-link').click(function() {
            pauseVideo();
        });
    });
    
  
    
          var ga = document.createElement('script');
          ga.type = 'text/javascript';
          ga.async = false;
          ga.src = 'https://www.youtube.com/player_api';
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(ga, s);
          var done = false;
          var player;


          var playerInfoList = [<%=vidurl %>];

          function onYouTubeIframeAPIReady() {
              if (typeof playerInfoList === 'undefined')
                  return;

              for (var i = 0; i < playerInfoList.length; i++) {
                   player = createPlayer(playerInfoList[i]);
              }
          }
          function createPlayer(playerInfo) {
              return new YT.Player(playerInfo.id, {
                  height: playerInfo.height,
                  width: playerInfo.width,
                  videoId: playerInfo.videoId,
                  playerVars: {
                      controls: 0,
                      showinfo: 0,
                      rel: 0,
                      disablekb: 1
                  },
                  events: {
                      'onReady': onPlayerReady,
                      'onStateChange': onPlayerStateChange
                  }
              });
          }

//          function onYouTubePlayerAPIReady() {
//              player = new YT.Player('howToVideo', {
//                  height: 'auto',
//                  width: '100%',
//                  videoId: 'ObaLqHHTiJ0',
//                  playerVars: {
//                      controls: 0,
//                      showinfo: 0,
//                      rel: 0,
//                      disablekb: 1
//                  },
//                  events: {
//                      'onReady': onPlayerReady,
//                      'onStateChange': onPlayerStateChange
//                  }
//              });
//          }
          function onPlayerReady(evt) {
          //alert($(window).width());
          if ($(window).width() >= 1024) {
           //alert('123');
           Myslider.slider.startAuto();
          }
              console.log('onPlayerReady', evt);

          }
          function onPlayerStateChange(evt) {

              console.log('onPlayerStateChange', evt);
              Myslider.slider.stopAuto();
              if (evt.data == YT.PlayerState.PLAYING && !done) {
                 // setTimeout(stopVideo, 6000);
                  done = true;

              }
          }

          function stopVideo() {
              console.log('stopVideo');
              player.stopVideo();
          }
          function pauseVideo() {
              console.log('pauseVideo');
              player.pauseVideo();
          }


</script>

<script type="text/javascript">
    function ImageChange() {
        if ($(window).width() <= 640) {
            // $(".item > img").attr('src', "/SG50/images/PG/slider/lowres/image01.jpg")
        }
        else {
            // $('.item > img').attr('src', '/SG50/images/PG/slider/highres/image01.jpg')
        };
    }
       
</script>

<link href="/SG50/include/PG/bx-slider/jquery.bxslider.css" rel="stylesheet" />

<script src="/SG50/include/PG/bx-slider/jquery.bxslider.js" type="text/javascript"></script>

<div style="height: auto" class="slider-container home-slider" id="slidercontent"
    runat="server">
</div>

<script type="text/javascript">



    //    // Slider attributes
    //    if ($(window).width() <= 1024) {
    //        $('.bxslider').bxSlider({
    //            pause: 5000,
    //            touchEnabled: false,
    //            autoStart: false,
    //            auto: false
    //        });
    //    }
    //    else {
    //        $('.bxslider').bxSlider({
    //            pause: 5000,
    //            auto: true
    //        });
    //    }

    // previous and next events 
  

</script>

<section class="pioneer-bg">
        <div class="pioneer-container">
            <h2 class="pioneer-h2">

                <img src="/SG50/images/PG/since1965.png">
              
            </h2>

            <p>
            <sc:Text ID="txtIntroDescription" Field="Introduction Description" runat="server" />
            </p>

          <a onserverclick="NextStory" id="lnkIntroLink" class="sg-button margin-top20 story-button" runat="server">
                <sc:Text ID="txtIntroBtn" runat="server" Field="Introduction Button Text" />
            </a>


        </div>
    </section>
