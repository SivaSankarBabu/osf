﻿using System;
using Sitecore.Data.Items;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Links;
using Sitecore.Diagnostics;
using System.Web;
using System.Web.UI.WebControls;
using Sitecore.Data;
namespace Layouts.Pg_footer_menu_sublayout {
  
	/// <summary>
	/// Summary description for Pg_footer_menu_sublayoutSublayout
	/// </summary>
  public partial class Pg_footer_menu_sublayoutSublayout : System.Web.UI.UserControl 
	{
     #region ID_DECLARATIONS
        public static string str_FAQs_Folder_ID = "{71C08395-7C9F-4781-ADC2-33CC43D55E02}";// /sitecore/content/SG50/Settings/FAQs
        public static string str_CF_FAQs_Folder_ID = "{EB2B32D7-5F10-48A8-8DC2-9F2C62BFE674}";// /sitecore/content/SG50/Settings/Celebration Fund FAQs
        public static string str_FAQs_template_ID = "{6D6AFA70-2CC9-488C-998F-A1B5873C77F9}";// /sitecore/templates/SG50/FAQ
        #endregion

        #region ITEM_DECLARATIONS
        public static Database web = Sitecore.Configuration.Factory.GetDatabase("web"); // get the web database
        public static Item itm_FAQs_Folder = web.GetItem(str_FAQs_Folder_ID); // item of FAQ folder
        public static Item itm_CF_FAQs_Folder = web.GetItem(str_CF_FAQs_Folder_ID); // item of Celebration Fund FAQ folder
        public static Item itm_FAQs_template = web.GetItem(str_FAQs_template_ID); // item of FAQ folder
        #endregion

        Item itmContext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e) 
        {
          // Put user code to initialize the page here
            Item itmContext = Sitecore.Context.Item;
            bindFAQs();
            bindCFFAQs();
        }

        protected string getContactUsLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Contact_Us_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Contact_Us_Item_ID));
            }
            return link;
        }

       
	
        protected string getSitemapLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Sitemap_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Sitemap_Item_ID));
            }
            return link;
        }

        protected void bindFAQs()
        {
            try
            {
                DataTable tempTblforFAQs = new DataTable();
                int faqNo = 0;
                tempTblforFAQs.Columns.Add("FAQ_ID", typeof(string));
                tempTblforFAQs.Columns.Add("FAQ_No", typeof(string));
                tempTblforFAQs.Columns.Add("FAQ_ID_No", typeof(string)); // for div id
                tempTblforFAQs.Columns.Add("FAQ_Question", typeof(string));
                tempTblforFAQs.Columns.Add("FAQ_Answer", typeof(string));
                tempTblforFAQs.Columns.Add("FAQ_OnClick", typeof(string)); // for javascript function
                tempTblforFAQs.Columns.Add("FAQ_Con_ID", typeof(string));

                DataRow tempRowforFAQs;

                List<Item> allFAQs = (from a in itm_FAQs_Folder.Axes.GetDescendants()
                                      where (a.TemplateID.ToString().Equals(str_FAQs_template_ID))
                                      select a).Take(100).ToList();

                foreach (Item i in allFAQs)
                {
                    faqNo++;
                    tempRowforFAQs = tempTblforFAQs.NewRow();
                    tempRowforFAQs["FAQ_ID"] = i.ID.ToString();
                    tempRowforFAQs["FAQ_No"] = faqNo.ToString();
                    tempRowforFAQs["FAQ_ID_No"] = "faq" + faqNo.ToString() + "ID";
                    tempRowforFAQs["FAQ_Question"] = i["Question"];
                    tempRowforFAQs["FAQ_Answer"] = i["Answer"];
                    tempRowforFAQs["FAQ_OnClick"] = "HideContent('faq" + faqNo.ToString() + "ID', 'faq" + faqNo.ToString() + "ConID'); return false";
                    tempRowforFAQs["FAQ_Con_ID"] = "faq" + faqNo.ToString() + "ConID";
                    tempTblforFAQs.Rows.Add(tempRowforFAQs);
                    repFAQs.DataSource = tempTblforFAQs;
                    repFAQs.DataBind();
                }
                
            }
            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
        }

        protected void bindCFFAQs()
        {
            try
            {
                DataTable tempTblCFforFAQs = new DataTable();
                int faqNo = 0;
                tempTblCFforFAQs.Columns.Add("FAQ_ID", typeof(string));
                tempTblCFforFAQs.Columns.Add("FAQ_No", typeof(string));
                tempTblCFforFAQs.Columns.Add("FAQ_ID_No", typeof(string)); // for div id
                tempTblCFforFAQs.Columns.Add("FAQ_Question", typeof(string));
                tempTblCFforFAQs.Columns.Add("FAQ_Answer", typeof(string));
                tempTblCFforFAQs.Columns.Add("FAQ_OnClick", typeof(string)); // for javascript function
                tempTblCFforFAQs.Columns.Add("FAQ_Con_ID", typeof(string));

                DataRow tempRowforCFFAQs;

                List<Item> allFAQs = (from a in itm_CF_FAQs_Folder.Axes.GetDescendants()
                                      where (a.TemplateID.ToString().Equals(str_FAQs_template_ID))
                                      select a).Take(100).ToList();

                foreach (Item i in allFAQs)
                {
                    faqNo++;
                    tempRowforCFFAQs = tempTblCFforFAQs.NewRow();
                    tempRowforCFFAQs["FAQ_ID"] = i.ID.ToString();
                    tempRowforCFFAQs["FAQ_No"] = faqNo.ToString();
                    tempRowforCFFAQs["FAQ_ID_No"] = "faq" + faqNo.ToString() + "ID2";
                    tempRowforCFFAQs["FAQ_Question"] = i["Question"];
                    tempRowforCFFAQs["FAQ_Answer"] = i["Answer"];
                    tempRowforCFFAQs["FAQ_OnClick"] = "HideContent('faq" + faqNo.ToString() + "ID2', 'faq" + faqNo.ToString() + "ConID2'); return false";
                    tempRowforCFFAQs["FAQ_Con_ID"] = "faq" + faqNo.ToString() + "ConID2";
                    tempTblCFforFAQs.Rows.Add(tempRowforCFFAQs);
                    
                    repCFFAQs.DataSource = tempTblCFforFAQs;
                    repCFFAQs.DataBind();
                }

            }
            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
        }

        protected void repFAQs_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater r = e.Item.FindControl("repFAQs") as Repeater;
            if (r != null && r.Items.Count > 0)
            {
                r.Visible = true;
            }
        }

        protected void repCFFAQs_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater r = e.Item.FindControl("repCFFAQs") as Repeater;
            if (r != null && r.Items.Count > 0)
            {
                r.Visible = true;
            }
        }
    }
  
}