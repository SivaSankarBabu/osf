﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Publishing;
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using System.Web.UI;
using CAPTCHA;

namespace Layouts.Pgstorymobilesublayout
{

    /// <summary>
    /// Summary description for PgstorymobilesublayoutSublayout
    /// </summary>
    public partial class PgstorymobilesublayoutSublayout : System.Web.UI.UserControl
    {
        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");

        string StoryName = string.Empty;
        string StoryArea = string.Empty;
        string StroryPioneersArea = string.Empty;
        string StoryPioneerName = string.Empty;
        string StoryEmail = string.Empty;
        Item itmContext = Sitecore.Context.Item;
        CaptchaGenerator cgen;
        private void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                cgen = new CaptchaGenerator();
                GetCaptchaimage();
                Sitecore.Data.Fields.CheckboxField CheckboxTerms = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["TC Checkbox"]);
                if (CheckboxTerms.Checked == true)
                    DivTerms.Visible = true;

                Sitecore.Data.Fields.CheckboxField CheckboxAgreement = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Agreement Checkbox"]);
                if (CheckboxAgreement.Checked == true)
                    DivAgreement.Visible = true;
                //SetInitialRow1();
            }

        }

        private void GetCaptchaimage()
        {


            string strpath = Server.MapPath("/SG50/images/PG/") + "CaptchaImage.gif";
            cgen.ResetCaptchaColor();

            txt_CaptchaVal.Text = cgen.GenerateCaptcha(strpath);


            string strpath2 = "/SG50/layouts/PG/sublayouts/" + "CaptchaImage.ashx?id=" + strpath;
            Image1.ImageUrl = strpath2;



        }
        protected void Button1_Click1(object sender, ImageClickEventArgs e)
        {
            cgen = new CaptchaGenerator();
            GetCaptchaimage();
        }

        public void imgToML(FileUpload imgfile, string ImageItemNameStr, string ItemName, Item newForm)
        {
            var item = newForm;
            string[] allowedExtension = new string[] { ".jpg", ".png", ".gif", ".Jpeg" };
            string filename = string.Empty;
            Item ImagenewForm = null;
            if (imgfile.HasFile)
            {
                if (imgfile.PostedFile.ContentLength <= 1048576)
                {

                    string ext = System.IO.Path.GetExtension(imgfile.FileName);
                    if (allowedExtension.Contains(ext))
                    {

                        // Response.Write(box1.PostedFile.FileName);
                        Sitecore.Resources.Media.MediaCreatorOptions optionsM = new Sitecore.Resources.Media.MediaCreatorOptions();
                        optionsM.Database = master;
                        optionsM.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                        optionsM.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                        filename = imgfile.FileName.Replace(".jpg", "").Replace(".png", "").Replace(".gif", "").Replace(".Jpeg", "");
                        filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                        //  filename = filename + "Original";
                        // string filename = box1.FileName.Replace(".doc", "").Replace(".docx", "").Replace(".xls", "").Replace(".xlsx", "").Replace(".ppt", "").Replace(".pptx", "").Replace(".pdf", "");
                        // filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");

                        string Destination = "/sitecore/media library/SG50/PG/Story Images/" + ItemName + ImageItemNameStr + filename;
                        optionsM.Destination = Destination;
                        string strimgpath = Server.MapPath("/SG50/layouts/PG/Story Images/" + ItemName + ImageItemNameStr + imgfile.FileName);
                        imgfile.SaveAs(strimgpath);
                        optionsM.FileBased = false;
                        Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                        Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(strimgpath, optionsM);


                        string path = newForm.Paths.Path.ToString();// Sitecore.Context.Item.Paths.Path.ToString();
                        Item ImageItem1 = master.GetItem(path + "/Images");



                        Sitecore.Data.Items.TemplateItem Imagetemplate = master.GetItem("/sitecore/templates/SG50/Pioneer Generation/PGStrory Image Banner");
                        Item ImageItem = master.GetItem(ImageItem1.ID.ToString());
                        string ImageItemName = ImageItemNameStr;
                        ImagenewForm = ImageItem.Add(ImageItemName.ToString(), Imagetemplate);
                        ImagenewForm.Editing.BeginEdit();
                        //  ImagenewForm.Fields["Image"].Value = "/SG50/PG/Story Images/Chrysanthemumjpg";//   "/SG50/PG/Story Images/" + filename; //Chrysanthemumjpg";// "/SG50/PG/Story Images/" + filename;


                        Sitecore.Data.Items.MediaItem image = item.Database.GetItem(Destination);
                        if (image != null)
                        {
                            Sitecore.Data.Fields.ImageField imagefield = ImagenewForm.Fields["Image"];
                            imagefield.Alt = image.Alt;
                            imagefield.MediaID = image.ID;
                            imagefield.Src = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image);

                        }
                        ImagenewForm.Editing.EndEdit();
                    }
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (txt_CaptchaVal.Text == SG50Class.StripUnwantedString(txt_Captcha.Text))
                {
                    StoryName = SG50Class.StripUnwantedString(txtName.Text.TrimStart().TrimEnd());
                    StoryArea = SG50Class.StripUnwantedString(txtStoryArea.Text.TrimStart().TrimEnd());
                    StoryArea = StoryArea.Replace("\n", "<br />");
                    StroryPioneersArea = SG50Class.StripUnwantedString(txtPioneersArea.Text.TrimStart().TrimEnd());
                    StoryPioneerName = SG50Class.StripUnwantedString(txtPioneerName.Text.TrimStart().TrimEnd());
                    StoryEmail = SG50Class.StripUnwantedString(txtEmail.Text.TrimStart().TrimEnd());

                    if (StoryName != "" && StoryArea != "" && StroryPioneersArea != "" && StoryPioneerName != "" && StoryEmail != "")
                    {
                        if (fileOriginalImage.HasFile)
                        {

                            if (fileOriginalImage.PostedFile.ContentLength <= 1048576 && filename1.PostedFile.ContentLength <= 1048576 && filename2.PostedFile.ContentLength <= 1048576 && filename3.PostedFile.ContentLength <= 1048576)
                            {
                                System.IO.Stream stream = fileOriginalImage.PostedFile.InputStream;
                                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                                //if (image.Width >= 1280 && image.Height >= 740)
                               // {

                                    string filebaseOriginalHash = string.Empty;


                                    try
                                    {


                                        using (new Sitecore.SecurityModel.SecurityDisabler())
                                        {

                                            Item newForm = null;
                                            Item ImagenewForm = null;
                                            string filename = string.Empty;
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Pioneer Generation/PGStory");
                                            Item parentItem = master.GetItem("{655A7E18-1AE6-447E-8A1F-C9A4768126B7}");
                                            string ItemName = StoryPioneerName + "-" + DateTime.Now.ToString("yyMMdd") + "-" + DateTime.Now.ToString("hhmmss"); // Guid.NewGuid().ToString();
                                            newForm = parentItem.Add(ItemName.ToString(), template);

                                            newForm.Editing.BeginEdit();
                                            newForm.Fields["Story Description"].Value = Convert.ToString(StoryArea);
                                            newForm.Fields["Pioneers Description"].Value = Convert.ToString(StroryPioneersArea);
                                            newForm.Fields["Categories"].Value = "";// Convert.ToString(ddlCategories.SelectedItem.Text);
                                            newForm.Fields["Pioneers Name"].Value = Convert.ToString(StoryPioneerName);
                                            newForm.Fields["Age"].Value = Convert.ToString(ddlAge.SelectedItem.Text);
                                            newForm.Fields["Name"].Value = Convert.ToString(StoryName);
                                            newForm.Fields["Email Address"].Value = Convert.ToString(StoryEmail);
                                            newForm.Editing.EndEdit();

                                            Item Folder = null;
                                            Folder = master.GetItem("/sitecore/content/SG50/Pioneer Generation/Images");
                                            Folder.CopyTo(newForm, "Images");

                                            string[] allowedExtension = new string[] { ".jpg", ".png", ".gif", ".Jpeg" };
                                            bool Contains = true;
                                            // FileUpload box1 = (FileUpload)GridView2.Items[0].Controls[1].FindControl("FileUpload1");
                                            //  Label lblFUMsg = (Label)GridView2.Items[0].Controls[2].FindControl("lblFUMsg");
                                            bool result = true;
                                            var item = newForm;
                                            using (new Sitecore.SecurityModel.SecurityDisabler())
                                            {
                                                if (item != null)
                                                {

                                                    var source = master;
                                                    var target = Sitecore.Data.Database.GetDatabase("web");

                                                    var options = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now)
                                                    {
                                                        RootItem = item,
                                                        Deep = true,
                                                    };

                                                    var publisher = new Publisher(options);
                                                    publisher.PublishAsync();

                                                }
                                            }



                                            imgToML(fileOriginalImage, "Original", ItemName, newForm);

                                            imgToML(fileOriginalImage, "Thumbnail", ItemName, newForm);

                                            imgToML(filename1, "Image2", ItemName, newForm);

                                            imgToML(filename2, "Image3", ItemName, newForm);

                                            imgToML(filename3, "Image4", ItemName, newForm);


                                            var item1 = newForm;
                                            using (new Sitecore.SecurityModel.SecurityDisabler())
                                            {
                                                if (item1 != null)
                                                {

                                                    var source = master;
                                                    var target = Sitecore.Data.Database.GetDatabase("web");

                                                    var options = new PublishOptions(source, target, PublishMode.SingleItem, item1.Language, DateTime.Now)
                                                    {
                                                        RootItem = item1,
                                                        Deep = true,
                                                    };

                                                    var publisher = new Publisher(options);
                                                    publisher.PublishAsync();

                                                }
                                            }


                                            var itemMedia = master.GetItem("{2EC9C2D6-89FB-4C81-B9D7-3B4A7733701C}"); ;
                                            using (new Sitecore.SecurityModel.SecurityDisabler())
                                            {
                                                if (itemMedia != null)
                                                {

                                                    var source = master;
                                                    var target = Sitecore.Data.Database.GetDatabase("web");

                                                    var options = new PublishOptions(source, target, PublishMode.SingleItem, itemMedia.Language, DateTime.Now)
                                                    {
                                                        RootItem = itemMedia,
                                                        Deep = true,
                                                    };

                                                    var publisher = new Publisher(options);
                                                    publisher.PublishAsync();

                                                }
                                            }



                                        }
                                        string url = "~/SG50/Pioneer Generation/Thank%20You.aspx";
                                        if (!(url.Contains("://")))
                                            Response.Redirect(url);

                                    }

                                    catch (Exception ee)
                                    {
                                        throw;

                                    }
                                //}
                               // else
                                //{

                               //     ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Image dimensions must be above 1280x740 pixels.');", true);
                               // }

                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Image size exceeded 1mb. Please upload with smaller size.');", true);
                            }

                        }

                        else
                        {

                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Select image.');", true);
                        }

                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill all the required fields.');", true);
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Captcha Code');", true);
                }

            }
            else
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Data!');", true);
            }
        }

    }
}