﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using System.Net.Mail;
using System.Web.Services;
using System.Net;
using Sitecore.Publishing;
namespace Layouts.Pghomesublayout_bottom
{

    /// <summary>
    /// Summary description for Pghomesublayout_bottomSublayout
    /// </summary>
    public partial class Pghomesublayout_bottomSublayout : System.Web.UI.UserControl
    {
        static Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
            Session["Share"] = "N";
            if (!IsPostBack)
            {

                Story_List("ALL", "");
            }

        }
        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                SendAcknowledgementEmail(SG50Class.StripUnwantedString(txtEnterEmail.Text.Trim()), "SG50 :: Pioneer Generation - Subscriber", AcknowledgementContent());
            }
            catch (Exception)
            {
                throw;
            }
        }
        private string AcknowledgementContent()
        {
            StringBuilder message = new StringBuilder();
            message.Append("Dear Admin,<br><br>");
            message.Append("You have new subscriber : " + txtEnterEmail.Text.Trim());
            message.Append("<br><br>");
            message.Append("SG50 PG Programme.");
            return message.ToString();
        }
        protected void SendAcknowledgementEmail(string toAddress, string subject, string body)
        {
            try
            {
                if (toAddress != "")
                {
                    string senderID = string.Empty;
                    string Host = string.Empty;
                    string smtpEmail = string.Empty;
                    string smtpPassword = string.Empty;
                    string CCList = string.Empty;
                    int Port;

                    Item itemconfiguration = SG50Class.web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");

                    senderID = itemconfiguration["From Address"];
                    Host = itemconfiguration["SMTP Host Name"];
                    smtpEmail = itemconfiguration["SMTP User Name"];
                    smtpPassword = itemconfiguration["Password"];
                    CCList = itemconfiguration["CC Mails"];
                    Port = System.Convert.ToInt32(itemconfiguration["Port No"]);

                    MailMessage enquiry_mail = new MailMessage();


                    MailAddress mailto = default(MailAddress);
                    mailto = new MailAddress(senderID);
                    enquiry_mail.To.Add(mailto);

                    MailAddress mailfrom = default(MailAddress);
                    mailfrom = new MailAddress(senderID);

                    string[] words = CCList.Split(';');
                    foreach (string cc in words)
                    {
                        if (cc != "")
                        {
                            MailAddress mailcc = default(MailAddress);

                            mailcc = new MailAddress(cc);
                            enquiry_mail.CC.Add(mailcc);
                        }
                    }

                    enquiry_mail.From = mailfrom;
                    enquiry_mail.BodyEncoding = Encoding.UTF8;
                    enquiry_mail.IsBodyHtml = true;
                    enquiry_mail.Subject = subject;
                    enquiry_mail.Body = body;

                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = Host;
                    smtp.Port = 25;
                    if (!string.IsNullOrEmpty(smtpEmail) && !string.IsNullOrEmpty(smtpPassword))
                    {
                        smtp.Credentials = new System.Net.NetworkCredential(smtpEmail, smtpPassword);
                        smtp.UseDefaultCredentials = false;
                    }
                    else
                    {
                        smtp.EnableSsl = false;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    }

                    smtp.Send(enquiry_mail);
                    txtEnterEmail.Text = "";
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Thank You!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid email address');", true);
                }
            }
            catch (Exception ee)
            {
                //throw;
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid email address!');", true);
            }
        }
        protected void lbAll_Click(object sender, EventArgs e)
        {

            Story_List("ALL", "");

        }
        protected void lbSortRecent_Click(object sender, EventArgs e)
        {

            Story_List("Recent", "");

        }
        protected void lbPopular_Click(object sender, EventArgs e)
        {

            Story_List("Popular", "");

        }
        public void Story_List(string type, string CategoryValue)
        {

            DataTable dtrepEvents = new DataTable();
            dtrepEvents.Columns.Add("ImageUrl", typeof(string));
            dtrepEvents.Columns.Add("ImageText", typeof(string));
            dtrepEvents.Columns.Add("item", typeof(Item));
            dtrepEvents.Columns.Add("href", typeof(string));
            dtrepEvents.Columns.Add("Id", typeof(string));
            dtrepEvents.Columns.Add("PName", typeof(string));

            lbAll.CssClass = "";
            lbSortRecent.CssClass = "";
            lbPopular.CssClass = "";
            Item itmPress = SG50Class.web.GetItem(SG50Class.str_PGStory_Item_ID);
            IEnumerable<Item> itmIEnumPressReleases = null;
            IEnumerable<Item> itmIEnumPressReleases2 = null;
            IEnumerable<Item> itmprea = null;
            if (type == "ALL")
            {
                lbAll.CssClass = "active";
                itmIEnumPressReleases2 = from a in itmPress.Axes.GetDescendants()
                                         where
                                         a.TemplateID.ToString().Equals(SG50Class.str_PGStory_Template_ID)
                                         select a;

                itmprea = itmIEnumPressReleases2.Randomize();


                Session["RandomScroll"] = itmprea;
                itmIEnumPressReleases = itmprea.Take(9).ToArray();
                string id = "";
                foreach (Item item in itmIEnumPressReleases)
                {
                    id += item.ID.ToString() + ",";
                }
                Session["ScrollIds"] = id;
            }
            else if (type == "Recent")
            {
                lbSortRecent.CssClass = "active";
                itmIEnumPressReleases2 = from a in itmPress.Axes.GetDescendants().OrderByDescending(x => x.Statistics.Created)  //OrderBy(a => a.Statistics.Created).Reverse().Take(9) 
                                         where
                                         a.TemplateID.ToString().Equals(SG50Class.str_PGStory_Template_ID)


                                         select a;
                itmIEnumPressReleases = itmIEnumPressReleases2.Take(9);

            }
            else if (type == "Popular")
            {
                lbPopular.CssClass = "active";
                itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants().Take(9)
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_PGStory_Template_ID)
                                        orderby
                                        a.Fields["PopularCount"].ToString() descending
                                        select a;
            }
            else if (type == "Categories")
            {
                itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants().Take(9)
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_PGStory_Template_ID)
                                        && a.Fields["Categories"].ToString() == CategoryValue
                                        select a;
            }

            foreach (Item a in itmIEnumPressReleases)
            {
                DataRow drrepEvents;
                drrepEvents = dtrepEvents.NewRow();
                drrepEvents["PName"] = a.Fields["Pioneers Name"];
                drrepEvents["ImageText"] = a.Fields["Pioneers Description"];
                drrepEvents["item"] = a;
                drrepEvents["href"] = "/SG50/Pioneer Generation" + LinkManager.GetItemUrl(a).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "");
                drrepEvents["Id"] = a.ID;


                Sitecore.Collections.ChildList ccc = web.GetItem(a.ID).GetChildren();

                string curntItem = a.ID.ToString();

                string path = a.Paths.Path.ToString();
                Item ImageItemHumbnail = web.GetItem(path + "/Images/Thumbnail");

                if (ImageItemHumbnail != null)
                {

                    Sitecore.Data.Fields.ImageField image = ImageItemHumbnail.Fields["Image"];

                    if (image.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        string url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                        drrepEvents["ImageUrl"] = url;// stringArray[ImagesCount.Count - 1];
                    }
                    else
                    {
                        drrepEvents["ImageUrl"] = "/SG50/images/PG/logo.png";
                    }


                }
                else
                {
                    drrepEvents["ImageUrl"] = "/SG50/images/PG/logo.png";
                }

                dtrepEvents.Rows.Add(drrepEvents);

            }

            repStoryList.DataSource = dtrepEvents;
            repStoryList.DataBind();
        }
        public void Story_Categories_List1(object sender, EventArgs e)
        {


            Story_List("Categories", KampongChief.Title);
        }
        public void Story_Categories_List2(object sender, EventArgs e)
        {

            Story_List("Categories", TheRocksteady.Title);
        }
        public void Story_Categories_List3(object sender, EventArgs e)
        {

            Story_List("Categories", Trailblazer.Title);
        }
        public void Story_Categories_List4(object sender, EventArgs e)
        {

            Story_List("Categories", TheGiver.Title);
        }
        public void Story_Categories_List5(object sender, EventArgs e)
        {

            Story_List("Categories", TheDieHard.Title);
        }
        protected void repStoryList_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "More")
            {
                string result = e.CommandArgument.ToString();
                string[] words = result.Split(',');

                Item newForm = null;


                using (new Sitecore.SecurityModel.SecurityDisabler())
                {

                    Item FormData = master.GetItem(words[1]);

                    int likes = 0;
                    if (!string.IsNullOrEmpty(FormData["PopularCount"]))
                        likes = System.Convert.ToInt32(FormData["PopularCount"]);
                    likes = likes + 1;
                    newForm = master.GetItem(words[1]);
                    newForm.Editing.BeginEdit();
                    newForm.Fields["PopularCount"].Value = System.Convert.ToString(likes);
                    newForm.Editing.EndEdit();


                    Item Mediaitem = master.GetItem(newForm.ID.ToString());
                    var source1 = master;
                    var target = Sitecore.Data.Database.GetDatabase("web");

                    var options = new PublishOptions(source1, target, PublishMode.SingleItem, Mediaitem.Language, DateTime.Now)
                    {
                        RootItem = Mediaitem,
                        Deep = true,
                    };

                    var publisher = new Publisher(options);
                    publisher.PublishAsync();

                }
                Session["Share"] = "N";
                string url = words[0];
                if (!(url.Contains("://")))
                    Response.Redirect(url);

            }
        }

    }
    public static class IEnumerableExtensions
    {

        public static IEnumerable<t> Randomize<t>(this IEnumerable<t> target)
        {
            Random r = new Random();

            return target.OrderBy(x => (r.Next()));
        }
    }
}
