﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Pgstorydetails_sublayout.Pgstorydetails_sublayoutSublayout"
    CodeFile="~/SG50/layouts/PG/sublayouts/PGStoryDetails Sublayout.ascx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">

<script type="text/javascript">
    $(function () {

        var domainName = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Host Name"].Value.ToString() %>";
        var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
        var thumbimg = "<%=MainImgThumb.Src%>"

        window.fbAsyncInit = function () {
            FB.init({
                appId: FBID, status: true, cookie: true, xfbml: true,
                version: 'v2.2'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        $('#shareOnFacebook').click(function () {
            //var imagePath = domainName + $('#content_0_DetailsImgBanner img').attr('src'), shareDescription = $('#divDescription').text();
            //var imagePath = domainName + $('#mainimg').attr('src');
            var shareDescription = $('#divDescription').text();
            var imagePath = domainName + thumbimg;
            postToFeed(imagePath, shareDescription);
        });


        function postToFeed(imagePath, shareDescription) {
            var regxp = new RegExp("\\ ", "g");

            var image1 = imagePath.replace(regxp, "%20");
            image1 = imagePath.replace("?bc=White", "");

            var SessionID = '<%= Session["StoryRedirecturl"] %>';
                var storyobj = {

                    method: 'feed',
                    link: SessionID,
                    picture: image1

                };
                FB.ui(storyobj);
            }

    });


        function newPopup() {
            var shareDescription = $('#divDescription').text()
            var SessionID = '<%= Session["StoryRedirecturl"] %>';
            var urls = encodeURIComponent(SessionID);
            var tempurl = 'https://twitter.com/intent/tweet?text=' + urls
            popupWindow = window.open(
               tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')
        }

</script>

<div class="slider-container details-slider">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <div id="DetailsImgBanner" runat="server" class="carousel-inner detailsbanner">
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
                href="#carousel-example-generic" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
</div>
<section class="pioneer-content">
    <div class="custom-container" id="divDescription">
        <p>
            <sc:Text runat="server" ID="txtDescription" Field="Story Description"></sc:Text>
        </p>
        <p>
            <i>Story & images submitted by
            <sc:Text runat="server" ID="Text1" Field="Name"></sc:Text>
            </i>
        </p>
    </div>
</section>
<section class="pics-section">
    <ul class="pics-gallery" runat="server" id="UlInner">
    </ul>
</section>
<section class="share-section backtohome">
    <ul class="share-list">
        <li>
            <div class="share-block">
                <span>SHARE THIS STORY </span>
                <br />
                <br />

                <div class="share-icons">
                    <div class="facebook">
                        <a id="shareOnFacebook">
                            <img src="/SG50/images/PG/fc-lg.png" /></a>

                    </div>
                    <div class="twitter">
                        <a onclick="JavaScript:newPopup();">
                            <img src="/SG50/images/PG/tw-lg.png" /></a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </li>
        <li class="border-left">
            <div class="share-block">
                <span>SHARE THIS QUOTE</span>
                <br />
                <br />
                <p></p>
                <%--<a id="A1" runat="server"  class="event-button">Share a Quote</a>--%><a id="ShareQuote" runat="server" onserverclick="Share_Quote" class="event-button">Share a Quote</a>
                <div class="clearfix"></div>
            </div>
        </li>
    </ul>
    <a href="/SG50/Pioneer Generation" class="next-story backtohome-pos">
        <img src="/SG50/images/PG/arrow-next.png" />
        <span>Back<br />
            to Home </span></a>
    <a class="next-story" id="storylink" runat="server" onserverclick="NextStory">
        <img src="/SG50/images/PG/arrow-next.png" />
        <span>NEXT<br />
            STORY 
        </span></a>
    <img id="MainImgThumb" style="display: none" runat="server" />
</section>

<script src="/SG50/include/PG/js/jquery.validate.min.js" type="text/javascript"></script>

