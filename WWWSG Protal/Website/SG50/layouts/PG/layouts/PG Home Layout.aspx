<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="-1" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,800,700italic,800italic'
        rel='stylesheet' type='text/css' />
    <link href="/SG50/include/PG/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/SG50/include/PG/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/SG50/include/PG/css/responsive.css" rel="stylesheet" type="text/css" />

    <script src="/SG50/include/js/1.8.3/jquery-1.8.3.min.js" type="text/javascript" language="javascript"></script>
    <script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-46719765-2', 'singapore50.sg');
        ga('send', 'pageview');
    </script>

    <sc:sublayout runat="server" renderingid="{DCC472CB-879C-423F-9487-4CA1A971EA17}"
        path="/SG50/layouts/PG/sublayouts/PG Meta Tag Sublayout.ascx" id="sublayoutPGMetaTag"
        placeholder="contentHeader"></sc:sublayout>
</head>
<body class="sgContainer">
    <form method="post" runat="server" name="mainform" enctype="multipart/form-data"
    id="mainform">
    <sc:placeholder id="plhHeader" runat="server" key="Header"></sc:placeholder>
    <sc:placeholder id="plhContent" runat="server" key="Content"></sc:placeholder>
    <sc:placeholder id="PlhBottom" runat="server" key="ContentBottom"></sc:placeholder>
    <sc:placeholder id="PlhDetails" runat="server" key="ContentDetails" visible="false"></sc:placeholder>
    <sc:placeholder id="PlhShare" runat="server" key="ContentShare" visible="false"></sc:placeholder>
    <sc:placeholder id="plhFooter" runat="server" key="Footer"></sc:placeholder>
    </form>
</body>
</html>
