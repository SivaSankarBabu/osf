﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Galleryphotolist.GalleryphotolistSublayout" CodeFile="~/SG50/layouts/GalleryPhotoList.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript" language="javascript"></script>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<script src="/SG50/include/js/b6NoKG6GEMQqshcL22y.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/jquery.bxslider.min.js"></script>
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<div class="masthead">

    <!-- Photos Header -->
    <div class="photo-header">
        <div class="photo-row">
            <div class="cell-12">
                <div class="group-text-button">
                    <a href="/SG50/GalleryLanding.aspx" class="title-button"><sc:Text ID="txtButtonText" runat="server" Field="Back Button Text" /></a>
                    <h1><sc:Text ID="txtTitle" runat="server" Field="Title" /></h1>
                </div>
                <p><sc:Text ID="txtDescription" runat="server" Field="Description" /></p>
            </div>
        </div>
    </div>
    <!-- Photos Header -->
    <div class="photo-gallery">
        <div class="photo-row">
            <div class="SG50Loader">
                <img src="/SG50/images/Gallery/SG50Loader.gif" /></div>
            <ul id="FaceBookList"></ul>
            <!--<div class="cell-12">
                <div class="pagination">
                    <ul>
                        <li><a href="#" class="pre"></a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li><a href="#">8</a></li>
                        <li><a href="#">9</a></li>
                        <li><a href="#" class="next"></a></li>
                    </ul>
                </div>
            </div>-->
        </div>
    </div>


</div>
<%--<script type="text/javascript">var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>
<script>
    $(function () {
        $.getJSON('https://graph.facebook.com/sg2015/albums', function (data) {
            fcontent = '';
            var result = $.map(data, function (value, key) {
                if (key != 'paging') {
                    for (var i in value) {
                        if (value[i]['name'] !== "Timeline Photos" &&
						 value[i]['name'] !== "Cover Photos" &&
						 value[i]['name'] !== "Profile Pictures"
					    ) {
                            description = '';
                            var updated_date = new Date(value[i]['updated_time'].substr(0, 19)).toDateString();

                            if (typeof value[i]['description'] != 'undefined') {
                                if (value[i]['description'].length >= 180) {
                                    description = value[i]['description'].substr(0, 170) + "...";
                                } else {
                                    description = value[i]['description'];
                                }
                            } else {
                                description = '';
                            }
                            if (typeof value[i]['count'] != 'undefined') {
                                no_of_photos = value[i]['count'];
                            } else {
                                no_of_photos = 0;
                            }
                            fcontent += '<li id="' + value[i]['id'] + '">' +
                                 '<div class="photo-cont">' +
                                    '<div class="ImageContainer"><img src="https://graph.facebook.com/' + value[i]['cover_photo'] + '/picture' + '"/></div>' +
                                    '<div class="no-photos right">' + no_of_photos + '</div>' +
                                    '<h1>' + value[i]['name'] + '</h1>' +
                                    '<span>' + updated_date + '</span>' +
                                    '<p>' + description + '</p>' +
                                '</div>' +
                            '</li>';
                        }
                    }
                }
            });
            $("#FaceBookList").html(fcontent);
            $(".SG50Loader").hide();
        });

        $(document).on("click", "li", function (event) {
            var album_id = jQuery(this).attr('id');
            var t = $('#' + album_id + ' h1').html();
            var d = $('#' + album_id + ' p').html();
            localStorage.setItem('facebook_album', album_id);
            localStorage.setItem('title', t);
            localStorage.setItem('description', d);
            window.location = "/SG50/GalleryLanding/GalleryPhotoList/GalleryPhotos.aspx";
        });
    });
</script>--%>
<script src="/SG50/include/js/p5sxG6fMQqshcLs3.js" type="text/javascript"></script>

