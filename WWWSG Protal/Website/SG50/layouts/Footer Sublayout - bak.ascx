﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="Layouts.Footer_sublayout.Footer_sublayoutSublayout" CodeFile="~/SG50/layouts/Footer Sublayout.ascx.cs"%>
<html>
<head>
    <script language="javascript" type="text/javascript" src="/SG50/modalpopup.js"></script>

</head>
<body>
    <!--  FAQ Sub Page Start -->
    <div id="dvPopup">
        <a href="#" onclick="HideModalPopup('dvPopup'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle">
            <p class="FontTitle1">10 THINGS YOU'D</p>
            <p class="FontTitle2">PROBABLY ASK,</p>
            <p class="FontTitle3">ANSWERED.</p>
        </div>
        <div id="Qmark">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <div id="faq1ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq1ID', 'faq1ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        1. What is the closing date for SG50 Celebration Fund applications ?
                    </p>
                </div>
                <div id="faq1ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq2ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq2ID', 'faq2ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        2. Who can apply for the SG50 Celebration Fund ?
                    </p>
                </div>
                <div id="faq2ConID" class="faqContents" style="display: none">
                    <p>A minimum of two individuals (with at least one applicant above the age of 18) who both reside in Singapore is required for those applying as a group. Non-profit organisations, societies registered with the Registry of Societies (ROS) and companies registered with the Accounting and Corporate Regulatory (ACRA) can also apply. Government Ministries, Departments, Organs of State and statutory boards are not eligible. </p>
                </div>
                <div id="faq3ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq3ID', 'faq3ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        3. What expenses are covered under the SG50 Celebration Fund?
                    </p>
                </div>
                <div id="faq3ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq4ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq4ID', 'faq4ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        4. Can my group submit applications for more than one project?
                    </p>
                </div>
                <div id="faq4ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq5ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq5ID', 'faq5ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        5. Can my project receive the SG50 Celebration Fund in addition to grant(s) from other government agency(s)?
                    </p>
                </div>
                <div id="faq5ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq6ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq6ID', 'faq6ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        6. Must all the projects be carried out in Singpore?
                    </p>
                </div>
                <div id="faq6ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq7ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq7ID', 'faq7ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        7. How long can my project run for?
                    </p>
                </div>
                <div id="faq7ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq8ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq8ID', 'faq8ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        8. Are we allowed to make changes to the project in the course of implementation?
                    </p>
                </div>
                <div id="faq8ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq9ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq9ID', 'faq9ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        9. How can i show that my project is part of the Singapore50 celebrations?
                    </p>
                </div>
                <div id="faq9ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <div id="faq10ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq10ID', 'faq10ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        10. When will we know if our application has been successful?
                    </p>
                </div>
                <div id="faq10ConID" class="faqContents" style="display: none">
                    <p>Answer will provide SOON.</p>
                </div>
                <br />
                <br />
                <p class="moreQues">Have more questions you'd like us to answer? <a href="#" onclick="return false;" class="getInTouch">Get in touch with us.</a></p>
            </div>
        </div>
    </div>
    <!-- FAQ Sub Page End -->
	
	 <!--  FAQ Bottom Sub Page Start -->
    <div id="dvPopup2">
        <a href="#" onclick="HideModalPopup('dvPopup2'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle2">
            <p class="FontTitle1">What you may want</p>
            <p class="FontTitle2">to know about the</p>
            <p class="FontTitle3">SG50 Celebration Fund.</p>
        </div>
        <div id="Qmark2">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <div id="faq1ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq1ID2', 'faq1ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        1.    Is there a closing date for SG50 Celebration Fund applications?
                    </p>
                </div>
                <div id="faq1ConID2" class="faqContents" style="display: none">
                    <p>        Yes. Application closes on 31 August 2015.</p>
                </div>
                <div id="faq2ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq2ID2', 'faq2ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       2.    Who can apply for the SG50 Celebration Fund?
                    </p>
                </div>
                <div id="faq2ConID2" class="faqContents" style="display: none">
                    <p>You can either apply as (i) Groups or (ii) Organisations. Under the Groups category, two individuals (at least one of whom is a Singaporean, aged 18 years and above) are required for the application. Societies registered with the Registry of Societies (ROS) and companies registered with the Accounting and Corporate Regulatory Authority (ACRA) can apply under the Organisations category. Government Ministries, Departments, Organs of State and statutory boards will however, not be eligible to apply for the fund. </p>
                </div>
                <div id="faq3ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq3ID2', 'faq3ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        3.    What expenses are covered under the SG50 Celebration Fund?
                    </p>
                </div>
                <div id="faq3ConID2" class="faqContents" style="display: none">
                    <p>Only expenses directly incurred in the implementation of the project will be funded. These do not include group/organisation’s start-up costs, capital expenditure, cash prizes, and fundraising expenses. A project report and an income and expenditure statement will need to be submitted within three months after your project’s completion.</p>
                </div>
                <div id="faq4ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq4ID2', 'faq4ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        
                    4.    Can I submit applications for more than one project?

                    </p>
                </div>
                <div id="faq4ConID2" class="faqContents" style="display: none">
                    <p>Yes. There is no limit to the number of proposals that each individual can submit. </p>
                </div>
                <div id="faq5ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq5ID2', 'faq5ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       5.  Can my project receive the SG50 Celebration Fund in addition to grant(s) from other government agencies?
                    </p>
                </div>
                <div id="faq5ConID2" class="faqContents" style="display: none">
                    <p>Project components that are concurrently funded by other government source(s) will not qualify for the SG50 Celebration Fund. </p>
                </div>
                <div id="faq6ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq6ID2', 'faq6ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        6.    How long can my project run for?
                    </p>
                </div>
                <div id="faq6ConID2" class="faqContents" style="display: none">
                    <p>Projects should be completed within 18 months from the date of the funding agreement, or by 31 December 2015, whichever is earlier. </p>
                </div>
                <div id="faq7ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq7ID2', 'faq7ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        7.     Are we allowed to make changes to the project in the course of implementation?
                    </p>
                </div>
                <div id="faq7ConID2" class="faqContents" style="display: none">
                    <p>You need to seek the agreement of the SG50 Celebration Fund team if there are changes, postponement and/or cancellation of your project.</p>
                </div>
                <div id="faq8ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq8ID2', 'faq8ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       8. 	How can I show that my project is part of the SG50 celebrations?
                    </p>
                </div>
                <div id="faq8ConID2" class="faqContents" style="display: none">
                    <p>All funded projects will bear the SG50 branding/logo in all your publicity materials. The SG50 Celebration Fund team will advise you on the use of the logo.</p>
                </div>
                <div id="faq9ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq9ID2', 'faq9ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        9. When will I know if my application is successful?
                    </p>
                </div>
                <div id="faq9ConID2" class="faqContents" style="display: none">
                    <p>You will be notified within two months from your application date.  </p>
                </div>
                <div id="faq10ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq10ID2', 'faq10ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        10. Can I submit my application form in hard copy?
                    </p>
                </div>
                <div id="faq10ConID2" class="faqContents" style="display: none">
                    <p>Yes, you can submit your application in hard copy. Just download the form, fill it in and mail it together with the relevant documents to the following address:

                    SG50 Celebration Fund Secretariat 
                    Ministry of Culture, Community and Youth (MCCY)  
                    140 Hill Street, Old Hill Street Police Station, #01-01A 
                    Singapore 179369 

                    Phone: 1800-8379798 (Operating Hours: 9am to 5:30pm, Mondays to Fridays)

                    Alternatively, you can email the form to SG50CelebrationFund@Singapore50.sg
                    </p>
                </div>
                <br />
                <br />
                <p class="moreQues">Have more questions you'd like us to answer? <a href="#" onclick="return false;" class="getInTouch">Get in touch with us.</a></p>
            </div>
        </div>
    </div>
    <!-- FAQ Bottom Sub Page End -->

    <!--  House Rules Sub Page Start -->
    <div id="dvHouseRulesPopup">
        <a href="#" onclick="HideModalPopup('dvHouseRulesPopup'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitleHR">
            <p class="FontTitleHR1">LET'S TRY TO KEEP</p>
            <p class="FontTitleHR2">THINGS LIGHT AND</p>
            <p class="FontTitleHR3">LIVELY, SHALL WE?</p>
        </div>
        <div class="houseRulesContents">
            <div>
                <sc:Text ID="sctContent" Field="Content" runat="server" />
            </div>
                 <div id="aboutContent">
					That's why we want to create a comfortable environment for you and the rest of Singapore to engage in discussion. The first rule of thumb would be to avoid creating misunderstanding or conflict, when posting or commenting within the SG50 site.
					<br/><br/>That means you should not use the site to advocate any politics, or demean others based on race and religion. The site also should not be used as a platform to launch personal attacks, threats, insults, advertisements or deragatory remarks. And it goes without saying that vulgarities arre not allowed.
					<br/><br/>Any posts or comments that are deemed insensitive, irrelevant or inappropriate will be removed without prior notice. Please note that discussions on the SG50 site reflect the views of the participants, and not the opinion of the SG50 Programme Office.
              		</div>       
        </div>
    </div>
    <!--  House Rules Sub Page End -->

	    <!--  Acknowledgement Start -->
    <div id="dvAckPopup">
        <a href="#" onclick="HideModalPopup('dvAckPopup'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitleHR">
            <p class="FontTitleHR1">A BIG THANK YOU</p>
        </div>
        <div class="ackContents">
			<div id="aboutContent">
				We may be small nation, but we’re brimming with big talent. And the SG50 website is a wonderful testament of what the little red dot and its people have to offer. 
				<br/><br/>Thank you to the following photographers who have captured the Singaporean spirit, through their vibrant snapshots featured on the website. 
				<br/><br/><br/>Andrew JK Tan – <a href="http://www.flickr.com/photos/andrewjktan/"> http://www.flickr.com/photos/andrewjktan/</a> 
				<br/>Byran Ong – <a href="http://www.flickr.com/photos/smrt173/"> http://www.flickr.com/photos/smrt173/ </a>  
				<br/>Lufudesu – <a href="http://www.flickr.com/people/8557677@N06/"> http://www.flickr.com/people/8557677@N06/ </a>
			</div>
        </div>
    </div>
    <!--  Acknowledgement Sub Page End -->
	
	
	
	
    <div class="footer_nav">
         <a href="<%=getContactUsLink() %>">
            <span>CONTACT US</span>
        </a>
        <span id="divider">|</span>
        <a href="#" onclick="ShowModalPopup('dvPopup'); return false; ">
            <span>FREQUENTLY ASKED QUESTIONS (FAQ)</span>
        </a>
        <span id="divider">|</span>
        <a href="#" onclick="ShowModalPopup('dvHouseRulesPopup'); return false; ">
            <span>HOUSE RULES</span>
        </a>
		
		</a>
        <span id="divider">|</span>
        <a href="#" onclick="ShowModalPopup('dvAckPopup'); return false; ">
            <span>ACKNOWLEDGEMENT</span>
        </a>
    </div>

    <p id="cpDesktop">Copyright &copy; MCCY. All rights reserved.</p>
    <p id="cpMobile">Copyright &copy; MCCY</p>
</body>
</html>
