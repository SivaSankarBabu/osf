﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Sitemap_sublayout.Sitemap_sublayoutSublayout" CodeFile="~/SG50/layouts/Sitemap Sublayout.ascx.cs" %>

<div class="sitemap">
    <%--<ul><a href="https://www.singapore50.sg/">Homepage</a></ul><br/>--%>
    <ul><a href="https://www.singapore50.sg/SG50/About">ABOUT</a></ul>
    <br />
    <ul style="margin-left: 40px;"><a href="https://www.singapore50.sg/SG50/About#DivLittleDot">BRAND GUIDELINES</a></ul>
    <br />
    <ul style="margin-left: 40px;"><a href="https://www.singapore50.sg/SG50/About#divinitiatives">INITIATIVES</a></ul>
    <br />
    <ul style="margin-left: 40px;"><a href="https://www.singapore50.sg/SG50/About#DivMeetthecommitte">COMMITTEE</a></ul>
    <br />
    <ul><a href="https://www.singapore50.sg/SG50/WhatsOn.aspx">WHAT'S ON</a></ul>
    <br />
    <ul><a href="https://www.singapore50.sg/SG50/pulse.aspx">PULSE</a></ul>
    <br />
    <ul><a href="https://www.singapore50.sg/SG50/Celebration%20Fund%20and%20Ideas/Ground-Up Projects.aspx">CELEBRATION FUND & IDEAS</a></ul>
    <br />
    <ul style="margin-left: 40px;"><a href="https://www.singapore50.sg/SG50/Celebration%20Fund.aspx">WHAT IS IT?</a></ul>
    <br />
   <%-- <ul style="margin-left: 40px;"><a href="https://www.singapore50.sg/SG50/Apply%20For%20Funding.aspx">APPLY FOR FUNDING</a></ul>
    <br />--%>
    <ul style="margin-left: 40px;"><a href="https://www.singapore50.sg/SG50/Celebration%20Fund%20and%20Ideas/Ground-Up Projects.aspx">GROUND-UP PROJECTS</a></ul>
    <br />
    <ul style="margin-left: 40px;"><a href="https://www.singapore50.sg/SG50/Idea%20All%20Page.aspx">CELEBRATION IDEAS</a></ul>
    <br />
    <ul><a href="https://www.singapore50.sg/SG50/GalleryLanding.aspx">GALLERY</a></ul>
    <br />
    <ul><a href="https://www.singapore50.sg/SG50/COLLECTIBLES.aspx">COLLECTIBLES</a></ul>
    <br />
   <%-- <ul><a href="https://www.singapore50.sg/SG50/Pioneer Generation">SINCE 1965</a></ul>
    <br />
    <ul><a href="http://www.iconsof.sg/">Initiatives</a></ul>
    <br />
    <ul style="margin-left: 40px;"><a href="http://www.iconsof.sg/">Icons of SG</a></ul>
    <br />--%>


    <ul><a href="https://www.mynewsdesk.com/sg/singapore50">PRESS</a></ul>
    <br />
</div>
