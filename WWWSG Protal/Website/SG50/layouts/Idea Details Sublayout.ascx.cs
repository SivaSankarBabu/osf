﻿using Sitecore.Links;
using System;
using System.Configuration;

namespace Layouts.Idea_details_sublayout {
  
	/// <summary>
	/// Summary description for Idea_details_sublayoutSublayout
	/// </summary>
  public partial class Idea_details_sublayoutSublayout : System.Web.UI.UserControl 
	{
    private void Page_Load(object sender, EventArgs e) {
      // Put user code to initialize the page here
    }
    protected string getCurrentLink()
        {
            string link = "";
            
                link = ConfigurationManager.AppSettings["SG50websiteURL"] + LinkManager.GetItemUrl(Sitecore.Context.Item);
            
            return link;
        }

    protected string getCelebrationIdeasLink()
    {
        string link = "";
        if (SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID) != null)
        {
            link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID));
        }
        return link;
    }

      
  }
}