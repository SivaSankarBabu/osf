﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Idea_submission_sublayout.Idea_submission_sublayoutSublayout" CodeFile="~/SG50/layouts/Idea Submission Sublayout.ascx.cs" %>

<sc:text ID="Text2" runat='server' field ='Additional Javascript'/>

<style>
    .ideaForm {
        background-color: #F01334;
        width: 100%;
        min-height: 768px;
        text-align: center;
        color: white;
        overflow: hidden;
        position: relative;
    }

    .ideaTitle {
        padding-top: 160px;
        font-size: 30px;
        font-weight: bold;
        font-family: helvetica;
    }

    .ideaBlurb {
        padding-left: 300px;
        padding-right: 300px;
        padding-top: 5px;
        width: 650px;
        margin-top: 10px;
        margin-bottom: 25px;
        font-size: 15px;
        line-height: 20px;
    }

    .ideaSuccessTitle {
        padding-top: 150px;
        font-size: 32px;
        font-weight: bold;
        font-family: helvetica;
        margin-bottom: 50px;
    }

    .ideaSuccessBlurb {
        padding-left: 300px;
        padding-right: 300px;
        padding-top: 5px;
        font-style: italic;
        font-size: 32px;
    }

    .ideaSuccessMore {
        padding-left: 300px;
        padding-right: 300px;
        padding-top: 5px;
        margin-top: 75px;
        font-size: 32px;
    }

        .ideaSuccessMore a, .ideaSuccessMore a:hover, .ideaSuccessMore a:visited, questionText a, questionText a:hover, questionText a:visited {
            color: black;
        }

    .ideaCircle {
        width: 600px;
        height: 600px;
        -webkit-border-radius: 300px;
        -moz-border-radius: 300px;
        border-radius: 300px;
    }

    .ideaFormContent {
        margin-left: 350px;
        margin-right: 350px;
        margin-bottom: 15px;
    }

    .ideaForm input[type="text"] {
        background-color: #F01334;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        font-style: italic;
        color: #EEEEEE;
        border-bottom: solid 1px #ffffff;
        font-size: 25px;
        line-height: 25px;
        outline: none;
    }
    .ideaForm input, .ideaForm select{
	font-family: helvetica;
    }

    #sfName {
        width: 320px;
        float: left;
    }

        #sfName input[type="text"] {
            width: 300px;
        }

    #sfAge {
        width: 60px;
        float: left;
    }

        #sfAge input[type="text"] {
            width: 50px;
        }

    .ideaForm select {
        background-color: #F01334;
        border: 0px;
        color: #eeeeee;
        font-size: 25px;
        font-style: italic;
        width: 150px;
        outline: none;
    }

    .styled_select {
        width: 150px;
        border-bottom: 1px solid white;
        position: relative;
        float: left;
    }

    textarea {
        background-color: #F01334;
        border: 0px;
        line-height: 50px;
        width: 500px;
        height: 140px;
        color: #eeeeee;
        font-size: 25px;
        font-style: italic;
        font-family: helvetica;
        background-image: url('/SG50/images/submission/submission_idea_line.png');
        background-position: bottom right;
        background-repeat: no-repeat;
        outline: none;
    }

    .divBtn {
        background-image: url('/SG50/images/submission/submission_submit.png');
        border: none;
        padding-left: 35px;
        padding-right: 10px;
        width: 180px;
        cursor: pointer;
        font-size: 14px;
        height: 35px;
        padding-top: 0px;
        background-color: transparent;
        background-repeat: no-repeat;
        color: white;
        font-size: 20px;
        text-align: left;
    }

    .ideaFormContent hr {
        margin-top: 25px;
        border-width: 0;
        color: white;
        background-color: white;
        margin-left: 20px;
        margin-right: 20px;
        height: 2px;
        margin-bottom: 25px;
    }

    .continue {
        margin-top: 25px;
    }

    .ideaformDiv {
        position: absolute;
        top: 0px;
        left: 0px;
        text-align: center;
    }

    .questionMark {
        float: left;
        padding-left: 55px;
    }

    .questionText {
        float: left;
        font-size: 20px;
        width: 400px;
        text-align: left;
        padding-left: 15px;
	line-height:1.2em;
    }

        .questionText a, .questionText a:hover, .questionText a:visited {
            color: white;
            font-weight: bold;
        }
		.btnDiv{
			text-align:right;
			margin-right:30px;
		}

    
</style>

<div class="ideaForm">
    <canvas id="bgCircle" width="1280" height="768" style="position: absolute; top: 0px; left: 0px;"></canvas>
    <div class="ideaformDiv">
        <asp:Panel ID="pnForm" runat="server">
            <div class="ideaTitle">How will you celebrate Singapore's 50th?</div>
            <div class="ideaBlurb">Each of us has our own idea of a celebration. In a short sentence or two, tell us how you'll celebrate Singapore's 50th birthday. Inspire us and you just might see your idea around your estate.</div>
            <%--<asp:Label runat="server" ID="lblfname" AssociatedControlID="txtfname" Text="First Name "></asp:Label>--%>
            <%--<span style='color: #ff7f00;'>*</span>--%>
            <div class="ideaFormContent">

                <div id="sfName">
                    <asp:TextBox runat="server" ononblur="if(value=='') value = 'Your Name'" onfocus="if(value=='Your Name') value = ''" ID="txtfname" MaxLength="255" Text="Your Name"></asp:TextBox>
                    <div class="errMsg">
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtfname" Display="Dynamic"
                            ValidationGroup="EnquiryForm"
                            ErrorMessage="Please fill in your name." ID="rfvfname"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexfname" ControlToValidate="txtfname" Display="Dynamic"
                            ValidationGroup="EnquiryForm"
                            ErrorMessage="Invalid characters." runat="server" ValidationExpression="^[a-zA-Z'.\s]{1,255}$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div id="sfAge">
                    <asp:TextBox runat="server" ID="txtlname" MaxLength="3" Text="Age" ononblur="if(value=='') value = 'Age'" onfocus="if(value=='Age') value = ''"></asp:TextBox>
                    <div class="errMsg">
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtlname" Display="Dynamic"
                            ValidationGroup="EnquiryForm"
                            ErrorMessage="Please fill in your age." ID="rfvlname"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexlname" ControlToValidate="txtlname" Display="Dynamic"
                            ValidationGroup="EnquiryForm"
                            ErrorMessage="Invalid characters." runat="server" ValidationExpression="^[0-9]{1,3}$"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="styled_select">
                    <asp:DropDownList runat="server" ID="ddlLocations">
                        <asp:ListItem Selected="True" Text="Region" Value="Region"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="North" Value="North"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="East" Value="East"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="South" Value="South"></asp:ListItem>
                        <asp:ListItem Selected="False" Text="West" Value="West"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="errMsg">
                    <asp:RequiredFieldValidator ID="rfvLocations" runat="server" ErrorMessage="Please select Region" ControlToValidate="ddlLocations"
                        ValidationGroup="EnquiryForm" InitialValue="Region"></asp:RequiredFieldValidator>
                </div>

            </div>

            <div class="ideaFormContent">
                <asp:TextBox runat="server" Style="resize: none;" ID="txtemail" MaxLength="120" TextMode="MultiLine" onkeyDown="return checkTextAreaMaxLength(this,event,'120');"  onkeyUp="return checkTextAreaMaxLength(this,event,'120');" Height></asp:TextBox>
                <div class="errMsg">
                    <asp:RequiredFieldValidator ID="rfvemail" ControlToValidate="txtemail" Display="Dynamic"
                        ValidationGroup="EnquiryForm"
                        ErrorMessage="Please fill in your idea." runat="server"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regexemail" ControlToValidate="txtemail" Display="Dynamic"
                        ValidationGroup="EnquiryForm"
                        runat="server" ValidationExpression="[^=}{><]*"
                        ErrorMessage="Invalid characters."></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="ideaFormContent">
                <div class="btnDiv">
                    <asp:Button ID="btnCommentCancel" runat="server" Text="CANCEL" CausesValidation="False" CssClass="divBtn" OnClick="btnCancel_Click" />
                    <asp:Button ID="btnSubmit" runat="server" CssClass="divBtn"
                        ValidationGroup="EnquiryForm" Text="SUBMIT IDEA" OnClick="btnSubmit_Click" />
                </div>
            </div>
            <div class="ideaFormContent">
                <hr />
                <div class="questionMark">
                    <img src="\SG50\images\submission\submission_icon1.png" />
                </div>
                <div class="questionText">
                    Having trouble coming up with an idea? Click <a href="<%= getCelebrationIdeaLink() %>">here</a>.
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnSuccess" runat="server" Visible="false">


            <div class="ideaSuccessTitle">Thank you for your contribution.</div>
            <div class="ideaSuccessBlurb">We know you’ve put quite a bit of thought into your celebration idea. It means a lot to us as much as it does to you. So if you’d like to bring this idea to life, we’d love to help you out with funding.</div>
            <div class="ideaSuccessMore">To find out more, click <a href="<%= getWaytoCelebrateLink() %>">here</a></div>
            <div class="continue">
                <a href="<%= getLink() %>">
                    <img src="\SG50\images\continuebtn.jpg" /></a>
            </div>
        </asp:Panel>
    </div>

    <script type="text/javascript">
        var canvas = document.getElementById('bgCircle');
        var context = canvas.getContext('2d');
        //var centerX = window.innerWidth / 2;
        var centerX = 1280 / 2;
        var centerY = (canvas.height / 2) - 50;
        var radius = 450;

        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        context.lineWidth = 2;
        context.strokeStyle = '#ffffff';
        context.stroke();
        // Start ** Limit number of input characters
        document.onmousedown = disableclick;
        var status = "Right Click Disabled";

        function checkTextAreaMaxLength(textBox, e, length) {

            var mLen = textBox["MaxLength"];
            if (null == mLen)
                mLen = length;

            var maxLength = parseInt(mLen);
            if (!checkSpecialKeys(e)) {
                if (textBox.value.length > maxLength - 1) {
                    if (window.event)//IE
                    {
                        e.returnValue = false;
                        return false;
                    }
                    else//Firefox
                        e.preventDefault();
                    if (textBox.value.length > length && $("#content_0_txtemail").val() != undefined) {
                        $("#content_0_txtemail").val($("#content_0_txtemail").val().slice(0, -($("#content_0_txtemail").val().length - length)));
                    }
                }
            }
        }

        function disableclick(event) {
            if (event.button == 2) {
                alert(status);
                return false;
            }
        }

        function checkSpecialKeys(e) {
            if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 35 && e.keyCode != 36 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                return false;
            else
                return true;
        }
        // End ** Limit number of input characters
    </script>
</div>
