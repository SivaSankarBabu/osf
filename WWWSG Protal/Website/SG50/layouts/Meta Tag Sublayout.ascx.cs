﻿using Sitecore.Data.Items;
using System;
using System.Configuration;

namespace Layouts.Meta_tag_sublayout
{

    /// <summary>
    /// Summary description for Meta_tag_sublayoutSublayout
    /// </summary>
    public partial class Meta_tag_sublayoutSublayout : System.Web.UI.UserControl
    {
        private Item itmMain = Sitecore.Context.Item;
        private Item itmOGTitle = SG50Class.web.GetItem(SG50Class.str_FBShare_Title_Item_ID);

        private void Page_Load(object sender, EventArgs e)
        {
            FillinMetaTag();
        }

        private void FillinMetaTag()
        {
            string metaOGTitle = "";
            string metaOGDesp = "";

            // if (itmMain.Fields["About Image"] != null && !itmMain["About Image"].ToString().Trim().Equals(""))
            // {
            // Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)itmMain.Fields["About Image"]);
            // if (imgField.MediaItem != null)
            // {
            // string imgSrc = ConfigurationManager.AppSettings["SG50websiteURL"] + Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem) + "?w=250";

            // metaFBShareImg = metaFBShareImg.Replace("[imgLink]", imgSrc);

            // litMetaTag.Text = metaFBShareImg;
            // }
            // }

            if (itmMain.Fields["Idea"] != null && !itmMain["Idea"].ToString().Trim().Equals(""))
            {
                metaOGDesp = "<meta property=\"og:description\" content=\"" + itmMain.Fields["Idea"].ToString().Replace("\"", "&quot;") + "\"/>";
            }

            litMetaTag.Text += "<meta property=\"og:type\" content=\"website\" />"+"\n";

            if (itmMain.ID.ToString() == "{54047EF7-4E9F-4757-833F-7AB656A39438}" || itmMain.ID.ToString() == "{031B0304-5A24-4EC7-B206-9EF71FB6C0CE}" || itmMain.ID.ToString() == "{A0F57D01-6124-422C-9EA0-F47A0B114032}" || itmMain.ID.ToString() == "{F7A324C7-C198-4F3B-BEC5-17383571C176}" || itmMain.ID.ToString() == "{E3387D22-9A6E-4FBF-A018-28F7EB755672}")
            {
                litMetaTag.Text += "<meta property=\"Title\" content=\"" + itmMain["Meta Title"] + "\" />" + "\n";
                litMetaTag.Text += "<meta property=\"Description\" content=\"" + itmMain["Meta Description"] + "\" />" + "\n";
                litMetaTag.Text += "<meta property=\"keywords\" content=\"" + itmMain["Tags for SGspirit"] + "\" />" + "\n";
                litMetaTag.Text += "<meta property=\"keywords\" content=\"" + itmMain["Key Words"] + "\" />" + "\n";

            }
            else
            {
                litMetaTag.Text += "<meta property=\"og:title\" content=\"" + itmOGTitle.Fields["Value"].ToString() + "\" />";
                litMetaTag.Text += "<meta property=\"og:url\" content=\"" + ConfigurationManager.AppSettings["SG50websiteURL"] + "\" />";
                litMetaTag.Text += metaOGDesp;
                litMetaTag.Text += "<meta property=\"og:image\" content=\"" + ConfigurationManager.AppSettings["SG50websiteURL"] + "/SG50/images/imgcommitee01.png\"/>";
                litMetaTag.Text += "<meta property=\"Keywords\" content=\"" + itmMain["Keywords"] + "\" />";
                litMetaTag.Text += "<meta property=\"Description\" content=\"" + itmMain["Description"].Replace("\"", "") + "\" />";
            }

            
        }
    }
}