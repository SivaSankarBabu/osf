﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Events_listing_sublayout.Events_listing_sublayoutSublayout"
    CodeFile="~/SG50/layouts/Events Listing Sublayout.ascx.cs" %>
<div class="press">
    <div class="pressBanner">
        <div id="pressBannerInner">
            <div class="pressBannerImage desktop">
                <sc:image runat="server" field="Banner Image" datasource="{6EBFAFA6-ED1B-4DBE-8805-4FA379E281BF}" />
            </div>
            <div class="pressBannerImage mobile">
                <sc:image runat="server" field="Banner Image For Responsive" datasource="{B7B44CFD-57E7-4603-9298-9B6EDD44138C}" />
            </div>
            <div class="pressBannerCaption desktop">
                <h1>
                    MARK YOUR CALENDARS AND PENCIL US IN.</h1>
            </div>
            <div class="pressBannerCaption mobile">
                <h1>
                    MARK YOUR CALENDARS AND PENCIL US IN.</h1>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var dom = {};
    dom.query = jQuery.noConflict(true);
</script>

<script type="text/javascript" src="/SG50/include/JS/jquery-1.11.1.js"></script>

<script src="/SG50/include/jqtransformplugin/jquery.jqtransform.js"></script>

<link href="/SG50/include/jqtransformplugin/jqtransform.css" rel="stylesheet" />
<link href="/SG50/include/css/style.css" type="text/css" rel="stylesheet" />
<link href="/SG50/include/Css/responsive_Grid.css" rel="stylesheet" />

<script src="/SG50/include/js/isotope.pkgd.min.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript">
    $(function() {
        $('.grid-dropdown').jqTransform({ imgPath: '/SG50/include/jqtransformplugin/img/' });
    });
</script>

<style>
    .contentContainer
    {
        width: 1280px;
        min-height: 768px;
        background-color: #FFF;
        overflow: hidden;
        background-image: url('/SG50/images/icon3.png');
        background-repeat: no-repeat;
        background-position: right bottom;
    }
    .leftContentContainer
    {
        width: 1020px;
        float: left;
        min-height: 500px;
        background-image: url('/SG50/images/shadow.jpg');
        background-repeat: no-repeat;
        background-position: right top;
    }
    .rightContentContainer
    {
        width: 260px;
        float: left;
    }
    .rightSubContent
    {
        padding-left: 20px;
        padding-right: 20px;
    }
    .rightSubContent.eventCalendarDiv
    {
        padding-left: 10px;
        padding-right: 10px;
    }
    .leftContent
    {
        margin-left: 115px;
        margin-right: 30px;
        margin-bottom: 30px;
    }
    .breadcrumb
    {
        margin-top: 25px;
        font-size: 16px;
        color: #ef2c2e;
    }
    .breadcrumb img
    {
        width: 10px;
        height: 10px;
    }
    .breadcrumb a, .breadcrumb a:hover, .breadcrumb a:visited
    {
        color: #353333;
    }
    .pressTitle
    {
        margin-top: 50px;
        font-size: 32px;
        border-bottom: solid 1px #113706;
    }
    .rightTitle
    {
        font-size: 18px;
        color: #ef2c2e;
        margin-top: 25px;
        margin-bottom: 20px;
    }
    .rightImage
    {
        margin-top: 569px;
    }
    .PortfolioPagination
    {
        width: 100%;
        text-align: right;
        margin-top: 5px;
        vertical-align: top;
    }
    .prTitle
    {
        font-size: 20px;
        line-height: 40px;
    }
    .prDate
    {
        font-size: 16px;
        line-height: 25px;
        color: #353333;
    }
    .prBlurb
    {
        font-size: 16px;
        line-height: 20px;
        margin-top: 15px;
        margin-bottom: 25px;
    }
    .prItem
    {
        margin-top: 25px;
        padding-bottom: 15px; /*border-bottom:dotted 1px #353333;*/
        border-bottom: solid 1px #ccc;
    }
    .prItem a, .prItem a:hover, .prItem a:visited
    {
        color: black;
    }
    .prLatest
    {
        padding-top: 15px;
        color: #353333;
    }
    .prLatest a, .prLatest a:hover, .prLatest a:visited
    {
        color: #353333;
        font-size: 14px;
    }
    .divider
    {
        height: 15px;
        border-bottom: 1px solid #f0f0f0;
        margin-left: 0px;
        margin-right: 0px;
    }
</style>
<style>
    .calendarLeft
    {
        border-right: 1px solid #f0f0f0;
        border-top: 1px solid #f0f0f0;
        border-bottom: 1px solid #f0f0f0;
    }
    .calendarRight
    {
        border-left: 1px solid #f0f0f0;
        border-top: 1px solid #f0f0f0;
        border-bottom: 1px solid #f0f0f0;
    }
    .calendarCenter
    {
        border-left: 1px solid #f0f0f0;
        border-right: 1px solid #f0f0f0;
        border-top: 1px solid #f0f0f0;
        border-bottom: 1px solid #f0f0f0;
    }
    .tblEventCalendar
    {
        border-spacing: 0px;
        font-size: 12px;
        border: 1px solid #d9d9d9;
    }
    .tblEventCalendar tr td
    {
        vertical-align: middle;
    }
    #calendar
    {
        margin-top: 35px;
    }
    .eventTitle
    {
        margin-top: 30px;
        font-size: 30px;
        border-bottom: solid 1px #113706 !important;
        color: #353333;
    }
    .eventTab
    {
        float: right;
        margin-top: 10px;
        font-size: 16px;
    }
    .eventTab div
    {
        color: #353333;
        padding: 10px;
        display: inline;
        cursor: pointer;
        font-size: 14px;
    }
    .eventTab .active
    {
        color: #fff;
        background-color: #ef2c2e;
        font-weight: bold;
    }
    .ecTitle
    {
        font-size: 20px;
        color: #353333;
        line-height: 1.2em;
    }
    .ecDate
    {
        font-size: 14px;
        line-height: 25px;
        color: #353333;
    }
    .ecBlurb
    {
        font-size: 16px;
        line-height: 20px;
        margin-top: 15px;
        min-height: 128px;
        width: 635px;
        margin-left: 25px;
        float: left;
    }
    .ecImg
    {
        width: 205px;
        float: left;
        margin-top: 20px;
    }
    .ecItem
    {
        margin-top: 10px;
        padding-bottom: 15px; /*border-bottom:dotted 1px #353333;*/
        border-bottom: solid 1px #ccc;
    }
    .ecItemHide
    {
        display: none;
    }
    .ecItem a, .ecItem a:hover, .ecItem a:visited
    {
        color: black;
        font-size: 16px;
    }
    .ecBlurb, ecBlurb p
    {
        font-size: 16px;
        line-height: 25px;
        color: #565656;
    }
</style>
<style>
    .page_navigation
    {
        width: 100%;
        text-align: right;
        margin-top: 10px;
        vertical-align: top;
    }
    .page_navigation a
    {
        padding: 3px;
        margin: 2px;
        color: #353333;
        text-decoration: none;
        font-size: 14px;
    }
    .page_navigation a.previous_link, .page_navigation a.next_link
    {
        font-size: 12px;
    }
    .active_page
    {
        color: red !important;
    }
    @media screen and (min-width: 300px) and (max-width: 768px)
    {
        .contentContainer
        {
            width: 100%;
        }
        .leftContentContainer
        {
            width: 100%;
        }
        .rightContentContainer
        {
            display: none;
        }
        .leftContent
        {
            margin-top: 30px;
            margin-left: 10px;
            margin-right: 10px;
        }
        .eventTab
        {
            float: left;
            width: 100%;
            margin-bottom: 10px;
            padding-top: 10px; /* border-bottom: 1px dotted black;*/
            padding-bottom: 3px;
        }
        .ecBlurb
        {
            float: left;
            width: 98%;
            margin-left: 0;
            font-size: 16px;
        }
    }
    .grid
    {
        height: 400px;
    }
    @media (max-width: 768px) and (min-width: 481px)
    {
        .grid
        {
            height: 450px !important;
        }
    }
    .video-player
    {
        height: 0;
        overflow: hidden;
        padding-bottom: 55%;
        padding-top: 30px;
        position: relative;
    }
    .video-player iframe, .video-player object, .video-player embed
    {
        height: 100%;
        left: 0;
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $("#dinUpcoming").show();
        $("#divpast").hide();
        $("#divpast").css({ "display": "none" });
    });

</script>

<p>
    <a onclick='postToFeed();'></a>
</p>
<p id='msg'>
</p>

<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...

        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        //fbtitle = Title.replace(/'/gi, "")
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            //caption: fbdes,
            description: fbdes
        };

        //function callback(response) {

        //    document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
        //}

        FB.ui(obj);

    }

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#dinUpcoming").show();
        $("#divpast").hide();

    });

</script>

<sc:text id="txtFBAPIID" field="Facebook AppID" runat="server" datasource="{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}"
    visible="false" />
<section class="main-content">
    <!---main-content start-------->
    <section class="grid-content">
        <div>

            <ul class="title-tabs">

                <div class="eventTab">
                    <div id="eventTabUpcoming" class="active">CURRENT & UPCOMING</div>
                    <div id="eventTabPast" class="">PAST</div>
                </div>
            </ul>
            <div class="dropdown-container">

                <div id="divyear" class="grid-dropdown">

                    <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true" CausesValidation="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                        <asp:ListItem Value="0">Year</asp:ListItem>
                        <asp:ListItem Value="2015">2015</asp:ListItem>
                        <asp:ListItem Value="2014">2014</asp:ListItem>
                    </asp:DropDownList>

                </div>
                <div id="divmonth" class="grid-dropdown" style="position: relative">

                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="" CausesValidation="true" AutoPostBack="true" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                        <asp:ListItem Value="0">Month</asp:ListItem>
                        <asp:ListItem Value="01">January</asp:ListItem>
                        <asp:ListItem Value="02">February</asp:ListItem>
                        <asp:ListItem Value="03">March</asp:ListItem>
                        <asp:ListItem Value="04">April</asp:ListItem>
                        <asp:ListItem Value="05">May</asp:ListItem>
                        <asp:ListItem Value="06">June</asp:ListItem>
                        <asp:ListItem Value="07">July</asp:ListItem>
                        <asp:ListItem Value="08">August</asp:ListItem>
                        <asp:ListItem Value="09">September</asp:ListItem>
                        <asp:ListItem Value="10">October</asp:ListItem>
                        <asp:ListItem Value="11">November</asp:ListItem>
                        <asp:ListItem Value="12">December</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="head-title">
            <h1>
                <sc:Text ID="Text1" Field="Content Title" runat="server" />
            </h1>
            <sc:FieldRenderer ID="txtDescription" runat="server" FieldName="Content Description" />
            <a href="#" id="calender" runat="server" onserverclick="DownloadCalendar">a simple click</a>.</p>
            
        </div>
        <div id="dinUpcoming" class="grid-container container1">

            <div>
                <asp:Repeater ID="repUpcomingEvents" runat="server">
                    <ItemTemplate>
                        <div class="grid">
                            <a href=''>
                                <div class="video-player" style="position: relative" id="DivImagestag" visible="true" runat="server">
                                    <asp:Image runat="server" ID="repImg" Style="position: absolute; top: 0; left: 0" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>' />
                                    <iframe style="z-index: 3; position: absolute; top: 0; left: 0" src="<%#Eval("videoURL")%>" frameborder="0"></iframe>
                                </div>
                            </a>
                            <h2>
                                <sc:Text ID="Text1" Field="Content Title" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </h2>
                            <span>
                                <asp:Label ID="lblEventDates" runat="server" Text='<%#Eval("EventDate")%>'></asp:Label>
                            </span>
                            <p>
                                <sc:Text ID="Text4" Field="Blurb" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </p>
                            <p><a href='<%# DataBinder.Eval(Container.DataItem, "href") %>'>Read More</a></p>
                            <ul>
                                <li><a href="JavaScript:postToFeed('<%# Eval("SocialTitle").ToString()%>','<%# Eval("SocialContent").ToString()%>','<%# Eval("socialThumbnail")%>','<%# Eval("EventUrl")%>')">
                                    <img src="/SG50/images/facebook.png" /></a></li>
                                <li><a href="JavaScript:newPopup('<%# Eval("TwitterContent")%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')">
                                    <img src="/SG50/images/twitter.png" /></li>
                            </ul>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div id="divpast" class="grid-container container2">
            <div>
                <asp:Repeater ID="repPastEvents" runat="server">
                    <ItemTemplate>
                        <div class="grid">
                            <a href="#">
                                <%--<div id="DivImagestag" visible="true" runat="server" style="position: relative" class="video-player">
                                    <asp:Image runat="server" ID="repImg" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>' />
                                </div>--%>
                                <div class="video-player" style="position: relative" id="DivImagestag" visible="true" runat="server">
                                    <asp:Image runat="server" ID="repImg" Style="position: absolute; top: 0; left: 0" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>' />
                                    <iframe style="z-index: 3; position: absolute; top: 0; left: 0" src="<%#Eval("videoURL")%>" frameborder="0"></iframe>
                                </div>
                            </a>
                            <h2>
                                <sc:Text ID="Text1" Field="Content Title" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </h2>
                            <span>
                                <sc:Date ID="Date1" Field="Event Date" runat="server" Format="dd MMMM yyyy" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </span>
                            <p>
                                <sc:Text ID="Text4" Field="Blurb" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </p>
                            <p><a href='<%# DataBinder.Eval(Container.DataItem, "href") %>'>Read More</a></p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <asp:Label ID="lblSearchResult" ForeColor="Red" Font-Bold="true" runat="server" Visible="false"></asp:Label>

        </div>
    </section>
</section>

<script type="text/javascript">

    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function(response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }


    $("#eventTabUpcoming").click(function() {
        // $("#eventToday").hide();
        $("#dinUpcoming").show();
        $("#divpast").hide();
        // $("#eventTabToday").removeClass("active");
        $("#eventTabUpcoming").addClass("active");
        $("#eventTabPast").removeClass("active");
        $("#divmonth").show();
        $("#divyear").show();
        $("#divpast").css({ "display": "none" });

    });

    $("#eventTabPast").click(function() {
        //$("#eventToday").hide();
        $("#dinUpcoming").hide();
        $("#divpast").show();
        // $("#eventTabToday").removeClass("active");
        $("#eventTabUpcoming").removeClass("active");
        $("#eventTabPast").addClass("active");
        $("#divmonth").hide();
        $("#divyear").hide();
        $("#dinUpcoming").css({ "display": "none" });
    });
</script>

<div class="rightContentContainer" style="display: none">
    <div class="rightContent">
        <div class="rightSubContent eventCalendarDiv">
            <div id="calendar">
                <!--  Dynamically Filled -->
            </div>
        </div>
    </div>
</div>
