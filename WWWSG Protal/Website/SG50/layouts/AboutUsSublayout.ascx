﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Aboutussublayout.AboutussublayoutSublayout" CodeFile="~/SG50/layouts/AboutUsSublayout.ascx.cs" %>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<link href="/SG50/include/css/style_About.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_About.css" rel="stylesheet" type="text/css">
<!--existing site css-->
<link rel="stylesheet" type="text/css" href="/SG50/include/Css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/Css/responsive.css" />
<link href="/SG50/include/Css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<script src="/SG50/include/js/jquery-1.11.1.js"></script>

<div id="homePage" class="container rew">
    <!--Start OF Banner-->
    <div id="banner" class="section current">
        <sc:Image ID="BannerImg" runat="server" Field="Banner Image" />
        <div class="about-us featured-in" id="DivLittleDot">
            <div class="about-left">
                <sc:FieldRenderer ID="txtTitle" runat="server" FieldName="About Title" />
                <sc:FieldRenderer ID="aboutContent" runat="server" FieldName="About Content" />
                <sc:Image ID="logoImg" runat="server" CssClass="about-icon" Field="About Image" />
                <div class="clear">
                </div>
                <a href="#" id="dlLnkLogoAndGuide" runat="server" onserverclick="DownloadForm">
                    <sc:Text ID="btnText" Field="Button Text" runat="server" />
                </a>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>

    <div class="clear initiatives-pan pad section" id="divinitiatives">
        <div class="featured-in">
            <h3><span>Our Initiatives</span></h3>

            <div class="intiatives-desktop">

                <asp:Repeater ID="repInitiatives" runat="server">
                    <ItemTemplate>
                        <ul class="<%# DataBinder.Eval(Container.DataItem, "LeftRightimg") %>">
                            <li class="img-box"><span class="<%# DataBinder.Eval(Container.DataItem, "LeftRightArrow") %>"></span>
                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" alt="<%# DataBinder.Eval(Container.DataItem, "alt") %>" /></li>
                            <li class="text-box">
                                <div class="ourintiatives-cont">
                                    <h4>
                                        <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label></h4>
                                    <p>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Label>
                                    </p>
                                    <a id="A1" href='<%#Eval("href")%>' runat="server" visible='<%#Eval("Status")%>' class="readmore-btn"><%#Eval("ButtonText")%>                                      
                                        
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </ItemTemplate>
                </asp:Repeater>


                <%--   <ul class="ourintiatives leftimg">
                    <li class="img-box">
                        <span class="arrowleft"></span>
                        <img src="images/in_img1.jpg" alt="" />
                    </li>
                    <li class="text-box">
                        <div class="ourintiatives-cont">
                            <h4>Pioneers of tomorrow</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat leo vitae elit bibendum condimentum</p>
                            <a href="#">READ MORE</a>
                        </div>
                    </li>
                </ul>
                <ul class="ourintiatives rightimg">
                    <li class="img-box">
                        <span class="arrowright"></span>
                        <img src="images/in_img2.jpg" alt="" />

                    </li>
                    <li class="text-box">
                        <div class="ourintiatives-cont">
                            <h4>the pioneering spirit.since 1965</h4>
                            <p>They were the true trailblazers. The men and women who dare to dream. And their pioneering spirit still lives on in every one of us.</p>
                            <a href="#">READ MORE</a>
                        </div>
                    </li>
                </ul>--%>
            </div>
            <div class="clear"></div>
        </div>
    </div>


    <!--End OF Banner-->
    <!--Start OF way to go team pan-->
    <div class="clear featured-pan pad section">
        <div class="featured-in way-to">
            <h3>
                <span>
                    <sc:Text ID="txtway" Field="Way To Go Title" runat="server" />
                </span>
            </h3>
            <sc:Text ID="txtWayContent" Field="Way To Go Content" runat="server" />
            <ul>
                <li>
                    <sc:Image ID="Img1" runat="server" Field="Image1" />
                    <span>
                        <sc:Text ID="txt1" Field="Text1" runat="server" />
                    </span></li>
                <li>
                    <sc:Image ID="Img2" runat="server" Field="Image2" />
                    <span>
                        <sc:Text ID="txt2" Field="Text2" runat="server" />
                    </span></li>
                <li>
                    <sc:Image ID="Img3" runat="server" Field="Image3" />
                    <span>
                        <sc:Text ID="txt3" Field="Text3" runat="server" />
                    </span></li>
                <li>
                    <sc:Image ID="Img4" runat="server" Field="Image4" />
                    <span>
                        <sc:Text ID="txt4" Field="Text4" runat="server" />
                    </span></li>
                <li>
                    <sc:Image ID="Img5" runat="server" Field="Image5" />
                    <span>
                        <sc:Text ID="txt5" Field="Text5" runat="server" />
                    </span></li>
            </ul>
            <div class="clear">
            </div>
        </div>
    </div>
    <!--End OF way to go team pan-->
    <!--Start OF Meet the committee pan-->
    <div class="clear news-feed pad section COMMITTEEBlk" id="DivMeetthecommitte">
        <div class="featured-in">
            <sc:Text ID="txtmeet" Field="Meet The Committee Title" runat="server" />
            <sc:Image ID="ImgMeet" runat="server" Field="Meet The Committee image" />
        </div>
    </div>
    <!--End OF Meet the committee pan-->
</div>



