﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System.Xml;
using System.Data;
using System.IO;
using Sitecore.Publishing;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;
using CAPTCHA;
using FUNDED_Logger;
using System.Globalization;
namespace Layouts.Applicationstep3
{

    public partial class Applicationstep3Sublayout : System.Web.UI.UserControl
    {
        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        DataTable dtBudget;
        DataTable dtDonations;
        DataTable dtTimeLine;
        DataTable dtFiles;
        string[] fileName = new string[5];
        string Searchword, Searchword1, Replaceword;
        Item itmContext = Sitecore.Context.Item;
        CaptchaGenerator cgen;

        string strddlSalutationAppone = string.Empty;
        string strapplicantonename = string.Empty;
        string strNricnum = string.Empty;
        string strOccupationAppOne = string.Empty;
        string strddlGenderAppOne = string.Empty;
        string strDobAppone = string.Empty;
        string strAddressAppOne = string.Empty;
        string strMobileNumAppone = string.Empty;
        string strPhoneNumberAppOne = string.Empty;
        string strEmailAppOne = string.Empty;

        string strddlSalutationAppTwo = string.Empty;
        string strddlIdType = string.Empty;
        string strtxtIdNumAppTwo = string.Empty;
        string strNameAppTwo = string.Empty;
        string strOccupationAppTwo = string.Empty;
        string strDobAppTwo = string.Empty;
        string strAddressAppTwo = string.Empty;
        string strMobileNumberAppTwo = string.Empty;
        string strPhoneNumberAppTwo = string.Empty;
        string strEmailAppTwo = string.Empty;

        string strddlSalutationAppTwoOver = string.Empty;
        string strNameAppTwoOver = string.Empty;
        string strNricOver = string.Empty;
        string strOccupationAppTwoOver = string.Empty;
        string strDobAppTwoOver = string.Empty;
        string strAddressAppTwoOver = string.Empty;
        string strMobileNumberAppTwoOver = string.Empty;
        string strPhoneNumberAppTwoOver = string.Empty;
        string strEmailAppTwoOver = string.Empty;
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["Local"] == null)
                {
                    Logger.WriteLine("8. Session Values is empty in Step3 page load. so redirecting to Step 1.");
                    string path = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('There is an error occurred during the submission, Please fill again.'); window.location='" + "/SG50/Apply For Funding.aspx';", true);
                }

                if (!IsPostBack)
                {

                    Logger.WriteLine("9. Funded form step3 process Started");
                    cgen = new CaptchaGenerator();
                    GetCaptchaimage();
                    Session.Timeout = 45;

                    if (Session["Local"] != null && !string.IsNullOrEmpty(Session["Local"].ToString()))
                    {
                        if (Session["Local"].ToString() == "False")
                        {
                            Logger.WriteLine("10. Displaying OverSeas Section in step3");

                            Label1.Text = "";
                            Label2.Text = "";
                            Overseas.Visible = true;

                        }
                        if (Session["Local"].ToString() == "True")
                        {
                            Logger.WriteLine("10. Displaying Local Section in step3");
                            Label1.Text = " (in NRIC)";
                            Label2.Text = "(in Singapore)";
                            divApplicanttwo.Visible = true;
                        }

                    }
                    else
                    {
                        Logger.WriteLine("10. Session is empty in step 3 so redirecting to step1. Unable to display Local/Overseas Section");
                        string path = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('There is an error occurred during the submission, Please fill again.'); window.location='" + "/SG50/Apply For Funding.aspx';", true);
                    }

                    Sitecore.Data.Fields.CheckboxField CheckboxTerms = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Terms and Conditions"]);
                    if (CheckboxTerms.Checked == true)
                    {
                        // Response.Write("Test1");
                        DivTerms.Visible = true;
                    }

                    Sitecore.Data.Fields.CheckboxField CheckboxAgreement = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Agreement"]);
                    if (CheckboxAgreement.Checked == true)
                    {
                        // Response.Write("Test2");
                        DivAgreement.Visible = true;
                    }

                    Sitecore.Data.Fields.CheckboxField CheckboxSupporting = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Supporting Documents"]);
                    if (CheckboxSupporting.Checked == true)
                    {
                        //Response.Write("Test3");
                        DivSupporting.Visible = true;
                    }
                }
                Sitecore.Data.Database webData = Sitecore.Context.Database;

                if (webData.GetItem("{36AA7F1D-216D-4C76-ABA9-1EAB325D3643}") != null)
                {
                    Item mediaItem = webData.GetItem("{36AA7F1D-216D-4C76-ABA9-1EAB325D3643}");
                    string mediaUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItem));
                }
                if (webData.GetItem("{600FA06E-D4CD-4375-9EE4-C6299DC53B5D}") != null)
                {
                    Item mediaItemAgreement = webData.GetItem("{600FA06E-D4CD-4375-9EE4-C6299DC53B5D}");
                    string mediaUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItemAgreement));
                }
                if (Session["Budjet"] != null)
                {
                    dtBudget = (DataTable)Session["Budjet"];
                }
                if (Session["donations"] != null)
                {
                    dtDonations = (DataTable)Session["donations"];
                }

                if (Session["Timeline"] != null)
                {
                    dtTimeLine = (DataTable)Session["Timeline"];
                }
                if (Session["FileId"] != null)
                {
                    dtFiles = (DataTable)Session["FileId"];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }

        public bool validationCheck()
        {
            bool validation = false;

            try
            {
                strddlSalutationAppone = ddlSalutationAppone.SelectedItem.Text;
                strapplicantonename = SG50Class.StripUnwantedString(txtapplicantonename.Text.TrimEnd());
                strNricnum = SG50Class.StripUnwantedString(txtNricnum.Text.TrimEnd());
                strOccupationAppOne = SG50Class.StripUnwantedString(txtOccupationAppOne.Text.TrimEnd());
                strddlGenderAppOne = ddlGenderAppOne.SelectedItem.Text;
                strDobAppone = txtDobAppone.Text.TrimEnd();
                strAddressAppOne = SG50Class.StripUnwantedString(txtAddressAppOne.Text.TrimEnd());
                strMobileNumAppone = SG50Class.StripUnwantedString(txtMobileNumAppone.Text.TrimEnd());
                strPhoneNumberAppOne = SG50Class.StripUnwantedString(txtPhoneNumberAppOne.Text.TrimEnd());
                strEmailAppOne = SG50Class.StripUnwantedString(txtEmailAppOne.Text.TrimEnd());

                strddlSalutationAppTwo = ddlSalutationAppTwo.SelectedItem.Text;
                strddlIdType = ddlIdType.SelectedItem.Text;
                strtxtIdNumAppTwo = SG50Class.StripUnwantedString(txtIdNumAppTwo.Text.TrimEnd());
                strNameAppTwo = SG50Class.StripUnwantedString(txtNameAppTwo.Text.TrimEnd());
                strOccupationAppTwo = SG50Class.StripUnwantedString(txtOccupationAppTwo.Text.TrimEnd());
                strDobAppTwo = txtDobAppTwo.Text.TrimEnd();
                strAddressAppTwo = SG50Class.StripUnwantedString(txtAddressAppTwo.Text.TrimEnd());
                strMobileNumberAppTwo = SG50Class.StripUnwantedString(txtMobileNumberAppTwo.Text.TrimEnd());
                strPhoneNumberAppTwo = SG50Class.StripUnwantedString(txtPhoneNumberAppTwo.Text.TrimEnd());
                strEmailAppTwo = SG50Class.StripUnwantedString(txtEmailAppTwo.Text.TrimEnd());

                strddlSalutationAppTwoOver = ddlSalutationAppTwoOver.SelectedItem.Text;
                strNameAppTwoOver = SG50Class.StripUnwantedString(txtNameAppTwoOver.Text.TrimEnd());
                strNricOver = SG50Class.StripUnwantedString(txtNricOver.Text.TrimEnd());
                strOccupationAppTwoOver = SG50Class.StripUnwantedString(txtOccupationAppTwoOver.Text.TrimEnd());
                strDobAppTwoOver = txtDobAppTwoOver.Text.TrimEnd();
                strAddressAppTwoOver = SG50Class.StripUnwantedString(txtAddressAppTwoOver.Text.TrimEnd());
                strMobileNumberAppTwoOver = SG50Class.StripUnwantedString(txtMobileNumberAppTwoOver.Text.TrimEnd());
                strPhoneNumberAppTwoOver = SG50Class.StripUnwantedString(txtPhoneNumberAppTwoOver.Text.TrimEnd());
                strEmailAppTwoOver = SG50Class.StripUnwantedString(txtEmailAppTwoOver.Text.TrimEnd());

                DateTime d;
                int EnteredIntValue = 0;
                if (strapplicantonename != "" && strNricnum != "" && strOccupationAppOne != "" && strDobAppone != "" && strAddressAppOne != "" && strMobileNumAppone != "" && strEmailAppOne != "" && strddlSalutationAppone != "Select" && strddlGenderAppOne != "Select")
                {
                    if (int.TryParse(strMobileNumAppone, out EnteredIntValue) && DateTime.TryParseExact(strDobAppone, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                    {
                        if (strapplicantonename.Length <= 66 && strNricnum.Length <= 9 && strOccupationAppOne.Length <= 100 && strAddressAppOne.Length <= 120 && strMobileNumAppone.Length == 8)
                        {
                            validation = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
            return validation;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    if (validationCheck())
                    {
                        if (txt_CaptchaVal.Text == SG50Class.StripUnwantedString(txt_Captcha.Text))
                        {
                            if (Session["ItemId"] != null && Session["ItemId"].ToString() != "")
                            {

                                Logger.WriteLine("11. Submitting step3 details.");
                                Logger.WriteLine("12. NRIC " + txtNricnum.Text);
                                string ItemId = Session["ItemId"].ToString();
                                Item FormItem = master.GetItem(ItemId);

                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {

                                    try
                                    {
                                        FormItem.Editing.BeginEdit();
                                        FormItem.Name = txtNricnum.Text + "-" + DateTime.Now.ToString("yyMMdd") + "-" + DateTime.Now.ToString("hhmmss");
                                        Session["ApplicationName"] = FormItem.Name;
                                        FormItem.Fields["salutation1"].Value = Convert.ToString(ddlSalutationAppone.SelectedItem.Text);
                                        FormItem.Fields["Name Of Applicant1"].Value = strapplicantonename;
                                        FormItem.Fields["NRIC Number1"].Value = strNricnum;
                                        FormItem.Fields["Nationality1"].Value = Convert.ToString("Singaporean");
                                        FormItem.Fields["Occupation1"].Value = strOccupationAppOne;
                                        FormItem.Fields["Gender1"].Value = Convert.ToString(ddlGenderAppOne.SelectedItem.Text);
                                        FormItem.Fields["Date Of Birth1"].Value = strDobAppone;
                                        FormItem.Fields["Address1"].Value = strAddressAppOne;
                                        FormItem.Fields["MobileNumber1"].Value = strMobileNumAppone;
                                        FormItem.Fields["Home Number1"].Value = strPhoneNumberAppOne;
                                        FormItem.Fields["Email Address1"].Value = strEmailAppOne;
                                        FormItem.Fields["Postal Code1"].Value = "";// Convert.ToString(txtPostalCodeAppOne.Text);

                                        if (Session["Local"] != null && !string.IsNullOrEmpty(Session["Local"].ToString()))
                                        {
                                            if (Session["Local"].ToString() == "True")
                                            {
                                                if (strtxtIdNumAppTwo != "" && strNameAppTwo != "" && strOccupationAppTwo != "" && strDobAppTwo != "" && strAddressAppTwo != "" && strMobileNumberAppTwo != "" && strEmailAppTwo != "" && strddlSalutationAppTwo != "Select" && strddlIdType != "Select")
                                                {
                                                    DateTime d;
                                                    int EnteredIntValue = 0;
                                                    int IDNumberLength = 0;
                                                    if (ddlIdType.SelectedValue == "NRIC")
                                                        IDNumberLength = 9;
                                                    else if (ddlIdType.SelectedValue == "PASSPORT")
                                                        IDNumberLength = 20;
                                                    if (strNameAppTwo.Length <= 66 && strtxtIdNumAppTwo.Length <= IDNumberLength && strOccupationAppTwo.Length <= 100 && strMobileNumberAppTwo.Length == 8 && strEmailAppTwo.Length <= 320 && int.TryParse(strMobileNumberAppTwo, out EnteredIntValue) && DateTime.TryParseExact(strDobAppTwo, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                                                    {
                                                        Logger.WriteLine("13. Step 3 local page values uploading to Sitecore");
                                                        FormItem.Fields["salutation2"].Value = Convert.ToString(ddlSalutationAppTwo.SelectedItem.Text);
                                                        FormItem.Fields["Name Of Applicant2"].Value = strNameAppTwo;
                                                        FormItem.Fields["Nationality2"].Value = Convert.ToString(ddlNationalityAppTwo.SelectedItem.Text);
                                                        FormItem.Fields["Id Type2"].Value = Convert.ToString(ddlIdType.SelectedItem.Text);

                                                        if (ddlIdType.SelectedValue == "PASSPORT")
                                                        {
                                                            FormItem.Fields["Id Number2"].Value = Convert.ToString("");
                                                            FormItem.Fields["PassportNo"].Value = Convert.ToString(txtIdNumAppTwo.Text);
                                                        }
                                                        else
                                                        {
                                                            FormItem.Fields["Id Number2"].Value = Convert.ToString(txtIdNumAppTwo.Text);
                                                            FormItem.Fields["PassportNo"].Value = Convert.ToString("");
                                                        }

                                                        FormItem.Fields["Occupation2"].Value = strOccupationAppTwo;
                                                        FormItem.Fields["Gender2"].Value = Convert.ToString(ddlgenderAppTwo.SelectedItem.Text);
                                                        FormItem.Fields["Date Of Birth2"].Value = strDobAppTwo;
                                                        FormItem.Fields["Address Type2"].Value = Convert.ToString(ddlAddressType.SelectedItem.Text);
                                                        FormItem.Fields["Postal Code2"].Value = "";// Convert.ToString(txtpostalcodeApptwo.Text);
                                                        FormItem.Fields["Address2"].Value = strAddressAppTwo;
                                                        FormItem.Fields["MobileNumber2"].Value = strMobileNumberAppTwo;
                                                        FormItem.Fields["Phione Number2"].Value = strPhoneNumberAppTwo;
                                                        FormItem.Fields["Email Address2"].Value = strEmailAppTwo;
                                                    }
                                                    else
                                                    {
                                                        GetCaptchaimage();
                                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill valid Data.');", true);
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    GetCaptchaimage();
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill valid Data.');", true);
                                                    return;
                                                }
                                            }
                                            else if (Session["Local"].ToString() == "False")
                                            {
                                                if (strNameAppTwoOver != "" && strNricOver != "" && strOccupationAppTwoOver != "" && strDobAppTwoOver != "" && strAddressAppTwoOver != "" && strMobileNumberAppTwoOver != "" && strEmailAppTwoOver != "" && strddlSalutationAppTwoOver != "Select")
                                                {

                                                    DateTime d;
                                                    int EnteredIntValue = 0;

                                                    if (strNameAppTwoOver.Length <= 66 && strNricOver.Length <= 9 && strOccupationAppTwoOver.Length <= 100 && strAddressAppTwoOver.Length <= 120 && strMobileNumberAppTwoOver.Length == 8 && int.TryParse(strMobileNumberAppTwoOver, out EnteredIntValue) && strEmailAppTwoOver.Length <= 320 && DateTime.TryParseExact(strDobAppTwoOver, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                                                    {
                                                        Logger.WriteLine("13. Step 3 overseas page values uploading to Sitecore.");
                                                        FormItem.Fields["salutation2"].Value = Convert.ToString(ddlSalutationAppTwoOver.SelectedItem.Text);
                                                        FormItem.Fields["Name Of Applicant2"].Value = strNameAppTwoOver;
                                                        FormItem.Fields["Nationality2"].Value = Convert.ToString("Singaporean");
                                                        FormItem.Fields["Id Type2"].Value = "NRIC";
                                                        FormItem.Fields["Id Number2"].Value = strNricOver;
                                                        FormItem.Fields["PassportNo"].Value = Convert.ToString("");
                                                        FormItem.Fields["Occupation2"].Value = strOccupationAppTwoOver;
                                                        FormItem.Fields["Gender2"].Value = Convert.ToString(ddlgenderAppTwoOver.SelectedItem.Text);
                                                        FormItem.Fields["Date Of Birth2"].Value = strDobAppTwoOver;
                                                        FormItem.Fields["Address Type2"].Value = Convert.ToString("Local");
                                                        FormItem.Fields["Postal Code2"].Value = "";// Convert.ToString(txtpostalcodeApptwoOver.Text);
                                                        FormItem.Fields["Address2"].Value = strAddressAppTwoOver;
                                                        FormItem.Fields["MobileNumber2"].Value = strMobileNumberAppTwoOver;
                                                        FormItem.Fields["Phione Number2"].Value = strPhoneNumberAppTwoOver;
                                                        FormItem.Fields["Email Address2"].Value = strEmailAppTwoOver;
                                                    }
                                                    else
                                                    {
                                                        GetCaptchaimage();
                                                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill valid Data.');", true);
                                                        return;
                                                    }
                                                }
                                                else
                                                {
                                                    GetCaptchaimage();
                                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill valid Data.');", true);
                                                    return;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Logger.WriteLine("13. Step 3 Session is empty before submit. so redirecting to step1.");
                                            string path = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('There is an error occurred during the submission, Please fill again.'); window.location='" + "/SG50/Apply For Funding.aspx';", true);
                                        }

                                        FormItem.Editing.EndEdit();

                                        for (int i = 0; i < dtBudget.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Project Fund");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newFund = parentItem.Add("fund" + i, template);
                                            try
                                            {
                                                newFund.Editing.BeginEdit();
                                                newFund.Fields["Item"].Value = Convert.ToString(dtBudget.Rows[i][1]);
                                                newFund.Fields["Fund Amount"].Value = Convert.ToString(dtBudget.Rows[i][2]);
                                                newFund.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }

                                        for (int i = 0; i < dtDonations.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Donations");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newDonation = parentItem.Add("Donation" + i, template);
                                            try
                                            {
                                                newDonation.Editing.BeginEdit();
                                                newDonation.Fields["Particulars Of Source"].Value = Convert.ToString(dtDonations.Rows[i][1]);
                                                newDonation.Fields["Funding Amount"].Value = Convert.ToString(dtDonations.Rows[i][2]);
                                                if (Convert.ToString(dtDonations.Rows[i][3]) == "1")
                                                {
                                                    newDonation.Fields["Funding Status"].Value = "Confirmed";
                                                }
                                                else
                                                {
                                                    newDonation.Fields["Funding Status"].Value = "Pending";
                                                }

                                                newDonation.Fields["Funding Duration"].Value = Convert.ToString(dtDonations.Rows[i][4]);
                                                newDonation.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }


                                        for (int i = 0; i < dtTimeLine.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Time Line");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newTimeline = parentItem.Add("TimeLine" + i, template);
                                            try
                                            {
                                                newTimeline.Editing.BeginEdit();
                                                newTimeline.Fields["Date"].Value = Convert.ToString(dtTimeLine.Rows[i][1]);
                                                newTimeline.Fields["Event"].Value = Convert.ToString(dtTimeLine.Rows[i][2]);
                                                newTimeline.Fields["Milestones"].Value = Convert.ToString(dtTimeLine.Rows[i][3]);
                                                newTimeline.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }
                                        for (int i = 0; i < FormItem.GetChildren().Count; i++)
                                        {
                                            if (FormItem.GetChildren()[i].TemplateName.ToString() == "File Template")
                                            {
                                                FormItem.GetChildren()[i].Delete();
                                            }

                                        }
                                        for (int i = 0; i < dtFiles.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/File Template");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newFile = parentItem.Add("File" + i, template);
                                            try
                                            {
                                                newFile.Editing.BeginEdit();

                                                Sitecore.Data.Fields.FileField files = newFile.Fields["Uploaded File Path"];

                                                if (files.Src != null)
                                                {
                                                    Sitecore.Data.Items.MediaItem sample = master.GetItem(dtFiles.Rows[i][0].ToString());
                                                    fileName[i] = sample.Name;
                                                    newFile["Name"] = sample.Name;
                                                    files.Src = sample.MediaPath;
                                                    files.SetAttribute("showineditor", "1");
                                                    files.MediaID = sample.ID;
                                                    //Publishing Media item
                                                    Item Mediaitem = master.GetItem(dtFiles.Rows[i][0].ToString());
                                                    var source = master;
                                                    var target = Sitecore.Data.Database.GetDatabase("web");

                                                    var options = new PublishOptions(source, target, PublishMode.SingleItem, Mediaitem.Language, DateTime.Now)
                                                    {
                                                        RootItem = Mediaitem,
                                                        Deep = true,
                                                    };

                                                    var publisher = new Publisher(options);
                                                    publisher.Publish();
                                                }

                                                newFile.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw ex;
                                    }

                                    Logger.WriteLine("14 Generating XML document.");
                                    generateXml(FormItem);
                                    generateXml_Code(FormItem);
                                    Session["Budjet"] = null;
                                    Session["donations"] = null;
                                    Session["Timeline"] = null;
                                    var item = FormItem;
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        if (item != null)
                                        {

                                            var source = master;
                                            var target = Sitecore.Data.Database.GetDatabase("web");

                                            var options = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now)
                                            {
                                                RootItem = item,
                                                Deep = true,
                                            };

                                            var publisher = new Publisher(options);
                                            publisher.Publish();

                                        }
                                    }
                                    Session["ItemId"] = null;
                                    Session["SEA"] = null;
                                    Session["Organisation"] = null;
                                    Session["NRIC"] = txtNricnum.Text;
                                    GetCaptchaimage();
                                    Logger.WriteLine("15 Redirecting to Thank you page.");
                                    string url = "~/SG50/step4.aspx";
                                    if (!(url.Contains("://")))
                                        Response.Redirect(url);



                                }

                            }
                            else
                            {
                                GetCaptchaimage();
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('There is an error in submission, Please try again to fill the Form at Step 2.');", true);
                            }
                        }
                        else
                        {
                            GetCaptchaimage();
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Captcha Code');", true);

                        }
                    }
                    else
                    {
                        GetCaptchaimage();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Data.');", true);

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }

        private void generateXml(Item FormItem)
        {
            XmlDocument docConfig = new XmlDocument();
            XmlNode xmlNode = docConfig.CreateNode(XmlNodeType.XmlDeclaration, "", "");
            XmlElement rootElement = docConfig.CreateElement("new_fundapplication");
            docConfig.AppendChild(rootElement);

            XmlElement hedder = docConfig.CreateElement("Applicant_Details");
            docConfig.DocumentElement.PrependChild(hedder);
            docConfig.ChildNodes.Item(0).AppendChild(hedder);


            XmlElement TagName = docConfig.CreateElement("Applicant1");
            hedder.AppendChild(TagName);

            XmlElement TagSalutation = docConfig.CreateElement("mccy_salutation");
            XmlText tagSalutationValue = docConfig.CreateTextNode(FormItem["salutation1"]);
            TagSalutation.PrependChild(tagSalutationValue);
            TagName.AppendChild(TagSalutation);

            XmlElement TagNric = docConfig.CreateElement("mccy_nameofapplicantasinnric");
            XmlText TagNricValue = docConfig.CreateTextNode(FormItem["Name Of Applicant1"]);
            TagNric.PrependChild(TagNricValue);
            TagName.AppendChild(TagNric);

            XmlElement TagIdtype = docConfig.CreateElement("mccy_idtype");
            XmlText TagIdtypeValue = docConfig.CreateTextNode("NRIC");
            TagIdtype.PrependChild(TagIdtypeValue);
            TagName.AppendChild(TagIdtype);

            XmlElement TagIdNumber = docConfig.CreateElement("mccy_idnumber");
            XmlText TagIdNumberValue = docConfig.CreateTextNode(FormItem["NRIC Number1"]);
            TagIdNumber.PrependChild(TagIdNumberValue);
            TagName.AppendChild(TagIdNumber);

            XmlElement TagNationality = docConfig.CreateElement("mccy_nationality");
            XmlText TagNationalityValue = docConfig.CreateTextNode("Singaporian");
            TagNationality.PrependChild(TagNationalityValue);
            TagName.AppendChild(TagNationality);

            XmlElement TagOccDesignation = docConfig.CreateElement("mccy_occupationdesignation");
            XmlText TagOccDesignationValue = docConfig.CreateTextNode(FormItem["Occupation1"]);
            TagOccDesignation.PrependChild(TagOccDesignationValue);
            TagName.AppendChild(TagOccDesignation);

            XmlElement TagGender = docConfig.CreateElement("mccy_gender");
            XmlText TagGenderValue = docConfig.CreateTextNode(FormItem["Gender1"]);
            TagGender.PrependChild(TagGenderValue);
            TagName.AppendChild(TagGender);

            XmlElement TagDOB = docConfig.CreateElement("mccy_dateofbirth");
            XmlText TagDOBValue = docConfig.CreateTextNode(FormItem["Date Of Birth1"]);
            TagDOB.PrependChild(TagDOBValue);
            TagName.AppendChild(TagDOB);

            XmlElement TagAddressType1 = docConfig.CreateElement("mccy_addresstype1");
            XmlText TagAddressType1Value = docConfig.CreateTextNode("local");
            TagAddressType1.PrependChild(TagAddressType1Value);
            TagName.AppendChild(TagAddressType1);

            XmlElement TagAddress1 = docConfig.CreateElement("mccy_address1");
            XmlText TagAddress1Value = docConfig.CreateTextNode(FormItem["Address1"]);
            TagAddress1.PrependChild(TagAddress1Value);
            TagName.AppendChild(TagAddress1);

            XmlElement TagPostalCode1 = docConfig.CreateElement("mccy_postalcode1");
            XmlText TagPostalCode1Value = docConfig.CreateTextNode(FormItem["Postal Code1"]);
            TagPostalCode1.PrependChild(TagPostalCode1Value);
            TagName.AppendChild(TagPostalCode1);

            XmlElement TagBuildingName1 = docConfig.CreateElement("mccy_buildingname1");
            XmlText TagBuildingName1Value = docConfig.CreateTextNode("");
            TagBuildingName1.PrependChild(TagBuildingName1Value);
            TagName.AppendChild(TagBuildingName1);

            XmlElement TagBlkno1 = docConfig.CreateElement("mccy_blkhseno1");
            XmlText TagBlkno1Value = docConfig.CreateTextNode("");
            TagBlkno1.PrependChild(TagBlkno1Value);
            TagName.AppendChild(TagBlkno1);

            XmlElement TagStreetName1 = docConfig.CreateElement("mccy_streetname1");
            XmlText TagStreetName1Value = docConfig.CreateTextNode("");
            TagStreetName1.PrependChild(TagStreetName1Value);
            TagName.AppendChild(TagStreetName1);

            XmlElement TagFloorno1 = docConfig.CreateElement("mccy_floorno1");
            XmlText TagFloorno1Value = docConfig.CreateTextNode("");
            TagFloorno1.PrependChild(TagFloorno1Value);
            TagName.AppendChild(TagFloorno1);

            XmlElement TagUnit1 = docConfig.CreateElement("mccy_unitno1");
            XmlText TagUnit1Value = docConfig.CreateTextNode("");
            TagUnit1.PrependChild(TagUnit1Value);
            TagName.AppendChild(TagUnit1);

            XmlElement TagOverAddressline1 = docConfig.CreateElement("mccy_overseaaddressline1");
            XmlText TagOverAddressline1Value = docConfig.CreateTextNode("");
            TagOverAddressline1.PrependChild(TagOverAddressline1Value);
            TagName.AppendChild(TagOverAddressline1);

            XmlElement TagOverAddressline2 = docConfig.CreateElement("mccy_overseaaddressline2");
            XmlText TagOverAddressline2Value = docConfig.CreateTextNode("");
            TagOverAddressline2.PrependChild(TagOverAddressline2Value);
            TagName.AppendChild(TagOverAddressline2);

            XmlElement TagHomePhone = docConfig.CreateElement("mccy_homephone");
            XmlText TagHomePhoneValue = docConfig.CreateTextNode(FormItem["Home Number1"]);
            TagHomePhone.PrependChild(TagHomePhoneValue);
            TagName.AppendChild(TagHomePhone);

            XmlElement TagMobilePhone = docConfig.CreateElement("mccy_mobilephone");
            XmlText TagMobilePhoneValue = docConfig.CreateTextNode(FormItem["MobileNumber1"]);
            TagMobilePhone.PrependChild(TagMobilePhoneValue);
            TagName.AppendChild(TagMobilePhone);

            XmlElement TagEmail = docConfig.CreateElement("mccy_email");
            XmlText TagEmailValue = docConfig.CreateTextNode(FormItem["Email Address1"]);
            TagEmail.PrependChild(TagEmailValue);
            TagName.AppendChild(TagEmail);



            XmlElement TagName2 = docConfig.CreateElement("Applicant2");
            hedder.AppendChild(TagName2);

            XmlElement TagSalutation2 = docConfig.CreateElement("mccy_salutation2");
            XmlText tagSalutation2Value = docConfig.CreateTextNode(FormItem["Salutation2"]);
            TagSalutation2.PrependChild(tagSalutation2Value);
            TagName2.AppendChild(TagSalutation2);

            XmlElement TagNric2 = docConfig.CreateElement("mccy_nameofapplicant2asinnricpassport");
            XmlText TagNric2Value = docConfig.CreateTextNode(FormItem["Name Of Applicant2"]);
            TagNric2.PrependChild(TagNric2Value);
            TagName2.AppendChild(TagNric2);

            XmlElement TagIdtype2 = docConfig.CreateElement("mccy_idtype2");
            XmlText TagIdtype2Value = docConfig.CreateTextNode(FormItem["Id Type2"]);
            TagIdtype2.PrependChild(TagIdtype2Value);
            TagName2.AppendChild(TagIdtype2);

            XmlElement TagIdNumber2 = docConfig.CreateElement("mccy_idnumber2");
            XmlText TagIdNumber2Value = docConfig.CreateTextNode(FormItem["Id Number2"]);
            TagIdNumber2.PrependChild(TagIdNumber2Value);
            TagName2.AppendChild(TagIdNumber2);

            XmlElement TagPassport = docConfig.CreateElement("mccy_passportno");
            XmlText TagPassportValue = docConfig.CreateTextNode(FormItem["PassportNo"]);
            TagPassport.PrependChild(TagPassportValue);
            TagName2.AppendChild(TagPassport);

            XmlElement TagNationality2 = docConfig.CreateElement("mccy_nationality2");
            XmlText TagNationality2Value = docConfig.CreateTextNode(FormItem["Nationality2"]);
            TagNationality2.PrependChild(TagNationality2Value);
            TagName2.AppendChild(TagNationality2);

            XmlElement TagOccDesignation2 = docConfig.CreateElement("mccy_occupationdesignation2");
            XmlText TagOccDesignation2Value = docConfig.CreateTextNode(FormItem["Occupation2"]);
            TagOccDesignation2.PrependChild(TagOccDesignation2Value);
            TagName2.AppendChild(TagOccDesignation2);

            XmlElement TagGender2 = docConfig.CreateElement("mccy_gender2");
            XmlText TagGender2Value = docConfig.CreateTextNode(FormItem["Gender2"]);
            TagGender2.PrependChild(TagGender2Value);
            TagName2.AppendChild(TagGender2);

            XmlElement TagDOB2 = docConfig.CreateElement("mccy_dateofbirth2");
            XmlText TagDOB2Value = docConfig.CreateTextNode(FormItem["Date Of Birth2"]);
            TagDOB2.PrependChild(TagDOB2Value);
            TagName2.AppendChild(TagDOB2);

            XmlElement TagAddressType2 = docConfig.CreateElement("mccy_addresstype2");
            XmlText TagAddressType2Value = docConfig.CreateTextNode(FormItem["Address Type2"]);
            TagAddressType2.PrependChild(TagAddressType2Value);
            TagName2.AppendChild(TagAddressType2);

            XmlElement TagAddress2 = docConfig.CreateElement("mccy_address2");
            XmlText TagAddress2Value = docConfig.CreateTextNode(FormItem["Address2"]);
            TagAddress2.PrependChild(TagAddress2Value);
            TagName2.AppendChild(TagAddress2);

            XmlElement TagPostalCode2 = docConfig.CreateElement("mccy_postalcode2");
            XmlText TagPostalCode2Value = docConfig.CreateTextNode(FormItem["Postal Code2"]);
            TagPostalCode2.PrependChild(TagPostalCode2Value);
            TagName2.AppendChild(TagPostalCode2);

            XmlElement TagBuildingName2 = docConfig.CreateElement("mccy_buildingname2");
            XmlText TagBuildingName2Value = docConfig.CreateTextNode("");
            TagBuildingName2.PrependChild(TagBuildingName2Value);
            TagName2.AppendChild(TagBuildingName2);

            XmlElement TagBlkno2 = docConfig.CreateElement("mccy_blkhseno2");
            XmlText TagBlkno2Value = docConfig.CreateTextNode("");
            TagBlkno2.PrependChild(TagBlkno2Value);
            TagName2.AppendChild(TagBlkno2);

            XmlElement TagStreetName2 = docConfig.CreateElement("mccy_streetname2");
            XmlText TagStreetName2Value = docConfig.CreateTextNode("");
            TagStreetName2.PrependChild(TagStreetName2Value);
            TagName2.AppendChild(TagStreetName2);

            XmlElement TagFloorno2 = docConfig.CreateElement("mccy_floorno2");
            XmlText TagFloorno2Value = docConfig.CreateTextNode("");
            TagFloorno2.PrependChild(TagFloorno2Value);
            TagName2.AppendChild(TagFloorno2);

            XmlElement TagUnit2 = docConfig.CreateElement("mccy_unitno2");
            XmlText TagUnit2Value = docConfig.CreateTextNode("");
            TagUnit2.PrependChild(TagUnit2Value);
            TagName2.AppendChild(TagUnit2);

            XmlElement TagOverAddressline2_1 = docConfig.CreateElement("mccy_overseaaddressline1_2");
            XmlText TagOverAddressline2_1Value = docConfig.CreateTextNode("");
            TagOverAddressline2_1.PrependChild(TagOverAddressline2_1Value);
            TagName2.AppendChild(TagOverAddressline2_1);

            XmlElement TagOverAddressline2_2 = docConfig.CreateElement("mccy_overseaaddressline2_1");
            XmlText TagOverAddressline2_2Value = docConfig.CreateTextNode("");
            TagOverAddressline2_2.PrependChild(TagOverAddressline2_2Value);
            TagName2.AppendChild(TagOverAddressline2_2);

            XmlElement TagHomePhone2 = docConfig.CreateElement("mccy_homephone2");
            XmlText TagHomePhone2Value = docConfig.CreateTextNode(FormItem["Phione Number2"]);
            TagHomePhone2.PrependChild(TagHomePhone2Value);
            TagName2.AppendChild(TagHomePhone2);

            XmlElement TagMobilePhone2 = docConfig.CreateElement("mccy_mobilephone2");
            XmlText TagMobilePhone2Value = docConfig.CreateTextNode(FormItem["MobileNumber2"]);
            TagMobilePhone2.PrependChild(TagMobilePhone2Value);
            TagName2.AppendChild(TagMobilePhone2);

            XmlElement TagEmail2 = docConfig.CreateElement("mccy_email2");
            XmlText TagEmail2Value = docConfig.CreateTextNode(FormItem["Email Address2"]);
            TagEmail2.PrependChild(TagEmail2Value);
            TagName2.AppendChild(TagEmail2);



            XmlElement Organizationhedder = docConfig.CreateElement("Organisation_Details");
            docConfig.DocumentElement.PrependChild(Organizationhedder);
            docConfig.ChildNodes.Item(0).AppendChild(Organizationhedder);



            XmlElement TagOraganization = docConfig.CreateElement("Organisation");
            Organizationhedder.AppendChild(TagOraganization);

            XmlElement TagOraganizationName = docConfig.CreateElement("mccy_nameoforganisation");
            XmlText OraganizationNameValue = docConfig.CreateTextNode(FormItem["Name Of Organisation"]);
            TagOraganizationName.PrependChild(OraganizationNameValue);
            TagOraganization.AppendChild(TagOraganizationName);


            XmlElement Taguen = docConfig.CreateElement("mccy_uen");
            XmlText TaguenValue = docConfig.CreateTextNode(FormItem["UEN"]);
            Taguen.PrependChild(TaguenValue);
            TagOraganization.AppendChild(Taguen);


            XmlElement TagOraganizationType = docConfig.CreateElement("mccy_organisationtype");
            XmlText TagOraganizationTypeValue = docConfig.CreateTextNode(FormItem["Organisation Type"]);
            TagOraganizationType.PrependChild(TagOraganizationTypeValue);
            TagOraganization.AppendChild(TagOraganizationType);

            XmlElement TagNatureOfBussiness = docConfig.CreateElement("mccy_natureofbusiness");
            XmlText TagNatureOfBussinessValue = docConfig.CreateTextNode(FormItem["Nature Of Business"]);
            TagNatureOfBussiness.PrependChild(TagNatureOfBussinessValue);
            TagOraganization.AppendChild(TagNatureOfBussiness);

            XmlElement TagFoundingYear = docConfig.CreateElement("mccy_foundingyr");
            XmlText TagFoundingYearValue = docConfig.CreateTextNode(FormItem["Founding Year"]);
            TagFoundingYear.PrependChild(TagFoundingYearValue);
            TagOraganization.AppendChild(TagFoundingYear);

            XmlElement TagNoOfEmployees = docConfig.CreateElement("mccy_numberofemployees");
            XmlText TagNoOfEmployeesValue = docConfig.CreateTextNode(FormItem["Number Of Employees"]);
            TagNoOfEmployees.PrependChild(TagNoOfEmployeesValue);
            TagOraganization.AppendChild(TagNoOfEmployees);

            XmlElement TagOrgAddressType = docConfig.CreateElement("mccy_addresstype");
            XmlText TagOrgAddressTypeValue = docConfig.CreateTextNode("");
            TagOrgAddressType.PrependChild(TagOrgAddressTypeValue);
            TagOraganization.AppendChild(TagOrgAddressType);

            XmlElement TagOrgAddress = docConfig.CreateElement("mccy_address");
            XmlText TagOrgAddressValue = docConfig.CreateTextNode(FormItem["Organisation Address"]);
            TagOrgAddress.PrependChild(TagOrgAddressValue);
            TagOraganization.AppendChild(TagOrgAddress);

            XmlElement TagOrgPostalCode = docConfig.CreateElement("mccy_postalcode");
            XmlText TagPostalCodeValue = docConfig.CreateTextNode(FormItem["Organisation postalcode"]);
            TagOrgPostalCode.PrependChild(TagPostalCodeValue);
            TagOraganization.AppendChild(TagOrgPostalCode);

            XmlElement TagOrgBulidingName = docConfig.CreateElement("mccy_buildingname");
            XmlText TagOrgBulidingNameValue = docConfig.CreateTextNode("");
            TagOrgBulidingName.PrependChild(TagOrgBulidingNameValue);
            TagOraganization.AppendChild(TagOrgBulidingName);

            XmlElement TagOrgBlkNo = docConfig.CreateElement("mccy_blkhseno");
            XmlText TagOrgBlkNoValue = docConfig.CreateTextNode("");
            TagOrgBlkNo.PrependChild(TagOrgBlkNoValue);
            TagOraganization.AppendChild(TagOrgBlkNo);

            XmlElement TagOrgStreetName = docConfig.CreateElement("mccy_streetname");
            XmlText TagOrgStreetNameValue = docConfig.CreateTextNode("");
            TagOrgStreetName.PrependChild(TagOrgStreetNameValue);
            TagOraganization.AppendChild(TagOrgStreetName);


            XmlElement TagOrgFloorNo = docConfig.CreateElement("mccy_floorno");
            XmlText TagOrgFloorNoValue = docConfig.CreateTextNode("");
            TagOrgFloorNo.PrependChild(TagOrgFloorNoValue);
            TagOraganization.AppendChild(TagOrgFloorNo);

            XmlElement TagOrgUnitNo = docConfig.CreateElement("mccy_unitno");
            XmlText TagOrgUnitNoValue = docConfig.CreateTextNode("");
            TagOrgUnitNo.PrependChild(TagOrgUnitNoValue);
            TagOraganization.AppendChild(TagOrgUnitNo);

            XmlElement TagOrgAddressLine1 = docConfig.CreateElement("mccy_overseaaddressline1_1");
            XmlText TagOrgAddressLine1Value = docConfig.CreateTextNode("");
            TagOrgAddressLine1.PrependChild(TagOrgAddressLine1Value);
            TagOraganization.AppendChild(TagOrgAddressLine1);

            XmlElement TagOrgAddressLine2 = docConfig.CreateElement("mccy_overseaaddressline2_2");
            XmlText TagOrgAddressLine2Value = docConfig.CreateTextNode("");
            TagOrgAddressLine2.PrependChild(TagOrgAddressLine2Value);
            TagOraganization.AppendChild(TagOrgAddressLine2);

            XmlElement TagOrgPhone = docConfig.CreateElement("mccy_phonenumber");
            XmlText TagOrgPhoneValue = docConfig.CreateTextNode(FormItem["Organisation Phone Number"]);
            TagOrgPhone.PrependChild(TagOrgPhoneValue);
            TagOraganization.AppendChild(TagOrgPhone);

            XmlElement TagOrgEmail = docConfig.CreateElement("mccy_emailaddress");
            XmlText TagOrgEmailValue = docConfig.CreateTextNode(FormItem["Organisation Email Address"]);
            TagOrgEmail.PrependChild(TagOrgEmailValue);
            TagOraganization.AppendChild(TagOrgEmail);




            XmlElement ProjectDetails = docConfig.CreateElement("Project_Details");
            docConfig.DocumentElement.PrependChild(ProjectDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectDetails);

            string SportSEA = string.Empty;
            if (Session["SEA"] != null)
                SportSEA = "2";
            else
                SportSEA = "1";

            XmlElement TagProjectDescription = docConfig.CreateElement("Project_Description");
            ProjectDetails.AppendChild(TagProjectDescription);

            XmlElement TagApplicationType = docConfig.CreateElement("mccy_applicationtype");
            XmlText TagApplicationTypeValue = docConfig.CreateTextNode(SportSEA);
            TagApplicationType.PrependChild(TagApplicationTypeValue);
            TagProjectDescription.AppendChild(TagApplicationType);

            XmlElement TagProjectTitle = docConfig.CreateElement("mccy_projecttitle");
            XmlText TagProjectTitleValue = docConfig.CreateTextNode(FormItem["Project Title"]);
            TagProjectTitle.PrependChild(TagProjectTitleValue);
            TagProjectDescription.AppendChild(TagProjectTitle);

            XmlElement TagProjectObjectives = docConfig.CreateElement("mccy_projectobjectives");
            XmlText TagProjectObjectivesValue = docConfig.CreateTextNode(FormItem["Project Objectives"]);
            TagProjectObjectives.PrependChild(TagProjectObjectivesValue);
            TagProjectDescription.AppendChild(TagProjectObjectives);

            XmlElement TagIntendedParticipants = docConfig.CreateElement("mccy_intendedparticipantsbeneficiaries");
            XmlText TagIntendedParticipantsValue = docConfig.CreateTextNode(FormItem["Intended Participants"]);
            TagIntendedParticipants.PrependChild(TagIntendedParticipantsValue);
            TagProjectDescription.AppendChild(TagIntendedParticipants);

            XmlElement TagEstimatedParticipants = docConfig.CreateElement("mccy_estimatedtotalnoofparticipantsbenefit");
            XmlText TagEstimatedParticipantsValue = docConfig.CreateTextNode(FormItem["Esitimated NoOfparticipants"]);
            TagEstimatedParticipants.PrependChild(TagEstimatedParticipantsValue);
            TagProjectDescription.AppendChild(TagEstimatedParticipants);

            string mccy_FormType = string.Empty;
            if (Session["Local"] != null)
            {
                if (Session["Local"].ToString() == "True")
                    mccy_FormType = "LOCAL";
                else
                    mccy_FormType = "OVERSEAS";
            }


            XmlElement TagDeclarationType = docConfig.CreateElement("mccy_FormType");
            XmlText TagDeclarationTypeValue = docConfig.CreateTextNode(mccy_FormType);
            TagDeclarationType.PrependChild(TagDeclarationTypeValue);
            TagProjectDescription.AppendChild(TagDeclarationType);

            XmlElement TagAppliedas = docConfig.CreateElement("mccy_Appliedas");
            XmlText TagAppliedasValue = docConfig.CreateTextNode("Individual");
            TagAppliedas.PrependChild(TagAppliedasValue);
            TagProjectDescription.AppendChild(TagAppliedas);


            XmlElement TagProjectBudget = docConfig.CreateElement("Project_Budget");
            ProjectDetails.AppendChild(TagProjectBudget);

            XmlElement TagAmountRequested = docConfig.CreateElement("mccy_amountrequested");
            XmlText TagAmountRequestedValue = docConfig.CreateTextNode(FormItem["Amount Requested"]);
            TagAmountRequested.PrependChild(TagAmountRequestedValue);
            TagProjectBudget.AppendChild(TagAmountRequested);

            XmlElement TagProjectedExpenditure = docConfig.CreateElement("mccy_totalprojectedexpenditure");
            XmlText TagProjectedExpenditureValue = docConfig.CreateTextNode(FormItem["Projected Expenditure"]);
            TagProjectedExpenditure.PrependChild(TagProjectedExpenditureValue);
            TagProjectBudget.AppendChild(TagProjectedExpenditure);


            XmlElement Taganyfundraisingcomponent = docConfig.CreateElement("mccy_anyfundraisingcomponent");
            XmlText TaganyfundraisingcomponentValue = docConfig.CreateTextNode("");
            Taganyfundraisingcomponent.PrependChild(TaganyfundraisingcomponentValue);
            TagProjectBudget.AppendChild(Taganyfundraisingcomponent);

            XmlElement TagRelevantProjectExperience = docConfig.CreateElement("mccy_relevantprojectexperiencs");
            XmlText TagRelevantProjectExperienceValue = docConfig.CreateTextNode(FormItem["Relevant Project Experience"]);
            TagRelevantProjectExperience.PrependChild(TagRelevantProjectExperienceValue);
            TagProjectBudget.AppendChild(TagRelevantProjectExperience);



            XmlElement TagProjectTimeLine = docConfig.CreateElement("Proposed_Project_Timeline");
            ProjectDetails.AppendChild(TagProjectTimeLine);

            XmlElement TagProjectStartDate = docConfig.CreateElement("mccy_proposedstartdate");
            XmlText TagProjectStartDateValue = docConfig.CreateTextNode(FormItem["Start Date"]);
            TagProjectStartDate.PrependChild(TagProjectStartDateValue);
            TagProjectTimeLine.AppendChild(TagProjectStartDate);

            XmlElement TagProjectEndDate = docConfig.CreateElement("mccy_proposedenddate");
            XmlText TagProjectEndDateValue = docConfig.CreateTextNode(FormItem["End Date"]);
            TagProjectEndDate.PrependChild(TagProjectEndDateValue);
            TagProjectTimeLine.AppendChild(TagProjectEndDate);


            XmlElement TagProjectDeclaration = docConfig.CreateElement("Project_Declaration");
            ProjectDetails.AppendChild(TagProjectDeclaration);

            XmlElement Tagmccydeclarantname = docConfig.CreateElement("mccy_declarantname");
            XmlText TagmccydeclarantnameValue = docConfig.CreateTextNode("");
            Tagmccydeclarantname.PrependChild(TagmccydeclarantnameValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarantname);

            XmlElement Tagmccydeclarationdate = docConfig.CreateElement("mccy_declarationdate");
            XmlText TagmccydeclarationdateValue = docConfig.CreateTextNode("");
            Tagmccydeclarationdate.PrependChild(TagmccydeclarationdateValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarationdate);




            XmlElement ProjectExpenditures = docConfig.CreateElement("Project_Expenditures");
            docConfig.DocumentElement.PrependChild(ProjectExpenditures);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectExpenditures);



            Sitecore.Collections.ChildList ChildBudget = FormItem.GetChildren();
            foreach (Item BudgetItem in ChildBudget)
            {
                if (BudgetItem.TemplateName.ToString() == "Project Fund")
                {
                    XmlElement TagProjectExpenditure = docConfig.CreateElement("Expenditure");
                    ProjectExpenditures.AppendChild(TagProjectExpenditure);

                    XmlElement TagExpenditure = docConfig.CreateElement("mccy_name");
                    XmlText TagExpenditureValue = docConfig.CreateTextNode(BudgetItem["Item"]);
                    TagExpenditure.PrependChild(TagExpenditureValue);
                    TagProjectExpenditure.AppendChild(TagExpenditure);

                    XmlElement TagFund = docConfig.CreateElement("mccy_projectedexpenditure");
                    XmlText TagFundValue = docConfig.CreateTextNode(BudgetItem["Fund Amount"]);
                    TagFund.PrependChild(TagFundValue);
                    TagProjectExpenditure.AppendChild(TagFund);
                }
            }



            XmlElement ProjectFundingSources = docConfig.CreateElement("Project_FundingSources");
            docConfig.DocumentElement.PrependChild(ProjectFundingSources);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectFundingSources);



            Sitecore.Collections.ChildList ChildFund = FormItem.GetChildren();
            foreach (Item DonationItem in ChildFund)
            {
                if (DonationItem.TemplateName.ToString() == "Donations")
                {
                    XmlElement TagFundingSource = docConfig.CreateElement("FundingSource");
                    ProjectFundingSources.AppendChild(TagFundingSource);

                    XmlElement TagSource = docConfig.CreateElement("mccy_name");
                    XmlText TagSourceValue = docConfig.CreateTextNode(DonationItem["Particulars Of Source"]);
                    TagSource.PrependChild(TagSourceValue);
                    TagFundingSource.AppendChild(TagSource);

                    XmlElement TagFundAmount = docConfig.CreateElement("mccy_fundingamount");
                    XmlText TagFundAmountValue = docConfig.CreateTextNode(DonationItem["Funding Amount"]);
                    TagFundAmount.PrependChild(TagFundAmountValue);
                    TagFundingSource.AppendChild(TagFundAmount);

                    XmlElement TagDuration = docConfig.CreateElement("mccy_fundingdurationnumberofmonths");
                    XmlText TagDurationValue = docConfig.CreateTextNode(DonationItem["Funding Duration"]);
                    TagDuration.PrependChild(TagDurationValue);
                    TagFundingSource.AppendChild(TagDuration);

                    XmlElement TagFundStatus = docConfig.CreateElement("mccy_fundingstatus");
                    XmlText TagFundStatusValue = docConfig.CreateTextNode(DonationItem["Funding Status"]);
                    TagFundStatus.PrependChild(TagFundStatusValue);
                    TagFundingSource.AppendChild(TagFundStatus);
                }
            }


            XmlElement ProjectMileStones = docConfig.CreateElement("Project_Milestones");
            docConfig.DocumentElement.PrependChild(ProjectMileStones);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectMileStones);



            Sitecore.Collections.ChildList ChildTimeLine = FormItem.GetChildren();
            foreach (Item TimeLineItem in ChildTimeLine)
            {
                if (TimeLineItem.TemplateName.ToString() == "Time Line")
                {
                    XmlElement TagProjectMileStone = docConfig.CreateElement("Milestone");
                    ProjectMileStones.AppendChild(TagProjectMileStone);

                    XmlElement TagNameMileStone = docConfig.CreateElement("mccy_name");
                    XmlText TagNameMileStoneValue = docConfig.CreateTextNode(TimeLineItem["Milestones"]);
                    TagNameMileStone.PrependChild(TagNameMileStoneValue);
                    TagProjectMileStone.AppendChild(TagNameMileStone);

                    XmlElement TagActivityEvent = docConfig.CreateElement("mccy_activityevent");
                    XmlText TagActivityEventValue = docConfig.CreateTextNode(TimeLineItem["Event"]);
                    TagActivityEvent.PrependChild(TagActivityEventValue);
                    TagProjectMileStone.AppendChild(TagActivityEvent);

                    XmlElement TagMonitoringRequired = docConfig.CreateElement("mccy_monitoringrequired");
                    XmlText TagMonitoringRequiredValue = docConfig.CreateTextNode("");
                    TagMonitoringRequired.PrependChild(TagMonitoringRequiredValue);
                    TagProjectMileStone.AppendChild(TagMonitoringRequired);

                    XmlElement TagEventDate = docConfig.CreateElement("mccy_schedulemilestonedate");
                    XmlText TagEventDateValue = docConfig.CreateTextNode(TimeLineItem["Date"]);
                    TagEventDate.PrependChild(TagEventDateValue);
                    TagProjectMileStone.AppendChild(TagEventDate);

                }
            }

            XmlElement ProjectAttachedDetails = docConfig.CreateElement("Attachment_details");
            docConfig.DocumentElement.PrependChild(ProjectAttachedDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectAttachedDetails);

            Sitecore.Collections.ChildList ChildFiles = FormItem.GetChildren();
            int fileext = 0;
            foreach (Item ChildFile in ChildFiles)
            {
                if (ChildFile.TemplateName.ToString() == "File Template")
                {
                    XmlElement TagAttachment = docConfig.CreateElement("Have_Attachment");
                    XmlText TagAttachmentValue = docConfig.CreateTextNode("Yes");
                    TagAttachment.PrependChild(TagAttachmentValue);
                    ProjectAttachedDetails.AppendChild(TagAttachment);

                    XmlElement TagAttachmentMain = docConfig.CreateElement("Attachment");
                    ProjectAttachedDetails.AppendChild(TagAttachmentMain);

                    XmlElement TagAttachmnetName = docConfig.CreateElement("Name");
                    string fname = Session["ApplicationName"].ToString() + (fileext + 1) + "-" + dtFiles.Rows[fileext][1].ToString();
                    XmlText TagAttachmnetNameValue = docConfig.CreateTextNode(fname);
                    TagAttachmnetName.PrependChild(TagAttachmnetNameValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetName);

                    XmlElement TagAttachmnetPath = docConfig.CreateElement("Location");
                    Sitecore.Data.Fields.FileField file = ChildFile.Fields["Uploaded File Path"];
                    XmlText TagAttachmnetPathValue = docConfig.CreateTextNode(Server.MapPath("~/SG50/Application Forms/archive/" + Session["ApplicationName"].ToString() + "/" + fname)); //file.MediaItem.Paths.Path.ToString());
                    TagAttachmnetPath.PrependChild(TagAttachmnetPathValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetPath);
                    fileext++;
                }
            }

            bool folderExists = Directory.Exists(Server.MapPath("/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString()));
            if (!folderExists)
            {
                Directory.CreateDirectory(Server.MapPath("/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString()));
            }
            if (dtFiles.Rows.Count > 0)
            {
                for (int i = 0; i < dtFiles.Rows.Count; i++)
                {
                    File.Copy(Server.MapPath("/SG50/Applied Forms/" + dtFiles.Rows[i]["TotalFileName"].ToString()), Server.MapPath("/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + (i + 1) + "-" + dtFiles.Rows[i]["TotalFileName"].ToString()));
                    File.Delete(Server.MapPath("/SG50/Applied Forms/" + dtFiles.Rows[i]["TotalFileName"].ToString()));
                }
            }

            docConfig.Save(Server.MapPath("~/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + "_gen" + ".xml"));
        }

        private void generateXml_Code(Item FormItem)
        {
            XmlDocument docConfig = new XmlDocument();
            XmlNode xmlNode = docConfig.CreateNode(XmlNodeType.XmlDeclaration, "", "");
            XmlElement rootElement = docConfig.CreateElement("new_fundapplication");
            docConfig.AppendChild(rootElement);

            XmlElement hedder = docConfig.CreateElement("Applicant_Details");
            docConfig.DocumentElement.PrependChild(hedder);
            docConfig.ChildNodes.Item(0).AppendChild(hedder);


            XmlElement TagName = docConfig.CreateElement("Applicant1");
            hedder.AppendChild(TagName);

            XmlElement TagSalutation = docConfig.CreateElement("mccy_salutation");
            XmlText tagSalutationValue = docConfig.CreateTextNode(ddlSalutationAppone.SelectedValue);
            TagSalutation.PrependChild(tagSalutationValue);
            TagName.AppendChild(TagSalutation);

            XmlElement TagNric = docConfig.CreateElement("mccy_nameofapplicantasinnric");
            XmlText TagNricValue = docConfig.CreateTextNode(FormItem["Name Of Applicant1"]);
            TagNric.PrependChild(TagNricValue);
            TagName.AppendChild(TagNric);

            XmlElement TagIdtype = docConfig.CreateElement("mccy_idtype");
            XmlText TagIdtypeValue = docConfig.CreateTextNode("SP");
            TagIdtype.PrependChild(TagIdtypeValue);
            TagName.AppendChild(TagIdtype);

            XmlElement TagIdNumber = docConfig.CreateElement("mccy_idnumber");
            XmlText TagIdNumberValue = docConfig.CreateTextNode(FormItem["NRIC Number1"]);
            TagIdNumber.PrependChild(TagIdNumberValue);
            TagName.AppendChild(TagIdNumber);

            XmlElement TagNationality = docConfig.CreateElement("mccy_nationality");
            XmlText TagNationalityValue = docConfig.CreateTextNode("SG");
            TagNationality.PrependChild(TagNationalityValue);
            TagName.AppendChild(TagNationality);

            XmlElement TagOccDesignation = docConfig.CreateElement("mccy_occupationdesignation");
            XmlText TagOccDesignationValue = docConfig.CreateTextNode(FormItem["Occupation1"]);
            TagOccDesignation.PrependChild(TagOccDesignationValue);
            TagName.AppendChild(TagOccDesignation);

            XmlElement TagGender = docConfig.CreateElement("mccy_gender");
            XmlText TagGenderValue = docConfig.CreateTextNode(ddlGenderAppOne.SelectedValue);
            TagGender.PrependChild(TagGenderValue);
            TagName.AppendChild(TagGender);

            XmlElement TagDOB = docConfig.CreateElement("mccy_dateofbirth");
            XmlText TagDOBValue = docConfig.CreateTextNode(FormItem["Date Of Birth1"]);
            TagDOB.PrependChild(TagDOBValue);
            TagName.AppendChild(TagDOB);

            XmlElement TagAddressType1 = docConfig.CreateElement("mccy_addresstype1");
            XmlText TagAddressType1Value = docConfig.CreateTextNode("1");
            TagAddressType1.PrependChild(TagAddressType1Value);
            TagName.AppendChild(TagAddressType1);

            XmlElement TagAddress1 = docConfig.CreateElement("mccy_address1");
            XmlText TagAddress1Value = docConfig.CreateTextNode(FormItem["Address1"]);
            TagAddress1.PrependChild(TagAddress1Value);
            TagName.AppendChild(TagAddress1);

            XmlElement TagPostalCode1 = docConfig.CreateElement("mccy_postalcode1");
            XmlText TagPostalCode1Value = docConfig.CreateTextNode(FormItem["Postal Code1"]);
            TagPostalCode1.PrependChild(TagPostalCode1Value);
            TagName.AppendChild(TagPostalCode1);

            XmlElement TagBuildingName1 = docConfig.CreateElement("mccy_buildingname1");
            XmlText TagBuildingName1Value = docConfig.CreateTextNode("");
            TagBuildingName1.PrependChild(TagBuildingName1Value);
            TagName.AppendChild(TagBuildingName1);

            XmlElement TagBlkno1 = docConfig.CreateElement("mccy_blkhseno1");
            XmlText TagBlkno1Value = docConfig.CreateTextNode("");
            TagBlkno1.PrependChild(TagBlkno1Value);
            TagName.AppendChild(TagBlkno1);

            XmlElement TagStreetName1 = docConfig.CreateElement("mccy_streetname1");
            XmlText TagStreetName1Value = docConfig.CreateTextNode("");
            TagStreetName1.PrependChild(TagStreetName1Value);
            TagName.AppendChild(TagStreetName1);

            XmlElement TagFloorno1 = docConfig.CreateElement("mccy_floorno1");
            XmlText TagFloorno1Value = docConfig.CreateTextNode("");
            TagFloorno1.PrependChild(TagFloorno1Value);
            TagName.AppendChild(TagFloorno1);

            XmlElement TagUnit1 = docConfig.CreateElement("mccy_unitno1");
            XmlText TagUnit1Value = docConfig.CreateTextNode("");
            TagUnit1.PrependChild(TagUnit1Value);
            TagName.AppendChild(TagUnit1);

            XmlElement TagOverAddressline1 = docConfig.CreateElement("mccy_overseaaddressline1");
            XmlText TagOverAddressline1Value = docConfig.CreateTextNode("");
            TagOverAddressline1.PrependChild(TagOverAddressline1Value);
            TagName.AppendChild(TagOverAddressline1);

            XmlElement TagOverAddressline2 = docConfig.CreateElement("mccy_overseaaddressline2");
            XmlText TagOverAddressline2Value = docConfig.CreateTextNode("");
            TagOverAddressline2.PrependChild(TagOverAddressline2Value);
            TagName.AppendChild(TagOverAddressline2);

            XmlElement TagHomePhone = docConfig.CreateElement("mccy_homephone");
            XmlText TagHomePhoneValue = docConfig.CreateTextNode(FormItem["Home Number1"]);
            TagHomePhone.PrependChild(TagHomePhoneValue);
            TagName.AppendChild(TagHomePhone);

            XmlElement TagMobilePhone = docConfig.CreateElement("mccy_mobilephone");
            XmlText TagMobilePhoneValue = docConfig.CreateTextNode(FormItem["MobileNumber1"]);
            TagMobilePhone.PrependChild(TagMobilePhoneValue);
            TagName.AppendChild(TagMobilePhone);

            XmlElement TagEmail = docConfig.CreateElement("mccy_email");
            XmlText TagEmailValue = docConfig.CreateTextNode(FormItem["Email Address1"]);
            TagEmail.PrependChild(TagEmailValue);
            TagName.AppendChild(TagEmail);

            string AddressType2Value = string.Empty;
            string salutation2Value = string.Empty;
            string Gender2Value = string.Empty;
            string nationality2Value = string.Empty;
            string IDType = string.Empty;

            if (Session["Local"] != null)
            {
                if (Session["Local"].ToString() == "True")
                {
                    AddressType2Value = ddlAddressType.SelectedValue;
                    nationality2Value = ddlNationalityAppTwo.SelectedValue;
                    Gender2Value = ddlgenderAppTwo.SelectedValue;
                    salutation2Value = ddlSalutationAppTwo.SelectedValue;

                    if (ddlIdType.SelectedValue == "PASSPORT")
                    {
                        IDType = "FT";
                    }
                    else
                    {
                        IDType = "SP";
                    }
                }
                else
                {
                    AddressType2Value = "1";
                    nationality2Value = "SG";
                    Gender2Value = ddlgenderAppTwoOver.SelectedValue;
                    salutation2Value = ddlSalutationAppTwoOver.SelectedValue;
                    IDType = "SP";
                }
            }



            XmlElement TagName2 = docConfig.CreateElement("Applicant2");
            hedder.AppendChild(TagName2);

            XmlElement TagSalutation2 = docConfig.CreateElement("mccy_salutation2");
            XmlText tagSalutation2Value = docConfig.CreateTextNode(salutation2Value);
            TagSalutation2.PrependChild(tagSalutation2Value);
            TagName2.AppendChild(TagSalutation2);

            XmlElement TagNric2 = docConfig.CreateElement("mccy_nameofapplicant2asinnricpassport");
            XmlText TagNric2Value = docConfig.CreateTextNode(FormItem["Name Of Applicant2"]);
            TagNric2.PrependChild(TagNric2Value);
            TagName2.AppendChild(TagNric2);

            XmlElement TagIdtype2 = docConfig.CreateElement("mccy_idtype2");
            XmlText TagIdtype2Value = docConfig.CreateTextNode(IDType);
            TagIdtype2.PrependChild(TagIdtype2Value);
            TagName2.AppendChild(TagIdtype2);

            XmlElement TagIdNumber2 = docConfig.CreateElement("mccy_idnumber2");
            XmlText TagIdNumber2Value = docConfig.CreateTextNode(FormItem["Id Number2"]);
            TagIdNumber2.PrependChild(TagIdNumber2Value);
            TagName2.AppendChild(TagIdNumber2);


            XmlElement TagPassport = docConfig.CreateElement("mccy_passportno");
            XmlText TagPassportValue = docConfig.CreateTextNode(FormItem["PassportNo"]);
            TagPassport.PrependChild(TagPassportValue);
            TagName2.AppendChild(TagPassport);

            XmlElement TagNationality2 = docConfig.CreateElement("mccy_nationality2");
            XmlText TagNationality2Value = docConfig.CreateTextNode(nationality2Value);
            TagNationality2.PrependChild(TagNationality2Value);
            TagName2.AppendChild(TagNationality2);

            XmlElement TagOccDesignation2 = docConfig.CreateElement("mccy_occupationdesignation2");
            XmlText TagOccDesignation2Value = docConfig.CreateTextNode(FormItem["Occupation2"]);
            TagOccDesignation2.PrependChild(TagOccDesignation2Value);
            TagName2.AppendChild(TagOccDesignation2);

            XmlElement TagGender2 = docConfig.CreateElement("mccy_gender2");
            XmlText TagGender2Value = docConfig.CreateTextNode(Gender2Value);
            TagGender2.PrependChild(TagGender2Value);
            TagName2.AppendChild(TagGender2);

            XmlElement TagDOB2 = docConfig.CreateElement("mccy_dateofbirth2");
            XmlText TagDOB2Value = docConfig.CreateTextNode(FormItem["Date Of Birth2"]);
            TagDOB2.PrependChild(TagDOB2Value);
            TagName2.AppendChild(TagDOB2);




            XmlElement TagAddressType2 = docConfig.CreateElement("mccy_addresstype2");
            XmlText TagAddressType2Value = docConfig.CreateTextNode(AddressType2Value);
            TagAddressType2.PrependChild(TagAddressType2Value);
            TagName2.AppendChild(TagAddressType2);

            XmlElement TagAddress2 = docConfig.CreateElement("mccy_address2");
            XmlText TagAddress2Value = docConfig.CreateTextNode(FormItem["Address2"]);
            TagAddress2.PrependChild(TagAddress2Value);
            TagName2.AppendChild(TagAddress2);

            XmlElement TagPostalCode2 = docConfig.CreateElement("mccy_postalcode2");
            XmlText TagPostalCode2Value = docConfig.CreateTextNode(FormItem["Postal Code2"]);
            TagPostalCode2.PrependChild(TagPostalCode2Value);
            TagName2.AppendChild(TagPostalCode2);

            XmlElement TagBuildingName2 = docConfig.CreateElement("mccy_buildingname2");
            XmlText TagBuildingName2Value = docConfig.CreateTextNode("");
            TagBuildingName2.PrependChild(TagBuildingName2Value);
            TagName2.AppendChild(TagBuildingName2);

            XmlElement TagBlkno2 = docConfig.CreateElement("mccy_blkhseno2");
            XmlText TagBlkno2Value = docConfig.CreateTextNode("");
            TagBlkno2.PrependChild(TagBlkno2Value);
            TagName2.AppendChild(TagBlkno2);

            XmlElement TagStreetName2 = docConfig.CreateElement("mccy_streetname2");
            XmlText TagStreetName2Value = docConfig.CreateTextNode("");
            TagStreetName2.PrependChild(TagStreetName2Value);
            TagName2.AppendChild(TagStreetName2);

            XmlElement TagFloorno2 = docConfig.CreateElement("mccy_floorno2");
            XmlText TagFloorno2Value = docConfig.CreateTextNode("");
            TagFloorno2.PrependChild(TagFloorno2Value);
            TagName2.AppendChild(TagFloorno2);

            XmlElement TagUnit2 = docConfig.CreateElement("mccy_unitno2");
            XmlText TagUnit2Value = docConfig.CreateTextNode("");
            TagUnit2.PrependChild(TagUnit2Value);
            TagName2.AppendChild(TagUnit2);

            XmlElement TagOverAddressline2_1 = docConfig.CreateElement("mccy_overseaaddressline1_2");
            XmlText TagOverAddressline2_1Value = docConfig.CreateTextNode("");
            TagOverAddressline2_1.PrependChild(TagOverAddressline2_1Value);
            TagName2.AppendChild(TagOverAddressline2_1);

            XmlElement TagOverAddressline2_2 = docConfig.CreateElement("mccy_overseaaddressline2_1");
            XmlText TagOverAddressline2_2Value = docConfig.CreateTextNode("");
            TagOverAddressline2_2.PrependChild(TagOverAddressline2_2Value);
            TagName2.AppendChild(TagOverAddressline2_2);

            XmlElement TagHomePhone2 = docConfig.CreateElement("mccy_homephone2");
            XmlText TagHomePhone2Value = docConfig.CreateTextNode(FormItem["Phione Number2"]);
            TagHomePhone2.PrependChild(TagHomePhone2Value);
            TagName2.AppendChild(TagHomePhone2);

            XmlElement TagMobilePhone2 = docConfig.CreateElement("mccy_mobilephone2");
            XmlText TagMobilePhone2Value = docConfig.CreateTextNode(FormItem["MobileNumber2"]);
            TagMobilePhone2.PrependChild(TagMobilePhone2Value);
            TagName2.AppendChild(TagMobilePhone2);

            XmlElement TagEmail2 = docConfig.CreateElement("mccy_email2");
            XmlText TagEmail2Value = docConfig.CreateTextNode(FormItem["Email Address2"]);
            TagEmail2.PrependChild(TagEmail2Value);
            TagName2.AppendChild(TagEmail2);



            XmlElement Organizationhedder = docConfig.CreateElement("Organisation_Details");
            docConfig.DocumentElement.PrependChild(Organizationhedder);
            docConfig.ChildNodes.Item(0).AppendChild(Organizationhedder);



            XmlElement TagOraganization = docConfig.CreateElement("Organisation");
            Organizationhedder.AppendChild(TagOraganization);

            XmlElement TagOraganizationName = docConfig.CreateElement("mccy_nameoforganisation");
            XmlText OraganizationNameValue = docConfig.CreateTextNode(FormItem["Name Of Organisation"]);
            TagOraganizationName.PrependChild(OraganizationNameValue);
            TagOraganization.AppendChild(TagOraganizationName);


            XmlElement Taguen = docConfig.CreateElement("mccy_uen");
            XmlText TaguenValue = docConfig.CreateTextNode(FormItem["UEN"]);
            Taguen.PrependChild(TaguenValue);
            TagOraganization.AppendChild(Taguen);


            XmlElement TagOraganizationType = docConfig.CreateElement("mccy_organisationtype");
            XmlText TagOraganizationTypeValue = docConfig.CreateTextNode(FormItem["Organisation Type"]);
            TagOraganizationType.PrependChild(TagOraganizationTypeValue);
            TagOraganization.AppendChild(TagOraganizationType);

            XmlElement TagNatureOfBussiness = docConfig.CreateElement("mccy_natureofbusiness");
            XmlText TagNatureOfBussinessValue = docConfig.CreateTextNode(FormItem["Nature Of Business"]);
            TagNatureOfBussiness.PrependChild(TagNatureOfBussinessValue);
            TagOraganization.AppendChild(TagNatureOfBussiness);

            XmlElement TagFoundingYear = docConfig.CreateElement("mccy_foundingyr");
            XmlText TagFoundingYearValue = docConfig.CreateTextNode(FormItem["Founding Year"]);
            TagFoundingYear.PrependChild(TagFoundingYearValue);
            TagOraganization.AppendChild(TagFoundingYear);

            XmlElement TagNoOfEmployees = docConfig.CreateElement("mccy_numberofemployees");
            XmlText TagNoOfEmployeesValue = docConfig.CreateTextNode(FormItem["Number Of Employees"]);
            TagNoOfEmployees.PrependChild(TagNoOfEmployeesValue);
            TagOraganization.AppendChild(TagNoOfEmployees);

            XmlElement TagOrgAddressType = docConfig.CreateElement("mccy_addresstype");
            XmlText TagOrgAddressTypeValue = docConfig.CreateTextNode("");
            TagOrgAddressType.PrependChild(TagOrgAddressTypeValue);
            TagOraganization.AppendChild(TagOrgAddressType);

            XmlElement TagOrgAddress = docConfig.CreateElement("mccy_address");
            XmlText TagOrgAddressValue = docConfig.CreateTextNode(FormItem["Organisation Address"]);
            TagOrgAddress.PrependChild(TagOrgAddressValue);
            TagOraganization.AppendChild(TagOrgAddress);

            XmlElement TagOrgPostalCode = docConfig.CreateElement("mccy_postalcode");
            XmlText TagPostalCodeValue = docConfig.CreateTextNode(FormItem["Organisation postalcode"]);
            TagOrgPostalCode.PrependChild(TagPostalCodeValue);
            TagOraganization.AppendChild(TagOrgPostalCode);

            XmlElement TagOrgBulidingName = docConfig.CreateElement("mccy_buildingname");
            XmlText TagOrgBulidingNameValue = docConfig.CreateTextNode("");
            TagOrgBulidingName.PrependChild(TagOrgBulidingNameValue);
            TagOraganization.AppendChild(TagOrgBulidingName);

            XmlElement TagOrgBlkNo = docConfig.CreateElement("mccy_blkhseno");
            XmlText TagOrgBlkNoValue = docConfig.CreateTextNode("");
            TagOrgBlkNo.PrependChild(TagOrgBlkNoValue);
            TagOraganization.AppendChild(TagOrgBlkNo);

            XmlElement TagOrgStreetName = docConfig.CreateElement("mccy_streetname");
            XmlText TagOrgStreetNameValue = docConfig.CreateTextNode("");
            TagOrgStreetName.PrependChild(TagOrgStreetNameValue);
            TagOraganization.AppendChild(TagOrgStreetName);


            XmlElement TagOrgFloorNo = docConfig.CreateElement("mccy_floorno");
            XmlText TagOrgFloorNoValue = docConfig.CreateTextNode("");
            TagOrgFloorNo.PrependChild(TagOrgFloorNoValue);
            TagOraganization.AppendChild(TagOrgFloorNo);

            XmlElement TagOrgUnitNo = docConfig.CreateElement("mccy_unitno");
            XmlText TagOrgUnitNoValue = docConfig.CreateTextNode("");
            TagOrgUnitNo.PrependChild(TagOrgUnitNoValue);
            TagOraganization.AppendChild(TagOrgUnitNo);

            XmlElement TagOrgAddressLine1 = docConfig.CreateElement("mccy_overseaaddressline1_1");
            XmlText TagOrgAddressLine1Value = docConfig.CreateTextNode("");
            TagOrgAddressLine1.PrependChild(TagOrgAddressLine1Value);
            TagOraganization.AppendChild(TagOrgAddressLine1);

            XmlElement TagOrgAddressLine2 = docConfig.CreateElement("mccy_overseaaddressline2_2");
            XmlText TagOrgAddressLine2Value = docConfig.CreateTextNode("");
            TagOrgAddressLine2.PrependChild(TagOrgAddressLine2Value);
            TagOraganization.AppendChild(TagOrgAddressLine2);

            XmlElement TagOrgPhone = docConfig.CreateElement("mccy_phonenumber");
            XmlText TagOrgPhoneValue = docConfig.CreateTextNode(FormItem["Organisation Phone Number"]);
            TagOrgPhone.PrependChild(TagOrgPhoneValue);
            TagOraganization.AppendChild(TagOrgPhone);

            XmlElement TagOrgEmail = docConfig.CreateElement("mccy_emailaddress");
            XmlText TagOrgEmailValue = docConfig.CreateTextNode(FormItem["Organisation Email Address"]);
            TagOrgEmail.PrependChild(TagOrgEmailValue);
            TagOraganization.AppendChild(TagOrgEmail);




            XmlElement ProjectDetails = docConfig.CreateElement("Project_Details");
            docConfig.DocumentElement.PrependChild(ProjectDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectDetails);

            string SportSEA = string.Empty;
            if (Session["SEA"] != null)
                SportSEA = "2";
            else
                SportSEA = "1";

            XmlElement TagProjectDescription = docConfig.CreateElement("Project_Description");
            ProjectDetails.AppendChild(TagProjectDescription);

            XmlElement TagApplicationType = docConfig.CreateElement("mccy_applicationtype");
            XmlText TagApplicationTypeValue = docConfig.CreateTextNode(SportSEA);
            TagApplicationType.PrependChild(TagApplicationTypeValue);
            TagProjectDescription.AppendChild(TagApplicationType);

            XmlElement TagProjectTitle = docConfig.CreateElement("mccy_projecttitle");
            XmlText TagProjectTitleValue = docConfig.CreateTextNode(FormItem["Project Title"]);
            TagProjectTitle.PrependChild(TagProjectTitleValue);
            TagProjectDescription.AppendChild(TagProjectTitle);

            XmlElement TagProjectObjectives = docConfig.CreateElement("mccy_projectobjectives");
            XmlText TagProjectObjectivesValue = docConfig.CreateTextNode(FormItem["Project Objectives"]);
            TagProjectObjectives.PrependChild(TagProjectObjectivesValue);
            TagProjectDescription.AppendChild(TagProjectObjectives);

            XmlElement TagIntendedParticipants = docConfig.CreateElement("mccy_intendedparticipantsbeneficiaries");
            XmlText TagIntendedParticipantsValue = docConfig.CreateTextNode(FormItem["Intended Participants"]);
            TagIntendedParticipants.PrependChild(TagIntendedParticipantsValue);
            TagProjectDescription.AppendChild(TagIntendedParticipants);

            XmlElement TagEstimatedParticipants = docConfig.CreateElement("mccy_estimatedtotalnoofparticipantsbenefit");
            XmlText TagEstimatedParticipantsValue = docConfig.CreateTextNode(FormItem["Esitimated NoOfparticipants"]);
            TagEstimatedParticipants.PrependChild(TagEstimatedParticipantsValue);
            TagProjectDescription.AppendChild(TagEstimatedParticipants);


            XmlElement TagProjectBudget = docConfig.CreateElement("Project_Budget");
            ProjectDetails.AppendChild(TagProjectBudget);

            XmlElement TagAmountRequested = docConfig.CreateElement("mccy_amountrequested");
            XmlText TagAmountRequestedValue = docConfig.CreateTextNode(FormItem["Amount Requested"]);
            TagAmountRequested.PrependChild(TagAmountRequestedValue);
            TagProjectBudget.AppendChild(TagAmountRequested);

            XmlElement TagProjectedExpenditure = docConfig.CreateElement("mccy_totalprojectedexpenditure");
            XmlText TagProjectedExpenditureValue = docConfig.CreateTextNode(FormItem["Projected Expenditure"]);
            TagProjectedExpenditure.PrependChild(TagProjectedExpenditureValue);
            TagProjectBudget.AppendChild(TagProjectedExpenditure);


            XmlElement Taganyfundraisingcomponent = docConfig.CreateElement("mccy_anyfundraisingcomponent");
            XmlText TaganyfundraisingcomponentValue = docConfig.CreateTextNode("");
            Taganyfundraisingcomponent.PrependChild(TaganyfundraisingcomponentValue);
            TagProjectBudget.AppendChild(Taganyfundraisingcomponent);

            XmlElement TagRelevantProjectExperience = docConfig.CreateElement("mccy_relevantprojectexperiencs");
            XmlText TagRelevantProjectExperienceValue = docConfig.CreateTextNode(FormItem["Relevant Project Experience"]);
            TagRelevantProjectExperience.PrependChild(TagRelevantProjectExperienceValue);
            TagProjectBudget.AppendChild(TagRelevantProjectExperience);



            XmlElement TagProjectTimeLine = docConfig.CreateElement("Proposed_Project_Timeline");
            ProjectDetails.AppendChild(TagProjectTimeLine);

            XmlElement TagProjectStartDate = docConfig.CreateElement("mccy_proposedstartdate");
            XmlText TagProjectStartDateValue = docConfig.CreateTextNode(FormItem["Start Date"]);
            TagProjectStartDate.PrependChild(TagProjectStartDateValue);
            TagProjectTimeLine.AppendChild(TagProjectStartDate);

            XmlElement TagProjectEndDate = docConfig.CreateElement("mccy_proposedenddate");
            XmlText TagProjectEndDateValue = docConfig.CreateTextNode(FormItem["End Date"]);
            TagProjectEndDate.PrependChild(TagProjectEndDateValue);
            TagProjectTimeLine.AppendChild(TagProjectEndDate);

            XmlElement TagProjectDeclaration = docConfig.CreateElement("Project_Declaration");
            ProjectDetails.AppendChild(TagProjectDeclaration);

            XmlElement Tagmccydeclarantname = docConfig.CreateElement("mccy_declarantname");
            XmlText TagmccydeclarantnameValue = docConfig.CreateTextNode("");
            Tagmccydeclarantname.PrependChild(TagmccydeclarantnameValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarantname);

            XmlElement Tagmccydeclarationdate = docConfig.CreateElement("mccy_declarationdate");
            XmlText TagmccydeclarationdateValue = docConfig.CreateTextNode("");
            Tagmccydeclarationdate.PrependChild(TagmccydeclarationdateValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarationdate);

            XmlElement ProjectExpenditures = docConfig.CreateElement("Project_Expenditures");
            docConfig.DocumentElement.PrependChild(ProjectExpenditures);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectExpenditures);



            Sitecore.Collections.ChildList ChildBudget = FormItem.GetChildren();
            foreach (Item BudgetItem in ChildBudget)
            {
                if (BudgetItem.TemplateName.ToString() == "Project Fund")
                {
                    XmlElement TagProjectExpenditure = docConfig.CreateElement("Expenditure");
                    ProjectExpenditures.AppendChild(TagProjectExpenditure);

                    XmlElement TagExpenditure = docConfig.CreateElement("mccy_name");
                    XmlText TagExpenditureValue = docConfig.CreateTextNode(BudgetItem["Item"]);
                    TagExpenditure.PrependChild(TagExpenditureValue);
                    TagProjectExpenditure.AppendChild(TagExpenditure);

                    XmlElement TagFund = docConfig.CreateElement("mccy_projectedexpenditure");
                    XmlText TagFundValue = docConfig.CreateTextNode(BudgetItem["Fund Amount"]);
                    TagFund.PrependChild(TagFundValue);
                    TagProjectExpenditure.AppendChild(TagFund);
                }
            }



            XmlElement ProjectFundingSources = docConfig.CreateElement("Project_FundingSources");
            docConfig.DocumentElement.PrependChild(ProjectFundingSources);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectFundingSources);



            Sitecore.Collections.ChildList ChildFund = FormItem.GetChildren();
            foreach (Item DonationItem in ChildFund)
            {
                if (DonationItem.TemplateName.ToString() == "Donations")
                {
                    XmlElement TagFundingSource = docConfig.CreateElement("FundingSource");
                    ProjectFundingSources.AppendChild(TagFundingSource);

                    XmlElement TagSource = docConfig.CreateElement("mccy_name");
                    XmlText TagSourceValue = docConfig.CreateTextNode(DonationItem["Particulars Of Source"]);
                    TagSource.PrependChild(TagSourceValue);
                    TagFundingSource.AppendChild(TagSource);

                    XmlElement TagFundAmount = docConfig.CreateElement("mccy_fundingamount");
                    XmlText TagFundAmountValue = docConfig.CreateTextNode(DonationItem["Funding Amount"]);
                    TagFundAmount.PrependChild(TagFundAmountValue);
                    TagFundingSource.AppendChild(TagFundAmount);

                    XmlElement TagDuration = docConfig.CreateElement("mccy_fundingdurationnumberofmonths");
                    XmlText TagDurationValue = docConfig.CreateTextNode(DonationItem["Funding Duration"]);
                    TagDuration.PrependChild(TagDurationValue);
                    TagFundingSource.AppendChild(TagDuration);
                    string Funding_Status = string.Empty;
                    if (DonationItem["Funding Status"] == "Confirmed")
                        Funding_Status = "2";
                    else
                        Funding_Status = "1";

                    XmlElement TagFundStatus = docConfig.CreateElement("mccy_fundingstatus");
                    XmlText TagFundStatusValue = docConfig.CreateTextNode(Funding_Status);
                    TagFundStatus.PrependChild(TagFundStatusValue);
                    TagFundingSource.AppendChild(TagFundStatus);
                }
            }


            XmlElement ProjectMileStones = docConfig.CreateElement("Project_Milestones");
            docConfig.DocumentElement.PrependChild(ProjectMileStones);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectMileStones);



            Sitecore.Collections.ChildList ChildTimeLine = FormItem.GetChildren();
            foreach (Item TimeLineItem in ChildTimeLine)
            {
                if (TimeLineItem.TemplateName.ToString() == "Time Line")
                {
                    XmlElement TagProjectMileStone = docConfig.CreateElement("Milestone");
                    ProjectMileStones.AppendChild(TagProjectMileStone);

                    XmlElement TagNameMileStone = docConfig.CreateElement("mccy_name");
                    XmlText TagNameMileStoneValue = docConfig.CreateTextNode(TimeLineItem["Milestones"]);
                    TagNameMileStone.PrependChild(TagNameMileStoneValue);
                    TagProjectMileStone.AppendChild(TagNameMileStone);

                    XmlElement TagActivityEvent = docConfig.CreateElement("mccy_activityevent");
                    XmlText TagActivityEventValue = docConfig.CreateTextNode(TimeLineItem["Event"]);
                    TagActivityEvent.PrependChild(TagActivityEventValue);
                    TagProjectMileStone.AppendChild(TagActivityEvent);

                    XmlElement TagMonitoringRequired = docConfig.CreateElement("mccy_monitoringrequired");
                    XmlText TagMonitoringRequiredValue = docConfig.CreateTextNode("");
                    TagMonitoringRequired.PrependChild(TagMonitoringRequiredValue);
                    TagProjectMileStone.AppendChild(TagMonitoringRequired);

                    XmlElement TagEventDate = docConfig.CreateElement("mccy_schedulemilestonedate");
                    XmlText TagEventDateValue = docConfig.CreateTextNode(TimeLineItem["Date"]);
                    TagEventDate.PrependChild(TagEventDateValue);
                    TagProjectMileStone.AppendChild(TagEventDate);

                }
            }

            XmlElement ProjectAttachedDetails = docConfig.CreateElement("Attachment_details");
            docConfig.DocumentElement.PrependChild(ProjectAttachedDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectAttachedDetails);

            Sitecore.Collections.ChildList ChildFiles = FormItem.GetChildren();
            int fileext = 0;
            foreach (Item ChildFile in ChildFiles)
            {
                if (ChildFile.TemplateName.ToString() == "File Template")
                {

                    XmlElement TagAttachment = docConfig.CreateElement("Have_Attachment");
                    XmlText TagAttachmentValue = docConfig.CreateTextNode("Yes");
                    TagAttachment.PrependChild(TagAttachmentValue);
                    ProjectAttachedDetails.AppendChild(TagAttachment);

                    XmlElement TagAttachmentMain = docConfig.CreateElement("Attachment");
                    ProjectAttachedDetails.AppendChild(TagAttachmentMain);

                    XmlElement TagAttachmnetName = docConfig.CreateElement("Name");
                    string fname = Session["ApplicationName"].ToString() + (fileext + 1) + "-" + dtFiles.Rows[fileext][1].ToString();
                    XmlText TagAttachmnetNameValue = docConfig.CreateTextNode(fname);
                    TagAttachmnetName.PrependChild(TagAttachmnetNameValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetName);

                    XmlElement TagAttachmnetPath = docConfig.CreateElement("Location");
                    Sitecore.Data.Fields.FileField file = ChildFile.Fields["Uploaded File Path"];
                    XmlText TagAttachmnetPathValue = docConfig.CreateTextNode(Server.MapPath("~/SG50/Application Forms/archive/" + Session["ApplicationName"].ToString() + "/" + fname)); //file.MediaItem.Paths.Path.ToString());
                    TagAttachmnetPath.PrependChild(TagAttachmnetPathValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetPath);
                    fileext++;
                }
            }



            docConfig.Save(Server.MapPath("~/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + ".xml"));
        }


        protected void btnStepBack_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string url = "~/SG50/Step2.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string url = "~/SG50/Apply For Funding.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string url = "~/SG50/Step2.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }

        private void GetCaptchaimage()
        {

            cgen = new CaptchaGenerator();
            string strpath = Server.MapPath("/SG50/images/PG/") + "CaptchaImage.gif";
            cgen.ResetCaptchaColor();

            txt_CaptchaVal.Text = cgen.GenerateCaptcha(strpath);


            string strpath2 = "/SG50/layouts/PG/sublayouts/" + "CaptchaImage.ashx?id=" + strpath;
            Image1.ImageUrl = strpath2;



        }
        protected void Button1_Click1(object sender, ImageClickEventArgs e)
        {

            cgen = new CaptchaGenerator();
            GetCaptchaimage();

        }
    }
}