﻿using System;
using Sitecore.Data.Items;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Links;
using Sitecore.Diagnostics;

namespace Layouts.Projects_sublayout {
  
	/// <summary>
	/// Summary description for Projects_sublayoutSublayout
	/// </summary>
  public partial class Projects_sublayoutSublayout : System.Web.UI.UserControl
  {
      Item itmContext = Sitecore.Context.Item;
    private void Page_Load(object sender, EventArgs e) {
      // Put user code to initialize the page here
    }

    protected string getShareAnIdeaLink()
    {
        string link = "";
        if (SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID) != null)
        {
            link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID));
        }
        return link;
    }
  }
}