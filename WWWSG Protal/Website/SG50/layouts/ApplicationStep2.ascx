﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Applicationstep2.Applicationstep2Sublayout" CodeFile="~/SG50/layouts/ApplicationStep2.ascx.cs" %>
<link href="/SG50/include/css/style.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/dropkick.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/responsive.css" rel="stylesheet" type="text/css" />
<style>
    .validation_summary_as_bulletlist ul {
        display: none;
    }
</style>
<script type="text/javascript" src="/SG50/include/js/jquery.min.js"></script>
<script src="/SG50/include/js/1.8/jquery-ui.min.js" type="text/javascript"></script>

<script src="/SG50/include/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="/SG50/include/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/SG50/include/js/mktSignup.js"></script>

<link href="/SG50/include/css/jquery-ui.css" rel="stylesheet" />


<script type="text/javascript">
    $(function () {

        $(".datepickerCompleted").datepicker({

            dateFormat: 'dd/mm/yy',
            minDate: 0,

            maxDate: new Date(2015, 11, 31)
        });

    });
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57 || keyCode == 9) || specialKeys.indexOf(keyCode) != -1);
        // document.getElementById("error").style.display = ret ? "none" : "inline";
        return ret;
    }

    function IsDate(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = (keyCode >= 127 && keyCode <= 0);
        return ret;
    }
</script>
<link rel="stylesheet" type="text/css" media="screen" href="/SG50/include/css/stylesheet.css" />
<section class="main-content">
    <!---main-content start-------->
    <div class="form-banner">
        <img src="/Sg50/images/Images/batch-4/banner.jpg" />
    </div>
    <section class="form-container">

        <div class="head-title">
            <h1>APPLICATION FOR SG50 CELEBRATION FUND</h1>
        </div>
        <div class="grid-container">
            <div class="formprocess-nav">
                <ul>
                    <li>
                        <div class="process-serial">1</div>
                        <div class="process-title">

                            <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="#989898" OnClick="LinkButton1_Click">
                            <h2>STEP 1</h2>
                            <p>Application overview</p>
                            </asp:LinkButton>

                        </div>
                    </li>
                    <li class="active">
                        <div class="process-serial">2</div>
                        <div class="process-title">
                            <h2>STEP 2</h2>
                            <p>Tell us about your project</p>
                        </div>
                    </li>
                    <li>
                        <div class="process-serial">3</div>
                        <div class="process-title">
                            <%if (Session["Step3"] != null)
                              { %>
                            <asp:LinkButton ID="LinkButton2" runat="server" ValidationGroup="step2" ForeColor="#989898" OnClick="LinkButton2_Click">
                            <h2>STEP 3</h2>
                            <p>Tell us more about you</p>
                            </asp:LinkButton>
                            <%}
                              else
                              { %>
                            <h2>STEP 3</h2>
                            <p>Tell us more about you</p>
                            <%} %>
                        </div>
                    </li>
                </ul>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="step2" CssClass="validation_summary_as_bulletlist" runat="server" HeaderText='Please fill in required fields in the correct format and try again.' ForeColor="Red" ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" />
            <div class="form-content">
                <!------form content start------------>
                <h1 class="big-font">SG50 Celebration Fund - Online Application Form</h1>
                <p><span class="mandatory">*</span>Mandatory fields.</p>
                <sc:FieldRenderer ID="txtStep2Description" runat="server" FieldName="Step2 Description" />
                <!----- heading starts--------------------->
                <div class="heading-box">
                    <p>About The Project</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>
                <!----- heading ends--------------------->

                <h1>Project description</h1>
                <div class="form-float">
                    <div class="form-left">
                        <label><span class="mandatory">*</span>Project title</label><br />
                        <asp:TextBox ID="txtProjTitle" CssClass="ValidationRequired" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ValidationGroup="step2" ControlToValidate="txtProjTitle" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <label><span class="mandatory">*</span>In less than 2000 characters, briefly describe your project idea, objectives and outcomes that you would like to achieve, and how do you think your project contributes to SG50 objectives?</label>
                <asp:TextBox ID="txtObjectives" CssClass="ValidationRequired" runat="server" TextMode="MultiLine">
                              
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="regobjective" ForeColor="Red" ValidationGroup="step2" ValidationExpression="^[\s\S]{0,2000}$" ErrorMessage="You cannot enter more than 2000 characters." ControlToValidate="txtObjectives" runat="server" Display="Dynamic">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="reqObjectives" runat="server" ValidationGroup="step2" ControlToValidate="txtObjectives" Text="" ForeColor="Red"></asp:RequiredFieldValidator>


                <h1>Target audience</h1>
                <div class="form-float">
                    <div class="form-left">
                        <label><span class="mandatory">*</span>Who are the intended participants / beneficiaries? </label>
                        <br />
                        <asp:TextBox ID="txtintendedparticipants" CssClass="ValidationRequired" MaxLength="100" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqintendedparticipants" ValidationGroup="step2" runat="server" ControlToValidate="txtintendedparticipants" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Estimated total number of participants / beneficiaries</label><br />
                        <asp:TextBox ID="txtEstparticipants" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" MaxLength="6" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqEstparticipants" runat="server" ValidationGroup="step2" ControlToValidate="txtEstparticipants" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                    </div>

                </div>
                <!----- heading starts--------------------->
                <div class="heading-box">
                    <p>Budget</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>
                <!----- heading ends--------------------->
                <h1>Project budget</h1>
                <div class="form-float">
                    <div class="form-left">
                        <label style="margin: -1px 0px 0px;"><span class="mandatory">*</span>Amount requested for under the SG50 Celebration Fund: $ </label>

                        <asp:Label ID="lblCap" runat="server" Text="Maximum cap of $50,000"></asp:Label>

                        <br />
                        <asp:TextBox ID="txtAmountRequested" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" MaxLength="5" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqAmountRequested" ValidationGroup="step2" runat="server" ControlToValidate="txtAmountRequested" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regAmountRequested" runat="server" ErrorMessage="Please Enter Only Digits." ValidationExpression="\d{0,5}" ControlToValidate="txtAmountRequested" ForeColor="Red" ValidationGroup="step2"></asp:RegularExpressionValidator>

                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Total projected expenditure: $</label><br />
                        <asp:TextBox ID="txtTotalExpenditure" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" MaxLength="9" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqTotalExpenditure" runat="server" ValidationGroup="step2" ControlToValidate="txtTotalExpenditure" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regTotalExpenditure" runat="server" ErrorMessage="Please Enter Only Digits." ValidationExpression="\d{0,9}" ControlToValidate="txtTotalExpenditure" ForeColor="Red" ValidationGroup="step2"></asp:RegularExpressionValidator>

                    </div>

                </div>
                <sc:FieldRenderer ID="txtProjectExpendiure" runat="server" FieldName="Project Expenditure" />
                <div class="form-float">
                    <div class="form-left">
                        <label><span class="mandatory">*</span>Item </label>
                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Projected expenditure ($)</label>
                    </div>
                    <br />
                    <br />
                    <asp:Repeater ID="GridView1" runat="server" OnItemCommand="GridView1_ItemCommand" OnItemDataBound="GridView1_ItemDataBound">

                        <ItemTemplate>
                            <div class="form-left">

                                <asp:HiddenField ID="rowno" runat="server" Value='<%#Eval("RowNumber")%>' />
                                <asp:TextBox ID="txtItem" CssClass="ValidationRequired" Value='<%#Eval("Item")%>' MaxLength="100" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredItem" runat="server" ControlToValidate="txtItem" ValidationGroup="step2" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                            </div>
                            <div class="form-right">

                                <asp:TextBox ID="txtProjected" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" MaxLength="8" ondrop="return false;" onpaste="return false;" Value='<%#Eval("Projected")%>' AutoPostBack="true" OnTextChanged="txtProjected_TextChanged" runat="server"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" CssClass="cancel" ImageUrl="~/SG50/images/Images/batch-4/add.png" CommandName="AddNewColumn" runat="server" />
                                <asp:ImageButton ID="ImageButton2" CssClass="cancel" ImageUrl="~/SG50/images/Images/batch-4/minus.png" CommandArgument='<%#Eval("RowNumber")%>' CommandName="RemoveColumn" runat="server" />
                                <asp:RequiredFieldValidator ID="reqProject" ValidationGroup="step2" runat="server" ControlToValidate="txtProjected" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="step2" runat="server" ErrorMessage="Please Enter Only Digits." ValidationExpression="^.\d{0,8}$" ControlToValidate="txtProjected" ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="form-float">
                    <div class="form-left" style="visibility: hidden">
                        <label><span class="mandatory">*</span></label><br />
                        <input type="text" />
                    </div>


                    <div class="form-right">
                        <label><span class="mandatory"></span>Total: $</label><br />
                        <asp:TextBox ReadOnly="true" ID="txtCount1" Text="0" runat="server"></asp:TextBox>

                    </div>
                </div>
                <h1>Please list down other sources of funding that you intend to apply to support this project</h1>
                <p>These may include sponsorships, grants, donations from foundations / organisations, etc.</p>
                <div class="form-float">
                    <div class="form-left">
                        <div class="form-left-left">
                            <label><span class="mandatory"></span>Particulars of source</label>
                        </div>
                        <div class="form-left-right">
                            <label><span class="mandatory"></span>Funding amount</label>
                        </div>
                    </div>
                    <div class="form-right">
                        <div class="form-right-left">
                            <label><span class="mandatory"></span>Funding status</label>
                        </div>
                        <div class="form-right-right">
                            <label><span class="mandatory"></span>Funding duration (number of months) </label>
                        </div>
                    </div>



                    <asp:Repeater ID="GridView2" runat="server" OnItemCommand="GridView2_ItemCommand" OnItemDataBound="GridView2_ItemDataBound">
                        <ItemTemplate>
                            <div class="form-float">
                                <div class="form-left">
                                    <div class="form-left-left">

                                        <asp:HiddenField ID="rowno" runat="server" Value='<%#Eval("RowNumber")%>' />
                                        <asp:TextBox ID="txtsource" MaxLength="100" Value='<%#Eval("source")%>' runat="server"></asp:TextBox>

                                    </div>
                                    <div class="form-left-right">

                                        <asp:TextBox ID="txtamount" onkeypress="return IsNumeric(event);" Value='<%#Eval("amount")%>' runat="server" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regAmount" runat="server" ValidationGroup="step2" ErrorMessage="Please Enter Only Digits." ValidationExpression="\d{0,10}" ControlToValidate="txtamount" ForeColor="Red"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="form-right">
                                    <div class="form-right-left">
                                        <asp:HiddenField ID="hiddenrowno" runat="server" Value='<%#Eval("Status")%>' />
                                        <asp:DropDownList ID="ddlStatus" runat="server">
                                            <asp:ListItem Value="0">Pending</asp:ListItem>
                                            <asp:ListItem Value="1">Confirmed</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-right-right">
                                        <asp:TextBox ID="txtduration" onkeypress="return IsNumeric(event);" Value='<%#Eval("duration")%>' runat="server" MaxLength="2"></asp:TextBox>
                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtduration" MinimumValue="1" MaximumValue="60" ValidationGroup="step2"  ErrorMessage="Please enter duration value 1 to 60" ForeColor="Red"></asp:RangeValidator>
                                        <asp:RegularExpressionValidator ID="regDuration" runat="server" ValidationGroup="step2" ErrorMessage="Please Enter Only Digits." ValidationExpression="\d{0,2}" ControlToValidate="txtduration" ForeColor="Red"></asp:RegularExpressionValidator>
                                        <br />
                                        <asp:ImageButton ID="ImageButton1" CssClass="cancel" ImageUrl="~/SG50/images/Images/batch-4/add.png" CommandName="AddNewColumn" runat="server" />
                                        <asp:ImageButton ID="ImageButton2" CssClass="cancel" ImageUrl="~/SG50/images/Images/batch-4/minus.png" CommandArgument='<%#Eval("RowNumber")%>' CommandName="RemoveColumn" runat="server" />
                                    </div>
                                </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
                <!----- heading starts--------------------->
                <div class="heading-box">
                    <p>Timeline</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>
                <!----- heading ends--------------------->
                <h1>Proposed project timeline</h1>
                <div class="form-float">
                    <div class="form-left">
                        <div class="form-left-left">
                            <label><span class="mandatory">*</span>Start date </label>
                            <br />

                            <asp:TextBox ID="txtStartDate" onkeypress="return IsDate(event);" CssClass="datepickerCompleted calender ValidationRequired" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqStartDate" runat="server" ValidationGroup="step2" ControlToValidate="txtStartDate" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-left-right">
                            <label style="width: 325px"><span class="mandatory">*</span>End date (To be completed by 31 Dec 2015) </label>
                            <br />
                            <asp:TextBox ID="txtEndDate" onkeypress="return IsDate(event);" CssClass="datepickerCompleted calender ValidationRequired" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqEndDate" runat="server" Display="Dynamic" ValidationGroup="step2" ControlToValidate="txtEndDate" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpVal1" Style="font-size: 13px" ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" ValidationGroup="step2" Type="Date" CultureInvariantValues="true" Operator="GreaterThanEqual" ErrorMessage="End date cannot be earlier than start date" ForeColor="Red" runat="server"></asp:CompareValidator>
                        </div>
                    </div>
                    <div class="form-right" style="visibility: hidden">
                        <div class="form-right-left">
                            <label><span class="mandatory">*</span>items </label>
                            <br />
                            <select>
                                <option>1</option>
                                <option>1</option>
                                <option>1</option>
                                <option>1</option>

                            </select>
                        </div>
                        <div class="form-right-right">
                            <label><span class="mandatory">*</span>items </label>
                            <br />
                            <input type="text" /><a href="#"><img src="/SG50/images/Images/batch-4/add.png" /></a>
                        </div>
                    </div>
                </div>
                <label>Schedule / Milestones of activities (From commencement to completion of project)</label>
                <div class="form-float">
                    <div class="form-left">
                        <div class="form-left-left">
                            <label><span class="mandatory">*</span>Date</label>
                        </div>
                        <div class="form-left-right">
                            <label><span class="mandatory">*</span>Activity (e.g Talk to Publisher)</label>
                        </div>
                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Deliverables (e.g Conceptualisation of book layout)</label>
                    </div>

                    <asp:Repeater ID="GridView3" runat="server" OnItemCommand="GridView3_ItemCommand" OnItemDataBound="GridView3_ItemDataBound">
                        <ItemTemplate>
                            <div class="form-left">
                                <div class="form-left-left">

                                    <asp:HiddenField ID="rowno" runat="server" Value='<%#Eval("RowNumber")%>' />
                                    <asp:TextBox ID="txtDate" onkeypress="return IsDate(event);" CssClass="datepickerCompleted calender ValidationRequired" Value='<%#Eval("Date")%>' runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqEndDate11" runat="server" Display="Dynamic" ValidationGroup="step2" ControlToValidate="txtDate" Text="" ForeColor="Red"></asp:RequiredFieldValidator>


                                </div>
                                <div class="form-left-right">

                                    <asp:TextBox ID="txtEvent" Value='<%#Eval("Event")%>' CssClass="ValidationRequired" MaxLength="100" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ValidationGroup="step2" ControlToValidate="txtEvent" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-right">

                                <asp:TextBox ID="txtMilestone" Value='<%#Eval("Milestone")%>' CssClass="ValidationRequired" MaxLength="100" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ValidationGroup="step2" ControlToValidate="txtMilestone" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:ImageButton ID="ImageButton1" CssClass="cancel" ImageUrl="~/SG50/images/Images/batch-4/add.png" CommandName="AddNewColumn" runat="server" />
                                <asp:ImageButton ID="ImageButton2" CssClass="cancel" ImageUrl="~/SG50/images/Images/batch-4/minus.png" CommandArgument='<%#Eval("RowNumber")%>' CommandName="RemoveColumn" runat="server" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
                <h1><span class="mandatory">*</span>What relevant experience does your team have in carrying out this project?</h1>
                <asp:TextBox ID="txtRelevantExp" runat="server" CssClass="ValidationRequired" TextMode="MultiLine"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regExp" ForeColor="Red" ValidationGroup="step2" ValidationExpression="^[\s\S]{0,2000}$" ErrorMessage="You cannot enter more than 2000 characters." ControlToValidate="txtRelevantExp" runat="server" Display="Dynamic">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="reqRelevantExp" runat="server" ValidationGroup="step2" ControlToValidate="txtRelevantExp" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                <!----- heading starts--------------------->
                <div class="heading-box">
                    <p>Others</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>


                <link href="/SG50/include/css/jquery-filestyle.css" rel="stylesheet" />

                <!----- heading ends--------------------->
                <h1>Upload proposal or supporting documents or information of parties involved (if any)</h1>
                <label>Format acceptable:DOC, XLS, PPT, PDF; File size limit: 5MB &nbsp;<asp:Label ID="lblfup1" ForeColor="Red" runat="server" Style="float: none; margin: 0 0 0 20px;"></asp:Label></label><br />
                <asp:FileUpload ID="fupFileone" runat="server" CssClass="jfilestyle" data-buttonText="Browse" /><br />
                <asp:FileUpload ID="fupFiletwo" runat="server" CssClass="jfilestyle" data-buttonText="Browse" /><br />
                <asp:FileUpload ID="fupFileThree" runat="server" CssClass="jfilestyle" data-buttonText="Browse" /><br />
                <asp:FileUpload ID="fupFileFour" runat="server" CssClass="jfilestyle" data-buttonText="Browse" /><br />
                <asp:FileUpload ID="fupFileFive" runat="server" CssClass="jfilestyle" data-buttonText="Browse" />

                <div class="addfile">
                </div>
                <table>
                    <tr>
                        <td>

                            <div class="next-step">
                                <asp:Button ID="btnStepBack" CssClass="cancel" runat="server" Text="BACK" OnClick="btnStepBack_Click" />
                            </div>
                        </td>
                        <td style="width: 10px"></td>
                        <td>

                            <div class="next-step">
                                <asp:Button ID="btnNextStep" ValidationGroup="step2" runat="server" Text="NEXT STEP" OnClick="btnNextStep_Click" />
                            </div>

                        </td>
                    </tr>
                </table>



            </div>
            <!------form content Ends------------>
    </section>





</section>
<script src="/SG50/include/js/jquery-filestyle.min.js"></script>
<script>
    $(document).ready(function () {
        $(":file").jfilestyle({ buttonText: "Browse", icon: false });

    });



</script>

<!---main-content end-------->
