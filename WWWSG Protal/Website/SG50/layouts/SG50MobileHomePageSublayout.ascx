﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="Layouts.Sg50mobilehomepagesublayout.Sg50mobilehomepagesublayoutSublayout" CodeFile="~/SG50/layouts/SG50MobileHomePageSublayout.ascx.cs" %>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<link href="/SG50/include/css/movingboxes.css" rel="stylesheet" />

<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/jquery.cookie.js"></script>
<script type="text/javascript" async src="/SG50/include/js/widgets.js"></script>
<script src="/SG50/include/js/news-feeds.js"></script>
<div id="homePage" class="container rew">
    <div class="fix-arws">
        <a href="#" class="display">
            <img src="/SG50/images/Home Page/uparrow.jpg" alt="prev" />prev</a>
        <div style="height: 3px"></div>
        <a href="#" class="display">
            <img src="/SG50/images/Home Page/downarrow.jpg" alt="next" />next</a>
    </div>
    <!--Curosal Banner-->
    <script type="text/javascript">

        function functionToExecute(name) {

            $('#' + name).addClass("video");
        }

        function linktype(name) {
            $('#' + name).attr("target", "_blank");
        }
    </script>

    <!--Start OF Banner-->
    <div id="DvHomePageBanners" runat="server" class="section current">
    </div>
    <!--End OF Banner-->

    <!--Start OF Featured news pan-->
    <div class="clear featured-pan pad section">
        <div class="featured-in">
            <h3>
                <span>featured news</span></h3>
            <asp:Repeater ID="repFeatureNews" runat="server">
                <ItemTemplate>
                    <a href='<%#Eval("href")%>' target="_blank">
                        <div class="ft-news">
                            <div class="img-pan">
                                <img src="<%# DataBinder.Eval(Container.DataItem, "NewsImage") %>" />
                            </div>
                            <p>
                                <em>
                                    <%#Eval("Date")%></em><%#Eval("NewsDescription") %>
                            </p>
                        </div>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
            <div class="clear">
            </div>
        </div>
    </div>
    <!--End OF news feed pan-->

        <!--Start OF news feed pan-->
    <div class="clear news-feed pad section" style="display:none">
        <div class="featured-in">
            <h3><span>newsfeed</span></h3>
            <div class="overlay">
                <div class="loading">
                    <img src="/SG50/images/Home Page/loading.gif" alt="" /></div>
                <div class="newsfeed">
                    <div class="news-left">
                        <div class="youtube">
                            <iframe width="100%" src="https://www.youtube.com/embed/AXWcZ_HbHFE" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="news-logo-pan">
                            <div class="news-logo-left">
                                <img src="/SG50/images/Home Page/newsfeed-big-logo.png" alt="" /></div>
                            <div class="news-logo-right">
                                Follow us for the latest updates and conversations.
                <ul>
                    <li><a href="#">
                        <img src="/SG50/images/Home Page/fbicon.jpg" alt="" /></a></li>
                    <li><a href="#">
                        <img src="/SG50/images/Home Page/news-twitter.png" alt="" /></a></li>
                    <li><a href="#">
                        <img src="/SG50/images/Home Page/news-youtube.png" alt="" /></a></li>
                    <li><a href="#">
                        <img src="/SG50/images/Home Page/news-insta.png" alt="" /></a></li>
                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-right">
                        <div class="insta">
                            <ul>
                                <li><span>
                                    <img src="/SG50/images/Home Page/news-insta.png" alt="" /></span><img src="/SG50/images/Home Page/instagram1.jpg" alt="" /></li>
                                <li><span>
                                    <img src="/SG50/images/Home Page/news-insta.png" alt="" /></span><img src="/SG50/images/Home Page/instagram2.jpg" alt="" /></li>
                                <li><span>
                                    <img src="/SG50/images/Home Page/news-insta.png" alt="" /></span><img src="/SG50/images/Home Page/instagram3.jpg" alt="" /></li>
                            </ul>
                        </div>
                        <div class="feed-pan">
                            <div class="feeds tweet">
                                <div class="feeds-logo">
                                    <img src="/SG50/images/Home Page/news-twitter.png" alt="" /></div>
                                <div class="feeds-top">
                                    <img src="/SG50/images/Home Page/newsfeed-big-logo.png" alt="" />
                                    <p>Singapore50<span>@twitterapi</span></p>
                                </div>
                                <div class="feeds-mid">
                                    <p>Donec eu lectus eros. Sed vel dui a nunc viverra aliquet. Cras libero mi,Sed vel dui a nunc viverra aliquet. @Singapore50...</p>
                                </div>
                                <div class="feeds-bottom">
                                    <p>
                                        <img src="/SG50/images/Home Page/tweet-some.png" alt="" />by Someone</p>
                                    <ul>
                                        <li><a href="#">
                                            <img src="/SG50/images/Home Page/tweet-back.png" alt="" /></a></li>
                                        <li><a href="#">
                                            <img src="/SG50/images/Home Page/tweet-some.png" alt="" /></a></li>
                                        <li><a href="#">
                                            <img src="/SG50/images/Home Page/tweet-rating.png" alt="" /></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="feeds">
                                <div class="feeds-logo">
                                    <img src="/SG50/images/Home Page/tweet-fb.png" alt="" /></div>
                                <div class="feeds-top">
                                    <img src="/SG50/images/Home Page/newsfeed-big-logo.png" alt="" />
                                    <p>Singapore50<span>Nov 05</span></p>
                                </div>
                                <div class="feeds-mid">
                                    <p>Donec eu lectus eros. Sed vel dui a nunc viverra aliquet. @Singapore50...</p>
                                </div>
                                <div class="fb-img clear">
                                    <img src="/SG50/images/Home Page/fb.jpg" alt="" /></div>
                                <div class="feeds-bottom">
                                    <p>
                                        <img src="/SG50/images/Home Page/like-pan.jpg" alt="" /></p>
                                    <ul>
                                        <li><a href="#">
                                            <img src="/SG50/images/Home Page/ft-flag.jpg" alt="" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <!--End OF news feed pan-->


    <!--Start OF events calender-->
    <div class="clear featured-pan pad section">
        <div class="featured-in">
            <h3>
                <span>events calendar</span></h3>

            <ul id="slider">
                <asp:Repeater ID="repEvents" runat="server">
                    <ItemTemplate>
                        <li>
                            <div class="event-pan">
                                <div class="event-img">
                                    <img src="<%# DataBinder.Eval(Container.DataItem, "NewsImage") %>" />
                                </div>
                                <a href='<%#Eval("href")%>'>
                                    <div class="event-in">
                                        <span><%#Eval("itemName") %></span> <em>
                                            <%#Eval("Date")%></em>
                                        <p>
                                            <%#Eval("NewsDescription") %>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
    <!--End OF events calender-->

        <!--Start OF news feed pan-->
  <div class="clear news-feed pad section ds-play" style="display:none">
    <div class="featured-in">
      <h3><span>social feeds</span></h3>
      <div class="overlay">
        <div class="loadingsocial"><img src="/SG50/images/Home Page/loading.gif" alt="" /></div>
        <div class="socail-feed">
          <ul>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/1.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/2.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/3.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/4.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/5.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/6.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/7.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/8.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/9.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/10.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/11.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/12.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/13.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/14.jpg" /></a></li>
            <li><a href="#"><img src="/SG50/images/Home Page/social-feed/15.jpg" /></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
     <!--End OF news feed pan-->


</div>

<script src="/SG50/include/js/jquery.bxslider.min.js"></script>
<script src="/SG50/include/js/jquery.movingboxes.min.js"></script>
<script src="/SG50/include/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
    function ImageChange() {


        if ($(window).width() >= 1024) {
            $('#slider').movingBoxes({
                startPanel: 1,      // start with this panel
                wrap: false,  // if true, the panel will infinitely loop
                buildNav: true,   // if true, navigation links will be added
                navFormatter: function () { return "&gt;"; } // function which returns the navigation text for each panel
            });
        }

    }
    $(document).ready(function () {
        ImageChange();
        $(window).resize(function () {
            ImageChange();

        });
    });

</script>

<script type="text/javascript">
    function ShowMenu() {
        $(".menu").toggle()
    }
    $(function () {
        $('p:empty').remove();
        $('h1:empty').remove();
        $(window).scroll(function () {
            var e = $(this).scrollTop();
            $(".section ").each(function () {
                if (e > $(this).offset().top - 100) {
                    $(".section").removeClass("current");
                    $(this).addClass("current");
                    return
                }
            })
        });
        $("div.section").first();
        $("a.display").on("click", function (e) {
            e.preventDefault();
            var t = $(this).text(),
                n = $(this);

            if (t.trim() == "next" && $(".current").next("div.section").length > 0) {

                var r = $(".current").next(".section");
                var i = r.offset().top;
                $(".current").removeClass("current");
                $("body,html").animate({
                    scrollTop: i
                }, function () {
                    r.addClass("current")
                })
            } else if (t.trim() == "prev" && $(".current").prev("div.section").length > 0) {
                var s = $(".current").prev(".section");
                var i = s.offset().top;
                $(".current").removeClass("current");
                $("body,html").animate({
                    scrollTop: i
                }, function () {
                    s.addClass("current")
                })
            }
        })
    });
    if ($(window).width() <= 767) {
        $(".bxslider").bxSlider({
            pause: 5e3,
            touchEnabled: false,
            autoStart: false,
            auto: false,
            oneToOneTouch: false
        })
    } else {
        $(".bxslider").bxSlider({
            pause: 5e3,
            auto: true,
            oneToOneTouch: false,
            touchEnabled: false
        })
    } </script>
 
 <script>
$(function(){
var wp = $('.newsfeed').width();
var wh = $('.newsfeed').height();
$('.loading').width(wp).height(wh)

var sw = $('.socail-feed').width();
var hs = $('.socail-feed').height();
$('.loadingsocial').width(sw).height(hs)

function HideEvents(divId) {
    if ($(divId).length > 0) {
        if ($(divId).find('li').length > 0) {
            $(divId).show();
        } else {
            $(divId).hide();
        }
    } else { return false; }
}

HideEvents('div.clear.featured-pan.pad.section:eq(1)');

})
</script>

