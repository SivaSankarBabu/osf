﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Compaigncommunitysublayout.CompaigncommunitysublayoutSublayout"
    CodeFile="~/SG50/layouts/CompaignCommunitySublayout.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />

<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript"
    language="javascript"></script>

<script src="/SG50/include/js/jquery.min_Home.js"></script>

<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>

<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">

<script src="/SG50/include/js/jquery.bxslider.min.js"></script>

<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<link href="/SG50/include/css/thematic-campaign-responsive.css" rel="stylesheet"
    type="text/css">
<div class="container custom-res" id="homePage">
    <div class="masthead hide-for-small">
        <div class="banner-01 story_details border-Bnone" id="DvCampaignTopStory" runat="server">
        </div>
        <ul class="banner-03 custom custom_space_nav" id="DvCampaignBanners" runat="server">
        </ul>
    </div>
    <div class="clanding custom-slider" id="DivMobile" runat="server">
    </div>
    <div class="thepioneer padding-none">
        <sc:Image ID="imgPioneer" Field="Pioneer Spirit Image" runat="server" CssClass="hide-for-small" />
        <sc:Image ID="Image1" Field="PioneerSpiritMobileImage" runat="server" CssClass="show-for-small" />
    </div>
    <div class="gallery" id="divUserStories" runat="server">
        <%--<a href="#" class="loadmore">Load More</a>--%>
    </div>
    <div id="divLoading" class="Lazy-load" style="text-align: center; display: none">
        <div id="marker-end">
            <img src="/SG50/images/PG/bx_loader.gif">
        </div>
    </div>
</div>
<%--<asp:HiddenField ID="hdnRandom" runat="server" />--%>

<script type="text/javascript">
    var skipCount = 12;
    var Counter = 12;

    $(function () {

        $('.menu_nav').find('.ABOUT a:first').addClass('selected');
        function ChangeOnFocus(ctrl, findCtrl, classNames) {
            $(ctrl).mouseenter(function () {
                $(this).find(findCtrl).addClass(classNames);
            }).mouseleave(function () {
                $(this).find(findCtrl).removeClass(classNames);
            })
        }
        if ($(window).width() >= 1024) {
            ChangeOnFocus('.gallery ul li', '.gtooltip', 'active');
        }

        function BindUserStoriesInScroll() {
            $(window).scroll(function () {
                ///alert(navigator.userAgent);
                if (navigator.userAgent.match(/iPhone|iPod/)) {
                    if ($(document).height() - ($(window).height() + $(window).scrollTop()) <= $('.footer').height()) {
                        setTimeout(function () { }, 1000);
                        if (skipCount == $('.gallery ul li').length) {
                            LoadMoreStories(skipCount, Counter);
                            skipCount = skipCount + Counter;
                        }
                    }
                } else if (navigator.userAgent.match(/Android/)) {
                    if ($(document).height() - ($(window).height() + $(window).scrollTop()) <= $('.footer').height()) {
                        setTimeout(function () { }, 1000);
                        if (skipCount == $('.gallery ul li').length) {
                            LoadMoreStories(skipCount, Counter);
                            skipCount = skipCount + Counter;
                        }
                    }
                } else {
                    if ($(document).height() - ($(window).height() + $(window).scrollTop()) <= $('.footer').height()) {
                        setTimeout(function () { }, 1000);
                        if (skipCount == $('.gallery ul li').length) {
                            LoadMoreStories(skipCount, Counter);
                            skipCount = skipCount + Counter;
                        }
                    }
                }
            });
        }

        //        function BindUserStoriesInScroll() {
        //            $(window).scroll(function() {
        //                 if ($(document).height() == ($(window).height() + $(window).scrollTop())) {
        //                //if ($(window).scrollTop() + $(window).height() + $(".footer-section").height() + $(".Lazy-load").height() >= $(document).height() && $('#pg_lazyload_disable').val() == 1) {
        //                    setTimeout(function() { }, 1000);
        //                    LoadMoreStories(skipCount, 4);
        //                    skipCount = skipCount + 4;
        //                }
        //            });
        //        }

        BindUserStoriesInScroll();


        function LoadMoreStories(skipCount, nextLoadNoofItems) {
            if (skipCount != 0 && skipCount != '' && skipCount != undefined && skipCount != null && nextLoadNoofItems != '' && nextLoadNoofItems != undefined && nextLoadNoofItems != null) {
                GetStoires(skipCount, nextLoadNoofItems);
            } else return false;
        }


        function GetStoires(skipCount, nextLoadNoofItems) {
            try {
                $.ajax({
                    type: 'post',
                    url: '/SG50/ajax/ajax_content.aspx/LoadMoreStories', data: "{'skipCount':'" + skipCount + "','nextCount':'" + nextLoadNoofItems + "'}", contentType: 'application/json; charset=utf-8', dataType: 'json', success: function (data) {
                        if (data.d != null && data.d != '') {
                            // alert(data.d);
                            $('#divLoading').show(); setTimeout(function () {
                                $('#content_0_divUserStories ul').append(data.d); ChangeOnFocus('.gallery ul li ', '.gtooltip', 'active'); $('#divLoading').hide();
                            }, 1000);
                            setTimeout(function () { }, 1000);

                            //skipCount = skipCount + parseInt(nextLoadNoofItems); alert(skipCount);

                        }
                    }, failure: function () { alert('F'); }, error: function (err) { }
                })
            }
            catch (msg) {
            }

        }


    })

</script>

<script>
    // var str = "https://youtu.be/c8fdZ7ANzmA";
    // var pieces = str.split(/[\s/]+/);
    //  alert(pieces[pieces.length - 1]);

    var str = "<%=Session["Banner Video URL"] %>";

    //alert(str);
    var key = str.split(/[\s/]+/);
    key = key[key.length - 1];
    // alert(key);
    //  var APIKey = 'AIzaSyBi7B2buWEpUaWgto-puq5L2nbbEjeqFwg';

    var APIKey = 'AIzaSyB4VFYXvYG4_hzoupeI0ZYueTQvjbcr3QQ';
    $(function () {
        $.getJSON('https://www.googleapis.com/youtube/v3/videos?part=contentDetails,statistics&id=' + key + '&key=' + APIKey + '&alt=json', function (data) {
            var numViews = +data.items[0].statistics.viewCount;
            $('.views').text(numViews + ' views');

            var duration = data.items[0].contentDetails.duration;
            duration = duration.replace(/PT/gi, "");
            duration = duration.replace(/H/gi, ":");
            if (duration.indexOf('M') != '-1') {
                duration = duration.replace(/M/gi, ":");
            } else {
                duration = "0:" + duration;
            }
            duration = duration.replace(/S/gi, "");
            $('.time').text(duration);

        });
    });

    $(function () {
        if ($(window).width() >= 1024) {

            $('.gallery ul li').mouseenter(function () {
                $(this).find('.gtooltip').addClass('active');
            })

            $('.gallery ul li').mouseleave(function () {
                $(this).find('.gtooltip').removeClass('active');
            })

            HideMobile('.thepioneer');
        }

        function HideMobile(ctrl) {
            //  alert($(ctrl).length);
            $(ctrl).find('img:eq(1)').hide();
        }
    })

    $(function () {
        if ($(window).width() <= 1024) {
            $(".gallery ul li img").each(function () {
                $(this).attr("src", $(this).attr("src").replace("/images/", "images/mobile/"));
            });


        }


    });
    $(function () {
        if ($(window).width() <= 1023) {
            $(".thepioneer img").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });
            HideDesktop('.thepioneer');
        }

        function HideDesktop(ctrl) {
            //  alert($(ctrl).length);
            $(ctrl).find('img:eq(0)').hide();
        }

    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".bxslider").bxSlider({
            pause: 5e3,
            touchEnabled: false,
            autoStart: false,
            auto: false,
            oneToOneTouch: false
        });
    });
</script>

<script type="text/javascript">    var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>

