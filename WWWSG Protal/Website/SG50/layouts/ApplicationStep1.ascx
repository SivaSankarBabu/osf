﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Applicationstep1.Applicationstep1Sublayout" CodeFile="~/SG50/layouts/ApplicationStep1.ascx.cs" %>
<link href="/SG50/include/css/style.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/dropkick.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/responsive.css" rel="stylesheet" type="text/css" />
<section class="main-content">
    <!---main-content start-------->
    <div class="form-banner">
        <img src="/Sg50/images/Images/batch-4/banner.jpg" />
    </div>
    <section class="form-container">

        <div class="head-title">
            <h1>APPLICATION FOR SG50 CELEBRATION FUND</h1>
        </div>
        <div class="grid-container">
            <div class="formprocess-nav">
                <ul>
                    <li class="active">
                        <div class="process-serial">1</div>
                        <div class="process-title">
                            <h2>STEP 1</h2>
                            <p>Application overview</p>
                        </div>
                    </li>
                    <li>
                        <div class="process-serial">2</div>
                        <div class="process-title">
                            <%if (Session["Step2"] != null)
                              { %>
                            <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="#989898" OnClick="LinkButton1_Click">
                            <h2>STEP 2</h2>
                            <p>Tell us about your project</p>
                            </asp:LinkButton>
                            <%}
                              else
                              { %>
                            <h2>STEP 2</h2>
                            <p>Tell us about your project</p>
                            <%} %>
                        </div>
                    </li>
                    <li>
                        <div class="process-serial">3</div>
                        <div class="process-title">
                            <%if (Session["Step3"] != null)
                              { %>
                            <asp:LinkButton ID="LinkButton2" runat="server" ForeColor="#989898" OnClick="LinkButton2_Click">
                            <h2>STEP 3</h2>
                            <p>Tell us more about you</p>
                            </asp:LinkButton>
                            <%}
                              else
                              { %>
                            <h2>STEP 3</h2>
                            <p>Tell us more about you</p>
                            <%} %>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="form-content">
                <!------form content start------------>
                <h1>
                    <sc:Text ID="txtTitle" Field="Title" runat="server" />
                </h1>
                <sc:FieldRenderer ID="txtDescription" runat="server" FieldName="Description" />
                <div class="heading-box">
                    <p>SO YOU’VE GOT A CELEBRATION PROJECT?</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>
                <h1>SG50 is all about marking our journey, our people, our values. It’s your chance to be part of Singapore’s biggest celebration yet.</h1>
                <div class="check-box" id="DivSports" runat="server" visible="false">
                    <asp:CheckBox ID="chkSports" runat="server" />
                    <p>
                        <sc:Text ID="txtChecboxText" Field="Applying Sports Related Project Text" runat="server" />
                    </p>

                </div>
                <div class="heading-box">
                    <p>Please select what’s applicable to continue</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>

                <div class="form-option-box">
                    <div class="form-options">
                        <img src="/SG50/images/Images/batch-4/profile.png" />
                        <sc:FieldRenderer ID="txtGroupApplication" runat="server" FieldName="Group Application" />
                        <br />
                        <br />
                        <div class="apply-btn">
                            <asp:Button ID="btnLocalProj" runat="server" Text="APPLY FOR LOCAL PROJECT" OnClick="btnLocalProj_Click" />
                        </div>
                        <div class="apply-btn">
                            <p>OR</p>
                        </div>
                        <div class="apply=btn">
                            <a href="/Step2.aspx"></a>
                            <asp:Button ID="btnOverseasProj" runat="server" Text="APPLY FOR OVERSEAS PROJECT" OnClick="btnOverseasProj_Click" />
                        </div>
                    </div>
                    <div class="form-options">
                        <img src="/SG50/images/Images/batch-4/oraganization.png" />
                        <sc:FieldRenderer ID="txtOrganizationApplication" runat="server" FieldName="Organization Application" />
                        <br />
                        <br />

                        <div class="apply=btn">
                            <asp:Button ID="btnOrganisation" runat="server" Text="APPLY FOR ORGANISATION PROJECT" OnClick="btnOrganisation_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <!------form content Ends------------>
        </div>
    </section>
</section>
<!---main-content end-------->
