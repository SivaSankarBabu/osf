<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Footer_sublayout.Footer_sublayoutSublayout" CodeFile="~/SG50/layouts/Footer Sublayout.ascx.cs" %>
<%--<html>
<head>--%>

    <script language="javascript" type="text/javascript" src="/SG50/include/js/modalpopupforfaq.js"></script>

    <script type="text/javascript">
        Prevention();
    </script>

<%--</head>
<body>--%>
    <div class="footer_nav">
        <a href="#" onclick="ShowModalPopup('dvHouseRulesPopup'); return false; ">
            <span>HOUSE RULES</span> </a><span id="divider">|</span>
        <a href="#" onclick="ShowModalPopup('dvPopup'); return false; "><span>FREQUENTLY ASKED
            QUESTIONS (FAQ)</span> </a><span id="divider">|</span>  <a href="<%=getContactUsLink() %>"><span>CONTACT US</span> </a><span id="divider">|</span> <a href="/SG50/Acknowledgement/" ">
                <span>ACKNOWLEDGEMENT</span> </a><span id="divider">|</span> <a href="<%=getSitemapLink() %>">
                    <span>SITEMAP</span> </a>
    </div>
    <p id="cpDesktop">
        Copyright &copy; SG50 Programme Office. All rights reserved.
    </p>
    <p id="cpMobile">
        Copyright &copy; SG50 Programme Office. All rights reserved.
    </p>
    <!-- FAQs -->
    <div id="dvPopup">
        <a href="#" onclick="HideModalPopup('dvPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle">
            <p style="line-height: normal">
                <sc:Text ID="sct_FAQTitle" runat="server" DataSource="{8F9DC4B9-8D30-4F6D-AD4D-523759BDBC3C}"
                    Field="Value" />
            </p>
        </div>
        <div id="Qmark">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <!-- FAQs Repeater -->
                <asp:Repeater ID="repFAQs" OnItemDataBound="repFAQs_OnItemDataBound" runat="server">
                    <ItemTemplate>
                        <div id="<%# DataBinder.Eval(Container.DataItem, "FAQ_ID_No") %>" class="faqTitle"
                            onclick="<%# DataBinder.Eval(Container.DataItem, "FAQ_OnClick") %>">
                            <p>
                                <a href="#">
                                    <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_No") %>.
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_Question") %>
                            </p>
                        </div>
                        <div id="<%# DataBinder.Eval(Container.DataItem, "FAQ_Con_ID") %>" class="faqContents"
                            style="display: none">
                            <p>
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_Answer") %>
                            </p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <!-- End of FAQs Repeater -->
            </div>
        </div>
    </div>
    <!-- End FAQs -->
    <!-- Celebration Fund FAQs -->
    <div id="dvPopup2">
        <a href="#" onclick="HideModalPopup('dvPopup2'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle2">
            <p>
                <p>
                    <sc:Text ID="sct_celebrationFundTitle" runat="server" DataSource="{22567F50-0BA2-4CA4-B4FC-271D2E897433}"
                        Field="Value" />
                </p>
            </p>
        </div>
        <div id="Qmark2">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <!-- Celebration Fund FAQs -->
                <asp:Repeater ID="repCFFAQs" OnItemDataBound="repCFFAQs_OnItemDataBound" runat="server">
                    <ItemTemplate>
                        <div id='<%# DataBinder.Eval(Container.DataItem, "FAQ_ID_No") %>' class="faqTitle"
                            onclick="<%# DataBinder.Eval(Container.DataItem, "FAQ_OnClick") %>">
                            <p>
                                <a href="#">
                                    <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_No") %>.
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_Question") %>
                            </p>
                        </div>
                        <div id='<%# DataBinder.Eval(Container.DataItem, "FAQ_Con_ID") %>' class="faqContents"
                            style="display: none">
                            <p>
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_Answer") %>
                            </p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <!-- End  of Celebration Fund FAQs -->
                <br />
            </div>
        </div>
    </div>
    <!-- End Celebration Fund FAQs -->
    <!--  House Rules Sub Page Start -->
    <div id="dvHouseRulesPopup">
        <a href="#" onclick="HideModalPopup('dvHouseRulesPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div class="FontTitleHR">
            <p style="line-height: normal">
                <sc:Text ID="sct_HouseRulesTitle" runat="server" DataSource="{EAB352F0-4E79-48C6-A646-06CD5310B7E2}"
                    Field="Description" />
            </p>
        </div>
        <div class="houseRulesContents">
            <div class="aboutContent">
                <sc:Text ID="sct_HouseRulesContent" runat="server" DataSource="{EAB352F0-4E79-48C6-A646-06CD5310B7E2}"
                    Field="Value" />
            </div>
        </div>
    </div>
    <!--  House Rules Sub Page End -->
<%--</body>
</html>--%>
