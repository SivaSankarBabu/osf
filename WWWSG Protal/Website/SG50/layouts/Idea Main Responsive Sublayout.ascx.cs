﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace Layouts.Idea_main_sublayout
{

    /// <summary>
    /// Summary description for Idea_main_sublayoutSublayout
    /// </summary>
    public partial class Idea_main_sublayoutSublayout : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here        

            if (!IsPostBack)
            {
                FillrepIdeas("");
                noResult.Visible = false;
                noResultMob.Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        { 
            if(isValidated())
            {
                FillrepIdeas(SG50Class.StripUnwantedString(txtSearch.Text.ToString().Trim()));
            }
        }
        
        protected void btnSearchMob_Click(object sender, EventArgs e)
        {
            if (isValidatedMob())
            {
                FillrepIdeas(SG50Class.StripUnwantedString(txtSearchMob.Text.ToString().Trim()));
            }
        }

        private bool isValidated()
        {
            bool isValid = true;

            if (txtSearch.Text.ToString().Trim().Equals(""))
            {
                rfvSearch.IsValid = false;
                isValid = false;
            }
            else {
                // check regular expression
                string text = txtSearch.Text.ToString();
                Match match1 = Regex.Match(text, regexSearch.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexSearch.IsValid = false;
                }
            }
            return isValid;
        }

        private bool isValidatedMob()
        {
            bool isValid = true;

            if (txtSearchMob.Text.ToString().Trim().Equals(""))
            {
                rfvSearchMob.IsValid = false;
                isValid = false;
            }
            else
            {
                // check regular expression
                string text = txtSearchMob.Text.ToString();
                Match match1 = Regex.Match(text, regexSearchMob.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexSearchMob.IsValid = false;
                }
            }

            return isValid;
        }

        private void FillrepIdeas( string searchString)
        {
            try
            {
                DataTable dtrepIdeas = new DataTable();
                dtrepIdeas.Columns.Add("cssClass", typeof(string));
                dtrepIdeas.Columns.Add("ideaText", typeof(string));
                dtrepIdeas.Columns.Add("Name", typeof(string));
                dtrepIdeas.Columns.Add("Age", typeof(string));
                dtrepIdeas.Columns.Add("href", typeof(string));
                dtrepIdeas.Columns.Add("Region", typeof(string));




                DataRow drrepIdeas;

                Item itmIdeaRepository = SG50Class.web.GetItem(SG50Class.str_Idea_Repository_Item_ID);

                //IEnumerable<Item> itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                //                                      where
                //                                      a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                //                                      orderby
                //                                      Sitecore.DateUtil.IsoDateToDateTime(a.Fields["__Created"].Value.ToString())
                //                                      select a).Take(17);

                IEnumerable<Item> itmIEnumWestIdea = Enumerable.Empty<Item>();
                if (searchString.Equals("")) //onload
                {
                    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(17);
                }
                else
                {
                    //search
                    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        && a["Name"].ToLower().Contains(searchString.ToLower()) || a["idea"].ToLower().Contains(searchString.ToLower())
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(17);

                    if (itmIEnumWestIdea.Count() == 0)
                    {
                        //no results from search, show the most recent results
                        itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                            where
                                            a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                            select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(17);
                        noResult.Visible = true;
                        noResultMob.Visible = true;
                    }
                    else {
                        noResult.Visible = false;
                        noResultMob.Visible = false;
                    }
                }
                

                
                foreach (Item a in itmIEnumWestIdea)
                {
                    try
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = LinkManager.GetItemUrl(a);
                        drrepIdeas["cssClass"] = "";
                        if (a["Idea"].ToString().Count() > 31)
                        {
                            drrepIdeas["ideaText"] = a["Idea"].ToString().Remove(30) + "...";
                        }
                        else
                        {
                            drrepIdeas["ideaText"] = a["Idea"];
                        }
                        drrepIdeas["Name"] = a["Name"];
                        drrepIdeas["Age"] = a["Age"];
                        drrepIdeas["Region"] = a["Region"];

                        dtrepIdeas.Rows.Add(drrepIdeas);
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Idea_main_sublayoutSublayout FillrepIdeas a exSub1 : " + exSub1.ToString(), this);
                    }
                }

                //GenerateResults(dtrepIdeas);
                repIdeas.DataSource = dtrepIdeas;
                repIdeas.DataBind();

                repIdeasMobile.DataSource = dtrepIdeas;
                repIdeasMobile.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("Idea_main_sublayoutSublayout FillrepIdeas exMain : " + exMain.ToString(), this);
            }
        }
        protected string getSubmitAnIdeaLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Submit_An_Idea_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Submit_An_Idea_Item_ID));
            }
            return link;
        }

        protected string getRegionLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID));
            }
            return link;
        }

        protected string getRecentLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID));
            }
            return link;
        }

        protected string getSeeAllLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID));
            }
            return link;
        }
    }
}