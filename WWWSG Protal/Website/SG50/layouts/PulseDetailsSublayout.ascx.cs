﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Layouts.Pulsedetailssublayout
{

    /// <summary>
    /// Summary description for PulsedetailssublayoutSublayout
    /// </summary>
    public partial class PulsedetailssublayoutSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();

        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmContext = Sitecore.Context.Item;
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here

            if (!IsPostBack)
            {
                FillPulse();
                SocialSharing();
                FillRightSidePulse();
                fillRotatingBanner();
            }
        }

        public void FillPulse()
        {
            try
            {
                DataTable dtPulse = new DataTable();
                dtPulse.Columns.Add("item", typeof(Item));
                dtPulse.Columns.Add("imgSrc", typeof(string));
                dtPulse.Columns.Add("Title", typeof(string));
                dtPulse.Columns.Add("Id", typeof(string));
                dtPulse.Columns.Add("href", typeof(string));
                dtPulse.Columns.Add("videoURL", typeof(string));


                dtPulse.Columns.Add("imgSrc1", typeof(string));
                dtPulse.Columns.Add("Title1", typeof(string));
                dtPulse.Columns.Add("href1", typeof(string));


                dtPulse.Columns.Add("imgSrc2", typeof(string));
                dtPulse.Columns.Add("Title2", typeof(string));
                dtPulse.Columns.Add("href2", typeof(string));

                DataRow drPulse;
                IEnumerable<Item> Pulsechild = web.GetItem("{83DBF609-220C-45B2-9A27-D5411E98B8E3}").GetChildren();
                if (itmContext.Fields["YouTubeURL"] != null && !string.IsNullOrEmpty(itmContext.Fields["YouTubeURL"].ToString()))
                {


                    divIframe.Src = itmContext.Fields["YouTubeURL"].ToString();
                }
                else
                {
                    videoclass.Visible = false;

                }

                if (itmContext.Fields["Date"] != null && !string.IsNullOrEmpty(itmContext.Fields["Date"].ToString()))
                {
                    txtDate.Visible = true;
                    atID.Visible = true;
                    Date1.Visible = true;
                }
                else
                {
                    txtDate.Visible = false;
                    atID.Visible = false;
                    Date1.Visible = false;
                }
                int count = 1;

                StringBuilder sbBannerText = new StringBuilder();
                foreach (Item a in Pulsechild)
                {
                    drPulse = dtPulse.NewRow();
                    drPulse["item"] = a;
                    drPulse["title"] = a.Fields["Title"].ToString();
                    drPulse["Id"] = a.ID.ToString();
                    drPulse["href"] = LinkManager.GetItemUrl(a).ToString();
                    //drPulse["videoURL"] = a.Fields["YouTubeURL"].ToString();
                    Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                    if (mainImgField != null)
                    {
                        string mainimgSrc = mainImgField.Src;
                        if (!string.IsNullOrEmpty(mainimgSrc))
                        {
                            drPulse["imgSrc"] = mainimgSrc;
                        }
                    }


                    string StoryTitle = string.Empty;

                    if (drPulse["title"] != null && drPulse["title"] != "")
                    {
                        StoryTitle = drPulse["title"].ToString();
                        int storyLength = StoryTitle.Length;
                        if (storyLength > 50)
                        {

                            StoryTitle = StoryTitle.Substring(0, 40) + "...";
                            drPulse["title"] = StoryTitle;
                        }
                        else
                        {

                            // StoryTitle = StoryTitle;
                            drPulse["title"] = StoryTitle;
                        }
                    }



                    if (count % 2 == 0)
                    {
                        sbBannerText.Append("<div class='artical-cont'>");
                        sbBannerText.Append("<div class='articleimgs'>");
                        sbBannerText.Append("<img src='" + drPulse["imgSrc"] + "' />");
                        sbBannerText.Append("</div>");
                        sbBannerText.Append("<div class='Lorem-cont'>");
                        sbBannerText.Append(StoryTitle);
                        sbBannerText.Append("<a href='" + drPulse["href"] + "' class='readmore'>Read More</a>");
                        sbBannerText.Append("</div>");
                        sbBannerText.Append("</div>");
                        sbBannerText.Append("</li>");







                    }
                    else
                    {
                        sbBannerText.Append("<li>");
                        sbBannerText.Append("<div class='artical-cont'>");
                        sbBannerText.Append("<div class='articleimgs'>");
                        sbBannerText.Append("<img src='" + drPulse["imgSrc"] + "'  />");
                        sbBannerText.Append("</div>");
                        sbBannerText.Append("<div class='Lorem-cont'>");
                        sbBannerText.Append(StoryTitle);
                        sbBannerText.Append("<a href='" + drPulse["href"] + "' class='readmore'>Read More</a>");
                        sbBannerText.Append("</div>");
                        sbBannerText.Append("</div>");
                        sbBannerText.Append("<span class='texture-border'></span>");
                    }
                    count++;


                    dtPulse.Rows.Add(drPulse);
                }
                MbileCarosole.InnerHtml = sbBannerText.ToString();
                repPulse.DataSource = dtPulse;
                repPulse.DataBind();
                //repMobile.DataSource = dtPulse;
                // repMobile.DataBind();

                // dtPulse.DefaultView.Sort = "title";
                // repmob2.DataSource = dtPulse;
                // repmob2.DataBind();
                //repIframe.DataSource = dtPulse;
                //repIframe.DataBind();
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }
        private void SocialSharing()
        {

            DataTable dtrepEvents = new DataTable();
            dtrepEvents.Columns.Add("Id", typeof(string));
            dtrepEvents.Columns.Add("FaceBookTitle", typeof(string));
            dtrepEvents.Columns.Add("FaceBookContent", typeof(string));
            dtrepEvents.Columns.Add("FaceBookThumbnail", typeof(string));
            dtrepEvents.Columns.Add("TwitterContent", typeof(string));
            dtrepEvents.Columns.Add("href", typeof(string));
            dtrepEvents.Columns.Add("HostName", typeof(string));
            dtrepEvents.Columns.Add("AccessToken", typeof(string));
            dtrepEvents.Columns.Add("EventUrl", typeof(string));
            DataRow drrepEvents;
            //try
            //{
            string SocialTitle = string.Empty;
            string SocialContent = string.Empty;
            string TwitterContent = string.Empty;
            string noHTML = string.Empty;
            string inputHTML = string.Empty;

            drrepEvents = dtrepEvents.NewRow();
            drrepEvents["HostName"] = hostName + "/";
            drrepEvents["AccessToken"] = accessToken;
            drrepEvents["href"] = LinkManager.GetItemUrl(itmContext).ToString();
            drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(itmContext).ToString();

            if (!string.IsNullOrEmpty(itmContext.Fields["SocialSharingTitle"].ToString()))
            {
                drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["SocialSharingTitle"].ToString(), CommonMethods.faceBook);
            }
            else
            {
                drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["Title"].ToString(), CommonMethods.faceBook);
            }

            if (!string.IsNullOrEmpty(itmContext.Fields["SocialSharingDescription"].ToString()))
            {
                drrepEvents["FaceBookContent"] = cmObj.BuildString(itmContext.Fields["SocialSharingDescription"].ToString(), CommonMethods.faceBook); //+ " " + hostName + LinkManager.GetItemUrl(itmContext);
            }
            else
            {
                drrepEvents["FaceBookContent"] = cmObj.BuildString(itmContext.Fields["Sub Heading"].ToString(), CommonMethods.faceBook);
                //inputHTML = Sitecore.Web.UI.WebControls.FieldRenderer.Render(itmContext, "Description");// itmContext.Fields["Blurb"].ToString();
                //drrepEvents["FaceBookContent"] = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim() + " " + hostName + LinkManager.GetItemUrl(itmContext);
            }
            Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["SocialSharingImage"]);
            string fbimgSrc = fbimgField.Src;
            if (!fbimgSrc.Equals(""))
            {
                drrepEvents["FaceBookThumbNail"] = hostName + "/" + fbimgSrc;
            }
            else
            {
                Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Image"]);
                if (!string.IsNullOrEmpty(staticimgField.Src))
                {
                    drrepEvents["FaceBookThumbNail"] = hostName + "/" + staticimgField.Src;
                }
                else
                {
                    drrepEvents["FaceBookThumbNail"] = "";
                }

            }

            if (!string.IsNullOrEmpty(itmContext.Fields["SocialSharingDescription"].ToString()) && !string.IsNullOrWhiteSpace(itmContext.Fields["SocialSharingDescription"].ToString()))
                TwitterContent = itmContext.Fields["SocialSharingDescription"].ToString();
            else
            {
                inputHTML = itmContext.Fields["Sub Heading"].ToString(); // itmContext.Fields["Blurb"].ToString();
                noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                if (noHTML.Length > 120)
                    TwitterContent = noHTML.Substring(0, 119);
                else
                    TwitterContent = noHTML.Substring(0, noHTML.Length);
            }
            drrepEvents["TwitterContent"] = cmObj.BuildString(TwitterContent, CommonMethods.faceBook);
            drrepEvents["Id"] = itmContext.ID;
            dtrepEvents.Rows.Add(drrepEvents);
            //Response.Write("Count"+dtrepEvents.Rows.Count);
            repsocialSharingTop.DataSource = dtrepEvents;
            repsocialSharingTop.DataBind();
            //}
            //catch (Exception exSub1)
            //{
            //    Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            //}
        }
        public void FillRightSidePulse()
        {
            try
            {
                DataTable dtPulse = new DataTable();
                dtPulse.Columns.Add("item", typeof(Item));
                dtPulse.Columns.Add("imgSrc", typeof(string));
                dtPulse.Columns.Add("Title", typeof(string));
                dtPulse.Columns.Add("Id", typeof(string));
                dtPulse.Columns.Add("href", typeof(string));
                DataRow drPulse;
                IEnumerable<Item> Pulsechild = web.GetItem("{83DBF609-220C-45B2-9A27-D5411E98B8E3}").GetChildren().Take(5);
                foreach (Item a in Pulsechild)
                {
                    drPulse = dtPulse.NewRow();
                    drPulse["item"] = a;
                    drPulse["title"] = a.Fields["Title"].ToString();
                    drPulse["Id"] = a.ID.ToString();
                    drPulse["href"] = LinkManager.GetItemUrl(a).ToString();
                    Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Thumb Image"]);
                    if (mainImgField != null)
                    {
                        string mainimgSrc = mainImgField.Src;
                        if (!string.IsNullOrEmpty(mainimgSrc))
                        {
                            drPulse["imgSrc"] = mainimgSrc;
                        }
                    }


                    string StoryTitle = string.Empty;

                    if (drPulse["title"] != null && drPulse["title"] != "")
                    {
                        StoryTitle = drPulse["title"].ToString();
                        int storyLength = StoryTitle.Length;
                        if (storyLength > 50)
                        {

                            StoryTitle = StoryTitle.Substring(0, 40) + "...";
                            drPulse["title"] = StoryTitle;
                        }
                        else
                        {

                            // StoryTitle = StoryTitle;
                            drPulse["title"] = StoryTitle;
                        }
                    }

                    dtPulse.Rows.Add(drPulse);
                }
                repRight.DataSource = dtPulse;
                repRight.DataBind();
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }
        private void fillRotatingBanner()
        {
            DataTable dtrepSlide = new DataTable();
            dtrepSlide.Columns.Add("imgSrc", typeof(string));
            dtrepSlide.Columns.Add("slideID", typeof(string));
            dtrepSlide.Columns.Add("caption", typeof(string));
            DataRow drrepSlide;

            //DataTable dtrepMobileGallery = new DataTable();
            //dtrepMobileGallery.Columns.Add("imgSrc", typeof(string));
            //dtrepMobileGallery.Columns.Add("caption", typeof(string));
            //DataRow drrepMobileGallery;

            //DataTable dtrepThumbslider = new DataTable();
            //dtrepThumbslider.Columns.Add("number", typeof(string));
            //dtrepThumbslider.Columns.Add("thumbImgSrc", typeof(string));
            //DataRow drrepThumbslider;

            int BannerCount = 0;
            int slideID = 0;

            if (itmContext.Fields["Gallery"] != null && !itmContext["Gallery"].Equals(""))
            {
                Sitecore.Data.Fields.MultilistField mfBanners = itmContext.Fields["Gallery"];
                if (mfBanners != null)
                {
                    foreach (ID id in mfBanners.TargetIDs)
                    {
                        BannerCount++;
                        slideID++;
                        if (BannerCount <= 7)
                        {
                            drrepSlide = dtrepSlide.NewRow();
                            MediaItem item = Sitecore.Context.Database.GetItem(id.ToString());
                            if (item != null)
                            {
                                if (BannerCount == 1)
                                {
                                    imgBanner.ImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(item);
                                    innersrc.InnerHtml = item.Alt.ToString();

                                }

                                drrepSlide["imgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(item);
                                drrepSlide["caption"] = item.Alt.ToString();
                                drrepSlide["slideID"] = "slide" + slideID.ToString();
                                dtrepSlide.Rows.Add(drrepSlide);


                            }

                        }
                    }

                    repRotating.DataSource = dtrepSlide;
                    repRotating.DataBind();
                }
            }

            if (BannerCount > 1)
            {
                sliderContainer.Visible = true;
            }
            else
            {
                sliderContainer.Visible = false;
            }
            if (BannerCount > 0)
            {

                GalleryPortion.Visible = true;
            }
            else
            {

                GalleryPortion.Visible = false;
            }
        }
        protected void imgSearchButton_Click(object sender, ImageClickEventArgs e)
        {
            // Response.Redirect("/sg50/pulse?Search=" + SG50Class.StripUnwantedHtml(txtSerachPulse.Text));
            Response.Redirect("/SG50/SearchResult.aspx?searchWord=" + txtSerachPulse.Text.Trim());
        }
    }
}