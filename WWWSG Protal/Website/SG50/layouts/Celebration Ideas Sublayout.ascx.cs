﻿using Sitecore.Links;
using System;
using System.Configuration;

namespace Layouts.Celebration_ideas_sublayout {
  
	/// <summary>
	/// Summary description for Celebration_ideas_sublayoutSublayout
	/// </summary>
  public partial class Celebration_ideas_sublayoutSublayout : System.Web.UI.UserControl 
	{
    private void Page_Load(object sender, EventArgs e) {
      // Put user code to initialize the page here
    }

    protected string getSubmitAnIdeaLink()
    {
        string link = "";
        if (SG50Class.web.GetItem(SG50Class.str_Submit_An_Idea_Item_ID) != null)
        {
            link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Submit_An_Idea_Item_ID));
        }
        return link;
    }

  }
}