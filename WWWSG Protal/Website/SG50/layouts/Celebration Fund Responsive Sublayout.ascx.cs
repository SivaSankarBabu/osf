﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Data.Fields;

namespace Layouts.Celebration_fund_responsive_sublayout {
  
	/// <summary>
	/// Summary description for Celebration_fund_responsive_sublayoutSublayout
	/// </summary>
  public partial class Celebration_fund_responsive_sublayoutSublayout : System.Web.UI.UserControl 
	{private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
		private void Page_Load(object sender, EventArgs e) {
      // Put user code to initialize the page here
       
      Item itmContext = Sitecore.Context.Item;
    }
     public void DownloadForm(object sender, EventArgs e)
    {
        //string str = "";
        //str = SG50Class.str_Form_Item_ID;

        Item item = Sitecore.Context.Item;
        FileField file = item.Fields["DownloadFile"];

        if (file != null)
        {
            MediaItem mi = file.MediaItem;//web.GetItem(file.ID);
            if (mi != null)
            {
                Stream stream = mi.GetMediaStream();
                long fileSize = stream.Length;
                byte[] buffer = new byte[(int)fileSize];
                stream.Read(buffer, 0, (int)stream.Length);
                stream.Close();

                Response.ContentType = String.Format(mi.MimeType);
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + mi.Name + "." + mi.Extension);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(buffer);
                Response.End();
            }
            else { Response.Write("no mi"); }
        }
    }
  }
}