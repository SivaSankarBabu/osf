﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Applicationstep4.Applicationstep4Sublayout" CodeFile="~/SG50/layouts/ApplicationStep4.ascx.cs" %>
<link href="/SG50/include/css/style.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/dropkick.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/responsive.css" rel="stylesheet" type="text/css" />
<section class="main-content">
    <!---main-content start-------->
    <div class="foSg50rm-banner">
        <img src="/SG50/images/Images/batch-4/banner.jpg" />
    </div>
    <section class="form-container">
        <div class="head-title">
            <h1>THANK YOU</h1>
        </div>
        <div class="grid-container">

            <div class="form-content">
                <!------form content start------------>
                <p>for applying for the SG50 Celebration Fund. Successful applicants will be notified shortly. </p>

                <p>Your application number is <span class="mandatory">
                    <asp:Label ID="AppId" runat="server"></asp:Label></span>.</p>
                <p>A copy of your application will be sent to your email address. Please check your spam/junk folder if you did not receive it.</p>
                <p>
                    <label>For further enquiries,</label></p>
                <p>
                    Drop us an email at <span class="mandatory"><a href="mailto:SG50_CelebrationFund@mccy.gov.sg">SG50_CelebrationFund@mccy.gov.sg</a></span><%--, or<br />
                    <p>
                        Call us at 1800 837 9798  between 9am – 5.30pm, Mondays to Fridays.
                    </p>--%>
            </div>
            <!------form content Ends------------>
        </div>
    </section>
</section>
