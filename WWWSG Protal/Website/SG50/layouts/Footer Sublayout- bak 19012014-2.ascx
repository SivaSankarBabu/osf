<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="Layouts.Footer_sublayout.Footer_sublayoutSublayout" CodeFile="~/SG50/layouts/Footer Sublayout.ascx.cs"%>
<html>
<head>
    <script language="javascript" type="text/javascript" src="/SG50/include/js/modalpopupforfaq.js"></script>

</head>
<body>
    <!--  FAQ Sub Page Start -->
    <div id="dvPopup">
        <a href="#" onclick="HideModalPopup('dvPopup'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle">
            <p class="FontTitle1">ALL YOU NEED </p>
            <p class="FontTitle2">TO KNOW </p>
            <p class="FontTitle3">ABOUT SG50</p>
        </div>
        <div id="Qmark">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <div id="faq1ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq1ID', 'faq1ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        1. What is SG50?

                    </p>
                </div>
                <div id="faq1ConID" class="faqContents" style="display: none">
                    <p>SG50 is a nationwide effort to celebrate our country�s 50th birthday in 2015. That�s a huge milestone � 50 years of independence! This is a momentous event for Singaporeans to reflect on how far we�ve come together as a nation and people.
</p>
                </div>
                <div id="faq2ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq2ID', 'faq2ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        2. How long will the SG50 celebrations last?
                    </p>
                </div>
                <div id="faq2ConID" class="faqContents" style="display: none">
                    <p>The SG50 celebrations will span over a year, from January to December 2015. But we�re not waiting till 2015 to get started. 
</p>
                </div>
                <div id="faq3ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq3ID', 'faq3ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        3. How can Singaporeans get involved with SG50? 

                    </p>
                </div>
                <div id="faq3ConID" class="faqContents" style="display: none">
                    <p>This is our country, and our celebrations couldn�t be more meaningful if everyone got involved in a big way. <br>
Find out what your fellow Singaporeans are planning and tell us how you can make it even more special.</p>
                </div>
                <div id="faq4ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq4ID', 'faq4ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        4. What does the SG50 committee do?</p>
                </div>
                <div id="faq4ConID" class="faqContents" style="display: none">
                    <p>SG50 hopes to reach out to as many Singaporeans as possible. To head up our Steering Committee, we�ve invited individuals from a variety of fields in the public, people and private sectors. The SG50 Steering Committee is led by Minister for Education, Heng Swee Keat; while the Programme Office is led by Acting Minister for Culture Community and Youth, Lawrence Wong. <br>
The Steering Committee is supported by five teams, which will encourage active community participation and involvement leading up to 2015. The five teams are: 



					
</p>
<p>
	<ul>Education and Youth</ul>
	<ul>Culture and Community</ul>
	<ul>Economic and International</ul>
	<ul>Environment and Infrastructure</ul>
	<ul>Partnership</ul>
</p>

<p>Find out more about our committee members.</p>

                </div>
                <div id="faq5ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq5ID', 'faq5ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        5. What is the SG50 Celebration Fund?

                    </p>
                </div>
                <div id="faq5ConID" class="faqContents" style="display: none">
                    <p>To celebrate Singapore turning 50, we�re inviting all Singaporeans to come up with projects that express your pride, identity and love for our country. We want to help fund projects that will get Singaporeans excited about our 50th birthday. <br>

Got an idea? Get the funds to make it a reality. </p>
                </div>
                <div id="faq6ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq6ID', 'faq6ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        6. Who can apply for the SG50 Celebration Fund?
          </p>
                </div>
                <div id="faq6ConID" class="faqContents" style="display: none">
                    <p>The SG50 Celebration Fund is open to:</p>
<p><ul>Informal interest or community groups. Minimum of 2 individuals (at least one of whom is a Singaporean, aged 18 years and above)</ul>
<ul>Societies registered with the Registry of Societies (ROS) and; Companies registered with the Accounting and Corporate Regulatory Authority (ACRA)</ul>
</p>
<p>
Want to see if you qualify? Drop us an email at SG50CelebrationFund@Singapore50.sg
</p>
                </div>
                <div id="faq7ID" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq7ID', 'faq7ConID'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        7. How can I find out more about SG50?

                    </p>
                </div>
                <div id="faq7ConID" class="faqContents" style="display: none">
                    <p>If you have further questions, you can get in touch with us here.</p>
                </div>
                                <br />
                <br />

            </div>
        </div>
    </div>
    <!-- FAQ Sub Page End -->
	
	 <!--  FAQ Bottom Sub Page Start -->
    <div id="dvPopup2">
        <a href="#" onclick="HideModalPopup('dvPopup2'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle2">
            <p class="FontTitle1">All you need</p>
            <p class="FontTitle2">to know about</p>
            <p class="FontTitle3">SG50.</p>
        </div>
        <div id="Qmark2">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <div id="faq1ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq1ID2', 'faq1ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        1.    What is SG50?
                    </p>
                </div>
                <div id="faq1ConID2" class="faqContents" style="display: none">
                    <p>        SG50 is a nationwide effort to celebrate our country�s 50th birthday in 2015. That�s a huge milestone � 50 years of independence! This is a momentous event for Singaporeans to reflect on how far we�ve come together as a nation and people.</p>
                </div>
                <div id="faq2ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq2ID2', 'faq2ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       2.    How long will the SG50 celebrations last?
                    </p>
                </div>
                <div id="faq2ConID2" class="faqContents" style="display: none">
                    <p>The SG50 celebrations will span over a year, from January to December 2015. But we�re not waiting till 2015 to get started. </p>
                </div>
                <div id="faq3ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq3ID2', 'faq3ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        3.    How can Singaporeans get involved with SG50?
                    </p>
                </div>
                <div id="faq3ConID2" class="faqContents" style="display: none">
                    <p>This is our country, and our celebrations couldn�t be more meaningful if everyone got involved in a big way.</p>
					<p>Find out what your fellow Singaporeans are planning and <a href="#">tell us</a> how you can make it even more special.</p>
				</div>
                <div id="faq4ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq4ID2', 'faq4ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        
                    4.    What does the SG50 committee do?

                    </p>
                </div>
                <div id="faq4ConID2" class="faqContents" style="display: none">
                    <p>SG50 hopes to reach out to as many Singaporeans as possible. To head up our Steering Committee, we�ve invited individuals from a variety of fields in the public, people and private sectors. The SG50 Steering Committee is led by Minister for Education, Heng Swee Keat; while the Programme Office is led by Acting Minister for Culture Community and Youth, Lawrence Wong. </p>
					<p>
						The Steering Committee is supported by five teams, which will encourage active community participation and involvement leading up to 2015. The five teams are:
						
						<ul>Education and Youth</ul>
						<ul>Culture and Community</ul>
						<ul>Economic and International</ul>
						<ul>Environment and Infrastructure</ul>
						<ul>Education and Youth</ul>
						<ul>Partnership</ul>
					
					</p>
					<p><u>Find out more</u> about our committee members.</p>
				</div>
                <div id="faq5ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq5ID2', 'faq5ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       5.  What is the SG50 Celebration Fund?
                    </p>
                </div>
                <div id="faq5ConID2" class="faqContents" style="display: none">
                    <p>To celebrate Singapore turning 50, we�re inviting all Singaporeans to come up with projects that express your pride, identity and love for our country. We want to help fund projects that will get Singaporeans excited about our 50th birthday. </p>
					<p>Got an idea? <a href="#">Get the funds</a> to make it a reality</p>
				</div>
                <div id="faq6ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq6ID2', 'faq6ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        6.    Who can apply for the SG50 Celebration Fund?
                    </p>
                </div>
                <div id="faq6ConID2" class="faqContents" style="display: none">
                    <p>The SG50 Celebration Fund is open to:
						
						<ul>Informal interest or community groups. Minimum of 2 individuals (at least one of whom is a Singaporean, aged 18 years and above) </ul>
						<ul>Societies registered with the Registry of Societies (ROS) and; Companies registered with the Accounting and Corporate Regulatory Authority (ACRA)</ul>
						
					</p>
					
					<p>Want to see if you qualify? Drop us an email at <a href="#">SG50CelebrationFund@Singapore50.sg</a></p>
					
                </div>
                <div id="faq7ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq7ID2', 'faq7ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        7.     How can I find out more about SG50?
                    </p>
                </div>
                <div id="faq7ConID2" class="faqContents" style="display: none">
                    <p>If you have further questions, you can get in touch with us <a href="#">here</a>.</p>
                </div>
                
                <br />
                <br />
                <p class="moreQues">Have more questions you'd like us to answer? <a href="#" onclick="return false;" class="getInTouch">Get in touch with us.</a></p>
            </div>
        </div>
    </div>
    <!-- FAQ Bottom Sub Page End -->

    <!--  House Rules Sub Page Start -->
    <div id="dvHouseRulesPopup">
        <a href="#" onclick="HideModalPopup('dvHouseRulesPopup'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitleHR">
            <p class="FontTitleHR1">LET'S TRY TO KEEP</p>
            <p class="FontTitleHR2">THINGS LIGHT AND</p>
            <p class="FontTitleHR3">LIVELY, SHALL WE?</p>
        </div>
        <div class="houseRulesContents">
            <div>
                <sc:Text ID="sctContent" Field="Content" runat="server" />
            </div>
                 <div id="aboutContent">
					That's why we want to create a comfortable environment for you and the rest of Singapore to engage in discussion. The first rule of thumb would be to avoid creating misunderstanding or conflict, when posting or commenting within the SG50 site.
					<br/><br/>That means you should not use the site to advocate any politics, or demean others based on race and religion. The site also should not be used as a platform to launch personal attacks, threats, insults, advertisements or deragatory remarks. And it goes without saying that vulgarities arre not allowed.
					<br/><br/>Any posts or comments that are deemed insensitive, irrelevant or inappropriate will be removed without prior notice. Please note that discussions on the SG50 site reflect the views of the participants, and not the opinion of the SG50 Programme Office.
              		</div>       
        </div>
    </div>
    <!--  House Rules Sub Page End -->

	    <!--  Acknowledgement Start -->
    <div id="dvAckPopup">
        <a href="#" onclick="HideModalPopup('dvAckPopup'); return false;">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitleHR">
            <p class="FontTitleHR1">A BIG THANK YOU</p>
        </div>
        <div class="ackContents">
			<div id="aboutContent">
				We may be small nation, but we�re brimming with big talent. And the SG50 website is a wonderful testament of what the little red dot and its people have to offer. 
				<br/><br/>Thank you to the following photographers who have captured the Singaporean spirit, through their vibrant snapshots featured on the website. 
				<br/><br/><br/>Andrew JK Tan � <a href="http://www.flickr.com/photos/andrewjktan/" style="color: white;"> http://www.flickr.com/photos/andrewjktan/</a> 
				<br/>Byran Ong � <a href="http://www.flickr.com/photos/smrt173/" style="color: white;"> http://www.flickr.com/photos/smrt173/ </a>  
				<br/>Lufudesu � <a href="http://www.flickr.com/people/8557677@N06/" style="color: white;"> http://www.flickr.com/people/8557677@N06/ </a>
			</div>
        </div>
    </div>
    <!--  Acknowledgement Sub Page End -->
	
	
	
	
    <div class="footer_nav">
         <a href="<%=getContactUsLink() %>">
            <span>CONTACT US</span>
        </a>
        <span id="divider">|</span>
        <a href="#" onclick="ShowModalPopup('dvPopup'); return false; ">
            <span>FREQUENTLY ASKED QUESTIONS (FAQ)</span>
        </a>
        <span id="divider">|</span>
        <a href="#" onclick="ShowModalPopup('dvHouseRulesPopup'); return false; ">
            <span>HOUSE RULES</span>
        </a>
		
		</a>
        <span id="divider">|</span>
        <a href="#" onclick="ShowModalPopup('dvAckPopup'); return false; ">
            <span>ACKNOWLEDGEMENT</span>
        </a>
    </div>

    <p id="cpDesktop">Copyright &copy; SG50 Programme Office. All rights reserved.</p>
    <p id="cpMobile">Copyright &copy; SG50 Programme Office. All rights reserved.</p>
</body>
</html>
