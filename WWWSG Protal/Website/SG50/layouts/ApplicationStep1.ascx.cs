﻿using System;
using Sitecore.Data.Items;
using FUNDED_Logger;
namespace Layouts.Applicationstep1
{

    /// <summary>
    /// Summary description for Applicationstep1Sublayout
    /// </summary>
    public partial class Applicationstep1Sublayout : System.Web.UI.UserControl
    {
        Item itmContext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                Logger.WriteLine("\n...............................................\n");
                Logger.WriteLine("1. Funded form Step1 Process Started");

                Session.Timeout = 45;
                if (Session["SEA"] != null)
                {
                    if (Session["SEA"].ToString() == "1")
                    {
                        chkSports.Checked = true;
                    }
                }

                Sitecore.Data.Fields.CheckboxField CheckboxSports = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Applying Sports Related Project"]);
                if (CheckboxSports.Checked == true)
                {
                    DivSports.Visible = true;
                }
            }
            // Put user code to initialize the page here
        }

        protected void btnLocalProj_Click(object sender, EventArgs e)
        {
            
            if (Page.IsValid)
            {
               
                Logger.WriteLine("2. Redirecting to step2 local Page");
                if (chkSports.Checked == true)
                    Session["SEA"] = 1;
                Session["Local"] = "True";
                Session["Step2"] = "True";
                Session["Organisation"] = null;
                string url = "~/SG50/Step2.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        protected void btnOverseasProj_Click(object sender, EventArgs e)
        {
            
            if (Page.IsValid)
            {
                Logger.WriteLine("2. Redirecting to step2 Overseas");
                if (chkSports.Checked == true)
                    Session["SEA"] = 1;
                Session["Local"] = "False";
                Session["Step2"] = "True";
                Session["Organisation"] = null;
                string url = "~/SG50/Step2.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        protected void btnOrganisation_Click(object sender, EventArgs e)
        {
            
            if (Page.IsValid)
            {
                Logger.WriteLine("2. Redirecting to step2 organisation Page");
                if (chkSports.Checked == true)
                    Session["SEA"] = 1;
                Session["Step2"] = "True";
                Session["Organisation"] = "true";
                Session["Local"] = "False";
                string url = "~/SG50/Step2.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Session["Step2"] != null)
                {
                    if (chkSports.Checked == true)
                        Session["SEA"] = 1;
                    string url = "~/SG50/Step2.aspx";
                    if (!(url.Contains("://")))
                        Response.Redirect(url);
                }
            }
        }
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Session["Step3"] != null)
                {
                    if (Session["Organisation"] == "true")
                    {
                        string url = "~/SG50/Step3Organisation.aspx";
                        if (!(url.Contains("://")))
                            Response.Redirect(url);
                    }
                    else
                    {
                        string url = "~/SG50/Step3.aspx";
                        if (!(url.Contains("://")))
                            Response.Redirect(url);
                    }

                }
            }
        }
    }
}