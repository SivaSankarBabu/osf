﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Celebration_ideas_sublayout.Celebration_ideas_sublayoutSublayout" CodeFile="~/SG50/layouts/Celebration Ideas Sublayout.ascx.cs" %>

<div class="celebrationIdeas">
    <div style="position:relative;"><a href="<%= getSubmitAnIdeaLink() %>" ><img id="ideabackbtn" src="/SG50/images/submission/submission_return.jpg" /></a></div>
    <div class="celebrationIdeasTitle">
        <sc:Text runat="server" Field="Celebration Ideas Title" />
    </div>
    <div class="celebrationIdeasDesc">
        <sc:Text runat="server" Field="Celebration Ideas Top Description" />
    </div>
    <div class="celebrationIdeasImage">
        <div class="celebrationIdeasImageBanner">
            <div class="celebrationIdeasImage1">
                <sc:Image runat="server" Field="Image 1" />
            </div>
            <div class="caption">
                <sc:Image runat="server" Field="Icon Image 1" />
                <sc:Text runat="server" Field="Image 1 Title" />
                <sc:Text runat="server" Field="Image 1 Description" />
            </div>
        </div>
        <div class="celebrationIdeasImageBanner">
            <div class="celebrationIdeasImage2">
                <sc:Image runat="server" Field="Image 2" />
            </div>
            <div class="caption">
                <sc:Image runat="server" Field="Icon Image 2" />
                <sc:Text runat="server" Field="Image 2 Title" />
                <sc:Text runat="server" Field="Image 2 Description" />
            </div>
        </div>
        <div class="celebrationIdeasImageBanner">
            <div class="celebrationIdeasImage3">
                <sc:Image runat="server" Field="Image 3" />
            </div>
            <div class="caption">
                <sc:Image runat="server" Field="Icon Image 3" />
                <sc:Text runat="server" Field="Image 3 Title" />
                <sc:Text runat="server" Field="Image 3 Description" />
            </div>
        </div>
        <div class="celebrationIdeasImageBanner">
            <div class="celebrationIdeasImage4">
                <sc:Image runat="server" Field="Image 4" />
            </div>
            <div class="caption">
                <sc:Image runat="server" Field="Icon Image 4" />
                <sc:Text runat="server" Field="Image 4 Title" />
                <sc:Text runat="server" Field="Image 4 Description" />
            </div>
        </div>

    </div>
</div>
    <div class="celebrationIdeasBottomDesc">
        <sc:Text runat="server" Field="Celebration Ideas Bottom Description" />
    </div>
</div>
