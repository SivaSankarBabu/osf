﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;


namespace Layouts.Sg50homepagesublayout
{

    /// <summary>
    /// Summary description for Sg50homepagesublayoutSublayout
    /// </summary>
    public partial class Sg50homepagesublayoutSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetHomePageBanners();
                GetFeatureNews();
                EventsCalender();
            }
        }


        public void GetHomePageBanners()
        {
            try
            {
                Item HomeBannerItem = web.GetItem("{5B8C76BA-A217-4921-AD02-B00D0EAEAC2B}");

                if (HomeBannerItem != null)
                {
                    ChildList homePageBanners;
                    StringBuilder sbBannerText = new StringBuilder();
                    sbBannerText.Append("<div id='banner'>");
                    sbBannerText.Append(" <ul class='bxslider'>");
                    // StringBuilder pagerText = new StringBuilder();
                    homePageBanners = HomeBannerItem.GetChildren();

                    if (homePageBanners != null && homePageBanners.Count > 0)
                    {
                        int BannerCount = 0;
                        for (int i = 0; i < homePageBanners.Count; i++)
                        {
                            if (homePageBanners[i].Versions.Count > 0 && homePageBanners[i]["Show Banner"] == "1" && BannerCount < 5)
                            {

                                BannerCount++;
                                string BannermediaUrl = string.Empty;
                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)homePageBanners[i].Fields["Banner"]);
                                if (imgField != null && !string.IsNullOrEmpty(imgField.Src))
                                    BannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);

                                string MobileBannermediaUrl = string.Empty;
                                Sitecore.Data.Fields.ImageField MobileimgField = ((Sitecore.Data.Fields.ImageField)homePageBanners[i].Fields["MobileBanner"]);
                                if (MobileimgField != null && !string.IsNullOrEmpty(MobileimgField.Src))
                                    MobileBannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(MobileimgField.MediaItem);

                                string Banner_Display = string.Empty;
                                if (!string.IsNullOrEmpty(homePageBanners[i]["Button Link Type"]))
                                    Banner_Display = homePageBanners[i]["Button Link Type"];

                                sbBannerText.Append("<li class='item'><img src='" + BannermediaUrl + "' alt='' />");

                                if (Banner_Display == "Video")
                                {
                                    sbBannerText.Append("<div class='txt-pan gallery'>");
                                }
                                else
                                {
                                    sbBannerText.Append("<div class='txt-pan'>");
                                }
                                string Title = string.Empty;
                                string text = string.Empty;
                                string url = string.Empty;



                                if (!string.IsNullOrEmpty(homePageBanners[i]["Title"]))
                                    Title = homePageBanners[i]["Title"];
                                sbBannerText.Append("<h1>" + Title + "</h1>");

                                if (!string.IsNullOrEmpty(homePageBanners[i]["Description"]))
                                    text = homePageBanners[i]["Description"];
                                sbBannerText.Append("<p>" + text + "</p>");



                                if (!string.IsNullOrEmpty(homePageBanners[i]["Button URL"]))
                                    url = homePageBanners[i]["Button URL"];




                                if (!string.IsNullOrEmpty(homePageBanners[i]["Button Text"]) && homePageBanners[i]["Button Text"] != null)
                                {
                                    if (Banner_Display == "Video")
                                    {
                                        sbBannerText.Append("<a href='" + url + "' rel='prettyPhoto' class='readmore'>" + homePageBanners[i]["Button Text"] + "</a>");
                                    }
                                    else if (Banner_Display == "External Link")
                                    {
                                        sbBannerText.Append("<a href='" + url + "' class='readmore' target='_blank'>" + homePageBanners[i]["Button Text"] + "</a>");
                                    }
                                    else
                                    {
                                        sbBannerText.Append("<a href='" + url + "' class='readmore'>" + homePageBanners[i]["Button Text"] + "</a>");
                                    }
                                }


                                sbBannerText.Append("</div>");
                                sbBannerText.Append("</li>");

                            }
                        }
                        sbBannerText.Append("</ul>");
                        sbBannerText.Append("</div>");
                        DvHomePageBanners.InnerHtml = sbBannerText.ToString();
                        // slidercontent.InnerHtml = sbBannerText.ToString();

                        // pagerText.Append("<div id='pager' class='pager'>");
                        //int pagercount = 0;
                        //for (int i = 0; i < homePageBanners.Count; i++)
                        //{
                        //    if (homePageBanners[i].Versions.Count > 0 && homePageBanners[i]["Show Banner"] == "1" && pagercount < 5)
                        //    {
                        //        pagercount++;

                        //        pagerText.Append("<a href='#'><span></span></a>");
                        //    }
                        //}
                        //pagerText.Append("</div>");
                        //pageOuter.InnerHtml = pagerText.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void GetFeatureNews()
        {
            try
            {
                string url = string.Empty;
                DataTable dtrepNews = new DataTable();
                dtrepNews.Columns.Add("item", typeof(Item));
                dtrepNews.Columns.Add("NewsDescription", typeof(string));
                dtrepNews.Columns.Add("href", typeof(string));
                dtrepNews.Columns.Add("NewsImage", typeof(string));
                dtrepNews.Columns.Add("Date", typeof(string));


                Item itmPress = SG50Class.web.GetItem("{2A630274-86B0-4D5A-A14D-A542500A7AA4}");
                IEnumerable<Item> itmIEnumPressReleases = null;
                IEnumerable<Item> itmIEnumPressReleases2 = null;


                itmIEnumPressReleases2 = from a in itmPress.Axes.GetDescendants().OrderByDescending(x => x.Statistics.Created)
                                         where
                                         a.TemplateID.ToString().Equals("{EA3860C6-5ACA-46EB-8D94-82723B203F45}")
                                         select a;
                itmIEnumPressReleases = itmIEnumPressReleases2.Take(3);



                foreach (Item a in itmIEnumPressReleases)
                {


                    DataRow drrepNews;
                    drrepNews = dtrepNews.NewRow();
                    drrepNews["item"] = a;
                    drrepNews["NewsDescription"] = a.Fields["News Description"];


                    Sitecore.Data.Fields.LinkField link = ((Sitecore.Data.Fields.LinkField)a.Fields["News Link"]);
                    if (link != null && !string.IsNullOrEmpty(link.Url))
                    {
                        url = link.Url;
                    }

                    drrepNews["href"] = url;

                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["News Image"]);
                    if (imgField != null && !string.IsNullOrEmpty(imgField.Src))
                    {
                        string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem); ;
                        if (!string.IsNullOrEmpty(imgSrc))
                        {
                            drrepNews["NewsImage"] =  imgSrc;
                        }
                    }


                    Sitecore.Data.Fields.DateField date = a.Fields["News Date"];
                    if (date != null)
                    {
                        String[] suffixes =
               { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
         "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
         "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
         "th", "st" };

                        System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
                        int dd = System.Convert.ToInt32(datetime.ToString("dd"));
                        string prefix = suffixes[dd];
                        drrepNews["Date"] = datetime.ToString("dd, MMM yyyy").Replace(",", prefix); ;

                    }
                    dtrepNews.Rows.Add(drrepNews);

                }
                repFeatureNews.DataSource = dtrepNews;
                repFeatureNews.DataBind();
            }
            catch (Exception exMain)
            {
                throw exMain;
            }
        }

        public void EventsCalender()
        {
            try
            {
                DataTable dtrepEvents = new DataTable();
                dtrepEvents.Columns.Add("item", typeof(Item));
                dtrepEvents.Columns.Add("NewsDescription", typeof(string));
                dtrepEvents.Columns.Add("href", typeof(string));
                dtrepEvents.Columns.Add("NewsImage", typeof(string));
                dtrepEvents.Columns.Add("Date", typeof(string));
                dtrepEvents.Columns.Add("itemName", typeof(string));

                Item itmPress = SG50Class.web.GetItem("{6EBFAFA6-ED1B-4DBE-8805-4FA379E281BF}");
                IEnumerable<Item> itmIEnumPressReleases = null;
                IEnumerable<Item> itmIEnumPressReleases2 = null;


                itmIEnumPressReleases2 = from a in itmPress.Axes.GetDescendants()
                                         where
                                         a.TemplateID.ToString().Equals("{297C9677-56C3-433A-9841-6B594C1AA3F8}") && a.Fields["ThumbImage"].Value.ToString()!= ""
                                         orderby
                                         (a.Fields["Event Date"].Value.ToString() != "" ? Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Event Date"].Value.ToString()) : Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Start Date"].Value.ToString())) 
                                         select a;
                itmIEnumPressReleases = itmIEnumPressReleases2;

                int EventCount = 0;

                foreach (Item a in itmIEnumPressReleases)
                {
			
		                Sitecore.Data.Fields.DateField date = a.Fields["Event Date"];

                                Sitecore.Data.Fields.DateField Startdate = a.Fields["Start Date"];
                                Sitecore.Data.Fields.DateField Enddate = a.Fields["End Date"];
                                System.DateTime Startdatetime = Sitecore.DateUtil.IsoDateToDateTime(Startdate.Value);
                                System.DateTime Enddatetime = Sitecore.DateUtil.IsoDateToDateTime(Enddate.Value);
				 System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
 if ((datetime.Date.CompareTo(DateTime.Today.Date) >= 0 && "01/01/0001 00:00:00" != datetime.ToString()) || (Startdatetime.Date.CompareTo(DateTime.Today.Date) <= 0 && Enddatetime.Date.CompareTo(DateTime.Today.Date) >= 0 && "01/01/0001 00:00:00" != Startdatetime.ToString()) || (Startdatetime.Date.CompareTo(DateTime.Today.Date) >= 0 && Enddatetime.Date.CompareTo(DateTime.Today.Date) >= 0 && "01/01/0001 00:00:00" != Enddatetime.ToString()))
                        {
                   
			
                    if (EventCount < 8)
                    {
                        Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["ThumbImage"]);
                        if (imgField != null && imgField.MediaItem!=null && !string.IsNullOrEmpty(imgField.Src))
                        {

                            string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                            if (!string.IsNullOrEmpty(imgSrc))
                            {

                                DataRow drrepEvents;
                                drrepEvents = dtrepEvents.NewRow();
                                drrepEvents["item"] = a;
                                drrepEvents["NewsDescription"] = a.Fields["EventCalendarDescription"];
                                drrepEvents["href"] = "/SG50" + LinkManager.GetItemUrl(a).ToString().Replace("/SG50", "").Replace("/en", "");
                                drrepEvents["NewsImage"] =  imgSrc;
                                drrepEvents["itemName"] = a.Name;
                                

                                if (Startdate != null && Startdatetime.ToString() != "01/01/0001 00:00:00" && Enddate != null && Enddatetime.ToString() != "01/01/0001 00:00:00")
                                {
                                    //Response.Write("'" + Startdate.ToString() + "'");
                                    

                                    drrepEvents["Date"] = Startdatetime.ToString("MMMM yyyy") + "-" + Enddatetime.ToString("MMMM yyyy");
                                }


                                else if (date != null && !string.IsNullOrEmpty(date.ToString()))
                                {
                                    String[] suffixes =
               { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
         "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
         "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
         "th", "st" };
                                   
                                    int dd = System.Convert.ToInt32(datetime.ToString("dd"));
                                    string prefix = suffixes[dd];
                                    drrepEvents["Date"] = datetime.ToString("dd, MMM yyyy").Replace(",", prefix);

                                }
                                dtrepEvents.Rows.Add(drrepEvents);
                                EventCount++;
                            }
                        }
                    }
                }
              }
                repEvents.DataSource = dtrepEvents;
                repEvents.DataBind();
            }
            catch (Exception exMain)
            {
                throw exMain;
            }
        }
    }

}