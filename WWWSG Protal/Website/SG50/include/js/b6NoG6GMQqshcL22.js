
	$(function() {
				$.getJSON("https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UCqQZD3jRc3jkaVwRWvBoSmw&maxResults=3&order=date&key="+ api_key +"&alt=json",function(data) {
            // + converts the string to int
            var video = data.items;
            var content = '';
            var video_album_full_description = '';
            var result = $.map(video, function(value, key) { 
				
				if (value['snippet']['description'].length >= 180) {
                    des = value['snippet']['description'].substr(0, 170) + "...";
                } else {
                    des = value['snippet']['description'];
                }
                video_album_full_description = value['snippet']['description'];
                if (des == '')
                    video_album_full_description = des = '';
                content += '<li id="'+value['id']+'">'+
                    '<div class="photo-cont">' +
                        '<img src="' + value['snippet']['thumbnails']['medium']['url'] + '"/>' +
                        '<h1>' + value['snippet']['title'] + '</h1>' +
                        '<span>' + new Date(value['snippet']['publishedAt'].substr(0, 19)).toDateString() + '</span>' +
                        "<p data-rel='"+ video_album_full_description +"'>" + des + '</p>' +
                    '</div>' +
                '</li>';
				});
            $("#YouTubeList").html(content);
			if($(window).width() > 767) {
            	$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
			}
            $(".SG50Loader").hide();
    });
    $(document).on("click", "#YouTubeList li", function (event) {
		var v_album_id = $(this).attr('id');
        localStorage.setItem('youtube_video_play_list_id',jQuery(this).attr('id'));
		var vt = $('#'+v_album_id+' h1').html();
        var vd = $('#'+v_album_id+' p').data('rel');

        localStorage.setItem('vtitle', vt);
        localStorage.setItem('vdescription', vd);
        window.location = "/SG50/GalleryLanding/GalleryVideoList/GalleryVideos.aspx";
	});
            
            $.getJSON('https://graph.facebook.com/sg2015/albums?access_token='+x2HOnsd, function (data) {
            var fcontent = '';
            var count = 0 ;
            var result = $.map(data, function (value, key) {
                for (var i in value) {
					if ( value[i]['name'] !== "Timeline Photos" &&
						 value[i]['name'] !== "Cover Photos" &&
						 value[i]['name'] !== "Profile Pictures"
					){
					count = count + 1 ;
                    description = '';
                    var full_description = '';
                    if (typeof value[i]['description'] != 'undefined') {
                        full_description = value[i]['description'];
                        if (value[i]['description'].length >= 180) {
                            description = value[i]['description'].substr(0, 170) + "...";
                        } else {
                            description = value[i]['description'];
                        }
                    } else {
                        full_description = description = '';
                    }
                    if (count < 4) {
                        fcontent += '<li  id="' + value[i]['id'] + '">' +
                           '<div class="photo-cont">' +
                              '<div class="ImageContainer"><img src="https://graph.facebook.com/' + value[i]['cover_photo'] + '/picture' + '"/></div>' +
                              '<div class="no-photos right">' + value[i]['count'] + '</div>' +
                              '<h1>' + value[i]['name'] + '</h1>' +
                              '<span>' + new Date(value[i]['updated_time'].substr(0, 19)).toDateString() + '</span>' +
                              "<p data-rel='"+ full_description +"'>" + description + '</p>' +
                          '</div>' +
                      '</li>';
                    }
                   } 
                }
            });
            $("#FaceBookList").html(fcontent);
            $(".SG50Loader").hide();
        });
    $(document).on("click", "#FaceBookList li", function (event) {
        var album_id = jQuery(this).attr('id');
        var t = $('#'+album_id+' h1').html();
        var d = $('#'+album_id+' p').data('rel');
	
        localStorage.setItem('title', t);
        localStorage.setItem('description', d);
        localStorage.setItem('facebook_album',album_id);
        window.location = "/SG50/GalleryLanding/GalleryPhotoList/GalleryPhotos.aspx";
	});

	});

