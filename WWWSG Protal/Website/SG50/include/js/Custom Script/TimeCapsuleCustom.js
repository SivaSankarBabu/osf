﻿/* Declaring Global Variables and Constant Control Id's and Class Names */

var skipCount = 0, nextCount = 4, strLoadMore = 'Load More', categoryClick = 'Cateogory Click', divIdForVote = '#divVoteForItemPopup', filterSectionDiv = '.filterSection',
    post = 'post', js = 'json', appJson = 'application/json; charset=utf-8', loadMoreDivId = '#load_more_button', ajaxPagePath = '/SG50/ajax/ajax_content.aspx/',
    catchErr = 'An Error Occured While Processing your request, Please Contact Administrator', track_click = 0, total_pages = '2', allCat = 'All', mobileSkipCount = 0,
    animImg = '.animation_image', spanItemCount = '#content_0_spanTotalNuberOfItems';

$(function () {
    $(animImg).hide();

    function FitCategories(filterSection) {
        if ($(filterSection).length > 0) {
            var catCount = $(filterSection).find('li').length;
            if (catCount == 5) {
                $(filterSection).find('li a').css({ 'padding': '12px 37px' }); $(filterSection).find('li').css({ 'margin': '0px 16px 0px 0px' });
            } else if (catCount == 6) {
                $(filterSection).find('li a').css({ 'padding': '12px 27px' }); $(filterSection).find('li').css({ 'margin': '0px 14.8px 0px 0px' });
            }
        } else { return false; }
    }

    FitCategories(filterSectionDiv);

    /* Get Selected Categories when User Selects Multiple */
    function GetSelectedCategories(ctrlId) {
        var selectedCategories = '';
        if ($(ctrlId).length > 0) {
            $(ctrlId).find('li a.current').each(function () {
                selectedCategories += selectedCategories != '' ? ',' + $(this).text() : $(this).text();
            }); return selectedCategories;
        } else { return false; }
    }

    /* Bind Time Capsule Stories in Load More Button Click */
    function BindTimeCapsuleStories(btnId, skipCount, nextLoadNoofItems, categoryType) {
        if ($(btnId).length > 0 && btnId != '' && btnId != undefined && btnId != null) {
            $(btnId).click(function () {
                if (skipCount >= 0 && nextLoadNoofItems >= 0 && categoryType != '' && categoryType != undefined && categoryType != null) {
                    $(this).hide();
                    GetTimeCapsuleItems($('.listItems ul li').length, nextLoadNoofItems, strLoadMore);
                    $(this).show();
                } else return false;
            });
        } else return false;
    }

    /* Setting Item Information in Vote for an Item */
    function GetItemInformationForVote(ctrlId, targetCtrlId) {
        if ($(ctrlId).length > 0 && $(targetCtrlId).length > 0) {
            $(ctrlId).click(function () {
                var tagName = $(this).prop("tagName"), index = '';
                index = tagName == 'A' ? $(this).parent().parent().parent().index('.boxcontainer') : $(this).parent().index('.boxcontainer');
                var parentCtrl = $('.listItems .boxcontainer:eq(' + index + ')');
                $(targetCtrlId).find('img:eq(1)').attr('src', parentCtrl.find('.thumbIcon img').attr('src'));
                $(targetCtrlId).find('#spanTitle').text(parentCtrl.find('h4').text());
                $(targetCtrlId).find('p:eq(2)').text(parentCtrl.find('p.caption').text());
                $('#spanVoteForanItemId').text(parentCtrl.find('#divItemDetails').find('#itemId').val());
            });
        }
        else { return false; }
    }

    /* Calling "GetItemInformationForVote" fn to Get and Set the Item to the vote for item div */
    GetItemInformationForVote('.listItems .grabItem a,.thumbIcon,.caption,.boxcontainer h4', divIdForVote);

    /* Bind More Items in Load More button click */
    BindTimeCapsuleStories('.LoadMoreBtn', skipCount, nextCount, strLoadMore);

    /* Bind Suggest Item Panel in Category Click 'All' */
    function BindSuggestNewDiv(catName, appendDivId, appendToDivId) {
        if (catName != '' && catName != null && catName != undefined && $(appendDivId).length > 0 && $(appendToDivId).length > 0) {
            $(appendToDivId).html('').html('<ul>' + $(appendDivId).html());
        } else return false;
    }

    /* Bind Items When Category Click */
    $(filterSectionDiv).on("click", "li a", function () {
        mobileSkipCount = 0;
        GetItemsCountAsPerCategory(GetSelectedCategories(filterSectionDiv), spanItemCount);
        var pageLoadItems = $(this).text() == 'All' ? 3 : 4;
        $(loadMoreDivId).show(); GetTimeCapsuleItems(0, pageLoadItems, categoryClick);
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $(filterSectionDiv).slideToggle("slow");
        }
    });

    /* Get Items from Sitecore Admin using Ajax Call */
    function GetTimeCapsuleItems(skipCount, nextLoadNoofItems, accessType) {
        var categoryText = GetSelectedCategories(filterSectionDiv); skipCount = categoryText == allCat ? parseInt(skipCount) - 1 : skipCount;
        if (accessType == strLoadMore) { $(animImg).show(); $('#load_more_button').hide(); setTimeout(function () { }, 2000); }
        try {
            $.ajax({
                type: post, url: ajaxPagePath + 'LoadMoreTimeCapsuleItems', data: "{'skipCount':'" + skipCount + "','nextCount':'" + nextLoadNoofItems + "','categoryType':'" + categoryText + "'}", contentType: appJson, dataType: js,
                success: function (data) {
                    if (data.d != null || data.d == '') {
                        if (accessType == strLoadMore) {
                            $('.listItems ul:first').append(data.d); HitLike('.votes a');
                        } else {
                            if (categoryText == allCat) { BindSuggestNewDiv(categoryText, '#appendDiv ul', '.listItems'); $('.listItems ul').append(data.d + '</ul>'); }
                            else { $('.listItems').html('').html('<ul>' + data.d + '</ul>'); }
                            mobileSkipCount = $('.listItems li').length; HitLike('.votes a');
                        }
                        GetSelectedCategories(filterSectionDiv) == 'All' ? LikeItem('.listItems li:gt(0) .boxcontainer') : LikeItem('.listItems li .boxcontainer'); GetItemInformationForVote('.listItems .grabItem a,.thumbIcon,.caption,.boxcontainer h4', divIdForVote); skipCount = parseInt(skipCount) + parseInt(nextLoadNoofItems);
                        if (categoryText == allCat) {
                            HideLoadMore($(spanItemCount).text(), parseInt($('.listItems ul li').length) - 1);
                        } else {
                            HideLoadMore($(spanItemCount).text(), parseInt($('.listItems ul li').length));
                        }
                    } else { $(loadMoreDivId).hide(); }
                    if (accessType == strLoadMore) { $(animImg).hide(); $('#load_more_button').show(); }
                }, failure: function () { alert("Failure"); return false; }, error: function (err) { alert(err.message); return false; }
            })
        } catch (msg) { alert(catchErr); return false; }
    }

    /* function to Like the Item */
    function HitLike(ctrlId) {
        try {
            if ($(ctrlId).length > 0) {
                $(ctrlId).each(function () {
                    $(this).click(function () {
                        var parentItemCtrl = $(this).parent(); SaveLike(parentItemCtrl);
                    });
                });
            } else return false;
        } catch (err) { alert(catchErr); return false; }
    }

    /* Save Like in Sitecore Database */
    function SaveLike(parentItem) {
        if ($(parentItem).length > 0) {
            $.ajax({
                type: 'post', url: ajaxPagePath + 'LikeItem', data: "{'itemId':'" + parentItem.parent().find('#divItemDetails #itemId').val() + "','currentNoOfLikes':'" + parentItem.find('.votecount').text() + "'}", contentType: appJson, dataType: 'json', success: function (data) {
                    if (data.d != null && data.d != '') {
                        if (data.d == true) {
                            var likesControl = parentItem.find('.votecount'), likes = likesControl.text().replace(' likes', ''); likesControl.text('').text(parseInt(likes) + 1 + ' likes');
                        }
                    } else { return false; }
                }, failure: function () { alert('failure'); return false; }, error: function (err) { alert('Error 2'); return false; }
            })
        }
        else { return false; }
    }

    /*Calling function to hit the like */
    HitLike('.votes a');

    /* Hide Load More Button if all items loaded */
    function HideLoadMore(countFromAdmin, showedItems) {
        var ctrlId = $(window).width() >= 1260 ? '.LoadMoreBtn' : '';
        if ($(ctrlId).length > 0) {
            if (countFromAdmin != null && countFromAdmin != undefined && showedItems != null && showedItems != undefined) {
                if (parseInt(countFromAdmin) == parseInt(showedItems)) $(ctrlId).hide(); else $(ctrlId).show();
            } else { return false; }
        } else { return false; }
    }

    HideLoadMore($(spanItemCount).text(), parseInt($('.listItems ul li').length) - 1);

    /* Get Items Count as per the Selected Category */
    function GetItemsCountAsPerCategory(cat, assignCtrlId) {
        try {
            if (cat != '' && cat != undefined && cat != null) {
                $.ajax({
                    type: 'post', url: ajaxPagePath + 'GetItemsCountCategory', data: "{'itemCat':'" + cat + "'}", contentType: appJson, dataType: 'json', success: function (data) {
                        if (data.d != null && data.d != '' && data.d != undefined) {
                            $(assignCtrlId).text(data.d);
                        } else return false;
                    }, failure: function () { alert('Failure'); return false; }, error: function (err) { alert('Error 3'); return false; }
                })
            } else { return false; }
        } catch (exce) { alert(exce.message); return false; }
    }

    /* Calling 'GetItemsCountAsPerCategory' function to get Item count as per the selected category */
    GetItemsCountAsPerCategory(GetSelectedCategories(filterSectionDiv), spanItemCount);

    /* Get Items in Mobile in window Scroll */
    function GetItemsinWindowScroll() {
        try {
            mobileSkipCount = $('.listItems li').length;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                $("#load_more_button").hide();
                $(window).scroll(function () {
                    var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height(), scrolltrigger = 0.95;
                    if ($(document).height() - ($(window).height() + $(window).scrollTop()) <= $('.footer').height()) {
                        setTimeout(function () { }, 4000); $(animImg).show();
                        if (parseInt($('.listItems li').length) == parseInt(mobileSkipCount)) {
                            GetItemsCountAsPerCategory(GetSelectedCategories(filterSectionDiv), '');
                            $(animImg).show(); setTimeout(function () { }, 2000);
                            GetTimeCapsuleItems(parseInt(mobileSkipCount), nextCount, strLoadMore); $(animImg).hide();
                            mobileSkipCount = $('.listItems li').length + nextCount;
                        }
                    }
                    var bindedItemsCount = GetSelectedCategories(filterSectionDiv) == allCat ? parseInt($('.listItems li').length) - 1 : parseInt($('.listItems li').length);
                    if (bindedItemsCount == parseInt($(spanItemCount).text())) { $(animImg).hide(); }
                });
            }
        } catch (err) { alert(err.message); return false; }
    }

    /* Get items in window Scroll only for mobile */
    GetItemsinWindowScroll();

    /* Like item in When Vote for an item pop up Open */
    function LikeItem(ctrlId) {
        $(ctrlId).click(function () {
            var parentItem = $(this).find('.votes a').parent(); SaveLike(parentItem);
        });
    }

    /* Calling LikeItem function to Like the item */
    LikeItem('.listItems li:gt(0) .boxcontainer');

});