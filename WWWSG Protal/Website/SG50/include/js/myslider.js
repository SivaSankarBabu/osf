//preloadImage: 'images/loading.gif',
$(function(){

	/* HOME SLIDER GALLERY */
	if(!(!(document.getElementById("mySlider"))))
	{
		var leftWidth = Number($(".mySlider .slide").width());
		var prevSlide = 1;
		var currSlide = 1;
		var slidesTotal = Number($(".slide").length);
		var sliderAnimation = true;
		var autoSlider = true;

		$("#slide1").show();
		$('.thumbSlected').hide();
		$(".thumbslider div[rel='1'] .thumbSlected").show();
		$('.thumbslider div').attr('class','nonactive');
		$('.thumbslider div:first-child').attr('class','active');

		if(autoSlider){
			setInterval(function() {
				// Do something every 5 seconds
				CallSlider();
			}, 10000);
		}
		function ResetSlider(){
			leftWidth = Number($(".mySlider .slide").width());
			for(i=1;i<=slidesTotal;i++){
				var regularExp = /\d+/;
				var slidesLeft = $("#slide"+i).css("left");
				if(slidesLeft.charAt(0) == "0"){
					$("#slide"+currSlide).css('left',0);
				}else if(slidesLeft.charAt(0) == "-"){
					$("#slide"+i).css('left',-leftWidth);
				}else{
					$("#slide"+i).css('left',leftWidth);
				}
			}
		}

		function CallSlider(){
			leftWidth = Number($(".mySlider .slide").width());
			if(sliderAnimation){
				sliderAnimation = false;
				//alert(currSlide);
				if(currSlide < slidesTotal){
					currSlide += 1;
					sliderNext();
				}else{
					currSlide = 1;
					sliderNext();
				}
				prevSlide = currSlide;
			}
		}
		$('.thumbslider div').click(function() {
			if(sliderAnimation){
				currSlide = Number($(this).attr('rel'));
				if(currSlide == prevSlide) return;
				sliderAnimation = false;
				if(currSlide>prevSlide){	
					sliderNext();
					prevSlide = currSlide;
				}else{
					sliderPrev();
					prevSlide = currSlide;
				}
			}	
		});
		function sliderNext(){
			leftWidth = Number($(".mySlider .slide").width());
			if(currSlide == prevSlide) return;
			$("#slide"+prevSlide).show();
			$("#slide"+currSlide).show();		

			$("#slide"+prevSlide).animate({"left": -leftWidth}, "slow");
			$("#slide"+currSlide).css('left',leftWidth);

			$("#slide"+currSlide).animate({"left": 0}, "slow", function(){ sliderAnimation = true;});
			
			//$("#slide"+currSlide+" .caption").animate({ bottom:10},'slow');

			$(".thumbslider div").attr('class','nonactive');
			$(".thumbslider div[rel='" + currSlide + "']").attr('class','active');

			$('.thumbSlected').hide();				
			$(".thumbslider div[rel='" + currSlide + "'] .thumbSlected").show();
		}
		function sliderPrev(){
			leftWidth = Number($(".mySlider .slide").width());
			if(currSlide == prevSlide) return;
			$("#slide"+prevSlide).show();
			$("#slide"+currSlide).show();

			$("#slide"+prevSlide).animate({"left": leftWidth}, "slow");
			$("#slide"+currSlide).css('left',-leftWidth);

			$("#slide"+currSlide).animate({"left": 0}, "slow", function(){ sliderAnimation = true;});

			

			$(".thumbslider div").attr('class','nonactive');
			$(".thumbslider div[rel='" + currSlide + "']").attr('class','active');

			$('.thumbSlected').hide();				
			$(".thumbslider div[rel='" + currSlide + "'] .thumbSlected").show();

			//$("#slide"+currSlide+" .caption").animate({ bottom:0},'slow');
			
			/*$("#slide"+currSlide).animate({ 
				left: 0
			}, 500, function() {
				// Animation complete.
				sliderAnimation = true;
				alert("COMPLETE");
			});*/
		}
		$("#next").click(function(){
			//var item1 = $('.mySliderContainer div')[0];
			//alert(item1.getAttribute("id"));
			if(sliderAnimation){
				sliderAnimation = false;
				//alert(currSlide);
				if(currSlide < slidesTotal){
					currSlide += 1;
					sliderNext();
				}else{
					currSlide = 1;
					sliderNext();
				}
				prevSlide = currSlide;
			}
		});
		$("#prev").click(function(){
			if(sliderAnimation){
				sliderAnimation = false;
				if(currSlide > 1){
					currSlide -= 1;
					sliderPrev();
				}else{
					currSlide = slidesTotal;
					sliderPrev();
				}
				prevSlide = currSlide;
			}
		});

		var winWidth;
		$(window).resize(function() {
			winWidth = $(window).width();
			if(winWidth<710){
				ResetSlider();
			}
			else if(winWidth>710 && winWidth<868){
				ResetSlider();
			}
			else if(winWidth>868){
				ResetSlider();
			}
		});
	}


	/* PORTFOLIO SLIDER GALLERY */
	if(!(!(document.getElementById("myPortfolioSlider"))))
	{
		var leftWidth = $(".myPortfolioSlider .slide").width();
		var prevSlide = 1;
		var currSlide = 1;
		var slidesTotal = Number($(".slide").length);
		var sliderAnimation = true;
		var autoSlider = true;

		$("#slide1").show();
		$('.thumbSlected').hide();
		$(".thumbslider div[rel='1'] .thumbSlected").show();
		$('.thumbslider div').attr('class','nonactive');
		$('.thumbslider div:first-child').attr('class','active');

		if(autoSlider){
			setInterval(function() {
				// Do something every 5 seconds
				CallPortFolioSlider();
			}, 5000);
		}

		function CallPortFolioSlider(){			
			if(sliderAnimation){
				sliderAnimation = false;
				//alert(currSlide);
				if(currSlide < slidesTotal){
					currSlide += 1;
					sliderPortFolioNext();
				}else{
					currSlide = 1;
					sliderPortFolioNext();
				}
				prevSlide = currSlide;
			}
		}
		$('.thumbslider div').click(function() {
			if(sliderAnimation){
				currSlide = Number($(this).attr('rel'));
				if(currSlide == prevSlide) return;
				sliderAnimation = false;
				if(currSlide>prevSlide){	
					sliderPortFolioNext();
					prevSlide = currSlide;
				}else{
					sliderPortFolioPrev();
					prevSlide = currSlide;
				}
			}	
		});
		function sliderPortFolioNext(){
			if(currSlide == prevSlide) return;
			$("#slide"+prevSlide).show();
			$("#slide"+currSlide).show();
			$("#slide"+prevSlide).animate({"left": -leftWidth}, "slow");
			$("#slide"+currSlide).css('left',leftWidth);
			$("#slide"+currSlide).animate({"left": 0}, "slow", function(){ sliderAnimation = true;});
			//$("#slide"+currSlide+" .caption").animate({ bottom:10},'slow');

			$(".thumbslider div").attr('class','nonactive');
			$(".thumbslider div[rel='" + currSlide + "']").attr('class','active');

			$('.thumbSlected').hide();				
			$(".thumbslider div[rel='" + currSlide + "'] .thumbSlected").show();
		}
		function sliderPortFolioPrev(){
			//alert(currSlide+" : "+prevSlide);
			if(currSlide == prevSlide) return;
			$("#slide"+prevSlide).show();
			$("#slide"+currSlide).show();
			$("#slide"+prevSlide).animate({"left": leftWidth}, "slow");
			$("#slide"+currSlide).css('left',-leftWidth);
			$("#slide"+currSlide).animate({"left": 0}, "slow", function(){ sliderAnimation = true;});

			$(".thumbslider div").attr('class','nonactive');
			$(".thumbslider div[rel='" + currSlide + "']").attr('class','active');

			$('.thumbSlected').hide();				
			$(".thumbslider div[rel='" + currSlide + "'] .thumbSlected").show();

			//$("#slide"+currSlide+" .caption").animate({ bottom:0},'slow');
			
			/*$("#slide"+currSlide).animate({ 
				left: 0
			}, 500, function() {
				// Animation complete.
				sliderAnimation = true;
				alert("COMPLETE");
			});*/
		}

		//Thumb Image + PREV NEXT Function

		//var thumbWidth = $(".thumbslider div").width()+10;
		var thumbWidth = $(".thumbslider div").width();
		var imageSum = $(".thumbslider div").size();

		var noOfAnim = Number(imageSum - 5);
		var animNextCounter = 0;
		var animPrevCounter = 0;
		var leftMove = -thumbWidth;
		var rightMove = thumbWidth;
		//$('#prev').css('opacity', 0.5);
		$('#prev img').attr('src', '/images/Portfolio/arrow_left_g.png');

		if(noOfAnim<=0){
			//$("#prev").hide();
			//$("#next").hide();
			$('#prev img').attr('src', '/images/Portfolio/arrow_left_g.png');
			$('#next img').attr('src', '/images/Portfolio/arrow_right_g.png');
			$(".thumbsliderDiv").css('padding-left','45px')
		}
		rotateNext = function(){
			animNextCounter+=1;
			animPrevCounter-=1;
			leftMove = -(animNextCounter*thumbWidth);
			$(".thumbslider div").animate({"left": leftMove}, "slow");
		}; 
		rotatePrev = function(){
			animPrevCounter+=1;
			animNextCounter-=1;
			rightMove = animPrevCounter*thumbWidth;
			$(".thumbslider div").animate({"left": rightMove}, "slow");
		};

		$("#next").click(function(){
			if(animNextCounter<noOfAnim){
				rotateNext();
				//$("#prev").css('opacity', 1);
				$('#prev img').attr('src', '/images/Portfolio/arrow_left.png');
			}else{
				/*animNextCounter = 0;
				animPrevCounter = 0;
				$(".thumbslider div").animate({"left": 0}, "slow");*/
				//$('#next').css('opacity', 0.5);
				$('#next img').attr('src', '/images/Portfolio/arrow_right_g.png');
			}
		});
		$("#prev").click(function(){
			if(animPrevCounter<0){
				rotatePrev();
				//$('#next').css('opacity', 1);
				$('#next img').attr('src', '/images/Portfolio/arrow_right.png');
			}else{
				/*var maxWidth = thumbWidth*noOfAnim;
				animNextCounter = 0;
				animPrevCounter = -(noOfAnim);
				$(".thumbslider div").animate({"left": -maxWidth}, "slow");*/
				//$('#prev').css('opacity', 0.5);
				$('#prev img').attr('src', '/images/Portfolio/arrow_left_g.png');
			}
		});
	}


	/* HOME BANNER FADE EFFECT GALLERY */
	if(!(!(document.getElementById("homeBannerAnimation"))))
	{
		var prevSlide = 1;
		var currSlide = 1;
		var slidesTotal = Number($(".sliderImgBig").length);
		var sliderAnimation = true;
		var autoSlider = true;
		$('.sliderImgBig').hide();
		//$("#rightAnim1").show();
		$("#leftAnim1").show();
		$('.thumbslider div:first-child').attr('class','active');

		if(autoSlider){
			setInterval(function() {
				// Do something every 5 seconds
				CallAnimation();
			}, 5000);
		}

		function CallAnimation(){			
			if(sliderAnimation){
				sliderAnimation = false;
				//alert(currSlide);
				if(currSlide < slidesTotal){
					currSlide += 1;
					animationNext();
				}else{
					currSlide = 1;
					animationNext();
				}
				prevSlide = currSlide;
			}
		}


		$('.thumbslider div').click(function() {
			if(sliderAnimation){
				currSlide = Number($(this).attr('rel'));
				if(currSlide == prevSlide) return;
				sliderAnimation = false;
				if(currSlide>prevSlide){	
					animationNext();
					prevSlide = currSlide;
				}else{
					animationPrev();
					prevSlide = currSlide;
				}
			}	
		});

		function animationNext(){
			if(currSlide == prevSlide) return;
			
			$("#leftAnim"+prevSlide).fadeOut('slow', function() {
				$("#leftAnim"+currSlide).fadeIn();
				sliderAnimation = true;
			});
			/*$("#rightAnim"+prevSlide).fadeOut('slow', function() {
				$("#rightAnim"+currSlide).fadeIn();
			});

			var currSlideLeft = currSlide;
			var prevSlideLeft = prevSlide;
			$("#leftAnim"+prevSlide).fadeOut('slow', function() {
				$("#leftAnim"+currSlide).fadeIn('slow', function() {
					$("#rightAnim"+prevSlideLeft).fadeOut('slow', function() {
						$("#rightAnim"+currSlideLeft).fadeIn();
						sliderAnimation = true;
					});
				});
			});*/

			$(".thumbslider div").attr('class','nonactive');
			$(".thumbslider div[rel='" + currSlide + "']").attr('class','active');

			$('.thumbSlected').hide();				
			$(".thumbslider div[rel='" + currSlide + "'] .thumbSlected").show();
		}
		function animationPrev(){
			if(currSlide == prevSlide) return;
			
			$("#leftAnim"+prevSlide).fadeOut('slow', function() {
				$("#leftAnim"+currSlide).fadeIn();
				sliderAnimation = true;
			});
			/*$("#rightAnim"+prevSlide).fadeOut('slow', function() {
				$("#rightAnim"+currSlide).fadeIn();
			});
			var currSlideLeft = currSlide;
			var prevSlideLeft = prevSlide;
			$("#leftAnim"+prevSlide).fadeOut('slow', function() {
				$("#leftAnim"+currSlide).fadeIn('slow', function() {
					$("#rightAnim"+prevSlideLeft).fadeOut('slow', function() {
						$("#rightAnim"+currSlideLeft).fadeIn();
						sliderAnimation = true;
					});
				});
			});*/

			
			//$("#slide"+prevSlide).animate({"left": leftWidth}, "slow");
			//$("#slide"+currSlide).css('left',-leftWidth);
			//$("#slide"+currSlide).animate({"left": 0}, "slow", function(){ sliderAnimation = true;});

			$(".thumbslider div").attr('class','nonactive');
			$(".thumbslider div[rel='" + currSlide + "']").attr('class','active');

			$('.thumbSlected').hide();				
			$(".thumbslider div[rel='" + currSlide + "'] .thumbSlected").show();
		}
	}
	
});
