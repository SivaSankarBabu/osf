var a;

function ShowMenu() {
    $('.menu').toggle();
}

//var replaceChars = ['%', '#', ';'], replacedWithChars = ['%25', '%23', '%3B'];
function BuildStr(value) {
    if (value != '' && value != null) {
        return value.replace(/%/gi, '%25').replace(/#/gi, '%23').replace(/;/gi, '%3B').replace(/"/gi, '%22')
    } else { return ''; }
}
