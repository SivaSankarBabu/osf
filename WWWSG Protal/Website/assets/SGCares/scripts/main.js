'use strict';

var SGF = window.SGF || {};
var pledgeApp;
var pledgeForm = "https://sdyrvnonwyqxmwm.form.io/sgpledge/";
//var pledgeForm = "https://lgncclfwsvrimrt.form.io/sgcare-pledge/";
var submissionData;
var stickyStatus = true;

SGF.main = {
    init: function init() {
        SGF.main.initNav();
        SGF.main.initModal();
        SGF.main.initSlider();
        SGF.main.initHomeSlider();
        SGF.main.initExpand();
        $('.section-hero').imagesLoaded(function () {
            SGF.main.initStickSocial();
            SGF.main.initSidebar();
        });
        //SGF.main.initSocialAction();
        SGF.main.initToTop();
        SGF.main.initMobileArticleHover();
        SGF.main.initCustomSelect();
        SGF.main.initMasonryLayout();
        SGF.main.initPaging();
        SGF.main.initRelated();
        SGF.main.initPartners();
        SGF.main.initPledgeCounter();
        SGF.main.initFilter();
        SGF.main.initTapCell();
        SGF.main.closeMaintenanceBar();
        SGF.main.initChangeTopMaintenanceBar();
        SGF.main.getSelectedPillars();

        var windowWidth = $(window).width();
        $(window).resize(function () {
            if (windowWidth != $(window).width()) {
                SGF.main.addToPledgeTotal();
                SGF.main.initChangeTopMaintenanceBar();
                if ($('.storyCell').length) {
                    $('.storyCell').removeClass('active');
                }
                SGF.main.initTapCell();
            }
        });

        if (!$('.plege-hero').length && !$('.pledge-sticky').hasClass('showed')) {
            $('.pledge-sticky').addClass('active');
        }
        $(window).on('scroll', function () {
            if ($('.plege-hero').length && $('.section-hero').height() < $(window).scrollTop() + 300 && !$('.pledge-sticky').hasClass('showed')) {
                $('.pledge-sticky').addClass('active');
            }
        });
    },

    initChangeTopMaintenanceBar: function initChangeTopMaintenanceBar() {
        if ($('.maintenance').length) {
            var h_wt = $('header .wrap-top').height();
            $('.maintenance').css('top', h_wt);
        }
    },
    initTapCell: function initTapCell() {

        var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
        if (iOS) {
            $('.pledge-sticky .close').hover(function () {
                $('.pledge-sticky').removeClass('active');
                $('.pledge-sticky').addClass('showed');
            });
        };
        $('.pledge-sticky .close').on('click', function () {
            $('.pledge-sticky').removeClass('active');
            $('.pledge-sticky').addClass('showed');
        });
        var vp = $(window).width();
        if (vp < 1025) {
            $('.storyCell').on('click', function () {
                if (!$(this).hasClass('active')) {
                    $('.storyCell').removeClass('active');
                    $(this).addClass('active');
                }
            });
        }
    },
    initRelated: function initRelated() {
        if ($('.block-relate').length) {
            $.each($('.block-relate .item__info'), function (i, v) {
                $(v).css({ height: $(v).siblings('.item__img').height() });
            });

            $(window).on('resize', function () {
                $.each($('.block-relate .item__info'), function (i, v) {
                    $(v).css({ height: $(v).siblings('.item__img').height() });
                });
            });
        }
    },

    initSidebar: function initSidebar() {
        if ($('.side-bar').length) {
            var setSidebar = function setSidebar() {
                var breakpoint = 767;
                var width = $(window).width();
                var sidebar = $('.side-bar');
                if (width > breakpoint) {
                    var neo = $('.article-content').parent();
                    var currentTop = $(window).scrollTop() + $('#page-header').height();
                    if (currentTop > neo.offset().top) {
                        if (sidebar.not('.sticky')) {
                            sidebar.addClass('sticky');
                        }
                        var height = sidebar.height();
                        // var height = $(window).height();
                        // var gap = $(window).scrollTop() + height - (neo.offset().top + neo.height());
                        // var gap = $(window).scrollTop() + height - $('#page-footer').offset().top + 20;
                        var gap = $(window).scrollTop() + height - $('#page-footer').offset().top + 100;
                        if (gap > 0) {
                            sidebar.css({ top: -(gap - 70) });
                        } else {
                            sidebar.css({ top: '' });
                        }
                    } else {
                        sidebar.removeClass('sticky').css({ top: '' });
                    }
                } else {
                    sidebar.removeClass('sticky').css({ top: '' });
                }
            };

            setSidebar();

            $(window).on('resize', function () {
                setSidebar();
            });

            $(window).on('scroll', function () {
                setSidebar();
            });
        }
    },

    initPartners: function initPartners() {
        if ($('.section-partners').length && $('.btn-toggle').length) {
            $('.section-partners').on('click', '.btn-toggle', function (e) {
                e.preventDefault();
                var wrap = $('.wrap-collapse-content');
                var height = wrap.find('.inner')[0].clientHeight;
                var isExpand = $(this).is('.expand');

                // Tween
                var to = isExpand ? height : 0;
                wrap.css({ height: to });
                $('.btn-toggle').toggleClass('active');
            });

            $(window).on('resize', function () {
                var wrap = $('.wrap-collapse-content');
                if (wrap.height() > 0) {
                    var height = wrap.find('.inner')[0].clientHeight;
                    wrap.css({ height: height });
                }
            });
        }
    },

    initExpand: function initExpand() {
        $('.expand').on('click', function (e) {
            e.preventDefault();
            $('.content--most-recent.collapse').toggle();
        });

        if ($('.section-events-calendar').length) {
            $('.section-events-calendar').on('click', '.heading a', function (e) {
                if ($(this).is('.collapsed')) {
                    $('.content--most-recent').find('.active').removeClass('active');
                } else {
                    $('.content--most-recent').find('li').first().addClass('active');
                }
            });
        }
    },

    initNav: function initNav() {
        $('.icon-nav').off('click').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $('.wrap-nav').toggleClass('active');
            $('.nav-fade').toggleClass('show');
        });

        $('.nav-fade').off('click').on('click', function () {
            if ($('.icon-nav').is('.active')) {
                $('.icon-nav').removeClass('active');
                $('.wrap-nav').removeClass('active');
                $(this).removeClass('show');
            }
        });

        $('.share-box-link').on('click', function (e) {
            e.preventDefault();
            $('.shareCon').toggle();
        });

        $(window).resize(function () {
            if ($('.icon-nav').is('.active')) {
                $('.icon-nav').removeClass('active');
                $('.wrap-nav').removeClass('active');
                $('.nav-fade').toggleClass('show');
            }
        });

        $(window).on('scroll', function () {
            if ($('.icon-nav').is('.active')) {
                $('.icon-nav').removeClass('active');
                $('.wrap-nav').removeClass('active');
                $('.nav-fade').toggleClass('show');
            }
        });
    },

    initModal: function initModal() {

        if ($('.button-watch-video').length) {
            $('.button-watch-video').off('click').on('click', function (e) {
                e.preventDefault();
                var target = $(this).attr('href');
                $(target).modal();
                var iframe = $(target).find('iframe');
                iframe.attr('src', iframe.data('src'));

                $(target).off('hidden.bs.modal').on('hidden.bs.modal', function () {
                    iframe.attr('src', '');
                });
            });
        }

        if ($('.play').length) {
            $('.play').off('click').on('click', function (e) {
                e.preventDefault();
                var target = $(this).attr('href'), srcIframe = $(this).attr('data-src'),
                    gsText = $(this).attr('data-gs-cta'), gs_url = $(this).attr('data-gs-url'), gs_Videoname = $(this).attr('data-gs-vtext');

                var iframe = $(target).find('iframe'), palyIndex = $(this).parent().index('.video-play-wrapper');

                iframe.attr('src', srcIframe);

                if (palyIndex == 0 || palyIndex == 1 || palyIndex == 2 || palyIndex == 3 || palyIndex == 4) {
                    onYouTubeIframeAPIReady('player', gs_Videoname, srcIframe.split('/')[4]);
                } else {
                    console.log('Invalid Play Index :: ' + palyIndex)
                }

                $(target).modal();

                var desCtrl = $(target);
                if (gsText != null && gsText != '' && gs_url != "javascript:void(0)") {

                    desCtrl.find('.get-started:eq(0),.get-started-need').show();
                    desCtrl.find('.get-started a,.get-started-need a').attr('href', gs_url).text(gsText);


                    desCtrl.find('.get-started a').off('click').on('click', function () {
                        e.preventDefault();
                        ga('send', 'event', { eventCategory: 'SGCares Homepage', eventAction: 'Get started', eventLabel: gs_Videoname + ' video' })
                    })
                } else {
                    desCtrl.find('.get-started:eq(0),.get-started-need').hide();
                }

                desCtrl.find('.button__wrapper:eq(1)').off('click').on('click', function () {
                    e.preventDefault();
                    ga('send', 'event', { eventCategory: 'SGCares Homepage', eventAction: 'Close', eventLabel: gs_Videoname + ' video' })
                })

                $(target).off('hidden.bs.modal').on('hidden.bs.modal', function () {
                    iframe.attr('src', '');
                });
            });
        }
    },

    initSlider: function initSlider() {
        if ($('.list-quote').length) {
            if ($('.list-quote:visible').length && $('.list-quote:visible .item-quote').length > 1) {
                var bxQuote = $('.list-quote:visible').bxSlider({
                    controls: false, easing: 'ease-in-out', auto: true, pause: 5500
                });

                $('.list-quote:visible').data('slider', bxQuote);
                $('.list-quote .item-link').off('click').on('click', function (e) {
                    e.preventDefault();
                    if (!bxQuote) {
                        bxQuote = $('.list-quote:visible').data('slider');
                    }
                    if (bxQuote) {
                        bxQuote.goToNextSlide();
                    }
                });
            }
        }

        if ($('.list-article-thumb:visible').length && $('.list-article-thumb:visible .item').length > 1) {
            $('.list-article-thumb:visible').bxSlider({
                adaptiveHeight: true
            });
        }

        if ($('.list-project').length) {
            var ww = $(window).width();
            var ms = ww > 767 ? 3 : 1;
            var sw = Math.round(ww / ms);
            $('.list-project').bxSlider({
                maxSlides: ms, slideWidth: sw, moveSlides: 1, infiniteLoop: true, pager: false
            });
        }

        if ($('.list-img li').length) {
            $('.list-img').bxSlider({
                maxSlides: 1, moveSlides: 1, infiniteLoop: true, pager: $('.list-img li').length > 1 ? true : false, controls: false, adaptiveHeight: true,
            });
        }
    },

    initHomeSlider: function initHomeSlider() {
        if ($('.mobile-list').length && $('.news--three-col').length) {
            var topSlide = function topSlide() {
                var slide;
                if ($(window).width() < 768) {
                    $('.content.content--expand-collapse').addClass('in').css({ height: '' }).attr('aria-expanded', 'true');
                    $('.button--read-more').removeClass('collapsed').attr('aria-expanded', 'true');
                    slide = $('.mobile-list:visible').bxSlider({
                        pager: false,
                        adaptiveHeight: true
                    });
                    $('.mobile-list:visible').data('slide', slide);
                } else {
                    slide = $('.mobile-list:visible').data('slide');
                    if (slide) {
                        slide.destroySlider();
                        $('.mobile-list:visible').data('slide', null);
                    }
                    //
                    if ($('.content--expand-collapse').is('.in')) {
                        $('.button--read-more').parent().hide();
                        $('.button--read-more').parents('.content--expand-collapse').find('.button__wrapper').show();
                    }
                }
            };

            var bottomSlide = function bottomSlide() {
                var slide;
                if ($(window).width() < 768) {
                    slide = $('.news--three-col .content').bxSlider({
                        pager: false,
                        adaptiveHeight: true
                    });
                    $('.news--three-col .content').data('slide', slide);
                } else {
                    slide = $('.news--three-col .content').data('slide');
                    if (slide) {
                        slide.destroySlider();
                        $('.news--three-col .content').data('slide', null);
                    }
                }
            };
            // topSlide();


            bottomSlide();
            $('.button--read-more').on('click', function (e) {
                $('.button--read-more').parent().show();
                $(this).parent().hide();
            });

            //
            $(window).on('resize', function () {
                // topSlide();
                bottomSlide();
            });
        }
    },

    initStickSocial: function initStickSocial() {
        if ($('.block-social').length) {
            var obj = $('.block-social');
            var neo = $('.article-content');
            var top = neo.offset().top;
            var left = neo.offset().left - 50;
            var gap = $('#page-header').height();
            left = $(window).width() > 767 ? left : 0;

            $(window).on('scroll', function () {
                var wintop = $(window).scrollTop() + gap;

                if (wintop > top && obj.not('.sticky')) {
                    obj.addClass('sticky');
                    obj.css({ left: left, top: gap });
                } else {
                    obj.removeClass('sticky');
                    obj.css({ left: '', top: '' });
                }

                if (wintop > neo && obj.not('out')) {
                    obj.addClass('out');
                    obj.fadeOut();
                } else {
                    obj.removeClass('out');
                    obj.fadeIn();
                }
            });

            $(window).on('resize', function () {
                if (window.callback) {
                    clearTimeout(window.callback);
                }
                window.callback = setTimeout(function () {
                    SGF.main.initStickSocial();

                    // trigger calc now
                    top = neo.offset().top;
                    left = neo.offset().left - 65;
                    gap = $('#page-header').height();
                    left = $(window).width() > 767 ? left : 0;
                    var wintop = $(window).scrollTop() + gap;
                    if (wintop > top && obj.not('sticky')) {
                        obj.addClass('sticky');
                        obj.css({ left: left, top: gap });
                    } else {
                        obj.removeClass('sticky');
                        obj.css({ left: '', top: '' });
                    }
                }, 200);
            });
        }
    },

    initMobileArticleHover: function initMobileArticleHover() {
        if ($(window).width() < 992 && $('.block-projects').length) {
            $('.block-projects .project-item').on('touchstart', function (e) {
                $('.block-projects .project-item').removeClass('hover');
                $(this).addClass('hover');
                e.stopPropagation();
            });
            $(document).on('touchstart', function () {
                $('.block-projects .project-item').removeClass('hover');
            });
        }
    },

    initSocialAction: function initSocialAction() {
        if ($('.block-social').length) {
            $('.icon-facebook').off('click').on('click', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                url = url.replace(/\[URL\]/g, window.location.href);
                window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=320,width=480');
            });
        }
    },

    initToTop: function initToTop() {
        if ($('.btn-back-top').length) {
            $('.btn-back-top').off('click').on('click', function (e) {
                e.preventDefault();
                $('body,html').animate({ scrollTop: 0 }, 500);
            });

            $(window).on('scroll', function () {
                var scrollTop = $(window).scrollTop();
                var height = window.innerHeight;
                var position = scrollTop + height;
                var limitTop = $('#page-footer').offset().top;

                if (position > (height * 1.2) && position < limitTop) {
                    $('.btn-back-top').css({ 'position': 'fixed' });
                } else {
                    $('.btn-back-top').css({ 'position': '' });
                }
            });
        }
    },

    initCustomSelect: function initCustomSelect() {
        $('select').niceSelect();
    },

    initMasonryLayout: function initMasonryLayout() {
        var width = $('.news--grid .grid').width() / 3 - 100;
        var $grid = $('.grid').masonry({
            // options
            columnWidth: '.grid-sizer',
            itemSelector: '.grid-item'
        });

        // layout Masonry after each image loads
        $grid.imagesLoaded().progress(function () {
            $grid.masonry('layout');
        });
    },

    initPaging: function initPaging() {
        if ($('.wrap-paging').length) {
            var position = $('.section-hero').height() + $('.section-intro--3col').height();

            $('.wrap-paging').on('click', '.page-link', function (e) {

                /* By Suryam */
                var page_num = $(this).data('pager');

                e.preventDefault();
                if ($(this).is('.prev') || $(this).is('.next')) {
                    var wrap = $('.wrap-paging .wrap-inner');
                    var left = parseInt(wrap.attr('data-left')) || 0;
                    var width = wrap[0].scrollWidth;
                    var move = wrap.parent().width();
                    left = left + ($(this).is('.next') ? -move : move);
                    left = left > 0 ? 0 : left < -(width - move) ? -(width - move) : left;
                    wrap.css({ left: left });
                    wrap.attr('data-left', left);
                    return;
                }
                if (!$(this).is('.active')) {
                    $(this).addClass('active').siblings('.active').removeClass('active');
                }

                $('.storiesMain.clearfix').hide();
                $('.storiesMain.clearfix[data-page=' + page_num + ']').show();

                if ($(this).data('page') == 'new-page')
                    $('body,html').animate({ scrollTop: position }, 1000);
                else { }
            });
        }
    },

    initPledgeCounter: function initPledgeCounter() {
        var selectedPillarCount;

        SGF.main.addToPledgeTotal();

        $('.btn-pledge').on('click touchend', function (e) {
            e.preventDefault();
            selectedPillarCount = $(".wrap-pledge-options ul li.active").length;
            var value = SGF.main.getSelectedPillars();
            ga('send', 'event', { eventCategory: 'SGCares Homepage', eventAction: 'Pledge now', eventLabel: value });
            if (selectedPillarCount != 0) {
                try {

                    var ajaxUrl = '/SGCare/SGCaresAjax.aspx/UpdateCount';
                    $.ajax({
                        type: "post", url: ajaxUrl, dataType: "json", contentType: 'application/json; charset=utf-8', cache: false,
                        success: function (data) {
                            $('#pledge-count').val((parseInt(data.d)));
                            SGF.main.addToPledgeTotal(1);

                        }, failure: function (fail) { console.log(fail); return fail; }, error: function (err) { console.log(err); return false; }
                    });
                } catch (msg) { console.log('Error occured while calling UpdateCount method in main.js file.'); return false; }

            } else {
                console.log('Please select a pillar');
            }
        });

        $('.pledge-form a').on('click', function (e) {
            e.preventDefault();
            $.colorbox.close();
        });

        /* Pledge selection */
        $('.wrap-pledge .wrap-pledge-options ul li').on('click', function (e) {
            $(this).toggleClass('active');
        });

        /*var clock;
        clock = $('.pledge-counter').FlipClock(2168970, {
            clockFace: 'Counter',
            autoStart: false
         });*/
    },

    addToPledgeTotal: function addToPledgeTotal(value) {
        var screenWidth = $(window).width();

        var filter = /^[0-9-+]+$/;
        var offline = '';
        if ($('#init-value').val() != '' && $('#init-value').val() != null && $('#init-value').val() != undefined && filter.test($('#init-value').val())) {
            offline = $('#init-value').val()
        }
        else {
            offline = 0;
        }
        
        var online = '1';
        if ($('#pledge-count').val() != '' && $('#pledge-count').val() != null && $('#pledge-count').val() != undefined) {
            online = $('#pledge-count').val()
        }
        else {
            online = 1;
        }

        var param, finalValue = (parseInt(offline) + parseInt(online)).toString();
        //var param, finalValue = (parseInt($('#init-value').val()) + parseInt($('#pledge-count').val())).toString();

        if (screenWidth < 1100) {
            var ratio = 0.48;

            param = {
                image: '/assets/SGCares/images/nums.png', text: ("0000000" + finalValue).slice(-7),
                charsMap: '0123456789', charWidth: 85 * ratio,
                charHeight: 112 * ratio, imageSize: 850 * ratio + 'px ' + 112 * ratio + 'px', speed: 20, speedVariation: 20,
                onComplete: function onComplete() {
                    SGF.main.callPledgeOverlay(value);
                }
            };
        } else {
            param = {
                image: '/assets/SGCares/images/nums.png', text: ("0000000" + finalValue).slice(-7),
                charsMap: '0123456789', charWidth: 85, charHeight: 112,
                speed: 10, speedVariation: 10,
                onComplete: function onComplete() {
                    SGF.main.callPledgeOverlay(value);
                }
            };
        }

        $('.pledge-counter').splitFlap(param);
    },

    callPledgeOverlay: function callPledgeOverlay(value) {
        if (value !== undefined) {
            $(".btn-pledge").colorbox({
                inline: true, maxWidth: "100%", overlayClose: false, escKey: false, open: true,
                onClosed: function onClosed() {
                    $.colorbox.remove();
                    SGF.main.clearPledgeForm();
                    $('#error-sms').addClass('hidden');
                },
                onComplete: function onComplete() {
                    $('#frmPledge').on('touchend', function () { });
                }
            });

            /* ParsleyJS */
            $('#frmPledge').parsley().on('field:validated', function () {
                if ($('#frmPledge').find('.parsley-error').length !== 0) {
                    $('#error-sms').removeClass('hidden');
                } else {
                    $('#error-sms').addClass('hidden');
                }
            }).on('field:validated', function () {
                $.colorbox.resize();
            }).on('form:validate', function () {
                $('#error-sms').addClass('hidden');
            }).on('form:submit', function () {
                var selectedPillars = SGF.main.getSelectedPillars();
                ga('send', 'event', { eventCategory: 'SGCares Homepage', eventAction: 'Submit', eventLabel: selectedPillars });
                SGF.main.getPledge();
                return false;
            });
        }
    },

    initFilter: function initFilter() {
        $('.block-filter a').on('click', function (e) {
            e.preventDefault();
            $(".block-filter-wrapper").slideToggle();
        });

        $('.block-filter-body a').on('click', function (e) {
            e.preventDefault();
            $(".block-filter-wrapper").hide();
        });
    },

    initPledgeSticky: function initPledgeSticky() {
        if ($('.pledge-sticky').length) {
            $('.pledge-sticky a').off('click').on('click', function (e) {
                e.preventDefault();
                $('body,html').animate({ scrollTop: 0 }, 500);
            });
            $(window).on('scroll', function () {
                var scrollTop = $(window).scrollTop();
                var height = $(window).height();
                var position = scrollTop + height;
                var limitTop = $('#page-footer').offset().top;

                $('.pledge-sticky').css({ 'position': 'fixed' });

                if (stickyStatus == true) {
                    if (position > height * 1.8) {
                        $('.pledge-sticky').slideDown();
                    } else {
                        $('.pledge-sticky').slideUp();
                    }
                }
            });

            $('.pledge-sticky span').on('click', function () {
                stickyStatus = false;
                $('.pledge-sticky').slideUp();
            });
        }
    },

    formIOAPISettingsPOST: function formIOAPISettingsPOST(apiFunction, dataPayload) {
        var settings = {
            async: true, crossDomain: true, url: pledgeForm + apiFunction, method: "POST", headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }, processData: false, data: dataPayload
        };

        return settings;
    },

    formIOAPISettingsPUT: function formIOAPISettingsPUT(apiFunction, dataPayload, entryID) {
        var settings = {
            async: true, crossDomain: true, url: pledgeForm + apiFunction + "/" + entryID, method: "PUT", headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }, processData: false, data: dataPayload
        };

        return settings;
    },

    formIOAPISettingsGET: function formIOAPISettingsGET(apiFunction, dataPayload) {
        var settings = {
            async: true, crossDomain: true, url: pledgeForm + apiFunction + dataPayload, method: "GET"
        };

        return settings;
    },

    getPledgeData: function getPledgeData(param) {
        var data, pledgeData;
        var pillars = "";
        var tickBox = $('#chkAgree').prop('checked') == true ? true : false;

        if (param == 'email') {
            data = '?limit=1&data.txtEmail=' + $('#txtEmail').val();
        } else if (param == null) {
            $(".wrap-pledge-options ul li").each(function () {
                if ($(this).hasClass('active')) {
                    pillars += ' "hd' + $(this).attr('data-pledge-type') + '": "Yes",';
                } else {
                    if (submissionData == undefined) {
                        pledgeData = "No";
                    } else {
                        pledgeData = submissionData['hd' + $(this).attr('data-pledge-type')];
                    }
                    pillars += ' "hd' + $(this).attr('data-pledge-type') + '": "' + pledgeData + '",';
                }
            });

            data = '{"data": {"txtName": "' + $('#txtName').val() + '", "txtEmail": "' + $('#txtEmail').val() + '",' + pillars + ' "chkAgree": ' + tickBox + '}}';
        } else {
            data = "/" + param;
        }
        return data;
    },

    addPledge: function addPledge() {
        $.ajax(SGF.main.formIOAPISettingsPOST('submission', SGF.main.getPledgeData())).done(function (response) {
            SGF.main.clearPledgeForm();
        });
    },

    updatePledge: function updatePledge(entryID) {
        $.ajax(SGF.main.formIOAPISettingsPUT('submission', SGF.main.getPledgeData(), entryID)).done(function (response) {
            SGF.main.clearPledgeForm();
        });
    },

    getPledge: function getPledge() {
        $.ajax(SGF.main.formIOAPISettingsGET('submission', SGF.main.getPledgeData('email'))).done(function (response) {
            if (response.length > 0) {
                $.ajax(SGF.main.formIOAPISettingsGET('submission', SGF.main.getPledgeData(response[0]._id))).done(function (response2) {
                    submissionData = response2.data;
                    SGF.main.updatePledge(response[0]._id);
                });
            } else {
                SGF.main.addPledge();
            }
        });
    },

    clearPledgeForm: function clearPledgeForm() {
        $(".wrap-pledge-options ul li").removeClass('active');
        $("#frmPledge")[0].reset();
        $('#frmPledge').parsley().reset();
        // $('#error-sms').html('');
        $.colorbox.close();
    },

    closeMaintenanceBar: function closeMaintenanceBar() {
        $(".maintenance span i.fa-close").on('click', function () {
            $(".maintenance").hide();
        });
    },

    getSelectedPillars: function () {
        var ctrl = $(".wrap-pledge-options ul li.active"), pillarStr = '';
        if (ctrl.length) {
            pillarStr = '';
            $(ctrl).each(function () {
                pillarStr += pillarStr == '' ? $(this).find('.opt-desc').text() : ', ' + $(this).find('.opt-desc').text();
            });
            return pillarStr;
        }
    }
};

$(function () {
    SGF.main.init();


});
//# sourceMappingURL=main.js.map
