﻿// This code loads the IFrame Player API code asynchronously
var tag = document.createElement('script');
tag.src = "http://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// This code is called by the YouTube API to create the player object
function onYouTubeIframeAPIReady(event) {
    player = new YT.Player('youTubePlayer', {
        events: {
           
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

var pauseFlag = false;
function onPlayerReady(event) {
    // do nothing, no tracking needed
}
function onPlayerStateChange(event) {
    // track when user clicks to Play
    if (event.data == YT.PlayerState.PLAYING) {
        ga('send', 'event', 'SGCares Homepage', 'Play', 'top banner video');
        alert('play');
        pauseFlag = true;
    }
    // track when user clicks to Pause
    if (event.data == YT.PlayerState.PAUSED && pauseFlag) {
        ga('send', 'event', ' SGCares Homepage', 'Pause', 'top banner video');
        alert('pause');
        pauseFlag = false;
    }
    // track when video ends
    if (event.data == YT.PlayerState.ENDED) {
        ga('send', 'event', ' SGCares Homepage', 'Finished', 'top banner video');
        alert('end');
    }
}