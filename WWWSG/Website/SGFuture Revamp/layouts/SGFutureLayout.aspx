﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" %>

<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>SGfuture</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="vs_defaultClientScript" content="JavaScript" />

    <sc:Sublayout runat="server" RenderingID="{D1626E4E-96C0-418C-9D05-09E74ED23BC9}"
        Path="/SGFuture Revamp/sublayouts/MetaTagsSublayout.ascx" ID="sublayoutPGMetaTag"
        placeholder="contentHeader"></sc:Sublayout>

    <sc:VisitorIdentification runat="server" />

    <script src="/SGFuture Revamp/assets/scripts/jquery-3.4.0.js" type="text/javascript"></script>

    <!--<link rel="stylesheet" href="/SGFuture Revamp/assets/styles/vendor.css" />-->
    <link rel="stylesheet" href="/SGFuture Revamp/assets/styles/main.css" />

    <script type="text/javascript">

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-71697861-1', 'auto');
        ga('send', 'pageview');

    </script>


    <!-- Google Tag Manager -->
    <script type="text/javascript">(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-T6B92M');</script>
    <!-- End Google Tag Manager -->

</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T6B92M"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <form method="post" runat="server" id="mainform">
        <sc:Placeholder ID="scHeader" runat="server" Key="Header" />
        <sc:Placeholder ID="plhContent" runat="server" Key="Content" />
        <sc:Placeholder ID="scFooter" runat="server" Key="Footer" />
    </form>

    <script src="/SGFuture Revamp/assets/scripts/vendor.js" type="text/javascript"></script>
    <script src="/SGFuture Revamp/assets/scripts/plugins.js" type="text/javascript"></script>
    <script src="/SGFuture Revamp/assets/scripts/main.js" type="text/javascript"></script>

</body>
</html>
