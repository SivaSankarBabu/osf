﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectShowcaseDetailsSublayout.ascx.cs" Inherits="sgfuture.SGFuture_Revamp.sublayouts.ProjectShowcaseDetailsSublayout" %>
<main class="page page-article">
			<div class="container-fluid">
				<!-- begin section hero -->
				<section class="section-hero">
					<div class="wrap-hero">
                        <sc:Image ID="imgBanner" runat="server" CssClass="bg-thumb hidden-xs DskImg" Field="Project Desktop Image" />
                        <sc:Image ID="Image1" runat="server" CssClass="bg-thumb visible-xs MbImg" Field="Project Mobile Image" />
						<div class="container">
							<div class="row">
								<div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-6">
									<div class="hero-info">
										<h2 class="hero-title">
                                            <sc:Text ID="txtBannerText" runat="server" Field="Project Title" />											
										</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- end section hero -->

				<!-- begin section main -->
				<section class="section-main">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-push-1">
								<div class="block-social">
									<a id="shareFacebook" class="icon icon-facebook">facebook</a>
                                     <input type="hidden" runat="server" id="ProjectName" />
									<a id="mailDesktop" class="icon icon-mail" href="javascript:void(0)">mail</a>
									<a id="whatsapp" class ="icon icon-whatsapp" href="whatsapp://send?text=The text to share!" data-action="share/whatsapp/share">Share via Whatsapp</a>
								</div>
								<div class="article-info article-info-social">
									<h2 class="project-owner"><sc:Text ID="txtProjectNum" runat="server" Field="Project Number" /></h2>
									<div class="author-info info">
										<span class="head">PROJECT OWNER</span>
										<span class="name"><sc:Text ID="txtAuthor" runat="server" Field="Author" /></span>
									</div>
									<div class="project-info info" style="display:none !important">
										<span class="head">project name</span>
										<span class="desc"><sc:Text ID="txtTitle" runat="server" Field="Project Title" /></span>
									</div> 
								</div>
								<article class="article-content"><sc:Text ID="txtTopDesc" runat="server" Field="Top Description" /></article>
							</div>
						</div>
					</div>
					<div class="block-article-thumb">
						<div class="wrap-content">
							<ul class="list-article-thumb" id="Slideshow" runat="server"></ul>
						</div>
					</div>
					<div class="container hidden-xs" runat="server" id="divBottomDes">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-push-1">
								<article class="article-content">
									<sc:Text ID="txtBtmDesc" runat="server" Field="Bottom Description" />
								</article>
							</div>
						</div>
					</div>
				</section>
				<!-- end section main -->

				<!-- begin section quotes -->
				<section class="section-quote">
					<!-- begin block quotes -->
					<div class="block-quotes" runat="server"  id="divQuote">
						<div class="container">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-10 col-md-push-1">
									<div class="wrap-quote">
                                        <ul class="list-quote">
										 <asp:Repeater ID="rptQuotes" runat="server">
                                                <ItemTemplate>
											<li class="item-quote">
												<a class="item-link" href="#">
													<%--<figure class="avatar">
                                                        <img class="thumb" src='<%#DataBinder.Eval(Container.DataItem,"QuoteImage") %>' />
													</figure>--%>
													<figcaption class="info">
														<span class="author-name">
                                                            <asp:Label ID="lblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QuoteName") %>'></asp:Label>
														</span>
														<span class="author-title">
                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QuoteDesignation") %>'></asp:Label>
														</span>
														<p class="quote">
                                                            <asp:Label ID="lblQuotation" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation") %>'></asp:Label>
														</p>
                                                        
													</figcaption>
												</a>
											</li>
                                            </ItemTemplate>
                                            </asp:Repeater>	
                                            </ul>		
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end block quotes -->
				</section>
				<!-- end section quotes -->

				<!-- begin section idea -->
				<section class="section-idea">
					<div class="block-idea">
						<div class="line"></div>
						<div class="inner">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-8 col-md-7">	
                                        <h2 class="title">									
									        <sc:Text ID="txtIdeaTitle" runat="server" Field="Idea Title" />
                                        </h2>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-5">
										<div class="wrap-link">
											<span class="link-desc"><sc:Text ID="txtLink" runat="server" Field="Link Text" /></span>
											<%--<a class="link" href="#"></a>--%>
                                            <asp:HyperLink ID="lnkUrl"  runat="server" Target="_blank" CssClass="link"></asp:HyperLink>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<sc:Text ID="txtIdeaDesc" runat="server" Field="Idea Description" />
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 text-center">
                                        <%if (!String.IsNullOrEmpty(Sitecore.Context.Item["Get Started Text"]))
                                          { %>
										<div class="wrap-button button-blue">
											<span class="border-ver left"></span>
											<span class="border-ver right"></span>
											<span class="border-hoz top"></span>
											<span class="border-hoz bottom"></span>                                            									
                                            <asp:HyperLink ID="lnkGetStarted" runat="server" CssClass="button button-start" Target="_blank" onClick="ga('send', 'event', { eventCategory: 'Article', eventAction: 'Click', eventLabel: 'Get started'});">
                                                <sc:Text ID="txtGetStarted" runat="server" Field="Get Started Text" /></asp:HyperLink>
										</div><%} %>
                                         <%if (!String.IsNullOrEmpty(Sitecore.Context.Item["Be a Volunteer Text"]))
                                           { %>
										<div class="wrap-button button-blue">
											<span class="border-ver left"></span>
											<span class="border-ver right"></span>
											<span class="border-hoz top"></span>
											<span class="border-hoz bottom"></span>
                                                <asp:HyperLink ID="lnkVolunter" runat="server" CssClass="button button-volunteer" Target="_blank" onClick="ga('send', 'event', { eventCategory: 'Article', eventAction: 'Click', eventLabel: 'Be a volunteer'});">
                                                    <sc:Text ID="Text1" runat="server" Field="Be a Volunteer Text" /></asp:HyperLink>
										</div><%} %>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- end section idea -->
                		
                <!-- begin section more project -->
				<section class="section-more-project page-black text-center">
					<h2 class="title">more inspiring projects</h2>
					<div class="wrap-more-project">
						<ul class="list-project">
                            <asp:Repeater ID="rptInspiringProjects" runat="server">
                                <ItemTemplate>
							<li class="project-item" id="pj1">
								<article class="project">
									<a href='<%#DataBinder.Eval(Container.DataItem,"href") %>'>
										<figure class="wrap-thumb">
											<img class="thumb" src='<%# DataBinder.Eval(Container.DataItem,"BgImage") %>' />
                                            <input type="hidden" class="ItemName" value="<%#DataBinder.Eval(Container.DataItem,"ItemName") %>" />
										</figure>
										<div class="fade"></div>
										<figcaption class="wrap-info">
											<span class="project-date"><%# DataBinder.Eval(Container.DataItem,"Date") %> </span>
											<h2 class="project-title"><%# DataBinder.Eval(Container.DataItem,"ProjectTitle") %> </h2>
										</figcaption>
                                       <%-- <div class="wrap-line">
										<div class="line line-ver"></div>
										<div class="line line-hoz"></div>
										<div class="cross cross-top"></div>
										<div class="cross cross-bottom"></div>
										<div class="cross-bg"></div>
									</div>--%>
									</a>
								</article>
							</li>				
                            </ItemTemplate>
                            </asp:Repeater>		
						</ul>
					</div>
				</section>
				<!-- end section more project -->	                
			</div>
   <a class="btn-back-top" href="javascrip:;"></a>
		</main>
<div class="SocialShare">
   <%-- <asp:HiddenField ID="hdnSocialShareTitle" ClientIDMode="Static" runat="server" />--%>
    <div id="divSocialSahreDescription" style="display: none" runat="server" clientidmode="Static">
    </div>
  <%--  <asp:HiddenField ID="hdnSocialShareImage" runat="server" ClientIDMode="Static" />--%>
    <%--<asp:HiddenField ID="hdnHostName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnEventURL" runat="server" ClientIDMode="Static" />--%>
</div>

<script type="text/javascript">

    $('#whatsapp').attr('href', 'whatsapp://send?text=' + encodeURIComponent($("meta[property='og:title']").attr('content')) + ", " + encodeURIComponent(window.location.toString()) + '');
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";

        window.fbAsyncInit = function () {
            FB.init({
                appId: FBID, status: true, cookie: true,
                version: 'v2.2'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function FBShare(Title, Description, Img, HostName) {

            var fbtitle = Title, fbimg = Img, fbdes = unescape(Description),
             obj = {
                 method: 'feed', link: HostName, picture: fbimg, name: fbtitle, description: fbdes
             };
            FB.ui(obj);
        }

        $('#shareFacebook,#shareOnFooter').click(function () {
            $("meta[property='Title']").attr('content')
            //FBShare($('#hdnSocialShareTitle').val(), $('#divSocialSahreDescription').html(), $('#hdnHostName').val() + $('#hdnSocialShareImage').val(), $('#hdnEventURL').val());
            FBShare($("meta[property='og:title']").attr('content'), $('#divSocialSahreDescription').html(), $("meta[property='og:image']").attr('content'), window.location.toString().replace(' ', '%20'));
        });

        function WhatsApp(Content) {
            var message = encodeURIComponent(Content) + ', ' + encodeURIComponent(window.location.toString()) + '';
            var whatsapp_url = "whatsapp://send?text=" + message;
            window.location.href = whatsapp_url;
        }

        function MailTo() {
            var subject = $("meta[property='og:title']").attr('content') == '' ? ' ' : $("meta[property='og:title']").attr('content');
            var email = '', url = "mailto:" + email + "?subject=" + subject + "&body=" + $('#divSocialSahreDescription').html() + "%0A" + window.location.toString().replace(' ', '%20') + ' ';
            $('#mailDesktop').attr('href', url);
        }

        MailTo();

        $('#PjectShowcase').addClass('active');

        /*For hide when images are empty*/
        if ($('.list-article-thumb').find('li').length <= 0) {
            $('.block-article-thumb').remove();
        }
        else {
            $('.block-article-thumb').show();
        }


        ///* Hide ProjectName */
        //if ($('.project-info').find('.desc').html() == '') {
        //    $('.project-info').find('.head').hide();
        //}

        /*Hide Author Info*/
        if ($('.author-info').find('.name').html() == '') {
            $('.author-info').find('.head').hide();
        }

        /*For Hide Have your Own Idea Section*/
        if ($('.inner .row:eq(0)').find('.title').text().trim() == '' && $('.inner .row:eq(0)').find('.link-desc').html().trim() == '' && $('.inner .row:eq(1)').find('.col-xs-12').html().trim() == '') {
            $('.section-idea').hide();
        }

        if ($('.block-quotes').find('#content_0_quotation1').html() == '' && $('.block-quotes').find('#content_0_quotation2').html() == '') {
            $('.block-quotes').hide();
        }

        if ($('.block-quotes').find('#content_0_quotation1').html() == '') {
            $('#content_0_quotation1,.block-quotes .bx-pager-item .bx-pager-link').hide();
        }

        if ($('.block-quotes').find('#content_0_quotation2').html() == '') {
            $('#content_0_quotation2,.block-quotes .bx-pager-item').hide();
        }

        $(".project-item").click(function () {
            var itemName = $(this).find(".ItemName").val();
            ga('send', 'event', { eventCategory: 'Article – Related articles', eventAction: 'Click', eventLabel: itemName });
        });

        $('#shareFacebook').click(function () {
            var Title = $('#content_0_ProjectName').val();
            ga('send', 'event', {
                eventCategory: 'Article',
                eventAction: 'Share on Facebook',
                eventLabel: Title
            });
        });
        $('#mailDesktop').click(function () {
            var Title = $('#content_0_ProjectName').val();
            ga('send', 'event', {
                eventCategory: 'Article',
                eventAction: 'Email project',
                eventLabel: Title
            });
        });
        /*remove image width and height*/
        $(window).load(function () {
            $('.DskImg,.MbImg').removeAttr("height").removeAttr("width");
        })



</script>
