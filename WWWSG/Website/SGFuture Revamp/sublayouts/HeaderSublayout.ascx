﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderSublayout.ascx.cs" Inherits="sgfuture.SGFuture_Revamp.sublayouts.HeaderSublayout" %>
<header id="page-header">
    <div class="wrap-top">
        <div class="wrap-left pull-left">
            <a href="javascript:void(0);" class="icon-nav" onclick="ga('send', 'event', { eventCategory: 'SGFuture Header', eventAction: 'Click', eventLabel: ' burger menu select '});">
                <span class="icon icon-line"></span>
                <span class="icon icon-line"></span>
                <span class="icon icon-line"></span>
            </a>
            <a href="/sgfuture" class="logo" onclick="ga('send', 'event', { eventCategory: 'SGFuture Header', eventAction: 'Click', eventLabel: 'logo'});">
                <sc:Image ID="imgLogo" runat="server" Field="Header Logo" CssClass="thumb" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
            </a>
        </div>
        <div id="divDownloadTextShow" runat="server" class="wrap-right pull-right hidden-xs">
                        <a href="javascript:void(0)" target="_blank" id="DownloadFile" runat="server" class="button-download" onclick="ga('send', 'event', { eventCategory: 'SGFuture Header', eventAction: 'Click', eventLabel: 'Download SGFuture Report'});">
                <span class="download-text">
                    <sc:Text ID="txtDownloadText" runat="server" Field="Download SGFuture Report text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
                <span class="icon icon-download">download</span>
            </a>
          <%--  <asp:HiddenField ID="hdnDownload" runat="server" />--%>
        </div>
        <asp:HiddenField ID="hdnDownloadbtnimage" runat="server" />
    </div>
    <div class="wrap-nav">
        <nav class="text-center">

            <a href="/sgfuture" class="nav-item visible-xs first" runat="server" id="aSgfuture" visible="false">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="txtHome" runat="server" Field="Home Text for mobile" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a>
                      
             <a runat="server" id="aAbout" visible="false" href="/sgfuture/About-SGFuture" class="nav-item" onclick="ga('send', 'event', { eventCategory: 'SGFuture Header', eventAction: 'Click', eventLabel: 'About SG Future'});">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="txtAbout" runat="server" Field="About SGFuture Text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a>
            
             <a runat="server"  visible="false" href="/sgfuture/Project-Showcase" class="nav-item" id="PjectShowcase" onclick="ga('send', 'event', { eventCategory: 'SGFuture Header', eventAction: 'Click', eventLabel: 'Project Showcase'});">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="Text1" runat="server" Field="Project Showcase Text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a>

            
                <a runat="server"  visible="false" href="/sgfuture/Past-Engagements" class="nav-item last" id="PstEngagement" onClick="ga('send', 'event', { eventCategory: 'SGFuture Header', eventAction: 'Click', eventLabel: 'Past Engagements'});">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="Text2" runat="server" Field="Past Engagements Text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a>

            <a id="DownloadFileMenu" target="_blank" runat="server" class="nav-item visible-xs" href="javascript:void(0)" onclick="ga('send', 'event', { eventCategory: 'SGFuture Header', eventAction: 'Click', eventLabel: 'Download SGFuture Report'});">
                <sup class="sup"></sup>
                <span class="nav-text">
                    <sc:Text ID="txtDownload" runat="server" Field="Download SGFuture Report text" DataSource="{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" />
                </span>
            </a>
        </nav>
    </div>
    <div class="nav-fade"></div>
</header>

<script type="text/javascript">
    $(window).load(function () {
        var path = $('#header_0_hdnDownloadbtnimage').val();
        $('.icon.icon-download').css({ 'background': 'url("' + path + '") center center no-repeat', 'background-size': ' cover' });
    })
    var i = 0;
    $('.sup').each(function () {
        $(this).text('0' + i);
        i++;
    });
    $(".wrap-nav > .text-center a").filter(function () {
        return this.href == location.href.replace(/#.*/, "");
    }).addClass("active")
</script>
