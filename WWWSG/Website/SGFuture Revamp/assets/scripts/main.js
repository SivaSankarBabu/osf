'use strict';

var SGF = window.SGF || {};

SGF.main = {
	init: function init() {
		SGF.main.initNav();
		SGF.main.initModal();
		SGF.main.initSlider();
		SGF.main.initTabs();
		SGF.main.initStickSocial();
		SGF.main.initSocialAction();
		SGF.main.initToTop();
		SGF.main.initMobileArticleHover();
	},

	initNav: function initNav() {
		$('.icon-nav').off('click').on('click', function (e) {
			e.preventDefault();
			$(this).toggleClass('active');
			$('.wrap-nav').toggleClass('active');
			$('.nav-fade').toggleClass('show');
		});

		$('.nav-fade').off('click').on('click', function () {
			if ($('.icon-nav').is('.active')) {
				$('.icon-nav').removeClass('active');
				$('.wrap-nav').removeClass('active');
				$(this).removeClass('show');
			}
		});

		$(window).on('scroll', function () {
			if ($('.icon-nav').is('.active')) {
				$('.icon-nav').removeClass('active');
				$('.wrap-nav').removeClass('active');
				$('.nav-fade').toggleClass('show');
			}
		});
	},

	initModal: function initModal() {
		if ($('.button-watch-video').length) {
			$('.button-watch-video').off('click').on('click', function (e) {
				e.preventDefault();
				var target = $(this).attr('href');
				$(target).modal();
				var iframe = $(target).find('iframe');
				iframe.attr('src', iframe.data('src'));

				$(target).off('hidden.bs.modal').on('hidden.bs.modal', function () {
					iframe.attr('src', '');
				});
			});
		}
	},

	initSlider: function initSlider() {
		if ($('.list-quote').length) {
			if ($('.list-quote:visible').length && $('.list-quote:visible .item-quote').length > 1) {
				var bxQuote = $('.list-quote:visible').bxSlider({
					controls: false
				});

				$('.list-quote:visible').data('slider', bxQuote);

				$('.list-quote .item-link').off('click').on('click', function (e) {
					e.preventDefault();
					if (!bxQuote) {
						bxQuote = $('.list-quote:visible').data('slider');
					}
					if (bxQuote) {
						bxQuote.goToNextSlide();
					}
				});
			}
		}

		if ($('.list-article-thumb:visible').length && $('.list-article-thumb:visible .item').length > 1) {
			$('.list-article-thumb:visible').bxSlider({
				adaptiveHeight: true
			});
		}

		if ($('.list-project').length) {
			var ww = $(window).width();
			var ms = ww > 767 ? 3 : 1;
			var sw = Math.round(ww / ms);
			$('.list-project').bxSlider({
				maxSlides: ms,
				slideWidth: sw,
				moveSlides: 1,
				infiniteLoop: true,
				pager: false
			});
		}
	},

	initTabs: function initTabs() {
		if ($('.tabs-wrapper').length) {
			var ww = $(window).width();

			$('.tabs-top .item-tab').off('click').on('click', function (e) {
				e.preventDefault();
				$(this).parent().addClass('active').siblings().removeClass('active');
				$($(this).attr('href')).fadeIn().addClass('active').siblings().removeClass('active').hide();
				$('.bg-fade').attr('data-last-active', $(this).attr('href'));

				if (ww < 768) {
					var h = $(this).outerHeight();
					$('.tabs-top').css({ height: h });
					$('.tabs-top .icon-tab-nav').attr('style', '');
					//
					$('.block-article-thumb').show();
					$('.section-quote').show();
					$('.section-info').show();
					$('.bg-fade').hide();
					//
					var top = $('.tabs-wrapper').offset().top - ($('.wrap-top').height() + $('.tabs-top').height());
					$('html,body').animate({ scrollTop: top }, 300);
				}

				setTimeout(function () {
					SGF.main.initSlider();
				}, 50);
			});

			$('.tabs-top .icon-tab-nav').off('click').on('click', function (e) {
				e.preventDefault();
				$(this).attr('style', 'display: none !important;');
				$('.tabs-top').css({ height: '' });
				$('.tabs-top li').removeClass('active');
				$('.tab-content').removeClass('active').hide();
				//
				$('.block-article-thumb').hide();
				$('.section-quote').hide();
				$('.section-info').hide();
				$('.bg-fade').show();
				//
				$('html,body').animate({ scrollTop: 0 }, 300);
			});

			$('.bg-fade').off('click').on('click', function () {
				var target = $(this).attr('data-last-active');
				target = target ? target : '#tab-1';
				$('.item-tab[href="' + target + '"]').click();
			});

			if (ww < 768) {
				$.each($('.tabs-top .item-tab'), function (i, v) {
					var h = $(v).outerHeight();
					if ($(v).parent().is('.active')) {
						$('.tabs-top').css({ height: h });
					}
				});
				$('.bg-fade').hide();
			}
		}
	},

	initStickSocial: function initStickSocial() {
		if ($('.block-social').length) {
			var obj = $('.block-social');
			var neo = $('.article-info');
			var top = neo.offset().top;
			var left = neo.offset().left - 65;
			var gap = $('#page-header').height();
			var bot = $('.section-idea').offset().top;
			var height = obj.height();
			left = $(window).width() > 767 ? left : 0;

			$(window).on('scroll', function () {
				var wintop = $(window).scrollTop() + gap;

				if (wintop > top && obj.not('sticky')) {
					obj.addClass('sticky');
					obj.css({ left: left, top: gap });
				} else {
					obj.removeClass('sticky');
					obj.css({ left: '', top: '' });
				}

				if (wintop > bot - height && obj.not('out')) {
					obj.addClass('out');
					obj.fadeOut();
				} else {
					obj.removeClass('out');
					obj.fadeIn();
				}
			});

			$(window).on('resize', function () {
				if (window.callback) {
					clearTimeout(window.callback);
				}
				window.callback = setTimeout(function () {
					SGF.main.initStickSocial();

					// trigger calc now
					top = neo.offset().top;
					left = neo.offset().left - 65;
					gap = $('#page-header').height();
					left = $(window).width() > 767 ? left : 0;
					var wintop = $(window).scrollTop() + gap;
					if (wintop > top && obj.not('sticky')) {
						obj.addClass('sticky');
						obj.css({ left: left, top: gap });
					} else {
						obj.removeClass('sticky');
						obj.css({ left: '', top: '' });
					}
				}, 200);
			});
		}
	},

	initMobileArticleHover: function initMobileArticleHover() {
		if ($(window).width() < 992 && $('.block-projects').length) {
			$('.block-projects .project-item').on('touchstart', function (e) {
				$('.block-projects .project-item').removeClass('hover');
				$(this).addClass('hover');
				e.stopPropagation();
			});
			$(document).on('touchstart', function () {
				$('.block-projects .project-item').removeClass('hover');
			});
		}
	},

	initSocialAction: function initSocialAction() {
		if ($('.block-social').length) {
			$('.icon-facebook').off('click').on('click', function (e) {
				e.preventDefault();
				var url = $(this).attr('href');
				url = url.replace(/\[URL\]/g, window.location.href);
				window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=320,width=480');
			});
		}
	},

	initToTop: function initToTop() {
		if ($('.btn-back-top').length) {
			$('.btn-back-top').off('click').on('click', function (e) {
				e.preventDefault();
				$('body,html').animate({ scrollTop: 0 }, 500);
			});
			$(window).on('scroll', function () {
				var scrollTop = $(window).scrollTop();
				var height = $(window).height();
				var position = scrollTop + height;
				var limitTop = $('#page-footer').offset().top;

				if (position > height * 1.2 && position < limitTop) {
					$('.btn-back-top').css({ 'position': 'fixed' });
				} else {
					$('.btn-back-top').css({ 'position': '' });
				}
			});
		}
	}
};

$(function () {
	SGF.main.init();
});
//# sourceMappingURL=main.js.map
