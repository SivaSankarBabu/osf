﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;


// Purpose: Force download PDF

public partial class download_file : System.Web.UI.Page
{
    private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");

    protected void Page_Load(object sender, EventArgs e)
    {
        string str = "";

        if (Request.QueryString["f"] != null)
        {
            str = Request.QueryString["f"];
            Response.Write(str);
        }
        else {
            Response.Write("no file");
        }

        if (str.Length > 0)
        {
            MediaItem mi = web.GetItem(str);


            if (mi != null)
            {
                Stream stream = mi.GetMediaStream();
                long fileSize = stream.Length;
                byte[] buffer = new byte[(int)fileSize];
                stream.Read(buffer, 0, (int)stream.Length);
                stream.Close();
                Response.Clear();
                Response.ContentType = String.Format(mi.MimeType);
                Response.AddHeader("content-disposition", "attachment; filename=" + mi.Name + "." + mi.Extension);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(buffer);
                Response.End();
            }
            else { Response.Write("no mi"); }
        }
    }
}
