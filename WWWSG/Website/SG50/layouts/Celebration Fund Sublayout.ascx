﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Celebration_fund_sublayout.Celebration_fund_sublayoutSublayout" CodeFile="~/SG50/layouts/Celebration Fund Sublayout.ascx.cs" %>
<html class="html">
<head>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <meta name="generator" content="7.1.329.244" />
    <title>Home</title>
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="/SG50/Parrallax/css/site_global.css?4221015755" />
    <link rel="stylesheet" type="text/css" href="/SG50/Parrallax/css/index.css?239918620" id="pagesheet" />
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
    <!-- Other scripts -->
    <script type="text/javascript">

        document.documentElement.className += ' js';
    </script>
</head>

<body>

    <div class="desktop_1">
        <!-- Desktop div Start -->

        <div class="clearfix" id="page">
            <!-- column -->
            <div class="se_invi" id="u776">
                <!-- simple frame -->
            </div>
            <div class="clearfix pre_init se_invi" id="u783-8">
                <!-- content -->
                <sc:Text ID="Txttitle1" Field="Parallax Banner 1 Title" runat="server" />
                
            </div>
            <div class="clearfix pre_init se_invi" id="u979-6">
                <!-- content -->
                <sc:Text ID="TxtContent1" Field="Parallax Banner 1 Content" runat="server" />
                
            </div>
            <div class="se_invi" id="u462">
                <!-- simple frame -->
            </div>
            <div class="clearfix se_invi" id="u813-16">
                <!-- content -->
               <sc:Text ID="Text1" Field="Content 1 Header" runat="server" />
            </div>
            <div class="clearfix se_invi" id="u821-6">
                <!-- content -->
                <%--<p>All projects will go through an evaluation process, with each successful application receiving up to 90% of the project expenditure, capped at $50,000 per project.</p>--%>
                <sc:Text ID="Text2" Field="Content 1 Text" runat="server" />
            </div>
            <div class="clip_frame se_invi" id="u822">
                <!-- image -->
                <img class="block" id="u822_img" src="/SG50/Parrallax/images/bulb.png" alt="" width="237" height="237" />
            </div>
            <div class="se_invi" id="u730">
                <!-- simple frame -->
            </div>
            <div class="clearfix pre_init se_invi" id="u852-16">
                <!-- content -->
                <sc:Text ID="TextContent2" Field="Parallax Banner 2 Content" runat="server" />
            </div>
            <div class="clearfix pre_init se_invi" id="u846-6">
                <!-- content -->
                <sc:Text ID="TextTitle2" Field="Parallax Banner 2 Title" runat="server" />
            </div>
            <div class="clearfix colelem" id="u598">
                <!-- column -->
                <div class="clearfix colelem" id="u980-4">
                    <!-- content -->
                    <%--<p>LET'S GET STARTED.</p>--%>
                    <sc:Text ID="Text3" Field="Content 2 Header" runat="server" />
                </div>
                <div class="clearfix colelem" id="u981-12">
                    <!-- content -->
                    <%--<p>To get started, let’s find out a little more about your project.</p>
                    <p>You can download the application form, and either email it to <a class="nonblock" href="mailto:SG50CelebrationFund@Singapore50.sg"><span id="u981-4">SG50CelebrationFund@Singapore50.sg</span></a>&nbsp;or mail the hard copy of the application form to us.</p>
                    <p>&nbsp;</p>
                    <p>Simply download the form and you are ready to go. We look forward to hearing from you!</p>
                    </br>--%>
                    <sc:Text ID="Text4" Field="Content 2 Text" runat="server" />
                </div>
                <div id="btn_download_form" class="downloadButton1">
                    <div style="visibility: hidden;">
                        <a href="/sg50/Apply For Funding.aspx" id="lnkDownloadCFDesktop" runat="server">
                            <sc:Text ID="txtdownload" runat="server" Field="Button1 Text" />
                            </a>
                        
                    </div>
                    <%--<div id="btn_faq" class="downloadButton2" style="margin-left: 5px !important;">
                        <a href="#" onclick="ShowModalPopup('dvPopup2'); return false; ">
                            <sc:Text ID="txtFAQ" runat="server" Field="Button2 Text" />
                            </a>
                    </div>--%>

                    <%--<div class="clip_frame se_invi" id="u855">
                        <!-- image -->
                        <img class="block" id="u855_img" src="/SG50/Parrallax/images/notepad.png" alt="" width="252" height="252" />
                    </div>--%>
                    <div class="verticalspacer"></div>
                </div>

            </div>
        </div>
        <!-- Desktop div End -->

        <div class="mobile">
            <!-- Mobile div Start -->

            <div class="banner1">
                <div class="bannerImage1">
                    <sc:Image ID="Image1" runat="server" Field="Image 1" />
                    <%-- <img src="/SG50/Parrallax/images/celeb_fund_1.jpg" />--%>
                </div>
                <%--<sc:Text ID="Text1" Field="Banner 1 Text" runat="server" />--%>
                
                    <p>GIVE LIFE</p>
                    <p>TO YOUR SG50</p>
                    <p>CELEBRATORY IDEA.</p>
                    <!-- content -->
                    <p>Just as every significant milestone in life deserves a proper celebration, why not turn your ideas for Singapore’s 50th into</p>
                    <p>a reality? This is why we’ve created the SG50 Celebration Fund, to give you a little support when you need it.</p>
               
            </div>
            <div class="banner2">
                <div class="bannerImage2">
                    <sc:Image ID="Image2" runat="server" Field="Image 2" />
                    <%-- <img src="/SG50/Parrallax/images/celeb_fund_2.jpg" />--%>
                </div>
                <%--<sc:Text ID="Text2" Field="Banner 2 Text" runat="server" />--%>
                <p><span id="Span1">HOW</span>&nbsp;<span id="Span2">MUC</span>H <span id="Span3">SUPPOR</span>T</p>
                <p><span id="Span4">AR</span>E WE <span id="Span5">TALKIN</span>G <span id="Span6">ABOU</span>T?</p>
                <p>All projects will go through an evaluation process, with each successful application receiving up to 90% of the project expenditure, capped at $50,000 per project.</p>
            </div>
            <div class="banner3">
                <div class="bannerImage3">
                    <sc:Image ID="Image3" runat="server" Field="Image 3" />
                    <%-- <img src="/SG50/Parrallax/images/celeb_fund_3.jpg" />--%>
                </div>
                <%--<sc:Text ID="Text3" Field="Banner 3 Text" runat="server" />--%>
                <p id="P1">Each project will be evaluated on its ability to:</p>
                <p id="P2">• Raise awareness of our Singaporean identity and</p>
                <p id="P3">sense of belonging to Singapore;</p>
                <p id="P4">• Reach out and engage the community; as well as</p>
                <p id="P5">• Show potential in successful completion and execution</p>
                <p id="P6">according to plan.</p>
                <p id="P7">&nbsp;</p>
                <p>&nbsp;</p>
                <p>IS IT GUARANTEED</p>
                <p>FUNDING?</p>
            </div>
            <div class="banner4">
                <div class="bannerImage4">
                    <sc:Image ID="Image4" runat="server" Field="Image 4" />
                    <%-- <img src="/SG50/Parrallax/images/celeb_fund_4.jpg" />--%>
                </div>
                <%--<sc:Text ID="Text4" Field="Banner 4 Text" runat="server" />--%>
                <p>To get started, let’s find out a little more about your project.</p>
                <p>You can download the application form, and either email it to <a class="nonblock" href="mailto:SG50CelebrationFund@Singapore50.sg"><span id="Span7">SG50CelebrationFund@Singapore50.sg</span></a>&nbsp;or mail the hard copy of the application form to us.</p>
                <p>&nbsp;</p>
                <p>Simply download the form and you are ready to go. We look forward to hearing from you!</p>
                </br>
                <div class="downloadButton1" style="visibility: hidden;"><a href="/en/sg50/Apply For Funding.aspx" id="lnkDownloadCFmobile" runat="server">Apply For Funding</a></div>
                <%--<div class="downloadButton2" style="margin-left: 5px !important;"><a href="#" onclick="ShowModalPopup('dvPopup2'); return false; ">FAQ FOR SG50 CELEBRATION FUND</a></div>--%>
            </div>

        </div>
        <!-- Mobile div End -->

        <!-- JS includes -->
        <script type="text/javascript" src="/SG50/include/js/1.8.3/jquery-1.8.3.min.js"></script>
        <script src="/SG50/Parrallax/scripts/museutils.js?3865766194" type="text/javascript"></script>
        <script src="/SG50/Parrallax/scripts/jquery.scrolleffects.js?232004539" type="text/javascript"></script>
        <script src="/SG50/Parrallax/scripts/jquery.watch.js?4068933136" type="text/javascript"></script>
        <!-- Other scripts -->
        <script type="text/javascript">
            $(document).ready(function () {
                try {
                    Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
                    Muse.Utils.prepHyperlinks(true);/* body */
                    $('#u776').registerPositionScrollEffect([{ "in": [-Infinity, 157], "speed": [0, 0] }, { "in": [157, Infinity], "speed": [0, 0] }]);/* scroll effect */
                    $('#u776').registerBackgroundPositionScrollEffect([{ "in": [-Infinity, 157], "speed": [0, 1] }, { "in": [157, Infinity], "speed": [0, 2.2] }]);/* scroll effect */
                    $('#u783-8').registerPositionScrollEffect([{ "in": [-Infinity, 85], "speed": [0, 0] }, { "in": [85, Infinity], "speed": [0, 0] }]);/* scroll effect */
                    $('#u783-8').registerOpacityScrollEffect([{ "opacity": 100, "in": [-Infinity, 85], "fade": 62 }, { "in": [85, 85], "opacity": 100 }, { "opacity": 0, "in": [85, Infinity], "fade": 65 }]);/* scroll effect */
                    $('#u979-6').registerPositionScrollEffect([{ "in": [-Infinity, 84], "speed": [0, 0] }, { "in": [84, Infinity], "speed": [0, 0] }]);/* scroll effect */
                    $('#u979-6').registerOpacityScrollEffect([{ "opacity": 0, "in": [-Infinity, 84], "fade": 4 }, { "in": [84, 84], "opacity": 100 }, { "opacity": 0, "in": [84, Infinity], "fade": 1916 }]);/* scroll effect */
                    $('#u462').registerPositionScrollEffect([{ "in": [-Infinity, 378], "speed": [0, 0] }, { "in": [378, Infinity], "speed": [0, 1] }]);/* scroll effect */
                    $('#u813-16').registerPositionScrollEffect([{ "in": [-Infinity, 398], "speed": [0, 0] }, { "in": [398, Infinity], "speed": [0, 1] }]);/* scroll effect */
                    $('#u821-6').registerPositionScrollEffect([{ "in": [-Infinity, 400], "speed": [0, 0] }, { "in": [400, Infinity], "speed": [0, 1] }]);/* scroll effect */
                    $('#u822').registerPositionScrollEffect([{ "in": [-Infinity, 550], "speed": [0, 2] }, { "in": [550, Infinity], "speed": [0, 2] }]);/* scroll effect */
                    $('#u730').registerPositionScrollEffect([{ "in": [-Infinity, 727.6], "speed": [0, 1] }, { "in": [727.6, Infinity], "speed": [0, 1] }]);/* scroll effect */
                    $('#u730').registerBackgroundPositionScrollEffect([{ "in": [-Infinity, 727.6], "speed": [0, 1] }, { "in": [727.6, Infinity], "speed": [0, 1.5] }]);/* scroll effect */
                    $('#u852-16').registerPositionScrollEffect([{ "in": [-Infinity, 1161], "speed": [0, 1] }, { "in": [1161, Infinity], "speed": [0, 1.2] }]);/* scroll effect */
                    $('#u852-16').registerOpacityScrollEffect([{ "opacity": 0, "in": [-Infinity, 1161], "fade": 85 }, { "in": [1161, 1161], "opacity": 100 }, { "opacity": 0, "in": [1161, Infinity], "fade": 472 }]);/* scroll effect */
                    $('#u846-6').registerPositionScrollEffect([{ "in": [-Infinity, 870.6], "speed": [0, 1] }, { "in": [870.6, Infinity], "speed": [0, 1] }]);/* scroll effect */
                    $('#u846-6').registerOpacityScrollEffect([{ "opacity": 100, "in": [-Infinity, 870.6], "fade": 26 }, { "in": [870.6, 870.6], "opacity": 100 }, { "opacity": 0, "in": [870.6, Infinity], "fade": 233 }]);/* scroll effect */
                    $('#u855').registerPositionScrollEffect([{ "in": [-Infinity, 1244.9], "speed": [0, 1.6] }, { "in": [1244.9, Infinity], "speed": [0, 1.6] }]);/* scroll effect */
                    Muse.Utils.fullPage('#page');/* 100% height page */
                    Muse.Utils.showWidgetsWhenReady();/* body */
                    Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
                } catch (e) { Muse.Assert.fail('Error calling selector function:' + e); }
            });
        </script>

        <script type="text/javascript" src="/SG50/include/js/common.js"></script>
        <script type="text/javascript">
            var animation = true;
            $(document).ready(function () {

                $('#nav div').hover(
                    function () {
                        //show its submenu
                        $('div', this).fadeIn(700);
                        $(this).find("a:first").addClass("active_nav");
                        //$(this).find("a:first").css('background','url(images/DMP/mobile/menu_list2.png) no-repeat 95% 50%');
                    },
                    function () {
                        //hide its submenu
                        //$('div', this).slideUp(5);
                        $('div', this).hide();
                        $(this).find("a:first").removeClass("active_nav");
                        //$(this).find("a:first").css('background','url(images/DMP/mobile/menu_list.png) no-repeat 95% 50%');
                    }
                );
                $(".celebrationFund a").addClass("selected");
            });

        </script>

        <sc:Text ID="Text10" runat='server' Field='Additional Javascript' />
</body>
</html>

