﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Event_details_sublayout.Event_details_sublayoutSublayout" CodeFile="~/SG50/layouts/Event Details Sublayout.ascx.cs" %>
<style>
    .contentContainer {
        width: 1280px;
        min-height: 768px;
        background-color: #FFF;
        overflow: hidden;
        background-image: url('/SG50/images/icon2.png');
        background-repeat: no-repeat;
        background-position: right bottom;
    }

    .leftContentContainer {
        /*width: 1020px;
		width:920px;*/
        width: 860px;
        float: left;
        min-height: 500px;
        background-image: url('/SG50/images/shadow.jpg');
        background-repeat: no-repeat;
        background-position: right top;
    }

    .rightContentContainer {
        width: 420px;
        float: left;
    }

    .rightSubContent {
        padding-left: 20px;
        padding-right: 20px;
    }

    .leftContent {
        margin-left: 115px;
        margin-right: 10px;
        margin-bottom: 30px;
    }

    .breadcrumb {
        margin-top: 25px;
        font-size: 16px;
        color: #ef2c2e;
    }

        .breadcrumb img {
            width: 10px;
            height: 10px;
        }

        .breadcrumb a, .breadcrumb a:hover, .breadcrumb a:visited {
            color: #353333;
        }

    .pressTitle {
        margin-top: 50px;
        font-size: 32px;
        border-bottom: solid 1px #113706;
    }

    .rightTitle {
        font-size: 18px;
        color: #ef2c2e;
        margin-top: 25px;
        margin-bottom: 20px;
    }

    .rightImage {
        margin-top: 569px;
    }

    .PortfolioPagination {
        width: 100%;
        text-align: right;
        margin-top: 5px;
    }

    .prTitle {
        font-size: 20px;
        line-height: 40px;
    }

    .prDate {
        font-size: 16px;
        line-height: 25px;
        color: #353333;
    }

    .prBlurb {
        font-size: 16px;
        line-height: 20px;
        margin-top: 15px;
        margin-bottom: 25px;
    }

    .prItem {
        margin-top: 25px;
        padding-bottom: 15px; /*border-bottom:dotted 1px #353333;*/
        border-bottom: solid 1px #ccc;
    }

        .prItem a, .prItem a:hover, .prItem a:visited {
            color: black;
        }

    .prLatest {
        padding-top: 15px;
        color: #353333;
    }

        .prLatest a, .prLatest a:hover, .prLatest a:visited {
            color: #353333;
            font-size: 14px;
            line-height: 1.2em;
        }

    .divider {
        height: 15px;
        border-bottom: 1px solid #f0f0f0;
        margin-left: 0px;
        margin-right: 0px;
    }

    @media screen and (min-width: 300px) and (max-width: 768px) {
        .contentContainer {
            width: 100%;
        }

        .leftContentContainer {
            width: 100%;
        }

        .rightContentContainer {
            display: none;
        }

        .leftContent {
            margin-top: 30px;
            margin-left: 20px;
        }
    }
</style>
<style>
    .pressDetailsTitle {
        margin-top: 30px;
        margin-bottom: 25px;
        margin-right: 15px;
        font-size: 36px;
        color: #353333;
    }

    .pressDetailsBanner {
        height: 400px;
    }

    .pressDetailsDate {
        margin-bottom: 25px;
        font-size: 14px;
        color: #353333;
    }

    .pressDetailsContent {
        margin-right: 15px;
    }

        .pressDetailsContent, .pressDetailsContent p {
            font-size: 16px;
            color: #565656;
            line-height: 25px;
        }

    @media screen and (min-width: 300px) and (max-width: 768px) {
        .press {
            width: 100%;
            height: auto;
        }

        .pressDetailsBanner {
            width: 100%;
            height: auto;
        }

            .pressDetailsBanner img {
                width: 100%;
                height: auto;
            }
    }
</style>
<style>
    .pressDetailsBanner {
        overflow: hidden;
    }

    .imgLatest {
        float: left;
        width: 90px;
    }

    .lnkLatest {
        width: 280px;
        float: left;
        font-size: 14px;
    }

        .lnkLatest .lnkLatest a {
            font-size: 14px;
            color: 353333;
        }

    .ecdivider {
        border-bottom: 1px solid #f0f0f0;
        margin-left: 0px;
        margin-right: 0px;
    }

    .sliderContainer {
        height: 550px;
    }

    .mySlider {
        position: relative;
        float: left;
    }

        .mySlider .mySliderContainer {
            float: left;
            position: relative;
            width: 716px;
            height: 520px;
            overflow: hidden;
            z-index: 10;
            text-align: center;
        }

        .mySlider .slide {
            width: 716px;
            height: 425px;
            float: left;
            position: absolute;
            display: none;
            left: 0;
        }

            .mySlider .slide img {
                /* width:770px;*/
                height: 425px;
                overflow: hidden;
            }

        .mySlider .sliderControlDiv {
            float: left;
            position: absolute;
            bottom: 20px;
            left: 20px;
            width: auto;
            z-index: 12;
        }

        .mySlider .thumbslider {
            position: absolute;
            bottom: 5px;
            z-index: 11;
        }

            .mySlider .thumbslider div {
                float: left;
                cursor: pointer;
                color: #74489d;
                position: relative;
                margin-left: 5px;
                margin-right: 5px;
            }

                .mySlider .thumbslider div p {
                    color: #74489d;
                    padding: 0;
                    margin: 0;
                }

                .mySlider .thumbslider div img {
                    width: 92px;
                    height: 70px;
                    overflow: hidden;
                }

        .mySlider #prev, .mySlider #next {
            float: left;
            cursor: pointer;
            padding-left: 10px;
        }

        .mySlider #prev {
            /*right:80px;*/
        }

        .mySlider #next {
            /*right:20px;*/
        }

            .mySlider #prev img, .mySlider #next img {
                /*width:25px;
		height:25px;*/
            }

        .mySlider .caption, .homeRightBannerDiv .caption {
            position: absolute;
            bottom: 0px;
            left: 0px;
            height: auto;
            background-color: rgba(0, 0, 0, 0.5);
            width: 686px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 15px;
            padding-right: 15px;
            text-align: left;
        }

        .mySlider .captionDesc, .mySlider .captionDesc p, .homeRightBannerDiv .captionDesc, .homeRightBannerDiv .captionDesc p {
            font-size: 12px;
            color: #fff;
            width: 410px;
        }

            .mySlider .captionDesc p, .homeRightBannerDiv .captionDesc p {
                padding-bottom: 0;
            }
    /* Photo Gallery Styles */ .galleryDiv {
        float: left;
        margin-top: 20px;
        width: 100%;
        position: relative;
        margin-bottom: 20px;
        display: none;
    }

    .bigImgDiv {
        float: left;
        width: 100%;
    }

    .thumbDiv {
        float: left;
        width: 100%;
        padding-right: 30px;
        padding-top: 15px;
    }

        .thumbDiv a {
            margin-top: 10px;
            float: left;
            margin-right: 10px;
        }

    .bigImageCaption {
        height: auto;
        color: #fff;
        background: #000;
        width: 100%;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .caption {
        position: absolute;
        left: 0;
        bottom: 0;
        background: #000;
        height: auto;
        width: 100%;
        color: #fff;
        display: none;
        cursor: default;
    }

    .bigImageCaption p {
        color: #fff;
        font-size: 12px;
        margin-lett: 10px;
        padding-left: 10px;
    }

    @media screen and (min-width: 300px) and (max-width: 768px) {
        .galleryDiv {
            display: block;
        }

        .bigImgDiv img {
            width: 100%;
            height: auto;
        }

        .sliderContainer {
            display: none;
        }
    }
</style>

<script type="text/javascript" src="/SG50/include/js/myslider.js"></script>


<p>
    <a onclick='postToFeed();'></a>
</p>
<p id='msg'>
</p>

<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...


        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            // caption: fbdes,
            description: fbdes
        };
        FB.ui(obj);

    }

    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function (response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }

</script>

<script type="text/javascript">
    var animation = true;
    $(document).ready(function () {
        $('#expandContentDiv').hide();
        var $elem = $('body');

        $('#nav div').hover(
			function () {
			    //show its submenu
			    $('div', this).slideDown(400);
			    $(this).find("a:first").addClass("active_nav");
			},
			function () {
			    //hide its submenu
			    $('div', this).hide();
			    $(this).find("a:first").removeClass("active_nav");
			}
		);

        $("span.caption").css('display', 'none');
        $(".image img").click(function () {
            var image = $(this).parent().attr("rel");
            $(".image .caption").css('display', 'none');
            var captionDiv = $(this).parent().next("span.caption");

            var grandparentDiv = jQuery(this).parent().parent();

            grandparentDiv.children("span").css("display", "none");

            var thisImgDiv = $(this).parent().parent().prev();
            thisImgDiv.hide();
            thisImgDiv.fadeIn('slow');
            thisImgDiv.html('<img src="' + image + '"/><div class="bigImageCaption">' + captionDiv.html() + '</div>');
            return false;
        });

        $(".caption").each(function () {
            var bigImg = $(this).parent().parent().find("#image");
            var bottomVal = $(".caption").height();
            var parentHeight = $(this).parent().parent().height();
            var captionHeight = $(this).height();
        });

    });
</script>

<div class="press">
    <div class="pressDetailsBanner">
        <div>
            <div>
                <sc:Image ID="Image1" Field="Banner Image" runat="server" />
            </div>
        </div>
    </div>
</div>
<div class="contentContainer">
    <div class="leftContentContainer">
        <div class="leftContent">
            <sc:Sublayout ID="Sublayout1" runat="server" Path="/SG50/layouts/Breadcrumb Sublayout.ascx"></sc:Sublayout>
            <div class="pressDetailsTitle">
                <h1>
                    <sc:Text ID="Text1" Field="Content Title" runat="server" />
                </h1>
            </div>
            <div class="pressDetailsDate">
                <%= getDate() %>
            </div>
            <div class="sliderContainer" id="sliderContainer" runat="server">
                <div class="mySlider" id="mySlider">
                    <div class="mySliderContainer">
                        <asp:Repeater ID="repSlide" runat="server">
                            <ItemTemplate>
                                <div class="slide" id="<%# DataBinder.Eval(Container.DataItem,"slideID") %>">
                                    <img src="<%# DataBinder.Eval(Container.DataItem,"imgSrc") %>" alt="" />
                                    <div class="caption">
                                        <div class="captionDesc">
                                            <%# DataBinder.Eval(Container.DataItem,"caption") %>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="thumbslider">
                        <asp:Repeater ID="repThumbslider" runat="server">
                            <ItemTemplate>
                                <div rel="<%# DataBinder.Eval(Container.DataItem,"number") %>" class="">
                                    <img src="<%# DataBinder.Eval(Container.DataItem,"thumbImgSrc") %>" alt="" />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="galleryDiv">
                <div id="image" class="bigImgDiv">
                    <img src="<%= getImgLink() %>?h=425" alt="" border="0">
                    <div class="bigImageCaption">
                        <p>
                            <%= getCaption() %>
                        </p>
                    </div>
                </div>
                <div class="thumbDiv">
                    <asp:Repeater ID="repMobileGallery" runat="server">
                        <ItemTemplate>
                            <a rel="<%# DataBinder.Eval(Container.DataItem,"imgSrc") %>?h=425" class="image">
                                <img src="<%# DataBinder.Eval(Container.DataItem,"imgSrc") %>?h=70" class="thumb"
                                    border="0" />
                            </a><span class="caption" style="display: block;">
                                <%# DataBinder.Eval(Container.DataItem,"caption") %></span>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="pressDetailsContent">
                <p><sc:Text ID="Text2" Field="Content" runat="server" /></p>
            </div>
            <asp:Repeater ID="repsocialSharing" runat="server">
                <ItemTemplate>
                    <div style="text-align: center">
                        <a href="JavaScript:postToFeed('<%# Eval("FaceBookTitle").ToString()%>','<%# Eval("FaceBookContent").ToString()%>','<%# Eval("FaceBookThumbNail")%>','<%# Eval("EventUrl")%>')">
                            <img src="/SG50/images/facebook.png" /></a>
                        <a href="JavaScript:newPopup('<%# Eval("TwitterContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')">
                            <img src="/SG50/images/twitter.png" /></a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <div class="rightContentContainer">
        <div class="rightContent">
            <div class="rightSubContent">
                <div class="rightTitle">
                    Latest Events
                </div>
                <asp:Repeater ID="repLatest" runat="server">
                    <ItemTemplate>
                        <div id="divBackCurrent" runat="server" class="prLatest">
                            <div class="imgLatest">
                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                            </div>
                            <div class="lnkLatest">
                                <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>">
                                    <sc:Text ID="Text2" Field="Content Title" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                                </a>
                            </div>
                        </div>
                        <div style="clear: both;">
                            &nbsp;
                        </div>
                        <div class="ecdivider">
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
