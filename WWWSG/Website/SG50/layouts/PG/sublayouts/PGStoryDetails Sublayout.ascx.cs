﻿using Sitecore;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Text;
using System.Web.UI;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using System.Web;

namespace Layouts.Pgstorydetails_sublayout
{

    /// <summary>
    /// Summary description for Pgstorydetails_sublayoutSublayout
    /// </summary>
    public partial class Pgstorydetails_sublayoutSublayout : System.Web.UI.UserControl
    {
        Item itmContext = Sitecore.Context.Item;
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"] + "/SG50/Pioneer Generation"  ;
	
        private void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Session["ShareQuoteId"] = itmContext.ID.ToString();
                Placeholder plDetails = (Placeholder)this.Parent.FindControl("PlhDetails");
                Placeholder plShare = (Placeholder)this.Parent.FindControl("PlhShare");

                if (plDetails != null)
                {
                    plDetails.Visible = true;
                    plShare.Visible = false;
                }
                if (Session["Share"] == "Y")
                {


                    plDetails.Visible = false;
                    plShare.Visible = true;
                }

                if (itmContext != null)
                    Session["StoryRedirecturl"] = hostName.Replace(" ", "%20") + (LinkManager.GetItemUrl(itmContext).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "").Replace(" ", "%20"));

                string path = Sitecore.Context.Item.Paths.Path.ToString();

                Item ImageItemTop = web.GetItem(path + "/Images/Original");

		Item ImageItemThumb = web.GetItem(path + "/Images/Thumbnail");


                if (ImageItemThumb  != null)
                {
                    Sitecore.Data.Fields.ImageField image1 = ImageItemThumb.Fields["Image"];
                    if (image1.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        MainImgThumb.Src = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image1.MediaItem));
                        
                    }
                }

                Item ImageItemBottom1 = web.GetItem(path + "/Images/Image2");
                Item ImageItemBottom2 = web.GetItem(path + "/Images/Image3");
                Item ImageItemBottom3 = web.GetItem(path + "/Images/Image4");


                if (ImageItemTop != null)
                {
                    Sitecore.Data.Fields.ImageField image = ImageItemTop.Fields["Image"];
                    if (image.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        string url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                        StringBuilder DetailsHomeBanner = new StringBuilder();
                        DetailsHomeBanner.Append("<div class='item active'><img id='mainimg' src='" + url + "'/>");
                        DetailsHomeBanner.Append("<div class='carousel-caption'><div class='slide-caption'><p>" + itmContext.Fields["Pioneers Description"].ToString() + "<span>" + itmContext.Fields["Pioneers Name"].ToString() + "</span></p></div></div></div> </div>");
                        DetailsImgBanner.InnerHtml = DetailsHomeBanner.ToString();
                    }
                }

                StringBuilder DetailsInnerBanner = new StringBuilder();

                string buttom1 = string.Empty;
                string buttom2 = string.Empty;
                string buttom3 = string.Empty;

                if (ImageItemBottom1 != null)
                {
                    Sitecore.Data.Fields.ImageField image = ImageItemBottom1.Fields["Image"];
                    if (image.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        buttom1 = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                        DetailsInnerBanner.Append("<li><img src='" + buttom1 + "'></li>"); //class='single'

                    }
                }
                if (ImageItemBottom2 != null)
                {
                    Sitecore.Data.Fields.ImageField image2 = ImageItemBottom2.Fields["Image"];
                    if (image2.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        buttom2 = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image2.MediaItem, options));

                        DetailsInnerBanner.Append("<li><img src='" + buttom2 + "'></li>");
                    }
                }
                if (ImageItemBottom3 != null)
                {

                    Sitecore.Data.Fields.ImageField image3 = ImageItemBottom3.Fields["Image"];
                    if (image3.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        buttom3 = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image3.MediaItem, options));
                        DetailsInnerBanner.Append("<li><img src='" + buttom3 + "'></li>");
                    }

                }

                UlInner.InnerHtml = DetailsInnerBanner.ToString();



            }
        }

        public void NextStory(object sender, EventArgs e)
        {
            Sitecore.Collections.ChildList ccc = web.GetItem("{655A7E18-1AE6-447E-8A1F-C9A4768126B7}").GetChildren();
            string curntItem = Sitecore.Context.Item.ID.ToString();
            if (ccc[ccc.Count - 1].ID.ToString() == curntItem)
            {
                             
                string url = "/SG50/Pioneer Generation" + Sitecore.Links.LinkManager.GetItemUrl(ccc[0]).Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "");
                Response.Redirect(url);
            }
            else
            {
                for (int i = 0; i < ccc.Count; i++)
                {
                    if (ccc[i].ID.ToString() == curntItem)
                    {
                        string url = "/SG50/Pioneer Generation" + Sitecore.Links.LinkManager.GetItemUrl(ccc[i + 1]).Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "");
                        Response.Redirect(url);
                    }
                }
            }
        }



        public void Share_Quote(object sender, EventArgs e)
        {
            string valueNa = HttpContext.Current.Request.Url.AbsolutePath;
            Session["ShareQuoteId"] = itmContext.ID.ToString();
            // Response.Redirect("/SG50/Pioneer Generation/Share Quote.aspx");
            Session["Share"] = "Y";

            Response.Redirect(valueNa); // + "?Share=Share");

        }
    }
}
