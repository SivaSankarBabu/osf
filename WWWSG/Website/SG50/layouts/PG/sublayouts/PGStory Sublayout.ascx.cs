﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Publishing;
using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using System.Web.UI;
using CAPTCHA;
namespace Layouts.Pgstory_sublayout
{

    /// <summary>
    /// Summary description for Pgstory_sublayoutSublayout
    /// </summary>
    public partial class Pgstory_sublayoutSublayout : System.Web.UI.UserControl
    {
        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");

        string StoryName = string.Empty;
        string StoryArea = string.Empty;
        string StroryPioneersArea = string.Empty;
        string StoryPioneerName = string.Empty;
        string StoryEmail = string.Empty;
        Item itmContext = Sitecore.Context.Item;
        CaptchaGenerator cgen;
        private void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                cgen = new CaptchaGenerator();
                GetCaptchaimage();
                Sitecore.Data.Fields.CheckboxField CheckboxTerms = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["TC Checkbox"]);
                if (CheckboxTerms.Checked == true)
                    DivTerms.Visible = true;

                Sitecore.Data.Fields.CheckboxField CheckboxAgreement = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Agreement Checkbox"]);
                if (CheckboxAgreement.Checked == true)
                    DivAgreement.Visible = true;

            }
            // Put user code to initialize the page here

            CancelUnexpectedRePost();
        }

        private void GetCaptchaimage()
        {


            string strpath = Server.MapPath("/SG50/images/PG/") + "CaptchaImage.gif";
            cgen.ResetCaptchaColor();

            txt_CaptchaVal.Text = cgen.GenerateCaptcha(strpath);


            string strpath2 = "/SG50/layouts/PG/sublayouts/" + "CaptchaImage.ashx?id=" + strpath;
            Image1.ImageUrl = strpath2;



        }
        protected void Button1_Click1(object sender, ImageClickEventArgs e)
        {
            cgen = new CaptchaGenerator();
            GetCaptchaimage();
        }
        public bool LoadImage(String base64String, string type)
        {
            bool ImageSize = true;
            if (!string.IsNullOrEmpty(base64String))
            {
                byte[] byteArray = Convert.FromBase64String(base64String.Split(',')[1]);
                // byte[] byteArray = Convert.FromBase64String(base64String);
                System.Drawing.Image result = null;
                System.Drawing.Imaging.ImageFormat format = System.Drawing.Imaging.ImageFormat.Jpeg;

                result = new Bitmap(new MemoryStream(byteArray));

                using (System.Drawing.Image imageToExport = result)
                {
                    string filePath = string.Empty;
                    if (type == "Original")
                    {

                        string fileNameOriginal = string.Empty;
                        fileNameOriginal = txtName.Text + " Original" + DateTime.Now.ToString("yyMMdd") + DateTime.Now.ToString("hhmmss");
                        Session["fileNameOriginal"] = fileNameOriginal;
                        filePath = string.Format(Server.MapPath("/SG50/images/PG/PG_Temp/" + fileNameOriginal + ".{0}"), format.ToString());


                    }
                    else if (type == "Cropped")
                    {

                        string fileNameCropped = string.Empty;
                        fileNameCropped = txtName.Text + " Cropped" + DateTime.Now.ToString("yyMMdd") + DateTime.Now.ToString("hhmmss");
                        Session["fileNameCropped"] = fileNameCropped;
                        filePath = string.Format(Server.MapPath("/SG50/images/PG/PG_Temp/" + fileNameCropped + ".{0}"), format.ToString());

                    }
                    imageToExport.Save(filePath, format);
                    if (type == "Original")
                    {
                        int width = imageToExport.Width;
                        int height = imageToExport.Height;
                        decimal aspectRatio = width > height ? decimal.Divide(width, height) : decimal.Divide(height, width);
                        int fileSize = (int)new System.IO.FileInfo(filePath).Length;

                        if (fileSize <= 1048576 && width >= 1280 && height >= 740)
                            ImageSize = true;
                        else
                            ImageSize = true;
                    }



                }
            }


            return ImageSize;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (txt_CaptchaVal.Text == SG50Class.StripUnwantedString(txt_Captcha.Text))
                {
                    StoryName = SG50Class.StripUnwantedString(txtName.Text.TrimStart().TrimEnd());
                    StoryArea = SG50Class.StripUnwantedString(txtStoryArea.Text.TrimStart().TrimEnd());
                    StoryArea = StoryArea.Replace("\n", "<br />");
                    StroryPioneersArea = SG50Class.StripUnwantedString(txtPioneersArea.Text.TrimStart().TrimEnd());
                    StoryPioneerName = SG50Class.StripUnwantedString(txtPioneerName.Text.TrimStart().TrimEnd());
                    StoryEmail = SG50Class.StripUnwantedString(txtEmail.Text.TrimStart().TrimEnd());

                    if (StoryName != "" && StoryArea != "" && StroryPioneersArea != "" && StoryPioneerName != "" && StoryEmail != "")
                    {

                        bool ImageSize = LoadImage(txtOriginalb4.Value, "Original");
                        txtOriginalb4.Value = "";

                        HttpPostedFile file1 = Request.Files["filename1"];
                        HttpPostedFile file2 = Request.Files["filename2"];
                        HttpPostedFile file3 = Request.Files["filename3"];

                        if (ImageSize == true && (file1 != null && file1.ContentLength <= 1048576) && (file2 != null && file2.ContentLength <= 1048576) && (file3 != null && file3.ContentLength <= 1048576))
                        {
                            LoadImage(txtb4.Value, "Cropped");
                            txtb4.Value = "";
                            string filebaseOriginalHash = string.Empty;


                            try
                            {

                                if (Session["fileNameOriginal"] != null)
                                {
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {

                                        Item newForm = null;
                                        Item ImagenewForm = null;
                                        string filename = string.Empty;
                                        Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Pioneer Generation/PGStory");
                                        Item parentItem = master.GetItem("{655A7E18-1AE6-447E-8A1F-C9A4768126B7}");
                                        string ItemName = StoryPioneerName + "-" + DateTime.Now.ToString("yyMMdd") + "-" + DateTime.Now.ToString("hhmmss"); // Guid.NewGuid().ToString();
                                        newForm = parentItem.Add(ItemName.ToString(), template);

                                        newForm.Editing.BeginEdit();
                                        newForm.Fields["Story Description"].Value = Convert.ToString(StoryArea);
                                        newForm.Fields["Pioneers Description"].Value = Convert.ToString(StroryPioneersArea);
                                        newForm.Fields["Categories"].Value = "";// Convert.ToString(ddlCategories.SelectedItem.Text);
                                        newForm.Fields["Pioneers Name"].Value = Convert.ToString(StoryPioneerName);
                                        newForm.Fields["Age"].Value = Convert.ToString(ddlAge.SelectedItem.Text);
                                        newForm.Fields["Name"].Value = Convert.ToString(StoryName);
                                        newForm.Fields["Email Address"].Value = Convert.ToString(StoryEmail);
                                        newForm.Editing.EndEdit();

                                        Item Folder = null;
                                        Folder = master.GetItem("/sitecore/content/SG50/Pioneer Generation/Images");
                                        Folder.CopyTo(newForm, "Images");

                                        string[] allowedExtension = new string[] { ".jpg", ".png", ".gif", ".Jpeg", ".jpeg", ".jpeg" };
                                        bool Contains = true;
                                        bool result = true;
                                        var item = newForm;
                                        using (new Sitecore.SecurityModel.SecurityDisabler())
                                        {
                                            if (item != null)
                                            {

                                                var source = master;
                                                var target = Sitecore.Data.Database.GetDatabase("web");

                                                var options = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now)
                                                {
                                                    RootItem = item,
                                                    Deep = true,
                                                };

                                                var publisher = new Publisher(options);
                                                publisher.Publish();

                                            }
                                        }
                                        for (int i = 1; i < 3; i++)
                                        {


                                            string filebaseOriginal = string.Empty;




                                            if (i == 1)
                                            {
                                                if (Session["fileNameOriginal"] != null)
                                                {
                                                    filebaseOriginal = Server.MapPath("/SG50/images/PG/PG_Temp/" + Session["fileNameOriginal"] + ".Jpeg");
                                                    Session["fileNameOriginal"] = null;
                                                }
                                            }
                                            else
                                            {
                                                if (Session["fileNameCropped"] != null)
                                                {
                                                    filebaseOriginal = Server.MapPath("/SG50/images/PG/PG_Temp/" + Session["fileNameCropped"] + ".Jpeg");
                                                    Session["fileNameCropped"] = null;
                                                }
                                            }




                                            string Originalextension = Path.GetExtension(filebaseOriginal);
                                            string Originalfilename = Path.GetFileName(filebaseOriginal);

                                            if (filebaseOriginal != null)
                                            {

                                                if (allowedExtension.Contains(Originalextension))
                                                {
                                                    Sitecore.Resources.Media.MediaCreatorOptions optionsM = new Sitecore.Resources.Media.MediaCreatorOptions();
                                                    optionsM.Database = master;
                                                    optionsM.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                                                    optionsM.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                                                    filename = Originalfilename.Replace(".jpg", "").Replace(".png", "").Replace(".gif", "").Replace(".Jpeg", "");
                                                    filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                                                    string Destination = "/sitecore/media library/SG50/PG/Story Images/" + ItemName + filename;
                                                    optionsM.Destination = Destination;

                                                    bool FileExists = File.Exists(Server.MapPath("/SG50/layouts/PG/Story Images/" + Originalfilename));
                                                    if (!FileExists)
                                                    {

                                                        System.IO.File.Copy(filebaseOriginal, Server.MapPath("/SG50/layouts/PG/Story Images/" + Originalfilename));
                                                    }
                                                    optionsM.FileBased = false;
                                                    Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                                                    Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/layouts/PG/Story Images/" + Originalfilename), optionsM);
                                                    string path = newForm.Paths.Path.ToString();
                                                    Item ImageItem1 = master.GetItem(path + "/Images");
                                                    Sitecore.Data.Items.TemplateItem Imagetemplate = master.GetItem("/sitecore/templates/SG50/Pioneer Generation/PGStrory Image Banner");
                                                    Item ImageItem = master.GetItem(ImageItem1.ID.ToString());
                                                    string ImageItemName = string.Empty;

                                                    if (i == 1)
                                                        ImageItemName = "Original";
                                                    else
                                                        ImageItemName = "Thumbnail";


                                                    // imagecount = imagecount + 1;
                                                    ImagenewForm = ImageItem.Add(ImageItemName.ToString(), Imagetemplate);
                                                    ImagenewForm.Editing.BeginEdit();

                                                    Sitecore.Data.Items.MediaItem image = item.Database.GetItem(Destination);
                                                    if (image != null)
                                                    {
                                                        Sitecore.Data.Fields.ImageField imagefield = ImagenewForm.Fields["Image"];
                                                        imagefield.Alt = image.Alt;
                                                        imagefield.MediaID = image.ID;
                                                        imagefield.Src = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image);

                                                    }
                                                    ImagenewForm.Editing.EndEdit();
                                                }
                                                else
                                                {
                                                    result = false;
                                                }
                                            }
                                            else
                                            {
                                                result = false;

                                            }
                                        }


                                        int imagecount = 2;



                                        for (int i = 1; i < 4; i++)
                                        {

                                            HttpPostedFile file = Request.Files["filename" + i];

                                            if (file != null)
                                            {
                                                if (file.FileName != "")
                                                {

                                                    string ext = System.IO.Path.GetExtension(file.FileName);
                                                    if (allowedExtension.Contains(ext) && file.ContentLength <= 1048576)
                                                    {

                                                        Sitecore.Resources.Media.MediaCreatorOptions optionsM = new Sitecore.Resources.Media.MediaCreatorOptions();
                                                        optionsM.Database = master;
                                                        optionsM.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                                                        optionsM.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                                                        filename = file.FileName.Replace(".jpg", "").Replace(".png", "").Replace(".gif", "").Replace(".Jpeg", "");
                                                        filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                                                        string Destination = "/sitecore/media library/SG50/PG/Story Images/" + ItemName + filename;
                                                        optionsM.Destination = Destination;
                                                        file.SaveAs(Server.MapPath(@"/SG50/layouts/PG/Story Images/") + ItemName + file.FileName);

                                                        optionsM.FileBased = false;
                                                        Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                                                        Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath(@"/SG50/layouts/PG/Story Images/") + ItemName + file.FileName, optionsM);


                                                        string path = newForm.Paths.Path.ToString();// Sitecore.Context.Item.Paths.Path.ToString();
                                                        Item ImageItem1 = master.GetItem(path + "/Images");



                                                        Sitecore.Data.Items.TemplateItem Imagetemplate = master.GetItem("/sitecore/templates/SG50/Pioneer Generation/PGStrory Image Banner");
                                                        Item ImageItem = master.GetItem(ImageItem1.ID.ToString());
                                                        string ImageItemName = "Image" + imagecount;
                                                        imagecount = imagecount + 1;
                                                        ImagenewForm = ImageItem.Add(ImageItemName.ToString(), Imagetemplate);
                                                        ImagenewForm.Editing.BeginEdit();
                                                        Sitecore.Data.Items.MediaItem image = item.Database.GetItem(Destination);
                                                        if (image != null)
                                                        {
                                                            Sitecore.Data.Fields.ImageField imagefield = ImagenewForm.Fields["Image"];
                                                            imagefield.Alt = image.Alt;
                                                            imagefield.MediaID = image.ID;
                                                            imagefield.Src = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image);

                                                        }
                                                        ImagenewForm.Editing.EndEdit();

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                result = false;
                                            }
                                        }



                                        var item1 = newForm;
                                        using (new Sitecore.SecurityModel.SecurityDisabler())
                                        {
                                            if (item1 != null)
                                            {

                                                var source = master;
                                                var target = Sitecore.Data.Database.GetDatabase("web");

                                                var options = new PublishOptions(source, target, PublishMode.SingleItem, item1.Language, DateTime.Now)
                                                {
                                                    RootItem = item1,
                                                    Deep = true,
                                                };

                                                var publisher = new Publisher(options);
                                                publisher.Publish();

                                            }
                                        }


                                        var itemMedia = master.GetItem("{2EC9C2D6-89FB-4C81-B9D7-3B4A7733701C}"); ;
                                        using (new Sitecore.SecurityModel.SecurityDisabler())
                                        {
                                            if (itemMedia != null)
                                            {

                                                var source = master;
                                                var target = Sitecore.Data.Database.GetDatabase("web");

                                                var options = new PublishOptions(source, target, PublishMode.SingleItem, itemMedia.Language, DateTime.Now)
                                                {
                                                    RootItem = itemMedia,
                                                    Deep = true,
                                                };

                                                var publisher = new Publisher(options);
                                                publisher.Publish();

                                            }
                                        }



                                    }
                                    string url = "~/SG50/Pioneer Generation/Thank You.aspx";
                                    if (!(url.Contains("://")))
                                        Response.Redirect(url);
                                }
                                else
                                {

                                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please Select Image');", true);
                                }
                            }
                            catch (Exception ee)
                            {
                                throw;

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Uploaded images size exceeded 1mb and dimensions must be above 1280x740 pixels.');", true);
                        }

                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please fill all the required fields.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Captcha Code');", true);
                }
            }
            else
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Data!');", true);
            }
            if (Session["fileNameOriginal"] != null)
                Session["fileNameOriginal"] = null;

            if (Session["fileNameCropped"] != null)
                Session["fileNameCropped"] = null;
        }

        private void CancelUnexpectedRePost()
        {
            string clientCode = _repostcheckcode.Value;

            //Get Server Code from session (Or Empty if null)
            string serverCode = Session["_repostcheckcode"] as string ?? "";

            if (!IsPostBack || clientCode.Equals(serverCode))
            {
                //Codes are equals - The action was initiated by the user
                //Save new code (Can use simple counter instead Guid)
                string code = Guid.NewGuid().ToString();
                _repostcheckcode.Value = code;
                Session["_repostcheckcode"] = code;
            }
            else
            {
                //Unexpected action - caused by F5 (Refresh) button
               // Response.Redirect(Request.Url.AbsoluteUri);
                string url = Request.Url.AbsoluteUri;
                Response.Redirect(url);
            }
        }

    }
}
