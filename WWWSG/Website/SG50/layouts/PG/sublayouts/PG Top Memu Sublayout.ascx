﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Pg_top_memu_sublayout.Pg_top_memu_sublayoutSublayout" CodeFile="~/SG50/layouts/PG/sublayouts/PG Top Memu Sublayout.ascx.cs" %>
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<header class="header">
    <div class="main-nav">
        <div class="SG-Logo">
            <a href="/en.aspx" class=""><span>
                <img src="/SG50/images/PG/logo.png" class="show-lg">
                <img src="/SG50/images/mobile/logo.png" class="show-sm">
            </span></a>
        </div>
        <div class="m-nav-icon">Menu</div>
        <ul class="main-nav-list">
            <%--<li><a href="/SG50/">SG50</a> </li>--%>
            <li><a href="/SG50/About.aspx">ABOUT</a>
            <ul>
                    <div class="custom-container">
                        <a href="/SG50/About#DivLittleDot" id="GUIDELINESlnk"><span>BRAND GUIDELINES</span></a>
                        <a href="/SG50/About#divinitiatives" id="INITIATIVESlnk"><span>INITIATIVES</span></a>
                        <a href="/SG50/About#DivMeetthecommitte" id="COMMITTEElnk"><span>COMMITTEE</span></a>
                    </div>
                </ul>
             </li>
            <li><a href="/SG50/WhatsOn.aspx">WHAT’S ON </a></li>
            <li><a href="/sg50/pulse">PULSE</a></li>
            <li><a href="/SG50/Celebration Fund and Ideas/Ground-Up Projects.aspx">CELEBRATION FUND & IDEAS </a>
                <ul>
                    <div class="custom-container">
                        <a href="/SG50/Celebration Fund.aspx">What is it? </a><%--<a href="/SG50/Apply For Funding.aspx"><span>APPLY FOR FUNDING</span> </a>--%><a href="/SG50/Celebration Fund and Ideas/Ground-Up Projects.aspx">Ground-Up Projects</a> <a href="/Sg50/Idea all page.aspx">Celebration Ideas</a>
                    </div>
                </ul>
            </li>
            <li><a href="/SG50/GalleryLanding/">GALLERY</a></li>
            <li><a href="/SG50/Collectibles.aspx">COLLECTIBLES</a></li>
            <li><a href="https://singapore50.mynewsdesk.com/">PRESS </a></li>

            <%--<li><a href="/SG50/Pioneer Generation.aspx" class="active">SINCE 1965</a> </li>
            <li><a href="http://www.iconsof.sg/">INITIATIVES </a>
                <ul>
                    <div class="custom-container"><a href="http://www.iconsof.sg/">Icons of SG </a></div>
                </ul>
            </li>--%>
        </ul>
        <%--        <div class="social_nav">
            <div class="facebook">
                <a target="_blank" href="http://www.facebook.com/sg2015">
                    <img src="/SG50/images/PG/icon-fc.png" class="show-lg">
                    <img src="/SG50/images/PG/fc-lg.png" class="show-sm">
                </a>
            </div>
            <div class="twitter">
                <a target="_blank" href="https://twitter.com/SG50">
                    <img src="/SG50/images/PG/icon-tw.png" class="show-lg">
                    <img src="/SG50/images/PG/tw-lg.png" class="show-sm">
                </a>
            </div>
            <div class="youtube">
                <a target="_blank" href="https://www.youtube.com/user/ilovesingapore50">
                    <img src="/SG50/images/PG/icon-share.png" class="show-lg">
                    <img src="/SG50/images/PG/play.png" class="show-sm"></a>
            </div>
            <div>
                <a target="_blank" href="https://www.instagram.com/singapore50/">
                    <img src="/SG50/images/Instagram-social-icon.jpg" class="show-lg">
                    <img src="/SG50/images/Instagram-social-icon.jpg" class="show-sm"></a>
            </div>
        </div>--%>
        <div class="social_nav social-NewStyle">
            <div class="facebook">
                <a href="https://www.facebook.com/sg2015" target="_blank">
                    <span class="NewFacebook"></span>
                </a>
            </div>
            <div class="twitter">
                <a href="https://twitter.com/SG50" target="_blank">
                    <span class="NewTwitter"></span>
                </a>
            </div>
            <div class="youtube">
                <a href="https://www.youtube.com/user/ilovesingapore50" target="_blank">
                    <span class="NewYouTube"></span>
                </a>
            </div>
            <div class="youtube">
                <a href="https://www.instagram.com/singapore50/" target="_blank">
                    <span class="NewInstagram"></span>
                </a>
            </div>
        </div>
</header>
