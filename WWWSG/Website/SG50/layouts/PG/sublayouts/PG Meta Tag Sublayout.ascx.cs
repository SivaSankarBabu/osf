﻿using System;
using Sitecore;
using Sitecore.Collections;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
namespace Layouts.Pg_meta_tag_sublayout
{

    /// <summary>
    /// Summary description for Pg_meta_tag_sublayoutSublayout
    /// </summary>
    public partial class Pg_meta_tag_sublayoutSublayout : System.Web.UI.UserControl
    {
        private Item itmMain = Sitecore.Context.Item;
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");

        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        private void Page_Load(object sender, EventArgs e)
        {
            FillinMetaTag();
        }
        private void FillinMetaTag()
        {
            string url = string.Empty;

            if (Session["ShareQuoteId"] != null)
            {
                string stitem = Session["ShareQuoteId"].ToString();
                itmMain = SG50Class.web.GetItem(stitem);
            }

            if (itmMain != null)
            {

                if (itmMain.Fields["MetaTitle"] != null && !itmMain["MetaTitle"].ToString().Trim().Equals(""))
                {
                    PGlitMetaTag.Text = "<meta name=\"Title\" content=\"" + SG50Class.StripUnwantedHtml(itmMain.Fields["MetaTitle"].ToString()) + "\" />";
                }

                if (itmMain.Fields["MetaDescription"] != null && !itmMain["MetaDescription"].ToString().Trim().Equals(""))
                {
                    PGlitMetaTag.Text += "<meta name=\"description\" content=\"" + SG50Class.StripUnwantedHtml(itmMain.Fields["MetaDescription"].ToString()) + "\" />";
                }

                if (itmMain.Fields["MetaKeywords"] != null && !itmMain["MetaKeywords"].ToString().Trim().Equals(""))
                {
                    PGlitMetaTag.Text += "<meta name=\"keywords\" content=\"" + SG50Class.StripUnwantedHtml(itmMain.Fields["MetaKeywords"].ToString()) + "\" />";
                }

                PGlitMetaTag.Text += "<meta property=\"fb:app_id\" content=\"" + Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() + "\" />";

                PGlitMetaTag.Text += "<meta property=\"og:type\" content=\"website\" />";
                PGlitMetaTag.Text += "<meta property=\"og:site_name\" content=\"sg2015\" />";

                string pdesc = "";
                string sdesc = "";

                if (itmMain.Fields["Pioneers Description"] != null && !itmMain["Pioneers Description"].ToString().Trim().Equals(""))
                {
                    pdesc = itmMain.Fields["Pioneers Description"].ToString();
                    pdesc = pdesc.Substring(0, (pdesc.Length > 70 ? 70 : pdesc.Length));
                    PGlitMetaTag.Text += "<meta property=\"og:title\" content=\"" + pdesc + "\" />";
                }

                if (itmMain.Fields["Story Description"] != null && !itmMain["Story Description"].ToString().Trim().Equals(""))
                {
                    sdesc = itmMain.Fields["Story Description"].ToString();
                    sdesc = SG50Class.StripUnwantedHtml(sdesc);
                    //sdesc = sdesc.Substring(0, (sdesc.Length > 241 ? 241 : sdesc.Length));
                    sdesc = sdesc.Substring(0, sdesc.IndexOf('.',sdesc.IndexOf('.')+1 )+1);
                    PGlitMetaTag.Text += "<meta property=\"og:description\" content=\"" +  sdesc + "\" />";
                }
                PGlitMetaTag.Text += "<meta property=\"og:url\" content=\"" + hostName  +"/SG50/Pioneer Generation".Replace(" ", "%20") + LinkManager.GetItemUrl(itmMain).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "").Replace(" ", "%20") + "\"  />";

                string path = itmMain.Paths.Path.ToString();
                /*Item ImageItem = web.GetItem(path + "/Images");
                ChildList ImagesCount = null;
                if (ImageItem != null && ImageItem.Versions.Count > 0)
                {
                    ImagesCount = ImageItem.GetChildren();
                    for (int i = 0; i < ImagesCount.Count; i++)
                    {
                        if (ImagesCount.InnerChildren[i].Name == "Original")
                        {

                            Sitecore.Data.Fields.ImageField image = ImagesCount[i].Fields["Image"];
                            if (image.MediaItem != null)
                            {
                                MediaUrlOptions options = new MediaUrlOptions();
                                options.BackgroundColor = System.Drawing.Color.White;
                                url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                            }
                        }

                    }
                }*/
		

		Item ImageItemThumb = web.GetItem(path + "/Images/Thumbnail");


                if (ImageItemThumb  != null)
                {
                    Sitecore.Data.Fields.ImageField image = ImageItemThumb.Fields["Image"];
                    if (image.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                         
                    }
                }

                PGlitMetaTag.Text += "<meta property=\"og:image\" content=\"" + hostName + url.Replace(" ", "%20").Replace("?bc=White","") + "\"/>";


                PGlitMetaTag.Text += "<meta property=\"twitter:card\" content=\"" + "summary_large_image" + "\" />";
                PGlitMetaTag.Text += "<meta property=\"twitter:site\" content=\"" + "@singapore50" + "\" />";
                PGlitMetaTag.Text += "<meta property=\"twitter:creator\" content=\"" + "@singapore50" + "\" />";
                if (itmMain.Fields["Pioneers Description"] != null && !itmMain["Pioneers Description"].ToString().Trim().Equals(""))
                    PGlitMetaTag.Text += "<meta property=\"twitter:title\" content=\"" + pdesc + "\" />";
                if (itmMain.Fields["Story Description"] != null && !itmMain["Story Description"].ToString().Trim().Equals(""))
                    PGlitMetaTag.Text += "<meta property=\"twitter:description\" content=\"" + sdesc + "\" />";

                if (Request["mitem"] != null && Request["mitem"].ToString() != "")
                {
                    var itemMedia = master.GetItem( Request["mitem"] );
                    //var itemMedia = master.GetItem("{" + Request["mitem"] + "}");
                    url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(itemMedia);
                }

                PGlitMetaTag.Text += "<meta property=\"twitter:image\" content=\"" + hostName + url.Replace(" ", "%20").Replace("?bc=White","") + "\"/>";
               
		//PGlitMetaTag.Text += "<meta property=\"og:image\" content=\"" + hostName + url.Replace(" ", "%20").Replace("?bc=White","") + "\"/>";

      

            }
        }
    }
}