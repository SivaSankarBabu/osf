﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Pgstory_sublayout.Pgstory_sublayoutSublayout" CodeFile="~/SG50/layouts/PG/sublayouts/PGStory Sublayout.ascx.cs" %>
<!-- Crop CSS -->
<link rel="stylesheet" type="text/css" href="/SG50/include/PG/css/crop/image-crop.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/PG/css/crop/jquery.Jcrop.css" />
<script type="text/javascript">
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57 || keyCode >= 65 && keyCode <= 90 || keyCode >= 97 && keyCode <= 122 || keyCode == 9 || keyCode == 32) || specialKeys.indexOf(keyCode) != -1);
        return ret;
    }
</script>
<section class="share-story-section">
    <div class="custom-container">
        <h3>
            <asp:HiddenField runat="server" ID="_repostcheckcode" />
            <sc:Text ID="txtTitle" Field="Story Title" runat="server" />
        </h3>
        <p>
            <sc:Text ID="txtDescription" Field="Story Description" runat="server" />
        </p>

        <h5 class="h5">
            <sc:Text ID="txtPioneerTitle" Field="pioneer Title" runat="server" />
        </h5>

        <div class="fields-group">
            <label><sup>*</sup><sc:Text ID="txtQuestionsHeading" Field="Questions Heading" runat="server" />
            </label>
            <ul class="questions-list">
                <sc:FieldRenderer ID="txtQuestions" runat="server" FieldName="Questions" />
            </ul>
            <asp:TextBox ID="txtStoryArea" placeholder="Max 2000 characters" TextMode="MultiLine" runat="server" CssClass="form-textarea ValidationRequired"></asp:TextBox>

        </div>
        <div class="fields-group">
            <label><sup>*</sup>What’s the one thing he/she would tell the next generation of Pioneers?</label>
            <asp:TextBox ID="txtPioneersArea" placeholder="Max 140 characters" TextMode="MultiLine" runat="server" CssClass="form-textarea form-textarea140 ValidationRequired"></asp:TextBox>

        </div>

        <div class="fields-group" style="display: none">
            <div class="form-col2">
                <label><sup>*</sup>Categories</label>
                <asp:DropDownList ID="ddlCategories" runat="server" CssClass="form-select ValidationRequired">

                    <asp:ListItem Value="2" Text="Kampong Chief"></asp:ListItem>
                    <asp:ListItem Value="3" Text="The Rocksteady"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Trailblazer"></asp:ListItem>
                    <asp:ListItem Value="5" Text="The Giver"></asp:ListItem>
                    <asp:ListItem Value="6" Text="The Die-Hard"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="fields-group">
            <div class="form-col2">
                <label><sup>*</sup>Pioneer's Name</label>
                <asp:TextBox ID="txtPioneerName" onkeypress="return IsNumeric(event);" AutoCompleteType="Disabled" autocomplete="off" placeholder="100 characters" runat="server" CssClass="form-select ValidationRequired"></asp:TextBox>
            </div>
            <div class="form-col2-right">
                <label><sup>*</sup>Age</label>
                <asp:DropDownList ID="ddlAge" runat="server" CssClass="form-select">
                    <asp:ListItem Value="1" Text="50-60"></asp:ListItem>
                    <asp:ListItem Value="2" Text="60-70"></asp:ListItem>
                    <asp:ListItem Value="3" Text="70-80"></asp:ListItem>
                    <asp:ListItem Value="4" Text="80 & above"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="fields-group browse-group">
            <div class="form-col2">
                <label><sup>*</sup>Make it real, attach a photo of your pioneer</label>

                <!--<input class="form-select" type="file"> -->
                <div id="fileName" class="crop-file-name">
                    <span class="format">Format acceptable: JPG, PNG; File size limit: 1MB</span>
                </div>

                <input type="hidden" id="crop_image_hidden" name="cropped_image_name" value="">

                <div class="cropfile-valid" id="filetype"></div>

            </div>
            <div class="form-col2-right width100">
                <label class="visible-sm visible-lg visible-md">&nbsp;</label>
                <div class="crop-lg">
                    <div class="cropme">
                        <a href="javascript:void();" class="file-button">BROWSE</a>
                    </div>

                </div>
                <div class="crop-sm">
                    <div id="area">
                        <div>
                            <input type="hidden" id="crop-image-hidden-mobile" name="cropped_image_name_mobile" value="">
                            <input type="hidden" id="crop-original-image-hidden-mobile" name="cropped-original-image-name-mobile" value="">
                            <asp:HiddenField ID="txtb4" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="txtOriginalb4" runat="server"></asp:HiddenField>
                            <input name="photo" id="photo" type="file" />
                            <u>BROWSE</u>

                        </div>
                    </div>
                    <img src="/Sg50/images/PG/1415904037_Streamline-70-16.png" id="deleteImageMobile" class="MobileImgDel" />
                </div>
            </div>
            <div class="fields-group paddingbtm0 cropmobile">
                <div class="form-col2 recommend">
                    <p><span></span></p>
                </div>
                <div class="form-col2-right">&nbsp;</div>

            </div>


            <div class="fields-group paddingbtm0">
                <div class="form-col2 recommend">Recommended dimension 1280x740 pixels.</div>
                <div class="form-col2-right mobilehide">&nbsp;</div>
                <div class="form-col2 recommend mobilehide">
                    <div class="img-croping" id="Test" style="display: none;"></div>
                    <img src="/Sg50/images/PG/1415904037_Streamline-70-16.png" id="deleteImage" class="deleteicon" />
                </div>
                <div class="form-col2-right mobilehide">&nbsp;</div>
            </div>
        </div>
        <input type="hidden" id="BrowseCount" value="0">
        <div class="fields-group browse-group browse-option1">
            <div class="form-col2">
                <div id="fileName-1" class="crop-file-name" style="color: #929191;">Format acceptable: JPG, PNG; File size limit: 1MB</div>
                <div class="cropfile-valid" id="filetype-1"></div>
            </div>
            <div class="form-col2-right">
                <div class="customBrowse-group">
                    <input type="file" name="filename1" class="customBrowse" id="browseOne">
                    <a href="javascript:void();" class="file-button">BROWSE</a>
                </div>
                <div class="form-remove-buuton1">
                    <a href="javascript:void();"></a>
                </div>
            </div>
        </div>


        <div class="fields-group browse-group browse-option2">
            <div class="form-col2">
                <div id="fileName-2" class="crop-file-name" style="color: #929191;">Format acceptable: JPG, PNG; File size limit: 1MB</div>
                <div class="cropfile-valid" id="filetype-2"></div>
            </div>
            <div class="form-col2-right">
                <div class="customBrowse-group">
                    <input type="file" name="filename2" class="customBrowse" id="browseTwo">
                    <a href="javascript:void();" class="file-button">BROWSE</a>
                </div>
                <div class="form-remove-buuton2">
                    <a href="javascript:void();"></a>
                </div>
            </div>
        </div>


        <div class="fields-group browse-group browse-option3">
            <div class="form-col2">
                <div id="fileName-3" class="crop-file-name" style="color: #929191;">Format acceptable: JPG, PNG; File size limit: 1MB</div>
                <div class="cropfile-valid" id="filetype-3"></div>
            </div>
            <div class="form-col2-right">
                <div class="customBrowse-group">
                    <input type="file" name="filename3" class="customBrowse" id="browseThree">
                    <a href="javascript:void();" class="file-button">BROWSE</a>
                </div>
                <div class="form-remove-buuton3">
                    <a href="javascript:void();"></a>
                </div>
            </div>
        </div>


        <div class="form-add-buuton">
            <a href="javascript:void();">Add another photo (optional)</a>
        </div>

        <h5 class="h5 show-lg">Your Profile</h5>
        <h5 class="h5 show-sm ">Your Profile</h5>
        <div class="fields-group">
            <div class="form-col2">
                <label><sup>*</sup>Your Name</label>
                <asp:TextBox ID="txtName" AutoCompleteType="Disabled" autocomplete="off" placeholder="100 characters" runat="server" CssClass="form-select ValidationRequired"></asp:TextBox>
            </div>
            <div class="form-col2-right">
                <label><sup>*</sup>Email Address</label>
                <asp:TextBox ID="txtEmail" AutoCompleteType="Disabled" autocomplete="off" placeholder="100 characters" runat="server" CssClass="form-select"></asp:TextBox>

            </div>
        </div>

        <hr class="hr">

        <div class="fields-group">
            <div class="form-chk" id="DivTerms" runat="server" visible="false">
                <input class="form-checkbox" type="checkbox" name="t_c">

                <div class="chk-label">
                    <sup>*</sup><sc:Text ID="Conditions" Field="TC Text" runat="server" />
                    <sc:Link Field="TC URL" runat="server" ID="hyplknTC">
                        <sc:Text Field="TC URL Text" runat="server" ID="txtterms" />
                    </sc:Link>
                </div>
            </div>
            <div class="form-chk" id="DivAgreement" runat="server" visible="false">
                <input class="form-checkbox" type="checkbox" name="form_of_agreement">
                <div class="chk-label">
                    <sup>*</sup><sc:Text ID="Text1" Field="Agreement Text" runat="server" />

                    <sc:Link Field="Agreement URL" runat="server" ID="Link1">
                        <sc:Text Field="Agreement URL Text" Style="color: red" runat="server" ID="Text2" />
                    </sc:Link>
                </div>
            </div>

        </div>
        <br />
        <br />
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txt_CaptchaVal" runat="server" Visible="false"></asp:TextBox>
                    <asp:Image ID="Image1" runat="server" />

                    <asp:TextBox ID="txt_Captcha" MaxLength="4" Style="vertical-align: top" placeholder="Enter Captcha" Width="200px" runat="server" CssClass="form-select"></asp:TextBox>

                    <asp:ImageButton ID="Button1" Style="vertical-align: top" runat="server" ImageUrl="/SG50/images/PG/refreshIcon.gif"
                        CausesValidation="false" Text="refresh" CssClass="cancel" OnClick="Button1_Click1" />
                    <br />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Button1" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                ErrorMessage="Please enter Captcha" Style="font-size: 14px; font-weight: bolder" ForeColor="Red" ControlToValidate="txt_Captcha"></asp:RequiredFieldValidator>
        </div>
        <div class="fields-group form-submit">

            <asp:Button CssClass="submit-button" ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />

        </div>

        <div class="clearfix"></div>
    </div>
</section>

<script src="/SG50/include/PG/js/jquery.min.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" src="/SG50/include/PG/scripts/jquery.FileReader.js"
    language="javascript"></script>

<script type="text/javascript" src="/SG50/include/PG/scripts/jquery.Jcrop.js" language="javascript"></script>

<script type="text/javascript" src="/SG50/include/PG/scripts/jquery.SimpleCropper.js"
    language="javascript"></script>

<style>
    .error {
        color: #FF0000;
        padding-top: 5px;
        display: inline-block;
    }

    #t_c-error {
        float: left;
        display: block;
        position: absolute;
        margin-top: 19px;
    }

    #form_of_agreement-error {
        float: left;
        display: block;
        position: absolute;
        margin-top: 19px;
    }
</style>

<script>

    // Init Simple Cropper
    $('.cropme').simpleCropper();

</script>

<script type="text/javascript" language="javascript">

    // Button Browse One
    $("#browseOne").change(function (e) {
        var file = e.target.files[0];
        $('#fileName-1').empty().append(file.name);
        $("#filetype-1").empty();
        if (file.type != "image/jpeg" && file.type != "image/png") {
            $("#filetype-1").empty().append('Please Enter Valid Image File (jpeg, png)');
            return false;
        }
        if (file.size > 1048576) {
            $("#filetype-1").empty().append('Uploaded file size not permitted (max 1 MB)');
            return false;
        }
    });

    // Button Browse Two
    $("#browseTwo").change(function (e) {
        var file = e.target.files[0];
        $('#fileName-2').empty().append(file.name);
        $("#filetype-2").empty();
        if (file.type != "image/jpeg" && file.type != "image/png") {
            $("#filetype-2").empty().append('Please Enter Valid Image File (jpeg, png)');
            return false;
        }
        if (file.size > 1048576) {
            $("#filetype-2").empty().append('Uploaded file size not permitted (max 1 MB)');
            return false;
        }
    });

    // Button Browse Three
    $("#browseThree").change(function (e) {
        var file = e.target.files[0];
        $('#fileName-3').empty().append(file.name);
        $("#filetype-3").empty();
        if (file.type != "image/jpeg" && file.type != "image/png") {
            $("#filetype-3").empty().append('Please Enter Valid Image File (jpeg, png)');
            return false;
        }
        if (file.size > 1048576) {
            $("#filetype-3").empty().append('Uploaded file size not permitted (max 1 MB)');
            return false;
        }
    });





    $(".form-add-buuton a").click(function (e) {

        var bc = $('#BrowseCount').val();
        //    alert(bc);
        if (bc == 0) {

            $('.browse-option1').css('display', 'inline-block');
            $('#BrowseCount').val(1);

        } else if (bc == 1) {

            if ($('.browse-option2:visible').length == 0) {
                $('.browse-option2').css('display', 'inline-block');
                $('#BrowseCount').val(2);
            }
            else {
                $('.browse-option3').css('display', 'inline-block');
                $('#BrowseCount').val(2);
            }

        } else if (bc == 2) {
            if ($('.browse-option2:visible').length == 0) {
                $('.browse-option2').css('display', 'inline-block');
                $('#BrowseCount').val(3);
                $('.form-add-buuton').hide();
            }
            else if ($('.browse-option3:visible').length == 0) {
                $('.browse-option3').css('display', 'inline-block');
                $('#BrowseCount').val(3);
                $('.form-add-buuton').hide();
            }
            else {
                $('.browse-option1').css('display', 'inline-block');
                $('#BrowseCount').val(3);
                $('.form-add-buuton').hide();
            }
        }




    });


    $(".form-remove-buuton1 a").click(function (e) {
        var bc = $('#BrowseCount').val();



        $('.browse-option1').css('display', 'none');
        //$('.browse-option2').attr('id', 'browse-option1');
        //$('.browse-option3').attr('id', 'browse-option2');
        $('.form-add-buuton').show();
        $('#BrowseCount').val(bc - 1);
        $('#fileName-1').html('Format acceptable: JPG, PNG; File size limit: 1MB');
        $('#browseOne').val('');



    });

    $(".form-remove-buuton2 a").click(function (e) {
        var bc = $('#BrowseCount').val();

        $('.browse-option2').css('display', 'none');
        //$('.browse-option3').attr('id', 'browse-option2');
        $('.form-add-buuton').show();
        $('#BrowseCount').val(bc - 1);
        $('#fileName-2').html('Format acceptable: JPG, PNG; File size limit: 1MB');
        $('#browseTwo').val('');
    });


    $(".form-remove-buuton3 a").click(function (e) {

        var bc = $('#BrowseCount').val();
        $('.browse-option3').css('display', 'none');
        $('.form-add-buuton').show();
        $('#BrowseCount').val(bc - 1);
        $('#fileName-3').html('Format acceptable: JPG, PNG; File size limit: 1MB');
        $('#browseThree').val('');

    });



</script>

<script src="/SG50/include/PG/js/jquery.validate.min.js" type="text/javascript" language="javascript"></script>

<script src="/SG50/include/PG/js/bootstrap.min.js" type="text/javascript" language="javascript"></script>

<!--Mobile Crop Img -->

<script src="/SG50/include/PG/scripts/zepto.min.js" type="text/javascript" language="javascript"></script>

<script type="text/javascript" language="javascript">



    $(document).ready(function () {


        $("#mainform").validate({
            ignore: [],
            rules: {
                '<%=txtEmail.UniqueID%>': {
                    required: true,
                    email: true
                },
                '<%=txtName.UniqueID%>': {
                    required: true,
                    maxlength: 100
                },
                '<%=txtPioneerName.UniqueID%>': {
                    required: true,
                    maxlength: 100
                },
                '<%=txtStoryArea.UniqueID%>': {
                    required: true,
                    maxlength: 2000
                },
                '<%=txtPioneersArea.UniqueID%>': {
                    required: true,
                    maxlength: 140
                },
                t_c: {
                    required: true
                },
                form_of_agreement: {
                    required: true
                }
                //                ,
                //                cropped_image_name: {
                //                    required: true
                //                }
                //                ,
                //                cropped_image_name_mobile: {
                //                    required: true
                //                }

            }, // Specify the validation error messages
            messages: {
                '<%=txtEmail.UniqueID%>': {
                    required: "Please enter Email Address",
                    email: "Please enter a valid Email Address"
                },
                '<%=txtName.UniqueID%>': {
                    required: "Please enter Your Name",
                    maxlength: "Your name must not be exceeds 100 characters long"
                },
                '<%=txtPioneerName.UniqueID%>': {
                    required: "Please enter Pioneer's Name",
                    maxlength: "Pioneer's must not be exceeds 100 characters long"
                },
                '<%=txtStoryArea.UniqueID%>': {
                    required: "Please enter information",
                    maxlength: "Information must not be exceeds 2000 characters long"
                },
                '<%=txtPioneersArea.UniqueID%>': {
                    required: "Please enter information",
                    maxlength: "Information must not be exceeds 140 characters long"
                },

                //                cropped_image_name: {
                //                    required: "Please Select Image"
                //                }
                // ,
                //                cropped_image_name_mobile: {
                //                    required: "Please Select Image"
                //                },
                submitHandler: function (mainform) {

                    mainform.submit();
                }

            }
        });
        // delete code for image delete for desktop
        $('#deleteImage').click(function () {
            $('#Test').empty();
            $('#crop_image_hidden').val('');
            $("#filetype").empty();
            $('#fileName').empty().html('<span class="format">Format acceptable: JPG, PNG; File size limit: 5MB</span>');
            $('#Test').hide();
            $('#deleteImage').hide();
        });
        // delete code for image delete for mobile
        $('#deleteImageMobile').click(function () {
            $('#area img').remove();
            $('#crop-image-hidden-mobile').val('');
            $("#filetype").empty();
            $('#fileName').empty().html('<span class="format">Format acceptable: JPG, PNG; File size limit: 5MB</span>');
            $('#deleteImageMobile').hide();
        });
        $('#area u').click(function () {
            $('input[name=photo]').trigger('click');
        });

        $('input[name=photo]').change(function (e) {
            var file = e.target.files[0];
            var reader = new FileReader();

            reader.onloadend = function () {
                //$('#crop-original-image-hidden-mobile').val(reader.result);
                var txtOriginalb5 = $("[id$='txtOriginalb4']");
                txtOriginalb5.val(reader.result);

            }
            reader.readAsDataURL(file);


            $("#filetype").empty();
            $('#fileName').empty().append(file.name);


            //alert(reader.result);
            //image format and size validation
            if (file.type != "image/jpeg" && file.type != "image/png") {
                $("#filetype").empty().append('Please Enter Valid Image File (jpeg, png)');
                $('#area img').remove();
                $('#crop-image-hidden-mobile').val('');
                $('#deleteImageMobile').hide();
                return false;
            }
            if (file.size > 5000000) {
                $("#filetype").empty().append('Uploaded file size not permitted (max 5 MB)');
                $('#area img').remove();
                $('#crop-image-hidden-mobile').val('');
                $('#deleteImageMobile').hide();
                return false;
            }


            // RESET
            $('#area p span').css('width', 0 + "%").html('');
            $('#area img, #area canvas').remove();
            $('#area i').html(JSON.stringify(e.target.files[0]).replace(/,/g, ", <br/>"));

            // CANVAS RESIZING
            canvasResize(file, {
                width: 376,
                height: 376,
                crop: true,
                quality: 100,
                rotate: 0,
                callback: function (data, width, height) {

                    // SHOW AS AN IMAGE
                    // =================================================
                    var img = new Image();
                    img.onload = function () {

                        $(this).css({
                            'margin': '10px auto',
                            'width': width,
                            'height': height
                        }).appendTo('#area div');

                    };

                    $(img).attr('src', data);
                    $('#crop-image-hidden-mobile').val(data);
                    var txtb5 = $("[id$='txtb4']");
                    // $('#cropped_image_name').val("jpg");
                    txtb5.val(data);

                    $('#crop-image-hidden-mobile-error').hide();
                    $('#crop_image_hidden-error').hide();
                    $('#deleteImageMobile').show();



                    // /SHOW AS AN IMAGE
                    // =================================================


                    // IMAGE UPLOADING
                    // =================================================

                    // Create a new formdata
                    //                    var fd = new FormData();
                    //                    // Add file data
                    //                    var f = canvasResize('dataURLtoBlob', data);
                    //                    f.name = file.name;
                    //                    fd.append($('#area input').attr('name'), f);



                    // /IMAGE UPLOADING
                    // =================================================               
                }
            });

        });
    });
</script>

<script src="/SG50/include/PG/scripts/binaryajax.js" type="text/javascript" language="javascript"></script>

<script src="/SG50/include/PG/scripts/exif.js" type="text/javascript" language="javascript"></script>

<script src="/SG50/include/PG/scripts/canvasResize.js" type="text/javascript" language="javascript"></script>

<script src="/SG50/include/PG/js/placeholders.min.js" type="text/javascript" language="javascript"></script>

