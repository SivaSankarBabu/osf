﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Pg_footer_menu_sublayout.Pg_footer_menu_sublayoutSublayout" CodeFile="~/SG50/layouts/PG/sublayouts/PG Footer Menu Sublayout.ascx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">

<footer class="footer-section">
<script src="/SG50/include/PG/js/common.js"></script>
<script type="text/javascript">
    Prevention();
    $(function() { if ($('.pics-gallery li').length == 2) { $('.pics-gallery').addClass('secondimg') } if ($('.pics-gallery li').length == 1) { $('.pics-gallery').addClass('singleimg') } })
</script>
    <div class="custom-container">
        <div class="footer-nav">
            <a href="/SG50/Contact Us.aspx">Contact us</a> <span>|</span>
            <a href="#" onclick="ShowModalPopup('dvPopup'); return false; ">Frequently Asked Questions (faq)</a> <span>|</span>
            <a href="#" onclick="ShowModalPopup('dvHouseRulesPopup'); return false; ">House Rules</a> <span>|</span>
            <%--<a href="/SG50/Sponsors.aspx">Sponsors</a> <span>|</span>--%>
            <a href="#" onclick="ShowModalPopup('dvAckPopup'); return false; ">Acknowledgement</a> <span>|</span>
            <a href="/SG50/Sitemap.aspx">Sitemap</a>
        </div>

        <p class="copy">Copyright &copy;  Ministry of Culture, Community and Youth. All rights reserved. </p>
        <div class="clearfix"></div>
    </div>
</footer>
<script language="javascript" type="text/javascript" src="/SG50/include/PG/js/modalpopupforfaq.js"></script>
<link href="/SG50/include/css/style.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<style>
       @media screen and (min-width: 300px) and (max-width: 768px) {


    /*Footer*/
    .footer {
        clear: both;
        width: 100%;
        background-color: #353334;
        height: 235px;
        padding-bottom: 10px;
    }

        .footer #divider {
            display: none;
        }

        .footer .footer_nav {
            position: relative;
            top: 15px;
        }

    .footer_nav a span {
        float: left;
        width: 100%;
        text-decoration: none;
        text-align: center;
        color: #ffffff;
        font-size: 0.938em;
        padding: 10px 0;
        margin-left: 2%;
        margin-right: 2%;
        width: 98%;
    }

    .footer p {
        width: 100%;
        text-align: center;
        font-size: 0.750em;
        margin-top: 25px;
        margin-right: 0;
    }

    .footer #cpDesktop {
        display: none;
    }

    .footer #cpMobile {
        display: block;
    }





    .questionMark {
        padding-left: 10px !important;
    }

    .questionText {
        width: 75% !important;
    }

    .txtSearch, .learnMore {
        width: 90% !important;
    }

    #rightSide #noOfIdeas {
        width: 100%;
    }

    .btnSearch {
        left: 85%;
        top: 22px;
        position: absolute;
    }

    .btnSearchMob {
    }

    #learnMorePopUp {
        width: 50% !important;
        margin-left: -40%!important;
    }

    .learnMorePopUpTitle, .learnMorePopUpContent {
        width: 100%;
    }

    #dvPopup, #dvPopup2, #dvHouseRulesPopup, #dvAckPopup {
        width: 100%;
    }

    #FontTitle, #FontTitle2 {
        left: 10px;
        width: 80%;
        font-size: 36px;
    }

    #FontTitle, .FontTitle, .FontTitleHR1, .FontTitleHR {
        font-size: 36px;
        left: 10px;
    }

    .FontTitle1, .FontTitle2, .FontTitle3 {
        font-size: 34px;
        width: 95%;
    }

    #Qmark, #Qmark2 {
        right: 5px;
        top: 55px;
        width: 50px;
    }

    #Qmark2 {
        right: 5px;
        top: 55px;
        width: 50px;
    }

        #Qmark img, #Qmark2 img {
            width: 100%;
        }

    .faqBlock {
        left: 10px;
        width: 97%;
    }

        .faqBlock p {
            font-size: 90%;
        }

    .faqTitle, .faqContents {
        width: 97%;
    }

        .faqContents p, .faqTitle p {
            width: 100%;
            margin-left: 0px;
        }

        .faqContents ul {
            font-size: 90%;
        }

    #FontTitleHR {
        left: 10px;
        width: 95%;
    }

        #FontTitleHR p {
            width: 100%;
            font-size: 30px;
        }

    .houseRulesContents, .ackContents {
        width: 96%;
        margin-left: 10px;
        font-size: 85%;
    }
}
    </style>
  <!-- FAQs -->
    <div id="dvPopup">
        <a href="#" onclick="HideModalPopup('dvPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle">
            <p><sc:Text ID="sct_FAQTitle" runat="server" DataSource="{8F9DC4B9-8D30-4F6D-AD4D-523759BDBC3C}" Field="Value" /></p>
        </div>
        <div id="Qmark">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <!-- FAQs Repeater -->
                <asp:Repeater ID="repFAQs" OnItemDataBound="repFAQs_OnItemDataBound" runat="server">
                    <ItemTemplate>
                        <div id="<%# DataBinder.Eval(Container.DataItem, "FAQ_ID_No") %>" class="faqTitle" onclick="<%# DataBinder.Eval(Container.DataItem, "FAQ_OnClick") %>">
                            <p>
                                <a href="#" ><img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_No") %>. <%# DataBinder.Eval(Container.DataItem, "FAQ_Question") %>
                            </p>
                        </div>
                        <div id="<%# DataBinder.Eval(Container.DataItem, "FAQ_Con_ID") %>" class="faqContents" style="display: none">
                            <p><%# DataBinder.Eval(Container.DataItem, "FAQ_Answer") %></p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <!-- End of FAQs Repeater -->
            </div>
        </div>
    </div>
    <!-- End FAQs -->

    <!-- Celebration Fund FAQs -->
    <div id="dvPopup2">
        <a href="#" onclick="HideModalPopup('dvPopup2'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle2">
			<p><p><sc:Text ID="sct_celebrationFundTitle" runat="server" DataSource="{22567F50-0BA2-4CA4-B4FC-271D2E897433}" Field="Value" /></p></p>
        </div>
        <div id="Qmark2">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <!-- Celebration Fund FAQs -->
                <asp:Repeater ID="repCFFAQs" OnItemDataBound="repCFFAQs_OnItemDataBound" runat="server">
                    <ItemTemplate>
                        <div id='<%# DataBinder.Eval(Container.DataItem, "FAQ_ID_No") %>' class="faqTitle" onclick="<%# DataBinder.Eval(Container.DataItem, "FAQ_OnClick") %>">
                            <p>
                                <a href="#" ><img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                                <%# DataBinder.Eval(Container.DataItem, "FAQ_No") %>. <%# DataBinder.Eval(Container.DataItem, "FAQ_Question") %>
                            </p>
                        </div>
                        <div id='<%# DataBinder.Eval(Container.DataItem, "FAQ_Con_ID") %>' class="faqContents" style="display: none">
                            <p><%# DataBinder.Eval(Container.DataItem, "FAQ_Answer") %></p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <!-- End  of Celebration Fund FAQs -->
                <br />
            </div>
        </div>
    </div>
    <!-- End Celebration Fund FAQs -->

    <!--  House Rules Sub Page Start -->
    <div id="dvHouseRulesPopup">
        <a href="#" onclick="HideModalPopup('dvHouseRulesPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div class="FontTitleHR">
			<p><sc:Text ID="sct_HouseRulesTitle" runat="server" DataSource="{EAB352F0-4E79-48C6-A646-06CD5310B7E2}" Field="Description" /></p>
        </div>
        <div class="houseRulesContents">
            <div class="aboutContent">
			    <sc:Text ID="sct_HouseRulesContent" runat="server" DataSource="{EAB352F0-4E79-48C6-A646-06CD5310B7E2}" Field="Value" />
            </div>       
        </div>
    </div>
    <!--  House Rules Sub Page End -->

    <!--  Acknowledgement Start -->
    <div id="dvAckPopup">
        <a href="#" onclick="HideModalPopup('dvAckPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div class="FontTitleHR">
            <p class="FontTitleHR1"><sc:Text ID="sct_AckTitle" runat="server" DataSource="{7B218AA3-5F40-4369-AEAF-78CA52D4F8F9}" Field="Description" /> </p>
        </div>
        <div class="ackContents">
			<div class="aboutContent">
				<sc:Text ID="sct_AckContent" runat="server" DataSource="{7B218AA3-5F40-4369-AEAF-78CA52D4F8F9}" Field="Value" />
			</div>
        </div>
    </div>
    <!--  Acknowledgement Sub Page End -->