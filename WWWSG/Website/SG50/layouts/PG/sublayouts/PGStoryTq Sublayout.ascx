﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Pgstorytq_sublayout.Pgstorytq_sublayoutSublayout" CodeFile="~/SG50/layouts/PG/sublayouts/PGStoryTq Sublayout.ascx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<section class="thankyou-section">  
 <div class="custom-container"> 
 <div class="thankyou">
 <span><sc:Text ID="txtTqTitle" Field="Thank You Title" runat="server" /></span>
 <sc:Text ID="txtTqContent" Field="Thank You Content" runat="server" />
 <br /><br />
  <div><span><a id="back2home" runat="server" onserverclick="back2homeclk" class="sg-button"><< Back to Home</a></span></div>
 </div>
 
 <div class="clearfix"></div>
 </div></section>
<div class="clearfix">
</div>
