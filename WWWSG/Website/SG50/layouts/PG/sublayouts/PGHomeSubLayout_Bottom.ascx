﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Pghomesublayout_bottom.Pghomesublayout_bottomSublayout" CodeFile="~/SG50/layouts/PG/sublayouts/PGHomeSubLayout_Bottom.ascx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<asp:ScriptManager ID="ScriptManager1" runat="server" />
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSendMail" />
    </Triggers>
    <ContentTemplate>
        <section class="sorting-bg">  
 <div class="custom-container"> 
 <div class="sort-block">
 <span>SORT BY:</span>
   <asp:LinkButton ID="lbAll" runat="server" Text="ALL" ToolTip="ALL" OnClick="lbAll_Click" OnClientClick="RESET();"></asp:LinkButton>
                <asp:LinkButton ID="lbSortRecent" runat="server" Text="RECENT" ToolTip="Recent" OnClick="lbSortRecent_Click" OnClientClick="RESET();"></asp:LinkButton>
                <asp:LinkButton ID="lbPopular" runat="server" Text="POPULAR" ToolTip="Popular" OnClick="lbPopular_Click" OnClientClick="RESET();"></asp:LinkButton>
 </div>
 
 <div class="categories-list" style="display:none"> <a  class="link-categores"> Select Categories </a>
    <div class="categories-options"> 
       <a href="javascript:;" runat="server" id="KampongChief" onserverclick="Story_Categories_List1"  title="Kampong Chief">Kampong Chief</a>
       <a href="javascript:;" runat="server" id="TheRocksteady" onserverclick="Story_Categories_List2" title="The Rocksteady">The Rocksteady</a>
       <a href="javascript:;" runat="server" id="Trailblazer" onserverclick="Story_Categories_List3" title="Trailblazer">Trailblazer</a>
       <a href="javascript:;" runat="server" id="TheGiver" onserverclick="Story_Categories_List4" title="The Giver">The Giver</a>
       <a href="javascript:;" runat="server" id="TheDieHard" onserverclick="Story_Categories_List5" title="The Die-Hard">The Die-Hard</a>
       </div>  
       </div>
 
 <div class="email-block">
  
 
<div class="email-address">
 
 <input type="hidden" value="1" id="pg_lazyload_disable">
 <input type="hidden" value="2" id="bg_value">
  <input type="hidden" value="inc" id="bg_flag">
   
  <asp:TextBox ID="txtEnterEmail"   runat="server" AutoCompleteType="Disabled" autocomplete="off" ValidationGroup="Email" placeholder="EMAIL ADDRESS" ></asp:TextBox>
                        <asp:Button ID="btnSendMail" runat="server" class="show-lg" Text="SUBSCRIBE" onclick="btnSendMail_Click" ValidationGroup="Email" />
                         <asp:Button ID="btnSendMail2" runat="server" class="show-sm" Text="SUBSCRIBE" onclick="btnSendMail_Click" />
  
   
 </div>
 
 </div>
 
 <div class="clearfix"></div>
 </div>
 </section>
        <div id="pg_lazyload" id="backgroundPopup">
            <div class="loading" id="loading-image" style="display: none;">
                <img src="/SG50/images/PG/bx_loader.gif">
            </div>
            <div class="loading-bg" id="loading-bg" style="display: none;">
            </div>
            <section class="sortlist-bg0">  
 <div class="custom-container" id="slidercontent" runat="server">  
 <ul class="sortlist-container">  
  <asp:Repeater ID="repStoryList" runat="server" OnItemCommand="repStoryList_ItemCommand">
                        <ItemTemplate>
                            <li>
                                <div class="img">
                                    <asp:Image runat="server" ID="repImg" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ImageUrl") %>' />
                                    <div class="caption">
                                        <p>
                                            <sc:Text ID="Text1" Field="Pioneers Description" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' /><span><%# DataBinder.Eval(Container.DataItem, "PName") %></span>
                                        </p>
                                     <asp:LinkButton ID="hf" runat="server" CssClass="sg-button-sm" Text="more" CommandName="More" CommandArgument='<%#Eval("href") +","+ Eval("Id") %>' ></asp:LinkButton>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
 </ul>
 <div class="clearfix"></div>
 </div>
 



 </section>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="Lazy-load">
    <div id="marker-end" class="loading-lazy">
        <img src="/SG50/images/PG/bx_loader.gif">
    </div>
</div>

<script src="/SG50/include/PG/js/jquery.validate.min.js" type="text/javascript"></script>

<script type="text/javascript">
    var i = '2';
    $(document).ready(function () {

        $(".sort-block a").click(function (e) {
            //e.preventDefault();
            $(".sort-block a").removeClass("active");
            $(this).addClass("active"); i = '2';
            // alert(i);
        });

        //  ImageChange();
        $(window).resize(function () {
            // ImageChange();

        });

        $("#mainform").validate({
            ignore: [],
            rules: {
                '<%=txtEnterEmail.UniqueID  %>': {
                    required: true,
                    email: true

                }
            },
            errorPlacement: function (error, element) {
                if (element.rules().minChecked > 0) {
                    var $p = $(element).parent();
                    if ($p.siblings().hasClass("error") == false) {
                        error.insertAfter($p);
                    }
                }
                else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function (form) {
                form.submit();
            }
        });


        if (window.DeviceOrientationEvent) {

            window.addEventListener('orientationchange', function () { location.reload(); }, true);
        }



        var SortType;

        // lazyloading functionality 

        $(window).scroll(function () {
            //if ($(window).scrollTop() + $(window).height() == $(document).height() && $('#pg_lazyload_disable').val() == 1)
            if ($(window).scrollTop() + $(window).height() + $(".footer-section").height() + $(".Lazy-load").height() >= $(document).height() && $('#pg_lazyload_disable').val() == 1)
                if (SortType == 'Recent' && i < 3)
                    reloadq(i++);
                else if (SortType != 'Recent')
                    reloadq(i++);
        });
        function reloadq(i) {
            //alert(i);
            $k = $('#bg_flag').val();
            if (i <= 5) {
                j = i;
                $('#bg_value').val(j);
            } else {

                j = $('#bg_value').val();
                if (j <= 5 && $k == 'inc') {
                    j = Number(j) - 1;
                    if (j == 0 || j == 1)
                        $('#bg_flag').val('decr');
                    $('#bg_value').val(j);

                } else if (j >= 0 && $k == 'decr') {
                    j = $('#bg_value').val();
                    j = Number(j) + 1;
                    if (j == 5)
                        $('#bg_flag').val('inc');
                    $('#bg_value').val(j);

                }
            }



            if ($('#contentbottom_0_lbSortRecent').attr('class') == 'active')
                SortType = 'Recent';
            else if ($('#contentbottom_0_lbAll').attr('class') == 'active')
                SortType = 'ALL';
            else if ($('#contentbottom_0_lbPopular').attr('class') == 'active')
                SortType = 'Popular';



            $('#pg_lazyload_disable').val('0');

            $.ajax({
                url: '/SG50/ajax/ajax_content.aspx',
                dataType: "html",
                ajax: false,
                data: { "i": i, "query": 'xyz', "SortType": SortType },
                beforeSend: function (responseText) {
                    $('#pg_lazyload').append('<section class="sortlist-bg' + j + '" id="ajax-section' + i + '" style="display:none;"></section>');
                    if (i > 0) $("#marker-end").show();
                },
                error: function (responseText) {
                    console.log(responseText);
                },
                success: function (responseText) {
                    //  alert(responseText);
                    var $response = $(responseText);
                    var totitems = $response.filter('#totitems').text();
                    var curitems = $response.filter('#curitems').text();

                    if (i > totitems) {
                        responseText = '';

                    }
                    //$('#ajax-section').addClass('sortlist-bg'+i);
                    $("#marker-end").hide();
                    //$('#pg_lazyload').append('<section class="sortlist-bg'+i+'" id="ajax-section'+i+'"></section>');
                    if (responseText == '' || curitems == 0) {
                        $('#pg_lazyload_disable').val('0');
                        $("#images-end").show();
                    } else {
                        $('#pg_lazyload_disable').val('1');
                        $('#ajax-section' + i).attr("style", "display:block");
                        $('#ajax-section' + i).append(responseText);
                    }

                }
            });
        }
    });
    function RESET() {

        i = 2;
    }
</script>

<script>

    $(function () {
        if (!$.support.placeholder) {
            var active = document.activeElement;
            $('textarea').each(function (index, element) {
                if ($(this).val().length == 0) {
                    $(this).html($(this).attr('id')).addClass('hasPlaceholder');
                }
            });
            $('input, textarea').focus(function () {
                if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
                    $(this).val('').removeClass('hasPlaceholder');
                }
            }).blur(function () {
                if (($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder')))) {
                }
            });
            $(':text').blur();
            $(active).focus();
            $('form').submit(function () {
                $(this).find('.hasPlaceholder').each(function () { $(this).val(''); });
            });
        }
    });

</script>

<script src="/SG50/include/PG/js/placeholders.min.js"></script>

