﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Contact_us_sublayout.Contact_us_sublayoutSublayout" CodeFile="~/SG50/layouts/Contact Us Sublayout.ascx.cs" %>

<style>
    #contactUsLink, #contactUsLink a
    {
        color: #353333;
    }
    #contactUsLink img
    {
        width: 10px;
        height: 10px;
    }
</style>
<div class="contactUs">
    <div class="contactUsBanner">
        <div id="contactUsBannerInner">
            <div id="contactUsBannerImage">
                <img src="/SG50/images/contact/header.png" alt="" />
            </div>
            <div id="contactUsBannerCaption">
                <h1>
                    QUESTIONS?</h1>
                <h1>
                    DROP US A LINE.</h1>
            </div>
        </div>
    </div>
    <div id="contactUsContent">
        <div id="contactUsLink">
            <p>
                <a href="<%= getHomeLink() %>">SG50
                    <img src="/SG50/images/rightarrow.png" alt="rightarrow"></a> <a href="#"><span style="color: #ef2c2e;">
                        Contact Us</span></a></p>
        </div>
        <div id="contactUsMCCY">
            <sc:image id="ImgContact" runat="server" field="Image" />
            <p style="font-weight: bold; padding-top: 10px;">
                <sc:text id="txtContactUs" field="Title" runat="server" />
            </p>
            <p style="padding-top: 10px;">
                <sc:text id="txtDescription" field="Description" runat="server" />
            </p>
        </div>
        <br />
        <br />
        <div id="contactUsTitle">
            <p>
                <sc:text id="txtContactUsTitle" field="Contact Us Title" runat="server" />
            </p>
        </div>
        <div id="contactUsDiscription">
            <div id="welcomeText" runat="server">
                <br />
                <p>
                    <sc:text id="txtwelcome" field="Contact Us Welcome Message" runat="server" />
                </p>
                <br />
                <br />
            </div>
            <div class="successMessage">
                <asp:Panel ID="pnSuccess" runat="server" Visible="false">
                    <sc:text id="Text1" field="Success Message" runat="server" />
                </asp:Panel>
            </div>
        </div>
        <div id="contactUsForm" class="contactUsDetails" runat="server">
            <div class="formRow">
                <div class="left">
                    <asp:Label ID="nameLabel" runat="server" Text="Name " CssClass="contactUsLabel"></asp:Label>
                    <span style="color: Red;">*</span>
                </div>
                <div class="right">
                    <asp:TextBox ID="nameTextBox" runat="server" class="contactUsNameTextBox" MaxLength="255"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="nameTextBox" Display="Dynamic"
                        ValidationGroup="EnquiryForm" ErrorMessage="Please fill in your name." ID="rfvname"
                        Style="color: Red;"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    <asp:Label ID="emailID" runat="server" Text="Email " CssClass="contactUsLabel"></asp:Label>
                    <span style="color: Red;">*</span>
                </div>
                <div class="right">
                    <asp:TextBox ID="emailTextBox" runat="server" TextMode="Email" class="contactUsEmailTextBox"
                        MaxLength="255"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvemail" ControlToValidate="emailTextBox" Display="Dynamic"
                        ValidationGroup="EnquiryForm" ErrorMessage="Please fill in your email." runat="server"
                        Style="color: Red;"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regexemail" ControlToValidate="emailTextBox"
                        Display="Dynamic" ValidationGroup="EnquiryForm" runat="server" ValidationExpression="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"
                        ErrorMessage="Please fill in a valid email." Style="color: Red;"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    <asp:Label ID="subjectLabel" runat="server" Text="Subject " CssClass="contactUsLabel"></asp:Label>
                    <span style="color: Red;">*</span>
                </div>
                <div class="right">
                    <asp:TextBox ID="subjectTextBox" runat="server" class="contactUsSubjectTextBox" MaxLength="255"></asp:TextBox>
                    <div class="subjectValidation">
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="subjectTextBox" Display="Dynamic"
                            ValidationGroup="EnquiryForm" ErrorMessage="Please fill in subject." ID="rfvsubject"
                            Style="color: Red;"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    <asp:Label ID="messageLabel" runat="server" Text="Messsage " CssClass="contactUsLabel"></asp:Label>
                    <span style="color: Red;">*</span>
                </div>
                <div class="right">
                    <asp:TextBox ID="messageTextBox" runat="server" TextMode="MultiLine" class="contactUsMessageTextBox"
                        Style="height: 150px" MaxLength="455"></asp:TextBox>
                    <div class="messageValidation">
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="messageTextBox" Display="Dynamic"
                            ValidationGroup="EnquiryForm" ErrorMessage="Please fill in message." ID="rfvmessage"
                            Style="color: Red;"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                </div>
                <div class="right">
                    <asp:TextBox ID="txt_CaptchaVal" runat="server" Visible="false"></asp:TextBox>
                    <asp:Image ID="Image1" runat="server" />
                    <asp:TextBox ID="txt_Captcha" MaxLength="4" Style="vertical-align: top" placeholder="Enter Captcha"
                        Width="200px" runat="server" CssClass="form-select"></asp:TextBox>
                    <asp:ImageButton ID="Button1" Style="vertical-align: top" runat="server" ImageUrl="/SG50/images/PG/refreshIcon.gif"
                        CausesValidation="false" Text="refresh" CssClass="cancel" OnClick="Button1_Click1" />
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please fill Captcha"
                        ForeColor="Red" ValidationGroup="EnquiryForm" ControlToValidate="txt_Captcha"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                </div>
                <div class="right">
                    <div class="submitButton">
                        <asp:Button ID="SubmitButton" runat="server" Text="Submit" BorderStyle="Solid" ValidationGroup="EnquiryForm"
                            OnClick="SubmitButton_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
