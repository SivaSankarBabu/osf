﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Applicationstep3.Applicationstep3Sublayout" CodeFile="~/SG50/layouts/ApplicationStep3.ascx.cs" %>
<%@ Register Assembly="CustomCaptcha" Namespace="CustomCaptcha" TagPrefix="cc1" %>
<link href="/SG50/include/css/style.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/dropkick.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/responsive.css" rel="stylesheet" type="text/css" />
<style>
    .validation_summary_as_bulletlist ul {
        display: none;
    }
</style>
<script type="text/javascript" src="/SG50/include/js/jquery.min.js"></script>
<script src="/SG50/include/js/1.8/jquery-ui.min.js" type="text/javascript"></script>

<link href="/SG50/include/css/jquery-ui.css" rel="stylesheet" />

<script src="/SG50/include/js/jquery.validate.js" type="text/javascript"></script>

<script type="text/javascript" src="/SG50/include/js/jquery.maskedinput.js"></script>

<script type="text/javascript" src="/SG50/include/js/mktSignup.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="/SG50/include/css/stylesheet.css" />


<script>
    $(function () {
        $("#mainform").validate({
            rules: {
                '<%=ddlSalutationAppone.ClientID %>': {
                    selectNone: true
                }
            }
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $(".datepickerCompleted").datepicker({

            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '1924:1996',
            defaultDate: new Date(1996, 01, 01)

        });
    });
    function ValidateCheckBox(sender, args) {
        if (document.getElementById("<%=chkCondition.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox1(sender, args) {
        if (document.getElementById("<%=chkagreeable.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox2(sender, args) {
        if (document.getElementById("<%=chkDetails.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57 || keyCode == 9) || specialKeys.indexOf(keyCode) != -1);
        // document.getElementById("error").style.display = ret ? "none" : "inline";
        return ret;
    }

    function IsDate(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = (keyCode >= 127 && keyCode <= 0);
        return ret;
    }

    function isValidIC(sender, args) {
        // alert('Siva');
        document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderColor = "#bababa";
        document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderWidth = "1px";
        document.getElementById("<%=lblNRICPassport.ClientID %>").innerHTML = "";


        var ID = document.getElementById("<%=txtIdNumAppTwo.ClientID %>").value;
        //   var ID = document.getElementById("<%=txtIdNumAppTwo.ClientID %>").value;

        var ddl = document.getElementById("<%=ddlIdType.ClientID %>").value;



        var algType = 'UIN';

        if (ddl == 'PASSPORT') {

            if (ID.length <= 20) {

                args.IsValid = true;
            }
            else {

                document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderColor = "red";
                document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderWidth = "2px";
                document.getElementById("<%=lblNRICPassport.ClientID %>").innerHTML = "Exceeded Maximum Length 20";
                args.IsValid = false;
            }


        }
        else {

            if (ID.length <= 9) {

                ID = ID.toUpperCase();
                if (!ID.match(/^[S|T|F|G][0-9]{7}[A-Z]$/)) {
                    document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderColor = "red";
                    document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderWidth = "2px";
                    document.getElementById("<%=lblNRICPassport.ClientID %>").innerHTML = "Invalid NRIC No.";
                    args.IsValid = false;
                }
                else {

                    if (algType.toUpperCase() != "UIN" && algType.toUpperCase() != "FIN") {
                        args.IsValid = false;
                    }
                    else {

                        var x = new Array();
                        for (i = 0; i < ID.length; i++)
                            x[i] = ID.charAt(i);

                        //for prefix T (UIN) and G (FIN)
                        var type = (x[0].toUpperCase() == "S" || x[0].toUpperCase() == "F") ? 1 : 0;

                        //STEP 1
                        //multiply array 1 & 7 1st
                        //digit       N N N N N N N
                        //multiply by 2 7 6 5 4 3 2
                        //array loc   1 2 3 4 5 6 7
                        x[1] = parseInt(x[1]) * 2;
                        x[2] = parseInt(x[2]) * 7;
                        x[3] = parseInt(x[3]) * 6;
                        x[4] = parseInt(x[4]) * 5;
                        x[5] = parseInt(x[5]) * 4;
                        x[6] = parseInt(x[6]) * 3;
                        x[7] = parseInt(x[7]) * 2;

                        //STEP 2
                        //sum all multiply digit
                        var s = 0;
                        for (i = 1; i < 8; i++)
                            s += parseInt(x[i]);

                        //if UIN prefix = T or FIN prefix = G, add a weightage 4 to result
                        if (type == 0) { s = s + 4; }

                        //STEP 3
                        //divide sum by 11 giving the reminder(R1)
                        var r = s % 11;

                        //STEP 4 
                        //calculate P = 11 -r
                        var p = 11 - r;
                        var checkDigit = getCheckDigit(algType.toUpperCase(), p);

                        if (checkDigit.toUpperCase() == x[8].toUpperCase()) {
                            args.IsValid = true;

                        }
                        else {
                            document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderColor = "red";
                            document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderWidth = "2px";
                            document.getElementById("<%=lblNRICPassport.ClientID %>").innerHTML = "Invalid NRIC No.";
                            args.IsValid = false;
                        }

                    }
                }
            }
            else {

                document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderColor = "red";
                document.getElementById("<%=txtIdNumAppTwo.ClientID %>").style.borderWidth = "2px";
                document.getElementById("<%=lblNRICPassport.ClientID %>").innerHTML = "Exceeded Maximum Length 9";
                args.IsValid = false;
            }
        }
    }

    function getCheckDigit(algType, p) {
        //algType = algorithm. "UIN" = Unique Identification Number; 
        //                     "FIN" = Foreigner Identification Number
        var UIN, FIN, UENL, UENB, UENO;

        switch (p) {
            case 1:
                UIN = "A";
                FIN = "K";
                UENL = "C";
                UENB = "A";
                UENO = "A";
                break;
            case 2:
                UIN = "B";
                FIN = "L";
                UENL = "D";
                UENB = "B";
                UENO = "B";
                break;
            case 3:
                UIN = "C";
                FIN = "M";
                UENL = "E";
                UENB = "C";
                UENO = "C";
                break;
            case 4:
                UIN = "D";
                FIN = "N";
                UENL = "G";
                UENB = "D";
                UENO = "D";
                break;
            case 5:
                UIN = "E";
                FIN = "P";
                UENL = "H";
                UENB = "E";
                UENO = "E";
                break;
            case 6:
                UIN = "F";
                FIN = "Q";
                UENL = "K";
                UENB = "J";
                UENO = "F";
                break;
            case 7:
                UIN = "G";
                FIN = "R";
                UENL = "M";
                UENB = "K";
                UENO = "G";
                break;
            case 8:
                UIN = "H";
                FIN = "T";
                UENL = "N";
                UENB = "L";
                UENO = "H";
                break;
            case 9:
                UIN = "I";
                FIN = "U";
                UENL = "R";
                UENB = "M";
                UENO = "J";
                break;
            case 10:
                UIN = "Z";
                FIN = "W";
                UENL = "W";
                UENB = "W";
                UENO = "K";
                break;
            case 11:
                UIN = "J";
                FIN = "X";
                UENL = "Z";
                UENB = "X";
                UENO = "L";
                break;
        }

        if (algType == "UIN")
            return UIN.toUpperCase();
        else if (algType == "FIN")
            return FIN.toUpperCase();
        else if (algType == "UENL")
            return UENL.toUpperCase();
        else if (algType == "UENB")
            return UENB.toUpperCase();
        else if (algType == "UENO")
            return UENO.toUpperCase();
        else
            return null;
    }
    function Step2() {

        window.location = '<%= ResolveUrl("/Step2.aspx") %>'

    }
    function Step1() {

        window.location = '<%= ResolveUrl("/Apply For Funding.aspx") %>'

}
</script>

<section class="main-content">
    <!---main-content start-------->
    <div class="form-banner">
        <img src="/Sg50/images/Images/batch-4/banner.jpg" />
    </div>
    <section class="form-container">

        <div class="head-title">
            <h1>Tell us more about you</h1>
        </div>
        <div class="grid-container">
            <div class="formprocess-nav">
                <ul>
                    <li>
                        <div class="process-serial">1</div>
                        <div class="process-title">
                            <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="#989898" OnClientClick="Step1();" OnClick="LinkButton1_Click">
                            <h2>STEP 1</h2>
                            <p>Application overview</p>
                            </asp:LinkButton>
                        </div>
                    </li>
                    <li>
                        <div class="process-serial">2</div>
                        <div class="process-title">
                            <asp:LinkButton ID="LinkButton2" runat="server" ForeColor="#989898" OnClientClick="Step2();" OnClick="LinkButton2_Click">
                            <h2>STEP 2</h2>
                            <p>Tell us about your project</p>
                            </asp:LinkButton>
                        </div>
                    </li>
                    <li class="active">
                        <div class="process-serial">3</div>
                        <div class="process-title">
                            <h2>STEP 3</h2>
                            <p>Tell us more about you</p>
                        </div>
                    </li>
                </ul>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="step3" CssClass="validation_summary_as_bulletlist" runat="server" HeaderText='Please fill in required fields in the correct format and try again.' ForeColor="Red" ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" />
            <div class="form-content">
                <!------form content start------------>
                <!----- heading starts--------------------->
                <div id="divApplicantone">
                    <div class="heading-box">
                        <p>APPLICANT 1</p>
                        <span>(Must be Singaporean aged 18 years and above)</span>

                        <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                    </div>
                    <!----- heading ends--------------------->

                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Salutation</label><br />
                            <asp:DropDownList ID="ddlSalutationAppone" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Mr</asp:ListItem>
                                <asp:ListItem Value="3">Ms</asp:ListItem>
                                <asp:ListItem Value="2">Mrs</asp:ListItem>
                                <asp:ListItem Value="4">Mdm</asp:ListItem>
                                <asp:ListItem Value="5">Dr</asp:ListItem>
                                <asp:ListItem Value="6">Prof</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqSalutationAppone" runat="server" ValidationGroup="step3" ControlToValidate="ddlSalutationAppone" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Name of applicant (as in NRIC)</label><br />
                            <asp:TextBox ID="txtapplicantonename" MaxLength="66" CssClass="ValidationRequired" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqNameofapplicant" runat="server" ValidationGroup="step3" ControlToValidate="txtapplicantonename" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>NRIC number</label><br />
                            <asp:TextBox ID="txtNricnum" CssClass="ValidationRequired nric" MaxLength="9" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqNricnum" runat="server" ValidationGroup="step3" ControlToValidate="txtNricnum" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Nationality</label>
                            <br />
                            <p>Singaporean</p>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Occupation / Designation</label><br />
                            <asp:TextBox ID="txtOccupationAppOne" CssClass="ValidationRequired" MaxLength="100" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqOccupationAppOne" runat="server" ValidationGroup="step3" ControlToValidate="txtOccupationAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Gender</label><br />
                            <asp:DropDownList ID="ddlGenderAppOne" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqGenderAppOne" runat="server" ValidationGroup="step3" ControlToValidate="ddlGenderAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Date of birth</label><br />
                            <asp:TextBox ID="txtDobAppone" CssClass="datepickerCompleted calender ValidationRequired" onkeypress="return IsDate(event);" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqDobAppone" runat="server" ValidationGroup="step3" ControlToValidate="txtDobAppone" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-right" style="visibility: hidden">
                            <label><span class="mandatory">*</span>Estimated total number of participants / beneficiaries</label><br />
                            <input type="text" />
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <!-------------list-row start--------------------->
                    <label>
                        <span class="mandatory">*</span>Address
                        <asp:Label ID="Label1" runat="server"></asp:Label></label>
                    <asp:TextBox ID="txtAddressAppOne" runat="server" CssClass="ValidationRequired" TextMode="MultiLine" Style="width: 960px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regAddress" ForeColor="Red" ValidationExpression="^[\s\S]{0,120}$" ValidationGroup="step3" ErrorMessage="You cannot enter more than 120 characters." ControlToValidate="txtAddressAppOne" runat="server" Display="Dynamic">
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="reqtxtAddressAppOne" runat="server" ValidationGroup="step3" ControlToValidate="txtAddressAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label>
                                <span class="mandatory">*</span><span style="margin-right: 5px">Mobile number</span>
                                <asp:Label ID="Label2" runat="server"></asp:Label></label>
                            <br />
                            <asp:TextBox ID="txtMobileNumAppone" MaxLength="8" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqMobileNumAppone" runat="server" ValidationGroup="step3" ControlToValidate="txtMobileNumAppone" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionMobile" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter 8 Digits." ControlToValidate="txtMobileNumAppone" ForeColor="Red" ValidationExpression="\d{8,8}"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory"></span>Phone number</label><br />
                            <asp:TextBox ID="txtPhoneNumberAppOne" MaxLength="8" onkeypress="return IsNumeric(event);" runat="server"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionPhone" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter 8 Digits." ControlToValidate="txtPhoneNumberAppOne" ForeColor="Red" ValidationExpression="\d{8,8}"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Email address</label><br />
                            <asp:TextBox ID="txtEmailAppOne" CssClass="ValidationRequired" runat="server" MaxLength="320"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqEmailAppOne" ValidationGroup="step3" runat="server" ControlToValidate="txtEmailAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionEmail1" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter Valid Email Address." ControlToValidate="txtEmailAppOne" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                </div>
                <!-------------list-row start--------------------->

                <!-----------------------applicant2--------------------------------->

                <!----- heading starts--------------------->
                <div id="divApplicanttwo" runat="server" visible="false">
                    <div class="heading-box">
                        <p>APPLICANT 2</p>
                        <span>(Singaporean / Non-Singaporean)</span>

                        <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                    </div>
                    <!----- heading ends--------------------->

                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Salutation</label><br />
                            <asp:DropDownList ID="ddlSalutationAppTwo" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Mr</asp:ListItem>
                                <asp:ListItem Value="3">Ms</asp:ListItem>
                                <asp:ListItem Value="2">Mrs</asp:ListItem>
                                <asp:ListItem Value="4">Mdm</asp:ListItem>
                                <asp:ListItem Value="5">Dr</asp:ListItem>
                                <asp:ListItem Value="6">Prof</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqddlSalutationAppTwo" ValidationGroup="step3" ControlToValidate="ddlSalutationAppTwo" runat="server" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label>
                                <span class="mandatory">*</span>Name of applicant (as in NRIC)
(As in passport for Non-Singaporean)</label><br />
                            <asp:TextBox ID="txtNameAppTwo" CssClass="ValidationRequired" runat="server" MaxLength="66"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqNameAppTwo" runat="server" ControlToValidate="txtNameAppTwo" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Nationality</label><br />
                            <asp:DropDownList runat="server" ID="ddlNationalityAppTwo">
                                <asp:ListItem Value="AF" Text="AFGHAN"></asp:ListItem>
                                <asp:ListItem Value="AL" Text="ALBANIAN"></asp:ListItem>
                                <asp:ListItem Value="DG" Text="ALGERIAN"></asp:ListItem>
                                <asp:ListItem Value="US" Text="AMERICAN"></asp:ListItem>
                                <asp:ListItem Value="AD" Text="ANDORRAN"></asp:ListItem>
                                <asp:ListItem Value="AO" Text="ANGOLAN"></asp:ListItem>
                                <asp:ListItem Value="AG" Text="ANTIGUAN"></asp:ListItem>
                                <asp:ListItem Value="AR" Text="ARGENTINIAN"></asp:ListItem>
                                <asp:ListItem Value="AM" Text="ARMENIAN"></asp:ListItem>
                                <asp:ListItem Value="AU" Text="AUSTRALIAN"></asp:ListItem>
                                <asp:ListItem Value="AT" Text="AUSTRIAN"></asp:ListItem>
                                <asp:ListItem Value="AZ" Text="AZERBAIJANI"></asp:ListItem>

                                <asp:ListItem Value="BS" Text="BAHAMIAN"></asp:ListItem>
                                <asp:ListItem Value="BH" Text="BAHRAINI"></asp:ListItem>
                                <asp:ListItem Value="BD" Text="BANGLADESHI"></asp:ListItem>
                                <asp:ListItem Value="BB" Text="BARBADOS"></asp:ListItem>
                                <asp:ListItem Value="BL" Text="BELARUSSIAN"></asp:ListItem>
                                <asp:ListItem Value="BE" Text="BELGIAN"></asp:ListItem>
                                <asp:ListItem Value="BZ" Text="BELIZEAN"></asp:ListItem>
                                <asp:ListItem Value="BJ" Text="BENINESE"></asp:ListItem>
                                <asp:ListItem Value="BT" Text="BHUTANESE"></asp:ListItem>
                                <asp:ListItem Value="BO" Text="BOLIVIAN"></asp:ListItem>
                                <asp:ListItem Value="BA" Text="BOSNIAN"></asp:ListItem>
                                <asp:ListItem Value="BW" Text="BOTSWANA"></asp:ListItem>
                                <asp:ListItem Value="BR" Text="BRAZILIAN"></asp:ListItem>
                                <asp:ListItem Value="GB" Text="BRITISH"></asp:ListItem>
                                <asp:ListItem Value="GC" Text="BRITISH DEPENDENT TERRITORIES CITIZEN"></asp:ListItem>
                                <asp:ListItem Value="GG" Text="BRITISH NATIONAL OVERSEAS"></asp:ListItem>
                                <asp:ListItem Value="GE" Text="BRITISH OVERSEAS CITIZEN"></asp:ListItem>
                                <asp:ListItem Value="GJ" Text="BRITISH PROTECTED PERSON"></asp:ListItem>
                                <asp:ListItem Value="UK" Text="BRITISH SUBJECT"></asp:ListItem>
                                <asp:ListItem Value="BN" Text="BRUNEIAN"></asp:ListItem>
                                <asp:ListItem Value="BG" Text="BULGARIAN"></asp:ListItem>
                                <asp:ListItem Value="BF" Text="BURKINABE"></asp:ListItem>
                                <asp:ListItem Value="BI" Text="BURUNDIAN"></asp:ListItem>

                                <asp:ListItem Value="KA" Text="CAMBODIAN"></asp:ListItem>
                                <asp:ListItem Value="CM" Text="CAMEROONIAN"></asp:ListItem>
                                <asp:ListItem Value="CA" Text="CANADIAN"></asp:ListItem>
                                <asp:ListItem Value="CV" Text="CAPE VERDEAN"></asp:ListItem>
                                <asp:ListItem Value="CF" Text="CENTRAL AFRICAN REPUBLIC"></asp:ListItem>
                                <asp:ListItem Value="TD" Text="CHADIAN"></asp:ListItem>
                                <asp:ListItem Value="CL" Text="CHILEAN"></asp:ListItem>
                                <asp:ListItem Value="CN" Text="CHINESE"></asp:ListItem>
                                <asp:ListItem Value="CO" Text="COLOMBIAN"></asp:ListItem>
                                <asp:ListItem Value="KM" Text="COMORAN"></asp:ListItem>
                                <asp:ListItem Value="CG" Text="CONGOLESE"></asp:ListItem>
                                <asp:ListItem Value="CR" Text="COSTA RICAN"></asp:ListItem>
                                <asp:ListItem Value="CB" Text="CROATIAN"></asp:ListItem>
                                <asp:ListItem Value="CU" Text="CUBAN"></asp:ListItem>
                                <asp:ListItem Value="CY" Text="CYPRIOT"></asp:ListItem>
                                <asp:ListItem Value="CZ" Text="CZECH"></asp:ListItem>

                                <asp:ListItem Value="DK" Text="DANISH"></asp:ListItem>
                                <asp:ListItem Value="CD" Text="DEMOCRATIC REPUBLIC OF THE CONGO"></asp:ListItem>
                                <asp:ListItem Value="DJ" Text="DJIBOUTIAN"></asp:ListItem>
                                <asp:ListItem Value="DM" Text="DOMINICAN"></asp:ListItem>
                                <asp:ListItem Value="DO" Text="DOMINICAN (REPUBLIC)"></asp:ListItem>

                                <asp:ListItem Value="TP" Text="EAST TIMORESE"></asp:ListItem>
                                <asp:ListItem Value="EC" Text="ECUADORIAN"></asp:ListItem>
                                <asp:ListItem Value="EG" Text="EGYPTIAN"></asp:ListItem>
                                <asp:ListItem Value="GQ" Text="EQUATORIAL GUINEA"></asp:ListItem>
                                <asp:ListItem Value="ER" Text="ERITREAN"></asp:ListItem>
                                <asp:ListItem Value="EN" Text="ESTONIAN"></asp:ListItem>
                                <asp:ListItem Value="ET" Text="ETHIOPIAN"></asp:ListItem>


                                <asp:ListItem Value="FJ" Text="FIJIAN"></asp:ListItem>
                                <asp:ListItem Value="PH" Text="FILIPINO"></asp:ListItem>
                                <asp:ListItem Value="FI" Text="FINNISH"></asp:ListItem>
                                <asp:ListItem Value="FR" Text="FRENCH"></asp:ListItem>

                                <asp:ListItem Value="GA" Text="GABON"></asp:ListItem>
                                <asp:ListItem Value="GM" Text="GAMBIAN"></asp:ListItem>
                                <asp:ListItem Value="GO" Text="GEORGIAN"></asp:ListItem>
                                <asp:ListItem Value="DG" Text="GERMAN"></asp:ListItem>
                                <asp:ListItem Value="GH" Text="GHANAIAN"></asp:ListItem>
                                <asp:ListItem Value="GR" Text="GREEK"></asp:ListItem>
                                <asp:ListItem Value="GD" Text="GRENADIAN"></asp:ListItem>
                                <asp:ListItem Value="GT" Text="GUATEMALAN"></asp:ListItem>
                                <asp:ListItem Value="GN" Text="GUINEAN"></asp:ListItem>
                                <asp:ListItem Value="GW" Text="GUINEAN (BISSAU)"></asp:ListItem>
                                <asp:ListItem Value="GY" Text="GUYANESE"></asp:ListItem>

                                <asp:ListItem Value="HT" Text="HAITIAN"></asp:ListItem>
                                <asp:ListItem Value="HN" Text="HONDURAN"></asp:ListItem>
                                <asp:ListItem Value="HK" Text="HONG KONG"></asp:ListItem>
                                <asp:ListItem Value="HU" Text="HUNGARIAN"></asp:ListItem>

                                <asp:ListItem Value="IS" Text="ICELANDER"></asp:ListItem>
                                <asp:ListItem Value="IN" Text="INDIAN"></asp:ListItem>
                                <asp:ListItem Value="ID" Text="INDONESIAN"></asp:ListItem>
                                <asp:ListItem Value="IR" Text="IRANIAN"></asp:ListItem>
                                <asp:ListItem Value="IQ" Text="IRAQI"></asp:ListItem>
                                <asp:ListItem Value="IE" Text="IRISH"></asp:ListItem>
                                <asp:ListItem Value="IL" Text="ISRAELI"></asp:ListItem>
                                <asp:ListItem Value="IT" Text="ITALIAN"></asp:ListItem>
                                <asp:ListItem Value="CI" Text="IVORY COAST"></asp:ListItem>

                                <asp:ListItem Value="JM" Text="JAMAICAN"></asp:ListItem>
                                <asp:ListItem Value="JP" Text="JAPANESE"></asp:ListItem>
                                <asp:ListItem Value="JO" Text="JORDANIAN"></asp:ListItem>

                                <asp:ListItem Value="KZ" Text="KAZAKHSTANI"></asp:ListItem>
                                <asp:ListItem Value="KE" Text="KENYAN"></asp:ListItem>
                                <asp:ListItem Value="KI" Text="KIRIBATI"></asp:ListItem>
                                <asp:ListItem Value="SW" Text="KITTIAN & NEVISIAN"></asp:ListItem>
                                <asp:ListItem Value="KP" Text="KOREAN, NORTH"></asp:ListItem>
                                <asp:ListItem Value="KR" Text="KOREAN, SOUTH"></asp:ListItem>
                                <asp:ListItem Value="KW" Text="KUWAITI"></asp:ListItem>
                                <asp:ListItem Value="KY" Text="KYRGYZSTAN"></asp:ListItem>


                                <asp:ListItem Value="LA" Text="LAOTIAN"></asp:ListItem>
                                <asp:ListItem Value="LV" Text="LATVIAN"></asp:ListItem>
                                <asp:ListItem Value="LB" Text="LEBANESE"></asp:ListItem>
                                <asp:ListItem Value="LS" Text="LESOTHO"></asp:ListItem>
                                <asp:ListItem Value="LR" Text="LIBERIAN"></asp:ListItem>
                                <asp:ListItem Value="LY" Text="LIBYAN"></asp:ListItem>
                                <asp:ListItem Value="LI" Text="LIECHTENSTEINER"></asp:ListItem>
                                <asp:ListItem Value="LH" Text="LITHUANIAN"></asp:ListItem>
                                <asp:ListItem Value="LU" Text="LUXEMBOURGER"></asp:ListItem>

                                <asp:ListItem Value="MO" Text="MACAO"></asp:ListItem>
                                <asp:ListItem Value="MB" Text="MACEDONIAN"></asp:ListItem>
                                <asp:ListItem Value="MG" Text="MADAGASY"></asp:ListItem>
                                <asp:ListItem Value="MW" Text="MALAWIAN"></asp:ListItem>
                                <asp:ListItem Value="MY" Text="MALAYSIAN"></asp:ListItem>
                                <asp:ListItem Value="MV" Text="MALDIVIAN"></asp:ListItem>
                                <asp:ListItem Value="ML" Text="MALIAN"></asp:ListItem>
                                <asp:ListItem Value="MT" Text="MALTESE"></asp:ListItem>
                                <asp:ListItem Value="MH" Text="MARSHALLESE"></asp:ListItem>
                                <asp:ListItem Value="MR" Text="MAURITANEAN"></asp:ListItem>
                                <asp:ListItem Value="MU" Text="MAURITIAN"></asp:ListItem>
                                <asp:ListItem Value="MX" Text="MEXICAN"></asp:ListItem>
                                <asp:ListItem Value="MF" Text="MICRONESIAN"></asp:ListItem>
                                <asp:ListItem Value="MD" Text="MOLDAVIAN"></asp:ListItem>
                                <asp:ListItem Value="MC" Text="MONACAN"></asp:ListItem>
                                <asp:ListItem Value="MN" Text="MONGOLIAN"></asp:ListItem>
                                <asp:ListItem Value="MJ" Text="MONTENEGRIN"></asp:ListItem>
                                <asp:ListItem Value="MA" Text="MOROCCAN"></asp:ListItem>
                                <asp:ListItem Value="MZ" Text="MOZAMBICAN"></asp:ListItem>
                                <asp:ListItem Value="BU" Text="MYANMAR"></asp:ListItem>

                                <asp:ListItem Value="NA" Text="NAMIBIAN"></asp:ListItem>
                                <asp:ListItem Value="NR" Text="NAURUAN"></asp:ListItem>
                                <asp:ListItem Value="NP" Text="NEPALESE"></asp:ListItem>
                                <asp:ListItem Value="NT" Text="NETHERLANDS"></asp:ListItem>
                                <asp:ListItem Value="NZ" Text="NEW ZEALANDER"></asp:ListItem>
                                <asp:ListItem Value="NI" Text="NICARAGUAN"></asp:ListItem>
                                <asp:ListItem Value="NE" Text="NIGER"></asp:ListItem>
                                <asp:ListItem Value="NG" Text="NIGERIAN"></asp:ListItem>
                                <asp:ListItem Value="VU" Text="NI-VANUATU"></asp:ListItem>
                                <asp:ListItem Value="NO" Text="NORWEGIAN"></asp:ListItem>

                                <asp:ListItem Value="OM" Text="OMANI"></asp:ListItem>

                                <asp:ListItem Value="PK" Text="PAKISTANI"></asp:ListItem>
                                <asp:ListItem Value="PW" Text="PALAUAN"></asp:ListItem>
                                <asp:ListItem Value="PN" Text="PALESTINIAN"></asp:ListItem>
                                <asp:ListItem Value="PA" Text="PANAMANIAN"></asp:ListItem>
                                <asp:ListItem Value="PG" Text="PAPUA NEW GUINEAN"></asp:ListItem>
                                <asp:ListItem Value="PY" Text="PARAGUAYAN"></asp:ListItem>
                                <asp:ListItem Value="PE" Text="PERUVIAN"></asp:ListItem>
                                <asp:ListItem Value="PL" Text="POLISH"></asp:ListItem>
                                <asp:ListItem Value="PT" Text="PORTUGUESE"></asp:ListItem>

                                <asp:ListItem Value="QA" Text="QATARI"></asp:ListItem>

                                <asp:ListItem Value="RO" Text="ROMANIAN"></asp:ListItem>
                                <asp:ListItem Value="RF" Text="RUSSIAN"></asp:ListItem>
                                <asp:ListItem Value="RW" Text="RWANDAN"></asp:ListItem>

                                <asp:ListItem Value="SV" Text="SALVADORAN"></asp:ListItem>
                                <asp:ListItem Value="SM" Text="SAMMARINESE"></asp:ListItem>
                                <asp:ListItem Value="WS" Text="SAMOAN"></asp:ListItem>
                                <asp:ListItem Value="ST" Text="SAO TOMEAN"></asp:ListItem>
                                <asp:ListItem Value="SA" Text="SAUDI ARABIAN"></asp:ListItem>
                                <asp:ListItem Value="SN" Text="SENEGALESE"></asp:ListItem>
                                <asp:ListItem Value="RS" Text="SERBIAN"></asp:ListItem>
                                <asp:ListItem Value="SC" Text="SEYCHELLOIS"></asp:ListItem>
                                <asp:ListItem Value="SL" Text="SIERRA LEONE"></asp:ListItem>
                                <asp:ListItem Selected="True" Value="SG" Text="SINGAPORE CITIZEN"></asp:ListItem>
                                <asp:ListItem Value="SK" Text="SLOVAK"></asp:ListItem>
                                <asp:ListItem Value="SI" Text="SLOVENIAN"></asp:ListItem>
                                <asp:ListItem Value="SB" Text="SOLOMON ISLANDER"></asp:ListItem>
                                <asp:ListItem Value="SO" Text="SOMALI"></asp:ListItem>
                                <asp:ListItem Value="ZA" Text="SOUTH AFRICAN"></asp:ListItem>
                                <asp:ListItem Value="ES" Text="SPANISH"></asp:ListItem>
                                <asp:ListItem Value="LK" Text="SRI LANKAN"></asp:ListItem>
                                <asp:ListItem Value="LC" Text="ST. LUCIA"></asp:ListItem>
                                <asp:ListItem Value="VC" Text="ST. VINCENTIAN"></asp:ListItem>
                                <asp:ListItem Value="SS" Text="STATELESS"></asp:ListItem>
                                <asp:ListItem Value="SD" Text="SUDANESE"></asp:ListItem>
                                <asp:ListItem Value="SR" Text="SURINAMER"></asp:ListItem>
                                <asp:ListItem Value="SZ" Text="SWAZI"></asp:ListItem>
                                <asp:ListItem Value="SE" Text="SWEDISH"></asp:ListItem>
                                <asp:ListItem Value="CH" Text="SWISS"></asp:ListItem>
                                <asp:ListItem Value="SY" Text="SYRIAN"></asp:ListItem>


                                <asp:ListItem Value="TW" Text="TAIWANESE"></asp:ListItem>
                                <asp:ListItem Value="TI" Text="TAJIKISTANI"></asp:ListItem>
                                <asp:ListItem Value="TZ" Text="TANZANIAN"></asp:ListItem>
                                <asp:ListItem Value="TH" Text="THAI"></asp:ListItem>
                                <asp:ListItem Value="TG" Text="TOGOLESE"></asp:ListItem>
                                <asp:ListItem Value="TO" Text="TONGAN"></asp:ListItem>
                                <asp:ListItem Value="TT" Text="TRINIDADIAN & TOBAGONIAN"></asp:ListItem>
                                <asp:ListItem Value="TN" Text="TUNISIAN"></asp:ListItem>
                                <asp:ListItem Value="TR" Text="TURK"></asp:ListItem>
                                <asp:ListItem Value="TM" Text="TURKMEN"></asp:ListItem>
                                <asp:ListItem Value="TV" Text="TUVALU"></asp:ListItem>

                                <asp:ListItem Value="UG" Text="UGANDAN"></asp:ListItem>
                                <asp:ListItem Value="UR" Text="UKRAINIAN"></asp:ListItem>
                                <asp:ListItem Value="AE" Text="UNITED ARAB EMIRATES"></asp:ListItem>
                                <asp:ListItem Value="UN" Text="UNKNOWN"></asp:ListItem>
                                <asp:ListItem Value="UY" Text="URUGUAYAN"></asp:ListItem>
                                <asp:ListItem Value="UZ" Text="UZBEKISTAN"></asp:ListItem>

                                <asp:ListItem Value="VA" Text="VATICAN CITY STATE (HOLY SEE)"></asp:ListItem>
                                <asp:ListItem Value="VE" Text="VENEZUELAN"></asp:ListItem>
                                <asp:ListItem Value="VN" Text="VIETNAMESE"></asp:ListItem>
                                <asp:ListItem Value="YM" Text="YEMENI"></asp:ListItem>

                                <asp:ListItem Value="ZM" Text="ZAMBIAN"></asp:ListItem>
                                <asp:ListItem Value="ZW" Text="ZIMBABWEAN"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>ID type</label>
                            <br />
                            <asp:DropDownList ID="ddlIdType" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="NRIC">NRIC</asp:ListItem>
                                <asp:ListItem Value="PASSPORT">PASSPORT</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="requiedYPE" runat="server" ValidationGroup="step3" ControlToValidate="ddlIdType" ForeColor="Red" ErrorMessage=""></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>ID number</label><br />
                            <asp:TextBox ID="txtIdNumAppTwo" runat="server" onblur="isValidIC()" CssClass="ValidationRequired" MaxLength="20"></asp:TextBox>
                            <asp:Label ID="lblNRICPassport" ForeColor="Red" runat="server" Text=""></asp:Label>
                            <asp:CustomValidator ID="CustomValidator4" runat="server" ForeColor="Red" ValidationGroup="step3" ErrorMessage="" ClientValidationFunction="isValidIC"></asp:CustomValidator>
                            <asp:RequiredFieldValidator ID="reqIdNumAppTwo" runat="server" ControlToValidate="txtIdNumAppTwo" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Occupation/Designation</label><br />
                            <asp:TextBox ID="txtOccupationAppTwo" runat="server" CssClass="ValidationRequired" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqOccupationAppTwo" runat="server" ControlToValidate="txtOccupationAppTwo" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Gender</label><br />
                            <asp:DropDownList ID="ddlgenderAppTwo" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqgenderAppTwo" runat="server" ControlToValidate="ddlgenderAppTwo" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Date of birth</label><br />
                            <asp:TextBox ID="txtDobAppTwo" CssClass="datepickerCompleted calender ValidationRequired" onkeypress="return IsDate(event);" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqDobAppTwo" runat="server" ValidationGroup="step3" ControlToValidate="txtDobAppTwo" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Address type </label>
                            <br />
                            <asp:DropDownList ID="ddlAddressType" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Local</asp:ListItem>
                                <asp:ListItem Value="2">Overseas</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="step3" ControlToValidate="ddlAddressType" ForeColor="Red" ErrorMessage=""></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <label><span class="mandatory">*</span>Address</label>
                    <asp:TextBox ID="txtAddressAppTwo" runat="server" CssClass="ValidationRequired" TextMode="MultiLine" Style="width: 960px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regobjective" ForeColor="Red" ValidationExpression="^[\s\S]{0,120}$" ValidationGroup="step3" ErrorMessage="You cannot enter more than 120 characters." ControlToValidate="txtAddressAppTwo" runat="server" Display="Dynamic">
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="reqAddressAppTwo" runat="server" ControlToValidate="txtAddressAppTwo" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label>
                                <span class="mandatory">*</span>Mobile number (in Singapore)</label>
                            <br />
                            <asp:TextBox ID="txtMobileNumberAppTwo" CssClass="ValidationRequired" runat="server" onkeypress="return IsNumeric(event);" MaxLength="8"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqMobileNumber" runat="server" Display="Dynamic" ControlToValidate="txtMobileNumberAppTwo" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regMobile" runat="server" ErrorMessage="Please Enter 8 Digits." ValidationGroup="step3" ValidationExpression="\d{8,8}" ControlToValidate="txtMobileNumberAppTwo" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory"></span>Phone number</label><br />
                            <asp:TextBox ID="txtPhoneNumberAppTwo" runat="server" onkeypress="return IsNumeric(event);" MaxLength="8"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="regPhone" runat="server" ErrorMessage="Please Enter 8 Digits." ValidationGroup="step3" ValidationExpression="\d{8,8}" ControlToValidate="txtPhoneNumberAppTwo" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Email address</label><br />
                            <asp:TextBox ID="txtEmailAppTwo" runat="server" CssClass="ValidationRequired" MaxLength="320"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqEmailAppTwo" runat="server" Display="Dynamic" ValidationGroup="step3" ControlToValidate="txtEmailAppTwo" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionEmail2" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter Valid Email Address." ControlToValidate="txtEmailAppTwo" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-right" style="visibility: hidden">
                            <label><span class="mandatory">*</span>Estimated total number of participants / beneficiaries</label><br />
                            <input type="text" />
                        </div>
                    </div>
                </div>
                <div id="Overseas" runat="server" visible="false">
                    <div class="heading-box">
                        <p>APPLICANT 2</p>
                        <span>(Must be Singaporean aged 18 years and above)</asp:Label></span>

                        <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                    </div>
                    <!----- heading ends--------------------->

                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Salutation</label><br />
                            <asp:DropDownList ID="ddlSalutationAppTwoOver" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Mr</asp:ListItem>
                                <asp:ListItem Value="3">Ms</asp:ListItem>
                                <asp:ListItem Value="2">Mrs</asp:ListItem>
                                <asp:ListItem Value="4">Mdm</asp:ListItem>
                                <asp:ListItem Value="5">Dr</asp:ListItem>
                                <asp:ListItem Value="6">Prof</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="step3" ControlToValidate="ddlSalutationAppTwoOver" runat="server" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label>
                                <span class="mandatory">*</span>Name of applicant (as in NRIC)
                            </label>
                            <br />
                            <asp:TextBox ID="txtNameAppTwoOver" CssClass="ValidationRequired" runat="server" MaxLength="66"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNameAppTwoOver" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">

                            <label><span class="mandatory">*</span>NRIC number</label><br />
                            <asp:TextBox ID="txtNricOver" MaxLength="9" CssClass="ValidationRequired nric" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="step3" ControlToValidate="txtNricOver" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">


                            <label><span class="mandatory">*</span>Nationality</label><br />
                            <asp:Label ID="lblNationalityOver" runat="server" Text="Singaporean"></asp:Label>

                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Occupation / Designation</label><br />
                            <asp:TextBox ID="txtOccupationAppTwoOver" CssClass="ValidationRequired" runat="server" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtOccupationAppTwoOver" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">


                            <label><span class="mandatory">*</span>Gender</label><br />
                            <asp:DropDownList ID="ddlgenderAppTwoOver" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlgenderAppTwoOver" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>


                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Date of birth</label><br />
                            <asp:TextBox ID="txtDobAppTwoOver" CssClass="datepickerCompleted calender ValidationRequired" onkeypress="return IsDate(event);" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ValidationGroup="step3" ControlToValidate="txtDobAppTwoOver" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <!-------------list-row start--------------------->
                    <label><span class="mandatory">*</span>Address</label>
                    <asp:TextBox ID="txtAddressAppTwoOver" runat="server" CssClass="ValidationRequired" TextMode="MultiLine" Style="width: 960px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ForeColor="Red" ValidationExpression="^[\s\S]{0,120}$" ValidationGroup="step3" ErrorMessage="You cannot enter more than 120 characters." ControlToValidate="txtAddressAppTwoOver" runat="server" Display="Dynamic">
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtAddressAppTwoOver" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label>
                                <span class="mandatory">*</span><span>Mobile number </span>
                            </label>
                            <br />
                            <asp:TextBox ID="txtMobileNumberAppTwoOver" CssClass="ValidationRequired" runat="server" onkeypress="return IsNumeric(event);" MaxLength="8"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" Display="Dynamic" ControlToValidate="txtMobileNumberAppTwoOver" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please Enter 8 Digits." ValidationGroup="step3" ValidationExpression="\d{8,8}" ControlToValidate="txtMobileNumberAppTwoOver" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory"></span>Phone number</label><br />
                            <asp:TextBox ID="txtPhoneNumberAppTwoOver" runat="server" onkeypress="return IsNumeric(event);" MaxLength="8"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Please Enter 8 Digits." ValidationGroup="step3" ValidationExpression="\d{8,8}" ControlToValidate="txtPhoneNumberAppTwoOver" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Email address</label><br />
                            <asp:TextBox ID="txtEmailAppTwoOver" CssClass="ValidationRequired" runat="server" MaxLength="320    "></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" Display="Dynamic" ValidationGroup="step3" ControlToValidate="txtEmailAppTwoOver" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter Valid Email Address." ControlToValidate="txtEmailAppTwoOver" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-right" style="visibility: hidden">
                            <label><span class="mandatory">*</span>Estimated total number of participants / beneficiaries</label><br />
                            <input type="text" />
                        </div>
                    </div>
                </div>
                <!-------------list-row start--------------------->

                <!----- heading starts--------------------->
                <div class="heading-box">
                    <p>*DECLARATION</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>
                <!----- heading ends--------------------->

                <div class="check-box-container">
                    <div class="check-box" id="DivTerms" runat="server" visible="false">
                        <asp:CheckBox ID="chkCondition" runat="server" />
                        <p>
                            <sc:Text ID="txtTC" Field="Terms and Conditions Text" runat="server" />
                            <sc:Link Field="Terms and Conditio URL" runat="server" ID="hyplknTC">
                                <span class="mandatory">
                                    <sc:Text Field="Terms and Conditio URL Text" runat="server" ID="txtterms" />
                                </span>
                            </sc:Link>
                        </p>

                        <asp:CustomValidator ID="CustomValidator1" runat="server" ForeColor="Red" ValidationGroup="step3" ErrorMessage="Please Accept Terms and Conditions" ClientValidationFunction="ValidateCheckBox"></asp:CustomValidator>
                    </div>
                    <div class="check-box" id="DivAgreement" runat="server" visible="false">
                        <asp:CheckBox ID="chkagreeable" runat="server" /><p>
                            <sc:FieldRenderer ID="txtAgreement" runat="server" FieldName="Agreement Text" />
                            <sc:Link Field="Agreement URL" runat="server" ID="hyplnkAgreement">
                                <span class="mandatory">
                                    <sc:Text Field="Agreement URL Text" runat="server" ID="txAgree" />
                                </span>
                            </sc:Link>
                        </p>
                        <asp:CustomValidator ID="CustomValidator2" runat="server" ForeColor="Red" ValidationGroup="step3" ErrorMessage="Please Accept Terms and Conditions" ClientValidationFunction="ValidateCheckBox1"></asp:CustomValidator>
                    </div>
                    <div class="check-box" id="DivSupporting" runat="server" visible="false">

                        <asp:CheckBox ID="chkDetails" runat="server" />
                        <sc:FieldRenderer ID="txtSupporting" runat="server" FieldName="Supporting Documents Text" />
                        <asp:CustomValidator ID="CustomValidator3" runat="server" ForeColor="Red" ValidationGroup="step3" ErrorMessage="Please Accept Terms and Conditions" ClientValidationFunction="ValidateCheckBox2"></asp:CustomValidator>
                    </div>
                </div>
                <div>
                    <label><span class="mandatory">*</span>Enter verification code</label><br />
                    <div>

                        <asp:TextBox ID="txt_CaptchaVal" runat="server" Visible="false"></asp:TextBox>
                        <asp:Image ID="Image1" runat="server" />

                        <asp:TextBox ID="txt_Captcha" MaxLength="4" Style="vertical-align: top" placeholder="Enter Captcha" Width="200px" runat="server" CssClass="form-select ValidationRequired"></asp:TextBox>

                        <asp:ImageButton ID="ImageButton1" Style="vertical-align: top" runat="server" ImageUrl="/SG50/images/PG/refreshIcon.gif"
                            CausesValidation="false" Text="refresh" CssClass="cancel" OnClick="Button1_Click1" />
                        <br />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ErrorMessage="Please enter Captcha" Style="font-size: 14px; font-weight: bolder; padding-bottom: 5px" Display="Dynamic" ValidationGroup="step3" ForeColor="Red" ControlToValidate="txt_Captcha"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <table>
                    <tr>
                        <td style="width: 10px"></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="next-step">
                                <asp:Button ID="btnStepBack" CssClass="cancel" runat="server" OnClientClick="Step2();" Text="BACK" OnClick="btnStepBack_Click" />
                            </div>
                        </td>
                        <td style="width: 10px"></td>
                        <td>
                            <div class="next-step">
                                <asp:Button ID="Button1" ValidationGroup="step3" runat="server" Text="SUBMIT" OnClick="btnSubmit_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!------form content Ends------------>




        </div>
    </section>





</section>
