﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Share_an_idea_sublayout.Share_an_idea_sublayoutSublayout" CodeFile="~/SG50/layouts/Share An Idea Sublayout.ascx.cs" %>
<%--<script language="javascript" type="text/javascript" src="/SG50/include/js/modalpopup.js"></script>--%>

<script type="text/javascript">
    /*
    Cross-browser Modal Popup using Javascript (JQuery)
    */

    //Modal popup background ID. 
    //This value should be unique so that it does not conflict with other IDs in the page.
    var _ModalPopupBackgroundID = 'backgroundPopup_XYZ_20110418_Custom';

    function appearModalPopup(modalPopupID) {

        //Setting modal popup window

        //Boolean to detect IE6.
        var isIE6 = (navigator.appVersion.toLowerCase().indexOf('msie 6') > 0);

        var popupID = "#" + modalPopupID;

        //Get popup window margin top and left
        var popupMarginTop = $(popupID).height() / 2.5;
        var popupMarginLeft = $(popupID).width() / 2;

        //Set popup window left and z-index
        //z-index of modal popup window should be higher than z-index of modal background
        $(popupID).css({
            'left': '50%',
            'z-index': 9999
        });

        //Special case for IE6 because it does not understand position: fixed
        if (isIE6) {
            $(popupID).css({
                'top': $(document).scrollTop(),
                'margin-top': $(window).height() / 2 - popupMarginTop,
                'margin-left': -popupMarginLeft,
                'display': 'block',
                'position': 'absolute'
            });
        }
        else {
            $(popupID).css({
                /*'top': '40%',
                'margin-top': -popupMarginTop,*/
                top: '140px',
                'margin-left': -popupMarginLeft,
                'display': 'block',
                'position': 'fixed'
            });
        }

        //Automatically adding modal background to the page.
        var backgroundSelector = $('<div id="' + _ModalPopupBackgroundID + '" ></div>');

        //Add modal background to the body of the page.
        backgroundSelector.appendTo('body');

        //Set CSS for modal background. Set z-index of background lower than popup window.
        backgroundSelector.css({
            'width': $(document).width(),
            'height': $(document).height(),
            'display': 'none',
            'position': 'absolute',
            'top': 0,
            'left': 0,
            'z-index': 9990
        });

        backgroundSelector.fadeTo('fast', 0.8);

        // Set to default
        for (var i = 1; i < 11; i++) {
            $('#' + "faq" + i + "ID" + ' p a img').attr("src", "/SG50/images/celebration-faq/icon1.png");
            $("#" + "faq" + i + "ID").css("color", "black");
            $("#" + "faq" + i + "ConID").hide();
        }
    }

    function closeModalPopup(modalPopupID) {
        //Hide modal popup window
        $("#" + modalPopupID).css('display', 'none');

        //Remove modal background from DOM.
        $("#" + _ModalPopupBackgroundID).remove();
    }

    function HideContent(faqID, faqConID) {
        var a = $('#' + faqID + ' p a img').attr("src").replace(/\\/g, '/');
        var b = a.substring(a.lastIndexOf('/') + 1).split('.')[0];
        if (b != "undefined") {
            if (b == "icon1") {
                $('#' + faqID + ' p a img').attr("src", "/SG50/images/celebration-faq/icon2.png");
                $("#" + faqID).css("color", "white");
                $("#" + faqConID).show();
            }
            else {
                $('#' + faqID + ' p a img').attr("src", "/SG50/images/celebration-faq/icon1.png");
                $("#" + faqID).css("color", "black");
                $("#" + faqConID).hide();
            }
        }
    }
</script>

<style type="text/css" >
    .ball1
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 588px;
        left: 127px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball2
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 525px;
        left: 83px;
        border-radius: 50%;
        width: 60px;
        height: 60px;
    }

    .ball3
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 388px;
        left: 126px;
        border-radius: 50%;
        width: 170px;
        height: 170px;
    }

        .ball3 div
        {
            text-align: center;
            margin-top: 35px;
            margin-left: 20px;
            margin-right: 20px;
            margin-bottom: 35px;
        }

        .ball3 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball4
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 283px;
        left: 198px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball4 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball4 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball5
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 142px;
        left: 278px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball5 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball5 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball6
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 250px;
        left: 298px;
        border-radius: 50%;
        width: 60px;
        height: 60px;
    }

        .ball6 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball6 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball7
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 327px;
        left: 313px;
        border-radius: 50%;
        width: 60px;
        height: 60px;
    }

        .ball7 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball7 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball8
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 437px;
        left: 313px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball8 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball8 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }


    .ball9
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 243px;
        left: 366px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball9 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball9 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball10
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 381px;
        left: 384px;
        border-radius: 50%;
        width: 60px;
        height: 60px;
    }

        .ball10 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball10 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball11
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 107px;
        left: 471px;
        border-radius: 50%;
        width: 170px;
        height: 170px;
    }

        .ball11 div
        {
            text-align: center;
            margin-top: 35px;
            margin-left: 20px;
            margin-right: 20px;
            margin-bottom: 35px;
        }

        .ball11 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball12
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 285px;
        left: 537px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball12 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball12 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball13
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 338px;
        left: 452px;
        border-radius: 50%;
        width: 80px;
        height: 80px;
    }

        .ball13 div
        {
            font-size: 10px;
            text-align: center;
            margin-top: 15px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball13 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball14
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 430px;
        left: 465px;
        border-radius: 50%;
        width: 45px;
        height: 45px;
    }

        .ball14 div
        {
            font-size: 6px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball14 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball15
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 475px;
        left: 424px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball15 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball15 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball16
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 116px;
        left: 646px;
        border-radius: 50%;
        width: 80px;
        height: 80px;
    }

        .ball16 div
        {
            font-size: 10px;
            text-align: center;
            margin-top: 15px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball16 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball17
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 210px;
        left: 642px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball17 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball17 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball18
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 306px;
        left: 603px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball18 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball18 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball19
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 373px;
        left: 530px;
        border-radius: 50%;
        width: 165px;
        height: 165px;
    }

        .ball19 div
        {
            text-align: center;
            margin-top: 35px;
            margin-left: 20px;
            margin-right: 20px;
            margin-bottom: 35px;
        }

        .ball19 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball20
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 515px;
        left: 490px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball20 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball20 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball21
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 565px;
        left: 539px;
        border-radius: 50%;
        width: 70px;
        height: 70px;
    }

        .ball21 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball21 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball22
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 546px;
        left: 616px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball22 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball22 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball23
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 610px;
        left: 609px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball23 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball23 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball24
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 498px;
        left: 690px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball24 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball24 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball25
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 384px;
        left: 706px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball25 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball25 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball26
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 328px;
        left: 683px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball26 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball26 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball27
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 234px;
        left: 773px;
        border-radius: 50%;
        width: 170px;
        height: 170px;
    }

        .ball27 div
        {
            text-align: center;
            margin-top: 35px;
            margin-left: 20px;
            margin-right: 20px;
            margin-bottom: 35px;
        }

        .ball27 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball28
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 370px;
        left: 906px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball28 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball28 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball29
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 458px;
        left: 826px;
        border-radius: 50%;
        width: 100px;
        height: 100px;
    }

        .ball29 div
        {
            font-size: 8px;
            text-align: center;
            margin-top: 18px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .ball29 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball30
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 470px;
        left: 969px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball30 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball30 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball31
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 315px;
        left: 968px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball31 div
        {
            font-size: 4px;
            text-align: center;
            word-wrap: break-word;
            overflow: hidden;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball31 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball32
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 342px;
        left: 1016px;
        border-radius: 50%;
        width: 170px;
        height: 170px;
    }

        .ball32 div
        {
            text-align: center;
            margin-top: 35px;
            margin-left: 20px;
            margin-right: 20px;
            margin-bottom: 35px;
        }

        .ball32 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball33
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 178px;
        left: 402px;
        border-radius: 50%;
        width: 55px;
        height: 55px;
    }

        .ball33 div
        {
            font-size: 4px;
            text-align: center;
            margin-top: 10px;
            margin-right: 5px;
            margin-left: 5px;
        }

        .ball33 hr
        {
            border-width: 0;
            color: white;
            background-color: white;
            margin-left: 5px;
            margin-right: 5px;
            height: 1px;
        }

    .ball34
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 352px;
        left: 165px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball35
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 238px;
        left: 250px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball36
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 410px;
        left: 297px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball37
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 518px;
        left: 285px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball38
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 530px;
        left: 398px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball39
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 288px;
        left: 488px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball40
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 353px;
        left: 540px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball41
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 578px;
        left: 498px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball42
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 614px;
        left: 715px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball43
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 237px;
        left: 748px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball44
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 313px;
        left: 735px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball45
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 558px;
        left: 792px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball46
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 488px;
        left: 784px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball47
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 426px;
        left: 819px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball48
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 413px;
        left: 862px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball49
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 493px;
        left: 932px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .ball50
    {
        position: absolute;
        color: #fff;
        background-color: #CC3333;
        top: 311px;
        left: 1037px;
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }

    .shareAnIdea a, .shareAnIdea a:hover, .shareAnIdea a:visited
    {
        text-decoration: none;
        color: white;
    }

    /*.regionDropdown select {
        background-color: transparent;
        //-webkit-appearance: none;
        border: 0px;
        border-radius: 0;
        font-weight: bold;
        color: #666666;
        font-size: 20px;
        padding-top: 5px;
    }

        .regionDropdown select option {
            background-color: transparent;
            border: 0px;
            border-radius: 0;
            font-weight: bold;
            color: #666666;
            font-size: 26px;
            -webkit-appearance: none;
        }*/

    .regionDropdown div
    {
        font-weight: bold;
        color: #666666;
        font-size: 26px;
        padding-top: 2px;
    }

    .regionDropdown .non-selected
    {
        color: #b0adad;
    }

    .regionDropdown .selected
    {
        color: #5a5858;
    }

    .noOfideasStyle
    {
        font-weight: bold;
        color: #666666;
        font-size: 26px;
        line-height: 30px;
    }

    .ball
    {
        word-break: break-all;
    }

    .btnSearch, .txtSearch
    {
        float: left;
    }

    .txtSearch
    {
        width: 200px;
        height: 25px;
        padding-left: 5px;
        border: 1px solid #999;
        font-size: 16px;
        color: #999;
        font-size: 14px;
    }

    .learnMore
    {
        clear: both;
        width: 200px;
        float: left;
        text-align: right;
        margin-top: 10px;
    }

    #rightSide .learnMore a
    {
        font-size: 14px;
    }

    .ideaSearch
    {
        margin-top: 20px;
    }

    #rightSide #noOfIdeas
    {
        /*top:120px;*/
        position: relative;
        top: 0;
        float: left;
        padding-top: 10px;
    }

    #rightSide #sortBy
    {
        /*top:160px;*/
        position: relative;
        top: 0;
        float: left;
    }

    #ddlRegion
    {
        cursor: default;
        position: relative;
    }

        #ddlRegion img
        {
            border: 0px;
            top: 12px;
            position: absolute;
            left: 117px;
        }

    #ddlRecent
    {
        display: none;
        cursor: default;
    }

    #ddlSeeAll
    {
        display: none;
        cursor: default;
        top: 85px;
    }

   #ddlSeeAll:hover
        {
            color: #999;
        }

    #ddlRegion:hover
    {
        color: #999;
    }

    #learnMorePopUp
    {
        padding: 50px;
        width: 450px;
    }

    .popupClose
    {
        background: #5c5959;
        padding: 10px;
        position: absolute;
        right: 0;
        top: 0;
    }

    #shareAnIdeaTitle
    {
        margin-top: 30px;
    }

    #submitButton
    {
        top: 130px;
    }
</style>

<script type="text/javascript">
    function selectSorting() {
        var e = document.getElementById('ddlSorting');
        var sorting = e.options[e.selectedIndex].value;
        if (sorting == "region") {
            window.location = "<%= getRegionLink() %>";
        } else {
            window.location = "<%= getRecentLink() %>";
        }

    }

    function openDdl() {
        if ($('#ddlRecent').css('display') == 'none') {
            $('#ddlRecent').css('display', 'block');
            $('#ddlSeeAll').css('display', 'block');
        } else {
            $('#ddlRecent').css('display', 'none');
            $('#ddlSeeAll').css('display', 'none');
        }
    }

    function closeDdl() {
        window.location = "<%= getRecentLink() %>";
    }

    function closeDdl2() {
        window.location = "<%= getSeeAllLink() %>";
    }

    function openDdlMob() {

        if ($('#ddlRegionMob').css('display') == 'none') {
            $('#ddlSeeAllMob').css('display', 'block');
            $('#ddlRegionMob').css('display', 'block');
        } else {
            $('#ddlRegionMob').css('display', 'none');
            $('#ddlSeeAllMob').css('display', 'none');
        }
    }

    function focusTxtbox() {
        $('#<%=txtSearch.ClientID%>').focus();
    }


</script>

<style type="text/css">
    @media screen and (min-width: 300px) and (max-width: 768px)
    {
        #rightSide
        {
            display: none;
        }

        #shareAnIdeaTitle
        {
            width: 95% !important;
        }

        .txtSearch
        {
            width: 100% !important;
            padding-left:0!important;
            height:27px;
        }

        .learnMore
        {
            width: 100% !important;
        }

        .ball .mobile
        {
            width: 200px;
            height: 200px;
            margin-top: 0;
        }

        .ball
        {
            margin-left: 0;
            margin-right: 0;
        }

        .desktopp
        {
            display:none;
        }
    }

    .btnSearchMob
    {
        background-color: transparent;
        background-image: url('/sg50/images/search.png');
        background-repeat: no-repeat;
        position: relative;
        width: 20px;
        width: 29px;
        height: 29px;
        background-position: center;
        border: none;
        border-left: 0 !important;
    }

    #rightSideMobile
    {
        margin: 10px;
        margin-top: 50px;
    }

    .txtSearchMob
    {
        border-right: 0 !important;
    }

    #noOfIdeasMob p
    {
        font-size: 0.7em!important;
    }

    #sortByMob p
    {
        font-size: 0.7em!important;
    }

    #ddlRecentMob
    {
        cursor: default;
        position: relative;
    }

    #ddlRegionMob
    {
        display: none;
        cursor: default;
    }

    #ddlSeeAllMob
    {
        display: none;
        cursor: default;
    }

    #ddlRecentMob img
    {
        margin-bottom: 8px;
        margin-left: 5px;
    }

    #ddlSeeAllMob:hover
    {
        color: #999;
    }

    #ddlRegionMob:hover
    {
        color: #999;
    }

    .mobileDiv
    {
        display: none;
    }

    .mobile div
    {
        margin-top: 0;
        padding-top: 25%;
    }
</style>

<div class="shareAnIdea">
    <%--<img src="/SG50/images/1.jpg" style="position: absolute; top: 0; left: 0; z-index: -1;" />--%>
    <div id="shareAnIdeaTitle">
        <h1>SOME BIG, SOME SMALL. SOME HEARTFELT AND OTHERS, PLAIN WACKY.</h1>
        <h3>Submission for celebration ideas is now over. While we pick some to bring to life, see how your fellow Singaporeans want to celebrate our nation’s 50th. </h3>
    </div>
    <!-- <div id="submitButton">
        <a href="<%= getSubmitAnIdeaLink() %>">
            <img src="/SG50/images/arrow black.png" />
            <span style="font-size: 18px; letter-spacing: 2px;">SUBMIT</span></a>
    </div> -->
    <div id="rightSide">
        <div class="ideaSearch">
            <div onclick="focusTxtbox()">
                <asp:TextBox ID="txtSearch" MaxLength="200" CssClass="txtSearch" runat="server" Text="" ></asp:TextBox>
            </div>
            <asp:Button CssClass="btnSearch" ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="" />

            <%--<div class="btnSearch" id="btnSearch" runat="server" onclick="btnSearch_Click">search</div>--%>
            <div class="errorMsg">
                <asp:RequiredFieldValidator ID="rfvSearch" runat="server" Display="Dynamic" ErrorMessage="Please enter your name to search" ControlToValidate="txtSearch" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexSearch" ControlToValidate="txtSearch" Display="Dynamic"
                    ErrorMessage="Invalid characters." runat="server" ValidationExpression="^[a-zA-Z'.\s]{1,255}$" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RegularExpressionValidator>
            </div>
            <div id="noResult" class="noResult" runat="server" visible="false" style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;">
                No results found.
            </div>
        </div>
        <div class="learnMore">
            <a href="#" onclick="appearModalPopup('learnMorePopUp'); return false;">Learn More</a>
        </div>
        <div id="learnMorePopUp">
            <a href="#" onclick="closeModalPopup('learnMorePopUp'); return false;" class="popupClose">
                <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
            </a>
            <div class="learnMorePopUpTitle">
                <sc:Text Field="Learn More Title" DataSource="{2867D347-F425-4D0C-BC54-C9E33A78D31F}" runat="server" />
            </div>
            <div class="learnMorePopUpContent">
                <sc:Text Field="Learn More Content" DataSource="{2867D347-F425-4D0C-BC54-C9E33A78D31F}" runat="server" />
            </div>
        </div>
        <div id="noOfIdeas">
            <p>no. of ideas</p>
            <h1><%--/sitecore/content/SG50/Settings/Idea Count--%>
                <span class="noOfideasStyle">
                    <sc:Text ID="Text1" Field="Value" DataSource="{A11A26C5-6BCE-4676-B856-8285E1A56A95}" runat="server" /></span>
                </h1>
        </div>

        <div id="sortBy">
            <p style="margin-top: 15px;">
                <%--<select onchange="if(this.value == '') this.selectedIndex = 1; ">
                    <option value="">Sort by</option>
                    <option value="one">North</option>
                    <option value="two">East</option>
                    <option value="three">West</option>
                    <option value="four">South</option>
                </select>--%>
                sort by
            </p>
            <%--<h1>REGION</h1>--%>
            <%--<div class="regionDropdown">
                <select id="ddlSorting" onchange="selectSorting();">
                    <option value="region">REGION</option>
                    <option value="recent">RECENT</option>
                </select>

            </div>--%>
            <div class="regionDropdown">
                <div id="ddlRegion" onclick="openDdl();" class="selected">REGION<img src="/sg50/images/down_arrow.png" /></div>
                <div id="ddlRecent" onclick="closeDdl();" class="non-selected" style="position: absolute;">RECENT</div>
                <div id="ddlSeeAll" onclick="closeDdl2();" class="non-selected" style="position: absolute;">SEE ALL</div>
            </div>

        </div>
    </div>
    <div class="ball desktopp">
        <div class="ball1 desktop"></div>
        <div class="ball2 desktop"></div>

        <asp:Repeater ID="repIdeas" runat="server">
            <ItemTemplate>

                <div class="<%# DataBinder.Eval(Container.DataItem, "cssClass") %> mobile">
                    <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>" style="<%# DataBinder.Eval(Container.DataItem, "visible") %>">
                        <div>
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "ideaText") %></span>
                            <hr />
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "Name") %>, <%# DataBinder.Eval(Container.DataItem, "Age") %>
                            </br>
                                <%# DataBinder.Eval(Container.DataItem, "Region") %>
                            </span>
                        </div>
                    </a>
                </div>

            </ItemTemplate>
        </asp:Repeater>
        <%--<div class="ball3 mobile"><span>west</span></div>
        <div class="ball4 mobile"><span>west</span></div>
        <div class="ball13 mobile"><span>west</span></div>
        <div class="ball6 mobile"><span>west</span></div>
        <div class="ball7 mobile"><span>west</span></div>
        <div class="ball8 mobile"><span>west</span></div>
        <div class="ball9 mobile"><span>west</span></div>
        <div class="ball10 mobile"><span>west</span></div>--%>

        <%--        <div class="ball11 mobile"><span>north</span></div>
        <div class="ball12 mobile"><span>north</span></div>
        <div class="ball5 mobile"><span>north</span></div>
        <div class="ball17 mobile"><span>north</span></div>
        <div class="ball18 mobile"><span>north</span></div>
        <div class="ball16 mobile"><span>north</span></div>

        <div class="ball14 mobile"><span>sOUTH</span></div>
        <div class="ball15 mobile"><span>sOUTH</span></div>
        <div class="ball19 mobile"><span>sOUTH</span></div>
        <div class="ball20 mobile"><span>sOUTH</span></div>
        <div class="ball21 mobile"><span>sOUTH</span></div>
        <div class="ball22 mobile"><span>sOUTH</span></div>
        <div class="ball23 mobile"><span>sOUTH</span></div>
        <div class="ball24 mobile"><span>sOUTH</span></div>

        <div class="ball25 mobile"><span>east</span></div>
        <div class="ball26 mobile"><span>east</span></div>
        <div class="ball27 mobile"><span>east</span></div>
        <div class="ball28 mobile"><span>east</span></div>
        <div class="ball29 mobile"><span>east</span></div>
        <div class="ball30 mobile"><span>east</span></div>
        <div class="ball31 mobile"><span>29east</span></div>
        <div class="ball32 mobile"><span>east</span></div>--%>

        <div class="ball33 desktop"></div>
        <div class="ball34 desktop"></div>
        <div class="ball35 desktop"></div>
        <div class="ball36 desktop"></div>
        <div class="ball37 desktop"></div>
        <div class="ball38 desktop"></div>
        <div class="ball39 desktop"></div>
        <div class="ball40 desktop"></div>
        <div class="ball41 desktop"></div>
        <div class="ball42 desktop"></div>
        <div class="ball43 desktop"></div>
        <div class="ball44 desktop"></div>
        <div class="ball45 desktop"></div>
        <div class="ball46 desktop"></div>
        <div class="ball47 desktop"></div>
        <div class="ball48 desktop"></div>
        <div class="ball49 desktop"></div>
        <div class="ball50 desktop"></div>
    </div>

    <div class="ball mobileDiv">
        <div id="sliderIdea">
            <a class="buttons prev" href="#"><span><img src="\sg50\images\left.png" /></span></a>
            <div class="viewport">
                <ul class="overview">
                    <asp:Repeater ID="repIdeasMobile" runat="server">
                        <ItemTemplate>
                            <li>
                                <div class="mobile">
                                    <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>">
                                        <div>
                                            <span>
                                                <%# DataBinder.Eval(Container.DataItem, "ideaText") %></span>
                                            <hr />
                                            <span>
                                                <%# DataBinder.Eval(Container.DataItem, "Name") %>, <%# DataBinder.Eval(Container.DataItem, "Age") %>
                                        </br>
                                            <%# DataBinder.Eval(Container.DataItem, "Region") %>
                                            </span>
                                        </div>
                                    </a>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <a class="buttons next" href="#"><span><img src="\sg50\images\right.png" /></span></a>
        </div>


        <div id="rightSideMobile">

            <div id="noOfIdeasMob" style="width: 49%; float: left; margin-right: 2%">
                <p style="border-bottom: 1px solid #000000;">no of ideas</p>
                <%--/sitecore/content/SG50/Settings/Idea Count--%>
                <h1><span class="noOfideasStyle">
                    <sc:Text ID="Text5" Field="Value" DataSource="{A11A26C5-6BCE-4676-B856-8285E1A56A95}" runat="server" />
                </span></h1>
            </div>

            <div id="sortByMob" style="width: 49%; float: left;">
                <p style="border-bottom: 1px solid #000000;">
                    sort by
                </p>
                <div class="regionDropdown">
                    <div id="ddlRecentMob" onclick="openDdlMob();" class="selected">RECENT<img src="\sg50\images\down_arrow.png" /></div>
                    <div id="ddlRegionMob" onclick="closeDdl();" class="non-selected" style="position: absolute;">REGION</div>
                    <div id="ddlSeeAllMob" onclick="closeDdl2();" class="non-selected" style="position: absolute; margin-top: 30px;">SEE ALL</div>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div class="ideaSearch">

                <div style="min-width: 300px; max-width: 760px;">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="29">
                                <div onclick="focusTxtboxMob()">
                                    <asp:TextBox Width="100%" ID="txtSearchMob" MaxLength="200" CssClass="txtSearch txtSearchMob" runat="server" Text="" ></asp:TextBox>
                                </div>
                            </td>
                            <td width="29"  height="29" vallign="top" style="vertical-align:top;">
                                <div style="width:29px; height:29px; border:1px solid #999999; border-left:none;">
                                <asp:Button ValidationGroup='mobile' CssClass="btnSearchMob" ID="btnSearchMob" OnClick="btnSearchMob_Click" runat="server" Text="" />

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="errorMsg">
                    <asp:RequiredFieldValidator ValidationGroup='mobile' ID="rfvSearchMob" runat="server" Display="Dynamic" ErrorMessage="Please enter your name to search" ControlToValidate="txtSearchMob" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup='mobile' ID="regexSearchMob" ControlToValidate="txtSearchMob" Display="Dynamic"
                        ErrorMessage="Invalid characters." runat="server" ValidationExpression="^(a-z|A-Z|0-9)*[^@#$%^&*()']*$" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RegularExpressionValidator>
                </div>

                <div id="noResultMob" class="noResult" runat="server" visible="false" style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 3px;">
                    No results found.
                </div>

            </div>
            <!-- // idSearch -->
            <div class="learnMore" style="width: 100%; margin-bottom: 20px;">
                <a href="#" onclick="shModalPopup('learnMorePopUp'); return false;">Learn More</a>
            </div>
        </div>

        <%--<asp:Repeater ID="repIdeasMobile" runat="server">
            <ItemTemplate>
                <div class="mobile">
                    <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>">
                        <div>
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "ideaText") %></span>
                            <hr />
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "Name") %>, <%# DataBinder.Eval(Container.DataItem, "Age") %>
                            </br>
                                <%# DataBinder.Eval(Container.DataItem, "Region") %>
                            </span>
                        </div>
                    </a>
                </div>
            </ItemTemplate>
        </asp:Repeater>--%>
    </div>

     <script type="text/javascript">

         $(document).ready(function () {
            
             $('#sliderIdea').tinycarousel(
                 { infinite: false }
                 );
         });
    </script>
</div>
