﻿using System;
using Sitecore.Data.Items;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Links;
using Sitecore.Diagnostics;

using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using Sitecore.Configuration;
using Sitecore.Controls;
using Sitecore.Diagnostics;
using Sitecore.Web.UI.WebControls;
using Sitecore.SecurityModel;
using Sitecore.Security.Accounts;
using CAPTCHA;
using System.Web.UI;
namespace Layouts.Contact_sublayout
{

    /// <summary>
    /// Summary description for Contact_sublayoutSublayout
    /// </summary>
    public partial class Contact_sublayoutSublayout : System.Web.UI.UserControl
    {
        Item itmContext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
        }


        protected string getHomeLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Home_Item_ID));
            }
            return link;
        }
    }
}