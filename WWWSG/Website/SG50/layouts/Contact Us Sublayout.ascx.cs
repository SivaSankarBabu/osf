﻿using System;
using Sitecore.Data.Items;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Links;
using Sitecore.Diagnostics;

using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;
using Sitecore.Configuration;
using Sitecore.Controls;
using Sitecore.Diagnostics;
using Sitecore.Web.UI.WebControls;
using Sitecore.SecurityModel;
using Sitecore.Security.Accounts;
using CAPTCHA;
using System.Web.UI;
namespace Layouts.Contact_us_sublayout
{

    /// <summary>
    /// Summary description for Contact_us_sublayoutSublayout
    /// </summary>
    public partial class Contact_us_sublayoutSublayout : System.Web.UI.UserControl
    {
        Item itmContext = Sitecore.Context.Item;
        CaptchaGenerator cgen;
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            pnSuccess.Visible = false;
            if (!IsPostBack)
            {
                cgen = new CaptchaGenerator();
                GetCaptchaimage();
            }
        }
        private void GetCaptchaimage()
        {


            string strpath = Server.MapPath("/SG50/images/PG/") + "CaptchaImage.gif";
            cgen.ResetCaptchaColor();

            txt_CaptchaVal.Text = cgen.GenerateCaptcha(strpath);


            string strpath2 = "/SG50/layouts/PG/sublayouts/" + "CaptchaImage.ashx?id=" + strpath;
            Image1.ImageUrl = strpath2;



        }
        protected void Button1_Click1(object sender, ImageClickEventArgs e)
        {
            cgen = new CaptchaGenerator();
            GetCaptchaimage();
        }
        protected string getHomeLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Home_Item_ID));
            }
            return link;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            //if (isValidated())
            //{
            if (txt_CaptchaVal.Text == SG50Class.StripUnwantedString(txt_Captcha.Text))
            {
                SendEmail(SG50Class.web.GetItem(SG50Class.str_Email_Item_ID)["Value"], subjectTextBox.Text, messageTextBox.Text, nameTextBox.Text, SG50Class.StripUnwantedString(emailTextBox.Text));

                SendAcknowledgementEmail(SG50Class.StripUnwantedString(emailTextBox.Text), "Acknowledgement of your email", AcknowledgementContent());

                resetTextBox();
                //vldCaptcha.ErrorMessage = String.Empty;
                pnSuccess.Visible = true;
                contactUsForm.Visible = false;
                welcomeText.Visible = false;
            }
          //  }
        }

        private bool isValidated()
        {
            bool isValid = true;

            //check name
            if (nameTextBox.Text.Trim().Length == 0)
            {
                isValid = false;
                if (nameTextBox.Text.Trim().Length == 0)
                {
                    rfvname.IsValid = false;
                }
            }

            //check email
            if (emailTextBox.Text.Trim().Length == 0)
            {
                isValid = false;
                rfvemail.IsValid = false;
            }
            else
            {
                // check regular expression
                string name = SG50Class.StripUnwantedString(emailTextBox.Text.ToString());
                Match match = Regex.Match(name, regexemail.ValidationExpression);
                if (!match.Success)
                {
                    isValid = false;
                    regexemail.IsValid = false;
                }
            }

            //check subject
            if (subjectTextBox.Text.Trim().Length == 0)
            {
                isValid = false;
                if (subjectTextBox.Text.Trim().Length == 0)
                {
                    rfvsubject.IsValid = false;
                }
            }

            //check message
            if (messageTextBox.Text.Trim().Length == 0)
            {
                isValid = false;
                if (messageTextBox.Text.Trim().Length == 0)
                {
                    rfvmessage.IsValid = false;
                }
            }

            //if (!captcha.IsValid)
            //{
            //    isValid = false;
            //    vldCaptcha.IsValid = false;
            //    vldCaptcha.ErrorMessage = "The characters you typed did not match the characters in the image.";
            //}

            return isValid;

        }

        protected void SendEmail(string toAddress, string subject, string body, string name, string toCCAddress)
        {
            string result = "Message Sent Successfully..!!";
            string senderID = SG50Class.web.GetItem(SG50Class.str_Email_Item_ID)["Sender ID"];// use sender's email id here..
            const string senderPassword = ""; // sender password here...
            try
            {
                StringBuilder emailContent = new StringBuilder();
                emailContent.Append("Dear Admin,<br /><br />");
                emailContent.Append(body + "<br />");
                emailContent.Append("<br /><br />");
                emailContent.Append("Regards,<br />");
                emailContent.Append(name + "(" + emailTextBox.Text.ToString() + ")");


                SmtpClient smtp = new SmtpClient
                {
                    Host = SG50Class.web.GetItem(SG50Class.str_Email_Item_ID)["Host"], // smtp server address here...
                    Port = 25,
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    //Credentials = new System.Net.NetworkCredential(senderID, senderPassword),
                    //Timeout = 30000

                };

                string[] emailTo = toAddress.Split(',');

                MailMessage message = new MailMessage(senderID, emailTo[0], subject, emailContent.ToString());

                for (int i = 1; i < emailTo.Length; i++)
                {
                    message.To.Add(emailTo[i]);
                }
                //message.CC.Add(toCCAddress);
                message.IsBodyHtml = true;
                smtp.Send(message);

            }
            catch (Exception ex)
            {
                result = ex + "Error sending email.!!!";
            }
        }

        private void resetTextBox()
        {
            nameTextBox.Text = "";
            emailTextBox.Text = "";
            subjectTextBox.Text = "";
            messageTextBox.Text = "";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "clientscript", "document.getElementById('content_0_captcha_CaptchaInput').value='';", true);
        }

        private string AcknowledgementContent()
        {
            StringBuilder message = new StringBuilder();
            message.Append("Dear Sir / Mdm,");
            message.Append("<br /><br />");
            message.Append("Thank you for your feedback. ");
            message.Append("We will be looking into your feedback and aim to respond to you within 3-7 working days. ");
            message.Append("We seek your understanding and patience ahead if more time is needed to respond on the issue/s you have raised. ");
            message.Append("If you have any enquiries, do call us at 6338 3632 (Mon - Fri: 9am to 6pm). ");
            message.Append("<br /><br />");
            message.Append("To learn more about SG50, please visit our website at <a href='www.Singapore50.sg'>www.Singapore50.sg</a>.");
            message.Append("<br /><br />");
            message.Append("Thank you and have a pleasant day.");

            return message.ToString();
        }

        protected void SendAcknowledgementEmail(string toAddress, string subject, string body)
        {
            string result = "Message Sent Successfully..!!";
            string senderID = SG50Class.web.GetItem(SG50Class.str_Email_Item_ID)["Sender ID"];// use sender's email id here..
            const string senderPassword = ""; // sender password here...
            try
            {
                //StringBuilder emailContent = new StringBuilder();
                //emailContent.Append("Dear Admin,<br /><br />");
                //emailContent.Append(body + "<br />");
                //emailContent.Append("<br /><br />");
                //emailContent.Append("Regards,<br />");
                //emailContent.Append(name);

                SmtpClient smtp = new SmtpClient
                {
                    Host = SG50Class.web.GetItem(SG50Class.str_Email_Item_ID)["Host"], // smtp server address here...
                    Port = 25,
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,

                    //Credentials = new System.Net.NetworkCredential(senderID, senderPassword),
                    //Timeout = 30000

                };

                string[] emailTo = toAddress.Split(',');

                MailMessage message = new MailMessage(senderID, emailTo[0], subject, body);


                for (int i = 1; i < emailTo.Length; i++)
                {
                    message.To.Add(emailTo[i]);
                }
                //message.CC.Add(toCCAddress);
                message.IsBodyHtml = true;
                smtp.Send(message);

            }
            catch (Exception ex)
            {
                result = ex + "Error sending email.!!!";
            }
        }
    }
}