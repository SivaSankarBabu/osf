﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Collectiblessublayout.CollectiblessublayoutSublayout" CodeFile="~/SG50/layouts/CollectiblesSublayout.ascx.cs" %>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript" language="javascript"></script>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/jquery.bxslider.min.js"></script>
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">

<%--<link href="/SG50/include/css/thematic-campaign-responsive.css" rel="stylesheet" type="text/css">--%>


<!--//new css-->
<%--<link href="css/thematic-campaign.css" rel="stylesheet" type="text/css">--%>
<script type="text/javascript">

    function Pager(tableName, itemsPerPage) {
        this.tableName = tableName; this.itemsPerPage = itemsPerPage;
        this.currentPage = 1; this.pages = 0; this.inited = false;
        this.showRecords = function (from, to) {
            var rows = document.getElementById(tableName).rows;
            for (var i = 1; i < rows.length; i++) {
                if (i < from || i > to)
                    rows[i].style.display = 'none'; else rows[i].style.display = '';
            }
        }

        this.showPage = function (pageNumber) {
            if (!this.inited) {
                alert("not inited"); return;
            }

            var oldPageAnchor = document.getElementById('pg' + this.currentPage);
            oldPageAnchor.className = 'pg-normal';
            this.currentPage = pageNumber;
            var newPageAnchor = document.getElementById('pg' + this.currentPage);
            newPageAnchor.className = 'pg-selected';
            var from = (pageNumber - 1) * itemsPerPage + 1;
            var to = from + itemsPerPage - 1;
            this.showRecords(from, to);
        }

        this.prev = function () {
            if (this.currentPage > 1)
                this.showPage(this.currentPage - 1);
        }

        this.next = function () {
            if (this.currentPage < this.pages) {
                this.showPage(this.currentPage + 1);
            }
        }

        this.init = function () {
            var rows = document.getElementById(tableName).rows;
            var records = (rows.length - 1);
            this.pages = Math.ceil(records / itemsPerPage);
            this.inited = true;
        }

        this.showPageNav = function (pagerName, positionId) {
            if (!this.inited) {
                alert("not inited"); return;
            }
            var element = document.getElementById(positionId);
            var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> « Prev </span> ';
            for (var page = 1; page <= this.pages; page++)
                pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> ';
            pagerHtml += '<span onclick="' + pagerName + '.next();" class="pg-normal"> Next »</span>';
            element.innerHTML = pagerHtml;
        }
    }
</script>

</style>
<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...
        // alert(Img);

        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            // caption: fbdes,
            description: fbdes
        };
        FB.ui(obj);

    }
    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function (response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }



</script>
<div class="container custom-res" id="homePage">
    <div class="masthead">
        <!-- Gallery Banner -->
        <div class="banner-gallery">
            <div class="banner-gallery-text">
                <div class="banner-gt-middle">
                    <div class="gt-middle">
                        <div class="photo-row">
                            <%--<span>Own a piece of<br />
                                    singapore's<br />
                                    History</span>--%>

                            <%--<sc:Text ID="txtBannerTitle" runat="server" Field="Banner Image Overlay Text" />--%>
                        </div>
                    </div>
                </div>
            </div>
            <%--<img src="images/banner-collectibles.jpg" class="banner-image" />--%>
            <sc:Image ID="ImgBanner" runat="server" Field="Banner Image" />
        </div>
        <!-- Gallery Banner -->
        <!-- Photos Header -->
        <div class="photo-header">
            <div class="photo-row">
                <div class="cell-12 GalleryT">
                    <div class="group-text-button">
                        <h1>
                            <sc:Text ID="txtTitle" runat="server" Field="Title" />
                        </h1>

                        <asp:Repeater ID="repsocialSharingTop" runat="server">
                            <ItemTemplate>
                                <div class="si-title">
                                    <a class="facebook" href="JavaScript:postToFeed('<%# Eval("SocialShareTitle").ToString()%>','<%# Eval("SocialShareContent").ToString()%>','<%# Eval("SocialSharingThumbNail")%>','<%# Eval("EventUrl")%>')"></a>
                                    <a class="twitter" href="JavaScript:newPopup('<%# Eval("SocialShareContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')"></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <p>
                        <sc:Text ID="txtDescription" runat="server" Field="Description" />
                    </p>
                </div>
            </div>
        </div>
        <!-- Photos Header -->
        <div class="photo-gallery CollectiblesCont">
            <div class="photo-row">
                <ul>
                    <asp:Repeater ID="repNewCollectibles" runat="server" OnItemCommand="repNewCollectibles_ItemCommand">
                        <ItemTemplate>
                            <li>
                                <div class="photo-cont">
                                    <img alt="<%# DataBinder.Eval(Container.DataItem, "ALT") %>" src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                                    <h1>
                                        <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label></h1>
                                    <p>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Label>
                                    </p>
                                    <a class="FindMore" visible='<%#Eval("Status")%>' runat="server" id="ReadButton" target="_blank" href='<%# DataBinder.Eval(Container.DataItem, "href") %>'><%# DataBinder.Eval(Container.DataItem, "ButtonText") %></a>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <br />
            <div style="overflow: hidden; margin-top: 367px !important;">
            </div>
            <div class="cell-12">
                <div class="pagination">
                    <ul>
                        <li>
                            <asp:LinkButton ID="PrevControl" runat="server" CssClass="pre" OnClick="PrevControl_Click"></asp:LinkButton>
                        </li>
                        <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                            <ItemTemplate>
                                <li>
                                    <asp:LinkButton ID="btnPage" Enabled='<%# DataBinder.Eval(Container.DataItem, "Enable") %>'
                                        Style='<%# DataBinder.Eval(Container.DataItem, "style") %>'
                                        CommandName="Page" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>'
                                        runat="server" ForeColor="White" Font-Bold="True"><%# DataBinder.Eval(Container.DataItem, "ID") %>
                                    </asp:LinkButton></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li>
                            <asp:LinkButton ID="NextControl" runat="server" CssClass="next" OnClick="NextControl_Click"></asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <%--<div id="pageNavPosition" style="padding-top: 20px" align="center">
        </div>
        <script type="text/javascript">
            var pager = new Pager('tablepaging', 2);
            pager.init();
            pager.showPageNav('pager', 'pageNavPosition');
            pager.showPage(2);
        </script>--%>
    </div>
</div>
<script type="text/javascript">var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>
<script>
    $(function () {
        if ($(window).width() <= 1023) {
            $(".banner-image").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });
            $(".photo-cont img").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });
        }
    });
</script>


