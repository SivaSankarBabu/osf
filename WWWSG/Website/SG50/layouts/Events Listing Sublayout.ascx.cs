﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using Sitecore.Data.Fields;
using System.Web.UI.HtmlControls;
namespace Layouts.Events_listing_sublayout
{

    /// <summary>
    /// Summary description for Events_listing_sublayoutSublayout
    /// </summary>
    public partial class Events_listing_sublayoutSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();

        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        int latestCount = 5;

        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();


        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                FillrepUpcomingEvents();
                FillrepPastEvents();
                foreach (RepeaterItem items in repUpcomingEvents.Items)
                {
                    HtmlGenericControl noRecordsDiv = (items.FindControl("DivImagestag") as HtmlGenericControl);
                    System.Web.UI.WebControls.Image Projected = new System.Web.UI.WebControls.Image();
                    Projected = (System.Web.UI.WebControls.Image)items.FindControl("repImg");

                    if (string.IsNullOrEmpty(Projected.ImageUrl) && string.IsNullOrWhiteSpace(Projected.ImageUrl))
                    {
                        noRecordsDiv.Visible = false;
                    }
                }

                foreach (RepeaterItem items in repPastEvents.Items)
                {
                    HtmlGenericControl noRecordsDiv = (items.FindControl("DivImagestag") as HtmlGenericControl);
                    System.Web.UI.WebControls.Image Projected = new System.Web.UI.WebControls.Image();
                    Projected = (System.Web.UI.WebControls.Image)items.FindControl("repImg");

                    if (string.IsNullOrEmpty(Projected.ImageUrl) && string.IsNullOrWhiteSpace(Projected.ImageUrl))
                    {
                        noRecordsDiv.Visible = false;
                    }
                }

            }
        }
        private void FillrepUpcomingEvents()
        {
            try
            {
                DataTable dtrepEvents = new DataTable();
                dtrepEvents.Columns.Add("cssClass", typeof(string));
                dtrepEvents.Columns.Add("item", typeof(Item));
                dtrepEvents.Columns.Add("href", typeof(string));
                dtrepEvents.Columns.Add("imgSrc", typeof(string));
                dtrepEvents.Columns.Add("Id", typeof(string));
                dtrepEvents.Columns.Add("SocialTitle", typeof(string));
                dtrepEvents.Columns.Add("SocialContent", typeof(string));
                dtrepEvents.Columns.Add("socialThumbnail", typeof(string));
                dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                dtrepEvents.Columns.Add("videoURL", typeof(string));
                dtrepEvents.Columns.Add("HostName", typeof(string));
                dtrepEvents.Columns.Add("AccessToken", typeof(string));
                dtrepEvents.Columns.Add("EventUrl", typeof(string));
                dtrepEvents.Columns.Add("EventDate", typeof(string));

                DataRow drrepEvents;

                Item itmPress = SG50Class.web.GetItem(SG50Class.str_Events_Item_ID);

                IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                          where
                                                          a.TemplateID.ToString().Equals(SG50Class.str_Event_Details_Page_Template_ID)
                                                          orderby
                                                          (a.Fields["Event Date"].Value.ToString() != "" ? Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Event Date"].Value.ToString()) : Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Start Date"].Value.ToString()))
                                                          select a;


                foreach (Item a in itmIEnumPressReleases)
                {

                    Sitecore.Data.Fields.DateField date = a.Fields["Event Date"];
                    Sitecore.Data.Fields.DateField fromdate = a.Fields["Start Date"];
                    Sitecore.Data.Fields.DateField todate = a.Fields["End Date"];
                    System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
                    System.DateTime fromdatetime = Sitecore.DateUtil.IsoDateToDateTime(fromdate.Value);
                    System.DateTime todatetime = Sitecore.DateUtil.IsoDateToDateTime(todate.Value);
                    if (datetime.Date.CompareTo(DateTime.Today.Date) >= 0 || (fromdatetime.Date.CompareTo(DateTime.Today.Date) <= 0) && (todatetime.Date.CompareTo(DateTime.Today.Date) >= 0) || (fromdatetime.Date.CompareTo(DateTime.Today.Date) >= 0) && (todatetime.Date.CompareTo(DateTime.Today.Date) >= 0))
                    {
                        drrepEvents = dtrepEvents.NewRow();
                        dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                    }
                }

                lblSearchResult.Visible = false;
                repUpcomingEvents.Visible = true;
                repUpcomingEvents.DataSource = dtrepEvents;
                repUpcomingEvents.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
            }
        }
        public DataRow PastSubMethod(DataRow drrepEvents, Item a)
        {
            try
            {

                //drrepEvents = dtrepEvents.NewRow();
                drrepEvents["cssClass"] = "a";
                drrepEvents["item"] = a;
                drrepEvents["href"] = LinkManager.GetItemUrl(a).ToString() + "?Past=Yes";
                string SocialTitle = string.Empty;
                string SocialContent = string.Empty;
                if (!string.IsNullOrEmpty(a.Fields["Social Share Title"].ToString()))
                    SocialTitle = a.Fields["Social Share Title"].ToString();
                else
                    SocialTitle = a.Fields["Menu Title"].ToString();

                if (!string.IsNullOrEmpty(a.Fields["Social Share Content"].ToString()))
                    SocialContent = cmObj.BuildString(a.Fields["Social Share Content"].ToString(), CommonMethods.faceBook);
                else
                    SocialContent = " ";

                drrepEvents["videoURL"] = a.Fields["Video Link"].ToString();
                drrepEvents["SocialTitle"] = cmObj.BuildString(SocialTitle, CommonMethods.faceBook);
                drrepEvents["SocialContent"] = cmObj.BuildString(SocialContent, CommonMethods.faceBook);
                drrepEvents["TwitterContent"] = cmObj.BuildString(SocialContent, CommonMethods.faceBook);// +" " + hostName + LinkManager.GetItemUrl(a).ToString();
                Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Social Share Image"]);
                string fbimgSrc = fbimgField.Src;
                if (!string.IsNullOrEmpty(fbimgSrc))
                {
                    drrepEvents["socialThumbnail"] = hostName + "/" + fbimgSrc;
                }
                else
                {
                    Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image Thumbnail"]);
                    if (!string.IsNullOrEmpty(staticimgField.Src))
                    {
                        drrepEvents["socialThumbnail"] = hostName + "/" + staticimgField.Src;
                    }
                    else
                    {
                        drrepEvents["socialThumbnail"] = "";
                    }

                }

                if (a.Fields["Image Thumbnail"] != null && !a["Image Thumbnail"].ToString().Equals(""))
                {
                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image Thumbnail"]);
                    string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                    if (!imgSrc.Equals(""))
                    {
                        drrepEvents["imgSrc"] = imgSrc;// +"?w=205&h=128";
                    }
                }



                drrepEvents["Id"] = a.ID;
                // dtrepEvents.Rows.Add(drrepEvents);

            }
            catch (Exception exSub1)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
            return drrepEvents;
        }
        private void FillrepPastEvents()
        {
            try
            {
                DataTable dtrepEvents = new DataTable();
                dtrepEvents.Columns.Add("cssClass", typeof(string));
                dtrepEvents.Columns.Add("item", typeof(Item));
                dtrepEvents.Columns.Add("href", typeof(string));
                dtrepEvents.Columns.Add("imgSrc", typeof(string));
                dtrepEvents.Columns.Add("Id", typeof(string));
                dtrepEvents.Columns.Add("SocialTitle", typeof(string));
                dtrepEvents.Columns.Add("SocialContent", typeof(string));
                dtrepEvents.Columns.Add("socialThumbnail", typeof(string));
                dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                dtrepEvents.Columns.Add("videoURL", typeof(string));

                DataRow drrepEvents;

                Item itmPress = SG50Class.web.GetItem(SG50Class.str_Events_Item_ID);

                IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                          where
                                                          a.TemplateID.ToString().Equals(SG50Class.str_Event_Details_Page_Template_ID)
                                                          orderby
                                                          (a.Fields["Event Date"].Value.ToString() != "" ? Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Event Date"].Value.ToString()) : Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Start Date"].Value.ToString()))
                                                          select a;


                foreach (Item a in itmIEnumPressReleases)
                {

                    Sitecore.Data.Fields.DateField date = a.Fields["Event Date"];
                    Sitecore.Data.Fields.DateField fromdate = a.Fields["Start Date"];
                    Sitecore.Data.Fields.DateField todate = a.Fields["End Date"];
                    System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
                    System.DateTime fromdatetime = Sitecore.DateUtil.IsoDateToDateTime(fromdate.Value);
                    System.DateTime todatetime = Sitecore.DateUtil.IsoDateToDateTime(todate.Value);
                    if (!string.IsNullOrEmpty(a.Fields["Event Date"].ToString()))
                    {
                        if (datetime.Date.CompareTo(DateTime.Today.Date) < 0)
                        {
                            drrepEvents = dtrepEvents.NewRow();
                            dtrepEvents.Rows.Add(PastSubMethod(drrepEvents, a));
                        }
                    }
                    else
                    {
                        if ((fromdatetime.Date.CompareTo(DateTime.Today.Date) <= 0) && (todatetime.Date.CompareTo(DateTime.Today.Date) <= 0))
                        {
                            drrepEvents = dtrepEvents.NewRow();
                            dtrepEvents.Rows.Add(PastSubMethod(drrepEvents, a));
                        }
                    }
                }


                repPastEvents.DataSource = dtrepEvents;
                repPastEvents.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
            }
        }
        public void DownloadCalendar(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    Item item = Sitecore.Context.Item;
                    FileField file = item.Fields["Calendar"];
                    if (file != null)
                    {
                        MediaItem mi = file.MediaItem;//web.GetItem(file.ID);
                        if (mi != null)
                        {
                            Stream stream = mi.GetMediaStream();
                            long fileSize = stream.Length;
                            byte[] buffer = new byte[(int)fileSize];
                            stream.Read(buffer, 0, (int)stream.Length);
                            stream.Close();

                            Response.ContentType = String.Format(mi.MimeType);
                            string fileName = (mi.Name + "." + mi.Extension).ToString().Replace(' ', '_');
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                            // Response.AppendHeader("Content-Disposition", "attachment; filename=" + mi.Name + "." + mi.Extension);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(buffer);
                            Response.End();
                        }
                        else { Response.Write("no mi"); }

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        public DataRow MonthYearCommanFun(DataRow drrepEvents, Item a, System.DateTime fromdatetime, System.DateTime datetime, System.DateTime todatetime)
        {
            try
            {
                // drrepEvents = dtrepEvents.NewRow();
                drrepEvents["HostName"] = hostName + "/";
                drrepEvents["AccessToken"] = accessToken;

                drrepEvents["cssClass"] = "a";
                drrepEvents["item"] = a;
                drrepEvents["href"] = LinkManager.GetItemUrl(a).ToString();
                drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(a).ToString();

                if (!string.IsNullOrEmpty(a.Fields["Event Date"].ToString()))
                {
                    drrepEvents["EventDate"] = datetime.ToString("dd MMMM yyyy");
                }
                else if (!string.IsNullOrEmpty(a.Fields["Start Date"].ToString()) && !string.IsNullOrEmpty(a.Fields["End Date"].ToString()))
                {
                    if (fromdatetime.ToString("dd MMMM yyyy") == todatetime.ToString("dd MMMM yyyy"))
                    {
                        drrepEvents["EventDate"] = fromdatetime.ToString("dd MMMM yyyy");
                    }
                    else
                    {
                        drrepEvents["EventDate"] = fromdatetime.ToString("dd MMMM yyyy") + " - " + todatetime.ToString("dd MMMM yyyy");
                    }
                }
                else
                {
                    drrepEvents["EventDate"] = "";
                }

                string SocialTitle = string.Empty;
                string SocialContent = string.Empty;
                string videourl = string.Empty;
                if (!string.IsNullOrEmpty(a.Fields["Social Share Title"].ToString()))
                    SocialTitle = a.Fields["Social Share Title"].ToString();
                else
                    SocialTitle = a.Fields["Menu Title"].ToString();

                if (!string.IsNullOrEmpty(a.Fields["Social Share Content"].ToString()))
                    SocialContent = cmObj.BuildString(a.Fields["Social Share Content"].ToString(), CommonMethods.faceBook);
                else
                {
                    string inputHTML = string.Empty, noHtml = string.Empty;
                    // SocialContent = cmObj.BuildString(a.Fields["Blurb"].ToString(), CommonMethods.faceBook);

                    inputHTML = a.Fields["Blurb"].ToString();
                    noHtml = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                    SocialContent = cmObj.BuildString(noHtml, CommonMethods.faceBook);
                }

                drrepEvents["SocialTitle"] = cmObj.BuildString(SocialTitle, CommonMethods.faceBook); ;
                drrepEvents["SocialContent"] = SocialContent;

                if (SocialContent.Length > 120)
                    drrepEvents["TwitterContent"] = SocialContent.Substring(0, 119);
                else
                    drrepEvents["TwitterContent"] = SocialContent.Substring(0, SocialContent.Length); 

                Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Social Share Image"]);
                string fbimgSrc = fbimgField.Src;
                if (!string.IsNullOrEmpty(fbimgSrc))
                {
                    drrepEvents["socialThumbnail"] = hostName + "/" + fbimgSrc;
                }
                else
                {
                    Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image Thumbnail"]);
                    if (!string.IsNullOrEmpty(staticimgField.Src))
                    {
                        drrepEvents["socialThumbnail"] = hostName + "/" + staticimgField.Src;
                    }
                    else
                    {
                        drrepEvents["socialThumbnail"] = "";
                    }

                }

                drrepEvents["videoURL"] = a.Fields["Video Link"].ToString();
                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image Thumbnail"]);
                string imgSrc = imgField.Src;

                Sitecore.Data.Fields.ImageField VideoimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Video Image"]);
                string VideoImgsrc = VideoimgField.Src;

                if (a.Fields["Image Thumbnail"] != null && !a["Image Thumbnail"].ToString().Equals(""))
                {
                    drrepEvents["imgSrc"] = hostName + "/" + imgSrc;
                }
                else if (a.Fields["Video Image"] != null && !a["Video Image"].ToString().Equals(""))
                {
                    drrepEvents["imgSrc"] = hostName + "/" + VideoImgsrc;
                }

                drrepEvents["Id"] = a.ID;

                // dtrepEvents.Rows.Add(drrepEvents);

            }
            catch (Exception exSub1)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }

            return drrepEvents;
        }
        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int DateMonthVale = Convert.ToInt32(ddlMonth.SelectedValue);
                int DateYearValue = Convert.ToInt32(ddlYear.SelectedValue);
                try
                {
                    DataTable dtrepEvents = new DataTable();
                    dtrepEvents.Columns.Add("cssClass", typeof(string));
                    dtrepEvents.Columns.Add("item", typeof(Item));
                    dtrepEvents.Columns.Add("href", typeof(string));
                    dtrepEvents.Columns.Add("imgSrc", typeof(string));
                    dtrepEvents.Columns.Add("Id", typeof(string));
                    dtrepEvents.Columns.Add("SocialTitle", typeof(string));
                    dtrepEvents.Columns.Add("SocialContent", typeof(string));
                    dtrepEvents.Columns.Add("socialThumbnail", typeof(string));
                    dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                    dtrepEvents.Columns.Add("videoURL", typeof(string));
                    dtrepEvents.Columns.Add("HostName", typeof(string));
                    dtrepEvents.Columns.Add("AccessToken", typeof(string));
                    dtrepEvents.Columns.Add("EventUrl", typeof(string));


                    dtrepEvents.Columns.Add("EventDate", typeof(string));
                    DataRow drrepEvents;

                    Item itmPress = SG50Class.web.GetItem(SG50Class.str_Events_Item_ID);

                    IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                              where
                                                              a.TemplateID.ToString().Equals(SG50Class.str_Event_Details_Page_Template_ID)
                                                              orderby
                                                              (a.Fields["Event Date"].Value.ToString() != "" ? Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Event Date"].Value.ToString()) : Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Start Date"].Value.ToString()))
                                                              select a;


                    foreach (Item a in itmIEnumPressReleases)
                    {

                        Sitecore.Data.Fields.DateField date = a.Fields["Event Date"];
                        System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
                        Sitecore.Data.Fields.DateField fromdate = a.Fields["Start Date"];
                        Sitecore.Data.Fields.DateField todate = a.Fields["End Date"];
                        System.DateTime fromdatetime = Sitecore.DateUtil.IsoDateToDateTime(fromdate.Value);
                        System.DateTime todatetime = Sitecore.DateUtil.IsoDateToDateTime(todate.Value);


                        if ((datetime.Date.CompareTo(DateTime.Today.Date) >= 0 && "01/01/0001 00:00:00" != datetime.ToString()) || (fromdatetime.Date.CompareTo(DateTime.Today.Date) <= 0 && todatetime.Date.CompareTo(DateTime.Today.Date) >= 0 && "01/01/0001 00:00:00" != fromdatetime.ToString()) || (fromdatetime.Date.CompareTo(DateTime.Today.Date) >= 0 && todatetime.Date.CompareTo(DateTime.Today.Date) >= 0 && "01/01/0001 00:00:00" != todatetime.ToString()))
                        {
                            if (DateMonthVale != 0 && DateYearValue != 0)
                            {
                                if ((datetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && datetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue)) || (fromdatetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && fromdatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue)) || (todatetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && todatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue)))
                                {
                                    drrepEvents = dtrepEvents.NewRow();
                                    dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                                }
                            }
                            else if (DateMonthVale > 0)
                            {


                                if ((datetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && "01/01/0001 00:00:00" != datetime.ToString()) || ((fromdatetime.Date.Month <= Convert.ToInt32(ddlMonth.SelectedValue) && "01/01/0001 00:00:00" != fromdatetime.ToString()) && (todatetime.Date.Month >= Convert.ToInt32(ddlMonth.SelectedValue) && "01/01/0001 00:00:00" != todatetime.ToString())))
                                {
                                    drrepEvents = dtrepEvents.NewRow();
                                    dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                                }
                            }
                            else if (DateYearValue != 0)
                            {
                                if ((datetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue) && "1/1/0001 12:00:00 AM" != datetime.ToString()) || (fromdatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue) && "1/1/0001 12:00:00 AM" != fromdatetime.ToString()) || (todatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue) && "1/1/0001 12:00:00 AM" != todatetime.ToString()))
                                {
                                    drrepEvents = dtrepEvents.NewRow();
                                    dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                                }
                            }
                            else
                            {
                                drrepEvents = dtrepEvents.NewRow();
                                dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                            }

                        }


                        if (dtrepEvents.Rows.Count > 0)
                        {
                            lblSearchResult.Visible = false;
                            repUpcomingEvents.Visible = true;
                            repUpcomingEvents.DataSource = dtrepEvents;
                            repUpcomingEvents.DataBind();
                            FillrepPastEvents();
                            foreach (RepeaterItem items in repUpcomingEvents.Items)
                            {
                                HtmlGenericControl noRecordsDiv = (items.FindControl("DivImagestag") as HtmlGenericControl);


                                System.Web.UI.WebControls.Image Projected = new System.Web.UI.WebControls.Image();
                                Projected = (System.Web.UI.WebControls.Image)items.FindControl("repImg");

                                if (string.IsNullOrEmpty(Projected.ImageUrl) && string.IsNullOrWhiteSpace(Projected.ImageUrl))
                                {
                                    noRecordsDiv.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            repUpcomingEvents.DataSource = null;
                            repUpcomingEvents.DataBind();
                            FillrepPastEvents();



                        }
                    }
                }
                catch (Exception exMain)
                {
                    Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
                }
            }
        }
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int DateMonthVale = Convert.ToInt32(ddlMonth.SelectedValue);
                int DateYearValue = Convert.ToInt32(ddlYear.SelectedValue);

                try
                {
                    DataTable dtrepEvents = new DataTable();
                    dtrepEvents.Columns.Add("cssClass", typeof(string));
                    dtrepEvents.Columns.Add("item", typeof(Item));
                    dtrepEvents.Columns.Add("href", typeof(string));
                    dtrepEvents.Columns.Add("imgSrc", typeof(string));
                    dtrepEvents.Columns.Add("Id", typeof(string));
                    dtrepEvents.Columns.Add("SocialTitle", typeof(string));
                    dtrepEvents.Columns.Add("SocialContent", typeof(string));
                    dtrepEvents.Columns.Add("socialThumbnail", typeof(string));
                    dtrepEvents.Columns.Add("TwitterContent", typeof(string));
                    dtrepEvents.Columns.Add("videoURL", typeof(string));
                    dtrepEvents.Columns.Add("HostName", typeof(string));
                    dtrepEvents.Columns.Add("AccessToken", typeof(string));
                    dtrepEvents.Columns.Add("EventUrl", typeof(string));
                    dtrepEvents.Columns.Add("EventDate", typeof(string));
                    DataRow drrepEvents;

                    Item itmPress = SG50Class.web.GetItem(SG50Class.str_Events_Item_ID);

                    IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                              where
                                                              a.TemplateID.ToString().Equals(SG50Class.str_Event_Details_Page_Template_ID)
                                                              orderby
                                                              (a.Fields["Event Date"].Value.ToString() != "" ? Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Event Date"].Value.ToString()) : Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Start Date"].Value.ToString()))
                                                              select a;


                    foreach (Item a in itmIEnumPressReleases)
                    {

                        Sitecore.Data.Fields.DateField date = a.Fields["Event Date"];
                        System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
                        Sitecore.Data.Fields.DateField fromdate = a.Fields["Start Date"];
                        Sitecore.Data.Fields.DateField todate = a.Fields["End Date"];
                        System.DateTime fromdatetime = Sitecore.DateUtil.IsoDateToDateTime(fromdate.Value);
                        System.DateTime todatetime = Sitecore.DateUtil.IsoDateToDateTime(todate.Value);

                        if (datetime.Date.CompareTo(DateTime.Today.Date) >= 0 || (fromdatetime.Date.CompareTo(DateTime.Today.Date) <= 0) && (todatetime.Date.CompareTo(DateTime.Today.Date) >= 0) || (fromdatetime.Date.CompareTo(DateTime.Today.Date) >= 0) && (todatetime.Date.CompareTo(DateTime.Today.Date) >= 0))
                        {
                            if (DateMonthVale != 0 && DateYearValue != 0)
                            {
                                if ((datetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && datetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue)) || (fromdatetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && fromdatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue)) || (todatetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && todatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue)))
                                {
                                    drrepEvents = dtrepEvents.NewRow();
                                    dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                                }
                            }
                            else if (DateMonthVale != 0)
                            {
                                if ((datetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && "01/01/0001 00:00:00" != datetime.ToString()) || (fromdatetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && "01/01/0001 00:00:00" != fromdatetime.ToString()) || (todatetime.Date.Month == Convert.ToInt32(ddlMonth.SelectedValue) && "01/01/0001 00:00:00" != todatetime.ToString()))
                                {
                                    drrepEvents = dtrepEvents.NewRow();
                                    dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                                }
                            }
                            else if (DateYearValue != 0)
                            {
                                if ((datetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue) && "01/01/0001 00:00:00" != datetime.ToString()) || (fromdatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue) && "01/01/0001 00:00:00" != fromdatetime.ToString()) || (todatetime.Date.Year == Convert.ToInt32(ddlYear.SelectedValue) && "01/01/0001 00:00:00" != todatetime.ToString()))
                                {
                                    drrepEvents = dtrepEvents.NewRow();
                                    dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                                }
                            }
                            else
                            {
                                drrepEvents = dtrepEvents.NewRow();
                                dtrepEvents.Rows.Add(MonthYearCommanFun(drrepEvents, a, fromdatetime, datetime, todatetime));
                            }
                        }



                        if (dtrepEvents.Rows.Count > 0)
                        {
                            lblSearchResult.Visible = false;
                            repUpcomingEvents.Visible = true;
                            repUpcomingEvents.DataSource = dtrepEvents;
                            repUpcomingEvents.DataBind();
                            FillrepPastEvents();
                            foreach (RepeaterItem items in repUpcomingEvents.Items)
                            {
                                HtmlGenericControl noRecordsDiv = (items.FindControl("DivImagestag") as HtmlGenericControl);

                                System.Web.UI.WebControls.Image Projected = new System.Web.UI.WebControls.Image();
                                Projected = (System.Web.UI.WebControls.Image)items.FindControl("repImg");

                                if (string.IsNullOrEmpty(Projected.ImageUrl) && string.IsNullOrWhiteSpace(Projected.ImageUrl))
                                {
                                    noRecordsDiv.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            repUpcomingEvents.DataSource = null;
                            repUpcomingEvents.DataBind();
                            FillrepPastEvents();



                        }
                    }
                }
                catch (Exception exMain)
                {
                    Log.Info("Events_listing_sublayoutSublayout FillrepEvents exMain : " + exMain.ToString(), this);
                }
            }
        }
    }
}
