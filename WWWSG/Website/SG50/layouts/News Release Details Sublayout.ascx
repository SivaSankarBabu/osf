﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.News_release_details_sublayout.News_release_details_sublayoutSublayout" CodeFile="~/SG50/layouts/News Release Details Sublayout.ascx.cs" %>



<style>
    .contentContainer {
        width: 1280px;
        min-height: 768px;
		background-color: #FFF;
		overflow: hidden;
		background-image:url('/SG50/images/icon1.png');
		background-repeat:no-repeat;
		background-position:right bottom;
    }

    .leftContentContainer {
        width: 1020px;
        float: left;
		min-height: 500px;
		background-image:url('/SG50/images/shadow.jpg');
		background-repeat:no-repeat;
		background-position:right top;
    }

    .rightContentContainer {
        width: 260px;
        float: left;
    }

    .rightSubContent {
        padding-left: 20px;
        padding-right: 20px;
    }

    .leftContent {
        margin-left: 115px;
        margin-right: 30px;
		margin-bottom: 30px;
    }

    .breadcrumb {
        margin-top: 25px;
        font-size: 16px;
        color: #ef2c2e;
    }
	.breadcrumb img{
		width:10px;
		height:10px;
	}

        .breadcrumb a, .breadcrumb a:hover, .breadcrumb a:visited {
            color: #353333;
        }


    .pressTitle {
        margin-top: 30px;
        font-size: 32px;
        border-bottom: solid 1px #113706;
    }

    .rightTitle {
        font-size: 18px;
        color: #ef2c2e;
        margin-top: 25px;
        margin-bottom: 20px;
    }

    .rightImage {
        margin-top: 569px;
    }

    .PortfolioPagination {
        width: 100%;
        text-align: right;
        margin-top: 5px;
    }

    .prTitle {
        font-size: 20px;
        line-height: 40px;
    }

    .prDate {
        font-size: 16px;
        line-height: 25px;
		color:#353333;
    }

    .prBlurb {
        font-size: 16px;
        line-height: 20px;
        margin-top: 15px;
        margin-bottom: 25px;
    }

    .prItem {
        margin-top:25px;
        padding-bottom:15px;
        /*border-bottom:dotted 1px #353333;*/
	border-bottom: solid 1px #ccc;
    }
    .prItem a, .prItem a:hover, .prItem a:visited {
        color: black;
    }

    .prLatest {
        padding-top: 15px;
        color: #353333;
    }

        .prLatest a, .prLatest a:hover, .prLatest a:visited {
           color: #353333;
			font-size:14px;
			line-height:1.2em;
        }

    .divider {
        height: 15px;
        border-bottom: 1px solid #f0f0f0;
        margin-left: 0px;
        margin-right: 0px;
    }

    @media screen and (min-width: 300px) and (max-width: 768px) {
        .contentContainer {
            width: 100%;
        }

        .leftContentContainer {
            width: 100%;
        }

        .rightContentContainer {
            display: none;
        }

        .leftContent {
            margin-top: 30px;
            margin-left: 10px;
        }
    }
</style>

<style>
    .pressDetailsTitle
    {
        margin-top: 30px;
        margin-bottom: 25px;
        font-size: 36px;
		color:#353333;
    }

    .pressDetailsBanner
    {
        height: 400px;
    }

    .pressDetailsDate
    {
        margin-bottom: 25px;
		font-size:14px;
		color:#353333;
    }

    .pressDetailsContent, .pressDetailsContent p{
		font-size:16px;
		color:#565656;
		line-height:25px;
	}

    @media screen and (min-width: 300px) and (max-width: 768px)
    {
        .press
        {
            width: 100%;
            height: auto;
        }

        .pressDetailsBanner
        {
            width: 100%;
            height: auto;
        }

            .pressDetailsBanner img
            {
                width: 100%;
                height: auto;
            }
    }
</style>
<div class="press">
    <div class="pressDetailsBanner">
        <div id="pressBannerInner">
            <div id="pressBannerImage">
                <sc:Image Field="Banner Image" runat="server" />
            </div>
        </div>
    </div>
</div>
<div class="contentContainer">
    <div class="leftContentContainer">
        <div class="leftContent">
            <sc:Sublayout ID="Sublayout1" runat="server" Path="/SG50/layouts/Breadcrumb Sublayout.ascx"></sc:Sublayout>
            <div class="pressDetailsTitle">
                <sc:Text ID="Text1" Field="Content Title" runat="server" />
            </div>
            <div class="pressDetailsDate">
                <sc:Date Field="Release Date" runat="server" Format="dd MMMM yyyy" />
            </div>
            <div class="pressDetailsContent">
                <sc:Text ID="Text2" Field="Content" runat="server" />
            </div>
        </div>
    </div>
    <div class="rightContentContainer">
        <div class="rightContent">
            <div class="rightSubContent">
                <div class="rightTitle">Latest Releases</div>
                <asp:Repeater ID="repLatest" runat="server">
                    <ItemTemplate>
                        <div class="prLatest">
                            <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>">
                                <sc:Text ID="Text2" Field="Content Title" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </a>
                        </div>
                        <div class="divider">&nbsp;</div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="rightImage">
                <img src="/SG50/images/icon1.png" /></div>
        </div>
    </div>

</div>
