﻿using System;
using Sitecore.Data.Items;
using Sitecore.Data;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Links;

namespace Layouts.Timecapsulenew
{

    /// <summary>
    /// Summary description for TimeCapsuleNewSublayout
    /// </summary>
    /// 
    public partial class TimecapsulenewSublayout : System.Web.UI.UserControl
    {
        private static Sitecore.Data.Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmContext = Sitecore.Context.Item;
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        public string timeCapsuleItemsFolderId = "{E3387D22-9A6E-4FBF-A018-28F7EB755672}", strNew = "New", strAll = "All", hostName = itemconfiguration["Host Name"],
            suggestedItemsFolderId = "{71F21422-2FF0-456B-9AB6-58877C42AEA2}", timeCapsuleItemTemplateId = "{06816772-B378-4433-8D72-E06DD1C398C2}", suggestNewItemTemplateId = "{1A0C6620-7F02-4E01-83FA-DB02D4362D4B}";

        #region Page Load Event

        /* Page-Load Event */
        private void Page_Load(object sender, EventArgs e)
        {
            GetTimeCapsuleItems(strAll, 7);
            GetCountNewCategoryItems();
            SocialSharing();
        }

        #endregion

        #region User Defined functions in Time Capsule

        private void SocialSharing()
        {
            DataTable dtSocialSharing = new DataTable();
            dtSocialSharing.Columns.Add("Id", typeof(string));
            dtSocialSharing.Columns.Add("SocialShareTitle", typeof(string));
            dtSocialSharing.Columns.Add("SocialShareContent", typeof(string));
            dtSocialSharing.Columns.Add("ItemName", typeof(string));
            dtSocialSharing.Columns.Add("href", typeof(string));
            dtSocialSharing.Columns.Add("HostName", typeof(string));
            dtSocialSharing.Columns.Add("AccessToken", typeof(string));
            dtSocialSharing.Columns.Add("EventUrl", typeof(string));
            dtSocialSharing.Columns.Add("SocialSharingThumbNail", typeof(string));
            dtSocialSharing.Columns.Add("TwitterSharingDescription", typeof(string));

            DataRow drSocialSharing;
            //  Item itmSocialSharing = SG50Class.web.GetItem("{5E04DC6C-B5E2-4313-B0D0-46B7803085D7}");
            drSocialSharing = dtSocialSharing.NewRow();
            drSocialSharing["HostName"] = hostName + "/";
            drSocialSharing["AccessToken"] = accessToken;
            drSocialSharing["href"] = LinkManager.GetItemUrl(itmContext).ToString();
            drSocialSharing["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(itmContext).ToString();
            if (!string.IsNullOrEmpty(itmContext.Fields["Social Sharing Title"].ToString()))
            {
                drSocialSharing["SocialShareTitle"] = itmContext.Fields["Social Sharing Title"].ToString();
            }
            if (!string.IsNullOrEmpty(itmContext.Fields["Social Sharing Description"].ToString()))
            {
                drSocialSharing["SocialShareContent"] = itmContext.Fields["Social Sharing Description"].ToString();
            }
            Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Social Shring Image"]);
            string fbimgSrc = fbimgField.Src;
            if (!fbimgSrc.Equals(""))
            {
                drSocialSharing["SocialSharingThumbNail"] = hostName + "/" + fbimgSrc;
            }
            else
            {
                drSocialSharing["SocialSharingThumbNail"] = "";
            }


            if (!string.IsNullOrEmpty(itmContext.Fields["Twitter Sharing Description"].ToString()))
                drSocialSharing["TwitterSharingDescription"] = itmContext.Fields["Twitter Sharing Description"].ToString();
            else
                drSocialSharing["TwitterSharingDescription"] = string.Empty;

            dtSocialSharing.Rows.Add(drSocialSharing);
            repsocialSharingTop.DataSource = dtSocialSharing;
            repsocialSharingTop.DataBind();
        }

        /* Determine weather is there any items related to "New" Category */
        void GetCountNewCategoryItems()
        {
            try
            {

                //return web.GetItem(timeCapsuleItemsFolderId).GetChildren().Where(x => x.Fields["Category"].Value == strNew && x.Fields["Is_Active"].Value == 1.ToString()).Count();
                int newItemCountActive = web.GetItem(suggestedItemsFolderId).GetChildren().Where(x => x.Fields["Is_Active"].Value == 1.ToString()).Count();
                listIDNew.Visible = newItemCountActive > 0 ? true : false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /* Get Time Capsule Items in Page_Load Event */
        private void GetTimeCapsuleItems(string categoryType, int skipItemsCount)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryType))
                {
                    // Building data table with adding columns 
                    DataTable dtTimeCapsule = new DataTable();
                    dtTimeCapsule.Columns.Add("ImgSrc", typeof(string));
                    dtTimeCapsule.Columns.Add("ItemTitle", typeof(string));
                    dtTimeCapsule.Columns.Add("ItemDescription", typeof(string));
                    dtTimeCapsule.Columns.Add("Itemvotes", typeof(string));
                    dtTimeCapsule.Columns.Add("Category", typeof(string));
                    dtTimeCapsule.Columns.Add("ButtonText", typeof(string));
                    dtTimeCapsule.Columns.Add("ID", typeof(string));
                    dtTimeCapsule.Columns.Add("Name", typeof(string));

                    DataRow drTimeCapsule;
                    IEnumerable<Item> timeCapsuleItemsFolder = itmContext.Axes.GetDescendants().Where(x => (x.TemplateID.ToString() == suggestNewItemTemplateId && x.Fields["Is_Active"].Value == 1.ToString()) || x.TemplateID.ToString() == timeCapsuleItemTemplateId).ToList();

                    IEnumerable<Item> itemsInTimeCapsuleItemsFolder = null;

                    itemsInTimeCapsuleItemsFolder = timeCapsuleItemsFolder.Select(x => x).Take(skipItemsCount).OrderBy(x => x.Name);
                    spanTotalNuberOfItems.InnerText = timeCapsuleItemsFolder.Select(x => x).Count().ToString();

                    if (itemsInTimeCapsuleItemsFolder.Count() > 0 && itemsInTimeCapsuleItemsFolder != null)
                    {
                        foreach (Item a in itemsInTimeCapsuleItemsFolder)
                        {
                            drTimeCapsule = dtTimeCapsule.NewRow();
                            drTimeCapsule["ItemTitle"] = a.Fields["Title"].ToString();
                            drTimeCapsule["ItemDescription"] = a.Fields["Description"].ToString();
                            drTimeCapsule["Itemvotes"] = a.Fields["votes"].ToString();
                            drTimeCapsule["ButtonText"] = a.Fields["Button Text"].ToString();
                            drTimeCapsule["ID"] = a.ID;
                            drTimeCapsule["Name"] = a.Name.Trim();

                            Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);

                            if (mainImgField != null)
                            {
                                string mainimgSrc = mainImgField.Src;
                                if (!string.IsNullOrEmpty(mainimgSrc))
                                    drTimeCapsule["imgSrc"] = mainimgSrc;
                            }
                            dtTimeCapsule.Rows.Add(drTimeCapsule);
                        }
                    }
                    repeaterTimeCapusuleItems.DataSource = dtTimeCapsule;
                    repeaterTimeCapusuleItems.DataBind();
                }
            }
            catch (Exception)
            {
                throw;
                //Response.Write(exSub1.Message + exSub1.StackTrace + " " + exSub1.InnerException);
            }
        }

        #endregion
    }
}