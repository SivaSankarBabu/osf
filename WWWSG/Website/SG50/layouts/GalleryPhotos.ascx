﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Galleryphotos.GalleryphotosSublayout" CodeFile="~/SG50/layouts/GalleryPhotos.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript" language="javascript"></script>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/jquery.bxslider.min.js"></script>
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<div class="masthead">
    
    <!-- Photos Header -->
    <div class="photo-header">
    	<div class="photo-row">
        	<div class="cell-12">
            	<div class="group-text-button">
                	<a href="/SG50/GalleryLanding/GalleryPhotoList.aspx" class="title-button"><sc:Text ID="txtButtonText" runat="server" Field="Button Text" /></a>
            		<h1><sc:Text ID="txtTitle" runat="server" Field="Title" /></h1>
                </div>
                <p class="mrg-b40"><sc:Text ID="txtDescription" runat="server" Field="Description" /></p>
            </div>
        </div>
    </div>
    <!-- Photos Header -->
   <div class="photo-gallery RS-gallery">
        <div class="photo-row">
        	<div class="SG50Loader"><img src="/SG50/images/Gallery/SG50Loader.gif"/></div>
        	<div class="popup-gallery">
            <ul id="FaceBookPhotosList"></ul> 
            </div>           
        </div>
        <!--<div class="cell-12">
                <div class="pagination">
                    <ul>
                        <li><a href="#" class="pre"></a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li><a href="#">8</a></li>
                        <li><a href="#">9</a></li>
                        <li><a href="#" class="next"></a></li>
                    </ul>
                </div>
       </div> -->
   </div> 
</div>
<script type="text/javascript">var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>
<!-- Add fancyBox -->
<script type="text/javascript" src="/SG50/include/js/jquery.magnific-popup.js"></script>
<script src="/SG50/include/js/b6NoKG6GEMQqshcL22y.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    });
</script>
<script>
    $(function() {
        var fcontent = '';
        var album_id = localStorage.getItem('facebook_album');

        $.getJSON('https://graph.facebook.com/' + album_id + '/photos?limit=500&access_token=' + x2HOnsd, function(data) {
            var result = $.map(data, function(value, key) {
                if (key != 'paging') {
                    for (var i in value) {
                        fcontent += '<li>' +
                            '<a href="' + value[i]['images'][2]['source'] + '">' +
                            '<div class="ImageContainer"><img src="' + value[i]['images'][0]['source'] + '"/></div>' +
                            '</a>' +
                       '</li>';
                    }
                }
            });
            // setting title  
            $(".group-text-button h1").html(localStorage.getItem('title'));
            // setting descrption  
            $(".mrg-b40").html(unescape(localStorage.getItem('description')));
            $("#FaceBookPhotosList").html(fcontent);
            $(".SG50Loader").hide();
        });
    });      
</script>


