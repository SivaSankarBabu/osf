﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Userstorydetailssublayout.UserstorydetailssublayoutSublayout" CodeFile="~/SG50/layouts/UserStoryDetailsSublayout.ascx.cs" %>


<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript" language="javascript"></script>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<%--<link href="/SG50/include/css/thematic-campaign-responsive.css" rel="stylesheet" type="text/css">--%>

<script>
 $(function () {
        $('.menu_nav').find('.ABOUT a:first').addClass('selected');
    });
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...
       
        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            // caption: fbdes,
            description: fbdes
        };
        FB.ui(obj);

    }

    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function (response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }

</script>
<div class="container custom-res" id="homePage">
    <div class="masthead">
        <div class="banner-01 border-Bnone">

            <sc:Image ID="imgBanner" runat="server" Field="Banner Image" />
        </div>
        <!-- Stories -->
        <div class="camp-story">
            <div class="camp-row">
                <div class="cell-12">
                    <div class="story-header">
                        <asp:LinkButton ID="btnPrevious" runat="server" Text="Previous Story" CssClass="prev-story" OnClick="btnPrevious_Click"></asp:LinkButton>
                        <span class="text">
                            <sc:Text ID="txtStoryTitle" runat="server" Field="Title" />
                        </span>
                        <asp:LinkButton ID="btnNext" runat="server" Text="Next Story" CssClass="next-story" OnClick="btnNext_Click"></asp:LinkButton>
                    </div>
                </div>
                <div class="story-info">
                    <sc:Text ID="txtDescription" Field="Descrption" runat="server" />
                </div>
                <div class="cell-12">
                    <div class="story-footer">
                        <div class="cell-6 padding-Lnone">
                            <p>
                                Story & images submitted by <strong>
                                    <sc:Text ID="txtSubmitted" runat="server" Field="Submitted By" />
                                </strong>
                            </p>
                        </div>
                        <div class="cell-6 padding-Rnone">
                            <div class="share text-right">
                                <span class="text">Share this story
                               
                                    <asp:Repeater ID="repsocialSharing" runat="server">
                                        <ItemTemplate>
                                            <div class="social-icons">
                                                <a class="social facebook" onclick="ga('send', 'event', 'UnOfficial Stories', 'Click', 'UnOfficial Story - Post - Facebook Share');" href="JavaScript:postToFeed('<%# Eval("FaceBookTitle").ToString()%>','<%# Eval("FaceBookContent").ToString()%>','<%# Eval("FaceBookThumbNail")%>','<%# Eval("EventUrl")%>')"></a>
                                                <a class="social twitter" onclick="ga('send', 'event', 'UnOfficial Stories', 'Click', 'UnOfficial Story - Post - Twitter Share');" href="JavaScript:newPopup('<%# Eval("TwitterContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')"></a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                   
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Stories -->
        <div class="camp-row">
            <div class="cell-12">
                <div class="content-header">
                    <h1>
                        <sc:Text ID="txtHeaderTxt" Field="Other StoriesTitle" runat="server" DataSource="{2E56CD94-2A02-4D4E-B53A-8B308B29F375}" />
                    </h1>
                    <a href="#" class="back-to-top hide-for-small">Back to top</a>
                </div>
            </div>
        </div>


        <ul class="banner-03 custom" id="DvCampaignBanners" runat="server">
        </ul>
    </div>
</div>
<script type="text/javascript">var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>
<script>
    $(function () {
        if ($(window).width() <= 1023) {
            $(".main-banner01").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });

            $(".banner-03 li img").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });
        }
    });
</script>

