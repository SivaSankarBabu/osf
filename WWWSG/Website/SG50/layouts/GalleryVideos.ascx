﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Galleryvideos.GalleryvideosSublayout" CodeFile="~/SG50/layouts/GalleryVideos.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />

<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>


<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">

<!--//new css-->
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/b6NoKG6GEMQqshcL22y.js" type="text/javascript"></script>

<script>
    $(function () {
        var content = '';
        var youtube_video_play_list_id = localStorage.getItem('youtube_video_play_list_id');
        var title = localStorage.getItem('vtitle');
        var pl_description = localStorage.getItem('vdescription');
        $.getJSON("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&channelId=UCqQZD3jRc3jkaVwRWvBoSmw&playlistId=" + youtube_video_play_list_id + "&key=" + api_key + "&maxResults=50&alt=json", function (data) {
            // + converts the string to int
            var info = data.items;
            var result = $.map(info, function (value, key) {

                content += '<li>' +
                      '<div class="photo-cont">' +
                          '<a class="youtube" href="https://www.youtube.com/embed/' + value.snippet.resourceId.videoId + '"><img src="' + value.snippet.thumbnails.medium['url'] + '"/></a>' +
                         '<h1>' + value.snippet.title + '</h1>' +
                // '<span>' + new Date(value['published']['$t']).toDateString() + '</span>' +
                // '<p>' + des + '</p>' +
                      '</div>' +
                  '</li>';

            });
            $("#YouTubePlayList").html(content);
            if ($(window).width() > 767) {
                $(".youtube").colorbox({ iframe: true, innerWidth: 640, innerHeight: 390 });
            }
            $(".SG50Loader").hide();
            $(".group-text-button h1").html(title);
            $(".mrg-b39").html(unescape(pl_description));
        });

    });
</script>

<div class="container custom-res" id="homePage">
    <div class="masthead">

        <!-- Photos Header -->
        <div class="photo-header">
            <div class="photo-row">
                <div class="cell-12">
                    <div class="group-text-button">
                        <a href="/SG50/GalleryLanding/GalleryVideoList.aspx" class="title-button"><sc:Text ID="txtButtonText" runat="server" Field="Back Button Text" /></a>
                        <h1><sc:Text ID="txtTitle" runat="server" Field="Title" /></h1>
                    </div>
                    <p class="mrg-b39"></p>
                    <br />
                    <%--<p class="mrg-b40"><sc:Text ID="txtDescription" runat="server" Field="Description" /></p>--%>
                </div>

            </div>
        </div>
        <!-- Photos Header -->
        <div class="photo-gallery gallery-VLRow">
            <div class="photo-row">
                <div class="popup-gallery videos popup-youtube">
                    <div class="SG50Loader">
                        <img src="/SG50/images/Gallery/SG50Loader.gif" />
                    </div>
                    <ul id="YouTubePlayList"></ul>
                </div>
            </div>
            <!-- <div class="cell-12">
            <div class="pagination">
                <ul>
                    <li><a href="#" class="pre"  data-id=''></a></li>
                    <li><a href="#" class="next" data-id=''></a></li>
                </ul>
            </div>
        </div> -->
        </div>
    </div>

</div>
<script src="/SG50/include/js/jquery.colorbox.js" type="text/javascript"></script>
<script type="text/javascript">var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>
