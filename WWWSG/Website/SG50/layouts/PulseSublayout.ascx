﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Pulsesublayout.PulsesublayoutSublayout" CodeFile="~/SG50/layouts/PulseSublayout.ascx.cs" %>

<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">
<script src="/SG50/include/js/jquery.bxslider.min.js"></script>
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    function RequiredSerachKeyWord(btnId, ctrlId, msg) {
        if ($(btnId).length > 0 && $(ctrlId).length > 0 && msg != '') {
            //alert('hi');
            $(btnId).click(function () {
                if ($(ctrlId).val() != '') {
                    return true;
                } else { alert(msg); $(ctrlId).focus(); return false; }
            });
        }
    }
    $(function () {

        RequiredSerachKeyWord('#content_0_imgSearchButton', '#content_0_txtSerachPulse', 'Please enter keywords to search.');
    })
</script>
<div class="container custom-res" id="homePage">
    <div class="masthead">
        <div class="photo-header">
            <div class="photo-row">
                <div class="cell-12 pulse">
                    <div class="group-text-button">
                        <h1>
                            <sc:Text ID="txtPluseTitle" Field="Title" runat="server" />
                        </h1>
                    </div>
                    <p>
                        <sc:Text ID="txtDescription" Field="Description" runat="server" />
                    </p>
                    <div class="form-group">
                        <asp:TextBox ID="txtSerachPulse" runat="server" placeholder="SEARCH" />
                        <asp:ImageButton ID="imgSearchButton" CssClass="input-button" runat="server" OnClick="imgSearchButton_Click" ImageUrl="~/SG50/images/search1.png" Style="float: left" />

                    </div>
                </div>
            </div>
        </div>
        <asp:Label ID="lblResult" ForeColor="Red" runat="server"></asp:Label>
        <div class="pulse-gallery">
            <div class="pulse-full-box">
                <asp:Repeater ID="repNewCollectibles" runat="server">
                    <ItemTemplate>
                        <li>
                            <a href='<%# DataBinder.Eval(Container.DataItem, "href") %>'>
                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                            </a>
                            <h1>
                                <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label></h1>
                            <p>
                                <%--<asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Label>--%>
                                <p>
                                    <%#Eval("Sub Title")%>
                                </p>
                            </p>
                            <input type="hidden" value='<%#Eval("width")%>' />
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="cell-12">
            <div class="pagination">
                <ul>
                    <li>
                        <asp:LinkButton ID="PrevControl" runat="server" CssClass="pre" OnClick="PrevControl_Click"></asp:LinkButton>
                    </li>
                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                        <ItemTemplate>
                            <li>
                                <asp:LinkButton ID="btnPage" Enabled='<%# DataBinder.Eval(Container.DataItem, "Enable") %>'
                                    Style='<%# DataBinder.Eval(Container.DataItem, "style") %>'
                                    CommandName="Page" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>'
                                    runat="server" ForeColor="White" Font-Bold="True"><%# DataBinder.Eval(Container.DataItem, "ID") %>
                                </asp:LinkButton>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <li>
                        <asp:LinkButton ID="NextControl" runat="server" CssClass="next" OnClick="NextControl_Click"></asp:LinkButton>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<%--<script src="/SG50/include/js/CustomScript.js"></script>--%>
<script type="text/javascript">
    var imageWidths = [], noOfRows = '', totalIamgeWidth = 0, ctrlNotFound = 'Control not found.', imageWidthRule = 'Image width should be 640 or 320.';
    $('.pulse-gallery > .pulse-full-box > li').hide();

    $(function () {
        // To Fill array with all images widths
        function GetImagesWidths(ctrlId) {
            if ($(ctrlId).length > 0) {
                for (var index = 0; index < $(ctrlId).find('li').find('input[type=hidden]').length; index++) {
                    var width = parseInt($(ctrlId).find('li:eq(' + index + ')').find('input[type=hidden]').val());
                    if (width == 0) { index--; } else { imageWidths.push(width); }
                } //alert(imageWidths);
            } else { return false; }
        }

        GetImagesWidths('.pulse-gallery');

        var divIndex = 0, skippedIndexs = [], indexes320 = [];

        // For Binding Events in Pulse Landing page
        function BindImages(ctrl, rowWidth) {
            if ($(ctrl).length > 0) {
                var str320 = '', missing320 = '';
                for (var index = 0; index < imageWidths.length; index++) {
                    var ctr = $(ctrl).find('.pulse-full-box:eq(' + divIndex + ')'), liCtrl = ctr.find('li:eq(' + index + ')').find('a'), url = liCtrl.attr('href').replace(/ /gi, '%20');
                    if (imageWidths[index] == 640 && rowWidth != 320) {
                        rowWidth -= imageWidths[index];
                        ctr.append('<a href=' + url + '><div class="pulse-box01"><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + index + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></div></a>');
                        indexes320 = [];
                    }
                    else if (imageWidths[index] == 320) {
                        indexes320.push(index);
                        if (indexes320.length == 1) {
                            if (imageWidths[index + 1] == 640) {
                                ctr.append('<a href=' + url + '><div class="pulse-box03"><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + index + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></div></a>');
                                indexes320 = []; rowWidth -= imageWidths[index];
                            }
                            else if ((imageWidths[index + 1] == 320 || index == imageWidths.length - 1) && rowWidth == 320 && /*ctr.find('li:eq(' + parseInt(index - 1) + ')').find('a').find('img').width()*/ imageWidths[index - 1] == 640) {
                                ctr.append('<a href=' + url + '><div class="pulse-box03"><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + index + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></div></a>');
                                indexes320 = []; rowWidth -= imageWidths[index];
                            }
                            else { str320 += '<a href=' + url + '><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + index + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></a>'; rowWidth -= imageWidths[index]; }
                        }
                        else if (imageWidths[index + 1] == 320 && rowWidth == 0 && indexes320.length == 0) {
                            ctr.append('<a href=' + url + '><div class="pulse-box03"><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + index + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></div></a>');
                            indexes320 = []; rowWidth -= imageWidths[index];
                        }
                        else { str320 += '<a href=' + url + '><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + index + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></a>'; rowWidth -= imageWidths[index]; }

                        if (indexes320.length >= 2) {
                            if (indexes320[1] - indexes320[0] == 1) { ctr.append('<div class="pulse-box02">' + str320 + '</div>'); indexes320 = []; str320 = ''; }
                        }

                        if (index == imageWidths.length - 1 && str320 != '') {
                            ctr.append('<a href=' + url + '><div class="pulse-box03"><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + index + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></div></a>'); rowWidth -= imageWidths[index];
                        }
                    } else { } //alert(imageWidthRule); return false; }

                    if (rowWidth == 320 && imageWidths[index + 1] == 640) {
                        if ($.inArray(320, imageWidths, index + 1) >= 0) {
                            var newIndex = $.inArray(320, imageWidths, index + 1);
                            liCtrl = ctr.find('li:eq(' + newIndex + ')').find('a'), url = liCtrl.attr('href').replace(/ /gi, '%20');
                            ctr.append('<a href=' + url + '><div class="pulse-box03"><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + newIndex + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></div></a>');
                            imageWidths.splice(newIndex, 1); ctr.find('li:eq(' + newIndex + ')').remove(); rowWidth -= imageWidths[newIndex]; newIndex = '';
                        }
                        else {
                            if (imageWidths[index] == 320) { BuildLast320Html(index); } else if (imageWidths[index] == 640) { BuildLast320Html(index - 1); }

                            function BuildLast320Html(missIndex) {
                                var prevCtrl = $(ctrl).find('div.pulse-box03:last').parent(), src = prevCtrl.attr('href');
                                liCtrl = ctr.find('li:eq(' + missIndex + ')').find('a'), url = liCtrl.attr('href').replace(/ /gi, '%20');
                                missing320 = '<a href=' + url + '><div class="pulse-box03"><div class="pulse-box"><div class="pulse-info"><h1>' + ctr.find('li:eq(' + missIndex + ')').find('h1').html() + '</h1><p>' + ctr.find('li:eq(' + index + ')').find('p:eq(1)').html() + '</p></div>' + liCtrl.html() + '</div></div></a>';
                                prevCtrl.remove(); imageWidths.splice(missIndex, 1); ctr.find('li:eq(' + missIndex + ')').remove(); index--; rowWidth -= imageWidths[missIndex];
                            }
                        }
                    } rowWidth = rowWidth == 0 ? 1280 : rowWidth;
                } ctr.append(missing320);
            } else { alert(ctrlNotFound); return false; }
        }

        BindImages('.pulse-gallery', 1280);

        // Hide paging when page count less than or equal to 1
        // Need o pass paging control id 
        function HideStartEndPaging(divId) {
            if ($(divId).length > 0) {
                if ($(divId).not('li:first,li:last').length > 0) {
                    $(divId).find('li:first,li:last').show();
                } else $(divId).not('li:first,li:last').hide();
            }
        }

        HideStartEndPaging('.pagination');
    });
</script>
