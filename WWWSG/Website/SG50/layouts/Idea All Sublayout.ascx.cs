﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace Layouts.Idea_all_sublayout {
  
	/// <summary>
	/// Summary description for Idea_all_sublayoutSublayout
	/// </summary>
  public partial class Idea_all_sublayoutSublayout : System.Web.UI.UserControl 
	{
        protected int CurrentPage
        {
            get
            {
                // look for current page in ViewState
                object o = this.ViewState["_CurrentPage"];
                if (o == null)
                    return 0;   // default to showing the first page
                else
                    return (int)o;
            }

            set
            {
                this.ViewState["_CurrentPage"] = value;
            }
        }

        protected int CurrentPageMob
        {
            get
            {
                // look for current page in ViewState
                object o = this.ViewState["_CurrentPageMob"];
                if (o == null)
                    return 0;   // default to showing the first page
                else
                    return (int)o;
            }

            set
            {
                this.ViewState["_CurrentPageMob"] = value;
            }
        }

        protected int PageCount
        {
            get
            {
                // look for current page count in ViewState
                object o = this.ViewState["_PageCount"];
                if (o == null)
                    return 1;   // default to just 1 page
                else
                    return (int)o;
            }

            set
            {
                this.ViewState["_PageCount"] = value;
            }
        }

        protected int PageCountMob
        {
            get
            {
                // look for current page count in ViewState
                object o = this.ViewState["_PageCountMob"];
                if (o == null)
                    return 1;   // default to just 1 page
                else
                    return (int)o;
            }

            set
            {
                this.ViewState["_PageCountMob"] = value;
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here        
            if (!IsPostBack)
            {
                FillrepIdeas("");
                noResult.Visible = false;
                noResultMob.Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (isValidated())
            {
                FillrepIdeas(SG50Class.StripUnwantedString(txtSearch.Text.ToString().Trim()));
            }
        }

        protected void btnSearchMob_Click(object sender, EventArgs e)
        {
            if (isValidatedMob())
            {
                FillrepIdeas(SG50Class.StripUnwantedString(txtSearchMob.Text.ToString().Trim()));
            }
        }

        private bool isValidated()
        {
            bool isValid = true;

            if (txtSearch.Text.ToString().Trim().Equals(""))
            {
                rfvSearch.IsValid = false;
                isValid = false;
            }
            else
            {
                // check regular expression
                string text = txtSearch.Text.ToString();
                Match match1 = Regex.Match(text, regexSearch.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexSearch.IsValid = false;
                }
            }

            return isValid;
        }

        private bool isValidatedMob()
        {
            bool isValid = true;

            if (txtSearchMob.Text.ToString().Trim().Equals(""))
            {
                rfvSearchMob.IsValid = false;
                isValid = false;
            }
            else
            {
                // check regular expression
                string text = txtSearchMob.Text.ToString();
                Match match1 = Regex.Match(text, regexSearchMob.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexSearchMob.IsValid = false;
                }
            }

            return isValid;
        }

        /*private void FillrepIdeas(string searchString)
        {
            try
            {
                DataTable dtrepIdeas = new DataTable();
                dtrepIdeas.Columns.Add("cssClass", typeof(string));
                dtrepIdeas.Columns.Add("ideaText", typeof(string));
                dtrepIdeas.Columns.Add("Name", typeof(string));
                dtrepIdeas.Columns.Add("Age", typeof(string));
                dtrepIdeas.Columns.Add("href", typeof(string));
                dtrepIdeas.Columns.Add("Region", typeof(string));

                DataRow drrepIdeas;

                Item itmIdeaRepository = SG50Class.web.GetItem(SG50Class.str_Idea_Repository_Item_ID);

                IEnumerable<Item> itmIEnumWestIdea = Enumerable.Empty<Item>();
                if (searchString.Equals("")) //onload
                {
                    /*itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));*/

                /*    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));

                }
                else
                {
                    //search
                    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        && a["Name"].ToLower().Contains(searchString.ToLower()) || a["idea"].ToLower().Contains(searchString.ToLower())
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));

                    if (itmIEnumWestIdea.Count() == 0)
                    {
                        //no results from search, show the most recent results
                        itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                            where
                                            a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                            select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));
                        noResult.Visible = true;
                        noResultMob.Visible = true;
                    }
                    else
                    {
                        noResult.Visible = false;
                        noResultMob.Visible = false;
                    }
                }

                foreach (Item a in itmIEnumWestIdea) 
                {
                    try
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = LinkManager.GetItemUrl(a);
                        drrepIdeas["cssClass"] = "";
                        if (a["Idea"].ToString().Count() > 31)
                        {
                            drrepIdeas["ideaText"] = a["Idea"].ToString().Remove(30) + "...";
                        }
                        else
                        {
                            drrepIdeas["ideaText"] = a["Idea"];
                        }
                        drrepIdeas["Name"] = a["Name"];
                        drrepIdeas["Age"] = a["Age"];
                        drrepIdeas["Region"] = a["Region"];

                        dtrepIdeas.Rows.Add(drrepIdeas);
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Idea_main_sublayoutSublayout FillrepIdeas a exSub1 : " + exSub1.ToString(), this);
                    }
                }

             
                //GenerateResults(dtrepIdeas);
                repIdeas.DataSource = drrepIdeas;
                repIdeas.DataBind();

               // repIdeasMobile.DataSource = dtrepIdeas;
               // repIdeasMobile.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("Idea_main_sublayoutSublayout FillrepIdeas exMain : " + exMain.ToString(), this);
            }
        }
      */

        private void FillrepIdeas(string searchString)
        {
            try
            {
                DataTable dtrepIdeas = new DataTable();
                dtrepIdeas.Columns.Add("cssClass", typeof(string));
                dtrepIdeas.Columns.Add("ideaText", typeof(string));
                dtrepIdeas.Columns.Add("Name", typeof(string));
                dtrepIdeas.Columns.Add("Age", typeof(string));
                dtrepIdeas.Columns.Add("href", typeof(string));
                dtrepIdeas.Columns.Add("Region", typeof(string));

                DataRow drrepIdeas;

                Item itmIdeaRepository = SG50Class.web.GetItem(SG50Class.str_Idea_Repository_Item_ID);

                IEnumerable<Item> itmIEnumWestIdea = Enumerable.Empty<Item>();
                if (searchString.Equals("")) //onload
                {
                    /*itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));*/

                    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));

                }
                else
                {
                    //search
                    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        && a["Name"].ToLower().Contains(searchString.ToLower()) || a["idea"].ToLower().Contains(searchString.ToLower())
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));

                    if (itmIEnumWestIdea.Count() == 0)
                    {
                        //no results from search, show the most recent results
                        itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                            where
                                            a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                            select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString()));
                        noResult.Visible = true;
                        noResultMob.Visible = true;
                    }
                    else
                    {
                        noResult.Visible = false;
                        noResultMob.Visible = false;
                    }
                }

                IList<idea> ideas = new List<idea>();

                foreach (Item a in itmIEnumWestIdea)
                {
                    try
                    {
                        idea _idea = new idea();
                        _idea.cssClass = "";
                        _idea.href = LinkManager.GetItemUrl(a);
                        _idea.ideaText = a["Idea"].ToString().Count() > 31 ? a["Idea"].ToString().Remove(30) + "..." : a["Idea"];
                        _idea.Name = a["Name"];
                        _idea.Age = a["Age"];
                        _idea.Region = a["Region"];

                        ideas.Add(_idea);
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Idea_main_sublayoutSublayout FillrepIdeas a exSub1 : " + exSub1.ToString(), this);
                    }
                }

                //For desktop
                PagedDataSource pagedData = new PagedDataSource();

                pagedData.AllowPaging = true;
                pagedData.PageSize = 21;
                pagedData.DataSource = ideas;
                pagedData.CurrentPageIndex = CurrentPage;

                //For mobile 
                PagedDataSource pagedDataMob = new PagedDataSource();

                pagedDataMob.AllowPaging = true;
                pagedDataMob.PageSize = 10;
                pagedDataMob.DataSource = ideas;
                pagedDataMob.CurrentPageIndex = CurrentPageMob;

                cmdPrev.Enabled = !pagedData.IsFirstPage;
                cmdNext.Enabled = !pagedData.IsLastPage;

                //Set the hidden fields
                hfTotalideas.Value = pagedData.DataSourceCount != null ? pagedData.DataSourceCount.ToString() : "0";
                hfCurrentPage.Value = (CurrentPage+1).ToString();
                hfCurrentPageMob.Value = (CurrentPageMob + 1).ToString();

                // Wire up the page numbers desktop
                if (pagedData.PageCount > 1)
                {
                    rptPages.Visible = true;
                    cmdPrev.Visible = true;
                    cmdNext.Visible = true;

                    ArrayList pages = new ArrayList();
                    for (int i = 0; i < pagedData.PageCount; i++)
                        if (i == CurrentPage)
                        {

                            pages.Add("<span style=' color:red'>" + (i + 1).ToString() + "</span>");
                        }
                        else
                        {
                            pages.Add((i + 1).ToString());
                        }

                    rptPages.DataSource = pages;
                    rptPages.DataBind();
                }
                else
                {
                    rptPages.Visible = false;
                    cmdPrev.Visible = false;
                    cmdNext.Visible = false;
                }

                // Wire up the page numbers Mobile
                if (pagedDataMob.PageCount > 1)
                {
                    ArrayList pages = new ArrayList();
                    for (int i = 0; i < pagedDataMob.PageCount; i++)
                        if (i == CurrentPageMob)
                        {

                            pages.Add("<span style=' color:red'>" + (i + 1).ToString() + "</span>");
                        }
                        else
                        {
                            pages.Add((i + 1).ToString());
                        }
                    rptPagesMob.DataSource = pages;
                    rptPagesMob.DataBind();
                }

                //For desktop
                 repIdeasNew.DataSource = pagedData;
                 repIdeasNew.DataBind();

                 //GenerateResults(dtrepIdeas);
                 // ===>> repIdeas.DataSource = pagedData;
                 // ===>> repIdeas.DataBind();

                //For mobile
                 repIdeasMobile.DataSource = pagedDataMob;
                 repIdeasMobile.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("Idea_main_sublayoutSublayout FillrepIdeas exMain : " + exMain.ToString(), this);
            }
        }

        protected string getRegionLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID));
            }
            return link;
        }

        protected string getRecentLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID));
            }
            return link;
        }

        protected void rptPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            LinkButton lnk = (LinkButton)e.Item.FindControl("btnPage");
            if (lnk != null)
            {
                
                if (lnk.CommandArgument.ToString() == (CurrentPage+1).ToString())
                {
                    lnk.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lnk.ForeColor = System.Drawing.Color.Black;
                }
            }
            
        }

        protected void rptPages_ItemCommand(object source,RepeaterCommandEventArgs e)
        {
            int _currentPage;
            if(int.TryParse(e.CommandArgument.ToString(), out _currentPage))
            {
                CurrentPage = Convert.ToInt32(e.CommandArgument) - 1;
                FillrepIdeas(txtSearch.Text.ToString().Trim());
            }
        }

        protected void rptPagesMob_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int _currentPage;
            if (int.TryParse(e.CommandArgument.ToString(), out _currentPage))
            {
                CurrentPageMob = Convert.ToInt32(e.CommandArgument) - 1;
                FillrepIdeas("");
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            rptPages.ItemCommand += new RepeaterCommandEventHandler(rptPages_ItemCommand);
            rptPagesMob.ItemCommand += new RepeaterCommandEventHandler(rptPagesMob_ItemCommand);
        }

        protected void cmdPrev_Click(object sender, System.EventArgs e)
        {
            // Set viewstate variable to the previous page
            CurrentPage -= 1;

            // Reload control
            FillrepIdeas("");
        }

        protected void cmdNext_Click(object sender, System.EventArgs e)
        {
            // Set viewstate variable to the next page
            CurrentPage += 1;

            // Reload control
            FillrepIdeas("");
        }

        protected void cmdFirst_Click(object sender, System.EventArgs e)
        {
            // Set viewstate variable to the first page
            CurrentPage = 0;

            // Reload control
            FillrepIdeas("");
        }

        protected void cmdLast_Click(object sender, System.EventArgs e)
        {
            // Set viewstate variable to the last page
            CurrentPage = PageCount - 1;

            // Reload control
            FillrepIdeas("");
        }

}

  class idea
  {
      public string cssClass { get; set; }
      public string ideaText { get; set; }
      public string Name { get; set; }
      public string Age { get; set; }
      public string href { get; set; }
      public string Region { get; set; }
  }
}