﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System.Xml;
using System.Data;
using System.IO;
using Sitecore.Publishing;
using System.Net.Mail;
using System.Text;
using System.Configuration;
using CAPTCHA;
using FUNDED_Logger;
using System.Globalization;
namespace Layouts.Applicationorg
{


    public partial class ApplicationorgSublayout : System.Web.UI.UserControl
    {
        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        DataTable dtBudget;
        DataTable dtDonations;
        DataTable dtTimeLine;
        DataTable dtFiles;
        string[] fileName = new string[5];
        string Searchword, Searchword1, Replaceword;
        Item itmContext = Sitecore.Context.Item;
        CaptchaGenerator cgen;
        string strddlSalutationAppone = string.Empty;
        string strapplicantonename = string.Empty;
        string strNricnum = string.Empty;
        string strOccupationAppOne = string.Empty;
        string strddlGenderAppOne = string.Empty;
        string strDobAppone = string.Empty;
        string strAddressAppOne = string.Empty;
        string strMobileNumAppone = string.Empty;
        string strPhoneNumberAppOne = string.Empty;
        string strEmailAppOne = string.Empty;

        string strOrgName = string.Empty;
        string struenNum = string.Empty;
        string strddlOrgType = string.Empty;
        string strNatureOfBusiness = string.Empty;
        string strFoundYear = string.Empty;
        string strNumOfEmp = string.Empty;
        string strOrgAddress = string.Empty;
        string strOrgPhone = string.Empty;
        string strOrgEmail = string.Empty;
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["Organisation"] == null)
                {
                    Logger.WriteLine("8. Session Values is empty in Step3 Organization page load. so redirecting to Step 1.");
                    string path = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('There is an error occurred during the submission, Please fill again.'); window.location='" + "/SG50/Apply For Funding.aspx';", true);
                }
                if (!IsPostBack)
                {
                    Logger.WriteLine("9. Funded form Oraganisation step3 process Started");
                    cgen = new CaptchaGenerator();
                    GetCaptchaimage();
                    Session.Timeout = 45;
                    //lblCaptchMessage.Text = "";

                    Sitecore.Data.Fields.CheckboxField CheckboxTerms = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Terms and Conditions"]);
                    if (CheckboxTerms.Checked == true)
                    {
                        // Response.Write("Test1");
                        DivTerms.Visible = true;
                    }

                    Sitecore.Data.Fields.CheckboxField CheckboxAgreement = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Agreement"]);
                    if (CheckboxAgreement.Checked == true)
                    {
                        // Response.Write("Test2");
                        DivAgreement.Visible = true;
                    }

                    Sitecore.Data.Fields.CheckboxField CheckboxSupporting = ((Sitecore.Data.Fields.CheckboxField)itmContext.Fields["Supporting Documents"]);
                    if (CheckboxSupporting.Checked == true)
                    {
                        //Response.Write("Test3");
                        DivSupporting.Visible = true;
                    }
                }
                Sitecore.Data.Database webData = Sitecore.Context.Database;

                if (webData.GetItem("{36AA7F1D-216D-4C76-ABA9-1EAB325D3643}") != null)
                {
                    Item mediaItem = webData.GetItem("{36AA7F1D-216D-4C76-ABA9-1EAB325D3643}");
                    string mediaUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItem));
                    //  hyplknTC.NavigateUrl = mediaUrl;
                }
                if (webData.GetItem("{600FA06E-D4CD-4375-9EE4-C6299DC53B5D}") != null)
                {
                    Item mediaItemAgreement = webData.GetItem("{600FA06E-D4CD-4375-9EE4-C6299DC53B5D}");
                    string mediaUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItemAgreement));
                    // hyplnkAgreement.NavigateUrl = mediaUrl;
                }
                if (Session["Budjet"] != null)
                {
                    dtBudget = (DataTable)Session["Budjet"];
                }
                if (Session["donations"] != null)
                {
                    dtDonations = (DataTable)Session["donations"];
                }

                if (Session["Timeline"] != null)
                {
                    dtTimeLine = (DataTable)Session["Timeline"];
                }
                if (Session["FileId"] != null)
                {
                    dtFiles = (DataTable)Session["FileId"];
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }

        public bool validationCheck()
        {
            bool validation = false;
            try
            {
                strddlSalutationAppone = ddlSalutationAppone.SelectedItem.Text;
                strapplicantonename = SG50Class.StripUnwantedString(txtapplicantonename.Text.TrimEnd());
                strNricnum = SG50Class.StripUnwantedString(txtNricnum.Text.TrimEnd());
                strOccupationAppOne = SG50Class.StripUnwantedString(txtOccupationAppOne.Text.TrimEnd());
                strddlGenderAppOne = ddlGenderAppOne.SelectedItem.Text;
                strDobAppone = txtDobAppone.Text.TrimEnd();
                strAddressAppOne = SG50Class.StripUnwantedString(txtAddressAppOne.Text.TrimEnd());
                strMobileNumAppone = SG50Class.StripUnwantedString(txtMobileNumAppone.Text.TrimEnd());
                strPhoneNumberAppOne = SG50Class.StripUnwantedString(txtPhoneNumberAppOne.Text.TrimEnd());
                strEmailAppOne = SG50Class.StripUnwantedString(txtEmailAppOne.Text.TrimEnd());

                strOrgName = SG50Class.StripUnwantedString(txtOrgName.Text.TrimEnd());
                struenNum = SG50Class.StripUnwantedString(txtuenNum.Text.TrimEnd());
                strddlOrgType = ddlOrgType.SelectedItem.Text;
                strNatureOfBusiness = SG50Class.StripUnwantedString(txtNatureOfBusiness.Text.TrimEnd());
                strFoundYear = SG50Class.StripUnwantedString(txtFoundYear.Text.TrimEnd());
                strNumOfEmp = SG50Class.StripUnwantedString(txtNumOfEmp.Text.TrimEnd());
                strOrgAddress = SG50Class.StripUnwantedString(txtOrgAddress.Text.TrimEnd());
                strOrgPhone = SG50Class.StripUnwantedString(txtOrgPhone.Text.TrimEnd());
                strOrgEmail = SG50Class.StripUnwantedString(txtOrgEmail.Text.TrimEnd());

                DateTime d;
                int EnteredIntValue = 0;
                if (strapplicantonename != "" && strNricnum != "" && strOccupationAppOne != "" && strDobAppone != "" && strAddressAppOne != "" && strMobileNumAppone != "" && strEmailAppOne != "" && strOrgName != "" & struenNum != "" && strNatureOfBusiness != "" && strFoundYear != "" && strNumOfEmp != "" && strOrgAddress != "" && strOrgEmail != "" && strddlSalutationAppone != "Select" && strddlGenderAppOne != "Select" && strddlOrgType != "Select" && strOrgPhone != "")
                {
                    if (int.TryParse(strMobileNumAppone, out EnteredIntValue) && int.TryParse(strNumOfEmp, out EnteredIntValue) && int.TryParse(strFoundYear, out EnteredIntValue) && int.TryParse(strOrgPhone, out EnteredIntValue))
                    {
                        if (strapplicantonename.Length <= 66 && strNricnum.Length <= 9 && strOccupationAppOne.Length <= 100 && strAddressAppOne.Length <= 120 && strMobileNumAppone.Length <= 8 && strEmailAppOne.Length < 320 && strOrgName.Length <= 100 && struenNum.Length <= 10 && strNatureOfBusiness.Length <= 255 && strFoundYear.Length == 4 && strNumOfEmp.Length <= 4 && strOrgAddress.Length <= 120 && strOrgPhone.Length == 8 && strOrgEmail.Length <= 320 && DateTime.TryParseExact(strDobAppone, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                        {
                            validation = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
            return validation;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {

                    if (validationCheck() == true)
                    {

                        if (txt_CaptchaVal.Text == SG50Class.StripUnwantedString(txt_Captcha.Text))
                        {
                            if (Session["ItemId"] != null && Session["ItemId"].ToString() != "")
                            {
                                Logger.WriteLine("10. Submitting  Oraganisation step3 details.");
                                Logger.WriteLine("11. NRIC." + txtNricnum.Text);
                                string ItemId = Session["ItemId"].ToString();
                                Item FormItem = master.GetItem(ItemId);

                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                {


                                    try
                                    {
                                        FormItem.Editing.BeginEdit();
                                        FormItem.Name = txtNricnum.Text + "-" + DateTime.Now.ToString("yyMMdd") + "-" + DateTime.Now.ToString("hhmmss");
                                        Session["ApplicationName"] = FormItem.Name;
                                        FormItem.Fields["salutation1"].Value = Convert.ToString(ddlSalutationAppone.SelectedItem.Text);
                                        FormItem.Fields["Name Of Applicant1"].Value = strapplicantonename;
                                        FormItem.Fields["NRIC Number1"].Value = strNricnum;
                                        FormItem.Fields["Nationality1"].Value = Convert.ToString("Singaporean");
                                        FormItem.Fields["Occupation1"].Value = strOccupationAppOne;
                                        FormItem.Fields["Gender1"].Value = Convert.ToString(ddlGenderAppOne.SelectedItem.Text);
                                        FormItem.Fields["Date Of Birth1"].Value = strDobAppone;
                                        FormItem.Fields["Address1"].Value = strAddressAppOne;
                                        FormItem.Fields["MobileNumber1"].Value = strMobileNumAppone;
                                        FormItem.Fields["Home Number1"].Value = strPhoneNumberAppOne;
                                        FormItem.Fields["Email Address1"].Value = strEmailAppOne;
                                        FormItem.Fields["Postal Code1"].Value = "";// Convert.ToString(txtPostalCodeAppOne.Text);

                                        FormItem.Fields["Name Of Organisation"].Value = strOrgName;
                                        FormItem.Fields["UEN"].Value = struenNum;
                                        FormItem.Fields["Organisation Type"].Value = Convert.ToString(ddlOrgType.SelectedItem.Text);
                                        FormItem.Fields["Nature Of Business"].Value = strNatureOfBusiness;
                                        FormItem.Fields["Founding Year"].Value = strFoundYear;
                                        FormItem.Fields["Number Of Employees"].Value = strNumOfEmp;
                                        FormItem.Fields["Organisation Address"].Value = strOrgAddress;
                                        FormItem.Fields["Organisation postalcode"].Value = "";// Convert.ToString(txtOrgPostal.Text);
                                        FormItem.Fields["Organisation Phone Number"].Value = strOrgPhone;
                                        FormItem.Fields["Organisation Email Address"].Value = strOrgEmail;
                                        FormItem.Editing.EndEdit();

                                        for (int i = 0; i < dtBudget.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Project Fund");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newFund = parentItem.Add("fund" + i, template);
                                            try
                                            {
                                                newFund.Editing.BeginEdit();
                                                newFund.Fields["Item"].Value = Convert.ToString(dtBudget.Rows[i][1]);
                                                newFund.Fields["Fund Amount"].Value = Convert.ToString(dtBudget.Rows[i][2]);
                                                newFund.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }

                                        for (int i = 0; i < dtDonations.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Donations");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newDonation = parentItem.Add("Donation" + i, template);
                                            try
                                            {
                                                newDonation.Editing.BeginEdit();
                                                newDonation.Fields["Particulars Of Source"].Value = Convert.ToString(dtDonations.Rows[i][1]);
                                                newDonation.Fields["Funding Amount"].Value = Convert.ToString(dtDonations.Rows[i][2]);
                                                if (Convert.ToString(dtDonations.Rows[i][3]) == "1")
                                                {
                                                    newDonation.Fields["Funding Status"].Value = "Confirmed";
                                                }
                                                else
                                                {
                                                    newDonation.Fields["Funding Status"].Value = "Pending";
                                                }

                                                newDonation.Fields["Funding Duration"].Value = Convert.ToString(dtDonations.Rows[i][4]);
                                                newDonation.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }


                                        for (int i = 0; i < dtTimeLine.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Time Line");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newTimeline = parentItem.Add("TimeLine" + i, template);
                                            try
                                            {
                                                newTimeline.Editing.BeginEdit();
                                                newTimeline.Fields["Date"].Value = Convert.ToString(dtTimeLine.Rows[i][1]);
                                                newTimeline.Fields["Event"].Value = Convert.ToString(dtTimeLine.Rows[i][2]);
                                                newTimeline.Fields["Milestones"].Value = Convert.ToString(dtTimeLine.Rows[i][3]);
                                                newTimeline.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }
                                        for (int i = 0; i < FormItem.GetChildren().Count; i++)
                                        {
                                            if (FormItem.GetChildren()[i].TemplateName.ToString() == "File Template")
                                            {
                                                FormItem.GetChildren()[i].Delete();
                                            }

                                        }
                                        for (int i = 0; i < dtFiles.Rows.Count; i++)
                                        {
                                            Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/File Template");
                                            Item parentItem = master.GetItem(ItemId);
                                            Item newFile = parentItem.Add("File" + i, template);
                                            try
                                            {
                                                newFile.Editing.BeginEdit();

                                                Sitecore.Data.Fields.FileField files = newFile.Fields["Uploaded File Path"];

                                                if (files.Src != null)
                                                {
                                                    Sitecore.Data.Items.MediaItem sample = master.GetItem(dtFiles.Rows[i][0].ToString());
                                                    fileName[i] = sample.Name;
                                                    newFile["Name"] = sample.Name;
                                                    files.Src = sample.MediaPath;
                                                    files.SetAttribute("showineditor", "1");
                                                    files.MediaID = sample.ID;
                                                    //Publishing Media item
                                                    Item Mediaitem = master.GetItem(dtFiles.Rows[i][0].ToString());
                                                    var source = master;
                                                    var target = Sitecore.Data.Database.GetDatabase("web");

                                                    var options = new PublishOptions(source, target, PublishMode.SingleItem, Mediaitem.Language, DateTime.Now)
                                                    {
                                                        RootItem = Mediaitem,
                                                        Deep = true,
                                                    };

                                                    var publisher = new Publisher(options);
                                                    publisher.PublishAsync();
                                                }

                                                newFile.Editing.EndEdit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw ex;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw ex;
                                    }


                                    Logger.WriteLine("12 Generating XML document.");
                                    generateXml(FormItem);
                                    generateXml_Code(FormItem);
                                    Session["Budjet"] = null;
                                    Session["donations"] = null;
                                    Session["Timeline"] = null;
                                    var item = FormItem;
                                    using (new Sitecore.SecurityModel.SecurityDisabler())
                                    {
                                        if (item != null)
                                        {

                                            var source = master;
                                            var target = Sitecore.Data.Database.GetDatabase("web");

                                            var options = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now)
                                            {
                                                RootItem = item,
                                                Deep = true,
                                            };

                                            var publisher = new Publisher(options);
                                            publisher.PublishAsync();

                                        }
                                    }
                                    Session["ItemId"] = null;

                                    Session["SEA"] = null;
                                    Session["Organisation"] = null;
                                    Session["NRIC"] = txtNricnum.Text;
                                    GetCaptchaimage();
                                    Logger.WriteLine("13 Redirecting to Thank you page.");
                                    string url = "~/SG50/step4.aspx";
                                    if (!(url.Contains("://")))
                                        Response.Redirect(url);

                                }
                            }
                            else
                            {
                                GetCaptchaimage();
                                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('There is an error in submission, Please try again to fill the Form at Step 2.');", true);
                            }

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Captcha Code');", true);
                        }

                    }
                    else
                    {
                        GetCaptchaimage();
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Data.');", true);

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }


        private void generateXml(Item FormItem)
        {
            XmlDocument docConfig = new XmlDocument();
            XmlNode xmlNode = docConfig.CreateNode(XmlNodeType.XmlDeclaration, "", "");
            XmlElement rootElement = docConfig.CreateElement("new_fundapplication");
            docConfig.AppendChild(rootElement);

            XmlElement hedder = docConfig.CreateElement("Applicant_Details");
            docConfig.DocumentElement.PrependChild(hedder);
            docConfig.ChildNodes.Item(0).AppendChild(hedder);


            XmlElement TagName = docConfig.CreateElement("Applicant1");
            hedder.AppendChild(TagName);

            XmlElement TagSalutation = docConfig.CreateElement("mccy_salutation");
            XmlText tagSalutationValue = docConfig.CreateTextNode(FormItem["salutation1"]);
            TagSalutation.PrependChild(tagSalutationValue);
            TagName.AppendChild(TagSalutation);

            XmlElement TagNric = docConfig.CreateElement("mccy_nameofapplicantasinnric");
            XmlText TagNricValue = docConfig.CreateTextNode(FormItem["Name Of Applicant1"]);
            TagNric.PrependChild(TagNricValue);
            TagName.AppendChild(TagNric);

            XmlElement TagIdtype = docConfig.CreateElement("mccy_idtype");
            XmlText TagIdtypeValue = docConfig.CreateTextNode("NRIC");
            TagIdtype.PrependChild(TagIdtypeValue);
            TagName.AppendChild(TagIdtype);

            XmlElement TagIdNumber = docConfig.CreateElement("mccy_idnumber");
            XmlText TagIdNumberValue = docConfig.CreateTextNode(FormItem["NRIC Number1"]);
            TagIdNumber.PrependChild(TagIdNumberValue);
            TagName.AppendChild(TagIdNumber);

            XmlElement TagNationality = docConfig.CreateElement("mccy_nationality");
            XmlText TagNationalityValue = docConfig.CreateTextNode("Singaporian");
            TagNationality.PrependChild(TagNationalityValue);
            TagName.AppendChild(TagNationality);

            XmlElement TagOccDesignation = docConfig.CreateElement("mccy_occupationdesignation");
            XmlText TagOccDesignationValue = docConfig.CreateTextNode(FormItem["Occupation1"]);
            TagOccDesignation.PrependChild(TagOccDesignationValue);
            TagName.AppendChild(TagOccDesignation);

            XmlElement TagGender = docConfig.CreateElement("mccy_gender");
            XmlText TagGenderValue = docConfig.CreateTextNode(FormItem["Gender1"]);
            TagGender.PrependChild(TagGenderValue);
            TagName.AppendChild(TagGender);

            XmlElement TagDOB = docConfig.CreateElement("mccy_dateofbirth");
            XmlText TagDOBValue = docConfig.CreateTextNode(FormItem["Date Of Birth1"]);
            TagDOB.PrependChild(TagDOBValue);
            TagName.AppendChild(TagDOB);

            XmlElement TagAddressType1 = docConfig.CreateElement("mccy_addresstype1");
            XmlText TagAddressType1Value = docConfig.CreateTextNode("");
            TagAddressType1.PrependChild(TagAddressType1Value);
            TagName.AppendChild(TagAddressType1);

            XmlElement TagAddress1 = docConfig.CreateElement("mccy_address1");
            XmlText TagAddress1Value = docConfig.CreateTextNode(FormItem["Address1"]);
            TagAddress1.PrependChild(TagAddress1Value);
            TagName.AppendChild(TagAddress1);

            XmlElement TagPostalCode1 = docConfig.CreateElement("mccy_postalcode1");
            XmlText TagPostalCode1Value = docConfig.CreateTextNode(FormItem["Postal Code1"]);
            TagPostalCode1.PrependChild(TagPostalCode1Value);
            TagName.AppendChild(TagPostalCode1);

            XmlElement TagBuildingName1 = docConfig.CreateElement("mccy_buildingname1");
            XmlText TagBuildingName1Value = docConfig.CreateTextNode("");
            TagBuildingName1.PrependChild(TagBuildingName1Value);
            TagName.AppendChild(TagBuildingName1);

            XmlElement TagBlkno1 = docConfig.CreateElement("mccy_blkhseno1");
            XmlText TagBlkno1Value = docConfig.CreateTextNode("");
            TagBlkno1.PrependChild(TagBlkno1Value);
            TagName.AppendChild(TagBlkno1);

            XmlElement TagStreetName1 = docConfig.CreateElement("mccy_streetname1");
            XmlText TagStreetName1Value = docConfig.CreateTextNode("");
            TagStreetName1.PrependChild(TagStreetName1Value);
            TagName.AppendChild(TagStreetName1);

            XmlElement TagFloorno1 = docConfig.CreateElement("mccy_floorno1");
            XmlText TagFloorno1Value = docConfig.CreateTextNode("");
            TagFloorno1.PrependChild(TagFloorno1Value);
            TagName.AppendChild(TagFloorno1);

            XmlElement TagUnit1 = docConfig.CreateElement("mccy_unitno1");
            XmlText TagUnit1Value = docConfig.CreateTextNode("");
            TagUnit1.PrependChild(TagUnit1Value);
            TagName.AppendChild(TagUnit1);

            XmlElement TagOverAddressline1 = docConfig.CreateElement("mccy_overseaaddressline1");
            XmlText TagOverAddressline1Value = docConfig.CreateTextNode("");
            TagOverAddressline1.PrependChild(TagOverAddressline1Value);
            TagName.AppendChild(TagOverAddressline1);

            XmlElement TagOverAddressline2 = docConfig.CreateElement("mccy_overseaaddressline2");
            XmlText TagOverAddressline2Value = docConfig.CreateTextNode("");
            TagOverAddressline2.PrependChild(TagOverAddressline2Value);
            TagName.AppendChild(TagOverAddressline2);

            XmlElement TagHomePhone = docConfig.CreateElement("mccy_homephone");
            XmlText TagHomePhoneValue = docConfig.CreateTextNode(FormItem["Home Number1"]);
            TagHomePhone.PrependChild(TagHomePhoneValue);
            TagName.AppendChild(TagHomePhone);

            XmlElement TagMobilePhone = docConfig.CreateElement("mccy_mobilephone");
            XmlText TagMobilePhoneValue = docConfig.CreateTextNode(FormItem["MobileNumber1"]);
            TagMobilePhone.PrependChild(TagMobilePhoneValue);
            TagName.AppendChild(TagMobilePhone);

            XmlElement TagEmail = docConfig.CreateElement("mccy_email");
            XmlText TagEmailValue = docConfig.CreateTextNode(FormItem["Email Address1"]);
            TagEmail.PrependChild(TagEmailValue);
            TagName.AppendChild(TagEmail);



            XmlElement TagName2 = docConfig.CreateElement("Applicant2");
            hedder.AppendChild(TagName2);

            XmlElement TagSalutation2 = docConfig.CreateElement("mccy_salutation2");
            XmlText tagSalutation2Value = docConfig.CreateTextNode(FormItem["Salutation2"]);
            TagSalutation2.PrependChild(tagSalutation2Value);
            TagName2.AppendChild(TagSalutation2);

            XmlElement TagNric2 = docConfig.CreateElement("mccy_nameofapplicant2asinnricpassport");
            XmlText TagNric2Value = docConfig.CreateTextNode(FormItem["Name Of Applicant2"]);
            TagNric2.PrependChild(TagNric2Value);
            TagName2.AppendChild(TagNric2);

            XmlElement TagIdtype2 = docConfig.CreateElement("mccy_idtype2");
            XmlText TagIdtype2Value = docConfig.CreateTextNode(FormItem["Id Type2"]);
            TagIdtype2.PrependChild(TagIdtype2Value);
            TagName2.AppendChild(TagIdtype2);

            XmlElement TagIdNumber2 = docConfig.CreateElement("mccy_idnumber2");
            XmlText TagIdNumber2Value = docConfig.CreateTextNode(FormItem["Id Number2"]);
            TagIdNumber2.PrependChild(TagIdNumber2Value);
            TagName2.AppendChild(TagIdNumber2);

            XmlElement TagPassport = docConfig.CreateElement("mccy_passportno");
            XmlText TagPassportValue = docConfig.CreateTextNode(FormItem["PassportNo"]);
            TagPassport.PrependChild(TagPassportValue);
            TagName2.AppendChild(TagPassport);

            XmlElement TagNationality2 = docConfig.CreateElement("mccy_nationality2");
            XmlText TagNationality2Value = docConfig.CreateTextNode(FormItem["Nationality2"]);
            TagNationality2.PrependChild(TagNationality2Value);
            TagName2.AppendChild(TagNationality2);

            XmlElement TagOccDesignation2 = docConfig.CreateElement("mccy_occupationdesignation2");
            XmlText TagOccDesignation2Value = docConfig.CreateTextNode(FormItem["Occupation2"]);
            TagOccDesignation2.PrependChild(TagOccDesignation2Value);
            TagName2.AppendChild(TagOccDesignation2);

            XmlElement TagGender2 = docConfig.CreateElement("mccy_gender2");
            XmlText TagGender2Value = docConfig.CreateTextNode(FormItem["Gender2"]);
            TagGender2.PrependChild(TagGender2Value);
            TagName2.AppendChild(TagGender2);

            XmlElement TagDOB2 = docConfig.CreateElement("mccy_dateofbirth2");
            XmlText TagDOB2Value = docConfig.CreateTextNode(FormItem["Date Of Birth2"]);
            TagDOB2.PrependChild(TagDOB2Value);
            TagName2.AppendChild(TagDOB2);

            XmlElement TagAddressType2 = docConfig.CreateElement("mccy_addresstype2");
            XmlText TagAddressType2Value = docConfig.CreateTextNode(FormItem["Address Type2"]);
            TagAddressType2.PrependChild(TagAddressType2Value);
            TagName2.AppendChild(TagAddressType2);

            XmlElement TagAddress2 = docConfig.CreateElement("mccy_address2");
            XmlText TagAddress2Value = docConfig.CreateTextNode(FormItem["Address2"]);
            TagAddress2.PrependChild(TagAddress2Value);
            TagName2.AppendChild(TagAddress2);

            XmlElement TagPostalCode2 = docConfig.CreateElement("mccy_postalcode2");
            XmlText TagPostalCode2Value = docConfig.CreateTextNode(FormItem["Postal Code2"]);
            TagPostalCode2.PrependChild(TagPostalCode2Value);
            TagName2.AppendChild(TagPostalCode2);

            XmlElement TagBuildingName2 = docConfig.CreateElement("mccy_buildingname2");
            XmlText TagBuildingName2Value = docConfig.CreateTextNode("");
            TagBuildingName2.PrependChild(TagBuildingName2Value);
            TagName2.AppendChild(TagBuildingName2);

            XmlElement TagBlkno2 = docConfig.CreateElement("mccy_blkhseno2");
            XmlText TagBlkno2Value = docConfig.CreateTextNode("");
            TagBlkno2.PrependChild(TagBlkno2Value);
            TagName2.AppendChild(TagBlkno2);

            XmlElement TagStreetName2 = docConfig.CreateElement("mccy_streetname2");
            XmlText TagStreetName2Value = docConfig.CreateTextNode("");
            TagStreetName2.PrependChild(TagStreetName2Value);
            TagName2.AppendChild(TagStreetName2);

            XmlElement TagFloorno2 = docConfig.CreateElement("mccy_floorno2");
            XmlText TagFloorno2Value = docConfig.CreateTextNode("");
            TagFloorno2.PrependChild(TagFloorno2Value);
            TagName2.AppendChild(TagFloorno2);

            XmlElement TagUnit2 = docConfig.CreateElement("mccy_unitno2");
            XmlText TagUnit2Value = docConfig.CreateTextNode("");
            TagUnit2.PrependChild(TagUnit2Value);
            TagName2.AppendChild(TagUnit2);

            XmlElement TagOverAddressline2_1 = docConfig.CreateElement("mccy_overseaaddressline1_2");
            XmlText TagOverAddressline2_1Value = docConfig.CreateTextNode("");
            TagOverAddressline2_1.PrependChild(TagOverAddressline2_1Value);
            TagName2.AppendChild(TagOverAddressline2_1);

            XmlElement TagOverAddressline2_2 = docConfig.CreateElement("mccy_overseaaddressline2_1");
            XmlText TagOverAddressline2_2Value = docConfig.CreateTextNode("");
            TagOverAddressline2_2.PrependChild(TagOverAddressline2_2Value);
            TagName2.AppendChild(TagOverAddressline2_2);

            XmlElement TagHomePhone2 = docConfig.CreateElement("mccy_homephone2");
            XmlText TagHomePhone2Value = docConfig.CreateTextNode(FormItem["Phione Number2"]);
            TagHomePhone2.PrependChild(TagHomePhone2Value);
            TagName2.AppendChild(TagHomePhone2);

            XmlElement TagMobilePhone2 = docConfig.CreateElement("mccy_mobilephone2");
            XmlText TagMobilePhone2Value = docConfig.CreateTextNode(FormItem["MobileNumber2"]);
            TagMobilePhone2.PrependChild(TagMobilePhone2Value);
            TagName2.AppendChild(TagMobilePhone2);

            XmlElement TagEmail2 = docConfig.CreateElement("mccy_email2");
            XmlText TagEmail2Value = docConfig.CreateTextNode(FormItem["Email Address2"]);
            TagEmail2.PrependChild(TagEmail2Value);
            TagName2.AppendChild(TagEmail2);



            XmlElement Organizationhedder = docConfig.CreateElement("Organisation_Details");
            docConfig.DocumentElement.PrependChild(Organizationhedder);
            docConfig.ChildNodes.Item(0).AppendChild(Organizationhedder);



            XmlElement TagOraganization = docConfig.CreateElement("Organisation");
            Organizationhedder.AppendChild(TagOraganization);

            XmlElement TagOraganizationName = docConfig.CreateElement("mccy_nameoforganisation");
            XmlText OraganizationNameValue = docConfig.CreateTextNode(FormItem["Name Of Organisation"]);
            TagOraganizationName.PrependChild(OraganizationNameValue);
            TagOraganization.AppendChild(TagOraganizationName);


            XmlElement Taguen = docConfig.CreateElement("mccy_uen");
            XmlText TaguenValue = docConfig.CreateTextNode(FormItem["UEN"]);
            Taguen.PrependChild(TaguenValue);
            TagOraganization.AppendChild(Taguen);


            XmlElement TagOraganizationType = docConfig.CreateElement("mccy_organisationtype");
            XmlText TagOraganizationTypeValue = docConfig.CreateTextNode(FormItem["Organisation Type"]);
            TagOraganizationType.PrependChild(TagOraganizationTypeValue);
            TagOraganization.AppendChild(TagOraganizationType);

            XmlElement TagNatureOfBussiness = docConfig.CreateElement("mccy_natureofbusiness");
            XmlText TagNatureOfBussinessValue = docConfig.CreateTextNode(FormItem["Nature Of Business"]);
            TagNatureOfBussiness.PrependChild(TagNatureOfBussinessValue);
            TagOraganization.AppendChild(TagNatureOfBussiness);

            XmlElement TagFoundingYear = docConfig.CreateElement("mccy_foundingyr");
            XmlText TagFoundingYearValue = docConfig.CreateTextNode(FormItem["Founding Year"]);
            TagFoundingYear.PrependChild(TagFoundingYearValue);
            TagOraganization.AppendChild(TagFoundingYear);

            XmlElement TagNoOfEmployees = docConfig.CreateElement("mccy_numberofemployees");
            XmlText TagNoOfEmployeesValue = docConfig.CreateTextNode(FormItem["Number Of Employees"]);
            TagNoOfEmployees.PrependChild(TagNoOfEmployeesValue);
            TagOraganization.AppendChild(TagNoOfEmployees);

            XmlElement TagOrgAddressType = docConfig.CreateElement("mccy_addresstype");
            XmlText TagOrgAddressTypeValue = docConfig.CreateTextNode("local");
            TagOrgAddressType.PrependChild(TagOrgAddressTypeValue);
            TagOraganization.AppendChild(TagOrgAddressType);

            XmlElement TagOrgAddress = docConfig.CreateElement("mccy_address");
            XmlText TagOrgAddressValue = docConfig.CreateTextNode(FormItem["Organisation Address"]);
            TagOrgAddress.PrependChild(TagOrgAddressValue);
            TagOraganization.AppendChild(TagOrgAddress);

            XmlElement TagOrgPostalCode = docConfig.CreateElement("mccy_postalcode");
            XmlText TagPostalCodeValue = docConfig.CreateTextNode(FormItem["Organisation postalcode"]);
            TagOrgPostalCode.PrependChild(TagPostalCodeValue);
            TagOraganization.AppendChild(TagOrgPostalCode);

            XmlElement TagOrgBulidingName = docConfig.CreateElement("mccy_buildingname");
            XmlText TagOrgBulidingNameValue = docConfig.CreateTextNode("");
            TagOrgBulidingName.PrependChild(TagOrgBulidingNameValue);
            TagOraganization.AppendChild(TagOrgBulidingName);

            XmlElement TagOrgBlkNo = docConfig.CreateElement("mccy_blkhseno");
            XmlText TagOrgBlkNoValue = docConfig.CreateTextNode("");
            TagOrgBlkNo.PrependChild(TagOrgBlkNoValue);
            TagOraganization.AppendChild(TagOrgBlkNo);

            XmlElement TagOrgStreetName = docConfig.CreateElement("mccy_streetname");
            XmlText TagOrgStreetNameValue = docConfig.CreateTextNode("");
            TagOrgStreetName.PrependChild(TagOrgStreetNameValue);
            TagOraganization.AppendChild(TagOrgStreetName);


            XmlElement TagOrgFloorNo = docConfig.CreateElement("mccy_floorno");
            XmlText TagOrgFloorNoValue = docConfig.CreateTextNode("");
            TagOrgFloorNo.PrependChild(TagOrgFloorNoValue);
            TagOraganization.AppendChild(TagOrgFloorNo);

            XmlElement TagOrgUnitNo = docConfig.CreateElement("mccy_unitno");
            XmlText TagOrgUnitNoValue = docConfig.CreateTextNode("");
            TagOrgUnitNo.PrependChild(TagOrgUnitNoValue);
            TagOraganization.AppendChild(TagOrgUnitNo);

            XmlElement TagOrgAddressLine1 = docConfig.CreateElement("mccy_overseaaddressline1_1");
            XmlText TagOrgAddressLine1Value = docConfig.CreateTextNode("");
            TagOrgAddressLine1.PrependChild(TagOrgAddressLine1Value);
            TagOraganization.AppendChild(TagOrgAddressLine1);

            XmlElement TagOrgAddressLine2 = docConfig.CreateElement("mccy_overseaaddressline2_2");
            XmlText TagOrgAddressLine2Value = docConfig.CreateTextNode("");
            TagOrgAddressLine2.PrependChild(TagOrgAddressLine2Value);
            TagOraganization.AppendChild(TagOrgAddressLine2);

            XmlElement TagOrgPhone = docConfig.CreateElement("mccy_phonenumber");
            XmlText TagOrgPhoneValue = docConfig.CreateTextNode(FormItem["Organisation Phone Number"]);
            TagOrgPhone.PrependChild(TagOrgPhoneValue);
            TagOraganization.AppendChild(TagOrgPhone);

            XmlElement TagOrgEmail = docConfig.CreateElement("mccy_emailaddress");
            XmlText TagOrgEmailValue = docConfig.CreateTextNode(FormItem["Organisation Email Address"]);
            TagOrgEmail.PrependChild(TagOrgEmailValue);
            TagOraganization.AppendChild(TagOrgEmail);




            XmlElement ProjectDetails = docConfig.CreateElement("Project_Details");
            docConfig.DocumentElement.PrependChild(ProjectDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectDetails);

            string SportSEA = string.Empty;
            if (Session["SEA"] != null)
                SportSEA = "2";
            else
                SportSEA = "1";


            XmlElement TagProjectDescription = docConfig.CreateElement("Project_Description");
            ProjectDetails.AppendChild(TagProjectDescription);

            XmlElement TagApplicationType = docConfig.CreateElement("mccy_applicationtype");
            XmlText TagApplicationTypeValue = docConfig.CreateTextNode(SportSEA);
            TagApplicationType.PrependChild(TagApplicationTypeValue);
            TagProjectDescription.AppendChild(TagApplicationType);

            XmlElement TagProjectTitle = docConfig.CreateElement("mccy_projecttitle");
            XmlText TagProjectTitleValue = docConfig.CreateTextNode(FormItem["Project Title"]);
            TagProjectTitle.PrependChild(TagProjectTitleValue);
            TagProjectDescription.AppendChild(TagProjectTitle);

            XmlElement TagProjectObjectives = docConfig.CreateElement("mccy_projectobjectives");
            XmlText TagProjectObjectivesValue = docConfig.CreateTextNode(FormItem["Project Objectives"]);
            TagProjectObjectives.PrependChild(TagProjectObjectivesValue);
            TagProjectDescription.AppendChild(TagProjectObjectives);

            XmlElement TagIntendedParticipants = docConfig.CreateElement("mccy_intendedparticipantsbeneficiaries");
            XmlText TagIntendedParticipantsValue = docConfig.CreateTextNode(FormItem["Intended Participants"]);
            TagIntendedParticipants.PrependChild(TagIntendedParticipantsValue);
            TagProjectDescription.AppendChild(TagIntendedParticipants);

            XmlElement TagEstimatedParticipants = docConfig.CreateElement("mccy_estimatedtotalnoofparticipantsbenefit");
            XmlText TagEstimatedParticipantsValue = docConfig.CreateTextNode(FormItem["Esitimated NoOfparticipants"]);
            TagEstimatedParticipants.PrependChild(TagEstimatedParticipantsValue);
            TagProjectDescription.AppendChild(TagEstimatedParticipants);

            XmlElement TagDeclarationType = docConfig.CreateElement("mccy_FormType");
            XmlText TagDeclarationTypeValue = docConfig.CreateTextNode("ORGANISATION");
            TagDeclarationType.PrependChild(TagDeclarationTypeValue);
            TagProjectDescription.AppendChild(TagDeclarationType);

            XmlElement TagAppliedas = docConfig.CreateElement("mccy_Appliedas");
            XmlText TagAppliedasValue = docConfig.CreateTextNode("ORGANISATION");
            TagAppliedas.PrependChild(TagAppliedasValue);
            TagProjectDescription.AppendChild(TagAppliedas);


            XmlElement TagProjectBudget = docConfig.CreateElement("Project_Budget");
            ProjectDetails.AppendChild(TagProjectBudget);

            XmlElement TagAmountRequested = docConfig.CreateElement("mccy_amountrequested");
            XmlText TagAmountRequestedValue = docConfig.CreateTextNode(FormItem["Amount Requested"]);
            TagAmountRequested.PrependChild(TagAmountRequestedValue);
            TagProjectBudget.AppendChild(TagAmountRequested);

            XmlElement TagProjectedExpenditure = docConfig.CreateElement("mccy_totalprojectedexpenditure");
            XmlText TagProjectedExpenditureValue = docConfig.CreateTextNode(FormItem["Projected Expenditure"]);
            TagProjectedExpenditure.PrependChild(TagProjectedExpenditureValue);
            TagProjectBudget.AppendChild(TagProjectedExpenditure);


            XmlElement Taganyfundraisingcomponent = docConfig.CreateElement("mccy_anyfundraisingcomponent");
            XmlText TaganyfundraisingcomponentValue = docConfig.CreateTextNode("");
            Taganyfundraisingcomponent.PrependChild(TaganyfundraisingcomponentValue);
            TagProjectBudget.AppendChild(Taganyfundraisingcomponent);

            XmlElement TagRelevantProjectExperience = docConfig.CreateElement("mccy_relevantprojectexperiencs");
            XmlText TagRelevantProjectExperienceValue = docConfig.CreateTextNode(FormItem["Relevant Project Experience"]);
            TagRelevantProjectExperience.PrependChild(TagRelevantProjectExperienceValue);
            TagProjectBudget.AppendChild(TagRelevantProjectExperience);



            XmlElement TagProjectTimeLine = docConfig.CreateElement("Proposed_Project_Timeline");
            ProjectDetails.AppendChild(TagProjectTimeLine);

            XmlElement TagProjectStartDate = docConfig.CreateElement("mccy_proposedstartdate");
            XmlText TagProjectStartDateValue = docConfig.CreateTextNode(FormItem["Start Date"]);
            TagProjectStartDate.PrependChild(TagProjectStartDateValue);
            TagProjectTimeLine.AppendChild(TagProjectStartDate);

            XmlElement TagProjectEndDate = docConfig.CreateElement("mccy_proposedenddate");
            XmlText TagProjectEndDateValue = docConfig.CreateTextNode(FormItem["End Date"]);
            TagProjectEndDate.PrependChild(TagProjectEndDateValue);
            TagProjectTimeLine.AppendChild(TagProjectEndDate);

            XmlElement TagProjectDeclaration = docConfig.CreateElement("Project_Declaration");
            ProjectDetails.AppendChild(TagProjectDeclaration);

            XmlElement Tagmccydeclarantname = docConfig.CreateElement("mccy_declarantname");
            XmlText TagmccydeclarantnameValue = docConfig.CreateTextNode("");
            Tagmccydeclarantname.PrependChild(TagmccydeclarantnameValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarantname);

            XmlElement Tagmccydeclarationdate = docConfig.CreateElement("mccy_declarationdate");
            XmlText TagmccydeclarationdateValue = docConfig.CreateTextNode("");
            Tagmccydeclarationdate.PrependChild(TagmccydeclarationdateValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarationdate);

            XmlElement ProjectExpenditures = docConfig.CreateElement("Project_Expenditures");
            docConfig.DocumentElement.PrependChild(ProjectExpenditures);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectExpenditures);



            Sitecore.Collections.ChildList ChildBudget = FormItem.GetChildren();
            foreach (Item BudgetItem in ChildBudget)
            {
                if (BudgetItem.TemplateName.ToString() == "Project Fund")
                {
                    XmlElement TagProjectExpenditure = docConfig.CreateElement("Expenditure");
                    ProjectExpenditures.AppendChild(TagProjectExpenditure);

                    XmlElement TagExpenditure = docConfig.CreateElement("mccy_name");
                    XmlText TagExpenditureValue = docConfig.CreateTextNode(BudgetItem["Item"]);
                    TagExpenditure.PrependChild(TagExpenditureValue);
                    TagProjectExpenditure.AppendChild(TagExpenditure);

                    XmlElement TagFund = docConfig.CreateElement("mccy_projectedexpenditure");
                    XmlText TagFundValue = docConfig.CreateTextNode(BudgetItem["Fund Amount"]);
                    TagFund.PrependChild(TagFundValue);
                    TagProjectExpenditure.AppendChild(TagFund);
                }
            }



            XmlElement ProjectFundingSources = docConfig.CreateElement("Project_FundingSources");
            docConfig.DocumentElement.PrependChild(ProjectFundingSources);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectFundingSources);



            Sitecore.Collections.ChildList ChildFund = FormItem.GetChildren();
            foreach (Item DonationItem in ChildFund)
            {
                if (DonationItem.TemplateName.ToString() == "Donations")
                {
                    XmlElement TagFundingSource = docConfig.CreateElement("FundingSource");
                    ProjectFundingSources.AppendChild(TagFundingSource);

                    XmlElement TagSource = docConfig.CreateElement("mccy_name");
                    XmlText TagSourceValue = docConfig.CreateTextNode(DonationItem["Particulars Of Source"]);
                    TagSource.PrependChild(TagSourceValue);
                    TagFundingSource.AppendChild(TagSource);

                    XmlElement TagFundAmount = docConfig.CreateElement("mccy_fundingamount");
                    XmlText TagFundAmountValue = docConfig.CreateTextNode(DonationItem["Funding Amount"]);
                    TagFundAmount.PrependChild(TagFundAmountValue);
                    TagFundingSource.AppendChild(TagFundAmount);

                    XmlElement TagDuration = docConfig.CreateElement("mccy_fundingdurationnumberofmonths");
                    XmlText TagDurationValue = docConfig.CreateTextNode(DonationItem["Funding Duration"]);
                    TagDuration.PrependChild(TagDurationValue);
                    TagFundingSource.AppendChild(TagDuration);

                    XmlElement TagFundStatus = docConfig.CreateElement("mccy_fundingstatus");
                    XmlText TagFundStatusValue = docConfig.CreateTextNode(DonationItem["Funding Status"]);
                    TagFundStatus.PrependChild(TagFundStatusValue);
                    TagFundingSource.AppendChild(TagFundStatus);
                }
            }


            XmlElement ProjectMileStones = docConfig.CreateElement("Project_Milestones");
            docConfig.DocumentElement.PrependChild(ProjectMileStones);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectMileStones);



            Sitecore.Collections.ChildList ChildTimeLine = FormItem.GetChildren();
            foreach (Item TimeLineItem in ChildTimeLine)
            {
                if (TimeLineItem.TemplateName.ToString() == "Time Line")
                {
                    XmlElement TagProjectMileStone = docConfig.CreateElement("Milestone");
                    ProjectMileStones.AppendChild(TagProjectMileStone);

                    XmlElement TagNameMileStone = docConfig.CreateElement("mccy_name");
                    XmlText TagNameMileStoneValue = docConfig.CreateTextNode(TimeLineItem["Milestones"]);
                    TagNameMileStone.PrependChild(TagNameMileStoneValue);
                    TagProjectMileStone.AppendChild(TagNameMileStone);

                    XmlElement TagActivityEvent = docConfig.CreateElement("mccy_activityevent");
                    XmlText TagActivityEventValue = docConfig.CreateTextNode(TimeLineItem["Event"]);
                    TagActivityEvent.PrependChild(TagActivityEventValue);
                    TagProjectMileStone.AppendChild(TagActivityEvent);

                    XmlElement TagMonitoringRequired = docConfig.CreateElement("mccy_monitoringrequired");
                    XmlText TagMonitoringRequiredValue = docConfig.CreateTextNode("");
                    TagMonitoringRequired.PrependChild(TagMonitoringRequiredValue);
                    TagProjectMileStone.AppendChild(TagMonitoringRequired);

                    XmlElement TagEventDate = docConfig.CreateElement("mccy_schedulemilestonedate");
                    XmlText TagEventDateValue = docConfig.CreateTextNode(TimeLineItem["Date"]);
                    TagEventDate.PrependChild(TagEventDateValue);
                    TagProjectMileStone.AppendChild(TagEventDate);


                }
            }

            XmlElement ProjectAttachedDetails = docConfig.CreateElement("Attachment_details");
            docConfig.DocumentElement.PrependChild(ProjectAttachedDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectAttachedDetails);

            Sitecore.Collections.ChildList ChildFiles = FormItem.GetChildren();
            int fileext = 0;
            foreach (Item ChildFile in ChildFiles)
            {
                if (ChildFile.TemplateName.ToString() == "File Template")
                {
                    XmlElement TagAttachment = docConfig.CreateElement("Have_Attachment");
                    XmlText TagAttachmentValue = docConfig.CreateTextNode("Yes");
                    TagAttachment.PrependChild(TagAttachmentValue);
                    ProjectAttachedDetails.AppendChild(TagAttachment);

                    XmlElement TagAttachmentMain = docConfig.CreateElement("Attachment");
                    ProjectAttachedDetails.AppendChild(TagAttachmentMain);

                    XmlElement TagAttachmnetName = docConfig.CreateElement("Name");
                    string fname = Session["ApplicationName"].ToString() + (fileext + 1) + "-" + dtFiles.Rows[fileext][1].ToString();
                    XmlText TagAttachmnetNameValue = docConfig.CreateTextNode(fname);
                    TagAttachmnetName.PrependChild(TagAttachmnetNameValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetName);

                    XmlElement TagAttachmnetPath = docConfig.CreateElement("Location");
                    Sitecore.Data.Fields.FileField file = ChildFile.Fields["Uploaded File Path"];
                    XmlText TagAttachmnetPathValue = docConfig.CreateTextNode(Server.MapPath("~/SG50/Application Forms/archive/" + Session["ApplicationName"].ToString() + "/" + fname)); //file.MediaItem.Paths.Path.ToString());
                    TagAttachmnetPath.PrependChild(TagAttachmnetPathValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetPath);
                    fileext++;
                }
            }

            bool folderExists = Directory.Exists(Server.MapPath("/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString()));
            if (!folderExists)
            {
                Directory.CreateDirectory(Server.MapPath("/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString()));
            }
            if (dtFiles.Rows.Count > 0)
            {
                for (int i = 0; i < dtFiles.Rows.Count; i++)
                {
                    File.Copy(Server.MapPath("/SG50/Applied Forms/" + dtFiles.Rows[i]["TotalFileName"].ToString()), Server.MapPath("/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + (i + 1) + "-" + dtFiles.Rows[i]["TotalFileName"].ToString()));
                    File.Delete(Server.MapPath("/SG50/Applied Forms/" + dtFiles.Rows[i]["TotalFileName"].ToString()));
                }
            }
            docConfig.Save(Server.MapPath("~/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + "_gen" + ".xml"));
        }

        private void generateXml_Code(Item FormItem)
        {
            XmlDocument docConfig = new XmlDocument();
            XmlNode xmlNode = docConfig.CreateNode(XmlNodeType.XmlDeclaration, "", "");
            XmlElement rootElement = docConfig.CreateElement("new_fundapplication");
            docConfig.AppendChild(rootElement);

            XmlElement hedder = docConfig.CreateElement("Applicant_Details");
            docConfig.DocumentElement.PrependChild(hedder);
            docConfig.ChildNodes.Item(0).AppendChild(hedder);


            XmlElement TagName = docConfig.CreateElement("Applicant1");
            hedder.AppendChild(TagName);

            XmlElement TagSalutation = docConfig.CreateElement("mccy_salutation");
            XmlText tagSalutationValue = docConfig.CreateTextNode(ddlSalutationAppone.SelectedValue);
            TagSalutation.PrependChild(tagSalutationValue);
            TagName.AppendChild(TagSalutation);

            XmlElement TagNric = docConfig.CreateElement("mccy_nameofapplicantasinnric");
            XmlText TagNricValue = docConfig.CreateTextNode(FormItem["Name Of Applicant1"]);
            TagNric.PrependChild(TagNricValue);
            TagName.AppendChild(TagNric);

            XmlElement TagIdtype = docConfig.CreateElement("mccy_idtype");
            XmlText TagIdtypeValue = docConfig.CreateTextNode("SP");
            TagIdtype.PrependChild(TagIdtypeValue);
            TagName.AppendChild(TagIdtype);

            XmlElement TagIdNumber = docConfig.CreateElement("mccy_idnumber");
            XmlText TagIdNumberValue = docConfig.CreateTextNode(FormItem["NRIC Number1"]);
            TagIdNumber.PrependChild(TagIdNumberValue);
            TagName.AppendChild(TagIdNumber);

            XmlElement TagNationality = docConfig.CreateElement("mccy_nationality");
            XmlText TagNationalityValue = docConfig.CreateTextNode("SG");
            TagNationality.PrependChild(TagNationalityValue);
            TagName.AppendChild(TagNationality);

            XmlElement TagOccDesignation = docConfig.CreateElement("mccy_occupationdesignation");
            XmlText TagOccDesignationValue = docConfig.CreateTextNode(FormItem["Occupation1"]);
            TagOccDesignation.PrependChild(TagOccDesignationValue);
            TagName.AppendChild(TagOccDesignation);

            XmlElement TagGender = docConfig.CreateElement("mccy_gender");
            XmlText TagGenderValue = docConfig.CreateTextNode(ddlGenderAppOne.SelectedValue);
            TagGender.PrependChild(TagGenderValue);
            TagName.AppendChild(TagGender);

            XmlElement TagDOB = docConfig.CreateElement("mccy_dateofbirth");
            XmlText TagDOBValue = docConfig.CreateTextNode(FormItem["Date Of Birth1"]);
            TagDOB.PrependChild(TagDOBValue);
            TagName.AppendChild(TagDOB);

            XmlElement TagAddressType1 = docConfig.CreateElement("mccy_addresstype1");
            XmlText TagAddressType1Value = docConfig.CreateTextNode("");
            TagAddressType1.PrependChild(TagAddressType1Value);
            TagName.AppendChild(TagAddressType1);

            XmlElement TagAddress1 = docConfig.CreateElement("mccy_address1");
            XmlText TagAddress1Value = docConfig.CreateTextNode(FormItem["Address1"]);
            TagAddress1.PrependChild(TagAddress1Value);
            TagName.AppendChild(TagAddress1);

            XmlElement TagPostalCode1 = docConfig.CreateElement("mccy_postalcode1");
            XmlText TagPostalCode1Value = docConfig.CreateTextNode(FormItem["Postal Code1"]);
            TagPostalCode1.PrependChild(TagPostalCode1Value);
            TagName.AppendChild(TagPostalCode1);

            XmlElement TagBuildingName1 = docConfig.CreateElement("mccy_buildingname1");
            XmlText TagBuildingName1Value = docConfig.CreateTextNode("");
            TagBuildingName1.PrependChild(TagBuildingName1Value);
            TagName.AppendChild(TagBuildingName1);

            XmlElement TagBlkno1 = docConfig.CreateElement("mccy_blkhseno1");
            XmlText TagBlkno1Value = docConfig.CreateTextNode("");
            TagBlkno1.PrependChild(TagBlkno1Value);
            TagName.AppendChild(TagBlkno1);

            XmlElement TagStreetName1 = docConfig.CreateElement("mccy_streetname1");
            XmlText TagStreetName1Value = docConfig.CreateTextNode("");
            TagStreetName1.PrependChild(TagStreetName1Value);
            TagName.AppendChild(TagStreetName1);

            XmlElement TagFloorno1 = docConfig.CreateElement("mccy_floorno1");
            XmlText TagFloorno1Value = docConfig.CreateTextNode("");
            TagFloorno1.PrependChild(TagFloorno1Value);
            TagName.AppendChild(TagFloorno1);

            XmlElement TagUnit1 = docConfig.CreateElement("mccy_unitno1");
            XmlText TagUnit1Value = docConfig.CreateTextNode("");
            TagUnit1.PrependChild(TagUnit1Value);
            TagName.AppendChild(TagUnit1);

            XmlElement TagOverAddressline1 = docConfig.CreateElement("mccy_overseaaddressline1");
            XmlText TagOverAddressline1Value = docConfig.CreateTextNode("");
            TagOverAddressline1.PrependChild(TagOverAddressline1Value);
            TagName.AppendChild(TagOverAddressline1);

            XmlElement TagOverAddressline2 = docConfig.CreateElement("mccy_overseaaddressline2");
            XmlText TagOverAddressline2Value = docConfig.CreateTextNode("");
            TagOverAddressline2.PrependChild(TagOverAddressline2Value);
            TagName.AppendChild(TagOverAddressline2);

            XmlElement TagHomePhone = docConfig.CreateElement("mccy_homephone");
            XmlText TagHomePhoneValue = docConfig.CreateTextNode(FormItem["Home Number1"]);
            TagHomePhone.PrependChild(TagHomePhoneValue);
            TagName.AppendChild(TagHomePhone);

            XmlElement TagMobilePhone = docConfig.CreateElement("mccy_mobilephone");
            XmlText TagMobilePhoneValue = docConfig.CreateTextNode(FormItem["MobileNumber1"]);
            TagMobilePhone.PrependChild(TagMobilePhoneValue);
            TagName.AppendChild(TagMobilePhone);

            XmlElement TagEmail = docConfig.CreateElement("mccy_email");
            XmlText TagEmailValue = docConfig.CreateTextNode(FormItem["Email Address1"]);
            TagEmail.PrependChild(TagEmailValue);
            TagName.AppendChild(TagEmail);



            XmlElement TagName2 = docConfig.CreateElement("Applicant2");
            hedder.AppendChild(TagName2);

            XmlElement TagSalutation2 = docConfig.CreateElement("mccy_salutation2");
            XmlText tagSalutation2Value = docConfig.CreateTextNode(FormItem["Salutation2"]);
            TagSalutation2.PrependChild(tagSalutation2Value);
            TagName2.AppendChild(TagSalutation2);

            XmlElement TagNric2 = docConfig.CreateElement("mccy_nameofapplicant2asinnricpassport");
            XmlText TagNric2Value = docConfig.CreateTextNode(FormItem["Name Of Applicant2"]);
            TagNric2.PrependChild(TagNric2Value);
            TagName2.AppendChild(TagNric2);

            XmlElement TagIdtype2 = docConfig.CreateElement("mccy_idtype2");
            XmlText TagIdtype2Value = docConfig.CreateTextNode(FormItem["Id Type2"]);
            TagIdtype2.PrependChild(TagIdtype2Value);
            TagName2.AppendChild(TagIdtype2);

            XmlElement TagIdNumber2 = docConfig.CreateElement("mccy_idnumber2");
            XmlText TagIdNumber2Value = docConfig.CreateTextNode(FormItem["Id Number2"]);
            TagIdNumber2.PrependChild(TagIdNumber2Value);
            TagName2.AppendChild(TagIdNumber2);

            XmlElement TagPassport = docConfig.CreateElement("mccy_passportno");
            XmlText TagPassportValue = docConfig.CreateTextNode(FormItem["PassportNo"]);
            TagPassport.PrependChild(TagPassportValue);
            TagName2.AppendChild(TagPassport);

            XmlElement TagNationality2 = docConfig.CreateElement("mccy_nationality2");
            XmlText TagNationality2Value = docConfig.CreateTextNode(FormItem["Nationality2"]);
            TagNationality2.PrependChild(TagNationality2Value);
            TagName2.AppendChild(TagNationality2);

            XmlElement TagOccDesignation2 = docConfig.CreateElement("mccy_occupationdesignation2");
            XmlText TagOccDesignation2Value = docConfig.CreateTextNode(FormItem["Occupation2"]);
            TagOccDesignation2.PrependChild(TagOccDesignation2Value);
            TagName2.AppendChild(TagOccDesignation2);

            XmlElement TagGender2 = docConfig.CreateElement("mccy_gender2");
            XmlText TagGender2Value = docConfig.CreateTextNode(FormItem["Gender2"]);
            TagGender2.PrependChild(TagGender2Value);
            TagName2.AppendChild(TagGender2);

            XmlElement TagDOB2 = docConfig.CreateElement("mccy_dateofbirth2");
            XmlText TagDOB2Value = docConfig.CreateTextNode(FormItem["Date Of Birth2"]);
            TagDOB2.PrependChild(TagDOB2Value);
            TagName2.AppendChild(TagDOB2);

            XmlElement TagAddressType2 = docConfig.CreateElement("mccy_addresstype2");
            XmlText TagAddressType2Value = docConfig.CreateTextNode(FormItem["Address Type2"]);
            TagAddressType2.PrependChild(TagAddressType2Value);
            TagName2.AppendChild(TagAddressType2);

            XmlElement TagAddress2 = docConfig.CreateElement("mccy_address2");
            XmlText TagAddress2Value = docConfig.CreateTextNode(FormItem["Address2"]);
            TagAddress2.PrependChild(TagAddress2Value);
            TagName2.AppendChild(TagAddress2);

            XmlElement TagPostalCode2 = docConfig.CreateElement("mccy_postalcode2");
            XmlText TagPostalCode2Value = docConfig.CreateTextNode(FormItem["Postal Code2"]);
            TagPostalCode2.PrependChild(TagPostalCode2Value);
            TagName2.AppendChild(TagPostalCode2);

            XmlElement TagBuildingName2 = docConfig.CreateElement("mccy_buildingname2");
            XmlText TagBuildingName2Value = docConfig.CreateTextNode("");
            TagBuildingName2.PrependChild(TagBuildingName2Value);
            TagName2.AppendChild(TagBuildingName2);

            XmlElement TagBlkno2 = docConfig.CreateElement("mccy_blkhseno2");
            XmlText TagBlkno2Value = docConfig.CreateTextNode("");
            TagBlkno2.PrependChild(TagBlkno2Value);
            TagName2.AppendChild(TagBlkno2);

            XmlElement TagStreetName2 = docConfig.CreateElement("mccy_streetname2");
            XmlText TagStreetName2Value = docConfig.CreateTextNode("");
            TagStreetName2.PrependChild(TagStreetName2Value);
            TagName2.AppendChild(TagStreetName2);

            XmlElement TagFloorno2 = docConfig.CreateElement("mccy_floorno2");
            XmlText TagFloorno2Value = docConfig.CreateTextNode("");
            TagFloorno2.PrependChild(TagFloorno2Value);
            TagName2.AppendChild(TagFloorno2);

            XmlElement TagUnit2 = docConfig.CreateElement("mccy_unitno2");
            XmlText TagUnit2Value = docConfig.CreateTextNode("");
            TagUnit2.PrependChild(TagUnit2Value);
            TagName2.AppendChild(TagUnit2);

            XmlElement TagOverAddressline2_1 = docConfig.CreateElement("mccy_overseaaddressline1_2");
            XmlText TagOverAddressline2_1Value = docConfig.CreateTextNode("");
            TagOverAddressline2_1.PrependChild(TagOverAddressline2_1Value);
            TagName2.AppendChild(TagOverAddressline2_1);

            XmlElement TagOverAddressline2_2 = docConfig.CreateElement("mccy_overseaaddressline2_1");
            XmlText TagOverAddressline2_2Value = docConfig.CreateTextNode("");
            TagOverAddressline2_2.PrependChild(TagOverAddressline2_2Value);
            TagName2.AppendChild(TagOverAddressline2_2);

            XmlElement TagHomePhone2 = docConfig.CreateElement("mccy_homephone2");
            XmlText TagHomePhone2Value = docConfig.CreateTextNode(FormItem["Phione Number2"]);
            TagHomePhone2.PrependChild(TagHomePhone2Value);
            TagName2.AppendChild(TagHomePhone2);

            XmlElement TagMobilePhone2 = docConfig.CreateElement("mccy_mobilephone2");
            XmlText TagMobilePhone2Value = docConfig.CreateTextNode(FormItem["MobileNumber2"]);
            TagMobilePhone2.PrependChild(TagMobilePhone2Value);
            TagName2.AppendChild(TagMobilePhone2);

            XmlElement TagEmail2 = docConfig.CreateElement("mccy_email2");
            XmlText TagEmail2Value = docConfig.CreateTextNode(FormItem["Email Address2"]);
            TagEmail2.PrependChild(TagEmail2Value);
            TagName2.AppendChild(TagEmail2);



            XmlElement Organizationhedder = docConfig.CreateElement("Organisation_Details");
            docConfig.DocumentElement.PrependChild(Organizationhedder);
            docConfig.ChildNodes.Item(0).AppendChild(Organizationhedder);



            XmlElement TagOraganization = docConfig.CreateElement("Organisation");
            Organizationhedder.AppendChild(TagOraganization);

            XmlElement TagOraganizationName = docConfig.CreateElement("mccy_nameoforganisation");
            XmlText OraganizationNameValue = docConfig.CreateTextNode(FormItem["Name Of Organisation"]);
            TagOraganizationName.PrependChild(OraganizationNameValue);
            TagOraganization.AppendChild(TagOraganizationName);


            XmlElement Taguen = docConfig.CreateElement("mccy_uen");
            XmlText TaguenValue = docConfig.CreateTextNode(FormItem["UEN"]);
            Taguen.PrependChild(TaguenValue);
            TagOraganization.AppendChild(Taguen);


            XmlElement TagOraganizationType = docConfig.CreateElement("mccy_organisationtype");
            XmlText TagOraganizationTypeValue = docConfig.CreateTextNode(ddlOrgType.SelectedValue);
            TagOraganizationType.PrependChild(TagOraganizationTypeValue);
            TagOraganization.AppendChild(TagOraganizationType);

            XmlElement TagNatureOfBussiness = docConfig.CreateElement("mccy_natureofbusiness");
            XmlText TagNatureOfBussinessValue = docConfig.CreateTextNode(FormItem["Nature Of Business"]);
            TagNatureOfBussiness.PrependChild(TagNatureOfBussinessValue);
            TagOraganization.AppendChild(TagNatureOfBussiness);

            XmlElement TagFoundingYear = docConfig.CreateElement("mccy_foundingyr");
            XmlText TagFoundingYearValue = docConfig.CreateTextNode(FormItem["Founding Year"]);
            TagFoundingYear.PrependChild(TagFoundingYearValue);
            TagOraganization.AppendChild(TagFoundingYear);

            XmlElement TagNoOfEmployees = docConfig.CreateElement("mccy_numberofemployees");
            XmlText TagNoOfEmployeesValue = docConfig.CreateTextNode(FormItem["Number Of Employees"]);
            TagNoOfEmployees.PrependChild(TagNoOfEmployeesValue);
            TagOraganization.AppendChild(TagNoOfEmployees);

            XmlElement TagOrgAddressType = docConfig.CreateElement("mccy_addresstype");
            XmlText TagOrgAddressTypeValue = docConfig.CreateTextNode("1");
            TagOrgAddressType.PrependChild(TagOrgAddressTypeValue);
            TagOraganization.AppendChild(TagOrgAddressType);

            XmlElement TagOrgAddress = docConfig.CreateElement("mccy_address");
            XmlText TagOrgAddressValue = docConfig.CreateTextNode(FormItem["Organisation Address"]);
            TagOrgAddress.PrependChild(TagOrgAddressValue);
            TagOraganization.AppendChild(TagOrgAddress);

            XmlElement TagOrgPostalCode = docConfig.CreateElement("mccy_postalcode");
            XmlText TagPostalCodeValue = docConfig.CreateTextNode(FormItem["Organisation postalcode"]);
            TagOrgPostalCode.PrependChild(TagPostalCodeValue);
            TagOraganization.AppendChild(TagOrgPostalCode);

            XmlElement TagOrgBulidingName = docConfig.CreateElement("mccy_buildingname");
            XmlText TagOrgBulidingNameValue = docConfig.CreateTextNode("");
            TagOrgBulidingName.PrependChild(TagOrgBulidingNameValue);
            TagOraganization.AppendChild(TagOrgBulidingName);

            XmlElement TagOrgBlkNo = docConfig.CreateElement("mccy_blkhseno");
            XmlText TagOrgBlkNoValue = docConfig.CreateTextNode("");
            TagOrgBlkNo.PrependChild(TagOrgBlkNoValue);
            TagOraganization.AppendChild(TagOrgBlkNo);

            XmlElement TagOrgStreetName = docConfig.CreateElement("mccy_streetname");
            XmlText TagOrgStreetNameValue = docConfig.CreateTextNode("");
            TagOrgStreetName.PrependChild(TagOrgStreetNameValue);
            TagOraganization.AppendChild(TagOrgStreetName);


            XmlElement TagOrgFloorNo = docConfig.CreateElement("mccy_floorno");
            XmlText TagOrgFloorNoValue = docConfig.CreateTextNode("");
            TagOrgFloorNo.PrependChild(TagOrgFloorNoValue);
            TagOraganization.AppendChild(TagOrgFloorNo);

            XmlElement TagOrgUnitNo = docConfig.CreateElement("mccy_unitno");
            XmlText TagOrgUnitNoValue = docConfig.CreateTextNode("");
            TagOrgUnitNo.PrependChild(TagOrgUnitNoValue);
            TagOraganization.AppendChild(TagOrgUnitNo);

            XmlElement TagOrgAddressLine1 = docConfig.CreateElement("mccy_overseaaddressline1_1");
            XmlText TagOrgAddressLine1Value = docConfig.CreateTextNode("");
            TagOrgAddressLine1.PrependChild(TagOrgAddressLine1Value);
            TagOraganization.AppendChild(TagOrgAddressLine1);

            XmlElement TagOrgAddressLine2 = docConfig.CreateElement("mccy_overseaaddressline2_2");
            XmlText TagOrgAddressLine2Value = docConfig.CreateTextNode("");
            TagOrgAddressLine2.PrependChild(TagOrgAddressLine2Value);
            TagOraganization.AppendChild(TagOrgAddressLine2);

            XmlElement TagOrgPhone = docConfig.CreateElement("mccy_phonenumber");
            XmlText TagOrgPhoneValue = docConfig.CreateTextNode(FormItem["Organisation Phone Number"]);
            TagOrgPhone.PrependChild(TagOrgPhoneValue);
            TagOraganization.AppendChild(TagOrgPhone);

            XmlElement TagOrgEmail = docConfig.CreateElement("mccy_emailaddress");
            XmlText TagOrgEmailValue = docConfig.CreateTextNode(FormItem["Organisation Email Address"]);
            TagOrgEmail.PrependChild(TagOrgEmailValue);
            TagOraganization.AppendChild(TagOrgEmail);




            XmlElement ProjectDetails = docConfig.CreateElement("Project_Details");
            docConfig.DocumentElement.PrependChild(ProjectDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectDetails);

            string SportSEA = string.Empty;
            if (Session["SEA"] != null)
                SportSEA = "2";
            else
                SportSEA = "1";


            XmlElement TagProjectDescription = docConfig.CreateElement("Project_Description");
            ProjectDetails.AppendChild(TagProjectDescription);

            XmlElement TagApplicationType = docConfig.CreateElement("mccy_applicationtype");
            XmlText TagApplicationTypeValue = docConfig.CreateTextNode(SportSEA);
            TagApplicationType.PrependChild(TagApplicationTypeValue);
            TagProjectDescription.AppendChild(TagApplicationType);

            XmlElement TagProjectTitle = docConfig.CreateElement("mccy_projecttitle");
            XmlText TagProjectTitleValue = docConfig.CreateTextNode(FormItem["Project Title"]);
            TagProjectTitle.PrependChild(TagProjectTitleValue);
            TagProjectDescription.AppendChild(TagProjectTitle);

            XmlElement TagProjectObjectives = docConfig.CreateElement("mccy_projectobjectives");
            XmlText TagProjectObjectivesValue = docConfig.CreateTextNode(FormItem["Project Objectives"]);
            TagProjectObjectives.PrependChild(TagProjectObjectivesValue);
            TagProjectDescription.AppendChild(TagProjectObjectives);

            XmlElement TagIntendedParticipants = docConfig.CreateElement("mccy_intendedparticipantsbeneficiaries");
            XmlText TagIntendedParticipantsValue = docConfig.CreateTextNode(FormItem["Intended Participants"]);
            TagIntendedParticipants.PrependChild(TagIntendedParticipantsValue);
            TagProjectDescription.AppendChild(TagIntendedParticipants);

            XmlElement TagEstimatedParticipants = docConfig.CreateElement("mccy_estimatedtotalnoofparticipantsbenefit");
            XmlText TagEstimatedParticipantsValue = docConfig.CreateTextNode(FormItem["Esitimated NoOfparticipants"]);
            TagEstimatedParticipants.PrependChild(TagEstimatedParticipantsValue);
            TagProjectDescription.AppendChild(TagEstimatedParticipants);


            XmlElement TagProjectBudget = docConfig.CreateElement("Project_Budget");
            ProjectDetails.AppendChild(TagProjectBudget);

            XmlElement TagAmountRequested = docConfig.CreateElement("mccy_amountrequested");
            XmlText TagAmountRequestedValue = docConfig.CreateTextNode(FormItem["Amount Requested"]);
            TagAmountRequested.PrependChild(TagAmountRequestedValue);
            TagProjectBudget.AppendChild(TagAmountRequested);

            XmlElement TagProjectedExpenditure = docConfig.CreateElement("mccy_totalprojectedexpenditure");
            XmlText TagProjectedExpenditureValue = docConfig.CreateTextNode(FormItem["Projected Expenditure"]);
            TagProjectedExpenditure.PrependChild(TagProjectedExpenditureValue);
            TagProjectBudget.AppendChild(TagProjectedExpenditure);


            XmlElement Taganyfundraisingcomponent = docConfig.CreateElement("mccy_anyfundraisingcomponent");
            XmlText TaganyfundraisingcomponentValue = docConfig.CreateTextNode("");
            Taganyfundraisingcomponent.PrependChild(TaganyfundraisingcomponentValue);
            TagProjectBudget.AppendChild(Taganyfundraisingcomponent);

            XmlElement TagRelevantProjectExperience = docConfig.CreateElement("mccy_relevantprojectexperiencs");
            XmlText TagRelevantProjectExperienceValue = docConfig.CreateTextNode(FormItem["Relevant Project Experience"]);
            TagRelevantProjectExperience.PrependChild(TagRelevantProjectExperienceValue);
            TagProjectBudget.AppendChild(TagRelevantProjectExperience);



            XmlElement TagProjectTimeLine = docConfig.CreateElement("Proposed_Project_Timeline");
            ProjectDetails.AppendChild(TagProjectTimeLine);

            XmlElement TagProjectStartDate = docConfig.CreateElement("mccy_proposedstartdate");
            XmlText TagProjectStartDateValue = docConfig.CreateTextNode(FormItem["Start Date"]);
            TagProjectStartDate.PrependChild(TagProjectStartDateValue);
            TagProjectTimeLine.AppendChild(TagProjectStartDate);

            XmlElement TagProjectEndDate = docConfig.CreateElement("mccy_proposedenddate");
            XmlText TagProjectEndDateValue = docConfig.CreateTextNode(FormItem["End Date"]);
            TagProjectEndDate.PrependChild(TagProjectEndDateValue);
            TagProjectTimeLine.AppendChild(TagProjectEndDate);


            XmlElement TagProjectDeclaration = docConfig.CreateElement("Project_Declaration");
            ProjectDetails.AppendChild(TagProjectDeclaration);

            XmlElement Tagmccydeclarantname = docConfig.CreateElement("mccy_declarantname");
            XmlText TagmccydeclarantnameValue = docConfig.CreateTextNode("");
            Tagmccydeclarantname.PrependChild(TagmccydeclarantnameValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarantname);

            XmlElement Tagmccydeclarationdate = docConfig.CreateElement("mccy_declarationdate");
            XmlText TagmccydeclarationdateValue = docConfig.CreateTextNode("");
            Tagmccydeclarationdate.PrependChild(TagmccydeclarationdateValue);
            TagProjectDeclaration.AppendChild(Tagmccydeclarationdate);

            XmlElement ProjectExpenditures = docConfig.CreateElement("Project_Expenditures");
            docConfig.DocumentElement.PrependChild(ProjectExpenditures);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectExpenditures);



            Sitecore.Collections.ChildList ChildBudget = FormItem.GetChildren();

            foreach (Item BudgetItem in ChildBudget)
            {
                if (BudgetItem.TemplateName.ToString() == "Project Fund")
                {
                    XmlElement TagProjectExpenditure = docConfig.CreateElement("Expenditure");
                    ProjectExpenditures.AppendChild(TagProjectExpenditure);

                    XmlElement TagExpenditure = docConfig.CreateElement("mccy_name");
                    XmlText TagExpenditureValue = docConfig.CreateTextNode(BudgetItem["Item"]);
                    TagExpenditure.PrependChild(TagExpenditureValue);
                    TagProjectExpenditure.AppendChild(TagExpenditure);

                    XmlElement TagFund = docConfig.CreateElement("mccy_projectedexpenditure");
                    XmlText TagFundValue = docConfig.CreateTextNode(BudgetItem["Fund Amount"]);
                    TagFund.PrependChild(TagFundValue);
                    TagProjectExpenditure.AppendChild(TagFund);

                }
            }



            XmlElement ProjectFundingSources = docConfig.CreateElement("Project_FundingSources");
            docConfig.DocumentElement.PrependChild(ProjectFundingSources);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectFundingSources);



            Sitecore.Collections.ChildList ChildFund = FormItem.GetChildren();
            foreach (Item DonationItem in ChildFund)
            {
                if (DonationItem.TemplateName.ToString() == "Donations")
                {
                    XmlElement TagFundingSource = docConfig.CreateElement("FundingSource");
                    ProjectFundingSources.AppendChild(TagFundingSource);

                    XmlElement TagSource = docConfig.CreateElement("mccy_name");
                    XmlText TagSourceValue = docConfig.CreateTextNode(DonationItem["Particulars Of Source"]);
                    TagSource.PrependChild(TagSourceValue);
                    TagFundingSource.AppendChild(TagSource);

                    XmlElement TagFundAmount = docConfig.CreateElement("mccy_fundingamount");
                    XmlText TagFundAmountValue = docConfig.CreateTextNode(DonationItem["Funding Amount"]);
                    TagFundAmount.PrependChild(TagFundAmountValue);
                    TagFundingSource.AppendChild(TagFundAmount);

                    XmlElement TagDuration = docConfig.CreateElement("mccy_fundingdurationnumberofmonths");
                    XmlText TagDurationValue = docConfig.CreateTextNode(DonationItem["Funding Duration"]);
                    TagDuration.PrependChild(TagDurationValue);
                    TagFundingSource.AppendChild(TagDuration);

                    string Funding_Status = string.Empty;
                    if (DonationItem["Funding Status"] == "Confirmed")
                        Funding_Status = "2";
                    else
                        Funding_Status = "1";

                    XmlElement TagFundStatus = docConfig.CreateElement("mccy_fundingstatus");
                    XmlText TagFundStatusValue = docConfig.CreateTextNode(Funding_Status);
                    TagFundStatus.PrependChild(TagFundStatusValue);
                    TagFundingSource.AppendChild(TagFundStatus);
                }
            }


            XmlElement ProjectMileStones = docConfig.CreateElement("Project_Milestones");
            docConfig.DocumentElement.PrependChild(ProjectMileStones);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectMileStones);



            Sitecore.Collections.ChildList ChildTimeLine = FormItem.GetChildren();
            foreach (Item TimeLineItem in ChildTimeLine)
            {
                if (TimeLineItem.TemplateName.ToString() == "Time Line")
                {
                    XmlElement TagProjectMileStone = docConfig.CreateElement("Milestone");
                    ProjectMileStones.AppendChild(TagProjectMileStone);

                    XmlElement TagNameMileStone = docConfig.CreateElement("mccy_name");
                    XmlText TagNameMileStoneValue = docConfig.CreateTextNode(TimeLineItem["Milestones"]);
                    TagNameMileStone.PrependChild(TagNameMileStoneValue);
                    TagProjectMileStone.AppendChild(TagNameMileStone);

                    XmlElement TagActivityEvent = docConfig.CreateElement("mccy_activityevent");
                    XmlText TagActivityEventValue = docConfig.CreateTextNode(TimeLineItem["Event"]);
                    TagActivityEvent.PrependChild(TagActivityEventValue);
                    TagProjectMileStone.AppendChild(TagActivityEvent);

                    XmlElement TagMonitoringRequired = docConfig.CreateElement("mccy_monitoringrequired");
                    XmlText TagMonitoringRequiredValue = docConfig.CreateTextNode("");
                    TagMonitoringRequired.PrependChild(TagMonitoringRequiredValue);
                    TagProjectMileStone.AppendChild(TagMonitoringRequired);

                    XmlElement TagEventDate = docConfig.CreateElement("mccy_schedulemilestonedate");
                    XmlText TagEventDateValue = docConfig.CreateTextNode(TimeLineItem["Date"]);
                    TagEventDate.PrependChild(TagEventDateValue);
                    TagProjectMileStone.AppendChild(TagEventDate);


                }
            }

            XmlElement ProjectAttachedDetails = docConfig.CreateElement("Attachment_details");
            docConfig.DocumentElement.PrependChild(ProjectAttachedDetails);
            docConfig.ChildNodes.Item(0).AppendChild(ProjectAttachedDetails);

            Sitecore.Collections.ChildList ChildFiles = FormItem.GetChildren();
            int fileext = 0;
            foreach (Item ChildFile in ChildFiles)
            {
                if (ChildFile.TemplateName.ToString() == "File Template")
                {
                    XmlElement TagAttachment = docConfig.CreateElement("Have_Attachment");
                    XmlText TagAttachmentValue = docConfig.CreateTextNode("Yes");
                    TagAttachment.PrependChild(TagAttachmentValue);
                    ProjectAttachedDetails.AppendChild(TagAttachment);

                    XmlElement TagAttachmentMain = docConfig.CreateElement("Attachment");
                    ProjectAttachedDetails.AppendChild(TagAttachmentMain);

                    XmlElement TagAttachmnetName = docConfig.CreateElement("Name");
                    string fname = Session["ApplicationName"].ToString() + (fileext + 1) + "-" + dtFiles.Rows[fileext][1].ToString();
                    XmlText TagAttachmnetNameValue = docConfig.CreateTextNode(fname);
                    TagAttachmnetName.PrependChild(TagAttachmnetNameValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetName);

                    XmlElement TagAttachmnetPath = docConfig.CreateElement("Location");
                    Sitecore.Data.Fields.FileField file = ChildFile.Fields["Uploaded File Path"];
                    XmlText TagAttachmnetPathValue = docConfig.CreateTextNode(Server.MapPath("~/SG50/Application Forms/archive/" + Session["ApplicationName"].ToString() + "/" + fname)); //file.MediaItem.Paths.Path.ToString()
                    TagAttachmnetPath.PrependChild(TagAttachmnetPathValue);
                    TagAttachmentMain.AppendChild(TagAttachmnetPath);
                    fileext++;
                }
            }

            docConfig.Save(Server.MapPath("~/SG50/Application Forms/upload/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + ".xml"));
        }

        //public  void send_enquiry_confirmation(string email_subject, string ApplicationNo, string email_template, string emailto, string AppName)
        //{
        //    Item itemPress = SG50Class.web.GetItem(SG50Class.str_Email_Template_ID);
        //    string emailfrom = itemPress["Sender Id"];
        //    string CCList = itemPress["CC List"];
        //    string Host = itemPress["Host"];

        //    string FILENAME = HttpContext.Current.Server.MapPath(email_template);

        //    StreamReader objStreamReader = default(StreamReader);
        //    objStreamReader = File.OpenText(FILENAME);


        //    string contents = null;
        //    contents = objStreamReader.ReadToEnd();


        //    string path = hostName + "/~/media/SG50/Applied Forms/";

        //    //contents = contents.Replace("XXXXXX", ApplicationNo + DateTime.Now.ToString("yyMMddhhmmss"));

        //    contents = contents.Replace("[name]", AppName);

        //    //if (!string.IsNullOrEmpty(fileName[0]))
        //    //{
        //    //    contents = contents.Replace("[Link1]", path + fileName[0] + ".ashx");
        //    //    contents = contents.Replace("[this link]", "this link");
        //    //}
        //    //else
        //    //{
        //    //    contents = contents.Replace("[this link]", "");
        //    //}

        //    //if (!string.IsNullOrEmpty(fileName[1]))
        //    //{
        //    //    contents = contents.Replace("[Link2]", path + fileName[1] + ".ashx");
        //    //    contents = contents.Replace("[,link2]", ", link2");
        //    //}
        //    //else
        //    //{
        //    //    contents = contents.Replace("[,link2]", "");
        //    //}

        //    //if (!string.IsNullOrEmpty(fileName[2]))
        //    //{
        //    //    contents = contents.Replace("[Link3]", path + fileName[2] + ".ashx");
        //    //    contents = contents.Replace("[,link3]", ", link3");
        //    //}
        //    //else
        //    //{
        //    //    contents = contents.Replace("[,link3]", "");
        //    //}


        //    //if (!string.IsNullOrEmpty(fileName[3]))
        //    //{
        //    //    contents = contents.Replace("[Link4]", path + fileName[3] + ".ashx");
        //    //    contents = contents.Replace("[,link4]", ", link4");
        //    //}
        //    //else
        //    //{
        //    //    contents = contents.Replace("[,link4]", "");
        //    //}


        //    //if (!string.IsNullOrEmpty(fileName[4]))
        //    //{
        //    //    contents = contents.Replace("[Link5]", path + fileName[4] + ".ashx");
        //    //    contents = contents.Replace("[,link5]", ", link5");
        //    //}
        //    //else
        //    //{
        //    //    contents = contents.Replace("[,link5]", "");
        //    //}

        //    MailMessage enquiry_mail = new MailMessage();

        //    Attachment at = new Attachment(HttpContext.Current.Server.MapPath("~/SG50/Application Forms/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + ".docx"));
        //     enquiry_mail.Attachments.Add(at);

        //     if (dtFiles.Rows.Count > 0)
        //     {
        //         for (int i = 0; i < dtFiles.Rows.Count; i++)
        //         {
        //             Attachment at1 = new Attachment(HttpContext.Current.Server.MapPath("~/SG50/Application Forms/" + Session["ApplicationName"].ToString() + "/Attachments/" + dtFiles.Rows[i]["TotalFileName"].ToString()));
        //             enquiry_mail.Attachments.Add(at1);
        //         }
        //     }

        //     Attachment atPDF = new Attachment(HttpContext.Current.Server.MapPath("~/SG50/Application Forms/" + Session["ApplicationName"].ToString() + "/" + Session["ApplicationName"].ToString() + ".pdf"));
        //     enquiry_mail.Attachments.Add(atPDF);

        //    MailAddress mailto = default(MailAddress);
        //    mailto = new MailAddress(emailto);
        //    enquiry_mail.To.Add(mailto);

        //    MailAddress mailfrom = default(MailAddress);
        //    mailfrom = new MailAddress(emailfrom);

        //    string[] words = CCList.Split(';');
        //    foreach (string cc in words)
        //    {
        //        MailAddress mailcc = default(MailAddress);
        //        mailcc = new MailAddress(cc);
        //        enquiry_mail.CC.Add(cc);
        //    }

        //    //MailAddress mailBcc = default(MailAddress);
        //    //mailBcc = new MailAddress("SG50CelebrationFund@Singapore50.sg");
        //    //enquiry_mail.Bcc.Add(mailBcc);

        //    enquiry_mail.From = mailfrom;
        //    enquiry_mail.BodyEncoding = Encoding.UTF8;
        //    enquiry_mail.IsBodyHtml = true;
        //    enquiry_mail.Subject = email_subject;
        //    enquiry_mail.Body = contents;

        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host =  Host;
        //    smtp.Credentials = new System.Net.NetworkCredential("wstest@websynergies.in", "W$te$t999");
        //    smtp.Send(enquiry_mail);

        //    //objStreamReader = null;
        //}

        protected void btnStepBack_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string url = "~/SG50/Step2.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string url = "~/SG50/Apply For Funding.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string url = "~/SG50/Step2.aspx";
                if (!(url.Contains("://")))
                    Response.Redirect(url);
            }
        }
        private void GetCaptchaimage()
        {

            cgen = new CaptchaGenerator();
            string strpath = Server.MapPath("/SG50/images/PG/") + "CaptchaImage.gif";
            cgen.ResetCaptchaColor();

            txt_CaptchaVal.Text = cgen.GenerateCaptcha(strpath);


            string strpath2 = "/SG50/layouts/PG/sublayouts/" + "CaptchaImage.ashx?id=" + strpath;
            Image1.ImageUrl = strpath2;



        }
        protected void Button1_Click1(object sender, ImageClickEventArgs e)
        {

            cgen = new CaptchaGenerator();
            GetCaptchaimage();
        }
    }
}