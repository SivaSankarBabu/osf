﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using data = Sitecore.Data.Fields;


namespace Layouts.Compaigncommunitysublayout
{

    /// <summary>
    /// Summary description for CompaigncommunitysublayoutSublayout
    /// </summary>
    public partial class CompaigncommunitysublayoutSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);

        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCampaignStories();
                BindUserStories(0, 12);
                BindMobileCampaignStories();
            }
            // Put user code to initialize the page here
        }

        /* Bind Campaign Stories */
        public void BindCampaignStories()
        {

            Item CampaignItem = web.GetItem("{2C6775E7-59C8-41BF-AC77-1716C3DBDAEE}");
            if (CampaignItem != null)
            {
                Random rnd = new Random();
                ChildList CampaignBanners;
                StringBuilder sbBannerText = new StringBuilder();
                StringBuilder sbBannerTextTopStory = new StringBuilder();
                CampaignBanners = CampaignItem.GetChildren();
                int nextNumber = rnd.Next(1, 4);
                if (CampaignBanners != null && CampaignBanners.Count > 0)
                {

                    int BannerCount = 0;
                    sbBannerTextTopStory.Append("<div class='count custom-align banner-button-align'>");
                    sbBannerTextTopStory.Append("<div><span class='time'></span> <span class='views'></span></div>");
                    string TopStoryurl = string.Empty;

                    TopStoryurl = LinkManager.GetItemUrl(CampaignBanners[nextNumber]).ToString();
                    sbBannerTextTopStory.Append("<br/><a href='" + TopStoryurl + "' class='main-viewstory btn-hover-white'>Full story</a> </div>");
                    //hdnRandom.Value = CampaignBanners[nextNumber].Fields["Banner Video URL"].Value.ToString();
                    Session["Banner Video URL"] = CampaignBanners[nextNumber].Fields["Banner Video URL"].Value.ToString();

                    string TopStorymediaUrl = string.Empty;
                    string TopStorymediaAlt = string.Empty;
                    Sitecore.Data.Fields.ImageField TopStoryimgField = ((Sitecore.Data.Fields.ImageField)CampaignBanners[nextNumber].Fields["Banner Image"]);
                    if (TopStoryimgField != null && !string.IsNullOrEmpty(TopStoryimgField.Src) && TopStoryimgField.MediaItem != null)
                    {
                        TopStorymediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(TopStoryimgField.MediaItem);
                        TopStorymediaAlt = TopStoryimgField.Alt;
                    }
                    sbBannerTextTopStory.Append("<img src='" + TopStorymediaUrl + "' alt='" + TopStorymediaAlt + "' />");

                    DvCampaignTopStory.InnerHtml = sbBannerTextTopStory.ToString();
                    for (int i = 0; i < CampaignBanners.Count; i++)
                    {
                        if (CampaignBanners[i].Versions.Count > 0 && BannerCount <= 3)
                        {
                            if (i != nextNumber)
                            {
                                sbBannerText.Append("<li>");
                                sbBannerText.Append("<div class='image-top-text'>");
                                string Title = string.Empty;
                                Title = CampaignBanners[i].Fields["Title"].ToString();
                                sbBannerText.Append("<h3></h3><br/>");
                                sbBannerText.Append("<div class='count custom-align show-for-small'><span class='time'>3:42</span><span class='views'>22, 549 views</span></div>");
                                BannerCount++;
                                string url = string.Empty;
                                url = LinkManager.GetItemUrl(CampaignBanners[i]).ToString();
                                if (!string.IsNullOrEmpty(CampaignBanners[i]["Button Text"].ToString()))
                                {

                                    if (CampaignBanners[i].Name != "Campaign Story 4")
                                        sbBannerText.Append("<a href='" + url + "' class='viewstory btn-hover-white'>" + CampaignBanners[i]["Button Text"] + "</a></div>");
                                    else
                                        sbBannerText.Append("<a href='https://www.singapore50.sg/SG50/Pioneer%20Generation.aspx' target='_blank'  class='viewstory btn-hover-white'>" + CampaignBanners[i]["Button Text"] + "</a></div>");


                                }
                                else
                                {
                                    sbBannerText.Append("</div>");
                                }
                                string BannermediaUrl = string.Empty;
                                string BannermediaAlt = string.Empty;
                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)CampaignBanners[i].Fields["Story Image"]);
                                if (imgField != null && !string.IsNullOrEmpty(imgField.Src) && imgField.MediaItem != null)
                                {
                                    BannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                    BannermediaAlt = imgField.Alt;
                                }
                                sbBannerText.Append("<img src='" + BannermediaUrl + "' alt='" + BannermediaAlt + "' />");
                                sbBannerText.Append("</li>");
                            }
                        }
                    }
                    DvCampaignBanners.InnerHtml = sbBannerText.ToString();

                }
            }
        }



        public void BindMobileCampaignStories()
        {
            Item CampaignItem = web.GetItem("{2C6775E7-59C8-41BF-AC77-1716C3DBDAEE}");
            if (CampaignItem != null)
            {
                int BannerCount = 0;
                ChildList CampaignBanners;
                StringBuilder sbBannerText = new StringBuilder();
                CampaignBanners = CampaignItem.GetChildren();
                sbBannerText.Append("<ul class='bxslider'>");
                for (int i = 0; i < CampaignBanners.Count; i++)
                {
                    int j;
                    if (i == CampaignBanners.Count - 1)
                    {

                        j = 0;
                    }
                    else
                        j = i + 1;

                    if (CampaignBanners[j].Versions.Count > 0 && BannerCount <= 3)
                    {
                        BannerCount++;
                        sbBannerText.Append("<li>");
                        string BannermediaUrl = string.Empty;
                        string BannermediaAlt = string.Empty;
                        Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)CampaignBanners[j].Fields["MobileBanner"]);
                        if (imgField != null && !string.IsNullOrEmpty(imgField.Src) && imgField.MediaItem != null)
                        {
                            BannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                            BannermediaAlt = imgField.Alt;
                        }
                        sbBannerText.Append("<img src='" + BannermediaUrl + "' alt='" + BannermediaAlt + "' />");
                        string Title = string.Empty;
                        Title = CampaignBanners[j].Fields["Title"].ToString();
                        sbBannerText.Append("<span class='banner-content'>" + Title + "</span>");
                        string url = string.Empty;
                        if (j != 0)
                        {
                            // sbBannerText.Append("<div class='count'><span class='time'>3:42</span> <span class='views'>22, 549 views</span> </div>");


                            url = LinkManager.GetItemUrl(CampaignBanners[j]).ToString();
                            if (!string.IsNullOrEmpty(CampaignBanners[j]["Button Text"].ToString()))
                                sbBannerText.Append("<a href='" + url + "' class='main-viewstory'>" + CampaignBanners[j]["Button Text"] + "</a>");

                        }
                        else
                        {
                            url = "https://www.singapore50.sg/SG50/Pioneer%20Generation.aspx";
                            if (!string.IsNullOrEmpty(CampaignBanners[j]["Button Text"].ToString()))
                                sbBannerText.Append("<a href='" + url + "' class='main-viewstory' target='_blank'>" + CampaignBanners[j]["Button Text"] + "</a>");
                        }





                        sbBannerText.Append("</li>");




                    }
                }
                sbBannerText.Append("</ul>");

                DivMobile.InnerHtml = sbBannerText.ToString();
            }
        }

        /* Bind User Stories */
        public void BindUserStories(int skipNumberOfItems, int intialCount)
        {
            {
                try
                {
                    Item UserItem = web.GetItem("{2E56CD94-2A02-4D4E-B53A-8B308B29F375}");
                    if (UserItem != null)
                    {
                        StringBuilder sbBannerText = new StringBuilder();
                        sbBannerText.Append("<ul>");
                        IEnumerable<Item> items = UserItem.GetChildren().Skip(skipNumberOfItems).Take(intialCount);
                        if (items != null && items.Count() > 0)
                        {
                            foreach (var item in items)
                            {

                                if (item.Versions.Count > 0)
                                {
                                    sbBannerText.Append("<li>");
                                    sbBannerText.Append("<div class='Gallery-box'>");
                                    string BannermediaUrl = string.Empty;
                                    string BannermediaAlt = string.Empty;
                                    data.ImageField imgField = ((Sitecore.Data.Fields.ImageField)item.Fields["Thumb Image"]);
                                    if (imgField != null && !string.IsNullOrEmpty(imgField.Src) && imgField.MediaItem != null)
                                    {
                                        BannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                        BannermediaAlt = imgField.Alt;
                                    }
                                    sbBannerText.Append("<img src='" + BannermediaUrl + "' alt='" + BannermediaAlt + "' /><div class='gtooltip'>");
                                    if (!string.IsNullOrEmpty(item["Content"]))
                                    {
                                        string Content = string.Empty;
                                        Content = item["Content"];

                                        if (Content.Length >= 35)
                                        {
                                            Content = Content.Substring(0, 35);//+ "...";
                                            int i = Content.LastIndexOf(" ", 35);
                                            Content = string.Format("{0}...", Content.Substring(0, (i > 0) ? i : Content.Length).Trim());
                                        }

                                        sbBannerText.Append("<span>" + Content + "</span><div class='name'>");
                                    }
                                    if (!string.IsNullOrEmpty(item["Name"]))
                                        sbBannerText.Append(item["Name"] + "</div>");
                                    string url = string.Empty;
                                    url = LinkManager.GetItemUrl(item).ToString();
                                    sbBannerText.Append("<a href='" + url + "'>" + item["Button Text"] + "</a></div></div>");

                                }
                            }
                            sbBannerText.Append("</ul>");
                            divUserStories.InnerHtml = sbBannerText.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}