﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Campaignstorydetailsresponsivesublayout.CampaignstorydetailsresponsivesublayoutSublayout"
    CodeFile="~/SG50/layouts/CampaignStoryDetailsResponsiveSublayout.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />

<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript"
    language="javascript"></script>

<script src="/SG50/include/js/jquery.min_Home.js"></script>

<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>

<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<%--<link href="/SG50/include/css/thematic-campaign-responsive.css" rel="stylesheet" type="text/css">--%>

<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...


        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            // caption: fbdes,
            description: fbdes
        };
        FB.ui(obj);

    }

    function newPopup(urlasp, url1, HostName, AccessToken, twitterContent) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function (response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + urlasp + '  ' + BuildStr(twitterContent);
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }

</script>

<div class="container custom-res" id="homePage">
    <div class="masthead">
        <div class="banner-01 border-Bnone">
            <div class="count custom-align banner-button-align">
                <a href="#" class="banner-btn banner-video">
                    <sc:Text ID="txtWatch" runat="server" Field="Watch The Film Text" />
                </a>
                <div>
                    <span class="time">3:42</span>
                    <span class="views">1000 views</span>
                </div>

                <%--<span class="time">3:42</span>
                <span class="views">22, 549 views</span>--%>
                <sc:Text ID="txtTimeView" runat="server" Field="Time" />
            </div>
            <%--<img src="images/banner-img1.jpg" alt="" class="main-banner01" />--%>
            <sc:Image ID="imgBanner" runat="server" Field="MobileBanner" />
            <!--</div>-->
            <!--<div class="js-video [vimeo, widescreen]">
          <iframe id="video" width="560" height="315" src="https://www.youtube.com/embed/9B7te184ZpQ?rel=0" frameborder="0" allowscriptaccess="always" allowfullscreen="true"></iframe>
          <span class="overlay-close-btn">Close</span>
        </div>-->
            <div class="js-video [vimeo, widescreen]">
                <div id='video'>
                </div>
                <span class="overlay-close-btn">Close</span>
            </div>
        </div>
        <!-- LABEL -->
        <div class="camp-label">
            <div class="camp-row">
                <div class="label">
                    <div class="cell-9 text">
                        <sc:Text ID="txtQuote" runat="server" Field="Story Quote" />
                    </div>
                    <div class="cell-3 share text-right">
                        <span class="text">Share the #SGspirit
                            <asp:Repeater ID="repsocialSharingTop" runat="server">
                                <ItemTemplate>
                                    <div class="social-icons">
                                        <a class="social facebook" onclick="<%# DataBinder.Eval(Container.DataItem, "VideoFacebookShare") %>" href="JavaScript:postToFeed('<%# Eval("FaceBookTitle")%>','<%# Eval("BannerVideoURL")%>','<%# Eval("FaceBookThumbNail").ToString()%>','<%# Eval("EventUrl")%>')"></a>
                                        <a class="social twitter" onclick="<%# DataBinder.Eval(Container.DataItem, "VideoTwitterShare") %>" href="JavaScript:newPopup('<%# Eval("TwitterBannerVideoURL")%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>','<%# Eval("TwitterContent").ToString()%>')"></a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- LABEL -->
        <!-- Stories -->
        <div class="camp-story">
            <div class="camp-row">
                <div class="cell-12">
                    <div class="story-header">
                        <asp:LinkButton ID="btnPrevious" runat="server" Text="Previous Story" CssClass="prev-story"
                            OnClick="btnPrevious_Click"></asp:LinkButton>
                        <span class="text">
                            <sc:Text ID="txtStoryTitle" runat="server" Field="Title" />
                        </span>
                        <asp:LinkButton ID="btnNext" runat="server" Text="Next Story" CssClass="next-story"
                            OnClick="btnNext_Click"></asp:LinkButton>
                    </div>
                </div>
                <sc:Text ID="txtStoryDescription" runat="server" Field="Description" />
                <div class="cell-12">
                    <div class="story-footer" style="display: none">
                        <div class="cell-6 padding-Lnone">
                            <p>
                                Story & images submitted by <strong>
                                    <sc:Text ID="txtSubmitted" runat="server" Field="Submitted By" />
                                </strong>
                            </p>
                        </div>
                        <div class="cell-6 padding-Rnone">
                            <div class="share text-right">
                                <span class="text">Share this story
                                    <%--<div class="social-icons">
                                    <a href="#" class="social facebook"></a>

                                    <a href="#" class="social twitter"></a>
                                </div>
                                    <asp:Repeater ID="repsocialSharingBottom" runat="server">
                                        <ItemTemplate>
                                            <div class="social-icons">
                                                <a class="social facebook" onclick="<%# DataBinder.Eval(Container.DataItem, "PostFacebookShare") %>" href="JavaScript:postToFeed('<%# Eval("FaceBookTitle")%>','<%# Eval("FaceBookContent")%>','<%# Eval("FaceBookThumbNail")%>','<%# Eval("EventUrl")%>')"></a><a class="social twitter" onclick="<%# DataBinder.Eval(Container.DataItem, "PostTwitterShare") %>" href="JavaScript:newPopup('<%# Eval("TwitterContent")%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')"></a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>--%>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Stories -->
        <div class="camp-row">
            <div class="cell-12">
                <div class="content-header">
                    <h1>
                        <sc:Text ID="txtHeaderTxt" Field="Other StoriesTitle" runat="server" DataSource="{2C6775E7-59C8-41BF-AC77-1716C3DBDAEE}" />
                    </h1>
                    <%--<a href="/SG50/sgspirit" style="color:red;font-size: 14px;bottom: 22px;padding-left: 21px;right: 0; " >Back to home</a>--%>
                    <a href="#" class="back-to-top hide-for-small">Back to top</a>
                </div>
            </div>
        </div>
        <ul class="banner-03 custom" id="DvCampaignBanners" runat="server">
        </ul>
    </div>
</div>

<script type="text/javascript">    var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>

<script>

    var str = "<%=Sitecore.Context.Item.Fields["Banner Video URL"].Value.ToString() %>";
    // alert(str);
    var key = str.split(/[\s/]+/);
    key = key[key.length - 1];
    // alert(key);
    $(function () {
        $.getJSON('https://gdata.youtube.com/feeds/api/videos/' + key + '?alt=json', function (data) {

            // + converts the string to int
            var duration = +data.entry.media$group.yt$duration.seconds;
            var numViews = +data.entry.yt$statistics.viewCount;
            var hours = parseInt(duration / 3600) % 24;
            var minutes = parseInt(duration / 60) % 60;
            var seconds = duration % 60;
            var result = (hours < 1 ? "" : hours + ":") + (minutes < 1 ? "0" : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
            $('.time').text(result);
            $('.views').text(numViews + ' views')

        });
    });

    $(function () {

        if ($(window).width() <= 1023) {
            $(".main-banner01").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });

            $(".banner-03 li img").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });
        }

    });

</script>

<script type="application/javascript">
    var ytp = document.createElement('script');
    ytp.type = 'text/javascript';
    ytp.async = false;
    ytp.src = 'https://www.youtube.com/player_api';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ytp, s);
    var done = false;
    var player;
    var playerInfoList = [{ id: 'video', height: 'auto', width: '100%', videoId: key }, ];

    function onYouTubeIframeAPIReady() {
        if (typeof playerInfoList === 'undefined')
            return;
        //  alert($(window).width()); 
        if ($(window).width() <= 1023) {
            flag = 0;
        } else {
            flag = 1;
        }
        for (var i = 0; i < playerInfoList.length; i++) {
            player = createPlayer(playerInfoList[i], flag);
        }
    }

    function createPlayer(playerInfo, flag) {
        // alert(flag)
        if (flag == 0) {
            return new YT.Player(playerInfo.id, {
                height: playerInfo.height,
                width: playerInfo.width,
                videoId: playerInfo.videoId,
                playerVars: {
                    controls: 1,
                    autoplay: 1,
                    showinfo: 1,
                    rel: 0,
                    disablekb: 1
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        } else if (flag == 1) {
            return new YT.Player(playerInfo.id, {
                height: playerInfo.height,
                width: playerInfo.width,
                videoId: playerInfo.videoId,
                playerVars: {
                    controls: 1,
                    showinfo: 1,
                    rel: 0,
                    disablekb: 1
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }
    }

    function onPlayerReady(evt) {
        // console.log('onPlayerReady', evt);
    }
    function onPlayerStateChange(evt) {
        //console.log('onPlayerStateChange', evt);
        Myslider.slider.stopAuto();
        if (evt.data == YT.PlayerState.PLAYING && !done) {
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }
    function pauseVideo() {
        player.pauseVideo();
    }

    $('.banner-video').on('click', function (ev) {
        $('.js-video').addClass('active');
        $(".js-video #video")[0].src += "&autoplay=1";
        ev.preventDefault();
    });
    $('.overlay-close-btn').on('click', function () {
        stopVideo();
        $('.js-video').removeClass('active');
        $(this).hide();
    });
    $('#video').hover(function (event) {
        $('.overlay-close-btn').show();
    });
</script>

