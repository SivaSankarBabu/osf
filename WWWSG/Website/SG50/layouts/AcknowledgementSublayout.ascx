﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Acknowledgementsublayout.AcknowledgementsublayoutSublayout" CodeFile="~/SG50/layouts/AcknowledgementSublayout.ascx.cs" %>

<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />

<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript"
    language="javascript"></script>

<script src="/SG50/include/js/jquery.min_Home.js"></script>

<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>

<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">

<div class="container custom-res" id="homePage">
    <div class="masthead">

        <!-- acknowledgement Header -->
        <div class="acknowledgement-header">
            <div class="acknowledgement-row">
                <div class="cell-12 pulse">
                    <div class="group-text-button">
                        <h1>
                            <sc:Text ID="txtTitle" runat="server" Field="Title" />
                        </h1>
                    </div>
                    <sc:Text ID="txtContent" runat="server" Field="Content" />

                </div>
            </div>
        </div>
        <!-- Photos Header -->

        <div class="acknowledgement-gallery">
            <div class="acknowledgement-row2 BorderBottom">
                <div class="Ack-Title">
                    <h2>
                        <sc:Text ID="txtSubTitle" runat="server" Field="Sponsors Title" />
                    </h2>
                </div>
                <div class="sponsorsListContainer">
                    <ul class="sponsorsList">
                        <li>
                            <h3>
                                <sc:Text ID="gldText" runat="server" Field="Title" DataSource="{4A9FFA72-A23F-4E1A-AE04-C408486E0914}" />
                            </h3>
                            <ul>
                                <asp:Repeater ID="repGoldPartners" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href='<%#Eval("href")%>' target="_blank">
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" /></a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </li>

                        <li>
                            <h3>
                                <sc:Text ID="slverText" runat="server" Field="Title" DataSource="{04CCD1C7-6762-4866-AE78-43633FCA2211}" />
                            </h3>
                            <ul>
                                <asp:Repeater ID="repSliverPartners" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href='<%#Eval("href")%>' target="_blank">
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" /></a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </li>

                        <li>
                            <h3>
                                <sc:Text ID="bronzeText" runat="server" Field="Title" DataSource="{B82D0440-1E15-4529-8997-9FF8A36E44C9}" />
                            </h3>
                            <ul>
                                <asp:Repeater ID="repBronzePartners" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href='<%#Eval("href")%>' target="_blank">
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                                            </a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- // end of sponsorsListContainer -->

                <ul class="supportersList">
                    <h4>
                        <sc:Text ID="supportersText" runat="server" Field="Title" DataSource="{1BFC7F3B-C474-4431-8E00-0E8124DBA7EB}" />
                    </h4>
                    <asp:Repeater ID="repSupporter" runat="server">
                        <ItemTemplate>
                            <li>
                                <a href='<%#Eval("href")%>' target="_blank">
                                    <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" /></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div class="acknowledgement-row2">
                <div class="Ack-Title">
                    <h2>
                        <sc:Text ID="txtPhotoGrapher" runat="server" Field="Photographers Title" />
                    </h2>
                </div>
                <sc:Text ID="txtPhotoGraphersNames" runat="server" Field="Photographers Name" />
            </div>
        </div>
    </div>
</div>


