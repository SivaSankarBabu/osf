﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using Sitecore.Data.Fields;

namespace Layouts.Aboutusresponsivesublayout
{

    /// <summary>
    /// Summary description for AboutusresponsivesublayoutSublayout
    /// </summary>
    public partial class AboutusresponsivesublayoutSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        private void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                GetComitteeImages();
                GetComitteememberImages();
                FillInitiatives();
            }
        }


        public void DownloadForm(object sender, EventArgs e)
        {
            try
            {
                Item item = Sitecore.Context.Item;
                FileField file = item.Fields["Logo and Guide"];
                if (file != null && file.MediaItem != null)
                {
                    MediaItem mi = file.MediaItem;
                    if (mi != null && !string.IsNullOrEmpty(mi.MediaPath))
                    {
                        Stream stream = mi.GetMediaStream();
                        long fileSize = stream.Length;
                        byte[] buffer = new byte[(int)fileSize];
                        stream.Read(buffer, 0, (int)stream.Length);
                        stream.Close();
                        Response.ContentType = String.Format(mi.MimeType);
                        string fileName = (mi.Name + "." + mi.Extension).ToString().Replace(' ', '_');
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(buffer);
                        Response.End();
                    }
                    else { ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('File not found');", true); }
                }
                else { ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('File not found');", true); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void GetComitteeImages()
        {
            try
            {
                DataTable dtCommittee = new DataTable();
                dtCommittee.Columns.Add("item", typeof(Item));
                dtCommittee.Columns.Add("imgSrc", typeof(string));
                DataRow drrepCommittee;
                Item CommitteeItem = web.GetItem("{E5B78EED-D041-4BCD-9DE5-417838DBA776}");
                IEnumerable<Item> itmIEnumCommittee = from a in CommitteeItem.Axes.GetDescendants()
                                                      where
                                                      a.TemplateID.ToString().Equals("{2A82792C-83DB-4E20-81C4-5E7FDBEF9382}")
                                                      select a;
                foreach (Item a in itmIEnumCommittee)
                {
                    drrepCommittee = dtCommittee.NewRow();
                    drrepCommittee["item"] = a;
                    Sitecore.Data.Fields.ImageField CommitteeimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                    if (CommitteeimgField != null)
                    {
                        string CommitteeimgSrc = CommitteeimgField.Src;
                        if (!string.IsNullOrEmpty(CommitteeimgSrc))
                        {
                            drrepCommittee["imgSrc"] = CommitteeimgSrc;
                        }
                    }
                    dtCommittee.Rows.Add(drrepCommittee);
                }
                repCommittee.DataSource = dtCommittee;
                repCommittee.DataBind();
            }
            catch (Exception exSub2)
            {

            }
        }

        public void GetComitteememberImages()
        {
            try
            {
                DataTable dtmember = new DataTable();
                dtmember.Columns.Add("item", typeof(Item));
                dtmember.Columns.Add("imgSrc", typeof(string));
                DataRow drrepmember;
                Item CommitteeItem = web.GetItem("{E5B78EED-D041-4BCD-9DE5-417838DBA776}");
                IEnumerable<Item> itmIEnumCommittee = from a in CommitteeItem.Axes.GetDescendants()
                                                      where
                                                      a.TemplateID.ToString().Equals("{CB77AE00-016D-4403-B478-A919075F68E5}")
                                                      select a;
                foreach (Item a in itmIEnumCommittee)
                {
                    drrepmember = dtmember.NewRow();
                    drrepmember["item"] = a;
                    Sitecore.Data.Fields.ImageField CommitteeimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                    if (CommitteeimgField != null)
                    {
                        string CommitteeimgSrc = CommitteeimgField.Src;
                        if (!string.IsNullOrEmpty(CommitteeimgSrc))
                        {
                            drrepmember["imgSrc"] = CommitteeimgSrc;
                        }
                    }
                    dtmember.Rows.Add(drrepmember);
                }
                repMembers.DataSource = dtmember;
                repMembers.DataBind();
            }
            catch (Exception exSub2)
            {

            }

        }

        private void FillInitiatives()
        {
            try
            {
                DataTable dtInitiatives = new DataTable();
                dtInitiatives.Columns.Add("item", typeof(Item));
                dtInitiatives.Columns.Add("imgSrc", typeof(string));
                dtInitiatives.Columns.Add("Title", typeof(string));
                dtInitiatives.Columns.Add("Description", typeof(string));
                dtInitiatives.Columns.Add("ButtonText", typeof(string));
                dtInitiatives.Columns.Add("Id", typeof(string));
                dtInitiatives.Columns.Add("href", typeof(string));
                dtInitiatives.Columns.Add("Status", typeof(Boolean));
                dtInitiatives.Columns.Add("alt", typeof(string));

                dtInitiatives.Columns.Add("LeftRightimg", typeof(string));
                dtInitiatives.Columns.Add("LeftRightArrow", typeof(string));

                DataRow drInitiatives;
                Item itmInitiatives = web.GetItem("{6845B791-1A07-47CE-AC47-3E5308EA49F6}");
                Sitecore.Collections.ChildList Initiativeschild = itmInitiatives.GetChildren();
                int count = 1;
                foreach (Item a in Initiativeschild)
                {

                    drInitiatives = dtInitiatives.NewRow();

                    if (count % 2 == 0)
                    {
                        drInitiatives["LeftRightimg"] = "ourintiatives rightimg";
                        drInitiatives["LeftRightArrow"] = "arrowright";
                    }
                    else
                    {
                        drInitiatives["LeftRightimg"] = "ourintiatives leftimg";
                        drInitiatives["LeftRightArrow"] = "arrowleft";
                    }
                    count++;

                    drInitiatives["item"] = a;
                    drInitiatives["title"] = a.Fields["Title"].ToString();

                    drInitiatives["description"] = a.Fields["Description"].ToString();

                    drInitiatives["buttontext"] = a.Fields["ButtonText"].ToString();

                    if (string.IsNullOrEmpty(a.Fields["ButtonText"].ToString()))
                        drInitiatives["Status"] = false;
                    else
                        drInitiatives["Status"] = true;

                    Sitecore.Data.Fields.LinkField lnk = a.Fields["UrlLink"];
                    drInitiatives["href"] = lnk.Url;

                    drInitiatives["Id"] = a.ID.ToString();

                    Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["MainImage"]);

                    if (mainImgField != null)
                    {
                        string mainimgSrc = mainImgField.Src;
                        if (!string.IsNullOrEmpty(mainimgSrc))
                        {
                            drInitiatives["imgSrc"] = mainimgSrc;
                            drInitiatives["alt"] = mainImgField.Alt;

                        }
                    }
                    dtInitiatives.Rows.Add(drInitiatives);

                }
                repInitiatives.DataSource = dtInitiatives;
                repInitiatives.DataBind();

            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }
    }
}