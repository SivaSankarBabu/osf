﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Home_sublayout.Home_sublayoutSublayout" CodeFile="~/SG50/layouts/Home Sublayout.ascx.cs" %>

<script type="text/javascript">
    $(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            auto: true,
            pause: 10000,
        });
    });
</script>

<div class="content_inner">
    <div id="wrapper">
        <div id="slider">


            <div class="slide" style="background-image: url(/SG50/images/slider01.jpg);">
                <div class="slide-block">
                    <sc:Text ID="Text3" Field="Slider Content1" runat="server" />
                    <div class="carouselButton">
                        <a href="#aboutTitle">WHAT IS SG50</a>
                    </div>
                    <div class="carouselButton">
                        <a class="video" title="test" href="http://www.youtube.com/v/WYFe2-hqA2Q?fs=1&amp;autoplay=1" target="_blank">WATCH THE FILM</a>
                    </div>
                </div>
            </div>

            <div class="slide" style="background-image: url(/SG50/images/slider02.jpg);">
                <div class="slide-block">
                    <sc:Text ID="Text4" Field="Slider Content2" runat="server" />
                    <div class="carouselButton">
                        <a href="<%= getShareAnIdeaLink() %>">SHARE AN IDEA</a>
                    </div>

                </div>
            </div>

            <div class="slide" style="background-image: url(/SG50/images/slider03.jpg);">
                <div class="slide-block">
                    <sc:Text ID="Text5" Field="Slider Content3" runat="server" />
                    <div class="carouselButton">
                        <a href="http://www.iconsof.sg" target="_blank">ICONSOFSG</a>
                    </div>

                </div>

            </div>

        </div>
        <div id="pager" class="pager">
            <a href="#"><span></span></a>
            <a href="#"><span></span></a>
            <a href="#"><span></span></a>
        </div>
    </div>

    <ul class="bxslider">
        <li>
            <img src="/SG50/images/slider01.jpg" />
            <sc:Text Field="Slider Content 1 Responsive" runat="server" />
            <div class="carouselButtonMobile1">
                <a href="#aboutTitle">WHAT IS SG50</a>
            </div>
            <div class="carouselButtonMobile2">
                <a class="video" title="TVC" href="http://www.youtube.com/v/WYFe2-hqA2Q?fs=1&amp;autoplay=1" target="_blank">WATCH THE FILM</a>
            </div>
        </li>
        <li>
            <img src="/SG50/images/slider02.jpg" />
            <sc:Text Field="Slider Content 2 Responsive" runat="server" />
            <div class="carouselButtonMobile3">
                <a href="<%= getShareAnIdeaLink() %>">SHARE AN IDEA</a>
            </div>
        </li>
        <li>
            <img src="/SG50/images/slider03.jpg" />
            <sc:Text Field="Slider Content 3 Responsive" runat="server" />
            <div class="carouselButtonMobile4">
                <a href="http://www.iconsof.sg" target="_blank">ICONSOFSG</a>
            </div>
        </li>
    </ul>

    <div class="about">
        <div class="padding">
            <div id="aboutTitle">
                <h1>CELEBRATING THE LITTLE RED D<sc:Image ID="Image3" runat="server" Field="About Image 2" />T.</h1>
            </div>
            <div id="roundImage">
                <sc:Image ID="Image2" runat="server" Field="About Image" />
            </div>
            <sc:Text ID="aboutContent" Field="About Content" runat="server" />
            <div id="downloadLogoAndGuide"><a href="<%= getDownloadLink()%>">OFFICIAL BRAND GUIDE</a> </div>
            <div id="downloadLogoAndGuide2"><a href="<%= getDownloadLink()%>">SIMPLIFIED BRAND GUIDE</a> </div>
        </div>

    </div>
    <div class="sg50LogoAndGuide">
        <div class="padding">
            <div id="sg50LogoAndGuideBanner">
                <%--<sc:Image ID="Image1" runat="server" Field="LogoGuide Landing Image" />--%>
            </div>
            <div class="sg50LogoAndGuideContainer">
                <sc:Text ID="Text1" Field="Logo and Guide Content" runat="server" />
                <div id="icons">
                    <div id="eduAndYouth">
                        <sc:Image ID="Image4" runat="server" Field="LogoGuide Image1" />
                        <p>
                            Education and<br />
                            Youth
                        </p>
                    </div>
                    <div id="cultureAndCommunity">
                        <sc:Image ID="Image5" runat="server" Field="LogoGuide Image2" />
                        <p>
                            Culture and<br />
                            Community
                        </p>
                    </div>
                    <div id="ecoAndInter">
                        <sc:Image ID="Image6" runat="server" Field="LogoGuide Image3" />
                        <p>
                            Economic and<br />
                            International
                        </p>
                    </div>
                    <div id="enviAndInfra">
                        <sc:Image ID="Image7" runat="server" Field="LogoGuide Image4" />
                        <p>
                            Environment and<br />
                            Infrastructure
                        </p>
                    </div>
                    <div id="partnership">
                        <sc:Image ID="Image8" runat="server" Field="LogoGuide Image5" />
                        <p>Partnership</p>
                    </div>
                </div>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
    <%--<div class="sg50Committee">
        <div class="padding">
            <div id="sg50CommitteeBanner">
                <sc:Image ID="Image9" runat="server" Field="Committee Landing Image" />
            </div>
            <div id="sg50CommitteeTitle">
                <h1>MEET THE COMMITTEE.</h1>
            </div>
            <div id="sg50SteeringCommittee">
                <div id="sg50SteeringCommitteeTitle">
                    <h1>SG50 STEERING COMMITTEE</h1>
                </div>
                <div id="sg50SteeringCommitteeMembers">
                    <div id="hsk">
                        <sc:Image ID="Image10" runat="server" Field="Committee1" />
                        <sc:Text ID="Text9" Field="LogoGuide Content1" runat="server" />
                    </div>
                    <div id="hy">
                        <sc:Image ID="Image11" runat="server" Field="Committee2" />
                        <sc:Text ID="Text8" Field="LogoGuide Content2" runat="server" />
                    </div>
                    <div id="ccs">
                        <sc:Image ID="Image12" runat="server" Field="Committee3" />
                        <sc:Text ID="Text7" Field="LogoGuide Content3" runat="server" />
                    </div>
                </div>
            </div>
            <div id="programmeOffice">
                <div id="programmeOfficeTitle">
                    <h1>PROGRAMME OFFICE</h1>
                </div>
                <div id="programmeOfficeMember">
                    <div id="lw">
                        <sc:Image ID="Image13" runat="server" Field="Committee4" />
                        <sc:Text ID="Text6" Field="LogoGuide Content4" runat="server" />
                    </div>
                </div>
            </div>
            <div id="sg50CommitteeIcons">
                <div id="sg50CommitteeEduAndYouth">
                    <div id="sg50CommitteeEduAndYouthTitle">
                        <sc:Image ID="Image24" runat="server" Field="LogoGuide Image1" />
                        <h1>Education and Youth</h1>
                    </div>
                    <div id="sg50CommitteeEduAndYouthMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image14" runat="server" Field="Committee5" />
                            </div>
                            <sc:Text ID="Text10" Field="Committee Content1" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text11" Field="Committee Content2" runat="server" />
                            <div>
                                <sc:Image ID="Image15" runat="server" Field="Committee6" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteeCultureAndCommunity">
                    <div id="sg50CommitteeCultureAndCommunityTitle">
                        <sc:Image ID="Image25" runat="server" Field="LogoGuide Image2" />
                        <h1>Culture and Community</h1>
                    </div>
                    <div id="sg50CommitteeCultureAndCommunityMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image16" runat="server" Field="Committee7" />
                            </div>
                            <sc:Text ID="Text12" Field="Committee Content3" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text13" Field="Committee Content4" runat="server" />
                            <div>
                                <sc:Image ID="Image17" runat="server" Field="Committee8" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteeEcoAndInter">
                    <div id="sg50CommitteeEcoAndInterTitle">
                        <sc:Image ID="Image26" runat="server" Field="LogoGuide Image3" />
                        <h1>Economic and International</h1>
                    </div>
                    <div id="sg50CommitteeEcoAndInterMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image18" runat="server" Field="Committee9" />
                            </div>
                            <sc:Text ID="Text14" Field="Committee Content5" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text15" Field="Committee Content6" runat="server" />
                            <div>
                                <sc:Image ID="Image19" runat="server" Field="Committee10" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteeEnviAndInfra">
                    <div id="sg50CommitteeEnviAndInfraTitle">
                        <sc:Image ID="Image27" runat="server" Field="LogoGuide Image4" />
                        <h1>Environment and Infrastucture</h1>
                    </div>
                    <div id="sg50CommitteeEnviAndInfraMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image20" runat="server" Field="Committee11" />
                            </div>
                            <sc:Text ID="Text16" Field="Committee Content7" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text17" Field="Committee Content8" runat="server" />
                            <div>
                                <sc:Image ID="Image21" runat="server" Field="Committee12" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteePartnership">
                    <div id="sg50CommitteePartnershipTitle">
                        <sc:Image ID="Image28" runat="server" Field="LogoGuide Image5" />
                        <h1>Partnership</h1>
                    </div>
                    <div id="sg50CommitteePartnershipMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image22" runat="server" Field="Committee13" />
                            </div>
                            <sc:Text ID="Text18" Field="Committee Content9" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text19" Field="Committee Content10" runat="server" />
                            <div>
                                <sc:Image ID="Image23" runat="server" Field="Committee14" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sg50CommitteeMembers">
                <div id="sg50CommitteeMembersTitle">
                    <h1>MEMBERS</h1>
                </div>
                <div id="sg50CommitteeMembersSeparator">
                    <sc:Text ID="Text2" Field="Committee Content" runat="server" />
                </div>
            </div>
        </div>
    </div>--%>
    <div class="sg50Committee">
        <div class="padding">
            <div id="sg50CommitteeBanner">
                <%--<sc:Image ID="Image9" runat="server" Field="Committee Landing Image" />--%>
            </div>
            <div id="sg50CommitteeTitle">
                <h1>MEET THE COMMITTEE.</h1>
            </div>
            <div id="topMember">
                <div>
                    <img src="/SG50/images/committee01.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee02.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee03.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee04.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee05.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee06.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee07.jpg" />
                </div>
            </div>
            <div id="bottomMember">
                <div>
                    <img src="/SG50/images/member.jpg" />
                </div>
            </div>

            <div id="topMemberMobile">
                <div>
                    <img src="/SG50/images/mbcommittee01.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/mbcommittee02.jpg" />
                </div>
            </div>
            <div id="middleMemberMobile">
                <div>
                    <img src="/SG50/images/mbcommittee03.jpg" />
                </div>
            </div>
            <div id="bottomMemberMobile">
                <div>
                    <img src="/SG50/images/mbmembers.jpg" />
                </div>
            </div>

        </div>
    </div>
</div>
