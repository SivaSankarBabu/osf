﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Projects_sublayout.Projects_sublayoutSublayout" CodeFile="~/SG50/layouts/Projects Sublayout.ascx.cs" %>

<div class="projectss">
    <div id="projectsInner">
        <div id="projectsBannerImage">
            <img src="/SG50/images/sai.jpg" />
        </div>
        <div id="projectsBannerCaption">
            <h1>WE'RE TURNING 50, HOW WOULD YOU LIKE TO <span style="color: red;">CELEBRATE</span> ?</h1>
            <h2>In 2015, Singapore will be celebrating its 50th. We want to kick off the run-up to our big birthday with a bang. And you're all invited.</h2>
            <p>For Singapore's 50th, get involved in the party-planning and share your own celebration ideas. Big or small, hearfeit or plain whacky, every idea matters.</p>
        </div>
        <div id="shareAnIdeaButton">
            <a href="<%= getShareAnIdeaLink() %>">SHARE AN IDEA</a>
        </div>
    </div>
</div>
