﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using Sitecore.Data.Fields;

namespace Layouts.Aboutussublayout
{

    /// <summary>
    /// Summary description for AboutussublayoutSublayout
    /// </summary>
    public partial class AboutussublayoutSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Put user code to initialize the page here
                FillInitiatives();
            }
        }
        public void DownloadForm(object sender, EventArgs e)
        {
            try
            {
                Item item = Sitecore.Context.Item;
                FileField file = item.Fields["Logo and Guide"];
                if (file != null && file.MediaItem != null)
                {
                    MediaItem mi = file.MediaItem;
                    if (mi != null && !string.IsNullOrEmpty(mi.MediaPath))
                    {
                        Stream stream = mi.GetMediaStream();
                        long fileSize = stream.Length;
                        byte[] buffer = new byte[(int)fileSize];
                        stream.Read(buffer, 0, (int)stream.Length);
                        stream.Close();
                        Response.ContentType = String.Format(mi.MimeType);
                        string fileName = (mi.Name + "." + mi.Extension).ToString().Replace(' ', '_');
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.BinaryWrite(buffer);
                        Response.End();
                    }
                    else { ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('File not found');", true); }
                }
                else { ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('File not found');", true); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void FillInitiatives()
        {
            //try
            //{
            DataTable dtInitiatives = new DataTable();
            dtInitiatives.Columns.Add("item", typeof(Item));
            dtInitiatives.Columns.Add("imgSrc", typeof(string));
            dtInitiatives.Columns.Add("Title", typeof(string));
            dtInitiatives.Columns.Add("Description", typeof(string));
            dtInitiatives.Columns.Add("ButtonText", typeof(string));
            dtInitiatives.Columns.Add("Id", typeof(string));
            dtInitiatives.Columns.Add("href", typeof(string));
            dtInitiatives.Columns.Add("Status", typeof(Boolean));
            dtInitiatives.Columns.Add("alt", typeof(string));

            dtInitiatives.Columns.Add("LeftRightimg", typeof(string));
            dtInitiatives.Columns.Add("LeftRightArrow", typeof(string));

            DataRow drInitiatives;
            Item itmInitiatives = web.GetItem("{6845B791-1A07-47CE-AC47-3E5308EA49F6}");
            Sitecore.Collections.ChildList Initiativeschild = itmInitiatives.GetChildren();
            int count = 1;
            foreach (Item a in Initiativeschild)
            {

                drInitiatives = dtInitiatives.NewRow();

                if (count % 2 == 0)
                {
                    drInitiatives["LeftRightimg"] = "ourintiatives rightimg";
                    drInitiatives["LeftRightArrow"] = "arrowright";
                }
                else
                {
                    drInitiatives["LeftRightimg"] = "ourintiatives leftimg";
                    drInitiatives["LeftRightArrow"] = "arrowleft";
                }
                count++;
                drInitiatives["item"] = a;
                drInitiatives["title"] = a.Fields["Title"].ToString();

                drInitiatives["description"] = a.Fields["Description"].ToString();

                drInitiatives["buttontext"] = a.Fields["ButtonText"].ToString();

                if (string.IsNullOrEmpty(a.Fields["ButtonText"].ToString()))
                    drInitiatives["Status"] = false;
                else
                    drInitiatives["Status"] = true;

                Sitecore.Data.Fields.LinkField lnk = a.Fields["UrlLink"];
                drInitiatives["href"] = lnk.Url;

                drInitiatives["Id"] = a.ID.ToString();

                Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["MainImage"]);

                if (mainImgField != null)
                {
                    string mainimgSrc = mainImgField.Src;
                    if (!string.IsNullOrEmpty(mainimgSrc))
                    {
                        drInitiatives["imgSrc"] = mainimgSrc;
                        drInitiatives["alt"] = mainImgField.Alt;

                    }
                }
                dtInitiatives.Rows.Add(drInitiatives);

            }
            repInitiatives.DataSource = dtInitiatives;
            repInitiatives.DataBind();

            //}
            //catch (Exception exSub1)
            //{
            //    //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            //}
        }
    }
}