﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Searchresultsublayout.SearchresultsublayoutSublayout" CodeFile="~/SG50/layouts/SearchResultSublayout.ascx.cs" %>

<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript" language="javascript"></script>
<script src="/SG50/include/js/jquery.min_Home.js"></script>
<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>
<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
    $(function () {
        $('.cell-12:eq(0)').find('strong:eq(1)').text($('.photo-row .result-section').length + ' ');

        function RequiredSerachKeyWord(btnId, ctrlId, msg) {
            if ($(btnId).length > 0 && $(ctrlId).length > 0 && msg != '') {
                $(btnId).click(function () {
                    if ($(ctrlId).val() != '') {
                        return true;
                    } else { alert(msg); $(ctrlId).focus(); return false; }
                });
            }
        }

        RequiredSerachKeyWord('#content_0_imgSearchButton', '#content_0_txtSearch', 'Please enter keywords to search.');



    })
</script>

<div class="container custom-res" id="homePage">
    <div class="masthead">
        <div class="photo-header">
            <div class="photo-row">
                <div class="cell-12 pulse">
                    <div class="group-text-button">
                        <h1>SEARCH RESULTS</h1>
                    </div>
                    <p>Your search for <strong>“<%=Session["searchWord"] %>” </strong>returned <strong></strong>results.</p>
                    <div class="form-group">
                        <asp:TextBox ID="txtSearch" runat="server" ToolTip="Enter the Keyword to Search" placeholder="SEARCH"></asp:TextBox>
                        <asp:ImageButton ID="imgSearchButton" CssClass="input-button" runat="server" OnClick="imgSearchButton_Click" ImageUrl="~/SG50/images/search1.png" Style="float: left" />

                    </div>
                </div>
            </div>
        </div>

        <!-- Search Results Header -->

        <!-- SEARCH RESULTS -->
        <div class="SearchResults">
            <div style="text-align: center; font-family: Calibri;">
                <asp:Label ID="lblResult" Style="text-align: center" ForeColor="Red" runat="server"></asp:Label>
            </div>
            <div class="photo-row">
                <asp:Repeater ID="repNewCollectibles" runat="server">
                    <ItemTemplate>
                        <div class="result-section">
                            <a href="<%#Eval("href")%>">
                                <h2><%#Eval("Title")%></h2>
                            </a>
                            <p><%#Eval("Description")%></p>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>
                <div class="cell-12">
                    <div class="pagination">
                        <ul>
                            <li>
                                <asp:LinkButton ID="PrevControl" runat="server" CssClass="pre" OnClick="PrevControl_Click"></asp:LinkButton>
                            </li>
                            <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                <ItemTemplate>
                                    <li>
                                        <asp:LinkButton ID="btnPage" Enabled='<%# DataBinder.Eval(Container.DataItem, "Enable") %>'
                                            Style='<%# DataBinder.Eval(Container.DataItem, "style") %>'
                                            CommandName="Page" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>'
                                            runat="server" ForeColor="White" Font-Bold="True"><%# DataBinder.Eval(Container.DataItem, "ID") %>
                                        </asp:LinkButton>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            <li>
                                <asp:LinkButton ID="NextControl" runat="server" CssClass="next" OnClick="NextControl_Click"></asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>

<script type="text/ecmascript">

    $(function () {
        function HighlightWord(divId) {
            if ($(divId).length > 0) {
                $(divId).each(function () {
                    var thisCtrl = $(this).not('a'), searchWord = $('.cell-12.pulse').find('p:eq(0) strong:eq(0)').text(); searchWord = searchWord.substring(1, searchWord.length - 2);
                    $('#content_0_txtSearch').val(searchWord);
                    for (var index = 0; index < $(thisCtrl).find('p').length; index++) {
                        var thisHtml = $(thisCtrl).find('p:eq(' + index + ')');
                        if (thisHtml.text() != '') {
                            if (thisHtml.text().toLowerCase().indexOf(searchWord.toLowerCase()) > -1) {
                                thisHtml.html(thisHtml.text().replace(new RegExp(searchWord, "gi"), function (match) { return "<a>" + match + "</a>"; }));
                            }
                        }
                    }
                });
            } else return false;
        }

        HighlightWord('.result-section');
    })
</script>
