﻿//using System;
//using Sitecore.Data.Fields;
//using Sitecore.Data;
//using System.Data;
//using Sitecore.Data.Items;
//using ASPSnippets.FaceBookAPI;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
//using ASPSnippets.FaceBookAPI;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.IO;
using System.Text;
using Sitecore.Collections;
using Sitecore.Resources.Media;
using Sitecore;


namespace Layouts.Celebration_fund_events_details_sublayout
{

    /// <summary>
    /// Summary description for Celebration_fund_events_details_sublayoutSublayout
    /// </summary>
    public partial class Celebration_fund_events_details_sublayoutSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmContext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {



                Sitecore.Collections.ChildList ccc = web.GetItem("{DEFF9D32-481F-454D-A125-E0E46131822B}").GetChildren();//147C706A-EDE3-483A-BC4D-0197E59BDDCC


                string curntItem = Sitecore.Context.Item.ID.ToString();

                string path = Sitecore.Context.Item.Paths.Path.ToString();
                Item ImageItem = web.GetItem(path + "/Images");
                ChildList ImagesCount = null;
                if (ImageItem != null && ImageItem.Versions.Count > 0)
                {


                    ImagesCount = ImageItem.GetChildren();
                    string[] stringArray = new string[ImagesCount.Count];
                    int availableimage = 0;
                    for (int i = 0; i < ImagesCount.Count; i++)
                    {
                        Sitecore.Data.Fields.ImageField image = ImagesCount[i].Fields["Image"];

                        if (image.MediaItem != null)
                        {
                            MediaUrlOptions options = new MediaUrlOptions();
                            options.BackgroundColor = System.Drawing.Color.White;
                            string url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                            // img1.ImageUrl = url;
                            stringArray[i] = url;
                            availableimage = availableimage + 1;
                        }
                    }

                    //  img1.ImageUrl = stringArray[0];
                    //  img2.ImageUrl = stringArray[1];
                    StringBuilder sbBannerText = new StringBuilder();
                    if (availableimage == 1)
                    {
                       // Response.Write("one");
                        sbBannerText.Append("<div id='oneColumn'> <div> <img src='" + stringArray[0] + "'/></div> </div>");

                    }
                    else if (availableimage == 2)
                    {
                        // Response.Write(stringArray[0]);
                        //  Response.Write(stringArray[1]);
                        // Response.Write("two");
                        //sbBannerText.Append("<div id='twoColumn'> <div> <asp:Image ID='img1' runat='server' ImageUrl=" + stringArray[0] + " /></div>");
                        //sbBannerText.Append(" <div> <asp:Image ID='img2' runat='server' ImageUrl=" + stringArray[1] + " /></div> </div>");

                        sbBannerText.Append("<div id='twoColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append("<div id='twoColumn'> <div> <img src='" + stringArray[1] + "'/></div></div>");

                    }
                    else if (availableimage == 3)
                    {
                       // Response.Write("three");
                        sbBannerText.Append("<div id='threColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div> </div>");
                    }
                    else if (availableimage == 4)
                    {
                       // Response.Write("four");
                        sbBannerText.Append("<div id='fourColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[3] + "'/></div> </div>");
                    }
                    else if (availableimage == 5)
                    {
                       // Response.Write("five");
                        sbBannerText.Append("<div id='fiveColumn'> <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[3] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[4] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[5] + "'/></div> </div>");
                    }
                    else if (availableimage == 6)
                    {
                       // Response.Write("six");
                        sbBannerText.Append("<div id='sixColumn'> <div> <img src='" + stringArray[0] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[1] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[2] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[3] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[4] + "'/></div>");
                        sbBannerText.Append(" <div> <img src='" + stringArray[5] + "'/></div> </div>");
                    }


                    ImageCount.InnerHtml = sbBannerText.ToString();


                }

                // curntItem.web.ge

                if (ccc[0].ID.ToString() == curntItem)
                {
                    btnPrevious.Enabled = false;
                    btnPrevious.CssClass = "deactive";
                    //CssClass="deactive"
                }
                if (ccc[ccc.Count - 1].ID.ToString() == curntItem)
                {
                    btnNext.Enabled = false;
                    btnNext.CssClass = "deactive";
                }
                SocialSharing();

                //Youtube
                string YoutubeVideo = string.Empty;
                YoutubeVideo = Sitecore.Context.Item["Video Link1"];
                refVideo.HRef = YoutubeVideo;
              



            }
            //FaceBookConnect.API_Secret = "736c21acc64a1ef633f8766070dbf886";
            // Put user code to initialize the page here
        }
        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            Sitecore.Collections.ChildList ccc = web.GetItem("{147C706A-EDE3-483A-BC4D-0197E59BDDCC}").GetChildren();
            string curntItem = Sitecore.Context.Item.ID.ToString();

            for (int i = 0; i < ccc.Count; i++)
            {
                if (ccc[i].ID.ToString() == curntItem)
                {
                    string url = Sitecore.Links.LinkManager.GetItemUrl(ccc[i - 1]);
                    Response.Redirect(url);
                }
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            Sitecore.Collections.ChildList ccc = web.GetItem("{147C706A-EDE3-483A-BC4D-0197E59BDDCC}").GetChildren();
            string curntItem = Sitecore.Context.Item.ID.ToString();

            for (int i = 0; i < ccc.Count; i++)
            {
                if (ccc[i].ID.ToString() == curntItem)
                {
                    string url = Sitecore.Links.LinkManager.GetItemUrl(ccc[i + 1]);
                    Response.Redirect(url);
                }
            }
        }
        private void SocialSharing()
        {
            DataTable dtrepEvents = new DataTable();
            //dtrepEvents.Columns.Add("Id", typeof(string));
            //dtrepEvents.Columns.Add("FaceBookTitle", typeof(string));
            //DataRow drrepEvents;
            // drrepEvents = dtrepEvents.NewRow();
            //drrepEvents["FaceBookTitle"] = "Sandeep";
            //dtrepEvents.Rows.Add(drrepEvents);
            //grdsocial.DataSource = dtrepEvents;
            //grdsocial.DataBind();
            dtrepEvents.Columns.Add("Id", typeof(string));
            dtrepEvents.Columns.Add("FaceBookTitle", typeof(string));
            dtrepEvents.Columns.Add("FaceBookContent", typeof(string));
            dtrepEvents.Columns.Add("FaceBookThumbnail", typeof(string));
            dtrepEvents.Columns.Add("TwitterTitle", typeof(string));
            dtrepEvents.Columns.Add("TwitterContent", typeof(string));
            // dtrepEvents.Columns.Add("TwitterThumbnail", typeof(string));
            DataRow drrepEvents;
            // Sitecore.Data.Fields.DateField date = itmContext.Fields["Event Date"];
            // System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
            //if (datetime.Date.CompareTo(DateTime.Today.Date) > 0)
            // {
            try
            {
                drrepEvents = dtrepEvents.NewRow();
                drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["Facebook Title"].ToString(), CommonMethods.faceBook);
                drrepEvents["FaceBookContent"] = cmObj.BuildString(itmContext.Fields["Facebook Content"].ToString(), CommonMethods.faceBook);
                Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Facebook Image"]);
                string fbimgSrc = fbimgField.Src;
                if (!fbimgSrc.Equals(""))
                {
                    drrepEvents["FaceBookThumbNail"] =  fbimgSrc;
                }
                drrepEvents["TwitterTitle"] = itmContext.Fields["Twitter Title"].ToString();
                drrepEvents["TwitterContent"] = cmObj.BuildString(itmContext.Fields["Twitter Content"].ToString(), CommonMethods.faceBook);
                
                //Sitecore.Data.Fields.ImageField twimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Twitter ThumbNail"]);
                //string twimgSrc = twimgField.Src;
                //if (!twimgSrc.Equals(""))
                //{
                //    drrepEvents["TwitterThumbNail"] = "" + twimgSrc;
                //}

                drrepEvents["Id"] = itmContext.ID;
                dtrepEvents.Rows.Add(drrepEvents);
                repsocialSharing.DataSource = dtrepEvents;
                repsocialSharing.DataBind();

            }
            catch (Exception exSub1)
            {
                // Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
            //  }
        }
      

    }
}