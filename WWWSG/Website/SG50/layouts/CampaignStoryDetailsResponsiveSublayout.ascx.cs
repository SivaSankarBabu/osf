﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using Sitecore.Data.Fields;
using System.Text.RegularExpressions;

namespace Layouts.Campaignstorydetailsresponsivesublayout
{

    /// <summary>
    /// Summary description for CampaignstorydetailsresponsivesublayoutSublayout
    /// </summary>
    public partial class CampaignstorydetailsresponsivesublayoutSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();

        Item itmContext = Sitecore.Context.Item;
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindCampaignStories();
                SocialSharing();
            }
            // Put user code to initialize the page here
        }

        public void BindCampaignStories()
        {
            Item CampaignItem = web.GetItem("{2C6775E7-59C8-41BF-AC77-1716C3DBDAEE}");
            if (CampaignItem != null)
            {
                ChildList CampaignBanners;
                StringBuilder sbBannerText = new StringBuilder();
                CampaignBanners = CampaignItem.GetChildren();
                if (CampaignBanners != null && CampaignBanners.Count > 0)
                {
                    int BannerCount = 0;
                    for (int i = 0; i < CampaignBanners.Count; i++)
                    {
                        if (CampaignBanners[i].Versions.Count > 0 && BannerCount < 3)
                        {
                            string curntItem = Sitecore.Context.Item.ID.ToString();
                            if (CampaignBanners[i].ID.ToString() != curntItem)
                            {
                                sbBannerText.Append("<li>");
                                sbBannerText.Append("<div class='image-top-text'>");
                                string Title = string.Empty;
                                Title = CampaignBanners[i].Fields["Title"].ToString();
                                sbBannerText.Append("<h3></h3><br/>");
                                // sbBannerText.Append("<div class='count custom-align show-for-small'><span class='time'>3:42</span><span class='views'>22, 549 views</span></div>");
                                BannerCount++;
                                string url = string.Empty;
                                url = LinkManager.GetItemUrl(CampaignBanners[i]).ToString();

                                //if (i == 0)
                                //{
                                //    url = "https://www.singapore50.sg/SG50/Pioneer%20Generation.aspx";
                                //}

                                if (!string.IsNullOrEmpty(CampaignBanners[i]["Button Text"].ToString()))
                                {
                                    if (i == 0)
                                    {
                                        url = "https://www.singapore50.sg/SG50/Pioneer%20Generation.aspx";
                                        sbBannerText.Append("<a href='" + url + "' class='viewstory btn-hover-white' target='_blank'>" + CampaignBanners[i]["Button Text"] + "</a></div>");
                                    }
                                    else
                                        sbBannerText.Append("<a href='" + url + "' class='viewstory btn-hover-white'>" + CampaignBanners[i]["Button Text"] + "</a></div>");
                                }
                                else
                                {
                                    sbBannerText.Append("</div>");
                                }
                                string BannermediaUrl = string.Empty;
                                string BannermediaAlt = string.Empty;
                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)CampaignBanners[i].Fields["Story Image"]);
                                if (imgField != null && !string.IsNullOrEmpty(imgField.Src) && imgField.MediaItem != null)
                                {
                                    BannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                    BannermediaAlt = imgField.Alt;
                                }
                                sbBannerText.Append("<img src='" + BannermediaUrl + "' alt='" + BannermediaAlt + "' />");
                                sbBannerText.Append("</li>");
                            }
                        }
                    }
                    DvCampaignBanners.InnerHtml = sbBannerText.ToString();
                }
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            Sitecore.Collections.ChildList ccc = web.GetItem("{2C6775E7-59C8-41BF-AC77-1716C3DBDAEE}").GetChildren();
            if (ccc != null && ccc.Count > 0)
            {
                string curntItem = Sitecore.Context.Item.ID.ToString();
                for (int i = 0; i < ccc.Count; i++)
                {
                    if (ccc[0].ID.ToString() == curntItem)
                    {
                        string url = Sitecore.Links.LinkManager.GetItemUrl(ccc[ccc.Count - 1]);
                        Response.Redirect(url);
                    }
                    else if (ccc[i].ID.ToString() == curntItem)
                    {

                        string url = Sitecore.Links.LinkManager.GetItemUrl(ccc[i - 1]);
                        Response.Redirect(url);
                    }
                }
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            Sitecore.Collections.ChildList ccc = web.GetItem("{2C6775E7-59C8-41BF-AC77-1716C3DBDAEE}").GetChildren();
            string curntItem = Sitecore.Context.Item.ID.ToString();
            if (ccc != null && ccc.Count > 0)
            {
                for (int i = 0; i < ccc.Count; i++)
                {
                    if (ccc[ccc.Count - 1].ID.ToString() == curntItem)
                    {
                        string url = Sitecore.Links.LinkManager.GetItemUrl(ccc[0]);
                        Response.Redirect(url);
                    }
                    else if (ccc[i].ID.ToString() == curntItem)
                    {
                        string url = Sitecore.Links.LinkManager.GetItemUrl(ccc[i + 1]);
                        Response.Redirect(url);
                    }
                }
            }
        }
        private void SocialSharing()
        {

            DataTable dtrepEvents = new DataTable();
            dtrepEvents.Columns.Add("Id", typeof(string));
            dtrepEvents.Columns.Add("FaceBookTitle", typeof(string));
            dtrepEvents.Columns.Add("FaceBookContent", typeof(string));
            dtrepEvents.Columns.Add("FaceBookThumbnail", typeof(string));
            dtrepEvents.Columns.Add("TwitterContent", typeof(string));
            dtrepEvents.Columns.Add("href", typeof(string));
            dtrepEvents.Columns.Add("HostName", typeof(string));
            dtrepEvents.Columns.Add("AccessToken", typeof(string));
            dtrepEvents.Columns.Add("EventUrl", typeof(string));

            dtrepEvents.Columns.Add("PostFacebookShare", typeof(string));
            dtrepEvents.Columns.Add("PostTwitterShare", typeof(string));

            dtrepEvents.Columns.Add("BannerVideoURL", typeof(string));
            dtrepEvents.Columns.Add("TwitterBannerVideoURL", typeof(string));


            dtrepEvents.Columns.Add("VideoFacebookShare", typeof(string));
            dtrepEvents.Columns.Add("VideoTwitterShare", typeof(string));

            DataRow drrepEvents;

            //try
            //{
            string SocialTitle = string.Empty;
            string SocialContent = string.Empty;
            string TwitterContent = string.Empty;
            string noHTML = string.Empty;
            string inputHTML = string.Empty;
            string strBannerVideoURL = string.Empty;
            string strTwtBannerVideoURL = string.Empty;

            drrepEvents = dtrepEvents.NewRow();
            drrepEvents["HostName"] = hostName + "/";
            drrepEvents["AccessToken"] = accessToken;
            drrepEvents["href"] = LinkManager.GetItemUrl(itmContext).ToString();
            drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(itmContext).ToString();

            if (!string.IsNullOrEmpty(itmContext.Fields["Social Sharing Title"].ToString()))
            {
                drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["Social Sharing Title"].ToString(), CommonMethods.faceBook);
            }
            //else
            //{
            //    drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["Title"].ToString(), CommonMethods.faceBook);
            //}

            if (!string.IsNullOrEmpty(itmContext.Fields["Social Sharing Description"].ToString()))
            {
                drrepEvents["FaceBookContent"] = cmObj.BuildString(itmContext.Fields["Social Sharing Description"].ToString(), CommonMethods.faceBook); ; //+ " " + hostName + LinkManager.GetItemUrl(itmContext);

                strBannerVideoURL = itmContext.Fields["Social Sharing Description"].ToString();
            }
            //else
            //{
            //    inputHTML = Sitecore.Web.UI.WebControls.FieldRenderer.Render(itmContext, "Description");// itmContext.Fields["Blurb"].ToString();
            //    drrepEvents["FaceBookContent"] = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim() + " " + hostName + LinkManager.GetItemUrl(itmContext);
            //}
            Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Thumb Image"]);
            string fbimgSrc = fbimgField.Src;
            if (!fbimgSrc.Equals(""))
            {
                drrepEvents["FaceBookThumbNail"] = hostName + "/" + fbimgSrc;
            }
            else
            {
                Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Story Image"]);
                if (!string.IsNullOrEmpty(staticimgField.Src))
                {
                    drrepEvents["FaceBookThumbNail"] = hostName + "/" + staticimgField.Src;
                }
                else
                {
                    drrepEvents["FaceBookThumbNail"] = "";
                }

            }

            if (!string.IsNullOrEmpty(itmContext.Fields["Social Sharing Description"].ToString()) && !string.IsNullOrWhiteSpace(itmContext.Fields["Social Sharing Description"].ToString()))
                TwitterContent = itmContext.Fields["Social Sharing Description"].ToString();
            //else
            //{
            //    inputHTML = Sitecore.Web.UI.WebControls.FieldRenderer.Render(itmContext, "Description");// itmContext.Fields["Blurb"].ToString();
            //    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
            //    if (noHTML.Length > 120)
            //        TwitterContent = noHTML.Substring(0, 119);
            //    else
            //        TwitterContent = noHTML.Substring(0, noHTML.Length);
            //}


            if (!string.IsNullOrEmpty(itmContext.Fields["Banner Video URL"].ToString()))
            {
                strBannerVideoURL = itmContext.Fields["Banner Video URL"].ToString() + " <br /> " + strBannerVideoURL; //+ " " + hostName + LinkManager.GetItemUrl(itmContext);
                strTwtBannerVideoURL = itmContext.Fields["Banner Video URL"].ToString();
            }

            drrepEvents["TwitterBannerVideoURL"] = strTwtBannerVideoURL;
            drrepEvents["BannerVideoURL"] = cmObj.BuildString(strBannerVideoURL, CommonMethods.faceBook);
            drrepEvents["TwitterContent"] = cmObj.BuildString(TwitterContent, CommonMethods.faceBook); ;



            drrepEvents["Id"] = itmContext.ID;

            // Response.Write(itmContext.Name);
            //Dylans Singapore Spirit  Eddys Singapore Spirit   Jun Yuans Singapore Spirit  Campaign Story 4 

            if (itmContext.Name == "Dylans Singapore Spirit")
            {
                drrepEvents["PostFacebookShare"] = "ga('send', 'event', 'Dylan Singapore Spirit', 'Click', 'Dylan Singapore Spirit - Post - Facebook Share');";
                drrepEvents["PostTwitterShare"] = "ga('send', 'event', 'Dylan Singapore Spirit', 'Click', 'Dylan Singapore Spirit - Post - Twitter Share');";

                drrepEvents["VideoFacebookShare"] = "ga('send', 'event', 'Dylan Singapore Spirit', 'Click', 'Dylan Singapore Spirit - Video - Facebook Share');";
                drrepEvents["VideoTwitterShare"] = "ga('send', 'event', 'Dylan Singapore Spirit', 'Click', 'Dylan Singapore Spirit - Video - Twitter Share');";

            }
            else if (itmContext.Name == "Eddys Singapore Spirit")
            {
                drrepEvents["PostFacebookShare"] = "ga('send', 'event', 'Eddy Singapore Spirit', 'Click', 'Eddy Singapore Spirit - Post - Facebook Share');";
                drrepEvents["PostTwitterShare"] = "ga('send', 'event', 'Eddy Singapore Spirit', 'Click', 'Eddy Singapore Spirit - Post - Twitter Share');";

                drrepEvents["VideoFacebookShare"] = "ga('send', 'event', 'Eddy Singapore Spirit', 'Click', 'Eddy Singapore Spirit - Video - Facebook Share');";
                drrepEvents["VideoTwitterShare"] = "ga('send', 'event', 'Eddy Singapore Spirit', 'Click', 'Eddy Singapore Spirit - Video - Twitter Share');";

            }
            else if (itmContext.Name == "Jun Yuans Singapore Spirit")
            {
                drrepEvents["PostFacebookShare"] = "ga('send', 'event', 'Jun Yuan Singapore Spirit', 'Click', 'Jun Yuan Singapore Spirit - Post - Facebook Share');";
                drrepEvents["PostTwitterShare"] = "ga('send', 'event', 'Jun Yuan Singapore Spirit', 'Click', 'Jun Yuan Singapore Spirit - Post - Twitter Share');";

                drrepEvents["VideoFacebookShare"] = "ga('send', 'event', 'Jun Yuan Singapore Spirit', 'Click', 'Jun Yuan Singapore Spirit - Video - Facebook Share');";
                drrepEvents["VideoTwitterShare"] = "ga('send', 'event', 'Jun Yuan Singapore Spirit', 'Click', 'Jun Yuan Singapore Spirit - Video - Twitter Share');";
            }
            else if (itmContext.Name == "Campaign Story 4")
            {
                drrepEvents["PostFacebookShare"] = "ga('send', 'event', 'Official Story 4', 'Click', 'Official Story 4 - Post - Facebook Share');";
                drrepEvents["PostTwitterShare"] = "ga('send', 'event', 'Official Story 4', 'Click', 'Official Story 4 - Post - Twitter Share');";

                drrepEvents["VideoFacebookShare"] = "ga('send', 'event', 'Official Story 4', 'Click', 'Official Story 4 - Video - Facebook Share');";
                drrepEvents["VideoTwitterShare"] = "ga('send', 'event', 'Official Story 4', 'Click', 'Official Story 4 - Video - Twitter Share');";
            }





            dtrepEvents.Rows.Add(drrepEvents);
            //   Response.Write("Count"+dtrepEvents.Rows.Count);
            //repsocialSharingBottom.DataSource = dtrepEvents;
            //repsocialSharingBottom.DataBind();
            repsocialSharingTop.DataSource = dtrepEvents;
            repsocialSharingTop.DataBind();



            //}
            //catch (Exception exSub1)
            //{
            //    Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            //}
        }
    }
}