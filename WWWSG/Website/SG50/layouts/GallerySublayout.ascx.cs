﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using Sitecore.Data.Fields;
using System.Text.RegularExpressions;

namespace Layouts.Gallerysublayout
{

    /// <summary>
    /// Summary description for GallerysublayoutSublayout
    /// </summary>
    public partial class GallerysublayoutSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!IsPostBack)
                {
                    SocialSharing();
                    //  FillAPPS();
                    TotalAPPS();
                }
            }
            // Put user code to initialize the page here
        }

        private void SocialSharing()
        {
            DataTable dtSocialSharing = new DataTable();
            dtSocialSharing.Columns.Add("Id", typeof(string));
            dtSocialSharing.Columns.Add("SocialShareTitle", typeof(string));
            dtSocialSharing.Columns.Add("SocialShareContent", typeof(string));
            dtSocialSharing.Columns.Add("ItemName", typeof(string));
            dtSocialSharing.Columns.Add("href", typeof(string));
            dtSocialSharing.Columns.Add("HostName", typeof(string));
            dtSocialSharing.Columns.Add("AccessToken", typeof(string));
            dtSocialSharing.Columns.Add("EventUrl", typeof(string));
            dtSocialSharing.Columns.Add("SocialSharingThumbNail", typeof(string));
            DataRow drSocialSharing;

            //  Item itmSocialSharing = SG50Class.web.GetItem("{5E04DC6C-B5E2-4313-B0D0-46B7803085D7}");
            drSocialSharing = dtSocialSharing.NewRow();
            drSocialSharing["HostName"] = hostName + "/";
            drSocialSharing["AccessToken"] = accessToken;
            drSocialSharing["href"] = LinkManager.GetItemUrl(itmcontext).ToString();
            drSocialSharing["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(itmcontext).ToString();
            if (!string.IsNullOrEmpty(itmcontext.Fields["Social Sharing Title"].ToString()))
            {
                drSocialSharing["SocialShareTitle"] = cmObj.BuildString(itmcontext.Fields["Social Sharing Title"].ToString(), CommonMethods.faceBook);
            }
            //else
            //{
            //    drSocialSharing["SocialShareTitle"] = cmObj.BuildString(itmcontext.Fields["Title"].ToString(), CommonMethods.faceBook);
            //}
            if (!string.IsNullOrEmpty(itmcontext.Fields["Social Sharing Description"].ToString()))
            {
                drSocialSharing["SocialShareContent"] = cmObj.BuildString(itmcontext.Fields["Social Sharing Description"].ToString(), CommonMethods.faceBook);
            }
            //else
            //{
            //    drSocialSharing["SocialShareContent"] = cmObj.BuildString(itmcontext.Fields["Description"].ToString(), CommonMethods.faceBook);
            //}


            Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmcontext.Fields["SocialSharingThumbImage"]);
            string fbimgSrc = fbimgField.Src;
            if (!fbimgSrc.Equals(""))
            {
                drSocialSharing["SocialSharingThumbNail"] = hostName + "/" + fbimgSrc;
            }
            else
            {
                //Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)itmcontext.Fields["Banner Image"]);
                //if (!string.IsNullOrEmpty(staticimgField.Src))
                //{
                //    drSocialSharing["SocialSharingThumbNail"] = hostName + "/" + staticimgField.Src;
                //}
                //else
                //{
                drSocialSharing["SocialSharingThumbNail"] = "";
                //}

            }

            dtSocialSharing.Rows.Add(drSocialSharing);
            repsocialSharingTop.DataSource = dtSocialSharing;
            repsocialSharingTop.DataBind();
        }


        public void TotalAPPS()
        {
            try
            {
                DataTable dtApp = new DataTable();

                dtApp.Columns.Add("Sno", typeof(string));
                dtApp.Columns.Add("Title", typeof(string));
                dtApp.Columns.Add("Description", typeof(string));
                dtApp.Columns.Add("Version", typeof(string));
                dtApp.Columns.Add("imgSrc", typeof(string));
                dtApp.Columns.Add("AndroidURL", typeof(string));
                dtApp.Columns.Add("IphoneURL", typeof(string));
                DataRow drAPP;
                IEnumerable<Item> Pulsechild = web.GetItem("{72FA46BB-27D4-4ED3-95A7-293BF6125834}").GetChildren();
                Sitecore.Collections.ChildList AppsCount = web.GetItem("{72FA46BB-27D4-4ED3-95A7-293BF6125834}").GetChildren();

                int count = 1;
                StringBuilder sbBannerText = new StringBuilder();
                foreach (Item a in Pulsechild)
                {
                    drAPP = dtApp.NewRow();
                    drAPP["Sno"] = count;
                    drAPP["Title"] = a.Fields["Title"].ToString();
                    drAPP["Description"] = a.Fields["Description"].ToString();
                    drAPP["Version"] = a.Fields["version"].ToString();

                    Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                    if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
                    {
                        string mainimgSrc = mainImgField.Src;
                        if (!string.IsNullOrEmpty(mainimgSrc))
                        {
                            drAPP["imgSrc"] = mainimgSrc;
                        }
                    }

                    drAPP["AndroidURL"] = a.Fields["Android APP URL"].ToString();
                    drAPP["IphoneURL"] = a.Fields["Iphone APP URL"].ToString();
                    dtApp.Rows.Add(drAPP);

                    count++;
                }

                PagedDataSource pgsource = new PagedDataSource();
                pgsource.DataSource = dtApp.DefaultView;

                //pgsource.PageSize = 5;
                pgsource.AllowPaging = true;
                pgsource.PageSize = 5;
                // pgsource.PageSize = dtApp.Rows.Count;
                //Response.Write(dtApp.Rows.Count);
                pgsource.CurrentPageIndex = PageNumber;
                DataTable dtNewCollectiblePaging = new DataTable();
                dtNewCollectiblePaging.Columns.Add("ID", typeof(string));
                dtNewCollectiblePaging.Columns.Add("style", typeof(string));
                dtNewCollectiblePaging.Columns.Add("Enable", typeof(Boolean));

                if (pgsource.PageCount > 1)
                {
                    DataRow drNewCollectiblePaging;
                    //Style="padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important"
                    string css = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;";
                    string Activecss = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important";
                    rptPaging.Visible = true;
                    System.Collections.ArrayList pages = new System.Collections.ArrayList();
                    for (int i = 0; i <= pgsource.PageCount - 1; i++)
                    {
                        drNewCollectiblePaging = dtNewCollectiblePaging.NewRow();
                        drNewCollectiblePaging["ID"] = (i + 1).ToString();
                        if (pgsource.CurrentPageIndex + 1 == i + 1)
                        {
                            drNewCollectiblePaging["style"] = Activecss;
                            drNewCollectiblePaging["Enable"] = false;
                        }
                        else
                        {
                            drNewCollectiblePaging["style"] = css;
                            drNewCollectiblePaging["Enable"] = true;
                        }

                        dtNewCollectiblePaging.Rows.Add(drNewCollectiblePaging);
                        //pages.Add((i + 1).ToString());
                    }

                    rptPaging.DataSource = dtNewCollectiblePaging;
                    rptPaging.DataBind();
                }
                //Finally, set the datasource of the repeoater
                rpApp.DataSource = pgsource;
                //RepCourse.DataBind();
                rpApp.DataBind();
            }

            catch (Exception exSub1)
            {
                throw exSub1;
            }
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return System.Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["PageNumber"] = value; }
        }

        protected void rptPaging_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = System.Convert.ToInt32(e.CommandArgument) - 1;
            TotalAPPS();
        }
        //public void FillAPPS()
        //{
        //    try
        //    {
        //        DataTable dtAppL = new DataTable();

        //        dtAppL.Columns.Add("Sno", typeof(string));
        //        dtAppL.Columns.Add("Title", typeof(string));
        //        dtAppL.Columns.Add("Description", typeof(string));
        //        dtAppL.Columns.Add("Version", typeof(string));
        //        dtAppL.Columns.Add("imgSrc", typeof(string));
        //        dtAppL.Columns.Add("AndroidURL", typeof(string));
        //        dtAppL.Columns.Add("IphoneURL", typeof(string));

        //        DataTable dtAppR = new DataTable();

        //        dtAppR.Columns.Add("Sno", typeof(string));
        //        dtAppR.Columns.Add("Title", typeof(string));
        //        dtAppR.Columns.Add("Description", typeof(string));
        //        dtAppR.Columns.Add("Version", typeof(string));
        //        dtAppR.Columns.Add("imgSrc", typeof(string));
        //        dtAppR.Columns.Add("AndroidURL", typeof(string));
        //        dtAppR.Columns.Add("IphoneURL", typeof(string));



        //        DataRow drAPPL;

        //        DataRow drAPPR;
        //        IEnumerable<Item> Pulsechild = web.GetItem("{72FA46BB-27D4-4ED3-95A7-293BF6125834}").GetChildren();

        //        Sitecore.Collections.ChildList AppsCount = web.GetItem("{72FA46BB-27D4-4ED3-95A7-293BF6125834}").GetChildren();

        //        int count = 1;
        //        int TotalCount = 0;
        //        int TotalCount1 = 0;
        //        if (Pulsechild != null)
        //        {
        //            TotalCount = AppsCount.Count;
        //            TotalCount1 = AppsCount.Count;
        //        }

        //        if (TotalCount1 == 0)
        //            APPSection.Visible = false;
        //        else
        //            APPSection.Visible = true;


        //        TotalCount = TotalCount1 / 2;


        //        if (TotalCount1 % 2 != 0)
        //        {
        //            TotalCount = TotalCount + 1;
        //        }

        //        StringBuilder sbBannerText = new StringBuilder();
        //        foreach (Item a in Pulsechild)
        //        {


        //            if (count <= TotalCount)
        //            {
        //                drAPPL = dtAppL.NewRow();
        //                drAPPL["Sno"] = count;
        //                drAPPL["Title"] = a.Fields["Title"].ToString();
        //                drAPPL["Description"] = a.Fields["Description"].ToString();
        //                drAPPL["Version"] = a.Fields["version"].ToString();

        //                Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
        //                if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
        //                {
        //                    string mainimgSrc = mainImgField.Src;
        //                    if (!string.IsNullOrEmpty(mainimgSrc))
        //                    {
        //                        drAPPL["imgSrc"] = mainimgSrc;
        //                    }
        //                }

        //                drAPPL["AndroidURL"] = a.Fields["Android APP URL"].ToString();
        //                drAPPL["IphoneURL"] = a.Fields["Iphone APP URL"].ToString();
        //                dtAppL.Rows.Add(drAPPL);



        //            }
        //            else
        //            {


        //                drAPPR = dtAppR.NewRow();
        //                drAPPR["Sno"] = count;
        //                drAPPR["Title"] = a.Fields["Title"].ToString();
        //                drAPPR["Description"] = a.Fields["Description"].ToString();
        //                drAPPR["Version"] = a.Fields["version"].ToString();

        //                Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
        //                if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
        //                {
        //                    string mainimgSrc = mainImgField.Src;
        //                    if (!string.IsNullOrEmpty(mainimgSrc))
        //                    {
        //                        drAPPR["imgSrc"] = mainimgSrc;
        //                    }
        //                }

        //                drAPPR["AndroidURL"] = a.Fields["Android APP URL"].ToString();
        //                drAPPR["IphoneURL"] = a.Fields["Iphone APP URL"].ToString();
        //                dtAppR.Rows.Add(drAPPR);

        //            }
        //            count++;
        //        }
        //        rpLeft.DataSource = dtAppL;
        //        rpLeft.DataBind();

        //        rpRight.DataSource = dtAppR;
        //        rpRight.DataBind();

        //    }
        //    catch (Exception exSub1)
        //    {
        //        throw exSub1;
        //    }
        //}

    }
}