﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Linq;
using Sitecore.Data;
using Sitecore.Links;
using System.Web.UI;
using System.Web;

namespace Layouts.Searchresultsublayout
{

    /// <summary>
    /// Summary description for SearchresultsublayoutSublayout
    /// </summary>
    public partial class SearchresultsublayoutSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");

        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            string queryStringValue = Request.QueryString["searchWord"];
            Session["searchWord"] = queryStringValue;
            FillPulse(queryStringValue);
        }

        private void FillPulse(string Word)
        {
            try
            {
                DataTable dtPulse = new DataTable(); DataRow drPulse;
                dtPulse.Columns.Add("item", typeof(Item));
                dtPulse.Columns.Add("Title", typeof(string));
                dtPulse.Columns.Add("Description", typeof(string));
                dtPulse.Columns.Add("href", typeof(string));

                Item itmPulses = web.GetItem("{83DBF609-220C-45B2-9A27-D5411E98B8E3}");
                IEnumerable<Item> NewPulsechild = itmPulses.GetChildren();

                foreach (Item a in NewPulsechild)
                {
                    if (a.Fields["Title"].ToString().ToLower().Contains(Word.ToLower()) || a.Fields["Description"].ToString().ToLower().Contains(Word.ToLower()))
                    {

                        drPulse = dtPulse.NewRow();
                        drPulse["item"] = a;
                        drPulse["title"] = a.Fields["Title"].ToString();
                        if (a.Fields["Description"].ToString().Length > 400)
                        {
                            drPulse["description"] = a.Fields["Description"].ToString().Substring(0, 400) + "....";
                        }
                        else
                        {
                            drPulse["description"] = a.Fields["Description"].ToString();
                        }

                        drPulse["href"] = LinkManager.GetItemUrl(a).ToString();// +"/" + a.Name;

                        dtPulse.Rows.Add(drPulse);
                    }
                }
                //Response.Write("raju");
                PagedDataSource pgsource = new PagedDataSource();
                pgsource.DataSource = dtPulse.DefaultView;
                pgsource.AllowPaging = true;
                pgsource.PageSize = 30;
                pgsource.CurrentPageIndex = PageNumber;

                if (dtPulse.Rows.Count == 0)
                {
                    lblResult.Visible = true;
                    lblResult.Text = "No search records found..";
                }
                else
                {
                    lblResult.Visible = false;
                }


                if (PageNumber == 0)
                    PrevControl.Enabled = false;
                else
                    PrevControl.Enabled = true;

                //int pageNationCount = 0;
                //pageNationCount = (dtPulse.Rows.Count / pgsource.PageSize) - 1;
                if (PageNumber == pgsource.PageCount - 1)
                    NextControl.Enabled = false;
                else
                    NextControl.Enabled = true;

                DataTable dtNewCollectiblePaging = new DataTable();
                dtNewCollectiblePaging.Columns.Add("ID", typeof(string));
                dtNewCollectiblePaging.Columns.Add("style", typeof(string));
                dtNewCollectiblePaging.Columns.Add("Enable", typeof(Boolean));

                if (pgsource.PageCount > 1)
                {
                    DataRow drNewCollectiblePaging;
                    //Style="padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important"
                    string css = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;";
                    string Activecss = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important";
                    rptPaging.Visible = true;
                    System.Collections.ArrayList pages = new System.Collections.ArrayList();
                    for (int i = 0; i <= pgsource.PageCount - 1; i++)
                    {
                        drNewCollectiblePaging = dtNewCollectiblePaging.NewRow();
                        drNewCollectiblePaging["ID"] = (i + 1).ToString();
                        if (pgsource.CurrentPageIndex + 1 == i + 1)
                        {
                            drNewCollectiblePaging["style"] = Activecss;
                            drNewCollectiblePaging["Enable"] = false;
                        }
                        else
                        {
                            drNewCollectiblePaging["style"] = css;
                            drNewCollectiblePaging["Enable"] = true;
                        }

                        dtNewCollectiblePaging.Rows.Add(drNewCollectiblePaging);
                    }
                    rptPaging.DataSource = dtNewCollectiblePaging;
                    rptPaging.DataBind();
                }
                else
                {
                    rptPaging.Visible = false;
                    if (pgsource.PageCount <= 1)
                    {
                        PrevControl.Visible = NextControl.Visible = false;
                    }
                }

                repNewCollectibles.DataSource = pgsource;
                repNewCollectibles.DataBind();
            }
            catch (Exception exSub1)
            {
                //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
                Response.Write(exSub1.Message + "2." + exSub1.InnerException);
            }

        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return System.Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["PageNumber"] = value; }
        }

        protected void PrevControl_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;
            FillPulse(string.Empty);
        }

        protected void NextControl_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;
            FillPulse(string.Empty);
        }

        protected void rptPaging_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = System.Convert.ToInt32(e.CommandArgument) - 1;
            FillPulse(string.Empty);
        }

        protected void imgSearchButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Session["search"] = txtSearch.Text.Trim();
                if (Request.QueryString["searchWord"].ToString() != txtSearch.Text.Trim())
                {
                    Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri.Split('?')[0] + "?searchWord=" + txtSearch.Text.Trim());
                }
            }
            catch (Exception msg)
            {
                throw msg;
            }
        }

    }
}