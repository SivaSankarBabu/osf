﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" %>

<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
   
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
   
    <link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/SG50/include/css/sg50grc.css" type="text/css" />
    <script src="/SG50/include/js/jquery-1.11.3.min.js"></script>
    <script src="/SG50/include/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/SG50/include/js/jquery.min_Home.js"></script>
    <script src="/SG50/include/js/masonry.pkgd.min.js"></script>
    <script src="/SG50/include/js/imagesloaded.pkgd.min.js"></script>
    <script src="/SG50/include/js/sg50grc.js"></script>

    <sc:VisitorIdentification runat="server" />
    
     <script>
         (function(i, s, o, g, r, a, m) {
             i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() {
                 (i[r].q = i[r].q || []).push(arguments)
             }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
         })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
         ga('create', 'UA-46719765-2', 'singapore50.sg');
         ga('send', 'pageview');
    </script>
</head>
<body>
    <form method="post" runat="server" id="mainform">
        <div class="container" id="homePage">
            <div id="header">
                <sc:Placeholder ID="plhHeader" runat="server" Key="Header"></sc:Placeholder>
            </div>
            <div class="content">
                <sc:Placeholder ID="plhContent" runat="server" Key="main"></sc:Placeholder>
            </div>
            <div class="footer">
                <sc:Placeholder ID="plhFooter" runat="server" Key="Footer"></sc:Placeholder>
            </div>
        </div>
    </form>
</body>
</html>
