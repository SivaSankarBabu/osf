﻿using System;
using Sitecore.Data.Items;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Links;
using Sitecore.Diagnostics;

namespace Layouts.Top_navigation_sublayout
{

    /// <summary>
    /// Summary description for Top_navigation_sublayoutSublayout
    /// </summary>
    public partial class Top_navigation_sublayoutSublayout : System.Web.UI.UserControl
    {
        Item itmContext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
        }

        protected string getHomeLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Home_Item_ID));
            }
            return link;
        }

        protected string getCelebrationFundLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Celebration_Fund_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Celebration_Fund_Item_ID));
            }
            return link;
        }

        protected string getCelebrationIdeasLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID) != null)
            {
                //link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID));
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID));
            }

            return link;
        }

        protected string getICONLink()
        {
            string link = "";
            if (ICONClass.web.GetItem(ICONClass.str_Home_Page_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(ICONClass.web.GetItem(ICONClass.str_Home_Page_Item_ID));
            }
            return link;
        }

        protected string getPressLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Press_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Press_Item_ID));
            }
            return link;
        }

        protected string getWhatsOnLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Events_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Events_Item_ID));
            }
            return link;
        }

        protected string getProjectsLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Projects_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Projects_Item_ID));
            }
            return link;
        }

        protected string getCelebrationsFundEventsLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Celebrations_Events_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Celebrations_Events_Item_ID));
            }
            return link;
        }

        protected string getCollectiblesLink()
        {
            string link = "";
            if (SG50Class.web.GetItem("{5E04DC6C-B5E2-4313-B0D0-46B7803085D7}") != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem("{5E04DC6C-B5E2-4313-B0D0-46B7803085D7}"));
            }
            return link;
        }
        protected string getGalleryLink()
        {
            string link = "";
            if (SG50Class.web.GetItem("{923F1A49-AAD7-4000-9680-BF4F23374426}") != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem("{923F1A49-AAD7-4000-9680-BF4F23374426}"));
            }
            return link;
        }



    }
}
