﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Top_navigation_sublayout.Top_navigation_sublayoutSublayout" CodeFile="~/SG50/layouts/Top Navigation Sublayout.ascx.cs" %>
<%--<script type="text/javascript" src="/SG50/include/js/common.js"></script>--%>


<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">

<div class="header">
    <div class="responsiveMenu">
        <div class="menuBtn">
            <div class="SG50Logo">
                <a href="<%= getHomeLink() %>">
                    <span>
                        <img src="/SG50/images/PG/logo.png"></span>
                </a>
            </div>
            <a onclick="ShowMenu();" class="m-nav-icon">MENU</a>
            <div class="social-NewStyle">
                <div class="facebookMobile">
                    <a href="https://www.facebook.com/sg2015" target="_blank">
                        <span class="NewFacebook"></span>
                    </a>
                </div>
                <div class="twitterMobile">
                    <a href="https://twitter.com/SG50" target="_blank">
                        <span class="NewTwitter"></span>
                    </a>
                </div>

                <div class="youtubeMobile">
                    <a href="https://www.youtube.com/user/ilovesingapore50" target="_blank">
                        <span class="NewYouTube"></span>
                    </a>
                </div>
                <div class="facebookMobile">
                    <a href="https://www.instagram.com/singapore50/" target="_blank">
                        <span class="NewInstagram"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="menu_nav" id="nav">
            <div class="SG50Logo">
                <a href="<%= getHomeLink() %>">
                    <span>
                        <img src="/SG50/images/PG/logo.png"></span>
                </a>
            </div>
            <div class="ABOUT">
                <a href="/SG50/About">
                    <span>ABOUT</span>
                </a>
                <div class="mediumMenu">
                    <span class="innerMenuBg">
                        <a href="/SG50/About#DivLittleDot" id="GUIDELINESlnk"><span>BRAND GUIDELINES</span></a>
                        <a href="/SG50/About#divinitiatives" id="INITIATIVESlnk"><span>INITIATIVES</span></a>
                        <a href="/SG50/About#DivMeetthecommitte" id="COMMITTEElnk"><span>COMMITTEE</span></a>
                    </span>
                </div>
            </div>
            <div class="whatson">
                <a href="<%= getWhatsOnLink() %>">
                    <span>WHAT'S ON</span>
                </a>
            </div>
            <div class="PULSE">
                <a href="/sg50/pulse/">
                    <span>PULSE</span>
                </a>
            </div>
            <div class="celebrationFund">
                <a href="<%= getCelebrationsFundEventsLink() %>">
                    <span>CELEBRATION FUND & IDEAS</span>
                </a>
                <div class="mediumMenu">
                    <span class="innerMenuBg">
                        <a href="/sg50/Celebration Fund.aspx">
                            <span>WHAT IS IT?</span>
                        </a>
                       <%-- <a href="/SG50/Apply For Funding.aspx">
                            <span>APPLY FOR FUNDING</span>
                        </a>--%>
                        <a href="<%= getCelebrationsFundEventsLink() %>">
                            <span>GROUND-UP PROJECTS</span>
                        </a>
                        <a href="<%= getCelebrationIdeasLink() %>">
                            <span>CELEBRATION IDEAS</span>
                        </a>

                    </span>
                </div>
            </div>
            <div class="Gallery">
                <a href="/SG50/GalleryLanding/">
                    <span>GALLERY</span>
                </a>
            </div>
            <div class="Collectibles">
                <a href="<%= getCollectiblesLink() %>">
                    <span>COLLECTIBLES</span>
                </a>
            </div>
            <div class="press">
                <a href="https://singapore50.mynewsdesk.com/" target="_blank">
                    <span>PRESS</span>

                </a>

            </div>
        </div>
    </div>

    <div class="social_nav social-NewStyle">
        <div class="facebook">
            <a href="https://www.facebook.com/sg2015" target="_blank">
                <span class="NewFacebook"></span>
            </a>
        </div>
        <div class="twitter">
            <a href="https://twitter.com/SG50" target="_blank">
                <span class="NewTwitter"></span>
            </a>
        </div>
        <div>
            <a href="https://www.youtube.com/user/ilovesingapore50" target="_blank">
                <span class="NewYouTube"></span>
            </a>
        </div>
        <div>
            <a href="https://www.instagram.com/singapore50/" target="_blank">
                <span class="NewInstagram"></span>
            </a>
        </div>
    </div>
    <script type="text/javascript">


        var animation = true;
        $(document).ready(function () {

            $('#nav div').hover(
                function () {
                    //show its submenu
                    $('div', this).fadeIn(700);
                    $(this).find("a:first").addClass("active_nav");
                    //$(this).find("a:first").css('background','url(images/DMP/mobile/menu_list2.png) no-repeat 95% 50%');
                },
                function () {
                    //hide its submenu
                    //$('div', this).slideUp(5);
                    $('div', this).hide();
                    $(this).find("a:first").removeClass("active_nav");
                    //$(this).find("a:first").css('background','url(images/DMP/mobile/menu_list.png) no-repeat 95% 50%');
                }
            );

            $("#nav a").each(function () {
                if ($(this).attr("href") == window.location.pathname) {
                    $(this).addClass("selected");
                }
            });

        });

    </script>
