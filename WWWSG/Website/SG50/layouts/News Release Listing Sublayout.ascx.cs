﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Layouts.News_release_listing_sublayout
{

    /// <summary>
    /// Summary description for News_release_listing_sublayoutSublayout
    /// </summary>
    public partial class News_release_listing_sublayoutSublayout : System.Web.UI.UserControl
    {
        int latestCount = 5;
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            FillrepPressRelease();
            FillrepLatest();
        }

        private void FillrepLatest()
        {
            try {
                DataTable dtrepLatest = new DataTable();
                dtrepLatest.Columns.Add("item", typeof(Item));
                dtrepLatest.Columns.Add("href", typeof(string));

                DataRow drrepLatest;

                Item itmPress = SG50Class.web.GetItem(SG50Class.str_Press_Item_ID);

                IEnumerable<Item> itmIEnumPressReleases = (from a in itmPress.Axes.GetDescendants()
                                                          where
                                                          a.TemplateID.ToString().Equals(SG50Class.str_Press_Details_Page_Template_ID)
                                                           select a).Take(latestCount);

                foreach (Item a in itmIEnumPressReleases)
                {
                    try
                    {
                        drrepLatest = dtrepLatest.NewRow();

                        drrepLatest["item"] = a;
                        drrepLatest["href"] = LinkManager.GetItemUrl(a).ToString();

                        dtrepLatest.Rows.Add(drrepLatest);
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("News_release_listing_sublayout FillrepLatest exSub1 : " + exSub1.ToString(), this);
                    }
                }
                repLatest.DataSource = dtrepLatest;
                repLatest.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("News_release_listing_sublayout FillrepLatest exMain : " + exMain.ToString(), this);
            }
        }

        private void FillrepPressRelease()
        {
            try
            {
                DataTable dtrepPressRelease = new DataTable();
                dtrepPressRelease.Columns.Add("cssClass", typeof(string));
                dtrepPressRelease.Columns.Add("item", typeof(Item));
                dtrepPressRelease.Columns.Add("href", typeof(string));
                
                DataRow drrepPressRelease;

                Item itmPress = SG50Class.web.GetItem(SG50Class.str_Press_Item_ID);

                IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                             where
                                                             a.TemplateID.ToString().Equals(SG50Class.str_Press_Details_Page_Template_ID)
                                                             select a;

                foreach (Item a in itmIEnumPressReleases)
                {
                    try
                    {
                        drrepPressRelease = dtrepPressRelease.NewRow();

                        drrepPressRelease["cssClass"] = "a";
                        drrepPressRelease["item"] = a;
                        drrepPressRelease["href"] = LinkManager.GetItemUrl(a).ToString();
                        
                        dtrepPressRelease.Rows.Add(drrepPressRelease);
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("News_release_listing_sublayout FillrepPressRelease exSub1 : " + exSub1.ToString(), this);
                    }
                }

                //GenerateResults(dtrepPressRelease);
                repPressRelease.DataSource = dtrepPressRelease;
                repPressRelease.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("News_release_listing_sublayout FillrepPressRelease exMain : " + exMain.ToString(), this);
            }
        }
    }
}