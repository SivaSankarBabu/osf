﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" %>

<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>SG50</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;">

	<meta property="fb:admins" content="1036692083" />
    <meta property="fb:admins" content="1068841049" />
    <meta property="fb:app_id" content="565151546893256" />
	
	<sc:Sublayout runat="server" RenderingID="{6D5E4173-FA8F-44CC-9B34-EDE491E0ECF5}" Path="/SG50/layouts/Meta Tag Sublayout.ascx" ID="sublayoutMetaTag" placeholder="contentHeader"></sc:Sublayout>
	<meta property="og:image" content="http://sg50devcm.wph.sg/SG50/images/imglogo2.png"/>
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/jquery.fancybox-1.3.4.css" />
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="/SG50/include/css/jquery.bxslider.css" />

	
    <script type="text/javascript" src="/SG50/include/js/jquery.min.js"></script>
	<script type="text/javascript" src="/SG50/include/js/jquery.carouFredSel-6.2.0-packed.js"></script>
    <script type="text/javascript" src="/SG50/include/js/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="/SG50/include/js/video.js"></script>
    <script type="text/javascript" src="/SG50/include/js/jquery.bxslider.min.js"></script>

    		<script type="text/javascript">
				
    		    $(function () {
    		        $('#slider').carouFredSel({
    		            width: '100%',
    		            align: false,
    		            items: 3,
    		            items: {
    		                width: $('#wrapper').width() * 0.05,
    		                height: 715,
    		                visible: 1,
    		                minimum: 1
    		            },
    		            scroll: {
    		                items: 1,
    		                timeoutDuration: 10000,
    		                onBefore: function (data) {

    		                    //	find current and next slide
    		                    var currentSlide = $('.slide.active', this),
                                    nextSlide = data.items.visible,
                                    _width = $('#wrapper').width();

    		                    //	resize currentslide to small version
    		                    currentSlide.stop().animate({
    		                        width: _width * 0.05
    		                    });
    		                    currentSlide.removeClass('active');

    		                    //	hide current block
    		                    data.items.old.add(data.items.visible).find('.slide-block').stop().fadeOut();

    		                    //	animate clicked slide to large size
    		                    nextSlide.addClass('active');
    		                    nextSlide.stop().animate({
    		                        width: _width * 0.9
    		                    });
    		                },
    		                onAfter: function (data) {
    		                    //	show active slide block
    		                    data.items.visible.last().find('.slide-block').stop().fadeIn();
    		                }
    		            },
    		            onCreate: function (data) {

    		                //	clone images for better sliding and insert them dynamacly in slider
    		                var newitems = $('.slide', this).clone(true),
                                _width = $('#wrapper').width();

    		                $(this).trigger('insertItem', [newitems, newitems.length, false]);

    		                //	show images 
    		                $('.slide', this).fadeIn();
    		                $('.slide:first-child', this).addClass('active');
    		                $('.slide', this).width(_width * 0.05);

    		                //	enlarge first slide
    		                $('.slide:first-child', this).animate({
    		                    width: _width * 0.9
    		                },0);

    		                //	show first title block and hide the rest
    		                $(this).find('.slide-block').hide();
    		                $(this).find('.slide.active .slide-block').stop().fadeIn();
    		            },
    		            pagination: {
    		                container: '#pager',
    		                anchorBuilder: false
    		            }
    		        });

    		        //	Handle click events
    		        $('#slider').children().click(function () {
    		            $('#slider').trigger('slideTo', [this]);
    		        });

    		        //	Enable code below if you want to support browser resizing
    		        $(window).resize(function () {

    		            var slider = $('#slider'),
                            _width = $('#wrapper').width();

    		            //	show images
    		            slider.find('.slide').width(_width * 0.05);

    		            //	enlarge first slide
    		            slider.find('.slide.active').width(_width * 0.9);

    		            //	update item width config
    		            slider.trigger('configuration', ['items.width', _width * 0.05]);
    		        });

    		    });
		</script>

    <sc:VisitorIdentification runat="server" />
</head>
<body>
    <form method="post" runat="server" id="mainform">

        <div class="container" id="homePage">

            <div id="header">
                <sc:Placeholder ID="plhHeader" runat="server" Key="Header"></sc:Placeholder>
            </div>
            <div class="content">
                <sc:Placeholder ID="plhContent" runat="server" Key="Content"></sc:Placeholder>
            </div>
            <div class="footer">
                <sc:Placeholder ID="plhFooter" runat="server" Key="Footer"></sc:Placeholder>
            </div>

        </div>
		 <!--  FAQ Sub Page Start -->
    <div id="dvPopup">
        <a href="#" onclick="HideModalPopup('dvPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle">
            <p class="FontTitle1">ALL YOU NEED </p>
            <p class="FontTitle2">TO KNOW </p>
            <p class="FontTitle3">ABOUT SG50</p>
        </div>
        <div id="Qmark">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <div id="faq1ID" class="faqTitle" onclick="HideContent('faq1ID', 'faq1ConID'); return false;">
                    <p>
                        <a href="#" >
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        1. What is SG50?

                    </p>
                </div>
                <div id="faq1ConID" class="faqContents" style="display: none">
                    <p>SG50 is a nationwide effort to celebrate our country’s 50th birthday in 2015. That’s a huge milestone – 50 years of independence! This is a momentous event for Singaporeans to reflect on how far we’ve come together as a nation and people.
</p>
                </div>
                <div id="faq2ID" class="faqTitle" onclick="HideContent('faq2ID', 'faq2ConID'); return false;">
                    <p>
                        <a href="#" >
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        2. How long will the SG50 celebrations last?
                    </p>
                </div>
                <div id="faq2ConID" class="faqContents" style="display: none">
                    <p>The SG50 celebrations will span over a year, from January to December 2015. But we’re not waiting till 2015 to get started. 
</p>
                </div>
                <div id="faq3ID" class="faqTitle" onclick="HideContent('faq3ID', 'faq3ConID'); return false;">
                    <p>
                        <a href="#" >
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        3. How can Singaporeans get involved with SG50? 

                    </p>
                </div>
                <div id="faq3ConID" class="faqContents" style="display: none">
                    <p>This is our country, and our celebrations couldn’t be more meaningful if everyone got involved in a big way. <br>
Find out what your fellow Singaporeans are planning and <a href="http://www.singapore50.sg/celebrationideas.aspx"> tell us</a> how you can make it even more special.</p>
                </div>
                <div id="faq4ID" class="faqTitle" onclick="HideContent('faq4ID', 'faq4ConID'); return false;">
                    <p>
                        <a href="#" >
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        4. What does the SG50 committee do?</p>
                </div>
                <div id="faq4ConID" class="faqContents" style="display: none">
                    <p>SG50 hopes to reach out to as many Singaporeans as possible. To head up our Steering Committee, we’ve invited individuals from a variety of fields in the public, people and private sectors. The SG50 Steering Committee is led by Minister for Education, Heng Swee Keat; while the Programme Office is led by Acting Minister for Culture Community and Youth, Lawrence Wong. <br>
The Steering Committee is supported by five teams, which will encourage active community participation and involvement leading up to 2015. The five teams are: 
						
</p>
<p>
	<ul>
	<li> Education and Youth</li>
	<li>Culture and Community</li>
	<li>Economic and International</li>
	<li>Environment and Infrastructure</li>
	<li>Partnership</li> </ul>
</p>

<p><a href="http://www.singapore50.sg" >Find out</a> more about our committee members.</p>

                </div>
                <div id="faq5ID" class="faqTitle" onclick="HideContent('faq5ID', 'faq5ConID'); return false;">
                    <p>
                        <a href="#" >
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        5. What is the SG50 Celebration Fund?

                    </p>
                </div>
                <div id="faq5ConID" class="faqContents" style="display: none">
                    <p>To celebrate Singapore turning 50, we’re inviting all Singaporeans to come up with projects that express your pride, identity and love for our country. We want to help fund projects that will get Singaporeans excited about our 50th birthday. <br>

Got an idea? <a href="http://www.singapore50.sg/Celebration%20Fund.aspx" >Get the funds</a> to make it a reality. </p>
                </div>
                <div id="faq6ID" class="faqTitle" onclick="HideContent('faq6ID', 'faq6ConID'); return false;">
                    <p>
                        <a href="#" >
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        6. Who can apply for the SG50 Celebration Fund?
          </p>
                </div>
                <div id="faq6ConID" class="faqContents" style="display: none">
                    <p>The SG50 Celebration Fund is open to:</p>
<p><ul><li>Informal interest or community groups. Minimum of 2 individuals (at least one of whom is a Singaporean, aged 18 years and above)</li>
<li>Societies registered with the Registry of Societies (ROS) and; </li> 
<li> Companies registered with the Accounting and Corporate Regulatory Authority (ACRA)</li>
</ul>
</p>
<p>
Want to see if you qualify? Drop us an email at <a href="mailto:SG50CelebrationFund@Singapore50.sg" >SG50CelebrationFund@Singapore50.sg
</a></p>
                </div>
                <div id="faq7ID" class="faqTitle" onclick="HideContent('faq7ID', 'faq7ConID'); return false;">
                    <p>
                        <a href="#" >
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        7. How can I find out more about SG50?

                    </p>
                </div>
                <div id="faq7ConID" class="faqContents" style="display: none">
                    <p>If you have further questions, you can get in touch with us <a href="http://www.singapore50.sg/Contact%20Us.aspx">here.</a> </p>
                </div>
                <br />
                <br />

            </div>
        </div>
    </div>
    <!-- FAQ Sub Page End -->
	






	 <!--  FAQ Bottom Sub Page Start -->
    <div id="dvPopup2">
        <a href="#" onclick="HideModalPopup('dvPopup2'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitle2">
            <p class="FontTitle1">WHAT YOU MAY WANT  </p>
            <p class="FontTitle2">TO KNOW ABOUT THE</p>
            <p class="FontTitle3">SG50 CELEBRATION FUND.</p>
        </div>
        <div id="Qmark2">
            <img src="/SG50/images/celebration-faq/qmark.png" alt="" />
        </div>
        <div>
            <div class="faqBlock">
                <div id="faq1ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq1ID2', 'faq1ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        1.    Is there a closing date for SG50 Celebration Fund applications?
                    </p>
                </div>
                <div id="faq1ConID2" class="faqContents" style="display: none">
                    <p>        Yes. Application closes on 31 August 2015.</p>
                </div>
                <div id="faq2ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq2ID2', 'faq2ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       2.    Who can apply for the SG50 Celebration Fund?
                    </p>
                </div>
                <div id="faq2ConID2" class="faqContents" style="display: none">
                    <p>You can either apply as (i) Groups or (ii) Organisations. Under the Groups category, two individuals (at least one of whom is a Singaporean, aged 18 years and above) are required for the application. Societies registered with the Registry of Societies (ROS) and companies registered with the Accounting and Corporate Regulatory Authority (ACRA) can apply under the Organisations category. Government Ministries, Departments, Organs of State and statutory boards will however, not be eligible to apply for the fund. </p>
                </div>
                <div id="faq3ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq3ID2', 'faq3ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        3.    What expenses are covered under the SG50 Celebration Fund?
                    </p>
                </div>
                <div id="faq3ConID2" class="faqContents" style="display: none">
                    <p>Only expenses directly incurred in the implementation of the project will be funded. These do not include group/organisation’s start-up costs, capital expenditure, cash prizes, and fundraising expenses. A project report and an income and expenditure statement will need to be submitted within three months after your project’s completion.</p>
					<p>Find out what your fellow Singaporeans are planning and <a href="http://www.singapore50.sg/celebrationideas.aspx">tell us</a> how you can make it even more special.</p>
				</div>
                <div id="faq4ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq4ID2', 'faq4ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        
                    4.    Can I submit applications for more than one project?

                    </p>
                </div>
                <div id="faq4ConID2" class="faqContents" style="display: none">
                    <p>Yes. There is no limit to the number of proposals that each individual can submit. 
				</p>
				</div>
                <div id="faq5ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq5ID2', 'faq5ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       5.  Can my project receive the SG50 Celebration Fund in addition to grant(s) from other government agencies?
                    </p>
                </div>
                <div id="faq5ConID2" class="faqContents" style="display: none">
                    <p>Project components that are concurrently funded by other government source(s) will not qualify for the SG50 Celebration Fund. </p>
				</div>
                <div id="faq6ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq6ID2', 'faq6ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        6.    How long can my project run for?
                    </p>
                </div>
                <div id="faq6ConID2" class="faqContents" style="display: none">
                    <p>Projects should be completed within 18 months from the date of the funding agreement, or by 31 December 2015, whichever is earlier. </p>			
                </div>
                <div id="faq7ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq7ID2', 'faq7ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        7.     Are we allowed to make changes to the project in the course of implementation?
                    </p>
                </div>
                <div id="faq7ConID2" class="faqContents" style="display: none">
                    <p>You need to seek the agreement of the SG50 Celebration Fund team if there are changes, postponement and/or cancellation of your project.</p>
                </div>

		<div id="faq8ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq8ID2', 'faq8ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                        8.    How can I show that my project is part of the SG50 celebrations?
                    </p>
                </div>
                <div id="faq8ConID2" class="faqContents" style="display: none">
                    <p>All funded projects will bear the SG50 branding/logo in all your publicity materials. The SG50 Celebration Fund team will advise you on the use of the logo.</p>			
                </div>
                

		<div id="faq9ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq9ID2', 'faq9ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       9.    When will I know if my application is successful?
                    </p>
                </div>
                <div id="faq9ConID2" class="faqContents" style="display: none">
                    <p>You will be notified within two months from your application date.  </p>			
                </div>



		<div id="faq10ID2" class="faqTitle">
                    <p>
                        <a href="#" onclick="HideContent('faq10ID2', 'faq10ConID2'); return false;">
                            <img src="/SG50/images/celebration-faq/icon1.png" alt="" /></a>
                       10.    Can I submit my application form in hard copy?
                    </p>
                </div>
                <div id="faq10ConID2" class="faqContents" style="display: none">
                    <p>Yes, you can submit your application in hard copy. Just download the form, fill it in and mail it together with the relevant documents to the following address:</p>
			
			<p>
				<ul>SG50 Celebration Fund Secretariat </ul>	
				<ul>Ministry of Culture, Community and Youth (MCCY)</ul>
				<ul>Economic and International</ul>
				<ul>140 Hill Street, Old Hill Street Police Station, #01-01A </ul>
				<ul>Singapore 179369 </ul>
			</p>

			<p> Phone: 1800-8379798 (Operating Hours: 9am to 5:30pm, Mondays to Fridays) </p>
		<br>

			<p> Alternatively, you can email the form to SG50CelebrationFund@Singapore50.sg</p>
		<br>

			<p> More questions you’d like us to answer? <a href="http://www.singapore50.sg/Contact Us.aspx">Get in touch with us.</a></p>

                </div>
	
                <br />
              
            </div>
        </div>
    </div>
    <!-- FAQ Bottom Sub Page End -->

    <!--  House Rules Sub Page Start -->
    <div id="dvHouseRulesPopup">
        <a href="#" onclick="HideModalPopup('dvHouseRulesPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitleHR">
            <p class="FontTitleHR1">LET'S TRY TO KEEP</p>
            <p class="FontTitleHR2">THINGS LIGHT AND</p>
            <p class="FontTitleHR3">LIVELY, SHALL WE?</p>
        </div>
        <div class="houseRulesContents">
            <div>
                <sc:Text ID="sctContent" Field="Content" runat="server" />
            </div>
                 <div id="aboutContent">
					Birthdays are happy occasions, especially for one as big as SG50, and we would hate for anyone to feel unhappy
					<br/><br/>That's why we want to create a comfortable environment for you and the rest of Singapore to engage in discussion. The first rule of thumb would be to avoid creating misunderstanding or conflict, when posting or commenting within the SG50 site.
					<br/><br/>That means you should not use the site to advocate any politics, or demean others based on race and religion. The site also should not be used as a platform to launch personal attacks, threats, insults, advertisements or derogatory remarks. And it goes without saying, that vulgarities are not allowed.
					<br/><br/>Any posts or comments that are deemed insensitive, irrelevant or inappropriate will be removed without prior notice. Please note that discussions on the SG50 site reflect the views of the participants, and not the opinion of the SG50 Programme Office.
              		</div>       
        </div>
    </div>
    <!--  House Rules Sub Page End -->

	    <!--  Acknowledgement Start -->
    <div id="dvAckPopup">
        <a href="#" onclick="HideModalPopup('dvAckPopup'); return false;" class="closePopup">
            <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
        </a>
        <br />
        <div id="FontTitleHR">
            <p class="FontTitleHR1">A BIG THANK YOU</p>
        </div>
        <div class="ackContents">
			<div id="aboutContent">
				We may be small nation, but we’re brimming with big talent. And the SG50 website is a wonderful testament of what the little red dot and its people have to offer. 
				<br/><br/>Thank you to the following photographers who have captured the Singaporean spirit, through their vibrant snapshots featured on the website. 
				<br/><br/><br/>Andrew JK Tan – <a href="http://www.flickr.com/photos/andrewjktan/" target="_blank" style="color: white;"> http://www.flickr.com/photos/andrewjktan/</a> 
				<br/>Byran Ong – <a href="http://www.flickr.com/photos/smrt173/" target="_blank" style="color: white;"> http://www.flickr.com/photos/smrt173/ </a>  
				<br/>Lufudesu – <a href="http://www.flickr.com/people/8557677@N06/" target="_blank" style="color: white;"> http://www.flickr.com/people/8557677@N06/ </a>
			</div>
        </div>
    </div>
    <!--  Acknowledgement Sub Page End -->
		
    </form>
</body>
</html>
