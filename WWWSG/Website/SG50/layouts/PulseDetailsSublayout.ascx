﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Pulsedetailssublayout.PulsedetailssublayoutSublayout" CodeFile="~/SG50/layouts/PulseDetailsSublayout.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/include/css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/css/responsive.css" />

<script src="/SG50/include/js/jquery.tinycarousel.min.js" type="text/javascript"
    language="javascript"></script>

<script src="/SG50/include/js/jquery.min_Home.js"></script>

<script src="/SG50/include/js/jquery.prevention.js" type="text/javascript"></script>

<link href="/SG50/include/css/style_Home.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_Home.css" rel="stylesheet" type="text/css">
<!--//new css-->
<link href="/SG50/include/css/jquery.bxslider_Home.css" rel="stylesheet" type="text/css">

<script src="/SG50/include/js/jquery.bxslider.min.js"></script>

<link href="/SG50/include/css/thematic-campaign.css" rel="stylesheet" type="text/css">

<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function () {
        FB.init({
            appId: FBID, status: true, cookie: true,
            version: 'v2.2'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...


        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            // caption: fbdes,
            description: fbdes
        };
        FB.ui(obj);

    }

    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function (response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }

</script>

<script type="text/javascript">

    function RequiredSerachKeyWord(btnId, ctrlId, msg) {
        if ($(btnId).length > 0 && $(ctrlId).length > 0 && msg != '') {
            //alert('hi');
            $(btnId).click(function () {
                if ($(ctrlId).val() != '') {
                    return true;
                } else { alert(msg); $(ctrlId).focus(); return false; }
            });
        }
    }
    $(function () {


        // Pulse Details Sublayout Page
        RequiredSerachKeyWord('#content_0_imgSearchButton', '#content_0_txtSerachPulse', 'Please enter keywords to search.');

    })
</script>
<div class="masthead">
    <!-- Photos Header -->
    <div class="pulse-details">
        <div class="photo-row">
            <div class="pulse-box">
                <div class="LeftPulse-details">
                    <div class="Pulse-details-title">
                        <h1>
                            <sc:Text ID="txtTitle" Field="Title" runat="server" />
                        </h1>
                        <%--<span class=""><span>22 March, 2015</span> at <span>11:56</span>--%>
                        <span class=""><span>
                            <sc:Date ID="txtDate" Field="Date" Format="dd MMMM, yyy" runat="server" />
                            <span style="color: #000; padding: 0;" runat="server" id="atID">at</span>
                            <sc:Date ID="Date1" Field="Date" Format="HH:MM" runat="server" />
                        </span></span>
                    </div>
                    <div class="PulseDetails">
                        <div class="carousel-cont" runat="server" id="GalleryPortion" >
                            <!-- Carousel -->
                            <!-- Carousel -->
                            <!-- Carousel -->
                            <div class="CostomCarousel">
                                <div class="EnlargeImg" >
                                    <div class="ImgCont" >
                                        <%--<sc:image id="imgBanner" runat="server" field="Image" />--%>
                                        <asp:Image ID="imgBanner" runat="server" />
                                    </div>
                                    <p class="EnlargeImg-info" runat="server" id="innersrc">
                                    </p>
                                </div>
                                <div class="CustomScroller scrollbox2" id="sliderContainer" runat="server">
                                    <ul class="ThumbImg">
                                        <asp:Repeater ID="repRotating" runat="server">
                                            <ItemTemplate>
                                                <li>
                                                    <div class="slide" id='<%# DataBinder.Eval(Container.DataItem,"slideID") %>'>
                                                    </div>
                                                    <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" alt="<%# DataBinder.Eval(Container.DataItem, "caption") %>" />
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                            <!-- Carousel -->
                        </div>
                        <div class="text-area">
                            <p>
                                <sc:Text ID="txtDescription" runat="server" Field="Description" />
                            </p>
                        </div>
                        <div class="video-cont" id="videoclass" runat="server">
                            <iframe width="560" height="349" id="divIframe" runat="server" frameborder="0" allowfullscreen="True"></iframe>
                        </div>
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <div class="video-cont">
                                    <a class="social facebook" href="JavaScript:postToFeed('<%# Eval("FaceBookTitle").ToString()%>','<%# Eval("FaceBookContent").ToString()%>','<%# Eval("FaceBookThumbNail")%>','<%# Eval("EventUrl")%>')"></a><a class="social twitter" href="JavaScript:newPopup('<%# Eval("FaceBookContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')"></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div style="clear: both">
                        </div>
                        <div class="ShareIcons">
                            <p>
                                Share
                            </p>
                            <asp:Repeater ID="repsocialSharingTop" runat="server">
                                <ItemTemplate>
                                    <div class="social-icons">
                                        <a class="social facebook" href="JavaScript:postToFeed('<%# Eval("FaceBookTitle").ToString()%>','<%# Eval("FaceBookContent").ToString()%>','<%# Eval("FaceBookThumbNail")%>','<%# Eval("EventUrl")%>')"></a><a class="social twitter" href="JavaScript:newPopup('<%# Eval("FaceBookContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')"></a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <span class="texture-border"></span>
                        </div>
                        <div class="articles-carosel">
                            <div class="slider-wrap hide-for-small">
                                <div class="slider">
                                    <a href="#" class="prevarticle sa-left">previous article </a><a href="#" class="nextarticle sa-right">next article </a>
                                    <ul>
                                        <asp:Repeater ID="repPulse" runat="server">
                                            <ItemTemplate>
                                                <li>
                                                    <div class="articleimgs">
                                                        <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                                                    </div>
                                                    <div class="Lorem-cont">
                                                        <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                                        <a href='<%# DataBinder.Eval(Container.DataItem, "href") %>' class="readmore">Read More</a>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                                <a href="#" class="slider-arrow sa-left">
                                    <img src="/SG50/images/pulse/prev01.png" alt="prev" /></a> <a href="#" class="slider-arrow sa-right">
                                        <img src="/SG50/images/pulse/next01.png" alt="next" /></a>
                            </div>
                            <div class="slider-wrap show-for-small">
                                <div class="slider-mobile">
                                    <a href="#" class="prevarticle sa-mobile-left">previous article </a><a href="#" class="nextarticle sa-mobile-right">next article </a>
                                    <ul runat="server" id="MbileCarosole">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="RightPulse-cont">
                    <div class="cell-12 no-pad">
                        <div class="form-group">
                            <%--<input type="text" placeholder="SEARCH" />--%>
                            <asp:TextBox ID="txtSerachPulse" runat="server" placeholder="SEARCH" />
                            <asp:ImageButton ID="imgSearchButton" CssClass="input-button" runat="server" OnClick="imgSearchButton_Click"
                                ImageUrl="~/SG50/images/search1.png" Style="float: left" />
                            <%--<span class="input-button"></span>--%>
                        </div>
                        <div class="like-list">
                            <h1>you might also like</h1>
                            <ul>
                                <asp:Repeater ID="repRight" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                                            <div class="like-list-info">
                                                <p>
                                                    <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                                </p>
                                                <a href='<%# DataBinder.Eval(Container.DataItem, "href") %>' class="readmore">Read More</a>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Photos Header -->
</div>

<script type="text/javascript">    var animation = !0; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700), $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(), $(this).find("a:first").removeClass("active_nav") }), $("#nav a").each(function () { $(this).attr("href") == window.location.pathname && $(this).addClass("selected") }) });
</script>

<script>
    $(function () {
        if ($(window).width() <= 767) {
            $(".photo-cont img").each(function () {
                $(this).attr("src", $(this).attr("src").replace("images/", "images/mobile/"));
            });

        }
    });
</script>

<!-- jQuery -->

<script src="/SG50/include/js/jquery.lbslider.js"></script>

<script src="/SG50/include/js/enscroll-0.6.1.min.js" type="text/javascript"></script>

<script type="text/javascript">

    jQuery('.slider').lbSlider({
        leftBtn: '.sa-left',
        rightBtn: '.sa-right',
        autoPlay: false,
        autoPlayDelay: 5,
        visible: 2
    });
    jQuery('.slider-mobile').lbSlider({
        leftBtn: '.sa-mobile-left',
        rightBtn: '.sa-mobile-right',
        autoPlay: false,
        autoPlayDelay: 1,
        visible: 1
    });

    $(document).ready(function (e) {
        $('.scrollbox2').enscroll({
            horizontalScrolling: true,
            horizontalTrackClass: 'horizontal-track2',
            horizontalHandleClass: 'horizontal-handle2',
            cornerClass: 'corner2'
        });
        var totalWidth = 0;
        $('.ThumbImg li').each(function (index, V) {
            totalWidth += $(V).outerWidth();
            $('.ThumbImg').width(totalWidth);
        });
        $('.ThumbImg li').click(function () {
            $('.ThumbImg li').removeClass('active');
            $(this).addClass('active');
        });
        $('.ThumbImg').delegate('img', 'click', function () {
            $('.EnlargeImg img').attr('src', $(this).attr('src').replace('thumb', 'large'));
            $('.EnlargeImg-info').html($(this).attr('alt'));
        });
    });
    $(window).resize(function (e) {
        var totalWidth = 0;
        $('.ThumbImg li').each(function (index, V) {
            totalWidth += $(V).outerWidth();
            $('.ThumbImg').width(totalWidth);
        });
    });
</script>

