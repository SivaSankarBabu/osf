﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="Layouts.Press_sublayout.Press_sublayoutSublayout" CodeFile="~/SG50/layouts/Press Sublayout.ascx.cs" %>

<div class="press">
    <div class="pressBanner">
        <div id="pressBannerInner">
            <div class="pressBannerImage desktop">
                <sc:Image runat="server" Field="Banner Image" DataSource="{585F50AE-43B5-4CB8-9600-8AD0D350AC1F}" />
            </div>
            <!--<div class="pressBannerImage mobile">
                <sc:Image runat="server" Field="Banner Image For Responsive" DataSource="{585F50AE-43B5-4CB8-9600-8AD0D350AC1F}" />
            </div>-->
            <div class="pressBannerCaption desktop">
                <h1>NEWS, VIEWS AND EVERYTHING IN BETWEEN.</h1>
            </div>
			<div class="pressBannerCaption mobile">
                <h1>NEWS, VIEWS AND EVERYTHING IN BETWEEN.</h1>
            </div>
            <!--
			<div class="pressBannerCaption mobile">
                <h1>MARK YOUR CALENDARS AND PENCIL US IN.</h1>
            </div>
			--->
        </div>
    </div>
</div>