﻿using Sitecore.Data.Items;
using Sitecore.Links;
using System;

namespace Layouts.Breadcrumb_sublayout
{

    /// <summary>
    /// Summary description for Breadcrumb_sublayoutSublayout
    /// </summary>
    public partial class Breadcrumb_sublayoutSublayout : System.Web.UI.UserControl
    {
        private Item itmMain = Sitecore.Context.Item;
        private string strTemplateHome = SG50Class.str_Home_Template_ID;

        private string homeMenuTitle = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Menu Title"];

        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            litBreadcrumb.Text = generateBreadcrumb(itmMain);
        }

        private string generateBreadcrumb(Item itm)
        {
            string str = "";

            string templateID = itm.TemplateID.ToString();
            string itemID = itm.ID.ToString();

            if (templateID.Equals(SG50Class.str_Home_Template_ID))
            {
                str = "<a href='/'>" + homeMenuTitle + "</a>" + " <img src='/SG50/images/rightarrow.png' alt='rightarrow' /> ";
            }
            else if (templateID.Equals(SG50Class.str_Press_Details_Page_Template_ID))
            {
                string currentItemName = itm["Menu Title"];
                str = "<a href='/'>" + homeMenuTitle + "</a>" + " <img src='/SG50/images/rightarrow.png' alt='rightarrow' /> ";
                str += "<a href='" + LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Press_Item_ID)).ToString() + "'>" + SG50Class.web.GetItem(SG50Class.str_Press_Item_ID).Fields["Menu Title"].Value.ToString() + "</a>" + " <img src='/SG50/images/rightarrow.png' alt='rightarrow' /> ";
                str += currentItemName;
            }
            else if (templateID.Equals(SG50Class.str_Event_Details_Page_Template_ID))
            {
                string currentItemName = itm["Menu Title"];
                str = "<a href='/'>" + homeMenuTitle + "</a>" + " <img src='/SG50/images/rightarrow.png' alt='rightarrow' /> ";
                str += "<a href='" + LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Events_Item_ID)).ToString() + "'>" + SG50Class.web.GetItem(SG50Class.str_Events_Item_ID).Fields["Menu Title"].Value.ToString() + "</a>" + " <img src='/SG50/images/rightarrow.png' alt='rightarrow' /> ";
                str += currentItemName;
            }
            else if (itemID.Equals(SG50Class.str_Press_Item_ID) || itemID.Equals(SG50Class.str_Events_Item_ID))
            {
                str += generateBreadcrumb(itm.Parent) + itm["Menu Title"].Replace("<br />", "").ToString();
            }
            //else
            //{
            //    try
            //    {
            //        if (itm.Fields["Menu Title"].Value.ToString() != String.Empty && itm["Active"].Equals("1"))
            //        {
            //            Log.Error("201304117 : active if", this);
            //            if (itm.TemplateID.ToString().Equals(MCTClass.str_Portfolio_Page_Template_ID))
            //            {
            //                Log.Error("201304117 : default portfolio", this);
            //                str += generateBreadcrumb(MCTClass.web.GetItem(MCTClass.str_Portfolio_Section_ID));
            //            }
            //            else
            //            {
            //                Log.Error("201304117 : not portfolio", this);
            //                str += generateBreadcrumb(itm.Parent);
            //            }

            //            str += "<a href='" + LinkManager.GetItemUrl(itm) + "' >" + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(itm.Fields["Menu Title"].Value.Replace("<br />", "").ToString()) + "</a>";

            //            if (!itm.ID.ToString().Equals(itmMain.ID.ToString()))
            //            {
            //                Log.Error("201304117 : not current item", this);
            //                str += " <img src='/SG50/images/rightarrow.png' alt='rightarrow' /> ";
            //            }

            //        }
            //        else
            //        {
            //            Log.Error("201304117 : not active", this);
            //            str += generateBreadcrumb(itm.Parent);
            //        }
            //    }
            //    catch (Exception exMain)
            //    {
            //        str += generateBreadcrumb(itm.Parent);
            //    }
            //}
            return str;
        }
    }
}