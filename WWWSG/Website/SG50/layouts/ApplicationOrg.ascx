﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Applicationorg.ApplicationorgSublayout" CodeFile="~/SG50/layouts/ApplicationOrg.ascx.cs" %>
<%@ Register Assembly="CustomCaptcha" Namespace="CustomCaptcha" TagPrefix="cc1" %>
<link href="/SG50/include/css/style.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/dropkick.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/responsive.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/SG50/include/js/jquery.min.js"></script>
<script src="/SG50/include/js/1.8/jquery-ui.min.js" type="text/javascript"></script>
<link href="/SG50/include/js/1.8/jquery-ui.css" rel="Stylesheet" type="text/css" />
<link href="/SG50/include/css/jquery-ui.css" rel="stylesheet" />
<script src="/SG50/include/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="/SG50/include/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/SG50/include/js/mktSignup.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="/SG50/include/css/stylesheet.css" />
<style>
    .validation_summary_as_bulletlist ul {
        display: none;
    }
</style>

<script type="text/javascript">
    $(function () {
        $(".datepickerCompleted").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            yearRange: '1924:1996',
            defaultDate: new Date(1996, 01, 01)
            //        minDate: new Date(1950, 01, 31),
            //         maxDate: new Date(1996, 01, 31)
        });
    });

    function ValidateCheckBox(sender, args) {
        if (document.getElementById("<%=chkCondition.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox1(sender, args) {
        if (document.getElementById("<%=chkagreeable.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    function ValidateCheckBox2(sender, args) {
        if (document.getElementById("<%=chkDetails.ClientID %>").checked == true) {
            args.IsValid = true;
        } else {
            args.IsValid = false;
        }
    }
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57 || keyCode == 9) || specialKeys.indexOf(keyCode) != -1);
        return ret;
    }
    function IsDate(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = (keyCode >= 127 && keyCode <= 0);
        return ret;
    }
    function Step2() {

        window.location = '<%= ResolveUrl("/Step2.aspx") %>'

    }
    function Step1() {

        window.location = '<%= ResolveUrl("/Apply For Funding.aspx") %>'

    }
</script>

<section class="main-content">
    <!---main-content start-------->
    <div class="form-banner">
        <img src="/Sg50/images/Images/batch-4/banner.jpg" />
    </div>
    <section class="form-container">

        <div class="head-title">
            <h1>Tell us more about you</h1>
            <br />
            <h1>And Your Organisation</h1>
        </div>
        <div class="grid-container">
            <div class="formprocess-nav">
                <ul>
                    <li>
                        <div class="process-serial">1</div>
                        <div class="process-title">
                            <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="#989898" OnClientClick="Step1();" OnClick="LinkButton1_Click">
                            <h2>STEP 1</h2>
                            <p>Application overview</p>
                            </asp:LinkButton>
                        </div>
                    </li>
                    <li>
                        <div class="process-serial">2</div>
                        <div class="process-title">
                            <asp:LinkButton ID="LinkButton2" runat="server" ForeColor="#989898" OnClientClick="Step2();" OnClick="LinkButton2_Click">
                            <h2>STEP 2</h2>
                            <p>Tell us about your project</p>
                            </asp:LinkButton>
                        </div>
                    </li>
                    <li class="active">
                        <div class="process-serial">3</div>
                        <div class="process-title">
                            <h2>STEP 3</h2>
                            <p>Tell us more about you</p>
                        </div>
                    </li>
                </ul>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="step3" CssClass="validation_summary_as_bulletlist" runat="server" HeaderText='Please fill in required fields in the correct format and try again.' ForeColor="Red" ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" />
            <div class="form-content">
                <!------form content start------------>
                <!----- heading starts--------------------->
                <div id="divApplicantone">
                    <div class="heading-box">
                        <p>APPLICANT 1</p>
                        <span>(Must be Singaporean aged 18 years and above)</span>

                        <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                    </div>
                    <!----- heading ends--------------------->

                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Salutation</label><br />
                            <asp:DropDownList ID="ddlSalutationAppone" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Mr</asp:ListItem>
                                <asp:ListItem Value="3">Ms</asp:ListItem>
                                <asp:ListItem Value="2">Mrs</asp:ListItem>
                                <asp:ListItem Value="4">Mdm</asp:ListItem>
                                <asp:ListItem Value="5">Dr</asp:ListItem>
                                <asp:ListItem Value="6">Prof</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqSalutationAppone" runat="server" ValidationGroup="step3" ControlToValidate="ddlSalutationAppone" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Name of applicant (as in NRIC)</label><br />
                            <asp:TextBox ID="txtapplicantonename" MaxLength="66" CssClass="ValidationRequired" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqNameofapplicant" runat="server" ValidationGroup="step3" ControlToValidate="txtapplicantonename" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>NRIC number</label><br />
                            <asp:TextBox ID="txtNricnum" MaxLength="9" CssClass="ValidationRequired nric" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqNricnum" runat="server" ValidationGroup="step3" ControlToValidate="txtNricnum" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Nationality</label>
                            <br />
                            <p>Singaporean</p>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Occupation / Designation</label><br />
                            <asp:TextBox ID="txtOccupationAppOne" MaxLength="100" CssClass="ValidationRequired" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqOccupationAppOne" runat="server" ValidationGroup="step3" ControlToValidate="txtOccupationAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory">*</span>Gender</label><br />
                            <asp:DropDownList ID="ddlGenderAppOne" CssClass="ValidationRequired" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="M">Male</asp:ListItem>
                                <asp:ListItem Value="F">Female</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqGenderAppOne" runat="server" ValidationGroup="step3" ControlToValidate="ddlGenderAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Date of birth</label><br />
                            <asp:TextBox ID="txtDobAppone" CssClass="datepickerCompleted calender ValidationRequired" onkeypress="return IsDate(event);" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqDobAppone" runat="server" ValidationGroup="step3" ControlToValidate="txtDobAppone" Text="" ForeColor="Red"></asp:RequiredFieldValidator>

                        </div>
                        <div class="form-right" style="visibility: hidden">
                            <label><span class="mandatory">*</span>Estimated total number of participants / beneficiaries</label><br />
                            <input type="text" />
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <!-------------list-row start--------------------->
                    <label><span class="mandatory">*</span>Address (in NRIC)</label>
                    <asp:TextBox ID="txtAddressAppOne" runat="server" CssClass="ValidationRequired" TextMode="MultiLine" Style="width: 960px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regAddress" ForeColor="Red" ValidationExpression="^[\s\S]{0,120}$" ValidationGroup="step3" ErrorMessage="You cannot enter more than 120 characters." ControlToValidate="txtAddressAppOne" runat="server" Display="Dynamic">
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="reqtxtAddressAppOne" runat="server" ValidationGroup="step3" ControlToValidate="txtAddressAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Mobile number (in Singapore)</label>
                            <br />
                            <asp:TextBox ID="txtMobileNumAppone" MaxLength="8" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqMobileNumAppone" Display="Dynamic" runat="server" ValidationGroup="step3" ControlToValidate="txtMobileNumAppone" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionMobile" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter 8 Digits." ControlToValidate="txtMobileNumAppone" ForeColor="Red" ValidationExpression="\d{8,8}"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-right">
                            <label><span class="mandatory"></span>Phone number</label><br />
                            <asp:TextBox ID="txtPhoneNumberAppOne" onkeypress="return IsNumeric(event);" MaxLength="8" runat="server"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionPhone" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter 8 Digits." ControlToValidate="txtPhoneNumberAppOne" ForeColor="Red" ValidationExpression="\d{8,8}"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <!-------------list-row start--------------------->
                    <div class="form-float">
                        <!-------------list-row start--------------------->
                        <div class="form-left">
                            <label><span class="mandatory">*</span>Email address</label><br />
                            <asp:TextBox ID="txtEmailAppOne" MaxLength="320" CssClass="ValidationRequired" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqEmailAppOne" Display="Dynamic" ValidationGroup="step3" runat="server" ControlToValidate="txtEmailAppOne" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionEmail1" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter Valid Email Address." ControlToValidate="txtEmailAppOne" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ForeColor="Red" ValidationGroup="step3" ValidationExpression="^[\s\S]{0,320}$" ErrorMessage="You cannot enter more than 320 characters." ControlToValidate="txtEmailAppOne" runat="server" Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                </div>
                <!-------------list-row start--------------------->

                <!----- heading starts--------------------->
                <div class="heading-box">
                    <p>ORGANISATION INFORMATION </p>

                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>
                <!----- heading ends--------------------->

                <div class="form-float">
                    <!-------------list-row start--------------------->
                    <div class="form-left">
                        <label><span class="mandatory">*</span>Name of Organisation</label><br />
                        <asp:TextBox ID="txtOrgName" MaxLength="100" CssClass="ValidationRequired" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqOrgName" runat="server" ControlToValidate="txtOrgName" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ForeColor="Red" ValidationGroup="step3" ValidationExpression="^[\s\S]{0,100}$" ErrorMessage="You cannot enter more than 100 characters." ControlToValidate="txtOrgName" runat="server" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Unique Entity Number (UEN)</label><br />
                        <asp:TextBox ID="txtuenNum" runat="server" CssClass="ValidationRequired" MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqUenNum" runat="server" ControlToValidate="txtuenNum" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <!-------------list-row start--------------------->
                <div class="form-float">
                    <!-------------list-row start--------------------->
                    <div class="form-left">
                        <label><span class="mandatory">*</span>Organisation type</label><br />
                        <asp:DropDownList ID="ddlOrgType" CssClass="ValidationRequired" runat="server">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="OT01">Company Limited By Guarantee</asp:ListItem>
                            <asp:ListItem Value="OT02">Limited Liability Partnership</asp:ListItem>
                            <asp:ListItem Value="OT03">Partnership</asp:ListItem>
                            <asp:ListItem Value="OT04">Private Limited</asp:ListItem>
                            <asp:ListItem Value="OT05">Society</asp:ListItem>
                            <asp:ListItem Value="OT99">Others</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtuenNum" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Nature of business</label>
                        <br />
                        <asp:TextBox ID="txtNatureOfBusiness" MaxLength="255" CssClass="ValidationRequired" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqNatureOfBus" runat="server" ControlToValidate="txtNatureOfBusiness" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ForeColor="Red" ValidationGroup="step3" ValidationExpression="^[\s\S]{0,255}$" ErrorMessage="You cannot enter more than 225 characters." ControlToValidate="txtNatureOfBusiness" runat="server" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <!-------------list-row start--------------------->
                <div class="form-float">
                    <!-------------list-row start--------------------->
                    <div class="form-left">
                        <label><span class="mandatory">*</span>Founding year (YYYY)</label><br />
                        <asp:TextBox ID="txtFoundYear" MaxLength="4" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqYear" runat="server" ControlToValidate="txtFoundYear" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter Year." ControlToValidate="txtFoundYear" ForeColor="Red" ValidationExpression="^[12][0-9]{3}$"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Number of employees</label><br />
                        <asp:TextBox ID="txtNumOfEmp" CssClass="ValidationRequired" MaxLength="4" onkeypress="return IsNumeric(event);" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqNumOfEmp" runat="server" CssClass="ValidationRequired" ControlToValidate="txtNumOfEmp" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtNumOfEmp" MinimumValue="1" MaximumValue="9999" ValidationGroup="step3"  ErrorMessage="Please enter value 1 to 9999" ForeColor="Red"></asp:RangeValidator>
                    </div>
                </div>
                <!-------------list-row start--------------------->

                <label><span class="mandatory">*</span>Address of organisation (in Singapore)</label>
                <asp:TextBox ID="txtOrgAddress" runat="server" CssClass="ValidationRequired" TextMode="MultiLine" Style="width: 960px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ForeColor="Red" ValidationGroup="step3" ValidationExpression="^[\s\S]{0,120}$" ErrorMessage="You cannot enter more than 120 characters." ControlToValidate="txtOrgAddress" runat="server" Display="Dynamic">
                </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="reqOrgAddress" runat="server" ControlToValidate="txtOrgAddress" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                <!-------------list-row start--------------------->
                <div class="form-float">
                    <!-------------list-row start--------------------->
                    <div class="form-left">
                        <label><span class="mandatory">*</span>Phone number (in Singapore)</label><br />
                        <asp:TextBox ID="txtOrgPhone" CssClass="ValidationRequired" onkeypress="return IsNumeric(event);" runat="server" MaxLength="8"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqOrgPhone" Display="Dynamic" runat="server" ControlToValidate="txtOrgPhone" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="step3" runat="server" ErrorMessage="Please Enter 8 Digits." ControlToValidate="txtOrgPhone" ForeColor="Red" ValidationExpression="\d{8,8}"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-right">
                        <label><span class="mandatory">*</span>Email address</label><br />
                        <asp:TextBox ID="txtOrgEmail" MaxLength="320" CssClass="ValidationRequired" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqOrgEmail" runat="server" ControlToValidate="txtOrgEmail" ValidationGroup="step3" Text="" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ForeColor="Red" ValidationGroup="step3" ValidationExpression="^[\s\S]{0,320}$" ErrorMessage="You cannot enter more than 320 characters." ControlToValidate="txtOrgEmail" runat="server" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>


                </div>
                <!-------------list-row start--------------------->


                <div class="heading-box">
                    <p>*DECLARATION</p>
                    <img src="/SG50/images/Images/batch-4/down-arrow.png" />
                </div>
                <!----- heading ends--------------------->

                <div class="check-box-container">
                    <div class="check-box" id="DivTerms" runat="server" visible="false">
                        <asp:CheckBox ID="chkCondition" runat="server" />
                        <p>
                            <sc:FieldRenderer ID="txtTC" runat="server" FieldName="Terms and Conditions Text" />
                            <sc:Link Field="Terms and Conditio URL" runat="server" ID="hyplknTC">
                                <span class="mandatory">
                                    <sc:Text Field="Terms and Conditio URL Text" runat="server" ID="txtterms" />
                                </span>
                            </sc:Link>
                        </p>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ForeColor="Red" ValidationGroup="step3" ErrorMessage="Please Accept Terms and Conditions" ClientValidationFunction="ValidateCheckBox"></asp:CustomValidator>


                    </div>
                    <div class="check-box" id="DivAgreement" runat="server" visible="false">
                        <asp:CheckBox ID="chkagreeable" runat="server" /><p>
                            <sc:FieldRenderer ID="txtAgreement" runat="server" FieldName="Agreement Text" />
                            <sc:Link Field="Agreement URL" runat="server" ID="hyplnkAgreement">
                                <span class="mandatory">
                                    <sc:Text Field="Agreement URL Text" runat="server" ID="txAgree" />
                                </span>
                            </sc:Link>
                        </p>
                        <asp:CustomValidator ID="CustomValidator2" runat="server" ForeColor="Red" ValidationGroup="step3" ErrorMessage="Please Accept Terms and Conditions" ClientValidationFunction="ValidateCheckBox1"></asp:CustomValidator>
                    </div>
                    <div class="check-box" id="DivSupporting" runat="server" visible="false">

                        <asp:CheckBox ID="chkDetails" runat="server" />
                        <sc:FieldRenderer ID="txtSupporting" runat="server" FieldName="Supporting Documents Text" />
                        <asp:CustomValidator ID="CustomValidator3" runat="server" ForeColor="Red" ValidationGroup="step3" ErrorMessage="Please Accept Terms and Conditions" ClientValidationFunction="ValidateCheckBox2"></asp:CustomValidator>
                    </div>
                </div>
                <div>
                    <label><span class="mandatory">*</span>Enter verification code</label><br />
                    <div>

                        <asp:TextBox ID="txt_CaptchaVal" runat="server" Visible="false"></asp:TextBox>
                        <asp:Image ID="Image1" runat="server" />

                        <asp:TextBox ID="txt_Captcha" MaxLength="4" Style="vertical-align: top" placeholder="Enter Captcha" Width="200px" runat="server" CssClass="form-select ValidationRequired"></asp:TextBox>

                        <asp:ImageButton ID="ImageButton1" Style="vertical-align: top" runat="server" ImageUrl="/SG50/images/PG/refreshIcon.gif"
                            CausesValidation="false" Text="refresh" CssClass="cancel" OnClick="Button1_Click1" />
                        <br />

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                            ErrorMessage="Please enter Captcha" Style="font-size: 14px; font-weight: bolder" Display="Dynamic" ValidationGroup="step3" ForeColor="Red" ControlToValidate="txt_Captcha"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <table>
                    <tr>
                        <td style="width: 10px"></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="next-step">
                                <asp:Button ID="btnStepBack" CssClass="cancel" OnClientClick="Step2();" runat="server" Text="BACK" OnClick="btnStepBack_Click" />
                            </div>
                        </td>
                        <td style="width: 10px"></td>
                        <td>
                            <div class="next-step">
                                <asp:Button ID="Button1" ValidationGroup="step3" runat="server" Text="SUBMIT" OnClick="btnSubmit_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <!------form content Ends------------>
        </div>
    </section>
</section>
