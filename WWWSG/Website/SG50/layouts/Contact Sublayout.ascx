﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Contact_sublayout.Contact_sublayoutSublayout" CodeFile="~/SG50/layouts/Contact Sublayout.ascx.cs" %>
<style>
    #contactUsLink, #contactUsLink a
    {
        color: #353333;
    }
    #contactUsLink img
    {
        width: 10px;
        height: 10px;
    }
    #contactUsContent
    {
        min-height: 500px !important;
        position: relative;
    }
</style>
<div class="contactUs">
    <div class="contactUsBanner">
        <div id="contactUsBannerInner">
            <div id="contactUsBannerImage">
                <img src="/SG50/images/contact/header.png" alt="" />
            </div>
            <div id="contactUsBannerCaption">
                <h1>
                    QUESTIONS?</h1>
                <h1>
                    DROP US A LINE.</h1>
            </div>
        </div>
    </div>
    <div id="contactUsContent">
        <div id="contactUsLink">
            <p>
                <a href="<%= getHomeLink() %>">SG50
                    <img src="/SG50/images/rightarrow.png" alt="rightarrow"></a> <a href="#"><span style="color: #ef2c2e;">
                        Contact Us</span></a>
            </p>
        </div>
        
        <div id="contactUsTitle">
            <h1>
                <sc:text id="txtContactUsTitle" field="Contact Us Title" runat="server" />
            </h1>
            <div id="welcomeText" runat="server">
            <p>
                <sc:text id="txtwelcome" field="Contact Us Welcome Message" runat="server" />
            </p>
            <p>
                To submit your feedback, click <a href="https://www.ifaq.gov.sg/mccy/apps/feedback.aspx"
                    target="_blank" style="text-decoration: underline; color: #00CCFF;" id="lnkredirect"
                    runat="server">here</a>.
            </p>
            </div>
        </div>
        
        <div id="contactUsMCCY">
            <sc:image id="ImgContact" runat="server" field="Image" />
            <p style="font-weight: bold; padding-top: 10px;">
                <sc:text id="txtContactUs" field="Title" runat="server" />
            </p>
            <p style="padding-top: 10px;">
                <sc:text id="txtDescription" field="Description" runat="server" />
            </p>
        </div>
        <div id="contactUsDiscription">
            
                <br />
                <p>
                    
                </p>
                <br />
                <br />
            </div>
        </div>
    </div>
</div>
