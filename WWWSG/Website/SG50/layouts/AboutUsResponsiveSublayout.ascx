﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Aboutusresponsivesublayout.AboutusresponsivesublayoutSublayout"
    CodeFile="~/SG50/layouts/AboutUsResponsiveSublayout.ascx.cs" %>


<link href="/SG50/include/css/style_About.css" rel="stylesheet" />
<link href="/SG50/include/css/responsive_About.css" rel="stylesheet" type="text/css">
<!--existing site css-->
<link rel="stylesheet" type="text/css" href="/SG50/include/Css/skin.css" />
<link rel="stylesheet" type="text/css" href="/SG50/include/Css/responsive.css" />
<link href="/SG50/include/Css/prettyPhoto.css" rel="stylesheet" type="text/css" />

<div id="homePage" class="container rew">
    <!--<div class="fix-arws"> <a href="#" class="display"><img src="images/uparrow.jpg" alt="prev" />prev</a> <a href="#" class="display"><img src="images/downarrow.jpg" alt="next" />next</a> </div>-->
    <!--Start OF Banner-->
    <div id="banner" class="section current">
        <sc:image id="BannerImg" runat="server" field="Banner Image" />
        <div class="about-us featured-in" id="DivLittleDot">
            <div class="about-left">
                <sc:fieldrenderer id="txtTitle" runat="server" fieldname="About Title" />
                <sc:fieldrenderer id="aboutContent" runat="server" fieldname="About Content" />
                <sc:image id="logoImg" runat="server" cssclass="about-icon" field="About Image" />
                <div class="clear">
                </div>
                <a href="#" id="dlLnkLogoAndGuide" runat="server" onserverclick="DownloadForm">
                    <sc:text id="btnText" field="Button Text" runat="server" />.
                </a>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <!--End OF Banner-->
    <div class="clear initiatives-pan pad section INITIATIVESBlk" id="divinitiatives">
        <div class="featured-in">
            <h3>
                <span>Our Initiatives</span></h3>
             <div class="intiatives-desktop">
               <asp:Repeater ID="repInitiatives" runat="server">
                    <ItemTemplate>
                        <ul class="<%# DataBinder.Eval(Container.DataItem, "LeftRightimg") %>">
                            <li class="img-box"><span class="<%# DataBinder.Eval(Container.DataItem, "LeftRightArrow") %>"></span>
                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" alt="<%# DataBinder.Eval(Container.DataItem, "alt") %>" /></li>
                            <li class="text-box">
                                <div class="ourintiatives-cont">
                                    <h4>
                                        <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label></h4>
                                    <p>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Label>
                                    </p>
                                    <a id="A1" href='<%#Eval("href")%>' runat="server" visible='<%#Eval("Status")%>' class="readmore-btn"><%#Eval("ButtonText")%>                                      
                                        
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <!--Start OF way to go team pan-->
    <div class="clear featured-pan pad section">
        <div class="featured-in way-to">
            <h3>
                <span>
                    <sc:text id="txtway" field="Way To Go Title" runat="server" />
                </span>
            </h3>
            <sc:text id="txtWayContent" field="Way To Go Content" runat="server" />
            <ul>
                <li>
                    <sc:image id="Img1" runat="server" field="Image1" />
                    <span>
                        <sc:text id="txt1" field="Text1" runat="server" />
                    </span></li>
                <li>
                    <sc:image id="Img2" runat="server" field="Image2" />
                    <span>
                        <sc:text id="txt2" field="Text2" runat="server" />
                    </span></li>
                <li>
                    <sc:image id="Img3" runat="server" field="Image3" />
                    <span>
                        <sc:text id="txt3" field="Text3" runat="server" />
                    </span></li>
                <li>
                    <sc:image id="Img4" runat="server" field="Image4" />
                    <span>
                        <sc:text id="txt4" field="Text4" runat="server" />
                    </span></li>
                <li>
                    <sc:image id="Img5" runat="server" field="Image5" />
                    <span>
                        <sc:text id="txt5" field="Text5" runat="server" />
                    </span></li>
            </ul>
            <div class="clear">
            </div>
        </div>
    </div>
    <!--End OF way to go team pan-->
    <!--Start OF Meet the committee pan-->
    <div class="clear news-feed pad section" id="DivMeetthecommitte">
        <div class="featured-in">
            <h3>
                <span>MEET THE COMMITTEE</span></h3>
            <div class="meet-commitee">
                <ul>
                    <asp:Repeater ID="repCommittee" runat="server">
                        <ItemTemplate>
                            <li>
                                <asp:Image runat="server" ID="repImg" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>' />
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <div class="clear">
                </div>
            </div>
            <div class="commitee-members">
                <h5>
                    Members</h5>
                <ul>
                    <asp:Repeater ID="repMembers" runat="server">
                        <ItemTemplate>
                            <li>
                                <asp:Image runat="server" ID="repmemberImg" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>' />
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
    </div>
    <!--End OF Meet the committee pan-->
</div>
<%--<script src="js/jquery.min.js"></script>
<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script>
    $(document).ready(function () {
        ImageChange();
        $(window).resize(function () {
            ImageChange();
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("area[rel^='prettyPhoto']").prettyPhoto();
        $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({ animation_speed: 'normal', theme: 'light_square', autoplay_slideshow: false });
        $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({ animation_speed: 'normal', theme: 'light_square', autoplay_slideshow: false });
        $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
            custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
            changepicturecallback: function () { initialize(); }
        });
    });
</script>
<script>
    function ShowMenu() { $(".menu").toggle() } $(function () { $(window).scroll(function () { var e = $(this).scrollTop(); $(".section ").each(function () { if (e > $(this).offset().top - 100) { $(".section").removeClass("current"); $(this).addClass("current"); return } }) }); $("div.section").first(); $("a.display").on("click", function (e) { e.preventDefault(); var t = $(this).text(), n = $(this); if (t === "next" && $(".current").next("div.section").length > 0) { var r = $(".current").next(".section"); var i = r.offset().top; $(".current").removeClass("current"); $("body,html").animate({ scrollTop: i }, function () { r.addClass("current") }) } else if (t === "prev" && $(".current").prev("div.section").length > 0) { var s = $(".current").prev(".section"); var i = s.offset().top; $(".current").removeClass("current"); $("body,html").animate({ scrollTop: i }, function () { s.addClass("current") }) } }) }); var a; var animation = true; $(document).ready(function () { $("#nav div").hover(function () { $("div", this).fadeIn(700); $(this).find("a:first").addClass("active_nav") }, function () { $("div", this).hide(); $(this).find("a:first").removeClass("active_nav") }); $("#nav a").each(function () { if ($(this).attr("href") == window.location.pathname) { $(this).addClass("selected") } }) }); if ($(window).width() <= 767) { $(".bxslider").bxSlider({ pause: 5e3, touchEnabled: false, autoStart: false, auto: false, oneToOneTouch: false }) } else { $(".bxslider").bxSlider({ pause: 5e3, auto: true, oneToOneTouch: false, touchEnabled: false }) }
</script>
--%>
