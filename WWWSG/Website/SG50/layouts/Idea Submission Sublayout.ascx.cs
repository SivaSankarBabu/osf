﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Publishing;
using System;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Layouts.Idea_submission_sublayout
{

    /// <summary>
    /// Summary description for Idea_submission_sublayoutSublayout
    /// </summary>
    public partial class Idea_submission_sublayoutSublayout : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
        }

        protected string getLink()
        {
            string link = "";
            if (Request.UrlReferrer.ToString().Trim().ToLower().Equals(ConfigurationManager.AppSettings["SG50websiteURL"].ToString().ToLower() + LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID)).ToLower()))
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID));
            }
            else
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID));
            }

            return link;
        }

        protected string getWaytoCelebrateLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Celebration_Fund_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Celebration_Fund_Item_ID));
            }
            return link;
        }

        protected string getCelebrationIdeaLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Celebration_Ideas_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Celebration_Ideas_Item_ID));
            }
            return link;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtfname.Text = "";
            txtlname.Text = "";
            txtemail.Text = "";
            ddlLocations.SelectedIndex = 0;
            if (Request.UrlReferrer.ToString().Trim().ToLower().Equals(ConfigurationManager.AppSettings["SG50websiteURL"].ToString().ToLower() + LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID)).ToLower()))
            {
                Response.Redirect(LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID)));
            }
            else
            {
                Response.Redirect(LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID)));
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (isValidated())
            {
                if (isSaved())
                {
                    // Show success page
                    pnSuccess.Visible = true;
                    pnForm.Visible = false;
                }
            }
        }


        protected void createIdeaItm()
        {

            //txtfname.Text = "";
            //txtlname.Text = "";
            //txtemail.Text = "";
            //ddlLocations.SelectedIndex = 0;
        }

        private bool isSaved()
        {
            Boolean isSaved = false;

            try
            {
                Item itmIdea = SG50Class.master.GetItem(SG50Class.str_Settings_Idea_Count_Item_ID);
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    //string strItemName = DateTime.Now.ToString("yyMMddHHmmssfff");
                    string strItemName = "";

                    if (SG50Class.master.GetItem(SG50Class.str_Idea_Repository_Item_ID) != null)
                    {
                        Item itmIdeaRepository = SG50Class.master.GetItem(SG50Class.str_Idea_Repository_Item_ID);
                        TemplateItem template = SG50Class.master.GetTemplate(SG50Class.str_Idea_Template_ID);
                        int ideaIndex = 0;

                        Item item = SG50Class.master.GetItem(SG50Class.str_Settings_Idea_Count_Item_ID);

                        //get value if not locked
                        if (!item.Editing.IsEditing)
                        {
                            item.Editing.BeginEdit();
                            Int32.TryParse(item["Value"], out ideaIndex);
                            ideaIndex = ideaIndex + 1;
                            try
                            {
                                item.Fields["Value"].Value = ideaIndex.ToString();
                            }
                            finally
                            {
                                item.Editing.EndEdit();
                            }

                            strItemName = ideaIndex.ToString();
                            itmIdeaRepository.Add(strItemName, template);
                            Item itmNewIdea = SG50Class.master.GetItem(itmIdeaRepository.Paths.FullPath + "/" + strItemName);

                            itmNewIdea.Editing.BeginEdit();
                            try
                            {
                                itmNewIdea.Fields["Name"].Value = txtfname.Text;
                                itmNewIdea.Fields["Age"].Value = txtlname.Text;
                                itmNewIdea.Fields["Region"].Value = ddlLocations.SelectedValue;
                                itmNewIdea.Fields["Idea"].Value = txtemail.Text;
                                itmNewIdea.Fields["Index"].Value = ideaIndex.ToString();
                            }
                            finally
                            {
                                itmNewIdea.Editing.EndEdit();
                            }

                            Language language = Sitecore.Context.Language;
                            DateTime publishTime = DateTime.Now;

                            Sitecore.Publishing.PublishOptions options = new PublishOptions(SG50Class.master, SG50Class.web, PublishMode.SingleItem, language, publishTime);
                            Sitecore.Publishing.Pipelines.PublishItem.PublishItemPipeline.Run(itmNewIdea.ID, options);
                            Sitecore.Publishing.Pipelines.PublishItem.PublishItemPipeline.Run(itmIdea.ID, options);


                            isSaved = true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Info("Idea_submission_sublayoutSublayout isSaved exMain : " + ex.ToString(), this);
            }
            return isSaved;
        }

        protected Boolean isValidated()
        {
            bool isValid = true;
            if (ddlLocations.SelectedValue.Equals("Region"))
            {
                isValid = false;
                rfvLocations.IsValid = false;
            }

            //check name
            if (txtfname.Text.Trim().Length == 0)
            {
                isValid = false;
                rfvfname.IsValid = false;
            }
            else
            {
                // check regular expression
                string fname = txtfname.Text.ToString();
                Match match1 = Regex.Match(fname, regexfname.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexfname.IsValid = false;
                }
            }

            //check age
            if (txtlname.Text.Trim().Length == 0)
            {
                isValid = false;
                rfvlname.IsValid = false;
            }
            else
            {
                // check regular expression
                string age = txtlname.Text.ToString();
                Match match1 = Regex.Match(age, regexlname.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexlname.IsValid = false;
                }
            }

            //check email
            if (txtemail.Text.Trim().Length == 0)
            {
                isValid = false;
                rfvemail.IsValid = false;
            }
            else
            {
                // check regular expression
                string name = txtemail.Text.ToString();
                Match match = Regex.Match(name, regexemail.ValidationExpression);
                if (!match.Success)
                {
                    isValid = false;
                    regexemail.IsValid = false;
                }
            }


            return isValid;
        }

    }
}