﻿using System;
using Sitecore.Data.Items;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Links;
using Sitecore.Diagnostics;
using System.Text.RegularExpressions;

namespace Layouts.Share_an_idea_sublayout
{

    /// <summary>
    /// Summary description for Share_an_idea_sublayoutSublayout
    /// </summary>
    public partial class Share_an_idea_sublayoutSublayout : System.Web.UI.UserControl
    {
        Item itmContext = Sitecore.Context.Item;
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                FillrepIdeas("");
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (isValidated())
            {
                FillrepIdeas(SG50Class.StripUnwantedString(txtSearch.Text.ToString().Trim()));
            }
        }

        protected void btnSearchMob_Click(object sender, EventArgs e)
        {
            if (isValidatedMob())
            {
                FillrepIdeas(SG50Class.StripUnwantedString(txtSearchMob.Text.ToString().Trim()));
            }
        }

        private bool isValidated()
        {
            bool isValid = true;

            if (txtSearch.Text.ToString().Trim().Equals(""))
            {
                rfvSearch.IsValid = false;
                isValid = false;
            }
            else
            {
                // check regular expression
                string text = txtSearch.Text.ToString();
                Match match1 = Regex.Match(text, regexSearch.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexSearch.IsValid = false;
                }
            }
            return isValid;
        }

        private bool isValidatedMob()
        {
            bool isValid = true;

            if (txtSearchMob.Text.ToString().Trim().Equals(""))
            {
                rfvSearchMob.IsValid = false;
                isValid = false;
            }
            else
            {
                // check regular expression
                string text = txtSearchMob.Text.ToString();
                Match match1 = Regex.Match(text, regexSearchMob.ValidationExpression);
                if (!match1.Success)
                {
                    isValid = false;
                    regexSearchMob.IsValid = false;
                }
            }

            return isValid;
        }

        private void FillrepIdeas(string searchString)
        {
            try
            {
                DataTable dtrepIdeas = new DataTable();
                dtrepIdeas.Columns.Add("cssClass", typeof(string));
                dtrepIdeas.Columns.Add("ideaText", typeof(string));
                dtrepIdeas.Columns.Add("Name", typeof(string));
                dtrepIdeas.Columns.Add("Age", typeof(string));
                dtrepIdeas.Columns.Add("href", typeof(string));
                dtrepIdeas.Columns.Add("visible", typeof(string));
                dtrepIdeas.Columns.Add("Region", typeof(string));


                DataRow drrepIdeas;

                Item itmIdeaRepository = SG50Class.web.GetItem(SG50Class.str_Idea_Repository_Item_ID);

                IEnumerable<Item> itmIEnumWestIdea = Enumerable.Empty<Item>();
                IEnumerable<Item> itmIEnumNorthIdea = Enumerable.Empty<Item>();
                IEnumerable<Item> itmIEnumSouthIdea = Enumerable.Empty<Item>();
                IEnumerable<Item> itmIEnumEastIdea = Enumerable.Empty<Item>();

                if (searchString.Trim().Equals(""))
                {
                    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        &&
                                        a["Region"].ToString().Equals("West")
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);

                    itmIEnumNorthIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                         where
                                         a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                         &&
                                         a["Region"].ToString().Equals("North")
                                         select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(6);

                    itmIEnumSouthIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                         where
                                         a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                         &&
                                         a["Region"].ToString().Equals("South")
                                         select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);

                    itmIEnumEastIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        &&
                                        a["Region"].ToString().Equals("East")
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);
                }
                else
                {
                    itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        &&
                                        a["Region"].ToString().Equals("West")
                                        &&
                                        a["Name"].ToLower().Contains(searchString.ToLower())
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);

                    itmIEnumNorthIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                         where
                                         a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                         &&
                                         a["Region"].ToString().Equals("North")
                                         &&
                                         a["Name"].ToLower().Contains(searchString.ToLower())
                                         select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(6);

                    itmIEnumSouthIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                         where
                                         a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                         &&
                                         a["Region"].ToString().Equals("South")
                                         &&
                                         a["Name"].ToLower().Contains(searchString.ToLower())
                                         select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);

                    itmIEnumEastIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                        &&
                                        a["Region"].ToString().Equals("East")
                                        &&
                                        a["Name"].ToLower().Contains(searchString.ToLower())
                                        select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);

                    if (itmIEnumWestIdea.Count() == 0 && itmIEnumEastIdea.Count() == 0 && itmIEnumSouthIdea.Count() == 0 && itmIEnumNorthIdea.Count() == 0)
                    {
                        noResult.Visible = true;
                        itmIEnumWestIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                            where
                                            a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                            &&
                                            a["Region"].ToString().Equals("West")
                                            select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);

                        itmIEnumNorthIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                             where
                                             a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                             &&
                                             a["Region"].ToString().Equals("North")
                                             select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(6);

                        itmIEnumSouthIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                             where
                                             a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                             &&
                                             a["Region"].ToString().Equals("South")
                                             select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);

                        itmIEnumEastIdea = (from a in itmIdeaRepository.Axes.GetDescendants()
                                            where
                                            a.TemplateID.ToString().Equals(SG50Class.str_Idea_Template_ID)
                                            &&
                                            a["Region"].ToString().Equals("East")
                                            select a).OrderByDescending(x => Sitecore.DateUtil.IsoDateToDateTime(x.Fields["__Created"].Value.ToString())).Take(8);
                    }
                    else
                    {
                        noResult.Visible = false;
                    }
                }



                int west = 0;
                int north = 0;
                int south = 0;
                int east = 0;

                foreach (Item a in itmIEnumWestIdea)
                {
                    try
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = LinkManager.GetItemUrl(a);
                        drrepIdeas["cssClass"] = SG50Class.arr_west_region_CSS_Classes[west];
                        if (a["Idea"].ToString().Count() > 31)
                        {
                            drrepIdeas["ideaText"] = a["Idea"].ToString().Remove(30) + "...";
                        }
                        else
                        {
                            drrepIdeas["ideaText"] = a["Idea"];
                        }
                        drrepIdeas["Name"] = a["Name"];
                        drrepIdeas["Age"] = a["Age"];
                        drrepIdeas["Region"] = a["Region"];

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        west++;
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Events_listing_sublayoutSublayout FillrepIdeas a exSub1 : " + exSub1.ToString(), this);
                    }
                }

                if (west < 8)
                {
                    for (int i = west - 1; i < 7; i++)
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = "";
                        drrepIdeas["cssClass"] = SG50Class.arr_west_region_CSS_Classes[west];
                        drrepIdeas["ideaText"] = "";
                        drrepIdeas["Name"] = "";
                        drrepIdeas["Age"] = "";
                        drrepIdeas["visible"] = "display: none;";
                        drrepIdeas["Region"] = "";

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        west++;
                    }
                }

                foreach (Item a in itmIEnumNorthIdea)
                {
                    try
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = LinkManager.GetItemUrl(a);
                        drrepIdeas["cssClass"] = SG50Class.arr_north_region_CSS_Classes[north];
                        if (a["Idea"].ToString().Count() > 31)
                        {
                            drrepIdeas["ideaText"] = a["Idea"].ToString().Remove(30) + "...";
                        }
                        else
                        {
                            drrepIdeas["ideaText"] = a["Idea"];
                        }
                        drrepIdeas["Name"] = a["Name"];
                        drrepIdeas["Age"] = a["Age"];
                        drrepIdeas["Region"] = a["Region"];

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        north++;
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Events_listing_sublayoutSublayout FillrepIdeas a exSub1 : " + exSub1.ToString(), this);
                    }
                }

                if (north < 6)
                {
                    for (int i = north - 1; i < 5; i++)
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = "";
                        drrepIdeas["cssClass"] = SG50Class.arr_north_region_CSS_Classes[north];
                        drrepIdeas["ideaText"] = "";
                        drrepIdeas["Name"] = "";
                        drrepIdeas["Age"] = "";
                        drrepIdeas["visible"] = "display: none;";
                        drrepIdeas["Region"] = "";

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        north++;
                    }
                }

                foreach (Item a in itmIEnumSouthIdea)
                {
                    try
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = LinkManager.GetItemUrl(a);
                        drrepIdeas["cssClass"] = SG50Class.arr_south_region_CSS_Classes[south];
                        if (a["Idea"].ToString().Count() > 31)
                        {
                            drrepIdeas["ideaText"] = a["Idea"].ToString().Remove(30) + "...";
                        }
                        else
                        {
                            drrepIdeas["ideaText"] = a["Idea"];
                        }
                        drrepIdeas["Name"] = a["Name"];
                        drrepIdeas["Age"] = a["Age"];
                        drrepIdeas["Region"] = a["Region"];

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        south++;
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Events_listing_sublayoutSublayout FillrepIdeas a exSub1 : " + exSub1.ToString(), this);
                    }
                }

                if (south < 8)
                {
                    for (int i = south - 1; i < 7; i++)
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = "";
                        drrepIdeas["cssClass"] = SG50Class.arr_south_region_CSS_Classes[south];
                        drrepIdeas["ideaText"] = "";
                        drrepIdeas["Name"] = "";
                        drrepIdeas["Age"] = "";
                        drrepIdeas["visible"] = "display: none;";
                        drrepIdeas["Region"] = "";

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        south++;
                    }
                }

                foreach (Item a in itmIEnumEastIdea)
                {
                    try
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = LinkManager.GetItemUrl(a);
                        drrepIdeas["cssClass"] = SG50Class.arr_east_region_CSS_Classes[east];
                        if (a["Idea"].ToString().Count() > 31)
                        {
                            drrepIdeas["ideaText"] = a["Idea"].ToString().Remove(30) + "...";
                        }
                        else
                        {
                            drrepIdeas["ideaText"] = a["Idea"];
                        }
                        drrepIdeas["Name"] = a["Name"];
                        drrepIdeas["Age"] = a["Age"];
                        drrepIdeas["Region"] = a["Region"];

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        east++;
                    }
                    catch (Exception exSub1)
                    {
                        Log.Info("Events_listing_sublayoutSublayout FillrepIdeas a exSub1 : " + exSub1.ToString(), this);
                    }
                }

                if (east < 8)
                {
                    for (int i = east - 1; i < 7; i++)
                    {
                        drrepIdeas = dtrepIdeas.NewRow();
                        drrepIdeas["href"] = "";
                        drrepIdeas["cssClass"] = SG50Class.arr_east_region_CSS_Classes[east];
                        drrepIdeas["ideaText"] = "";
                        drrepIdeas["Name"] = "";
                        drrepIdeas["Age"] = "";
                        drrepIdeas["visible"] = "display: none;";
                        drrepIdeas["Region"] = "";

                        dtrepIdeas.Rows.Add(drrepIdeas);
                        east++;
                    }
                }
                //GenerateResults(dtrepIdeas);
                repIdeas.DataSource = dtrepIdeas;
                repIdeas.DataBind();

                repIdeasMobile.DataSource = dtrepIdeas;
                repIdeasMobile.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepIdeas exMain : " + exMain.ToString(), this);
            }
        }

        protected string getSubmitAnIdeaLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Submit_An_Idea_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Submit_An_Idea_Item_ID));
            }
            return link;
        }

        protected string getRegionLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Share_An_Idea_Item_ID));
            }
            return link;
        }

        protected string getRecentLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID));
            }
            return link;
        }

        protected string getSeeAllLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID));
            }
            return link;
        }
    }
}