﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Apply_for_funding_responsive_sublayout.Apply_for_funding_responsive_sublayoutSublayout"
    CodeFile="~/SG50/layouts/Apply For Funding Responsive Sublayout.ascx.cs" %>
<link href="/SG50/include/css/style.css" rel="stylesheet" type="text/css" />
<link href="/SG50/include/css/responsive.css" rel="stylesheet" type="text/css" />
<section class="main-content">
<div class="form-banner">
    
<sc:image id="imgBanner" field="Mobile Banner" runat="server" datasource="{8B16191B-48F5-4F18-BDD2-ACF3A6ECCEEF}"  CssStyle="height: auto;min-height: 202px;"/>
</div>
<section class="form-container" style="width: 83%;padding-left: 16px;font-size: 17px;line-height: 20px;margin-bottom: 25px;">
<div class="head-title">
<h1 style="font-size: 35px;line-height: 38px;">
    <sc:text id="txtTitle" field="Mobile Title" runat="server" datasource="{8B16191B-48F5-4F18-BDD2-ACF3A6ECCEEF}" />
</h1>
</div>
<sc:text id="txtContent" field="Mobile Content" runat="server" datasource="{8B16191B-48F5-4F18-BDD2-ACF3A6ECCEEF}"/>
</section>
</section>
