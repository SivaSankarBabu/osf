﻿using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Fields;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Configuration;
namespace Layouts.Event_details_sublayout
{

    /// <summary>
    /// Summary description for Event_details_sublayoutSublayout
    /// </summary>
    public partial class Event_details_sublayoutSublayout : System.Web.UI.UserControl
    {
        CommonMethods cmObj = new CommonMethods();

        Item itmContext = Sitecore.Context.Item;
        int latestCount = 6;

        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            fillRotatingBanner();
            FillrepLatest();
            if (Microsoft.Security.Application.Encoder.HtmlEncode(Request.QueryString["Past"]) == null)
            {
                SocialSharing();
            }
        }

        protected string getImgLink()
        {
            string imglink = "";

            if (itmContext.Fields["Image Gallery"] != null && !itmContext["Image Gallery"].Equals(""))
            {
                Sitecore.Data.Fields.MultilistField mfBanners = itmContext.Fields["Image Gallery"];
                if (mfBanners != null)
                {
                    MediaItem item = Sitecore.Context.Database.GetItem(mfBanners.TargetIDs[0].ToString());
                    imglink = Sitecore.Resources.Media.MediaManager.GetMediaUrl(item);
                }
            }
            return imglink;
        }

        protected string getDate()
        {
            string EventDate = string.Empty;
            Sitecore.Data.Fields.DateField date = itmContext.Fields["Event Date"];
            System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);

            Sitecore.Data.Fields.DateField fromdate = itmContext.Fields["Start Date"];
            Sitecore.Data.Fields.DateField todate = itmContext.Fields["End Date"];
            System.DateTime fromdatetime = Sitecore.DateUtil.IsoDateToDateTime(fromdate.Value);
            System.DateTime todatetime = Sitecore.DateUtil.IsoDateToDateTime(todate.Value);



            if (itmContext.Fields["Event Date"] != null && !itmContext["Event Date"].Equals(""))
            {
                EventDate = datetime.ToString("dd MMMM yyyy");

            }
            else if (!string.IsNullOrEmpty(itmContext.Fields["Start Date"].ToString()) && !string.IsNullOrEmpty(itmContext.Fields["End Date"].ToString()))
            {
                if (fromdatetime.ToString("MMMM yyyy") == todatetime.ToString("MMMM yyyy"))
                {
                    EventDate = fromdatetime.ToString("MMMM yyyy");
                }
                else
                {
                    EventDate = fromdatetime.ToString("MMMM yyyy") + " - " + todatetime.ToString("MMMM yyyy");
                }
            }
            return EventDate;
        }

        protected string getCaption()
        {
            string caption = "";

            if (itmContext.Fields["Image Gallery"] != null && !itmContext["Image Gallery"].Equals(""))
            {
                Sitecore.Data.Fields.MultilistField mfBanners = itmContext.Fields["Image Gallery"];
                if (mfBanners != null)
                {
                    MediaItem item = Sitecore.Context.Database.GetItem(mfBanners.TargetIDs[0].ToString());
                    caption = item.Alt.ToString();
                }
            }
            return caption;
        }

        private void fillRotatingBanner()
        {
            DataTable dtrepSlide = new DataTable();
            dtrepSlide.Columns.Add("imgSrc", typeof(string));
            dtrepSlide.Columns.Add("slideID", typeof(string));
            dtrepSlide.Columns.Add("caption", typeof(string));
            DataRow drrepSlide;

            DataTable dtrepMobileGallery = new DataTable();
            dtrepMobileGallery.Columns.Add("imgSrc", typeof(string));
            dtrepMobileGallery.Columns.Add("caption", typeof(string));
            DataRow drrepMobileGallery;

            DataTable dtrepThumbslider = new DataTable();
            dtrepThumbslider.Columns.Add("number", typeof(string));
            dtrepThumbslider.Columns.Add("thumbImgSrc", typeof(string));
            DataRow drrepThumbslider;

            int BannerCount = 0;
            int slideID = 0;

            if (itmContext.Fields["Image Gallery"] != null && !itmContext["Image Gallery"].Equals(""))
            {
                Sitecore.Data.Fields.MultilistField mfBanners = itmContext.Fields["Image Gallery"];
                if (mfBanners != null)
                {
                    foreach (ID id in mfBanners.TargetIDs)
                    {
                        BannerCount++;
                        slideID++;
                        if (BannerCount <= 7)
                        {
                            drrepSlide = dtrepSlide.NewRow();
                            MediaItem item = Sitecore.Context.Database.GetItem(id.ToString());
                            if (item != null)
                            {
                                drrepSlide["imgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(item) + "?h=425";
                                drrepSlide["caption"] = item.Alt.ToString();
                                drrepSlide["slideID"] = "slide" + slideID.ToString();
                                dtrepSlide.Rows.Add(drrepSlide);

                                drrepThumbslider = dtrepThumbslider.NewRow();
                                drrepThumbslider["number"] = BannerCount.ToString();
                                drrepThumbslider["thumbImgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(item) + "?h=70";
                                dtrepThumbslider.Rows.Add(drrepThumbslider);

                                drrepMobileGallery = dtrepMobileGallery.NewRow();
                                drrepMobileGallery["imgSrc"] = Sitecore.Resources.Media.MediaManager.GetMediaUrl(item);
                                drrepMobileGallery["caption"] = item.Alt.ToString();
                                dtrepMobileGallery.Rows.Add(drrepMobileGallery);
                            }

                        }
                    }

                    repSlide.DataSource = dtrepSlide;
                    repSlide.DataBind();
                    repThumbslider.DataSource = dtrepThumbslider;
                    repThumbslider.DataBind();
                    repMobileGallery.DataSource = dtrepMobileGallery;
                    repMobileGallery.DataBind();
                }
            }

            if (BannerCount == 0)
            {
                sliderContainer.Visible = false;
            }
            else
            {
                sliderContainer.Visible = true;
            }
        }

        private void FillrepLatest()
        {
            try
            {
                DataTable dtrepLatest = new DataTable();
                dtrepLatest.Columns.Add("item", typeof(Item));
                dtrepLatest.Columns.Add("href", typeof(string));
                dtrepLatest.Columns.Add("imgSrc", typeof(string));

                DataRow drrepLatest;

                Item itmPress = SG50Class.web.GetItem(SG50Class.str_Events_Item_ID);

                IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
                                                          where
                                                          a.TemplateID.ToString().Equals(SG50Class.str_Event_Details_Page_Template_ID)
                                                          orderby
                                                          Sitecore.DateUtil.IsoDateToDateTime(a.Fields["Event Date"].Value.ToString())
                                                          select a;

                int count = 0;

                foreach (Item a in itmIEnumPressReleases)
                {
                    Sitecore.Data.Fields.DateField date = a.Fields["Event Date"];
                    System.DateTime datetime = Sitecore.DateUtil.IsoDateToDateTime(date.Value);
                    if (datetime.Date.CompareTo(DateTime.Today.Date) > 0 && count < latestCount)
                    {
                        try
                        {
                            drrepLatest = dtrepLatest.NewRow();

                            drrepLatest["item"] = a;
                            drrepLatest["href"] = LinkManager.GetItemUrl(a).ToString();
                            if (a.Fields["Image Thumbnail"] != null && !a["Image Thumbnail"].ToString().Equals(""))
                            {
                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image Thumbnail"]);
                                string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                if (!imgSrc.Equals(""))
                                {
                                    drrepLatest["imgSrc"] = imgSrc + "?w=80&h=50";
                                }
                            }
                            dtrepLatest.Rows.Add(drrepLatest);

                            count++;
                        }
                        catch (Exception exSub1)
                        {
                            Log.Info("News_release_listing_sublayout FillrepLatest exSub1 : " + exSub1.ToString(), this);
                        }
                    }
                }
                repLatest.DataSource = dtrepLatest;
                repLatest.DataBind();
            }
            catch (Exception exMain)
            {
                Log.Info("News_release_listing_sublayout FillrepLatest exMain : " + exMain.ToString(), this);
            }
        }

        private void SocialSharing()
        {

            DataTable dtrepEvents = new DataTable();
            dtrepEvents.Columns.Add("Id", typeof(string));
            dtrepEvents.Columns.Add("FaceBookTitle", typeof(string));
            dtrepEvents.Columns.Add("FaceBookContent", typeof(string));
            dtrepEvents.Columns.Add("FaceBookThumbnail", typeof(string));
            dtrepEvents.Columns.Add("TwitterContent", typeof(string));
            dtrepEvents.Columns.Add("href", typeof(string));
            dtrepEvents.Columns.Add("HostName", typeof(string));
            dtrepEvents.Columns.Add("AccessToken", typeof(string));
            dtrepEvents.Columns.Add("EventUrl", typeof(string));
            DataRow drrepEvents;

            try
            {
                string SocialTitle = string.Empty;
                string SocialContent = string.Empty;
                string TwitterContent = string.Empty;
                string noHTML = string.Empty;
                string inputHTML = string.Empty;

                drrepEvents = dtrepEvents.NewRow();
                drrepEvents["HostName"] = hostName + "/";
                drrepEvents["AccessToken"] = accessToken;
                drrepEvents["href"] = LinkManager.GetItemUrl(itmContext).ToString();
                drrepEvents["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(itmContext).ToString();

                if (!string.IsNullOrEmpty(itmContext.Fields["Social Share Title"].ToString()))
                {
                    drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["Social Share Title"].ToString(), CommonMethods.faceBook);
                }
                else
                {
                    drrepEvents["FaceBookTitle"] = cmObj.BuildString(itmContext.Fields["Menu Title"].ToString(), CommonMethods.faceBook);
                }

                if (!string.IsNullOrEmpty(itmContext.Fields["Social Share Content"].ToString()))
                {
                    drrepEvents["FaceBookContent"] = cmObj.BuildString(itmContext.Fields["Social Share Content"].ToString(), CommonMethods.faceBook); //+ " " + hostName + LinkManager.GetItemUrl(itmContext);
                }
                else
                {
                    inputHTML = Sitecore.Web.UI.WebControls.FieldRenderer.Render(itmContext, "Blurb");// itmContext.Fields["Blurb"].ToString();
                    drrepEvents["FaceBookContent"] = cmObj.BuildString(Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim(), CommonMethods.faceBook);
                }
                Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Social Share Image"]);
                string fbimgSrc = fbimgField.Src;
                if (!fbimgSrc.Equals(""))
                {
                    drrepEvents["FaceBookThumbNail"] = hostName + "/" + fbimgSrc;
                }
                else
                {
                    Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)itmContext.Fields["Image Thumbnail"]);
                    if (!string.IsNullOrEmpty(staticimgField.Src))
                    {
                        drrepEvents["FaceBookThumbNail"] = hostName + "/" + staticimgField.Src;
                    }
                    else
                    {
                        drrepEvents["FaceBookThumbNail"] = "";
                    }

                }

                if (!string.IsNullOrEmpty(itmContext.Fields["Twitter Content"].ToString()) && !string.IsNullOrWhiteSpace(itmContext.Fields["Twitter Content"].ToString()))
                    TwitterContent = itmContext.Fields["Twitter Content"].ToString();
                else
                {
                    inputHTML = Sitecore.Web.UI.WebControls.FieldRenderer.Render(itmContext, "Blurb");// itmContext.Fields["Blurb"].ToString();
                    noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
                    if (noHTML.Length > 120)
                        TwitterContent = noHTML.Substring(0, 119);
                    else
                        TwitterContent = noHTML.Substring(0, noHTML.Length);

                }

                drrepEvents["TwitterContent"] = cmObj.BuildString(TwitterContent, CommonMethods.faceBook);


                drrepEvents["Id"] = itmContext.ID;
                dtrepEvents.Rows.Add(drrepEvents);
                repsocialSharing.DataSource = dtrepEvents;
                repsocialSharing.DataBind();

            }
            catch (Exception exSub1)
            {
                Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            }
        }
    }
}