﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using Sitecore.Data.Fields;
using System.Text.RegularExpressions;

namespace Layouts.Collectiblessublayout
{

    /// <summary>
    /// Summary description for CollectiblessublayoutSublayout
    /// </summary>
    public partial class CollectiblessublayoutSublayout : System.Web.UI.UserControl
    {

        CommonMethods cmObj = new CommonMethods();

        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        string accessToken = itemconfiguration["Twitter Bitly AccessToken"].ToString();
        private void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillNewCollectibles();
                SocialSharing();
            }

            // Put user code to initialize the page here
        }

        private void FillNewCollectibles()
        {
            //try
            //{
            DataTable dtNewCollectibles = new DataTable();
            dtNewCollectibles.Columns.Add("item", typeof(Item));
            dtNewCollectibles.Columns.Add("imgSrc", typeof(string));
            dtNewCollectibles.Columns.Add("Title", typeof(string));
            dtNewCollectibles.Columns.Add("Description", typeof(string));
            dtNewCollectibles.Columns.Add("Id", typeof(string));
            dtNewCollectibles.Columns.Add("href", typeof(string));
            dtNewCollectibles.Columns.Add("ButtonText", typeof(string));
            dtNewCollectibles.Columns.Add("Status", typeof(Boolean));
            dtNewCollectibles.Columns.Add("ALT", typeof(string));
            DataRow drNewCollectibles;
            Item itmNewCollectibles = web.GetItem("{5E04DC6C-B5E2-4313-B0D0-46B7803085D7}");
            Sitecore.Collections.ChildList NewCollectibleschild = itmNewCollectibles.GetChildren();
            foreach (Item a in NewCollectibleschild)
            {
                drNewCollectibles = dtNewCollectibles.NewRow();
                drNewCollectibles["item"] = a;
                drNewCollectibles["title"] = a.Fields["Title"].ToString();
                drNewCollectibles["description"] = a.Fields["Description"].ToString();
                drNewCollectibles["Id"] = a.ID.ToString();

                Sitecore.Data.Fields.LinkField externalURL = ((Sitecore.Data.Fields.LinkField)a.Fields["URL"]);

                if (externalURL != null)
                {
                    if (!string.IsNullOrEmpty(externalURL.Url))
                    {
                        drNewCollectibles["href"] = externalURL.Url;
                    }
                }

                if (string.IsNullOrEmpty(a.Fields["ButtonText"].ToString()))
                    drNewCollectibles["Status"] = false;
                else
                    drNewCollectibles["Status"] = true;

                drNewCollectibles["buttontext"] = a.Fields["ButtonText"].ToString();

                Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Main Image"]);
                if (mainImgField != null)
                {
                    string mainimgSrc = mainImgField.Src;
                    if (!string.IsNullOrEmpty(mainimgSrc))
                    {
                        drNewCollectibles["imgSrc"] = mainimgSrc;
                        drNewCollectibles["ALT"] = mainImgField.Alt;
                    }
                }
                dtNewCollectibles.Rows.Add(drNewCollectibles);

            }
            //repNewCollectibles.DataSource = dtNewCollectibles;
            PagedDataSource pgsource = new PagedDataSource();
            pgsource.DataSource = dtNewCollectibles.DefaultView;
            pgsource.AllowPaging = true;
            pgsource.PageSize = 9;
            pgsource.CurrentPageIndex = PageNumber;

            if (PageNumber == 0)
            {
                PrevControl.Enabled = false;
            }
            else
            {
                PrevControl.Enabled = true;
            }
            //int pageNationCount = 0;
            //pageNationCount = (dtNewCollectibles.Rows.Count / pgsource.PageSize) - 1;
            if (PageNumber == pgsource.PageCount - 1)
                NextControl.Enabled = false;
            else
                NextControl.Enabled = true;

            DataTable dtNewCollectiblePaging = new DataTable();
            dtNewCollectiblePaging.Columns.Add("ID", typeof(string));
            dtNewCollectiblePaging.Columns.Add("style", typeof(string));
            dtNewCollectiblePaging.Columns.Add("Enable", typeof(Boolean));

            if (pgsource.PageCount > 1)
            {
                DataRow drNewCollectiblePaging;
                //Style="padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important"
                string css = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;";
                string Activecss = "padding: 8px; margin: 2px; background: #ffa100; border: solid 1px #666; font: 8pt tahoma;background-color: #e6173e !important";
                rptPaging.Visible = true;
                System.Collections.ArrayList pages = new System.Collections.ArrayList();
                for (int i = 0; i <= pgsource.PageCount - 1; i++)
                {
                    drNewCollectiblePaging = dtNewCollectiblePaging.NewRow();
                    drNewCollectiblePaging["ID"] = (i + 1).ToString();
                    if (pgsource.CurrentPageIndex + 1 == i + 1)
                    {
                        drNewCollectiblePaging["style"] = Activecss;
                        drNewCollectiblePaging["Enable"] = false;
                    }
                    else
                    {
                        drNewCollectiblePaging["style"] = css;
                        drNewCollectiblePaging["Enable"] = true;
                    }

                    dtNewCollectiblePaging.Rows.Add(drNewCollectiblePaging);
                    //pages.Add((i + 1).ToString());
                }

                rptPaging.DataSource = dtNewCollectiblePaging;
                rptPaging.DataBind();
            }
            else
            {
                rptPaging.Visible = false;
                if (pgsource.PageCount <= 1)
                {
                    PrevControl.Visible = false;
                    NextControl.Visible = false;
                }

            }

            //Finally, set the datasource of the repeoater
            repNewCollectibles.DataSource = pgsource;
            //RepCourse.DataBind();
            repNewCollectibles.DataBind();

            //}
            //catch (Exception exSub1)
            //{
            //    //Log.Info("Events_listing_sublayoutSublayout FillrepEvents exSub1 : " + exSub1.ToString(), this);
            //}
        }

        private void SocialSharing()
        {
            DataTable dtSocialSharing = new DataTable();
            dtSocialSharing.Columns.Add("Id", typeof(string));
            dtSocialSharing.Columns.Add("SocialShareTitle", typeof(string));
            dtSocialSharing.Columns.Add("SocialShareContent", typeof(string));
            dtSocialSharing.Columns.Add("ItemName", typeof(string));
            dtSocialSharing.Columns.Add("href", typeof(string));
            dtSocialSharing.Columns.Add("HostName", typeof(string));
            dtSocialSharing.Columns.Add("AccessToken", typeof(string));
            dtSocialSharing.Columns.Add("EventUrl", typeof(string));
            dtSocialSharing.Columns.Add("SocialSharingThumbNail", typeof(string));
            DataRow drSocialSharing;

            //  Item itmSocialSharing = SG50Class.web.GetItem("{5E04DC6C-B5E2-4313-B0D0-46B7803085D7}");
            drSocialSharing = dtSocialSharing.NewRow();
            drSocialSharing["HostName"] = hostName + "/";
            drSocialSharing["AccessToken"] = accessToken;
            drSocialSharing["href"] = LinkManager.GetItemUrl(itmcontext).ToString();
            drSocialSharing["EventUrl"] = hostName + "/" + LinkManager.GetItemUrl(itmcontext).ToString();
            if (!string.IsNullOrEmpty(itmcontext.Fields["Social Sharing Title"].ToString()))
            {
                drSocialSharing["SocialShareTitle"] = cmObj.BuildString(itmcontext.Fields["Social Sharing Title"].ToString(), CommonMethods.faceBook);
            }
            //else
            //{
            //    drSocialSharing["SocialShareTitle"] = cmObj.BuildString(itmcontext.Fields["Title"].ToString(), CommonMethods.faceBook);
            //}
            if (!string.IsNullOrEmpty(itmcontext.Fields["Social Sharing Description"].ToString()))
            {
                drSocialSharing["SocialShareContent"] = cmObj.BuildString(itmcontext.Fields["Social Sharing Description"].ToString(), CommonMethods.faceBook);
            }
            //else
            //{
            //    drSocialSharing["SocialShareContent"] = cmObj.BuildString(itmcontext.Fields["Description"].ToString(), CommonMethods.faceBook);
            //}


            Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)itmcontext.Fields["SocialSharingThumbImage"]);
            string fbimgSrc = fbimgField.Src;
            if (!fbimgSrc.Equals(""))
            {
                drSocialSharing["SocialSharingThumbNail"] = hostName + "/" + fbimgSrc;
            }
            else
            {
                //Sitecore.Data.Fields.ImageField staticimgField = ((Sitecore.Data.Fields.ImageField)itmcontext.Fields["Banner Image"]);
                //if (!string.IsNullOrEmpty(staticimgField.Src))
                //{
                //    drSocialSharing["SocialSharingThumbNail"] = hostName + "/" + staticimgField.Src;
                //}
                //else
                //{
                drSocialSharing["SocialSharingThumbNail"] = "";
                //}

            }

            dtSocialSharing.Rows.Add(drSocialSharing);
            repsocialSharingTop.DataSource = dtSocialSharing;
            repsocialSharingTop.DataBind();
        }

        protected void repNewCollectibles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //PageNumber = System.Convert.ToInt32(e.CommandArgument) - 1;
            //FillNewCollectibles();
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                {
                    return System.Convert.ToInt32(ViewState["PageNumber"]);
                }
                else
                {
                    return 0;
                }
            }
            set { ViewState["PageNumber"] = value; }
        }
        protected void rptPaging_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            PageNumber = System.Convert.ToInt32(e.CommandArgument) - 1;
            FillNewCollectibles();
        }
        protected void PrevControl_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;
            FillNewCollectibles();
        }
        protected void NextControl_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;
            FillNewCollectibles();
        }
    }
}