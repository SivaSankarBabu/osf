﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System.Data;
using System.Text.RegularExpressions;
using FUNDED_Logger;
using System.Globalization;
namespace Layouts.Applicationstep2
{
    public partial class Applicationstep2Sublayout : System.Web.UI.UserControl
    {
        Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        private void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    Logger.WriteLine("3. Funded form step2 process Started");
                    Session.Timeout = 45;
                    SetInitialRow();
                    SetInitialRow1();
                    SetInitialRow2();
                    if (Session["Budjet"] != null)
                    {
                        GridView1.DataSource = Session["Budjet"];
                        GridView1.DataBind();

                        DataTable dd = new DataTable();
                        dd = (DataTable)Session["Budjet"];
                        ulong count1 = 0;
                        for (int i = 0; i < dd.Rows.Count; i++)
                        {
                            string aa = dd.Rows[i]["Projected"].ToString();

                            if (!string.IsNullOrEmpty(aa))
                                count1 = count1 + Convert.ToUInt64(aa);

                        }
                        txtCount1.Text = count1.ToString();
                    }
                    if (Session["donations"] != null)
                    {
                        GridView2.DataSource = Session["donations"];
                        GridView2.DataBind();

                    }
                    if (Session["Timeline"] != null)
                    {
                        GridView3.DataSource = Session["Timeline"];
                        GridView3.DataBind();
                    }
                    if (Session["ItemId"] != null)
                    {
                        string ItemId = Session["ItemId"].ToString();
                        Item FormData = master.GetItem(ItemId);
                        txtProjTitle.Text = FormData["Project Title"];
                        txtObjectives.Text = FormData["Project Objectives"];
                        txtintendedparticipants.Text = FormData["Intended Participants"];
                        txtEstparticipants.Text = FormData["Esitimated NoOfparticipants"];
                        txtAmountRequested.Text = FormData["Amount Requested"];
                        txtTotalExpenditure.Text = FormData["Projected Expenditure"];
                        txtStartDate.Text = FormData["Start Date"];
                        txtEndDate.Text = FormData["End Date"];
                        txtRelevantExp.Text = FormData["Relevant Project Experience"];
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteException(ex, "Exception Occurred");
                }
            }
        }

        private void SetInitialRow()
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dt.Columns.Add(new DataColumn("Item", typeof(string)));
                dt.Columns.Add(new DataColumn("Projected", typeof(string)));

                dr = dt.NewRow();
                dr["RowNumber"] = 1;
                dr["Item"] = string.Empty;
                dr["Projected"] = string.Empty;

                dt.Rows.Add(dr);

                //Store the DataTable in ViewState
                ViewState["CurrentTable"] = dt;

                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        private void SetInitialRow1()
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dt.Columns.Add(new DataColumn("source", typeof(string)));
                dt.Columns.Add(new DataColumn("amount", typeof(string)));
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("duration", typeof(string)));

                dr = dt.NewRow();
                dr["RowNumber"] = 1;
                dr["source"] = string.Empty;
                dr["amount"] = string.Empty;
                dr["Status"] = string.Empty;
                dr["duration"] = string.Empty;

                dt.Rows.Add(dr);

                //Store the DataTable in ViewState
                ViewState["CurrentTable1"] = dt;

                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        private void SetInitialRow2()
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
                dt.Columns.Add(new DataColumn("Date", typeof(string)));
                dt.Columns.Add(new DataColumn("Event", typeof(string)));
                dt.Columns.Add(new DataColumn("Milestone", typeof(string)));

                dr = dt.NewRow();
                dr["RowNumber"] = 1;
                dr["Date"] = string.Empty;
                dr["Event"] = string.Empty;
                dr["Milestone"] = string.Empty;

                dt.Rows.Add(dr);

                //Store the DataTable in ViewState
                ViewState["CurrentTable2"] = dt;

                GridView3.DataSource = dt;
                GridView3.DataBind();
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        public static System.Data.DataTable ToDataTable2(Repeater dataGridView)
        {

            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Date", typeof(string)));
            dt.Columns.Add(new DataColumn("Event", typeof(string)));
            dt.Columns.Add(new DataColumn("Milestone", typeof(string)));
            try
            {
                int cnt = 1;
                foreach (RepeaterItem row in dataGridView.Items)
                {


                    System.Web.UI.WebControls.HiddenField RowNumber = new System.Web.UI.WebControls.HiddenField();
                    System.Web.UI.WebControls.TextBox Date = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.TextBox Event = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.TextBox Milestone = new System.Web.UI.WebControls.TextBox();


                    RowNumber = (System.Web.UI.WebControls.HiddenField)row.FindControl("rowno");
                    Date = (System.Web.UI.WebControls.TextBox)row.FindControl("txtDate");
                    Event = (System.Web.UI.WebControls.TextBox)row.FindControl("txtEvent");
                    Milestone = (System.Web.UI.WebControls.TextBox)row.FindControl("txtMilestone");


                    dt.Rows.Add(RowNumber.Value, Date.Text, Event.Text, Milestone.Text);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }

            return dt;

        }
        public static System.Data.DataTable ToDataTable(Repeater dataGridView)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Item", typeof(string)));
            dt.Columns.Add(new DataColumn("Projected", typeof(string)));

            try
            {
                int cnt = 1;
                foreach (RepeaterItem items in dataGridView.Items)
                {


                    System.Web.UI.WebControls.HiddenField RowNumber = new System.Web.UI.WebControls.HiddenField();
                    System.Web.UI.WebControls.TextBox Item = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.TextBox Projected = new System.Web.UI.WebControls.TextBox();


                    RowNumber = (System.Web.UI.WebControls.HiddenField)items.FindControl("rowno");
                    Item = (System.Web.UI.WebControls.TextBox)items.FindControl("txtItem");
                    Projected = (System.Web.UI.WebControls.TextBox)items.FindControl("txtProjected");


                    dt.Rows.Add(RowNumber.Value, Item.Text, Projected.Text);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
            return dt;
        }

        public bool validationCheck()
        {
            bool validation = false;

            try
            {
                string strtxtProjTitle = SG50Class.StripUnwantedString(txtProjTitle.Text.TrimEnd());
                string strtxtObjectives = SG50Class.StripUnwantedString(txtObjectives.Text.TrimEnd());
                string strtxtintendedparticipants = SG50Class.StripUnwantedString(txtintendedparticipants.Text.TrimEnd());
                string strtxtEstparticipants = SG50Class.StripUnwantedString(txtEstparticipants.Text.TrimEnd());
                string strtxtAmountRequested = SG50Class.StripUnwantedString(txtAmountRequested.Text.TrimEnd());
                string strtxtTotalExpenditure = SG50Class.StripUnwantedString(txtTotalExpenditure.Text.TrimEnd());
                string strtxtStartDate = txtStartDate.Text.TrimEnd();
                string strtxtEndDate = txtEndDate.Text.TrimEnd();
                string strtxtRelevantExp = SG50Class.StripUnwantedString(txtRelevantExp.Text.Trim());
                if (strtxtProjTitle != "" && strtxtObjectives != "" && strtxtintendedparticipants != "" && strtxtEstparticipants != "" && strtxtAmountRequested != "" && strtxtTotalExpenditure != "" && strtxtStartDate != "" && strtxtEndDate != "" && strtxtRelevantExp != "")
                {
                    int EnteredIntValue = 0;
                    double EnteredIntValue1 = 0;
                    DateTime d;
                    if (int.TryParse(txtEstparticipants.Text, out EnteredIntValue) && int.TryParse(txtAmountRequested.Text, out EnteredIntValue) && double.TryParse(txtTotalExpenditure.Text, out EnteredIntValue1) && DateTime.TryParseExact(strtxtStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d) && DateTime.TryParseExact(strtxtEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                    {

                        if (strtxtProjTitle.Length <= 100 && strtxtObjectives.Length <= 2000 && strtxtintendedparticipants.Length <= 100 && strtxtEstparticipants.Length <= 6 && strtxtTotalExpenditure.Length <= 9 && strtxtRelevantExp.Length <= 2000 && strtxtAmountRequested.Length <= 5)
                        {

                            validation = true;
                        }
                        else
                        {
                            validation = false;
                        }
                    }
                    else
                    {
                        validation = false;
                    }
                }

                foreach (RepeaterItem items in GridView1.Items)
                {

                    System.Web.UI.WebControls.TextBox Item = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.TextBox Projected = new System.Web.UI.WebControls.TextBox();
                    Item = (System.Web.UI.WebControls.TextBox)items.FindControl("txtItem");
                    Projected = (System.Web.UI.WebControls.TextBox)items.FindControl("txtProjected");
                    if (Item.Text.TrimEnd() != "" && Projected.Text.TrimEnd() != "")
                    {
                        int EnteredIntValue = 0;
                        if (int.TryParse(Projected.Text, out EnteredIntValue))
                        {
                            if (Item.Text.Length <= 100 && Projected.Text.Length <= 8)
                            {
                                if (validation == true)
                                    validation = true;
                            }
                            else
                            {
                                validation = false;
                            }
                        }
                        else
                        {
                            validation = false;
                        }


                    }
                }
                foreach (RepeaterItem row in GridView3.Items)
                {

                    System.Web.UI.WebControls.TextBox Date = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.TextBox Event = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.TextBox Milestone = new System.Web.UI.WebControls.TextBox();

                    Date = (System.Web.UI.WebControls.TextBox)row.FindControl("txtDate");
                    Event = (System.Web.UI.WebControls.TextBox)row.FindControl("txtEvent");
                    Milestone = (System.Web.UI.WebControls.TextBox)row.FindControl("txtMilestone");
                    DateTime d;
                    if (Event.Text.TrimEnd() != "" && Milestone.Text.TrimEnd() != "" && Date.Text.TrimEnd() != "")
                    {
                        if (Event.Text.Length <= 100 && Milestone.Text.Length <= 100 && DateTime.TryParseExact(Date.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                        {
                            if (validation == true)
                                validation = true;
                        }
                        else
                        {
                            validation = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }

            return validation;
        }

        protected void btnNextStep_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    if (validationCheck() == true)
                    {
                        string strtxtProjTitle = txtProjTitle.Text.TrimEnd();
                        string strtxtObjectives = txtObjectives.Text.TrimEnd();
                        string strtxtintendedparticipants = txtintendedparticipants.Text.TrimEnd();
                        string strtxtEstparticipants = txtEstparticipants.Text.TrimEnd();
                        string strtxtAmountRequested = txtAmountRequested.Text.TrimEnd();
                        string strtxtTotalExpenditure = txtTotalExpenditure.Text.TrimEnd();
                        string strtxtStartDate = txtStartDate.Text.TrimEnd();
                        string strtxtEndDate = txtEndDate.Text.TrimEnd();
                        string strtxtRelevantExp = txtRelevantExp.Text.Trim();

                        Logger.WriteLine("4. Submitting step2 form values.");
                        Logger.WriteLine("5. Project Name" + txtProjTitle.Text);
                        string ext1 = System.IO.Path.GetExtension(fupFileone.FileName);
                        string ext2val = System.IO.Path.GetExtension(fupFiletwo.FileName);
                        string ext3val = System.IO.Path.GetExtension(fupFileThree.FileName);
                        string ext4val = System.IO.Path.GetExtension(fupFileFour.FileName);
                        string ext5val = System.IO.Path.GetExtension(fupFileFive.FileName);

                        string[] allowedExtension = new string[] { ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf" };
                        bool Contains = true;
                        int fileSize = 0;
                        if (fupFileone.HasFile)
                        {
                            if (!allowedExtension.Contains(ext1))
                            {
                                lblfup1.Text = "File Uploaded Format is in incorrect format OR File's size Exceeds 5mb";
                                Contains = false;
                            }
                            else
                            {
                                fileSize = fileSize + fupFileone.PostedFile.ContentLength;
                            }

                        }

                        if (fupFiletwo.HasFile)
                        {
                            if (!allowedExtension.Contains(ext2val))
                            {
                                lblfup1.Text = "File Uploaded Format is in incorrect format OR Exceed 5mb";
                                Contains = false;
                            }
                            else
                            {
                                fileSize = fileSize + fupFiletwo.PostedFile.ContentLength;
                            }
                        }


                        if (fupFileThree.HasFile)
                        {
                            if (!allowedExtension.Contains(ext3val))
                            {
                                lblfup1.Text = "File Uploaded Format is in incorrect format OR Exceed 5mb";
                                Contains = false;
                            }
                            else
                            {
                                fileSize = fileSize + fupFileThree.PostedFile.ContentLength;
                            }
                        }


                        if (fupFileFour.HasFile)
                        {
                            if (!allowedExtension.Contains(ext4val))
                            {
                                lblfup1.Text = "File Uploaded Format is in incorrect format OR Exceed 5mb";
                                Contains = false;
                            }
                            else
                            {
                                fileSize = fileSize + fupFileFour.PostedFile.ContentLength;
                            }
                        }


                        if (fupFileFive.HasFile)
                        {
                            if (!allowedExtension.Contains(ext5val))
                            {
                                lblfup1.Text = "File Uploaded Format is in incorrect format OR Exceed 5mb";
                                Contains = false;
                            }
                            else
                            {
                                fileSize = fileSize + fupFileFive.PostedFile.ContentLength;
                            }
                        }
                        if (Contains == true && fileSize <= 5242880)
                        {

                            Session["Budjet"] = ToDataTable(GridView1);
                            Session["donations"] = ToDataTable1(GridView2);
                            Session["Timeline"] = ToDataTable2(GridView3);

                            using (new Sitecore.SecurityModel.SecurityDisabler())
                            {
                                Item newForm = null;
                                if (Session["ItemId"] == null)
                                {
                                    Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/Application Form");
                                    Item parentItem = master.GetItem("{5AB0BF7A-506A-4B74-A9F5-EFA39BE16770}");
                                    string ItemName = Guid.NewGuid().ToString();
                                    newForm = parentItem.Add(ItemName.ToString(), template);
                                    Session["ItemId"] = newForm.ID;
                                }
                                else
                                {
                                    string ItemId = Session["ItemId"].ToString();
                                    newForm = master.GetItem(ItemId);
                                }

                                try
                                {
                                    newForm.Editing.BeginEdit();
                                    newForm.Fields["Project Title"].Value = Convert.ToString(txtProjTitle.Text);
                                    newForm.Fields["Project Objectives"].Value = Convert.ToString(txtObjectives.Text);
                                    newForm.Fields["Esitimated NoOfparticipants"].Value = Convert.ToString(txtEstparticipants.Text);
                                    newForm.Fields["Intended Participants"].Value = Convert.ToString(txtintendedparticipants.Text);
                                    newForm.Fields["Amount Requested"].Value = Convert.ToString(txtAmountRequested.Text);
                                    newForm.Fields["Projected Expenditure"].Value = Convert.ToString(txtTotalExpenditure.Text);
                                    newForm.Fields["Start Date"].Value = Convert.ToString(txtStartDate.Text);
                                    newForm.Fields["End Date"].Value = Convert.ToString(txtEndDate.Text);
                                    newForm.Fields["Relevant Project Experience"].Value = Convert.ToString(txtRelevantExp.Text);
                                    newForm.Editing.EndEdit();

                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                try
                                {

                                    DataTable DtFiles = new DataTable();
                                    DtFiles.Columns.Add("Id", typeof(string));
                                    DtFiles.Columns.Add("TotalFileName", typeof(string));
                                    if (fupFileone.HasFile)
                                    {
                                        string ext = System.IO.Path.GetExtension(fupFileone.FileName);
                                        if (allowedExtension.Contains(ext) && fupFileone.PostedFile.ContentLength <= 5242880)
                                        {
                                            Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                                            options.Database = master;
                                            options.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                                            options.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                                            string filename = fupFileone.FileName.Replace(".doc", "").Replace(".docx", "").Replace(".xls", "").Replace(".xlsx", "").Replace(".ppt", "").Replace(".pptx", "").Replace(".pdf", "");
                                            filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                                            options.Destination = "/sitecore/media library/SG50/Applied Forms/" + filename;
                                            fupFileone.SaveAs(Server.MapPath("/SG50/Applied Forms/" + fupFileone.FileName));
                                            options.FileBased = false;
                                            Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                                            Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/Applied Forms/" + fupFileone.FileName), options);

                                            DataRow drone = DtFiles.NewRow();
                                            drone["Id"] = sample.ID.ToString();
                                            drone["TotalFileName"] = fupFileone.FileName;
                                            DtFiles.Rows.Add(drone);
                                        }
                                    }

                                    if (fupFiletwo.HasFile)
                                    {
                                        string ext = System.IO.Path.GetExtension(fupFiletwo.FileName);
                                        if (allowedExtension.Contains(ext) && fupFiletwo.PostedFile.ContentLength <= 5242880)
                                        {

                                            Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                                            options.Database = master;
                                            options.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                                            options.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                                            string filename = fupFiletwo.FileName.Replace(".doc", "").Replace(".docx", "").Replace(".xls", "").Replace(".xlsx", "").Replace(".ppt", "").Replace(".pptx", "").Replace(".pdf", "");
                                            filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                                            options.Destination = "/sitecore/media library/SG50/Applied Forms/" + filename;
                                            fupFiletwo.SaveAs(Server.MapPath("/SG50/Applied Forms/" + fupFiletwo.FileName));
                                            options.FileBased = false;
                                            Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                                            Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/Applied Forms/" + fupFiletwo.FileName), options);

                                            DataRow drtwo = DtFiles.NewRow();
                                            drtwo["Id"] = sample.ID.ToString();
                                            drtwo["TotalFileName"] = fupFiletwo.FileName;
                                            DtFiles.Rows.Add(drtwo);
                                        }
                                    }

                                    if (fupFileThree.HasFile)
                                    {
                                        string ext = System.IO.Path.GetExtension(fupFileThree.FileName);
                                        if (allowedExtension.Contains(ext) && fupFileThree.PostedFile.ContentLength <= 5242880)
                                        {

                                            Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                                            options.Database = master;
                                            options.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                                            options.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                                            string filename = fupFileThree.FileName.Replace(".doc", "").Replace(".docx", "").Replace(".xls", "").Replace(".xlsx", "").Replace(".ppt", "").Replace(".pptx", "").Replace(".pdf", "");
                                            filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                                            options.Destination = "/sitecore/media library/SG50/Applied Forms/" + filename;
                                            fupFileThree.SaveAs(Server.MapPath("/SG50/Applied Forms/" + fupFileThree.FileName));
                                            options.FileBased = false;
                                            Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                                            Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/Applied Forms/" + fupFileThree.FileName), options);

                                            DataRow drthree = DtFiles.NewRow();
                                            drthree["Id"] = sample.ID.ToString();
                                            drthree["TotalFileName"] = fupFileThree.FileName;
                                            DtFiles.Rows.Add(drthree);
                                        }
                                    }
                                    if (fupFileFour.HasFile)
                                    {
                                        string ext = System.IO.Path.GetExtension(fupFileFour.FileName);
                                        if (allowedExtension.Contains(ext) && fupFileFour.PostedFile.ContentLength <= 5242880)
                                        {

                                            Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                                            options.Database = master;
                                            options.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                                            options.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                                            string filename = fupFileFour.FileName.Replace(".doc", "").Replace(".docx", "").Replace(".xls", "").Replace(".xlsx", "").Replace(".ppt", "").Replace(".pptx", "").Replace(".pdf", "");
                                            filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                                            options.Destination = "/sitecore/media library/SG50/Applied Forms/" + filename;
                                            fupFileFour.SaveAs(Server.MapPath("/SG50/Applied Forms/" + fupFileFour.FileName));
                                            options.FileBased = false;
                                            Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                                            Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/Applied Forms/" + fupFileFour.FileName), options);

                                            DataRow drfour = DtFiles.NewRow();
                                            drfour["Id"] = sample.ID.ToString();
                                            drfour["TotalFileName"] = fupFileFour.FileName;
                                            DtFiles.Rows.Add(drfour);
                                        }
                                    }

                                    if (fupFileFive.HasFile)
                                    {
                                        string ext = System.IO.Path.GetExtension(fupFileFive.FileName);
                                        if (allowedExtension.Contains(ext) && fupFileFive.PostedFile.ContentLength <= 5242880)
                                        {
                                            Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                                            options.Database = master;
                                            options.Language = Sitecore.Globalization.Language.Parse(Sitecore.Configuration.Settings.DefaultLanguage);
                                            options.Versioned = Sitecore.Configuration.Settings.Media.UploadAsVersionableByDefault;
                                            string filename = fupFileFive.FileName.Replace(".doc", "").Replace(".docx", "").Replace(".xls", "").Replace(".xlsx", "").Replace(".ppt", "").Replace(".pptx", "").Replace(".pdf", "");
                                            filename = Regex.Replace(filename, "[^a-zA-Z0-9]", "");
                                            options.Destination = "/sitecore/media library/SG50/Applied Forms/" + filename;
                                            fupFileFive.SaveAs(Server.MapPath("/SG50/Applied Forms/" + fupFileFive.FileName));
                                            options.FileBased = false;
                                            Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                                            Sitecore.Data.Items.MediaItem sample = creator.CreateFromFile(Server.MapPath("/SG50/Applied Forms/" + fupFileFive.FileName), options);

                                            DataRow drfive = DtFiles.NewRow();
                                            drfive["Id"] = sample.ID.ToString();
                                            drfive["TotalFileName"] = fupFileFive.FileName;
                                            DtFiles.Rows.Add(drfive);
                                        }
                                    }
                                    Session["FileId"] = DtFiles;
                                    Session["Step3"] = "True";
                                    if (Session["Organisation"] != null)
                                    {
                                        Logger.WriteLine("6. Redirecting to Step3 Oraganisation Page.");
                                        string url = "~/SG50/Step3Organisation.aspx";
                                        if (!(url.Contains("://")))
                                            Response.Redirect(url);
                                    }
                                    else if (Session["Local"] != null)
                                    {
                                        if (Session["Local"].ToString() == "False")
                                            Logger.WriteLine("6. Redirecting to Step3 overseas Page.");
                                        else if (Session["Local"].ToString() == "True")
                                            Logger.WriteLine("6. Redirecting to Step3 local Page.");

                                        string url = "~/SG50/Step3.aspx";
                                        if (!(url.Contains("://")))
                                            Response.Redirect(url);
                                    }
                                    else
                                    {
                                        Logger.WriteLine(" 7. Session is empty in Stop 2 so redirecting to step1.");
                                        string path = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('There is an error occurred during the submission, Please fill again.'); window.location='" + "/SG50/Apply For Funding.aspx';", true);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                        }
                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Invalid Data.');", true);

                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteException(ex, "Exception Occurred");
                }
            }
        }


        protected void GridView1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AddNewColumn")
                {
                    int rowIndex = 0;

                    if (ViewState["CurrentTable"] != null)
                    {
                        DataTable dtCurrentTable = ToDataTable(GridView1);// (DataTable)ViewState["CurrentTable"];
                        if (dtCurrentTable.Rows.Count < 10)
                        {
                            DataRow drCurrentRow = null;
                            if (dtCurrentTable.Rows.Count > 0)
                            {
                                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                                {
                                    //extract the TextBox values
                                    TextBox box1 = (TextBox)GridView1.Items[rowIndex].Controls[1].FindControl("txtItem");
                                    TextBox box2 = (TextBox)GridView1.Items[rowIndex].Controls[2].FindControl("txtProjected");

                                    drCurrentRow = dtCurrentTable.NewRow();
                                    drCurrentRow["RowNumber"] = i + 1;

                                    dtCurrentTable.Rows[i - 1]["Item"] = box1.Text;
                                    dtCurrentTable.Rows[i - 1]["Projected"] = box2.Text;
                                    rowIndex++;
                                }
                                dtCurrentTable.Rows.Add(drCurrentRow);
                                ViewState["CurrentTable"] = dtCurrentTable;

                                GridView1.DataSource = dtCurrentTable;
                                GridView1.DataBind();
                            }
                        }
                    }


                }
                else if (e.CommandName == "RemoveColumn")
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = ToDataTable(GridView1);

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["RowNumber"].ToString() == e.CommandArgument.ToString())
                        {
                            TextBox txtProjected = (TextBox)e.Item.FindControl("txtProjected");
                            if (!string.IsNullOrEmpty(txtProjected.Text))
                            {
                                ulong count1 = Convert.ToUInt64(txtCount1.Text);
                                txtCount1.Text = Convert.ToString(count1 - Convert.ToUInt64(txtProjected.Text));
                            }
                            row.Delete();
                            break;
                        }

                    }

                    dt.AcceptChanges();
                    ViewState["CurrentTable"] = dt;

                    int count = dt.Rows.Count;
                    System.Data.DataTable dtcount = new System.Data.DataTable();
                    dt.Columns.Remove("RowNumber");
                    DataColumn newCol = new DataColumn("RowNumber", typeof(int));
                    dt.Columns.Add(newCol);


                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        row["RowNumber"] = i;
                        i++;
                    }

                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        protected void GridView1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int rowno = 1;
                    string StrRow = DataBinder.Eval(e.Item.DataItem, "RowNumber").ToString();

                    if (!string.IsNullOrEmpty(StrRow))
                    {
                        rowno = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RowNumber"));
                    }

                    ImageButton minusimage = (ImageButton)e.Item.FindControl("ImageButton2");
                    if (rowno == 1)
                    {
                        minusimage.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }

        public static System.Data.DataTable ToDataTable1(Repeater dataGridView)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("source", typeof(string)));
            dt.Columns.Add(new DataColumn("amount", typeof(string)));
            dt.Columns.Add(new DataColumn("Status", typeof(string)));
            dt.Columns.Add(new DataColumn("duration", typeof(string)));
            try
            {

                int cnt = 1;
                foreach (RepeaterItem row in dataGridView.Items)
                {


                    System.Web.UI.WebControls.HiddenField RowNumber = new System.Web.UI.WebControls.HiddenField();
                    System.Web.UI.WebControls.TextBox source = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.TextBox amount = new System.Web.UI.WebControls.TextBox();
                    System.Web.UI.WebControls.DropDownList Status = new System.Web.UI.WebControls.DropDownList();
                    System.Web.UI.WebControls.TextBox duration = new System.Web.UI.WebControls.TextBox();


                    RowNumber = (System.Web.UI.WebControls.HiddenField)row.FindControl("rowno");
                    source = (System.Web.UI.WebControls.TextBox)row.FindControl("txtsource");
                    amount = (System.Web.UI.WebControls.TextBox)row.FindControl("txtamount");
                    Status = (System.Web.UI.WebControls.DropDownList)row.FindControl("ddlStatus");
                    duration = (System.Web.UI.WebControls.TextBox)row.FindControl("txtduration");


                    dt.Rows.Add(RowNumber.Value, source.Text, amount.Text, Status.SelectedValue, duration.Text);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }

            return dt;
        }
        protected void GridView2_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AddNewColumn")
                {
                    int rowIndex = 0;

                    if (ViewState["CurrentTable1"] != null)
                    {
                        DataTable dtCurrentTable = ToDataTable1(GridView2);// (DataTable)ViewState["CurrentTable"];
                        if (dtCurrentTable.Rows.Count < 10)
                        {
                            DataRow drCurrentRow = null;
                            if (dtCurrentTable.Rows.Count > 0)
                            {
                                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                                {
                                    //extract the TextBox values
                                    TextBox box1 = (TextBox)GridView2.Items[rowIndex].Controls[1].FindControl("txtsource");
                                    TextBox box2 = (TextBox)GridView2.Items[rowIndex].Controls[2].FindControl("txtamount");
                                    DropDownList box3 = (DropDownList)GridView2.Items[rowIndex].Controls[1].FindControl("ddlStatus");
                                    TextBox box4 = (TextBox)GridView2.Items[rowIndex].Controls[2].FindControl("txtduration");

                                    drCurrentRow = dtCurrentTable.NewRow();
                                    drCurrentRow["RowNumber"] = i + 1;

                                    dtCurrentTable.Rows[i - 1]["source"] = box1.Text;
                                    dtCurrentTable.Rows[i - 1]["amount"] = box2.Text;
                                    dtCurrentTable.Rows[i - 1]["Status"] = box3.SelectedValue;
                                    dtCurrentTable.Rows[i - 1]["duration"] = box4.Text;
                                    // dtCurrentTable.Rows[i - 1]["Column3"] = box3.Text;

                                    rowIndex++;
                                }
                                dtCurrentTable.Rows.Add(drCurrentRow);
                                ViewState["CurrentTable1"] = dtCurrentTable;

                                GridView2.DataSource = dtCurrentTable;
                                GridView2.DataBind();
                            }
                        }
                    }

                }
                else if (e.CommandName == "RemoveColumn")
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = ToDataTable1(GridView2);

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["RowNumber"].ToString() == e.CommandArgument.ToString())
                        {

                            row.Delete();
                            break;
                        }

                    }

                    dt.AcceptChanges();
                    ViewState["CurrentTable1"] = dt;

                    int count = dt.Rows.Count;
                    System.Data.DataTable dtcount = new System.Data.DataTable();
                    dt.Columns.Remove("RowNumber");
                    DataColumn newCol = new DataColumn("RowNumber", typeof(int));
                    dt.Columns.Add(newCol);


                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        row["RowNumber"] = i;
                        i++;
                    }

                    GridView2.DataSource = dt;
                    GridView2.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        protected void GridView2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    int rowno = 1;
                    string StrRow = DataBinder.Eval(e.Item.DataItem, "RowNumber").ToString();

                    if (!string.IsNullOrEmpty(StrRow))
                    {
                        rowno = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RowNumber"));
                    }
                    ImageButton minusimage = (ImageButton)e.Item.FindControl("ImageButton2");
                    if (rowno == 1)
                    {

                        minusimage.Visible = false;
                    }
                    string Status = (e.Item.FindControl("hiddenrowno") as HiddenField).Value;
                    if (!string.IsNullOrEmpty(Status))
                    {
                        DropDownList ddlStatus = (e.Item.FindControl("ddlStatus") as DropDownList);
                        ddlStatus.Items.FindByValue(Status).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        protected void GridView3_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int rowno = 1;
                    string StrRow = DataBinder.Eval(e.Item.DataItem, "RowNumber").ToString();

                    if (!string.IsNullOrEmpty(StrRow))
                    {
                        rowno = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RowNumber"));
                    }
                    ImageButton minusimage = (ImageButton)e.Item.FindControl("ImageButton2");
                    if (rowno == 1)
                    {

                        minusimage.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        protected void GridView3_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "AddNewColumn")
                {
                    int rowIndex = 0;

                    if (ViewState["CurrentTable2"] != null)
                    {
                        DataTable dtCurrentTable = ToDataTable2(GridView3);// (DataTable)ViewState["CurrentTable"];
                        if (dtCurrentTable.Rows.Count < 10)
                        {
                            DataRow drCurrentRow = null;
                            if (dtCurrentTable.Rows.Count > 0)
                            {
                                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                                {
                                    //extract the TextBox values
                                    TextBox box1 = (TextBox)GridView3.Items[rowIndex].Controls[1].FindControl("txtDate");
                                    TextBox box2 = (TextBox)GridView3.Items[rowIndex].Controls[2].FindControl("txtEvent");
                                    TextBox box3 = (TextBox)GridView3.Items[rowIndex].Controls[3].FindControl("txtMilestone");

                                    drCurrentRow = dtCurrentTable.NewRow();
                                    drCurrentRow["RowNumber"] = i + 1;

                                    dtCurrentTable.Rows[i - 1]["Date"] = box1.Text;
                                    dtCurrentTable.Rows[i - 1]["Event"] = box2.Text;
                                    dtCurrentTable.Rows[i - 1]["Milestone"] = box3.Text;

                                    rowIndex++;
                                }
                                dtCurrentTable.Rows.Add(drCurrentRow);
                                ViewState["CurrentTable2"] = dtCurrentTable;

                                GridView3.DataSource = dtCurrentTable;
                                GridView3.DataBind();
                            }
                        }
                    }


                }
                else if (e.CommandName == "RemoveColumn")
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = ToDataTable2(GridView3);

                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["RowNumber"].ToString() == e.CommandArgument.ToString())
                        {

                            row.Delete();
                            break;
                        }

                    }

                    dt.AcceptChanges();
                    ViewState["CurrentTable2"] = dt;

                    int count = dt.Rows.Count;
                    System.Data.DataTable dtcount = new System.Data.DataTable();
                    dt.Columns.Remove("RowNumber");
                    DataColumn newCol = new DataColumn("RowNumber", typeof(int));
                    dt.Columns.Add(newCol);


                    int i = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        row["RowNumber"] = i;
                        i++;
                    }

                    GridView3.DataSource = dt;
                    GridView3.DataBind();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }

        protected void txtProjected_TextChanged(object sender, EventArgs e)
        {
            try
            {

                ulong count = 0;
                foreach (RepeaterItem items in GridView1.Items)
                {
                    System.Web.UI.WebControls.TextBox Projected = new System.Web.UI.WebControls.TextBox();

                    Projected = (System.Web.UI.WebControls.TextBox)items.FindControl("txtProjected");
                    if (!string.IsNullOrEmpty(Projected.Text))
                        count = count + Convert.ToUInt64(Projected.Text);

                }
                txtCount1.Text = Convert.ToString(count);
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }

        }
        protected void btnStepBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    string url = "~/SG50/Apply For Funding.aspx";
                    if (!(url.Contains("://")))
                        Response.Redirect(url);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    string url = "~/SG50/Apply For Funding.aspx";
                    if (!(url.Contains("://")))
                        Response.Redirect(url);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    if (Session["Step3"] != null)
                    {
                        if (Session["Organisation"] == "true")
                        {
                            string url = "~/SG50/Step3Organisation.aspx";
                            if (!(url.Contains("://")))
                                Response.Redirect(url);
                        }
                        else
                        {
                            string url = "~/SG50/Step3.aspx";
                            if (!(url.Contains("://")))
                                Response.Redirect(url);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "Exception Occurred");
            }
        }
    }
}