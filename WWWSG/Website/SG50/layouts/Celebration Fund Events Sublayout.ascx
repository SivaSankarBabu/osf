﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Celebration_fund_events_sublayout.Celebration_fund_events_sublayoutSublayout"
    CodeFile="~/SG50/layouts/Celebration Fund Events Sublayout.ascx.cs" %>
<link rel="stylesheet" type="text/css" href="/SG50/Parrallax/css/site_global.css?4221015755" />
<link rel="stylesheet" type="text/css" href="/SG50/Parrallax/css/index.css?239918620"
    id="pagesheet" />
<!-- Other scripts -->

<script type="text/javascript">

    document.documentElement.className += ' js';
</script>

<script src="/SG50/include/jqtransformplugin/jquery.jqtransform.js"></script>

<link href="/SG50/include/jqtransformplugin/jqtransform.css" rel="stylesheet" />

<script language="javascript" type="text/javascript">
    $(function() {
        $('.grid-dropdown').jqTransform({ imgPath: '/SG50/include/jqtransformplugin/img/' });
    });
</script>

<div class="press">
    <div class="pressBanner">
        <div id="pressBannerInner">
            <div class="pressBannerImage desktop">
                <sc:image id="Image1" runat="server" field="Banner Image" datasource="{62A20A5B-8FC8-40E2-AB05-84C94AE82C85}" />
            </div>
            <div class="pressBannerImage mobile">
                <sc:image id="Image2" runat="server" field="Banner Image For Responsive" datasource="{B7B44CFD-57E7-4603-9298-9B6EDD44138C}" />
            </div>
            <div class="se_invi" id="u776">
                <!-- simple frame -->
            </div>
            <div class="clearfix pre_init se_invi" id="u783-8">
                <!-- content -->
                <%--<sc:Text ID="Txttitle1" Field="Parallax Banner 1 Title" runat="server" />--%>
            </div>
            <div class="clearfix pre_init se_invi" id="u979-6">
                <!-- content -->
                <%--<sc:Text ID="TxtContent1" Field="Parallax Banner 1 Content" runat="server" />--%>
            </div>
            <div class="pressBannerCaption desktop">
                <h1>
                    BREATHE LIFE INTO YOUR CELEBRATORY PROJECT.</h1>
                <a href="/en/sg50/Celebration Fund.aspx">ABOUT THE FUND</a><%--<a href="/sg50/Apply For Funding.aspx">APPLY
                    FOR FUNDING</a>--%>
            </div>
            <div class="pressBannerCaption mobile">
                <h1>
                    BREATHE LIFE INTO YOUR CELEBRATORY PROJECT.</h1>
                <a class="banner-btn" href="/en/sg50/Celebration Fund.aspx">ABOUT THE FUND</a><%--<a
                    class="banner-btn" href="/sg50/Apply For Funding.aspx">APPLY FOR FUNDING</a>--%>
            </div>
        </div>
    </div>
</div>
<link href="/SG50/include/css/style.css" rel="stylesheet" />
<link href="/SG50/include/Css/responsive_Grid.css" rel="stylesheet" />

<script src="/SG50/include/js/isotope.pkgd.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var $container = $('.grid-container');
        // init
        $container.isotope({
            // options
            itemSelector: '.grid',
            //layoutMode: 'fitRows'
            getSortData: {
                color: '[data-color]'
            },
            sortBy: 'color'

        });

    });
</script>

<style>
    .pressBannerCaption a
    {
        border: 2px solid #fff;
        color: #fff;
        margin: 0 7px;
        padding: 10px 25px;
    }
    .pressBannerCaption a:hover
    {
        border: 2px solid #fff;
        background: rgba(255, 255, 255, 0.2);
    }
    .contentContainer
    {
        width: 1280px;
        min-height: 768px;
        background-color: #FFF;
        overflow: hidden;
        background-image: url('/SG50/images/icon3.png');
        background-repeat: no-repeat;
        background-position: right bottom;
    }
    .leftContentContainer
    {
        width: 1020px;
        float: left;
        min-height: 500px;
        background-image: url('/SG50/images/shadow.jpg');
        background-repeat: no-repeat;
        background-position: right top;
    }
    .rightContentContainer
    {
        width: 260px;
        float: left;
    }
    .rightSubContent
    {
        padding-left: 20px;
        padding-right: 20px;
    }
    .rightSubContent.eventCalendarDiv
    {
        padding-left: 10px;
        padding-right: 10px;
    }
    .leftContent
    {
        margin-left: 115px;
        margin-right: 30px;
        margin-bottom: 30px;
    }
    .breadcrumb
    {
        margin-top: 25px;
        font-size: 16px;
        color: #ef2c2e;
    }
    .breadcrumb img
    {
        width: 10px;
        height: 10px;
    }
    .breadcrumb a, .breadcrumb a:hover, .breadcrumb a:visited
    {
        color: #353333;
    }
    .pressTitle
    {
        margin-top: 50px;
        font-size: 32px;
        border-bottom: solid 1px #113706;
    }
    .rightTitle
    {
        font-size: 18px;
        color: #ef2c2e;
        margin-top: 25px;
        margin-bottom: 20px;
    }
    .rightImage
    {
        margin-top: 569px;
    }
    .PortfolioPagination
    {
        width: 100%;
        text-align: right;
        margin-top: 5px;
        vertical-align: top;
    }
    .prTitle
    {
        font-size: 20px;
        line-height: 40px;
    }
    .prDate
    {
        font-size: 16px;
        line-height: 25px;
        color: #353333;
    }
    .prBlurb
    {
        font-size: 16px;
        line-height: 20px;
        margin-top: 15px;
        margin-bottom: 25px;
    }
    .prItem
    {
        margin-top: 25px;
        padding-bottom: 15px; /*border-bottom:dotted 1px #353333;*/
        border-bottom: solid 1px #ccc;
    }
    .prItem a, .prItem a:hover, .prItem a:visited
    {
        color: black;
    }
    .prLatest
    {
        padding-top: 15px;
        color: #353333;
    }
    .prLatest a, .prLatest a:hover, .prLatest a:visited
    {
        color: #353333;
        font-size: 14px;
    }
    .divider
    {
        height: 15px;
        border-bottom: 1px solid #f0f0f0;
        margin-left: 0px;
        margin-right: 0px;
    }
</style>
<style>
    .calendarLeft
    {
        border-right: 1px solid #f0f0f0;
        border-top: 1px solid #f0f0f0;
        border-bottom: 1px solid #f0f0f0;
    }
    .calendarRight
    {
        border-left: 1px solid #f0f0f0;
        border-top: 1px solid #f0f0f0;
        border-bottom: 1px solid #f0f0f0;
    }
    .calendarCenter
    {
        border-left: 1px solid #f0f0f0;
        border-right: 1px solid #f0f0f0;
        border-top: 1px solid #f0f0f0;
        border-bottom: 1px solid #f0f0f0;
    }
    .tblEventCalendar
    {
        border-spacing: 0px;
        font-size: 12px;
        border: 1px solid #d9d9d9;
    }
    .tblEventCalendar tr td
    {
        vertical-align: middle;
    }
    #calendar
    {
        margin-top: 35px;
    }
    .eventTitle
    {
        margin-top: 30px;
        font-size: 30px;
        border-bottom: solid 1px #113706;
        color: #353333;
    }
    .eventTab
    {
        float: right;
        margin-top: 10px;
        font-size: 16px;
    }
    .eventTab div
    {
        color: #353333;
        padding: 5px 10px 3px 10px;
        display: inline;
        cursor: pointer;
        font-size: 14px;
    }
    .eventTab .active
    {
        color: #fff;
        background-color: #ef2c2e;
        font-weight: bold;
    }
    .ecTitle
    {
        font-size: 20px;
        color: #353333;
        line-height: 1.2em;
    }
    .ecDate
    {
        font-size: 14px;
        line-height: 25px;
        color: #353333;
    }
    .ecBlurb
    {
        font-size: 16px;
        line-height: 20px;
        margin-top: 15px;
        min-height: 128px;
        width: 635px;
        margin-left: 25px;
        float: left;
    }
    .ecImg
    {
        width: 205px;
        float: left;
        margin-top: 20px;
    }
    .ecItem
    {
        margin-top: 10px;
        padding-bottom: 15px; /*border-bottom:dotted 1px #353333;*/
        border-bottom: solid 1px #ccc;
    }
    .ecItemHide
    {
        display: none;
    }
    .ecItem a, .ecItem a:hover, .ecItem a:visited
    {
        color: black;
        font-size: 16px;
    }
    .ecBlurb, ecBlurb p
    {
        font-size: 16px;
        line-height: 25px;
        color: #565656;
    }
</style>
<style>
    .page_navigation
    {
        width: 100%;
        text-align: right;
        margin-top: 10px;
        vertical-align: top;
    }
    .page_navigation a
    {
        padding: 3px;
        margin: 2px;
        color: #353333;
        text-decoration: none;
        font-size: 14px;
    }
    .page_navigation a.previous_link, .page_navigation a.next_link
    {
        font-size: 12px;
    }
    .active_page
    {
        color: red !important;
    }
    @media screen and (min-width: 300px) and (max-width: 768px)
    {
        .contentContainer
        {
            width: 100%;
        }
        .leftContentContainer
        {
            width: 100%;
        }
        .rightContentContainer
        {
            display: none;
        }
        .leftContent
        {
            margin-top: 30px;
            margin-left: 10px;
            margin-right: 10px;
        }
        .eventTab
        {
            float: left;
            width: 100%;
            margin-bottom: 10px;
            padding-top: 10px;
            border-bottom: 1px dotted black;
            padding-bottom: 3px;
        }
        .ecBlurb
        {
            float: left;
            width: 98%;
            margin-left: 0;
            font-size: 16px;
        }
    }
    .video-player
    {
        height: 0;
        overflow: hidden;
        padding-bottom: 55%;
        padding-top: 30px;
        position: relative;
    }
    .video-player iframe, .video-player object, .video-player embed
    {
        height: 100%;
        left: 0;
        position: absolute;
        top: 0;
        width: 100%;
    }
</style>
<p>
    <a onclick='postToFeed();'></a>
</p>
<p id='msg'>
</p>

<script>
    var FBID = "<%=Sitecore.Context.Database.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}").Fields["Facebook AppID"].Value.ToString() %>";
    window.fbAsyncInit = function() {
    FB.init({
        appId: FBID, status: true, cookie: true,
         version    : 'v2.2'
    });
    };
    
    (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
       
       

    function postToFeed(Title, Description, Img, HostName) {
        // calling the API ...

        // alert('postToFeed');
        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            link: HostName,
            picture: fbimg,
            name: fbtitle,
            //caption: fbdes,
            description: fbdes
        };

        FB.ui(obj);

    }

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#eventUpcoming").show();
        $("#eventPast").hide();
    });

</script>

<section class="main-content">
    <!---main-content start-------->
    <section class="grid-content">
        <div class="dropdown-container">
            <div class="grid-search">
                <asp:TextBox ID="txtSearch" BorderColor="Black" Text="Search" runat="server"></asp:TextBox>
                <asp:ImageButton ID="imgSearchButton" runat="server" OnClick="imgSearchButton_Click" ImageUrl="~/SG50/images/search1.png" />
            </div>
            <div class="grid-dropdown">
                <asp:DropDownList ID="ddlFundedSearch" runat="server" CausesValidation="true" Height="35px" Width="175px" AutoPostBack="true" OnSelectedIndexChanged="ddlFundedSearch_SelectedIndexChanged">
                    <asp:ListItem Value="0">See All</asp:ListItem>
                    <asp:ListItem Value="1">Recent</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="clear"></div>
        <div class="head-title">
            <h1>Ground-Up Projects</h1>

            <p>
                <sc:FieldRenderer ID="txtcon" runat="server" FieldName="Heading" />
                <sc:FieldRenderer ID="Text2" runat="server" FieldName="Page Content" />
                <%--<a href="/SG50/Apply For Funding.aspx">start your own.</a>--%>
            </p>
        </div>
        <div class="grid-container">
            <asp:Repeater ID="repUpcomingEvents" runat="server">
                <ItemTemplate>

                    <div class="grid" data-color="yellow">
                        <div class="video-player" style="position: relative">
                            <a href=''>
                                <img style="position: absolute; top: 0; left: 0" src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" />
                                <iframe style="z-index: 3; position: absolute; top: 0; left: 0" src="<%#Eval("videoURL")%>" frameborder="0"></iframe>
                            </a>
                        </div>
                        <h2>
                            <sc:Text ID="Text1" Field="Title" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                        </h2>
                        <span>
                            <sc:Text ID="txtCreatedBy" Field="By Whom" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                        </span>
                        <p>
                            <sc:Text ID="Text4" Field="Short Description" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>" style="color: #ef2c2e;">Read
                                            More</a>
                        </p>
                        <ul>
                            <li><a href="JavaScript:postToFeed('<%# Eval("FacebookTitle").ToString()%>','<%# Eval("FacebookContent").ToString()%>','<%# Eval("FacebookImage")%>','<%# Eval("EventUrl")%>')">
                                <img src="/SG50/images/facebook.png" /></a></li>
                            <li><a href="JavaScript:newPopup('<%# Eval("TwitterContent").ToString()%>','<%# Eval("href")%>','<%# Eval("HostName")%>','<%# Eval("AccessToken")%>')">
                                <img src="/SG50/images/twitter.png" /></li>
                        </ul>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Label ID="lblSearchResult" ForeColor="Red" Font-Bold="true" runat="server" Visible="false"></asp:Label>
        </div>
    </section>
</section>

<script type="text/javascript">
    function newPopup(urlasp, url1, HostName, AccessToken) {


        var accessToken = AccessToken;  //'02ee2b5bc478c512e0b0a0fd639201521a088cef';
        var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
        var result;
        $.getJSON(
            url,
            {},
            function(response) {

                result = response.data.url;
                var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(urlasp) + ' ' + result;
                popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')

            }
        );
    }
</script>

<div class="rightContentContainer" style="display: none">
    <div class="rightContent">
        <div class="rightSubContent eventCalendarDiv">
            <div id="calendar">
                <!--  Dynamically Filled -->
            </div>
        </div>
    </div>
</div>
