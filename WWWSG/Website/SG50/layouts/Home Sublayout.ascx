﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"
    Inherits="Layouts.Home_sublayout.Home_sublayoutSublayout" CodeFile="~/SG50/layouts/Home Sublayout.ascx.cs"   %>

<script type="text/javascript">
    $(function () {
        $('.bxslider').bxSlider({
            mode: 'fade',
            auto: true,
            pause: 10000,
        });

    });

</script>

<script>

    function functionToExecute(name) {

        $('#' + name).addClass("video");
    }

    function linktype(name) {
        // $('#' + name).attr("target", "_blank");
    }
</script>

<sc:text id="Text2" runat='server' field='Additional Javascript' />
<div class="content_inner">
    <div id="wrapper">
        <div id="slidercontent" runat="server">
        </div>
        <div id="pageOuter" runat="server">
            <div id="pager" class="pager">
            </div>
        </div>
    </div>
    <div id="MobileSlider" runat="server">
        <%-- <ul class="bxslider">
            <li>
                <sc:Image runat="server" Field="Image 1" />
                <sc:Text Field="Slider Content 1 Responsive" runat="server" />
                <!--<div class="carouselButtonMobile1">
                <a href="#aboutTitle">WHAT IS SG50</a>
            </div> -->
                <div class="carouselButtonMobile2">
                    <a class="video" href="<%= getURL("1") %>" target="blank"><%= getButtonText("1") %></a>
                </div>
            </li>
            <li>
                <sc:Image runat="server" Field="Image 2" />
                <sc:Text Field="Slider Content 2 Responsive" runat="server" />
                <div class="carouselButtonMobile3">
                    <!--<a href="<%= getShareAnIdeaLink() %>">VIEW THE IDEAS</a>-->
                    <a href="<%= getURL("2") %>"><%= getButtonText("2") %></a>
                </div>
            </li>
            <li>
                <sc:Image runat="server" Field="Image 3" />
                <sc:Text Field="Slider Content 3 Responsive" runat="server" />
                <div class="carouselButtonMobile4">
                    <a class="video" href="<%= getURL("3") %>" target="blank"><%= getButtonText("3") %></a>
                </div>
            </li>

            <li>
                <sc:Image runat="server" Field="Image 4" />
                <sc:Text Field="Slider Content 4 Responsive" runat="server" />
                <div class="carouselButtonMobile3">
                    <!--<a href="<%= getShareAnIdeaLink() %>">VIEW THE IDEAS</a>-->
                    <a href="<%= getURL("4") %>"><%= getButtonText("4") %></a>
                </div>
            </li>

            <li>
                <sc:Image runat="server" Field="Image 5" />
                <sc:Text Field="Slider Content 5 Responsive" runat="server" />
                <div class="carouselButtonMobile3">
                    <!--<a href="<%= getShareAnIdeaLink() %>">VIEW THE IDEAS</a>-->
                    <a href="<%= getURL("5") %>"><%= getButtonText("5") %></a>
                </div>
            </li>
        </ul>--%>
    </div>
    <%--  <a class="video" href='<%#Eval("videoURL") %>'>
            <img src='<%#Eval("imgSrc") %>'>
        </a>--%>
    <%--<div id="imgdiv" runat="server">
        
    </div>--%>
    <style>
        .about
        {
        }
        .monthly-high
        {
            background: #ebebeb;
            display: table;
            margin-top: -100px;
        }
        .design-block
        {
            background: #ec1a3a;
            display: table;
            margin: 0 !important; /*   border-bottom: 20px solid #f1f1f1;*/
        }
        .design-block > .padding
        {
            padding: 0;
        }
        .monthly-high > .padding
        {
            display: table;
        }
        .monthly-high #aboutTitle
        {
            display: block;
            width: auto;
            margin: 20px 0;
        }
        .monthly-container, .design-container
        {
            width: 100%;
        }
        .design-container .aboutContent, .design-container .img-content
        {
            width: 50%;
            float: left;
            margin: 0;
        }
        .monthly-container .aboutContent
        {
            width: 30%;
            float: left;
            color: #545454;
            line-height: 29px;
            position:relative;
        }
        .monthly-container .video-player
        {
            width: 65%;
            float: left;
        }
        .monthly-container .video-player img, .design-container .img-content img
        {
            width: 100%;
            display: table;
        }
        .design-container .aboutContent1
        {
            padding: 50px;
        }
        .aboutContent1 p
        {
            color: #ffffff !important;
            font-size: 22px !important;
            line-height: 27px !important;
            margin-top: 20px;
        }
        .design-container .aboutContent1 a
        {
            border: 3px solid #fff;
            color: #fff;
            display: table;
            margin: 40px 0;
            padding: 15px 40px;
            font-weight: bold;
        }
        .design-container .aboutContent1 a:hover
        {
            background: #fff;
            color: #ec1a3a;
        }
        .design-container h1
        {
            color: #fff;
            font-size: 4.375em;
            font-weight: bold;
            line-height: 60px;
            word-break: break-word;
        }
        .monthly-container .aboutContent h2
        {
            color: #545454;
            font-size: 1.625em;
            font-weight: bold;
            margin: 0 0 20px 0;
        }
        .monthly-container .aboutContent a
        {
            background: #ec1a3a;
            color: #fff;
            display: table;
            font-size: 18px;
            font-weight: bold;
            margin: 133px 0 0;
            padding: 10px 30px;
            text-transform: uppercase;
            position:absolute;
            bottom:15px;
        }
        .monthly-container .aboutContent a:hover
        {
            background: #c21730;
        }
        .img-content ul
        {
            background: #fff;
            display: table;
            height: 602px;
        }
        .img-content ul li
        {
            width: 31.5%;
            float: left;
            padding: 1px 0;
            height: 205px;
            display: table;
        }
        .video-player
        {
            height: 0;
            overflow: hidden;
            padding-bottom: 34.25%;
            padding-top: 30px;
            position: relative;
            display:table;
        }
        .video-player iframe, .video-player object, .video-player embed
        {
            height: 100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%;
        }
        @media screen and (min-width:320px) and (max-width:768px)
        {
            .monthly-container .aboutContent
            {
                width: 100%;
                height:auto !important;
            }
           
            .monthly-container .video-player
            {
                width: 100%;
            }
            .monthly-high
            {
                margin-top: 0;
            }
            .monthly-container .aboutContent h2
            {
                margin: 15px 0;
            }
            
            .about
            {
                margin-top: 15px;
            }
            .design-container .aboutContent, .design-container .img-content
            {
                float: none;
                width: 100%;
                margin: 10px 0;
            }
            .design-container h1
            {
                font-size: 3.375em;
            }
            .design-container .aboutContent1
            {
                padding: 10px;
                word-break: break-all;
            }
            .img-content ul
            {
                height: auto;
            }
            .img-content ul li
            {
                height: auto;
            }
            .design-container .aboutContent1 a
            {
                border: 3px solid #fff;
                color: #fff;
                display: table;
                font-weight: bold;
                margin: 15px 0;
                padding: 7px 13px;
                line-height: 23px;
                font-size: 14px;
            }
            .monthly-container .aboutContent a
            {
            	font-size: 16px;
            	position:relative;
            	 margin: 30px 0 0px;
            }
            .design-block
            {
                border: none;
            }
             .video-player {
                padding-bottom:56.25%;
            }
        }
        .about .aboutContent p
        {
        	font-size: 22px !important;
            line-height: 27px !important;
            word-break: break-word;
        	
        	}
    </style>
    <script>
        $(document).ready(function() {
        var h = $('.video-player').outerHeight();
        $('.equaldiv').css('height', h);
 });
    
    
    </script>
    <div class="about monthly-high ">
        <asp:Repeater ID="repMonth" runat="server">
            <ItemTemplate>
                <div class="padding">
                    <div id="aboutTitle">
                        <h1>
                            <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Name")%>'></asp:Label></h1>
                    </div>
                    <div class="monthly-container">
                        <div class="video-player">
                            <a href=''>
                                <img src="<%# DataBinder.Eval(Container.DataItem, "imgSrc") %>" style="position:absolute; top:0" />
                                <iframe src="<%#Eval("href")%>" frameborder="0" ></iframe> 
                            </a>
                        </div>
                        <div class="aboutContent equaldiv">
                            <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description")%>'></asp:Label>
                            <%--<a href="/Celebration Fund and Ideas/FUNDED PROJECTS.aspx"><sc:Text ID="txtdowload" Field="Monthly Highlights Button Text" runat="server" /></a>--%>
                              <sc:Link Field="Button URL" runat="server" ID="link">
                                <sc:Text Field="Monthly Highlights Button Text" runat="server" ID="linkText" />
                            </sc:Link>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="about design-block">
        <div class="padding">
            <div class="design-container">
                <div class="aboutContent">
                    <div class="aboutContent1">
                        <h1>
                            <sc:text field="About Title" runat="server" />
                        </h1>
                        <p>
                            <sc:text id="aboutContent" field="About Content" runat="server" />
                        </p>
                        <%--<a href="#" id="dlLnkLogoAndGuide1" runat="server" onserverclick="DownloadForm"><sc:Text ID="btnText" Field="Button Text" runat="server" /></a>--%>
                        <a href="#" id="dlLnkLogoAndGuide" runat="server" onserverclick="DownloadForm">DOWNLOAD
                            THE LOGO AND GUIDE</a>
                    </div>
                </div>
                <div class="img-content" id="imgcontent" runat="server">
                    <%--<ul>
                        <li>
                            <img src="SG50/images/Images/batch-4/home-page-imag.png" /></li>
                        <li>
                            <img src="SG50/images/Images/batch-4/home-page-imag.png" /></li>
                        <li>
                            <img src="SG50/images/Images/batch-4/home-page-imag.png" /></li>
                        <li>
                            <img src="SG50/images/Images/batch-4/home-page-imag.png" /></li>
                        <li>
                            <img src="SG50/images/Images/batch-4/home-page-imag.png" /></li>
                        <li>
                            <img src="SG50/images/Images/batch-4/home-page-imag.png" /></li>

                    </ul>--%>
                </div>
            </div>
        </div>
    </div>
    <div class="about" style="display: none">
        <div class="padding">
            <div id="aboutTitle">
                <sc:text field="About Title" runat="server" />
            </div>
            <%--<div id="roundImage">
                <sc:Image ID="Image2" runat="server" Field="About Image" />
            </div>--%>
            <div class="aboutContent">
            </div>
            <div id="downloadLogoAndGuide">
            </div>
            <%--<div id="downloadLogoAndGuide2"><a href="<%= getDownloadLink()%>">SIMPLIFIED BRAND GUIDE</a> </div>--%>
        </div>
    </div>
    <div class="sg50LogoAndGuide">
        <div class="padding">
            <div id="sg50LogoAndGuideBanner">
                <%--<sc:Image ID="Image1" runat="server" Field="LogoGuide Landing Image" />--%>
            </div>
            <div class="sg50LogoAndGuideContainer">
                <sc:text id="Text1" field="Logo and Guide Content" runat="server" />
                <div id="icons">
                    <div id="eduAndYouth">
                        <sc:image id="Image4" runat="server" field="LogoGuide Image1" />
                        <p>
                            Education and<br />
                            Youth
                        </p>
                    </div>
                    <div id="cultureAndCommunity">
                        <sc:image id="Image5" runat="server" field="LogoGuide Image2" />
                        <p>
                            Culture and<br />
                            Community
                        </p>
                    </div>
                    <div id="ecoAndInter">
                        <sc:image id="Image6" runat="server" field="LogoGuide Image3" />
                        <p>
                            Economic and<br />
                            International
                        </p>
                    </div>
                    <div id="enviAndInfra">
                        <sc:image id="Image7" runat="server" field="LogoGuide Image4" />
                        <p>
                            Environment and<br />
                            Infrastructure
                        </p>
                    </div>
                    <div id="partnership">
                        <sc:image id="Image8" runat="server" field="LogoGuide Image5" />
                        <p>
                            Partnership</p>
                    </div>
                </div>
                <p>
                    &nbsp;</p>
                <p>
                    &nbsp;</p>
                <p>
                    &nbsp;</p>
            </div>
        </div>
    </div>
    <%--<div class="sg50Committee">
        <div class="padding">
            <div id="sg50CommitteeBanner">
                <sc:Image ID="Image9" runat="server" Field="Committee Landing Image" />
            </div>
            <div id="sg50CommitteeTitle">
                <h1>MEET THE COMMITTEE.</h1>
            </div>
            <div id="sg50SteeringCommittee">
                <div id="sg50SteeringCommitteeTitle">
                    <h1>SG50 STEERING COMMITTEE</h1>
                </div>
                <div id="sg50SteeringCommitteeMembers">
                    <div id="hsk">
                        <sc:Image ID="Image10" runat="server" Field="Committee1" />
                        <sc:Text ID="Text9" Field="LogoGuide Content1" runat="server" />
                    </div>
                    <div id="hy">
                        <sc:Image ID="Image11" runat="server" Field="Committee2" />
                        <sc:Text ID="Text8" Field="LogoGuide Content2" runat="server" />
                    </div>
                    <div id="ccs">
                        <sc:Image ID="Image12" runat="server" Field="Committee3" />
                        <sc:Text ID="Text7" Field="LogoGuide Content3" runat="server" />
                    </div>
                </div>
            </div>
            <div id="programmeOffice">
                <div id="programmeOfficeTitle">
                    <h1>PROGRAMME OFFICE</h1>
                </div>
                <div id="programmeOfficeMember">
                    <div id="lw">
                        <sc:Image ID="Image13" runat="server" Field="Committee4" />
                        <sc:Text ID="Text6" Field="LogoGuide Content4" runat="server" />
                    </div>
                </div>
            </div>
            <div id="sg50CommitteeIcons">
                <div id="sg50CommitteeEduAndYouth">
                    <div id="sg50CommitteeEduAndYouthTitle">
                        <sc:Image ID="Image24" runat="server" Field="LogoGuide Image1" />
                        <h1>Education and Youth</h1>
                    </div>
                    <div id="sg50CommitteeEduAndYouthMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image14" runat="server" Field="Committee5" />
                            </div>
                            <sc:Text ID="Text10" Field="Committee Content1" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text11" Field="Committee Content2" runat="server" />
                            <div>
                                <sc:Image ID="Image15" runat="server" Field="Committee6" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteeCultureAndCommunity">
                    <div id="sg50CommitteeCultureAndCommunityTitle">
                        <sc:Image ID="Image25" runat="server" Field="LogoGuide Image2" />
                        <h1>Culture and Community</h1>
                    </div>
                    <div id="sg50CommitteeCultureAndCommunityMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image16" runat="server" Field="Committee7" />
                            </div>
                            <sc:Text ID="Text12" Field="Committee Content3" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text13" Field="Committee Content4" runat="server" />
                            <div>
                                <sc:Image ID="Image17" runat="server" Field="Committee8" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteeEcoAndInter">
                    <div id="sg50CommitteeEcoAndInterTitle">
                        <sc:Image ID="Image26" runat="server" Field="LogoGuide Image3" />
                        <h1>Economic and International</h1>
                    </div>
                    <div id="sg50CommitteeEcoAndInterMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image18" runat="server" Field="Committee9" />
                            </div>
                            <sc:Text ID="Text14" Field="Committee Content5" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text15" Field="Committee Content6" runat="server" />
                            <div>
                                <sc:Image ID="Image19" runat="server" Field="Committee10" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteeEnviAndInfra">
                    <div id="sg50CommitteeEnviAndInfraTitle">
                        <sc:Image ID="Image27" runat="server" Field="LogoGuide Image4" />
                        <h1>Environment and Infrastucture</h1>
                    </div>
                    <div id="sg50CommitteeEnviAndInfraMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image20" runat="server" Field="Committee11" />
                            </div>
                            <sc:Text ID="Text16" Field="Committee Content7" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text17" Field="Committee Content8" runat="server" />
                            <div>
                                <sc:Image ID="Image21" runat="server" Field="Committee12" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sg50CommitteePartnership">
                    <div id="sg50CommitteePartnershipTitle">
                        <sc:Image ID="Image28" runat="server" Field="LogoGuide Image5" />
                        <h1>Partnership</h1>
                    </div>
                    <div id="sg50CommitteePartnershipMembers">
                        <div id="hsk">
                            <div>
                                <sc:Image ID="Image22" runat="server" Field="Committee13" />
                            </div>
                            <sc:Text ID="Text18" Field="Committee Content9" runat="server" />
                        </div>
                        <div id="hy">
                            <sc:Text ID="Text19" Field="Committee Content10" runat="server" />
                            <div>
                                <sc:Image ID="Image23" runat="server" Field="Committee14" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sg50CommitteeMembers">
                <div id="sg50CommitteeMembersTitle">
                    <h1>MEMBERS</h1>
                </div>
                <div id="sg50CommitteeMembersSeparator">
                    <sc:Text ID="Text2" Field="Committee Content" runat="server" />
                </div>
            </div>
        </div>
    </div>--%>
    <div class="sg50Committee">
        <div class="padding">
            <div id="sg50CommitteeBanner">
                <%--<sc:Image ID="Image9" runat="server" Field="Committee Landing Image" />--%>
            </div>
            <div id="sg50CommitteeTitle">
                <h1>
                    MEET THE COMMITTEE.</h1>
            </div>
            <div id="topMember">
                <div style="padding-bottom: 20px">
                    <sc:image id="Image9" runat="server" field="MeetTheCommittee" />
                </div>
                <%--<div>
                    <img src="/SG50/images/committee01.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee02.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee03.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee04.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee05.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee06.jpg" />
                </div>
                <div>
                    <img src="/SG50/images/committee07.jpg" />
                </div>--%>
            </div>
            <%--  <div id="bottomMember">
                <div>
                    <img src="/SG50/images/member.jpg" />
                </div>
            </div>--%>
            <div id="topMemberMobile">
                <div>
                    <img src="/SG50/images/mbcommittee01.png" />
                </div>
            </div>
            <div id="middleMemberMobile">
                <div>
                    <img src="/SG50/images/mbcommittee03.jpg" />
                </div>
            </div>
            <div id="bottomMemberMobile">
                <div>
                    <img src="/SG50/images/mbmembers.jpg" />
                </div>
            </div>
        </div>
    </div>
</div>
