﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.News_release_listing_sublayout.News_release_listing_sublayoutSublayout" CodeFile="~/SG50/layouts/News Release Listing Sublayout.ascx.cs" %>

<style>
    .contentContainer {
        width: 1280px;
        min-height: 768px;
		background-color: #FFF;
		overflow: hidden;
		background-image:url('/SG50/images/icon1.png');
		background-repeat:no-repeat;
		background-position:right bottom;
    }

    .leftContentContainer {
        width: 1020px;
        float: left;
		min-height: 500px;
		background-image:url('/SG50/images/shadow.jpg');
		background-repeat:no-repeat;
		background-position:right top;
    }

    .rightContentContainer {
        width: 260px;
        float: left;
    }

    .rightSubContent {
        padding-left: 20px;
        padding-right: 20px;
    }

    .leftContent {
        margin-left: 115px;
        margin-right: 30px;
		margin-bottom: 30px;
    }

    .breadcrumb {
        margin-top: 25px;
        font-size: 16px;
        color: #ef2c2e;
    }
	.breadcrumb img{
		width:10px;
		height:10px;
	}

        .breadcrumb a, .breadcrumb a:hover, .breadcrumb a:visited {
            color: #353333;
        }


    .pressTitle {
        margin-top: 30px;
        font-size: 30px;
        border-bottom: solid 1px #113706;
		color:#353333;
    }

    .rightTitle {
        font-size: 18px;
        color: #ef2c2e;
        margin-top: 25px;
        margin-bottom: 20px;
    }

    .rightImage {
        margin-top: 569px;
    }

    .PortfolioPagination {
        width: 100%;
        text-align: right;
        margin-top: 5px;
		vertical-align:top;
		color:#353333;
		font-size:16px;
    }

    .prTitle {
        font-size: 20px;
        /*line-height: 40px;*/
		line-height: 1.2em;
		color:#353333;
    }

    .prDate {
        font-size: 14px;
        line-height: 25px;
		color:#353333;
    }

    .prBlurb {        
        margin-top: 15px;
        margin-bottom: 25px;
    }
	.prBlurb, prBlurb p {
		font-size: 16px;
        line-height: 25px;
		color:#565656;
	}

    .prItem {
        margin-top:10px;
        padding-bottom:15px;
        /*border-bottom:dotted 1px #353333;*/
	border-bottom: solid 1px #ccc;
    }
    .prItem a, .prItem a:hover, .prItem a:visited {
        color: black;
    }

    .prLatest {
        padding-top: 15px;
        color: #353333;
    }

        .prLatest a, .prLatest a:hover, .prLatest a:visited {
            color: #353333;
			font-size:14px;
        }

    .divider {
        height: 15px;
        border-bottom: 1px solid #f0f0f0;
        margin-left: 0px;
        margin-right: 0px;
    }

    @media screen and (min-width: 300px) and (max-width: 768px) {
        .contentContainer {
            width: 100%;
        }

        .leftContentContainer {
            width: 100%;
        }

        .rightContentContainer {
            display: none;
        }

        .leftContent {
            margin-top: 30px;
            margin-left: 10px;
			margin-right: 10px;
        }
    }
</style>


<style>
.page_navigation{
	width: 100%;
    text-align: right;
    margin-top: 10px;
	vertical-align: top;
}

.page_navigation a{
	padding:3px;
	margin:2px;
	color:#353333;
	text-decoration:none;
	font-size:14px;
}
.page_navigation a.previous_link, .page_navigation a.next_link{
	font-size:12px;
}
.active_page{
	color:#ef2c2e !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function () {

        //how much items per page to show
        var show_per_page = 3;
        //getting the amount of elements inside content div
        var number_of_items = $('#pressItems').children().size();
        //calculate the number of pages we are going to have
        var number_of_pages = Math.ceil(number_of_items / show_per_page);

        //set the value of our hidden input fields
        $('#current_page').val(0);
        $('#show_per_page').val(show_per_page);
		
		//for Upcoming events
        var navigation_html = '<a class="previous_link" href="javascript:previous();">&lt;&lt;</a>';
        var current_link = 0;
        while (number_of_pages > current_link) {
            navigation_html += '<a class="page_link" href="javascript:go_to_page(' + current_link + ')" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
            current_link++;
        }
        navigation_html += '<a class="next_link" href="javascript:next();">&gt;&gt;</a>';
		$('#page_navigation').html(navigation_html);
		$('#page_navigation_bottom').html(navigation_html);		
		//add active_page class to the first page link
        $('#page_navigation .page_link:first').addClass('active_page');
		$('#page_navigation_bottom .page_link:first').addClass('active_page');
		//hide all the elements inside content div
        $('#pressItems').children().css('display', 'none');
		//and show the first n (show_per_page) elements
        $('#pressItems').children().slice(0, show_per_page).css('display', 'block');
    });

    function previous() {
        new_page = parseInt($('#current_page').val()) - 1;
        //if there is an item before the current active link run the function
        if ($('#page_navigation .active_page').prev('.page_link').length == true) {
            go_to_page(new_page);
        }
    }

    function next() {
        new_page = parseInt($('#current_page').val()) + 1;
        //if there is an item after the current active link run the function
        if ($('#page_navigation .active_page').next('.page_link').length == true) {
            go_to_page(new_page);
        }
    }
    function go_to_page(page_num) {
        //get the number of items shown per page
        var show_per_page = parseInt($('#show_per_page').val());

        //get the element number where to start the slice from
        start_from = page_num * show_per_page;

        //get the element number where to end the slice
        end_on = start_from + show_per_page;

        //hide all children elements of content div, get specific items and show them
        $('#pressItems').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');

        /*get the page link that has longdesc attribute of the current page and add active_page class to it
        and remove that class from previously active page link*/
        $('.page_link[longdesc=' + page_num + ']').addClass('active_page').siblings('.active_page').removeClass('active_page');

        //update the current page input field
        $('#current_page').val(page_num);
    }  
</script>


<div class="contentContainer">
    <div class="leftContentContainer">
        <div class="leftContent">
            <sc:Sublayout ID="Sublayout1" runat="server" Path="/SG50/layouts/Breadcrumb Sublayout.ascx"></sc:Sublayout>
            <div class="pressTitle">
                <sc:Text ID="Text1" Field="Content Title" runat="server" />
            </div>
            <%-- <div class="PortfolioPagination" runat="server" id="divPagination">
                <asp:ImageButton ID="ibtnPrevious" runat="server" ImageUrl="/SG50/images/arrowRight.png" />1
                <asp:Repeater ID="repPagination" runat="server">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtn" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LinkText") %>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:ImageButton ID="ibtnNext" runat="server" ImageUrl="/SG50/images/arrowLeft.png" />
            </div> --%>
			<input type="hidden" id="show_per_page" />
            <input type="hidden" id="current_page" />  
            <div id="page_navigation" class="page_navigation"></div>  
            <div id="pressItems">
                <asp:Repeater ID="repPressRelease" runat="server">
                    <ItemTemplate>
                        <div class="prItem <%# DataBinder.Eval(Container.DataItem, "cssClass") %>">
                            <div class="prTitle">
                                <sc:Text ID="Text2" Field="Content Title" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </div>
                            <div class="prDate">
                                <sc:Date ID="Date1" runat="server" Format='dd MMMM yyyy'
                                    Field="Release Date" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </div>
                            <div class="prBlurb">
                                <sc:Text ID="Text3" Field="Blurb" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </div>
                            <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>" style="color: #ef2c2e;">Read More</a>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
			<div id="page_navigation_bottom" class="page_navigation"></div> 
        </div>
    </div>
    <div class="rightContentContainer">
        <div class="rightContent">
            <div class="rightSubContent">
                <div class="rightTitle">Latest Releases</div>
                <asp:Repeater ID="repLatest" runat="server">
                    <ItemTemplate>
                        <div class="prLatest">
                            <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>">
                                <sc:Text ID="Text2" Field="Content Title" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' />
                            </a>
                        </div>
                        <div class="divider">&nbsp;</div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

</div>
