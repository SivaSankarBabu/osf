﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Idea_all_sublayout.Idea_all_sublayoutSublayout" CodeFile="~/SG50/layouts/Idea All Sublayout.ascx.cs" %>
<script src="/SG50/include/js/jquery.simplePagination.js" type="text/javascript"></script>
<script type="text/javascript">
    /*
    Cross-browser Modal Popup using Javascript (JQuery)
    */
    //Modal popup background ID. 
    //This value should be unique so that it does not conflict with other IDs in the page.
    var _ModalPopupBackgroundID = 'backgroundPopup_XYZ_20140302_Custom';

    function shModalPopup(modalPopupID) {

        //Setting modal popup window

        //Boolean to detect IE6.
        var isIE6 = (navigator.appVersion.toLowerCase().indexOf('msie 6') > 0);

        var popupID = "#" + modalPopupID;

        //Get popup window margin top and left
        var popupMarginTop = $(popupID).height() / 2.5;
        var popupMarginLeft = $(popupID).width() / 2;

        //Set popup window left and z-index
        //z-index of modal popup window should be higher than z-index of modal background
        $(popupID).css({
            'left': '50%',
            'z-index': 9999
        });

        //Special case for IE6 because it does not understand position: fixed
        if (isIE6) {
            $(popupID).css({
                'top': $(document).scrollTop(),
                'margin-top': $(window).height() / 2 - popupMarginTop,
                'margin-left': -popupMarginLeft,
                'display': 'block',
                'position': 'absolute'
            });
        }
        else {
            $(popupID).css({
                /*'top': '32%',
                'margin-top': -popupMarginTop,*/
                'top': '140px',
                'margin-left': -popupMarginLeft,
                'display': 'block',
                'position': 'fixed'
            });
        }

        //Automatically adding modal background to the page.
        var backgroundSelector = $('<div id="' + _ModalPopupBackgroundID + '" ></div>');

        //Add modal background to the body of the page.
        backgroundSelector.appendTo('body');

        //Set CSS for modal background. Set z-index of background lower than popup window.
        backgroundSelector.css({
            'width': $(document).width(),
            'height': $(document).height(),
            'display': 'none',
            'position': 'absolute',
            'top': 0,
            'left': 0
        });

        backgroundSelector.fadeTo('fast', 0.8);

        // Set to default
        for (var i = 1; i < 11; i++) {
            $('#' + "faq" + i + "ID" + ' p a img').attr("src", "/SG50/images/celebration-faq/icon1.png");
            $("#" + "faq" + i + "ID").css("color", "black");
            $("#" + "faq" + i + "ConID").hide();
        }
    }

    function csModalPopup(modalPopupID) {
        //Hide modal popup window
        $("#" + modalPopupID).css('display', 'none');

        //Remove modal background from DOM.
        $("#" + _ModalPopupBackgroundID).remove();
    }

    function HideContent(faqID, faqConID) {
        var a = $('#' + faqID + ' p a img').attr("src").replace(/\\/g, '/');
        var b = a.substring(a.lastIndexOf('/') + 1).split('.')[0];
        if (b != "undefined") {
            if (b == "icon1") {
                $('#' + faqID + ' p a img').attr("src", "/SG50/images/celebration-faq/icon2.png");
                $("#" + faqID).css("color", "white");
                $("#" + faqConID).show();
            }
            else {
                $('#' + faqID + ' p a img').attr("src", "/SG50/images/celebration-faq/icon1.png");
                $("#" + faqID).css("color", "black");
                $("#" + faqConID).hide();
            }
        }
    }

</script>

<sc:Text ID="Text2" runat='server' Field='Additional Javascript' />

<style >
    body
    {
        overflow: scroll;
        background-color: #000000;
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -o-user-select: none;
        -ms-user-select: none;
    }

    hr
    {
        border-width: 0;
        color: white;
        background-color: white;
        /*margin-left: 40px;
        margin-right: 40px;*/
        /*margin-left: 20px;
        margin-right: 20px;*/
        height: 1px;
    }

    .ball
    {
        margin-left: 5px;
        margin-right: 5px;
        font-family: helvetica;
    }

        .ball span
        {
            font-family: helvetica;
        }

        .ball a, .ball a:hover, .ball a:visited
        {
            text-decoration: none;
            color: white;
        }

    .footer
    {
        position: relative;
        z-index: 11;
    }

    .content
    {
        position: relative;
        z-index: 10;
    }

    #shareAnIdeaTitle, #submitButton, #rightSide, #noOfIdeas, #sortBy, .regionDropdown, .regionDropdown select, .regionDropdown select option, .btnSearch, .txtSearch
    {
        z-index: 11;
    }

    #rightSide
    {
        float: left;
    }

    .regionDropdown div
    {
        font-weight: bold;
        color: #666666;
        font-size: 26px;
        padding-top: 2px;
    }

    .regionDropdown .non-selected
    {
        color: #b0adad;
    }

    .regionDropdown .selected
    {
        color: #5a5858;
    }

    #submitButton a
    {
        background-color: white;
    }

    .noOfideasStyle
    {
        font-weight: bold;
        color: #666666;
        font-size: 26px;
        line-height: 30px;
    }

    .ball
    {
        word-break: break-all;
    }

    .btnSearch, .txtSearch
    {
        float: left;
    }

    .txtSearch
    {
        width: 200px;
        height: 25px;
        border: 1px solid #999;
        font-size: 16px;
        color: #999;
        font-size: 14px;
        padding-left: 5px;
    }

    .learnMore
    {
        clear: both;
        width: 200px;
        float: left;
        text-align: right;
        margin-top: 10px;
    }

    #rightSide .learnMore a
    {
        font-size: 14px;
    }

    .ideaSearch
    {
        margin-top: 20px;
    }

    #rightSide #noOfIdeas
    {
        /*top:120px;*/
        position: relative;
        top: 0;
        float: left;
        padding-top: 10px;
    }

    #rightSide #sortBy
    {
        /*top:160px;*/
        position: relative;
        top: 0;
        float: left;
    }

    #ddlRecent, #ddlRecentMob
    {
        cursor: default;
        position: relative;
    }

    #ddlSeeAll img
    {
        border: 0px;
        top: 40px;
        /*top: 12px;*/
        position: absolute;
        left: 117px;
    }

    #ddlSeeAllMob img
    {
        margin-bottom: 8px;
        margin-left: 5px;
    }

    #ddlRegion
    {
        display: none;
        cursor: default;
        top: 85px;
    }

    #ddlRegionMob
    {
        display: none;
        cursor: default;
    }

    #ddlRecent, #ddlRecentMob
    {
        display: none;
        cursor: default;
    }

        #ddlRecent:hover, #ddlRecent:hover
        {
            color: #999;
        }

        #ddlRegion:hover, #ddlRecent:hover
        {
            color: #999;
        }

    .mobileDiv
    {
        display: none;
    }

    .mobile div
    {
        margin-top: 0;
        padding-top: 25%;
    }

    #learnMorePopUp
    {
        padding: 50px;
        width: 450px;
    }

    .popupClose
    {
        background: #5c5959;
        padding: 10px;
        position: absolute;
        right: 0;
        top: 0;
    }

    #noOfIdeasMob p
    {
        font-size: 0.7em!important;
    }

    #sortByMob p
    {
        font-size: 0.7em!important;
    }

    #shareAnIdeaTitle
    {
        margin-top: 30px;
    }

    #submitButton
    {
        top: 130px;
    }

    .paginationPage {
        
        vertical-align: middle;
        height: 35px;
        font-size: 16px;
        font-weight:bold;
    }

    .paginationPage a {
        color:black;
    }

    .aspNetDisabled {
        color:red;
    }

    .paginationnew td{
        vertical-align:middle !important;
    }
</style>

<script type="text/javascript">

    function openDdl() {

        if ($('#ddlRegion').css('display') == 'none') {
            $('#ddlRecent').css('display', 'block');
            $('#ddlRegion').css('display', 'block');
        } else {
            $('#ddlRegion').css('display', 'none');
            $('#ddlRecent').css('display', 'none');
        }
    }

    function closeDdl() {
        window.location = "<%= getRecentLink() %>";
    }

    function closeDdl2() {
        window.location = "<%= getRegionLink() %>";
    }
    //--------------------------------------------------------
    function openDdlMob() {

        if ($('#ddlRegionMob').css('display') == 'none') {
            $('#ddlRecentMob').css('display', 'block');
            $('#ddlRegionMob').css('display', 'block');
        } else {
            $('#ddlRegionMob').css('display', 'none');
            $('#ddlRecentMob').css('display', 'none');
        }
    }

    function focusTxtbox() {
        $('#<%=txtSearch.ClientID%>').focus();
    }

    function focusTxtboxMob() {
        $('#<%=txtSearchMob.ClientID%>').focus();
    }

</script>

<style type="text/css">
    @media screen and (min-width: 300px) and (max-width: 768px)
    {
        #rightSide
        {
            display: none;
        }

        #shareAnIdeaTitle
        {
            width: 95% !important;
        }

        .txtSearch
        {
            width: 100% !important;
            padding-left:0!important;
            height:27px;
        }

        .learnMore
        {
            width: 100% !important;
        }
    }

    .ideadiv
    {
        width: 31.623931623931625%;
        float: left !important;
        font-weight: bold;
        margin-bottom: 20px;
    }

    .nameAge
    {
        font-size: 12px;
        color: #353333;
        margin-top: 4px;
    }

    #rightSide
    {
        float: right !important;
    }

    .ideaCanvas
    {
        margin-top: 20px;
        margin-left: 50px;
    }

    #paginationContainer
    {
        float: right;
        margin-right: 50px;
        margin-top: 10px;
    }

    #paginationContainer ul li
    {
        float: left;
        padding-left: 7px;
        font-size: 14px;
        color: #000000;
        font-weight: bold;
     }

    #paginationContainer ul li a
    {
        color: #000000;
    }

    #paginationContainer ul li.active
    {
        color: red;
    }

    #paginationContainer ul li:first-child
    {
        margin-top: 1px !important;
    }

    #paginationContainer ul li:last-child
    {
         margin-top: 1px !important;
    }

    /*Moblile Paginator */
    #paginationContainerMob
    {
        /*float: right;*/
        margin-right: 50px;
        margin-top: 10px;
    }

    #paginationContainerMob ul li
    {
        float: left;
        padding-left: 7px;
       
        font-size: 14px;
        color: #000000;
        font-weight: bold;
    }

    #paginationContainerMob ul li a
    {
        color: #000000;
    }
    
    #paginationContainerMob ul li.active
    {
        color: red;
    }
    
    #paginationContainerMob ul li:first-child
    {
        margin-top: 1px !important;
        padding-left: 0;
    }
    
    #paginationContainerMob ul li:last-child
    {
        margin-top: 1px !important;
    }

    .btnSearchMob
    {
        background-color: transparent;
        background-image: url('/sg50/images/search.png');
        background-repeat: no-repeat;
        position: relative;
        width: 20px;
        width: 29px;
        height: 29px;
        background-position: center;
        border: none;
        border-left: 0 !important;
    }

    #rightSideMobile
    {
        margin: 10px;
        margin-top: 50px;
    }

    .ball
    {
        margin: 0;
        padding: 0;
    }

    .txtSearchMob
    {
        border-right: 0 !important;
    }

</style>

<div class="ballContainer shareAnIdea">

    <div id="rightSide">
        <div class="ideaSearch">
            <div onclick="focusTxtbox()">
                <asp:TextBox ID="txtSearch" CssClass="txtSearch" MaxLength="200" runat="server" Text="" CuasesValidation="False" ></asp:TextBox>
            </div>
            <asp:Button CssClass="btnSearch" ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="" />

            <%--<div class="btnSearch" id="btnSearch" runat="server" onclick="btnSearch_Click">search</div>--%>
            <div class="errorMsg">
                <asp:RequiredFieldValidator ID="rfvSearch" runat="server" Display="Dynamic" ErrorMessage="Please enter your name to search" ControlToValidate="txtSearch" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexSearch" ControlToValidate="txtSearch" Display="Dynamic"
                    ErrorMessage="Invalid characters." runat="server" ValidationExpression="^(a-z|A-Z|0-9)*[^@#$%^&*()']*$" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RegularExpressionValidator>
            </div>
             <%--ValidationExpression="^[a-zA-Z0-9.]*$"--%>
            <div id="noResult" class="noResult" runat="server" visible="false" style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 3px;">
                No results found.
            </div>
        </div>
        <div class="learnMore">
            <a href="#" onclick="shModalPopup('learnMorePopUp'); return false;">Learn More</a>
        </div>
        <div id="learnMorePopUp">
            <a href="#" onclick="csModalPopup('learnMorePopUp'); return false;" class="popupClose">
                <img align="right" src="/SG50/images/celebration-faq/close.png" alt="" />
            </a>
            <div class="learnMorePopUpTitle">
                <sc:Text ID="Text1" Field="Learn More Title" DataSource="{2867D347-F425-4D0C-BC54-C9E33A78D31F}" runat="server" />
            </div>
            <div class="learnMorePopUpContent">
                <sc:Text ID="Text3" Field="Learn More Content" DataSource="{2867D347-F425-4D0C-BC54-C9E33A78D31F}" runat="server" />
            </div>
        </div>
        <div id="noOfIdeas">
            <p>no of ideas</p>
            <%--/sitecore/content/SG50/Settings/Idea Count--%>
            <h1><span class="noOfideasStyle">
                <sc:Text ID="Text4" Field="Value" DataSource="{A11A26C5-6BCE-4676-B856-8285E1A56A95}" runat="server" />
            </span></h1>
        </div>

        <div id="sortBy">
            <p style="margin-top: 15px;">
                sort by
            </p>

            <div class="regionDropdown">
                <div id="ddlSeeAll" onclick="openDdl();" class="selected">SEE ALL<img src="/sg50/images/down_arrow.png" /></div>
                <div id="ddlRecent" onclick="closeDdl();" class="non-selected" style="position: absolute;">RECENT</div>
                <div id="ddlRegion" onclick="closeDdl2();" class="non-selected" style="position: absolute;">REGION</div>
            </div>

        </div>
    </div>

    <div id="shareAnIdeaTitle">
        <h1>SOME BIG, SOME SMALL.</h1>
        <h1>SOME HEARTFELT AND OTHERS, PLAIN WACKY.</h1>
        <h3>Submission for celebration ideas is now over. While we pick some to bring to life, see how your fellow Singaporeans want to celebrate our nation’s 50th. </h3>
    </div>

    <div style="clear: both"></div>
    <div id="canvas" class="ideaCanvas" style="height:370px;"></div>
    <div style="clear: both;"></div>
    <div id="paginationContainer" class="pagination" ></div>

    <div style="float:right;display: none;">

                    <div>
                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label></div>
                    <div>
                        <table class="paginationnew">
                            <tr>
                                <td>
                                   <!-- <asp:Button ID="cmdFirst" runat="server" Text="<< First" OnClick="cmdFirst_Click">
                                    </asp:Button>&nbsp; -->
                                </td>
                                <td>
                                    <asp:LinkButton ID="cmdPrev" runat="server" Text="" OnClick="cmdPrev_Click" CausesValidation="False">
                                        <span style="font-size:30px;">&#9666;</span>
                                    </asp:LinkButton>&nbsp;
                                </td>
                                <td>
                                    <asp:Repeater ID="rptPages" runat="server" OnItemDataBound="rptPages_ItemDataBound">
                                        <HeaderTemplate>
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr class="paginationPage">
                                                    <td>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>" CssClass="paginationPage"
                                                runat="server" CausesValidation="False"><%# Container.DataItem %> 
                                            </asp:LinkButton>&nbsp;
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </td> </tr> </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                                <td>
                                    &nbsp;<asp:LinkButton ID="cmdNext" runat="server" Text="" OnClick="cmdNext_Click" CausesValidation="False">
                                        <span style="font-size:30px;">&#9656;</span>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                   <!-- &nbsp;<asp:Button ID="cmdLast" runat="server" Text="Last >>" OnClick="cmdLast_Click">
                                    </asp:Button> -->
                                </td>
                            </tr>
                        </table>
                    </div>
                    &nbsp;</div>

    <div style="display: none;">
        <asp:Repeater ID="repIdeas" runat="server">
            <ItemTemplate>
                <div class="cssClass"><%# DataBinder.Eval(Container.DataItem, "cssClass") %></div>
                <div class="href"><%# DataBinder.Eval(Container.DataItem, "href") %></div>
                <div class="ideaText"><%# DataBinder.Eval(Container.DataItem, "ideaText") %></div>
                <div class="Name"><%# DataBinder.Eval(Container.DataItem, "Name") %></div>
                <div class="Age"><%# DataBinder.Eval(Container.DataItem, "Age") %></div>
                <div class="Region"><%# DataBinder.Eval(Container.DataItem, "Region") %></div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div style="clear: both;"></div>

    <div style="display: none;">
        <asp:Repeater ID="repIdeasNew" runat="server">
            <ItemTemplate>
                <div class="cssClass"><%# DataBinder.Eval(Container.DataItem, "cssClass") %></div>
                <div class="href"><%# DataBinder.Eval(Container.DataItem, "href") %></div>
                <div class="ideaText"><%# DataBinder.Eval(Container.DataItem, "ideaText") %></div>
                <div class="Name"><%# DataBinder.Eval(Container.DataItem, "Name") %></div>
                <div class="Age"><%# DataBinder.Eval(Container.DataItem, "Age") %></div>
                <div class="Region"><%# DataBinder.Eval(Container.DataItem, "Region") %></div>
            </ItemTemplate>
        </asp:Repeater>

    </div>
    <div class="ball mobileDiv">
     <div style="display: none;">
        <asp:Repeater ID="repIdeasMobile" runat="server">
            <ItemTemplate>
                <div class="cssClassMob"><%# DataBinder.Eval(Container.DataItem, "cssClass") %></div>
                <div class="hrefMob"><%# DataBinder.Eval(Container.DataItem, "href") %></div>
                <div class="ideaTextMob"><%# DataBinder.Eval(Container.DataItem, "ideaText") %></div>
                <div class="NameMob"><%# DataBinder.Eval(Container.DataItem, "Name") %></div>
                <div class="AgeMob"><%# DataBinder.Eval(Container.DataItem, "Age") %></div>
                <div class="RegionMob"><%# DataBinder.Eval(Container.DataItem, "Region") %></div>
            </ItemTemplate>
        </asp:Repeater>
         
         <asp:Repeater ID="rptPagesMob" runat="server" >
                                        <HeaderTemplate>
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr class="paginationPage">
                                                    <td>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>" CssClass="paginationPage"
                                                runat="server" CausesValidation="False"><%# Container.DataItem %> 
                                            </asp:LinkButton>&nbsp;
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </td> </tr> </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
    </div>
        <%--  <asp:Repeater ID="repIdeasMobile" runat="server">
            <ItemTemplate>
                <div class="mobile">
                    <a href="<%# DataBinder.Eval(Container.DataItem, "href") %>">
                        <div>
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "ideaText") %></span>
                            <hr />
                            <span>
                                <%# DataBinder.Eval(Container.DataItem, "Name") %>, <%# DataBinder.Eval(Container.DataItem, "Age") %>
                            </br>
                                <%# DataBinder.Eval(Container.DataItem, "Region") %>
                            </span>
                        </div>
                    </a>
                </div>
            </ItemTemplate>
        </asp:Repeater>--%>

        <div id="rightSideMobile">
            <div id="canvasMob" class="ideaCanvasMob">
            </div>

            <div id="paginationContainerMob" class="pagination"></div>
            <div style="clear: both; margin-top: 10px;"></div>

            <div id="noOfIdeasMob" style="width: 49%; float: left; margin-right: 2%">
                <p style="border-bottom: 1px solid #000000;">no of ideas</p>
                <%--/sitecore/content/SG50/Settings/Idea Count--%>
                <h1><span class="noOfideasStyle">
                    <sc:Text ID="Text5" Field="Value" DataSource="{A11A26C5-6BCE-4676-B856-8285E1A56A95}" runat="server" />
                </span></h1>
            </div>

            <div id="sortByMob" style="width: 49%; float: left;">
                <p style="border-bottom: 1px solid #000000;">
                    sort by
                </p>
                <div class="regionDropdown">
                    <div id="ddlSeeAllMob" onclick="openDdlMob();" class="selected">SEE ALL<img src="/sg50/images/down_arrow.png" /></div>
                    <div id="ddlRecentMob" onclick="closeDdl();" class="non-selected" style="position: absolute;">RECENT</div>
                    <div id="ddlRegionMob" onclick="closeDdl2();" class="non-selected" style="position: absolute; margin-top: 30px;">REGION</div>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div class="ideaSearch">

                 <div style="min-width: 300px; max-width: 760px;">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="29">
                                <div onclick="focusTxtboxMob()">
                                    <asp:TextBox Width="100%" ID="txtSearchMob" MaxLength="200" CssClass="txtSearch txtSearchMob" runat="server" Text="" ></asp:TextBox>
                                </div>
                            </td>
                            <td width="29"  height="29" vallign="top" style="vertical-align:top;">
                                <div style="width:29px; height:29px; border:1px solid #999999; border-left:none;">
                                <asp:Button CssClass="btnSearchMob" ID="btnSearchMob" OnClick="btnSearchMob_Click" runat="server" Text="" ValidationGroup='mobile' />

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="errorMsg">
                    <asp:RequiredFieldValidator ValidationGroup='mobile' ID="rfvSearchMob" runat="server" Display="Dynamic" ErrorMessage="Please enter your name to search" ControlToValidate="txtSearchMob" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup='mobile' ID="regexSearchMob" ControlToValidate="txtSearchMob" Display="Dynamic"
                        ErrorMessage="Invalid characters." runat="server" ValidationExpression="^(a-z|A-Z|0-9)*[^@#$%^&*()']*$" Style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 2px;"></asp:RegularExpressionValidator>
                </div>

                <div id="noResultMob" class="noResult" runat="server" visible="false" style="width: 100%; float: left; color: #ef2c2e; padding-top: 2px; font-size: 12px; padding-bottom: 3px;">
                    No results found.
                </div>

            </div>
            <!-- // idSearch -->
            <div class="learnMore" style="width: 100%;">
                <a href="#" onclick="shModalPopup('learnMorePopUp'); return false;">Learn More</a>
            </div>
        </div>

    </div>
    <asp:HiddenField ID="hfTotalideas" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfCurrentPage" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hfCurrentPageMob" runat="server" ClientIDMode="Static" />

    <script src="/SG50/include/js/protoclass.js"></script>
    <script src='/SG50/include/js/box2d.js'></script>

    <script type="text/javascript">

        $(document).ready(function () {

            var canvas;
            var canvasMob;

            var delta = [0, 0];
            var stage = [window.screenX, window.screenY, window.innerWidth, window.innerHeight - 50];
            getBrowserDimensions();

            var themes = [["#fff", "#fff", "#f21313", "#d61414", "#f88989"]];
            var theme;

            var cssClassArray = new Array();
            var ideaTextArray = new Array();
            var NameArray = new Array();
            var AgeArray = new Array();
            var hrefArray = new Array();
            var RegionArray = new Array();

            //For mobile
            var cssClassArrayMob = new Array();
            var ideaTextArrayMob = new Array();
            var NameArrayMob = new Array();
            var AgeArrayMob = new Array();
            var hrefArrayMob = new Array();
            var RegionArrayMob = new Array();

            //------------------------------- No of pages -------------------
            var itemsPerPage = 21;
            var itemsPerPageMob = 10;

            var totalItemCount = $('.cssClass').length
            var remainder = (totalItemCount % itemsPerPage)
            var noOfPage = (totalItemCount - remainder) / itemsPerPage;
            if (remainder > 0)
                noOfPage++;

            var remainderMob = (totalItemCount % itemsPerPageMob)
            var noOfPageMob = (totalItemCount - remainderMob) / itemsPerPageMob;
            if (remainderMob > 0)
                noOfPageMob++;
            //-------------------------->>

            if (totalItemCount < itemsPerPage) {
                itemsPerPage = totalItemCount;
            }

            if (totalItemCount < itemsPerPageMob) {
                itemsPerPageMob = totalItemCount;
            }

            for (var i = 0; i < $('.cssClass').length; i++) {
                cssClassArray[i] = $('.cssClass')[i].innerHTML;
            }
            for (var i = 0; i < $('.ideaText').length; i++) {
                ideaTextArray[i] = $('.ideaText')[i].innerHTML;
            }
            for (var i = 0; i < $('.Name').length; i++) {
                NameArray[i] = $('.Name')[i].innerHTML;
            }
            for (var i = 0; i < $('.Age').length; i++) {
                AgeArray[i] = $('.Age')[i].innerHTML;
            }
            for (var i = 0; i < $('.href').length; i++) {
                hrefArray[i] = $('.href')[i].innerHTML;
            }
            for (var i = 0; i < $('.Region').length; i++) {
                RegionArray[i] = $('.Region')[i].innerHTML;
            }

            //For mobile
            for (var i = 0; i < $('.cssClassMob').length; i++) {
                cssClassArrayMob[i] = $('.cssClassMob')[i].innerHTML;
            }
            for (var i = 0; i < $('.ideaTextMob').length; i++) {
                ideaTextArrayMob[i] = $('.ideaTextMob')[i].innerHTML;
            }
            for (var i = 0; i < $('.NameMob').length; i++) {
                NameArrayMob[i] = $('.NameMob')[i].innerHTML;
            }
            for (var i = 0; i < $('.AgeMob').length; i++) {
                AgeArrayMob[i] = $('.AgeMob')[i].innerHTML;
            }
            for (var i = 0; i < $('.hrefMob').length; i++) {
                hrefArrayMob[i] = $('.hrefMob')[i].innerHTML;
            }
            for (var i = 0; i < $('.RegionMob').length; i++) {
                RegionArrayMob[i] = $('.RegionMob')[i].innerHTML;
            }

            //$('.cssClass').length

            for (var i = 0; i < itemsPerPage; i++) {

                var ideadiv = $('<div>');
                var spanIdea = $('<div style=\"padding-left:10px; float:left;\">');
                var spanNameAge = $('<div style=\"padding-left:23px;\">');
                var bullet = $("<img style=\"float:left;\">");

                $(bullet).attr("src", "/sg50/images/idea_bullet.png");
                $(bullet).attr("width", "13");
                $(bullet).attr("height", "13");

                $(spanIdea).html("<a href=\"" + hrefArray[i] + " \" style=\"text-decoration:none; color:#000000;\" >" + ideaTextArray[i] + "</a>");
                $(spanNameAge).html(NameArray[i] + ', ' + AgeArray[i]);
                $(ideadiv).addClass("ideadiv");
                $(spanNameAge).addClass("nameAge");

                $(ideadiv).append(bullet);
                $(ideadiv).append(spanIdea);
                $(ideadiv).append("<div style=\"clear:both;\"></div>");
                $(ideadiv).append(spanNameAge);

                $('#canvas').append(ideadiv);

            }

            $('#canvas').append('<div style=\"clear:both;\">');

            //--------------------------------------- Mobile -------------------------
            for (var i = 0; i < itemsPerPageMob; i++) {

                var ideadiv = $('<div>');
                var spanIdea = $('<div style=\"padding-left:10px; float:left;\">');
                var spanNameAge = $('<div style=\"padding-left:23px;\">');
                var bullet = $("<img style=\"float:left;\">");

                $(bullet).attr("src", "/sg50/images/idea_bullet.png");
                $(bullet).attr("width", "13");
                $(bullet).attr("height", "13");

                $(spanIdea).html("<a href=\"" + hrefArrayMob[i] + " \" style=\"text-decoration:none; color:#000000;\" >" + ideaTextArrayMob[i] + "</a>");
                $(spanNameAge).html(NameArrayMob[i] + ', ' + AgeArrayMob[i]);
                $(ideadiv).addClass("ideadiv");
                $(spanNameAge).addClass("nameAge");

                $(ideadiv).append(bullet);
                $(ideadiv).append(spanIdea);
                $(ideadiv).append("<div style=\"clear:both;\"></div>");
                $(ideadiv).append(spanNameAge);

                $('#canvasMob').append(ideadiv);
            }
            
            $('#canvasMob').append('<div style=\"clear:both;\">');

            // BROWSER DIMENSIONS
            function getBrowserDimensions() {

                var changed = false;

                if (stage[0] != window.screenX) {

                    delta[0] = (window.screenX - stage[0]) * 50;
                    stage[0] = window.screenX;
                    changed = true;
                }

                if (stage[1] != window.screenY) {

                    delta[1] = (window.screenY - stage[1]) * 50;
                    stage[1] = window.screenY;
                    changed = true;
                }

                if (window.innerWidth > 1281) {
                    if (stage[2] != 1280) {

                        stage[2] = 1280;
                        changed = true;
                    }
                } else {
                    if (stage[2] != window.innerWidth) {

                        stage[2] = window.innerWidth;
                        changed = true;
                    }
                }

                if (stage[3] != (window.innerHeight - 50)) {

                    stage[3] = window.innerHeight - 50;
                    changed = true;
                }

                return changed;
            }

            $(function () {
                $("#paginationContainer").pagination({
                    items: $('#hfTotalideas').val()/* totalItemCount */,
                    itemsOnPage: 21,
                    currentPage: $('#hfCurrentPage').val(),
                    cssStyle: 'light-theme',
                    prevText: '<span style="font-size:30px;">&#9666;</span>',
                    nextText: '<span style="font-size:30px;">&#9656;</span>',
                    onPageClick: function (pageNumber, event) {

                        
                        var _postbackitem = 'content_0$rptPages$ctl' + (pageNumber < 10 ? '0' + pageNumber : pageNumber) + '$btnPage';
                        __doPostBack(_postbackitem, '');

                        /* $('#canvas').html("");

                        var starting_Item = (itemsPerPage * (pageNumber - 1)) + 1;
                        var lastItemPage = starting_Item + itemsPerPage;
                        if (noOfPage == pageNumber) {
                            lastItemPage = starting_Item + remainder;
                        }

                        for (var i = starting_Item; i < lastItemPage; i++) {

                            var ideadiv = $('<div>');
                            var spanIdea = $('<div style=\"padding-left:10px; float:left;\">');
                            var spanNameAge = $('<div style=\"padding-left:23px;\">');
                            var bullet = $("<img style=\"float:left;\">");

                            $(bullet).attr("src", "/sg50/images/idea_bullet.png");
                            $(bullet).attr("width", "13");
                            $(bullet).attr("height", "13");

                            $(spanIdea).html("<a href=\"" + hrefArray[i] + " \" style=\"text-decoration:none; color:#000000;\" >" + ideaTextArray[i] + "</a>");
                            $(spanNameAge).html(NameArray[i] + ', ' + AgeArray[i]);
                            $(ideadiv).addClass("ideadiv");
                            $(spanNameAge).addClass("nameAge");

                            $(ideadiv).append(bullet);
                            $(ideadiv).append(spanIdea);
                            $(ideadiv).append("<div style=\"clear:both;\"></div>");
                            $(ideadiv).append(spanNameAge);

                            $('#canvas').append(ideadiv);
                        }

                        $('#canvas').append('<div style=\"clear:both;\">'); */
                    } 
                });
            });  //-------

            $(function () {                 //----------------------------------- Paginator Mob
                $("#paginationContainerMob").pagination({
                    items: $('#hfTotalideas').val()/* totalItemCount */,
                    itemsOnPage: itemsPerPageMob,
                    currentPage: $('#hfCurrentPageMob').val(),
                    cssStyle: 'light-theme',
                    prevText: '<span style="font-size:30px;">&#9666;</span>',
                    nextText: '<span style="font-size:30px;">&#9656;</span>',
                    onPageClick: function (pageNumber, event) {

                        var _postbackitem = 'content_0$rptPagesMob$ctl' + (pageNumber < 10 ? '0' + pageNumber : pageNumber) + '$btnPage';
                        __doPostBack(_postbackitem, '');

                        /*$('#canvasMob').html("");

                        var starting_Item = (itemsPerPageMob * (pageNumber - 1)) + 1;
                        var lastItemPage = starting_Item + itemsPerPageMob;
                        if (noOfPageMob == pageNumber) {
                            lastItemPage = starting_Item + remainderMob;
                        }

                        for (var i = starting_Item; i < lastItemPage; i++) {

                            var ideadiv = $('<div>');
                            var spanIdea = $('<div style=\"padding-left:10px; float:left;\">');
                            var spanNameAge = $('<div style=\"padding-left:23px;\">');
                            var bullet = $("<img style=\"float:left;\">");

                            $(bullet).attr("src", "/sg50/images/idea_bullet.png");
                            $(bullet).attr("width", "13");
                            $(bullet).attr("height", "13");

                            $(spanIdea).html("<a href=\"" + hrefArray[i] + " \" style=\"text-decoration:none; color:#000000;\" >" + ideaTextArray[i] + "</a>");
                            $(spanNameAge).html(NameArray[i] + ', ' + AgeArray[i]);
                            $(ideadiv).addClass("ideadiv");
                            $(spanNameAge).addClass("nameAge");

                            $(ideadiv).append(bullet);
                            $(ideadiv).append(spanIdea);
                            $(ideadiv).append("<div style=\"clear:both;\"></div>");
                            $(ideadiv).append(spanNameAge);

                            $('#canvasMob').append(ideadiv);
                        }

                        $('#canvasMob').append('<div style=\"clear:both;\">');*/
                    }
                });
            });

        });

    </script>

</div>

