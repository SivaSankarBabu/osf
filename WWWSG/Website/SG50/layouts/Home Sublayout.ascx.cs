﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using Sitecore.Data.Fields;

namespace Layouts.Home_sublayout
{

    /// <summary>
    /// Summary description for Home_sublayoutSublayout
    /// </summary>
    public partial class Home_sublayoutSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmContext = Sitecore.Context.Item;
       
        static Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        string hostName = itemconfiguration["Host Name"];
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            // Get slider URL
            // getSliderUrl();
            getbannerimages();
            getMobilebannerimages();
            MonthlyHiglights();
            getCustomisedLogos();
            //getCustomisedLogos();
        }

        private void getbannerimages()
        {
            try
            {
                Item HomeBannerItem = web.GetItem(SG50Class.str_Home_Banner_Item_ID);

                if (HomeBannerItem != null)
                {
                    ChildList homePageBanners;
                    StringBuilder sbBannerText = new StringBuilder();
                    sbBannerText.Append("<div id='slider'>");
                    StringBuilder pagerText = new StringBuilder();
                    homePageBanners = HomeBannerItem.GetChildren();

                    if (homePageBanners.Count > 0)
                    {
                        int BannerCount = 0;
                        for (int i = 0; i < homePageBanners.Count; i++)
                        {
                            if (homePageBanners[i].Versions.Count > 0 && homePageBanners[i]["Show Banner"] == "1" && BannerCount < 5)
                            {
                                BannerCount++;
                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)homePageBanners[i].Fields["Image"]);
                                string mediaUrl5 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                sbBannerText.Append("<div class='slide' style=\"background-image: url('" + mediaUrl5 + "')\">");
                                sbBannerText.Append("<div class='slide-block'>");
                                string text = homePageBanners[i]["Slider Content"];
                                sbBannerText.Append(text);
                                sbBannerText.Append("<div  class='carouselButton'>");


                                string url = homePageBanners[i]["Button URL"];

                                //Sitecore.Data.Fields.CheckboxField LinkType = homePageBanners[i].Fields["Button Link Type"];
                                //Sitecore.Data.Fields.CheckboxField Video = homePageBanners[i].Fields["Banner Video"];
                                string Banner_Display = homePageBanners[i]["Button Link Type"];


                                if (Banner_Display == "Video")
                                {
                                    sbBannerText.Append("<a href=" + url + " class='video'>" + homePageBanners[i]["ButtonText"] + "</a>");
                                }
                                else if (Banner_Display == "External Link")
                                {
                                    sbBannerText.Append("<a href=" + url + " target='_blank'>" + homePageBanners[i]["ButtonText"] + "</a>");
                                }
                                else
                                {
                                    sbBannerText.Append("<a href=" + url + ">" + homePageBanners[i]["ButtonText"] + "</a>");
                                }


                                //if (LinkType.Checked == false && Video.Checked == false)
                                //{
                                //    sbBannerText.Append("<a href=" + url + ">" + homePageBanners[i]["ButtonText"] + "</a>");
                                //}

                                sbBannerText.Append("</div>");
                                sbBannerText.Append("</div>");
                                sbBannerText.Append("</div>");

                            }
                        }
                        sbBannerText.Append("</div>");
                        slidercontent.InnerHtml = sbBannerText.ToString();

                        pagerText.Append("<div id='pager' class='pager'>");
                        int pagercount = 0;
                        for (int i = 0; i < homePageBanners.Count; i++)
                        {
                            if (homePageBanners[i].Versions.Count > 0 && homePageBanners[i]["Show Banner"] == "1" && pagercount < 5)
                            {
                                pagercount++;

                                pagerText.Append("<a href='#'><span></span></a>");
                            }
                        }
                        pagerText.Append("</div>");
                        pageOuter.InnerHtml = pagerText.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private void getCustomisedLogos()
        {
            try
            {
                Item CustomisedItem = web.GetItem(SG50Class.str_Home_CustomisedLogos_Item_ID);
                if (CustomisedItem != null)
                {
                    ChildList CustomisedLogos;
                    StringBuilder sbBannerText = new StringBuilder();
                    sbBannerText.Append("<ul>");
                    //StringBuilder pagerText = new StringBuilder();
                    CustomisedLogos = CustomisedItem.GetChildren();
                    if (CustomisedLogos.Count > 0)
                    {
                        int LogoCount = 0;
                        for (int i = 0; i < CustomisedLogos.Count; i++)
                        {
                            if (CustomisedLogos[i].Versions.Count > 0 && CustomisedLogos[i]["Hide"] == "1" && LogoCount < 9)
                            {
                                LogoCount++;
                                sbBannerText.Append("<li>");
                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)CustomisedLogos[i].Fields["Image"]);
                                string mediaUrl5 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                sbBannerText.Append("<img src='" + mediaUrl5 + "')\"/>");
                                sbBannerText.Append("</li>");
                            }
                        }
                        sbBannerText.Append("</ul>");
                        imgcontent.InnerHtml = sbBannerText.ToString();                      
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private void MonthlyHiglights()
        {

            DataTable dtMonthlyHiglights = new DataTable();
            // dtrepEvents.Columns.Add("item", typeof(Item));
            dtMonthlyHiglights.Columns.Add("Name", typeof(string));
            dtMonthlyHiglights.Columns.Add("imgSrc", typeof(string));
            dtMonthlyHiglights.Columns.Add("Description", typeof(string));
            dtMonthlyHiglights.Columns.Add("href", typeof(string));
            DataRow drMonthlyHiglights;
            Item itmHighlights = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID);
            drMonthlyHiglights = dtMonthlyHiglights.NewRow();
            drMonthlyHiglights["Name"] = itmHighlights.Fields["Highlight Title"].ToString();
            Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)itmHighlights.Fields["Image"]);
            Sitecore.Data.Fields.ImageField VideoimgField = ((Sitecore.Data.Fields.ImageField)itmHighlights.Fields["Video Image"]);
            drMonthlyHiglights["href"] = itmHighlights.Fields["Video URL"].ToString();
            //Response.Write(drMonthlyHiglights["href"].ToString());
            //drMonthlyHiglights["href"] = lnk.Url;
            string imgSrc = imgField.Src;
            string VideoImgsrc = VideoimgField.Src;
            if (!string.IsNullOrEmpty(imgSrc))
            {
                drMonthlyHiglights["imgSrc"] = hostName + "/" + imgSrc;
            }
            else if (!string.IsNullOrEmpty(VideoImgsrc))
            {
                drMonthlyHiglights["imgSrc"] = hostName + "/" + VideoImgsrc;
            }
            drMonthlyHiglights["Description"] = itmHighlights.Fields["Description"].ToString();
            dtMonthlyHiglights.Rows.Add(drMonthlyHiglights);

            // IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants()
            //                                           where
            //                                          a.TemplateID.ToString().Equals(SG50Class.str_Home_Template_ID)
            //                                           select a;



            //foreach (Item a in itmIEnumPressReleases)
            //{
            //    Response.Write("11");
            //    drrepEvents = dtrepEvents.NewRow();
            //    drrepEvents["item"] = a;
            //    drrepEvents["Name"] = a.Fields["Highlight Title"].ToString();

            //    Sitecore.Data.Fields.LinkField lnk = a.Fields["Video URL"];
            //    Sitecore.Data.Fields.ImageField fbimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
            //    Sitecore.Data.Fields.ImageField fbVideoimgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Video Image"]);
            //    string fbimgSrc = fbimgField.Src;
            //    string fbVideoImgsrc = fbVideoimgField.Src;
            //    if (!string.IsNullOrEmpty(fbimgSrc))
            //    {
            //        drrepEvents["imgSrc"] = hostName + "/" + fbimgSrc;
            //    }
            //    else if (!string.IsNullOrEmpty(fbVideoImgsrc))
            //    {
            //        drrepEvents["imgSrc"] = hostName + "/" + fbVideoImgsrc;
            //    }
            //    dtrepEvents.Rows.Add(drrepEvents);

            //}



            repMonth.DataSource = dtMonthlyHiglights;
            repMonth.DataBind();

        }

        private void getMobilebannerimages()
        {
            try
            {
                Item HomeBannerItem = web.GetItem(SG50Class.str_Home_Banner_Item_ID);

                if (HomeBannerItem != null)
                {
                    ChildList homePageBanners;
                    StringBuilder sbBannerText = new StringBuilder();
                    sbBannerText.Append("<ul class='bxslider'>");

                    //StringBuilder pagerText = new StringBuilder();
                    homePageBanners = HomeBannerItem.GetChildren();
                    if (homePageBanners.Count > 0)
                    {
                        int MobileBannerCount = 0;
                        for (int i = 0; i < homePageBanners.Count; i++)
                        {
                            if (homePageBanners[i].Versions.Count > 0 && homePageBanners[i]["Show Banner"] == "1" && MobileBannerCount < 5)
                            {
                                MobileBannerCount++;
                                sbBannerText.Append("<li>");
                                Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)homePageBanners[i].Fields["Image"]);
                                string mediaUrl5 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                sbBannerText.Append("<img src='" + mediaUrl5 + "')\"/>");
                                //sbBannerText.Append("<div class='slide-block'>");
                                string text = homePageBanners[i]["Slider Content"];
                                sbBannerText.Append(text);
                                sbBannerText.Append("<div  class='carouselButtonMobile1'>");

                                string url = homePageBanners[i]["Button URL"];
                                //Sitecore.Data.Fields.CheckboxField LinkType = homePageBanners[i].Fields["Button Link Type"];
                                //Sitecore.Data.Fields.CheckboxField Video = homePageBanners[i].Fields["Banner Video"];
                                //if (LinkType.Checked == false && Video.Checked == true)
                                //{
                                //    sbBannerText.Append("<a href=" + url + " class='video'>" + homePageBanners[i]["ButtonText"] + "</a>");
                                //}
                                //if (LinkType.Checked == true && Video.Checked == false)
                                //{
                                //    sbBannerText.Append("<a href=" + url + " target='_blank'>" + homePageBanners[i]["ButtonText"] + "</a>");
                                //}

                                //if (LinkType.Checked == false && Video.Checked == false)
                                //{
                                //    sbBannerText.Append("<a href=" + url + ">" + homePageBanners[i]["ButtonText"] + "</a>");
                                //}

                                sbBannerText.Append("</div>");

                                sbBannerText.Append("</li>");


                            }
                        }
                        sbBannerText.Append("</ul>");
                        MobileSlider.InnerHtml = sbBannerText.ToString();

                        //pagerText.Append("<div id='pager' class='pager'>");
                        //for (int i = 0; i < homePageBanners.Count; i++)
                        //{
                        //    if (homePageBanners[i].Versions.Count > 0 && homePageBanners[i]["Show Banner"] == "1")
                        //    {
                        //        pagerText.Append("<a href='#'><span></span></a>");
                        //    }
                        //}
                        //pagerText.Append("</div>");
                        //pageOuter.InnerHtml = pagerText.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected string getShareAnIdeaLink()
        {
            string link = "";
            if (SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID) != null)
            {
                //link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_Recent_Item_ID));
                link = LinkManager.GetItemUrl(SG50Class.web.GetItem(SG50Class.str_Idea_All_Item_ID));
            }
            return link;
        }

        protected string getIconLink()
        {
            string link = "";
            if (ICONClass.web.GetItem(ICONClass.str_Home_Page_Item_ID) != null)
            {
                link = LinkManager.GetItemUrl(ICONClass.web.GetItem(ICONClass.str_Home_Page_Item_ID));
            }
            return link;
        }

        //public void DownloadForm(object sender, EventArgs e)
        //{

        //    string str = "";
        //    str = SG50Class.str_LogoGuide_Form_Item_ID;
        //    if (str.Length > 0)
        //    {

        //        MediaItem mi = web.GetItem(str);
        //        Response.Write(mi.Name);
        //        if (mi != null)
        //        {
        //            Stream stream = mi.GetMediaStream();
        //            long fileSize = stream.Length;
        //            byte[] buffer = new byte[(int)fileSize];
        //            stream.Read(buffer, 0, (int)stream.Length);
        //            stream.Close();

        //            Response.ContentType = String.Format(mi.MimeType);
        //            string fileName = (mi.Name + "." + mi.Extension).ToString().Replace(' ', '_');
        //            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
        //            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //            Response.BinaryWrite(buffer);
        //            Response.End();
        //        }
        //        else { Response.Write("no mi"); }
        //    }
        //}
        public void DownloadForm(object sender, EventArgs e)
        {
            try
            {
                Item item = Sitecore.Context.Item;
                FileField file = item.Fields["Logo and Guide"];
                if (file != null)
                {
                    MediaItem mi = file.MediaItem;//web.GetItem(file.ID);
                   // if (str.Length > 0)
                    //{
                       // MediaItem mi = web.GetItem(str);
                      //  Response.Write(mi.Name.ToString());
                        if (mi != null)
                        {
                            Stream stream = mi.GetMediaStream();
                            long fileSize = stream.Length;
                            byte[] buffer = new byte[(int)fileSize];
                            stream.Read(buffer, 0, (int)stream.Length);
                            stream.Close();

                            Response.ContentType = String.Format(mi.MimeType);
                            string fileName = (mi.Name + "." + mi.Extension).ToString().Replace(' ', '_');
                           Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
                           // Response.AppendHeader("Content-Disposition", "attachment; filename=" + mi.Name + "." + mi.Extension);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.BinaryWrite(buffer);
                            Response.End();
                        }
                        else { Response.Write("no mi"); }
                   // }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected string getURL(string URLFor)
        {
            string url = "";
            switch (URLFor)
            {
                case "1":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button1URL"] != null)
                        {
                            url = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button1URL"];
                        }
                        break;
                    }
                case "2":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button2URL"] != null)
                        {
                            url = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button2URL"];
                        }
                        break;
                    }
                case "3":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button3URL"] != null)
                        {
                            url = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button3URL"];
                        }
                        break;
                    }
                case "4":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button4URL"] != null)
                        {
                            url = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button4URL"];
                        }
                        break;
                    }

                case "5":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button5URL"] != null)
                        {
                            url = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button5URL"];
                        }
                        break;
                    }
            }

            return url;
        }

        protected string getButtonText(string TextFor)
        {
            string _text = "";
            switch (TextFor)
            {
                case "1":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button1"] != null)
                        {
                            _text = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button1"];
                        }
                        break;
                    }
                case "2":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button2"] != null)
                        {
                            _text = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button2"];
                        }
                        break;
                    }
                case "3":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button3"] != null)
                        {
                            _text = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button3"];
                        }
                        break;
                    }
                case "4":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button4"] != null)
                        {
                            _text = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button4"];
                        }
                        break;
                    }

                case "5":
                    {
                        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button5"] != null)
                        {
                            _text = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Button5"];
                        }
                        break;
                    }
            }

            return _text;
        }

        protected string getURL()
        {
            string url = "";
            if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["YouTube URL"] != null)
            {
                url = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["YouTube URL"];
            }
            return url;
        }

        protected string getURL2()
        {
            string url2 = "";
            if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["YouTube URL2"] != null)
            {
                url2 = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["YouTube URL2"];
            }
            return url2;
        }

        protected string getURL3()
        {
            string url3 = "";
            if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["YouTube URL3"] != null)
            {
                url3 = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["YouTube URL3"];
            }
            return url3;
        }



        //private void getSliderUrl()
        //{
        //    for (int i = 1; i < 6; i++)
        //    {
        //        if (SG50Class.web.GetItem(SG50Class.str_Home_Item_ID)["Image " + i] != null)
        //        {
        //            Item item = SG50Class.web.GetItem(SG50Class.str_Home_Item_ID);
        //            try
        //            {
        //                //Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)item.Fields["Image " + i]);
        //                //string mediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
        //                //if (1 == i)
        //                //    slider1Image.Style["background-image"] = "url(" + mediaUrl + ")";
        //                //else if (2 == i)
        //                //    slider2Image.Style["background-image"] = "url(" + mediaUrl + ")";
        //                //else if (3 == i)
        //                //    slider3Image.Style["background-image"] = "url(" + mediaUrl + ")";
        //                //else if (4 == i)
        //                //    slider4Image.Style["background-image"] = "url(" + mediaUrl + ")";
        //                //else
        //                //    slider5Image.Style["background-image"] = "url(" + mediaUrl + ")";

        //                switch (i)
        //                {
        //                    case 1:
        //                        {

        //                            Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)item.Fields["Image 1"]);
        //                            if (!string.IsNullOrEmpty(imgField.Src) && !string.IsNullOrEmpty(item["Slider Content1"]) && item["Show Banner1"] == "1")
        //                            {
        //                                string mediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
        //                                slider1Image.Style["background-image"] = "url(" + mediaUrl + ")";
        //                                slider1Image.Visible = true;
        //                                a1.Visible = true;
        //                                if (item["Banner1 Video"] == "1")
        //                                {

        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "myscript", "functionToExecute('linktypeone');", true);

        //                                }
        //                                if (item["Banner1 Link Type"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "linkscript", "linktype('linktypeone');", true);
        //                                }
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                continue;
        //                            }
        //                        }

        //                    case 2:
        //                        {
        //                            Sitecore.Data.Fields.ImageField imgField2 = ((Sitecore.Data.Fields.ImageField)item.Fields["Image 2"]);
        //                            if (!string.IsNullOrEmpty(imgField2.Src) && !string.IsNullOrEmpty(item["Slider Content2"]) && item["Show Banner2"] == "1")
        //                            {
        //                                string mediaUrl2 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField2.MediaItem);
        //                                slider2Image.Style["background-image"] = "url(" + mediaUrl2 + ")";
        //                                slider2Image.Visible = true;
        //                                a2.Visible = true;
        //                                if (item["Banner2 Video"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "myscript1", "functionToExecute('linktypetwo');", true);
        //                                }

        //                                if (item["Banner2 Link Type"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "linkscript2", "linktype('linktypetwo');", true);
        //                                }

        //                                break;
        //                            }
        //                            else
        //                            {
        //                                continue;
        //                            }
        //                        }

        //                    case 3:
        //                        {
        //                            Sitecore.Data.Fields.ImageField imgField3 = ((Sitecore.Data.Fields.ImageField)item.Fields["Image 3"]);
        //                            if (!string.IsNullOrEmpty(imgField3.Src) && !string.IsNullOrEmpty(item["Slider Content3"]) && item["Show Banner3"] == "1")
        //                            {
        //                                string mediaUrl3 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField3.MediaItem);
        //                                slider3Image.Style["background-image"] = "url(" + mediaUrl3 + ")";
        //                                slider3Image.Visible = true;
        //                                a3.Visible = true;
        //                                if (item["Banner3 Video"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "myscript2", "functionToExecute('linktypethree');", true);
        //                                }
        //                                if (item["Banner3 Link Type"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "linkscript3", "linktype('linktypethree');", true);
        //                                }
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                continue;
        //                            }
        //                        }
        //                    case 4:
        //                        {
        //                            Sitecore.Data.Fields.ImageField imgField4 = ((Sitecore.Data.Fields.ImageField)item.Fields["Image 4"]);
        //                            if (!string.IsNullOrEmpty(imgField4.Src) && !string.IsNullOrEmpty(item["Slider Content4"]) && item["Show Banner4"] == "1")
        //                            {
        //                                string mediaUrl4 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField4.MediaItem);
        //                                slider4Image.Style["background-image"] = "url(" + mediaUrl4 + ")";
        //                                slider4Image.Visible = true;
        //                                a4.Visible = true;
        //                                if (item["Banner4 Video"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "myscript3", "functionToExecute('linktypefour');", true);
        //                                }
        //                                if (item["Banner4 Link Type"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "linkscript4", "linktype('linktypefour');", true);
        //                                }
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                continue;
        //                            }
        //                        }

        //                    case 5:
        //                        {
        //                            Sitecore.Data.Fields.ImageField imgField5 = ((Sitecore.Data.Fields.ImageField)item.Fields["Image 5"]);
        //                            if (!string.IsNullOrEmpty(imgField5.Src) && !string.IsNullOrEmpty(item["Slider Content5"]) && item["Show Banner5"] == "1")
        //                            {
        //                                string mediaUrl5 = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField5.MediaItem);
        //                                slider5Image.Style["background-image"] = "url(" + mediaUrl5 + ")";
        //                                slider5Image.Visible = true;
        //                                a5.Visible = true;
        //                                if (item["Banner5 Video"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "myscript4", "functionToExecute('linktypefive');", true);
        //                                }
        //                                if (item["Banner5 Link Type"] == "1")
        //                                {
        //                                    ScriptManager.RegisterStartupScript(this, GetType(), "linkscript5", "linktype('linktypefive');", true);
        //                                }
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                continue;
        //                            }
        //                        }
        //                }
        //            }
        //            catch (Exception)
        //            {

        //            }
        //        }
        //    }
        //}

    }
}