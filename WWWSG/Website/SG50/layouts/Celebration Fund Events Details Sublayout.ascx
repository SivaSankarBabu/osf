﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" Inherits="Layouts.Celebration_fund_events_details_sublayout.Celebration_fund_events_details_sublayoutSublayout" CodeFile="~/SG50/layouts/Celebration Fund Events Details Sublayout.ascx.cs" %>
<link href="../../Css/responsive.css" rel="stylesheet" />
<link href="../../Css/style.css" rel="stylesheet" />
<style type="text/css">
    body {
        margin: 20px;
        font: normal 12px Arial, Helvetica, sans-serif;
    }

    h3 {
        font-size: 20px;
        border-bottom: #CCC solid 1px;
        padding: 0 0 5px 0;
        margin: 10px 0 2px;
    }

    #oneColumn {
        width: 100%;
        margin: 1% auto;
    }


        #oneColumn div {
            width: 100%;
            min-height: 150px;
            background: #CCC;
        }

    #twoColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }

        #twoColumn div {
            width: 49%;
            margin: 0 1% 0 0;
            min-height: 150px;
            background: #CCC;
            float: left;
        }

            #twoColumn div + div {
                width: 49%;
                margin: 0 0 0 1%;
                min-height: 150px;
                background: #CCC;
                float: left;
            }

    #threColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }


        #threColumn div {
            width: 68%;
            margin: 0 1% 0 0;
            min-height: 326px;
            background: #CCC;
            float: left;
        }

            #threColumn div + div, #threColumn div + div + div {
                width: 30%;
                margin: 0 0 0 1%;
                min-height: 150px;
                background: #CCC;
                float: left;
                ;
            }

                #threColumn div + div + div {
                    margin-top: 2%;
                }

    #fourColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }

        #fourColumn div, #fourColumn div + div + div {
            width: 49%;
            margin: 0 1% 0 0;
            min-height: 150px;
            background: #CCC;
            float: left;
        }

            #fourColumn div + div, #fourColumn div + div + div + div {
                margin: 0 0 0 1%;
            }

                #fourColumn div + div + div, #fourColumn div + div + div + div {
                    margin-top: 2%;
                }

    #fiveColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }


        #fiveColumn div, #fiveColumn div + div + div {
            width: 49%;
            margin: 0 1% 0 0;
            min-height: 150px;
            background: #CCC;
            float: left;
        }

            #fiveColumn div + div, #fiveColumn div + div + div + div {
                margin: 0 0 0 1%;
            }

                #fiveColumn div + div + div, #fiveColumn div + div + div + div {
                    margin-top: 2%;
                    margin-bottom: 2%;
                }

                    #fiveColumn div + div + div + div + div {
                        float: inherit;
                        margin: 0 auto;
                        width: 49%;
                        display: flex;
                    }

    #sixColumn {
        width: 100%;
        margin: 1% auto;
        display: table;
    }


        #sixColumn div, #sixColumn div + div + div + div {
            width: 49%;
            margin: 0 1% 0 0;
            min-height: 326px;
            background: #CCC;
            float: left;
        }

            #sixColumn div + div, #sixColumn div + div + div, #sixColumn div + div + div + div + div, #sixColumn div + div + div + div + div + div {
                width: 49%;
                margin: 0 0 0 1%;
                min-height: 150px;
                background: #CCC;
                float: left;
            }

            #sixColumn div + div {
                margin-bottom: 2%;
            }

                #sixColumn div + div + div + div, #sixColumn div + div + div + div + div, #sixColumn div + div + div + div + div + div {
                    margin-top: 2%;
                }
</style>
<script src='https://connect.facebook.net/en_US/all.js'></script>
<p>
    <a onclick='postToFeed();'>
        <%--<img src="images/fb.png" />--%></a>
</p>
<p id='msg'></p>
<script>
    FB.init({
        appId: "1512708352296160", status: true, cookie: true
    });

    function postToFeed(Title, Description, Img) {
        // calling the API ...


        var fbtitle = Title;
        var fbdes = Description;
        //var obj1 = "Nag";
        var fbimg = Img


        var obj = {
            method: 'feed',
            redirect_uri: 'https://www.facebook.com/cryswashington?fref=ts',
            link: 'https://www.singapore50.sg',
            picture: fbimg,
            name: fbtitle,
            caption: fbdes,
            description: fbdes
        };

        //function callback(response) {

        //    document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
        //}

        FB.ui(obj);

    }

    function newPopup(url) {
        var tempurl = 'https://twitter.com/intent/tweet?text=' + BuildStr(url)
        popupWindow = window.open(
            tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')
    }

</script>
<section class="main-content">
    <!---main-content start-------->
    <section class="grid-content">
        <!---grid-content start-------->
        <div class="head-title">
            <div class="project-title">
                <h1>
                    <sc:Text ID="txtTitle" Field="Title" runat="server" />
                </h1>
               

            </div>
            <sc:Text ID="txtShortDescription" Field="Short Description" runat="server" />
    </section>
    <!---grid-content ends-------->
    <section class="grid-content">

        <div id="dvVideo" runat="server" style="text-align: center">
            <a id="refVideo" runat="server" href="" class="video">
                <sc:Image ID="imageVideo" runat="server" Field="Video Image1" />

            </a>
        </div>
    </section>
   
    <section class="grid-content">
        <!---grid-content start-------->
        <div class="grid-container">

            <%--  <div class="img-container-left">
                    <a href="#">
                        <img src="../images/gallary-img.jpg" alt="gallary-imag" /></a>
                </div>
                <div class="img-container-right">
                    <a href="#">
                        <img src="../images/gallary-img2.jpg" alt="gallary-imag" /></a>
                    <a href="#">
                        <img src="../images/gallary-img3.jpg" alt="gallary-imag" /></a>
                </div>--%>
            <div id="ImageCount" runat="server">
                <%--<div id="twoColumn">
                    <div><asp:Image ID="img1"  runat="server" /></div>
                    <div><asp:Image ID="img2"  runat="server" /></div>
                </div>--%>
                <%-- <div id="threColumn">
                    <div class="first">
                        <img src="/SG50/images/gallary-img.jpg" alt="gallary-imag" />
                        <asp:Image ID="img1"  runat="server" />
                    </div>
                    <div>
                        <img src="/SG50/images/gallary-img2.jpg" alt="gallary-imag" />
                    </div>
                    <div>
                        <img src="/SG50/images/gallary-img3.jpg" alt="gallary-imag" />
                    </div>
                </div>--%>
            </div>
            <div class="head-title ">
                <div class="project-title">
                    <h1>The interview</h1>
                </div>
                <p class="caps-font">Duis autem vel eum iriure Sed ut perspiciatis unde omnis iste natus error sit vop accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                <span class="auth-name">Adi Puteri</span>
            </div>
            <div class="question-container">
                <sc:Text ID="txtQuestion" Field="Interview_Story" runat="server" />
            </div>
            <%-- <asp:Repeater ID="repsocialSharing" runat="server">
                <ItemTemplate>
                    <div>
                        <a href="JavaScript:postToFeed('<%# Eval("FaceBookTitle")%>','<%# Eval("FaceBookContent")%>','<%# Eval("FaceBookThumbNail")%>')">
                            <img src="/SG50/images/facebook.png" />
                        </a>
                        <a href="JavaScript:newPopup('<%# Eval("TwitterContent")%>');">
                            <img src="/SG50/images/twitter.png" />
                        </a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>--%>

            <div class="social-contianer">
                <asp:Repeater ID="repsocialSharing" runat="server">
                    <ItemTemplate>
                        <div>
                            <ul>
                                <li><a href="JavaScript:postToFeed('<%# Eval("FaceBookTitle").ToString()%>','<%# Eval("FaceBookContent").ToString()%>','<%# Eval("FaceBookThumbNail")%>')">
                                    <img src="/SG50/images/facebook.png" /></a>

                                </li>
                                <li><a href="JavaScript:newPopup('<%# Eval("TwitterContent").ToString()%>');">
                                    <img src="/SG50/images/twitter.png" /></a>

                                </li>

                            </ul>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <%--<a  href="#">Previous</a>
                     <a class="deactive" href="#">NEXT</a>--%>
                <div>
                    <asp:Button ID="btnPrevious" runat="server" Text="Previous" OnClick="btnPrevious_Click" />
                    <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" />
                </div>
            </div>


        </div>
        </div>
        </div>
    </section>
    <!---grid-content ends-------->
</section>
<!---main-content end-------->
