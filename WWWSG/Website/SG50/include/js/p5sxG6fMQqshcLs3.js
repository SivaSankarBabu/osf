$(function() {
$.getJSON('https://graph.facebook.com/sg2015/albums?access_token='+x2HOnsd, function (data) {
            fcontent = '';
            var result = $.map(data, function (value, key) {
                if (key != 'paging') {
                    for (var i in value) {
						if ( value[i]['name'] !== "Timeline Photos" &&
						 value[i]['name'] !== "Cover Photos" &&
						 value[i]['name'] !== "Profile Pictures"
					    ){
                        var description = '';
                        var full_description = '';
                        var updated_date = new Date(value[i]['updated_time'].substr(0,19)).toDateString();

                        if (typeof value[i]['description'] != 'undefined') {
                            full_description = value[i]['description'];
                            if (value[i]['description'].length >= 180) {
                                description = value[i]['description'].substr(0, 170) + "...";
                            } else {
                                description = value[i]['description'];
                            }
                        } else {
                            full_description = description = '';
                        }
                        if (typeof value[i]['count'] != 'undefined') {
                            no_of_photos = value[i]['count'];
                        } else {
                            no_of_photos = 0;
                        }
                        fcontent += '<li id="' + value[i]['id'] + '">' +
                             '<div class="photo-cont">' +
                                '<div class="ImageContainer"><img src="https://graph.facebook.com/' + value[i]['cover_photo'] + '/picture' + '"/></div>' +
                                '<div class="no-photos right">' + no_of_photos + '</div>' +
                                '<h1>' + value[i]['name'] + '</h1>' +
                                '<span>' + updated_date + '</span>' +
                                "<p data-rel='"+ full_description +"'>" + description + '</p>' +
                            '</div>' +
                        '</li>';
                    }
				  }
                }
            });
            $("#FaceBookList").html(fcontent);
            $(".SG50Loader").hide();
        });

    $(document).on("click", "li", function (event) {
	var album_id = jQuery(this).attr('id');
        var t = $('#'+album_id+' h1').html();
        var d = $('#'+album_id+' p').data('rel');
	localStorage.setItem('facebook_album',album_id);
        localStorage.setItem('title', t);
        localStorage.setItem('description', d);
        window.location = "/SG50/GalleryLanding/GalleryPhotoList/GalleryPhotos.aspx";
	});
});
