jQuery(document).ready(function() {

	$(".video").click(function() {
		var winWidth = $(window).width();
		var vidWidth = 640;
		var vidHeight = 385;
		if(winWidth<769){
			vidWidth = '100%';
			vidHeight = 285;
		}

		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'title'			: this.title,
			/*'width'			: 640,
			'height'		: 385,*/
			'width'			: vidWidth,
			'height'		: vidHeight,
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'			: 'swf',
			'swf'			: {
			'wmode'				: 'transparent',
			'allowfullscreen'	: 'true'
			}
		});
		

		return false;
	});



});
