$(document).ready(function () {
    var options = '', track_click = 0, total_pages = '2';
    var data = 'all',
        fbContentforVoteforItem = 'I have selected this item for the SG50 Time Capsule. The capsule, which will be re-opened in 2065, will hold 50 items that tell future generations something about what our life is like today. Pick your favourite item to send a message to the future at www.singapore50.sg/sg50timecapsule #SG50timecapsule #hellotmr',
        twitterContentforVoteforItem = 'I have selected this item for the #SG50TimeCapsule. Which item will you pick? Visit http://bit.ly/1NKLL9Y #hellotmr',
        fbContentforSuggestItem = 'I have suggested an item for the SG50 Time Capsule. The capsule, which will be re-opened in 2065, will hold 50 items that tell future generations something about what our life is like today. Now’s your turn to select your favourite items or suggest new ones at www.singapore50.sg/sg50timecapsule #SG50timecapsule #hellotmr',
        twitterContentforSuggestItem = 'I have suggested an item for the #SG50TimeCapsule. Now’s your turn! Visit http://bit.ly/1NKLL9Y #hellotmr', fbTitle = 'SG50 Time Capsule';


    $('#your_story').simplyCountable({
        maxCount: 500,
        onMaxCount: function (count, countable, counter) {
            alert('Maximum of 500 characters are allowed.')
        }
    });

    $("#v_your_story").characterCounter({
        limit: 500,
        counterSelector: '#my_counter',
        onExceed: function () {
            alert("Maximum of 500 characters are allowed.");
        }
    });

    $(document).on('click', '#ShowSuggestAnItem img,#Li1 img', function () {
        var dtop = parseInt($(window).scrollTop()) - 50;
        var divId = '.suggestAnItem_popup';
        $(divId).find('input[type=text],textarea').removeClass('error').val(''); $(divId).find('span.errortxt').hide();
        $(divId).find('span#counter').text(500);
        $('#suggestanitemform label.error').hide(); $(divId).find('#btnSuggestNewItem').removeAttr('disabled'); $(divId).addClass('productactive').css('top', dtop);
        $('.overlaybg').fadeIn();

    });

    $('#imgFbShareSuggestNewItem').click(function () {
        FaceBookShare($('#pSuggestFBTitle').text(), $('#pSuggestFbContent').text(), '', $('#spanEventUrl').text());
    });

    $('#imgTweetShareSuggestNewItem').click(function () {
        TwitterShare($.trim($('#pSuggestTwitterContent').text()), $('#spanEventUrl').text().replace($('#spanHostName').text(), ''), $('#spanHostName').text(), $('#spanAccessToken').text());
    });

    $(document).on('click', '#ShowVote', function () {
        $('.VoteForItem_popup').addClass('productactive');
        $('.overlaybg').fadeIn();
    });

    $(document).on('click', '.grabItem a,.caption,.thumbIcon,.boxcontainer h4', function () {
        var parentItem = $('.VoteForItem_popup'), dtop = parseInt($(window).scrollTop()) - 60;
        parentItem.find('input[type=text],textarea').removeClass('error').val('');
        parentItem.find('span#my_counter').text(500);
        parentItem.addClass('productactive').css('top', dtop); parentItem.find('label.error').hide(); parentItem.find('#btnVoteForAnItem').removeAttr('disabled');
        $('.overlaybg').fadeIn();
        //$("html, body").animate({ scrollTop: 900 }, "slow");
    });

    /* Get Selected Categories when User Selects Multiple */
    function GetSelectedCategories(ctrlId) {
        var selectedCategories = '';
        if ($(ctrlId).length > 0) {
            $(ctrlId).find('li a.current').each(function () {
                selectedCategories += selectedCategories != '' ? ',' + $(this).text() : $(this).text();
            }); return selectedCategories;
        } else { return false; }
    }

    /* Facebook Share Calling */
    $('#imgVoteThankyouFbShare').click(function () {
        GATrackingFBTwitterShare('Facebook', GetSelectedCategories(filterSectionDiv), $('#spanThankyouItemName').text().trim());
        FaceBookShare($('#spanThankyouItemName').text(), $('#pVoteFbContent').text(), $('#spanHostName').text() + $('.thankyouHolder2 img:eq(1)').attr('src'), $('#spanEventUrl').text()); return false;
    });

    /* Twitter Share Calling */
    $('#imgVoteThankyouTwitter').click(function () {
        GATrackingFBTwitterShare('Twitter', GetSelectedCategories(filterSectionDiv), $('#spanThankyouItemName').text().trim());
        TwitterShare($.trim($('#pVoteTwitterContent').text()), $('#spanEventUrl').text().replace($('#spanHostName').text(), ''), $('#spanHostName').text(), $('#spanAccessToken').text()); return false;
    });

    $('.suggestcate').click(function () {
        var divId = '.suggestAnItem_popup';
        $(divId).find('input[type=text],textarea').removeClass('error').val('');
        $(divId).find('span#counter').text(500); $('#suggestanitemform label.error').hide();
        var dtop = parseInt($(window).scrollTop()) - 70;
        $(".suggestcate").toggleClass("activeCate");
        if ($('.suggestAnItem_popup').hasClass("productactive")) {
            $('.suggestAnItem_popup').removeClass('productactive');
            //$('.overlaybg').fadeOut();
        } else {
            $('.suggestAnItem_popup').addClass('productactive').css('top', dtop);
            $('.overlaybg').fadeIn();
        }
    })
    $('.viewcate').click(function () {
        $(".viewcate").toggleClass("activeCate");
    })
    $('.clostBtn').click(function () {
        $('.VoteForItem_popup, .suggestAnItem_popup, .suggestcate').removeClass('productactive');
        $('.overlaybg').fadeOut();
    })
    $('.clostBtn').click(function () {
        $('.SuggestThankYou_popup').removeClass('thankyouactive');
        $('.overlaybg').fadeOut();
    });
    $('.clostBtn').click(function () {
        $('.SuggestThankYou_popup, .vus_popup').removeClass('thankyouactive');
        return false;
    });
    $('.clostBtn').click(function () {
        $('.suggestcate').removeClass('activeCate');
    })
    $("#showDiv").on("click", function () {
        $(".filterSection").slideToggle("slow");
    });

    function getheight() {
        var fixedHeight = $('ul .boxcontainer ')[1];
        fixedHeight = $(fixedHeight).height();
        var setheight = $('ul .boxcontainer ')[0];
        fixedHeight = (fixedHeight - 1) + "px";
        $(setheight).css({
            "height": fixedHeight
        });
    }
    $(window).load(function () {
        getheight();
    });
    $(window).resize(function () {
        getheight();
    });
});

var FBID = "896578207027840";
window.fbAsyncInit = function () {
    FB.init({
        appId: FBID,
        status: true,
        cookie: true,
        version: 'v2.3'
    });
};
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function FaceBookShare(Title, Description, img, HostName) {
    var fbtitle = Title, fbdes = Description, fbimg = img == '' || img == undefined || img == undefined ? $('#spanHostName').text() + $('#fbShareIcon').attr('src') : img;
    var obj = {
        method: 'feed', link: HostName, picture: fbimg, name: fbtitle.toString(), description: fbdes.toString()
    }; FB.ui(obj); return false;
}

function BuildStr(value) {
    if (value != '' && value != null) {
        return value.replace(/%/gi, '%25').replace(/#/gi, '%23').replace(/;/gi, '%3B').replace(/^/gi, '%5E').replace(/'/gi, "\\'"); ;

        //.replace(/&/gi, '%26')
//        .replace(/’/gi, '%27').replace(/\/'/gi, '%27')
    } else { return ''; }
}

function TwitterShare(urlasp, url1, HostName, AccessToken) {
    var accessToken = AccessToken;
    var url = 'https://api-ssl.bitly.com/v3/shorten?access_token=' + accessToken + '&longUrl=' + encodeURIComponent(HostName + url1);
    var result;
    $.getJSON(url, {}, function(response) {
        result = response.data.url;
        var tempurl = 'https://twitter.com/intent/tweet?text=' + escape(urlasp).replace('%u2019', '%27');
        popupWindow = window.open(tempurl, 'popUpWindow', 'height=400,width=600,left=300,top=80,resizable=no,scrollbars=no,toolbar=yes,menubar=no,location=no,directories=no,status=no')
    });

}

/* Facebook and Twitter share GA Tracking  */
function GATrackingFBTwitterShare(shareType, catName, itemName) {
    if (shareType != '' && shareType != undefined && shareType != null) {
        if (shareType == 'Facebook') {
            ga("send", "event", catName, "Click", itemName + '- Post - Facebook Share');
        } else if (shareType == 'Twitter') {
            ga("send", "event", catName, "Click", itemName + ' - Post - Twitter Share');
        } else { return false; }
    }
}



