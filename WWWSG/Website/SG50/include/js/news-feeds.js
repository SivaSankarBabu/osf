
var load_feed_data = function(e) {
    var t = "";
    var n = "";
    if (e.data.raw[6].type.toUpperCase() === "TWITTER") {
        if (e.data.raw[6].post.length >= 101) {
            t = e.data.raw[6].post.substr(0, 101) + "..."
        } else {
            t = e.data.raw[6].post
        }
    }

    if (e.data.raw[7].type.toUpperCase() === "FACEBOOK") {
        if (e.data.raw[7].post.length >= 85) {
            n = e.data.raw[7].post.substr(0, 85) + "..."
        } else {
            n = e.data.raw[7].post
        }
    }
 
    $(".newsfeed").html('<div class="newsfeed">'+
          '<div class="news-left">'+
           '<a href="' + e.data.raw[0].link + '" title="' + e.data.raw[0].post.replace(/'/g, "").replace(/"/g, "") + '">' + 
            '<div class="youtube"><iframe width="100%" src="https://www.youtube.com/embed/' + e.data.raw[0].post_id + '?html5=1" frameborder="0" allowfullscreen></iframe></div></a>'+
             '<div class="news-right">'+
              '<div class="feeds tweet">'+
                '<a href="https://twitter.com/sg50" target="_blank" title="' + e.data.raw[6].post.replace(/'/g, "").replace(/"/g, "") + '"><div class="feeds-logo"><img src="/SG50/images/Home Page/news-twitter.png" alt="" /></div>'+
                '<div class="feeds-top"><img src="' + e.data.raw[6].authorImageUrl + '" alt="TWITTER" />'+
                  '<p>' + e.data.raw[6].authorName +'<span>@twitterapi</span></p>'+
                '</div>'+
                '<div class="feeds-mid">'+
                  '<p>' + t +'</p>'+
                '</div>'+
                '<div class="feeds-bottom">'+
                  '<!--<p><img src="/SG50/images/Home Page/tweet-some.png" alt="" />by Someone</p> -->'+
                  '<ul>'+
                    '<li><a href="https://twitter.com/intent/tweet?in_reply_to=' + e.data.raw[6].post_id + '"><img src="/SG50/images/Home Page/tweet-back.png" alt="" /></a></li>' +
                    '<li><a href="https://twitter.com/intent/retweet?tweet_id=' + e.data.raw[6].post_id + '"><img src="/SG50/images/Home Page/tweet-some.png" alt="" /></a></li>' +
                    '<li><a href="https://twitter.com/intent/favorite?tweet_id=' + e.data.raw[6].post_id + '"><img src="/SG50/images/Home Page/tweet-rating.png" alt="" /></a></li>'+
                  '</ul>'+
                '</div>'+
              '</div>'+
              '<div class="instagram01"><span><a href="' + e.data.raw[1].link + '" target="_blank"><img src="/SG50/images/Home Page/news-insta.png" alt="" /></a></span><a href="' + e.data.raw[1].link + '" target="_blank"><img src="' + e.data.raw[1].imageUrl + '" title="' + e.data.raw[1].post.replace(/'/g, "").replace(/"/g, "") + '" /></a></div>'+
              '</div>'+
           
          '</div>'+
          '<div class="news-right">'+
           '<div class="news-logo-pan">'+
              '<div class="news-logo-left"><img src="/SG50/images/Home Page/newsfeed-big-logo.png" alt="" /></div>'+
              '<div class="news-logo-right">Follow us for the latest updates and conversations.'+
                '<ul>'+
                  '<li><a href="https://www.facebook.com/sg2015" target="_blank"><img src="/SG50/images/Home Page/fbicon.png" alt="facebook" /></a></li>'+
                  '<li><a href="https://twitter.com/sg50" target="_blank"><img src="/SG50/images/Home Page/news-twitter.png" alt="twitter" /></a></li>'+
                  '<li><a href="https://www.youtube.com/user/ilovesingapore50" target="_blank"><img src="/SG50/images/Home Page/news-youtube.png" alt="youtube" /></a></li>'+
                  '<li><a href="https://instagram.com/singapore50" target="_blank"><img src="/SG50/images/Home Page/news-insta.png" alt="instagram" /></a></li>'+
                '</ul>'+
              '</div>'+
            '</div>'+
            '<div class="insta">'+
              '<ul>'+
                 
                '<li><span><a href="' + e.data.raw[2].link + '" target="_blank"><img src="/SG50/images/Home Page/news-insta.png" alt="" /></a></span><a href="' + e.data.raw[2].link + '" target="_blank"><img src="' + e.data.raw[2].imageUrl + '" title="' + e.data.raw[2].post.replace(/'/g, "").replace(/"/g, "") + '" /></a></li>'+
                '<li><span><a href="' + e.data.raw[3].link + '" target="_blank"><img src="/SG50/images/Home Page/news-insta.png" alt="" /></a></span><a href="' + e.data.raw[3].link + '" target="_blank"><img src="' + e.data.raw[3].imageUrl + '" title="' + e.data.raw[3].post.replace(/'/g, "").replace(/"/g, "") + '" /></a></li>'+
              '</ul>'+
            '</div>'+
            '<div class="feed-pan">'+
             '<a href="' + e.data.raw[7].link + '" title="' + e.data.raw[7].post.replace(/'/g, "").replace(/"/g, "") + '" target="_blank">'+
              '<div class="feeds">'+
                '<div class="feeds-logo"><img src="/SG50/images/Home Page/tweet-fb.png" alt="" /></div>'+
                '<div class="feeds-top"><img src="' + e.data.raw[7].authorImageUrl + '" alt="" />'+
                  '<p> '+ e.data.raw[7].authorName + '</p>'+
                '</div>'+
                '<div class="feeds-mid">'+
                  '<p> '+ n + '</p>'+
                '</div>'+
              '</div></a>'+
            '</div>'+
            '<div class="instagram04"><span><a href="' + e.data.raw[4].link + '" target="_blank"><img src="/SG50/images/Home Page/news-insta.png" alt="" /></a></span><a href="' + e.data.raw[4].link + '" target="_blank"><img src="' + e.data.raw[4].imageUrl + '" title="' + e.data.raw[4].post.replace(/'/g, "").replace(/"/g, "") + '" /></a></div>'+
            '<div class="instagram05"><span><a href="' + e.data.raw[5].link + '" target="_blank"><img src="/SG50/images/Home Page/news-insta.png" alt="" /></a></span><a href="' + e.data.raw[5].link + '" target="_blank"><img src="' + e.data.raw[5].imageUrl + '" title="' + e.data.raw[5].post.replace(/'/g, "").replace(/"/g, "") + '" /></a></div>'+
            
          '</div>'+
          '<div class="clear"></div>'+
        '</div>')
};
var load_social_feed_data = function(e) {
    var t = "<ul>";
    var n = e.data.raw;
    $.each(n, function(e, n) {
        t = t + '<li><a href="' + n.link + '" target="_blank"><img src="' + n.imageUrl + '" title="' + n.post.replace(/'/g, "").replace(/"/g, "") + '"/></a></li>'
    });
    t = t + "</ul>";
    $(".socail-feed").html(t)
};
$(document).ready(function() {
    jQuery.ajax({
        type: "GET",
        url: "https://eureka.circussocial.com/admin/api/officialfeed",
        async: false,
        dataType: "json",
        crossDomain: true,
        beforeSend: function(e) {
            $(".loading").show();
        },
        success: function(e, t, n) {
            $(".loading").hide();
            if (e.status_code == "200") {
                $(".newsfeed").closest(".news-feed").show();
                load_feed_data(e)
            } else if (e.status_code == "400") {

              $(".newsfeed").closest(".news-feed").hide()
            }
        },
        error: function(e) {
            $(".loading").hide();
            $(".newsfeed").closest(".news-feed").hide();
            console.log(e)
        }
    });
    jQuery.ajax({
        type: "GET",
        url: "https://eureka.circussocial.com/admin/api/userfeed",
        async: false,
        dataType: "json",
        crossDomain: true,
        beforeSend: function(e) {
            $(".loadingsocial").show();
        },
        success: function(e, t, n) {
            $(".loadingsocial").hide();
            if (e.status_code == "200") {
                if ($(window).width() >= 640) $(".socail-feed").closest(".news-feed").show();
                load_social_feed_data(e)
            } else if (e.status_code == "400") {

                    $(".socail-feed").closest(".news-feed").hide();

            }
        },
        error: function(e) {
            $(".loadingsocial").hide();
            $(".socail-feed").closest(".news-feed").hide();
            console.log(e)
        }
    })
})
