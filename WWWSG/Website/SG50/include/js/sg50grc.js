function getLocation(location_no) {
	if( location_no == 0 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0020.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0204.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0269.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0388.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0426.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0582.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0712.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0787.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_0812.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_1143.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_1191.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_1334.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_1396.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_1526.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_001.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(15)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_002.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(16)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_003.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(17)' ).attr('src', '/SG50/images/Sg50GRC/locations/0/Jeremy_004.jpg');
	}

	if( location_no == 1 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_0147.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_0050.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_0267.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_0478.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_0638.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_0863.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/1/Jeremy_0123.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_1194.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_1277.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_1388.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_1453.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_1625.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_1664.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_1785.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/1/Gent_0984.jpg');
	}

	if( location_no == 2 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0050.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0116.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0238.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0271.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0496.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0672.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0735.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0783.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0860.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_1247.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0547.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/2/day16__E7A5823.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_1347.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_1419.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/2/Shane_0944.jpg');
	}

	if( location_no == 4 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_01.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_09.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/4/_E7A8314.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_04.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/4/_E7A8188.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_07.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_02.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/4/_E7A8464.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_03.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/4/day16__E7A5459.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/4/ValenceDAY2_1441.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/4/_E7A8251.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/4/_E7A8378.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_05.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/4/ec-grc_08.jpg');
	}

	if( location_no == 5 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/5/Raymond_0100.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/5/Raymond_1071.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/5/Raymond_1165.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/5/Allan_0646.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/5/Allan_0724.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/5/Allan_0866.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/5/Allan_0903.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/5/Allan_1106.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/5/Raymond_0174.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/5/Allan_1252.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/5/E7A8064.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/5/day17__E7A5917.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/5/day17__E7A6191.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/5/day17__E7A6376.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/5/day17__E7A6404.jpg');
	}

	if( location_no == 6 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/6/Allan_0050.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0064.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0224.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0275.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0429.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0626.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0803.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0925.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_1904.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_1138.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_1206.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_0972.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_1591.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_1504.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/6/Hizuan_2028.jpg');
	}

	if( location_no == 7 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_0241.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_0348.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_0464.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_1140.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_1209.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_1283.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_1379.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_1654.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_2304.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_2536.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_0026.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_0127.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_1544.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/7/Patrick_1465.jpg');
	}

	if( location_no == 8 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0221.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0116.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0014.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0289.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0239.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_1341.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0384.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0800.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0639.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0860.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_1195.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_1103.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_1141.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_0992.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/8/Daniel_1261.jpg');
	}

	if( location_no == 9 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0024.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0271.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0420.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0357.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0621.jpg');	
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0780.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/9/Rene_0798.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/9/Shane_1143.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0155.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0190.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0506.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0844.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0254.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0072.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/9/Roy_0685.jpg');
	}

	if( location_no == 10 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_1094.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_428.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_0676.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_1140.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_031.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_159.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_366.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_0573.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_315.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_1545.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_0939.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_0860.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_254.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_476.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/10/punggol_pasirris_183.jpg');
	}

	if( location_no == 11 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_10.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_07.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_09.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_03.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_08.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_05.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_12.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_02.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_13.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_04.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_14.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_06.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_01.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_11.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/11/sembawang_15.jpg');
	}

	if( location_no == 12 ) {		
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A37571304.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A3129086.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A3269226.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A40111558.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A3388345.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A3319276.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A3193150.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/12/TampinesGRC-7.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/12/1E7A3582539.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/12/TampinesGRC-6.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/12/TampinesGRC-5.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/12/TampinesGRC-8.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/12/TampinesGRC-1.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/12/TampinesGRC-2.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/12/TampinesGRC-4.jpg');
	}

	if( location_no == 13 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0134.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0033.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_1310.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0864.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0289.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0081.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0171.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0372.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0484.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0908.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0956.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_1075.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0721.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/13/ValenceDAY2_0789.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/13/tp_15.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(15)').attr('src', '/SG50/images/Sg50GRC/locations/13/tp_16.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(16)').attr('src', '/SG50/images/Sg50GRC/locations/13/tp_17.jpg');
	}

	if( location_no == 14 ) {
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(0)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1777.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(1)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_0289.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(2)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1356.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(3)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1378.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(4)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1067.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(5)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1538.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(6)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1501.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(7)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_0722.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(8)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1601.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(9)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_0566.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(10)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_0070.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(11)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1139.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(12)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_0514.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(13)').attr('src', '/SG50/images/Sg50GRC/locations/14/Allan_0975.jpg');
		$( '.sg50grc-row-location-' + location_no + ' .sg50grc-image:eq(14)').attr('src', '/SG50/images/Sg50GRC/locations/14/MunKong_1681.jpg');
	}
}

$( document ).ready(function() 
{	
	var $container = $('.grid');
	var dropdown_list = $( '.sg50grc-dropdown-value-list' );
	var location_toggle = $( '.sg50grc-toggle-location' );
	var location_title = $( '.sg50grc-dropdown-value' );
	
	//Load Masory
	$(window).on('load', function() {
      $container.masonry({
        // options
        itemSelector: '.grid-item',
        columnWidth: 285,
        gutter: '.gutter-sizer'
      });
    });

	//Resize
	$(window).resize(function() {
		if( window.innerWidth <= 612 ) {
			$( '.masory-grid' ).css('max-width', '288px');
		} else if( window.innerWidth <= 907 ) {
			$( '.masory-grid' ).css('max-width', '582px');
		} else if( window.innerWidth <= 1202 ) {
			$( '.sg50grc-map' ).css('background', 'url(/SG50/images/Sg50GRC/top/map-mobile.png) no-repeat top center');
			$( '.sg50grc-map' ).css('padding-bottom', '145px');
			$( '.sg50grc-map > .sg50grc-wrapper, .sg50grc-map > .sg50grc-marker' ).hide();
			$( '.masory-grid' ).css('max-width', '878px');
		} else {
			$( '.sg50grc-map' ).css('background', 'url(/SG50/images/Sg50GRC/top/map-color.png) no-repeat top right');
			$( '.sg50grc-map' ).css('padding-bottom', '0px');
			$( '.sg50grc-map > .sg50grc-wrapper, .sg50grc-map > .sg50grc-marker' ).show();
			$( '.masory-grid' ).css('max-width', '1170px');
		}
	});

	//Map configuration
	$('.sg50grc-map > .sg50grc-marker > img').hover(function() {
        $(this).addClass('transition');
    }, function() {
        $(this).removeClass('transition');
    });

	$(".sg50grc-map > .sg50grc-marker > img").click(function() { 
	    var location_no
	    , location_flag
	    , location_title_fromflag
	    , div1, div2, tdiv1, tdiv2;

	    location_no = $(this).attr('data-location');
	    location_flag = $(this).attr('data-flag');
	    location_location_clicked = $(this).attr('data-title');

	    location_title.text( location_location_clicked );
	    location_toggle.hide();

	    setTimeout(function() {
			$container.masonry('destroy'); 
			if( $( ".sg50grc-row-location-" + location_no + " .grid-item:first" ).attr('id') ) {
				//Do ntg ...
			} else {
				getLocation(location_no);

				div1 = $( ".sg50grc-row-location-" + location_no + " .grid-item:first" );
			    div2 = $( "#flag-" + location_flag );

			    tdiv1 = $( ".sg50grc-row-location-" + location_no + " .grid-item:first" ).clone();
			    tdiv2 = $( "#flag-" + location_flag ).clone();

			    div1.replaceWith(tdiv2);
		    	div2.replaceWith(tdiv1);
		    }
		}, 10);

		setTimeout(function() {
			$container.imagesLoaded( function() {
				$container.masonry();
			});
		}, 10);

		$( '.sg50grc-row-location-' + location_no ).fadeIn('slow');

		//Animate
		setTimeout(function() {
		   $('html, body').animate({
		        scrollTop: $( "#flag-" + location_flag ).offset().top - 60
		    }, 1000);
		}, 500);
	});

	//Dropdown
	$( '.sg50grc-dropdown' ).on( 'click', function() {
		if( $( this ).hasClass( 'closed' ) ) {
			$( this ).removeClass( 'closed' );
			dropdown_list.show();

			if( window.innerWidth <= 612 ) {
				$( '.masory-grid' ).css('min-height', '800px');
			} else if( window.innerWidth <= 907 ) {
				$( '.masory-grid' ).css('min-height', '800px');
			} else if( window.innerWidth <= 1202 ) {
				$( '.masory-grid' ).css('min-height', '650px');
			} else {
				$( '.masory-grid' ).css('min-height', '650px');
			}
		} else {
			$( this ).addClass( 'closed' );
			dropdown_list.hide();
			$( '.masory-grid' ).css('min-height', '0px');
		}	
	});

	$( '.sg50grc-dropdown-value-list > li' ).on( 'click', function() {	
		var location_no = $(this).attr('data-location');
		var location_new_title = $(this).text();

		$( '.sg50grc-dropdown' ).addClass( 'closed' );
		dropdown_list.hide();
		location_toggle.hide();

		location_title.text( location_new_title );

		setTimeout(function() {
			$container.masonry('destroy'); 
			if( $( ".sg50grc-row-location-" + location_no + " .grid-item:first" ).attr('id') ) {
				//Do ntg ...
			} else {
				getLocation(location_no);
			}
		}, 10);

		setTimeout(function() {
			$container.imagesLoaded( function() {
				$container.masonry();
			});
		}, 10);

		$( '.masory-grid' ).css('min-height', '0px');
		$( '.sg50grc-row-location-' + location_no ).fadeIn('slow');
	});
});