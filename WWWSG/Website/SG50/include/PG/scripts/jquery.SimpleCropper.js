/* 
    Author     : Tomaz Dragar
    Mail       : <tomaz@dragar.net>
    Homepage   : http://www.dragar.net
*/

(function($) {

    $.fn.simpleCropper = function() {

        var image_dimension_x = 500;
        var image_dimension_y = 376;
        var scaled_width = 0;
        var scaled_height = 0;
        var x1 = 0;
        var y1 = 0;
        var x2 = 0;
        var y2 = 0;
        var current_image = null;
        var aspX = 1;
        var aspY = 1;
        var file_display_area = null;
        var ias = null;
        var jcrop_api;
        var bottom_html = "<input type='file' id='fileInput' name='files[]'/><canvas id='myCanvas' style='display:none;'></canvas><div id='modal'></div><div id='preview'><div class='buttons'><div class='cancel'></div><div class='ok'></div></div></div>";
        $('body').append(bottom_html);

        //add click to element
        this.click(function() {
            aspX = 376; // $("#Test").width();
            aspY = 376; //$("#Test").height();
            file_display_area = $("#Test");
            $('#fileInput').click();
        });

        $(document).ready(function() {
            //capture selected filename
            $('#fileInput').change(function(click) {
                imageUpload($('#preview').get(0));
                // Reset input value
               // alert($(this).val());
                 $(this).val("");
            });

            //ok listener
            $('.ok').click(function() {
                preview();
                $('#preview').delay(100).hide();
                $('#modal').hide();
                jcrop_api.destroy();
                reset();
            });

            //cancel listener
            $('.cancel').click(function(event) {
                $('#preview').delay(100).hide();
                $('#modal').hide();
                jcrop_api.destroy();
                $('#Test').hide();
                $('#fileName').empty().html('<span class="format">Format acceptable: JPG, PNG; File size limit: 5MB</span>');
                $('#crop_image_hidden-error').show();
                $('#deleteImage').hide();
                reset();
            });
        });

        function reset() {
            scaled_width = 0;
            scaled_height = 0;
            x1 = 0;
            y1 = 0;
            x2 = 0;
            y2 = 0;
            current_image = null;
            aspX = 1;
            aspY = 1;
            file_display_area = null;
        }

        function imageUpload(dropbox) {
            var file = $("#fileInput").get(0).files[0];
            $("#filetype").empty();
            $('#fileName').empty().append(file.name);
            //var file = document.getElementById('fileInput').files[0];

            if (file.type != "image/jpeg" && file.type != "image/png") {
                $("#filetype").empty().append('Please Enter Valid Image File (jpeg, png)');
                $('#Test').empty();
                $('#crop_image_hidden').val('');
                $('#deleteImage').hide();
                return false;
            }
            if (file.size > 1048576) {
                $("#filetype").empty().append('Uploaded file size not permitted (max 1 MB)');
                $('#Test').empty();
                $('#crop_image_hidden').val('');
                $('#deleteImage').hide();
                return false;
            }
            $('#crop_image_hidden-error').hide();


            var imageType = /image.*/;

            if (file.type.match(imageType)) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    // Clear the current image.
                   $('#photo').remove();

                    // Create a new image with image crop functionality
                    current_image = new Image();
                    current_image.src = reader.result;
                    var txtOriginalb5 = $("[id$='txtOriginalb4']");
                   
                    txtOriginalb5.val(reader.result);
                  //  $('#crop-image-hidden-mobile').val("jpg");
                   // txtOriginalb5.val(reader.result);

                    current_image.id = "photo";
                    current_image.style['maxWidth'] = image_dimension_x + 'px';
                    current_image.style['maxHeight'] = image_dimension_y + 'px';
                    current_image.onload = function() {
                        // Calculate scaled image dimensions
                        if (current_image.width > image_dimension_x || current_image.height > image_dimension_y) {
                            if (current_image.width > current_image.height) {
                                scaled_width = image_dimension_x;
                                scaled_height = image_dimension_x * current_image.height / current_image.width;
                            }
                            if (current_image.width < current_image.height) {
                                scaled_height = image_dimension_y;
                                scaled_width = image_dimension_y * current_image.width / current_image.height;
                            }
                            if (current_image.width == current_image.height) {
                                scaled_width = image_dimension_x;
                                scaled_height = image_dimension_y;
                            }
                        }
                        else {
                            scaled_width = current_image.width;
                            scaled_height = current_image.height;
                        }


                        // Position the modal div to the center of the screen
                        $('#modal').css('display', 'block');
                        var window_width = $(window).width() / 2 - scaled_width / 2 + "px";
                        var window_height = $(window).height() / 2 - scaled_height / 2 + "px";

                        // Show image in modal view
                        $("#preview").css("top", window_height);
                        $("#preview").css("left", window_width);
                        $('#preview').show(500);


                        // Calculate selection rect
                        var selection_width = 0;
                        var selection_height = 0;

                        var max_x = Math.floor(scaled_height * aspX / aspY);
                        var max_y = Math.floor(scaled_width * aspY / aspX);


                        if (max_x > scaled_width) {
                            selection_width = scaled_width;
                            selection_height = max_y;
                        }
                        else {
                            selection_width = max_x;
                            selection_height = scaled_height;
                        }

                        ias = $(this).Jcrop({
                            onSelect: showCoords,
                            onChange: showCoords,
                            bgColor: 'white',
                            bgOpacity: 1,
                            aspectRatio: aspX / aspY,
                            setSelect: [0, 0, selection_width, selection_height]
                        }, function() {
                            jcrop_api = this;
                        });
                    }

                    // Add image to dropbox element
                    dropbox.appendChild(current_image);
                }
                reader.readAsDataURL(file);
                //txtOriginalb5.val(reader.result);
            } else {
                dropbox.innerHTML = "File not supported!";
            }
        }

        function showCoords(c) {
            //alert (y2)
            x1 = c.x;
            y1 = c.y;
            x2 = c.x2;
            y2 = c.y2;
        }

        function preview() {
            // Set canvas
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');

            // Delete previous image on canvas
            context.clearRect(0, 0, canvas.width, canvas.height);

            // Set selection width and height
            var sw = x2 - x1;
            var sh = y2 - y1;


            // Set image original width and height
            var imgWidth = current_image.naturalWidth;
            var imgHeight = current_image.naturalHeight;

            // Set selection koeficient
            var kw = imgWidth / $("#preview").width();
            var kh = imgHeight / $("#preview").height();

            // Set canvas width and height and draw selection on it
            canvas.width = aspX;
            canvas.height = aspY;
            context.drawImage(current_image, (x1 * kw), (y1 * kh), (sw * kw), (sh * kh), 0, 0, aspX, aspY);

            // Convert canvas image to normal img
            var dataUrl = canvas.toDataURL();
            var imageFoo = document.createElement('img');
            //var imageData = document.createElement('input');
            //imageData.setAttribute("id", "txtImageData");
            //imageData.setAttribute("type", "text");
            //imageData.setAttribute("value", dataUrl);

            var txtb5 = $("[id$='txtb4']");
            // alert(txtb5.val());
            txtb5.val(dataUrl);
            // alert(txtb5.val());

            imageFoo.src = dataUrl;
            $('#crop_image_hidden').val(dataUrl);
            $('#Test').show();
            $('#deleteImage').show();
            // Append it to the body element
            $('#preview').delay(100).hide();
            $('#modal').hide();
            file_display_area.html('');
            file_display_area.append(imageFoo);
            // file_display_area.append(imageData);

        }

        $(window).resize(function() {
            // Position the modal div to the center of the screen
            var window_width = $(window).width() / 2 - scaled_width / 2 + "px";
            var window_height = $(window).height() / 2 - scaled_height / 2 + "px";

            // Show image in modal view
            $("#preview").css("top", window_height);
            $("#preview").css("left", window_width);
        });
    }
} (jQuery));

  