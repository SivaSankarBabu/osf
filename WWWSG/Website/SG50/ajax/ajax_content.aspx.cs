﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using System.Configuration;
using System.IO;
using Sitecore.Data.Items;
using System.Globalization;
using Sitecore.Security;
using System.Security.Cryptography;
using System.Text;
using Sitecore;
using Sitecore.Globalization;
using Sitecore.SecurityModel;
using Sitecore.Resources.Media;
using Sitecore.Links;
using Sitecore.Diagnostics;
using Sitecore.Collections;
using System.Data;
using System.Net.Mail;
using System.Web.Services;
using System.Net;
using Sitecore.Publishing;

namespace PG
{

    /// <summary>
    /// Summary description for Pghomesublayout_bottomSublayout
    /// </summary>
    public partial class ajax_content : System.Web.UI.Page
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        private static Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
        Item itmcontext = Sitecore.Context.Item;
        public static string userStoriesParentItemId = "{2E56CD94-2A02-4D4E-B53A-8B308B29F375}", timeCapsuleItemsFolderId = "{E3387D22-9A6E-4FBF-A018-28F7EB755672}",
            voteForItemTempId = "{8EB27D3A-D2B3-442A-A7BF-AAF462829CF3}", voteCaptionForNewItem = "I want this in!", newItemCategory = "New", strAll = "All", newItemStatus = "0",
            newItemVotes = "0", suggestedItemsFolderId = "{71F21422-2FF0-456B-9AB6-58877C42AEA2}", suggestedItemsFolderTemplateId = "{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}", timeCapsuleItemTemplateId = "{06816772-B378-4433-8D72-E06DD1C398C2}", suggestNewItemTemplateId = "{1A0C6620-7F02-4E01-83FA-DB02D4362D4B}";
        public static List<string> listItems = new List<string>();

        public string returndata = "";

        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            // FillStories();

            DataTable dtrepEvents = new DataTable();
            dtrepEvents.Columns.Add("PName", typeof(string));

            dtrepEvents.Columns.Add("ImageUrl", typeof(string));
            dtrepEvents.Columns.Add("ImageText", typeof(string));
            dtrepEvents.Columns.Add("item", typeof(Item));
            dtrepEvents.Columns.Add("href", typeof(string));
            dtrepEvents.Columns.Add("Id", typeof(string));
            int cntr = int.Parse(Microsoft.Security.Application.Encoder.HtmlEncode(Request.QueryString["i"].ToString()));
            string SortType = Microsoft.Security.Application.Encoder.HtmlEncode(Request.QueryString["SortType"].ToString());
            Item itmPress = SG50Class.web.GetItem("{655A7E18-1AE6-447E-8A1F-C9A4768126B7}");
            int skip = 9;
            if (cntr > 2)
                skip = (cntr - 1) * 9;

            IEnumerable<Item> itmIEnumPressReleases = null;
            IEnumerable<Item> itmIEnumPressReleases2 = null;
            IEnumerable<Item> nitems = null;
            if (SortType == strAll)
            {

                //itmIEnumPressReleases2 = from a in itmPress.Axes.GetDescendants().OrderByDescending(x => x.Statistics.Created)
                //                        where
                //                        a.TemplateID.ToString().Equals(SG50Class.str_PGStory_Template_ID)
                //                        select a;
                itmIEnumPressReleases = ((IEnumerable<Item>)Session["RandomScroll"]);

                string[] lstids = Session["ScrollIds"].ToString().Split(',');
                for (int k = 0; k < lstids.Count(); k++)
                {
                    itmIEnumPressReleases = itmIEnumPressReleases.Where(x => x.ID.ToString() != lstids[k]).ToList();
                }

                itmIEnumPressReleases = itmIEnumPressReleases.Take(9).ToArray().Distinct();

                string id = Session["ScrollIds"].ToString();
                foreach (var item in itmIEnumPressReleases)
                {
                    id += item.ID.ToString() + ",";
                }
                Session["ScrollIds"] = id;
                nitems = from a in itmPress.Axes.GetDescendants()
                         where
                         a.TemplateID.ToString().Equals("{7E32B7AE-808C-4406-8DA3-49C3D104A2F1}")
                         select a;

            }
            else if (SortType == "Recent" && skip <= 9)
            {

                itmIEnumPressReleases2 = from a in itmPress.Axes.GetDescendants().OrderByDescending(x => x.Statistics.Created)
                                         where
                                         a.TemplateID.ToString().Equals(SG50Class.str_PGStory_Template_ID)

                                         select a;

                itmIEnumPressReleases = itmIEnumPressReleases2.Skip(skip).Take(9).ToArray();

                nitems = from a in itmPress.Axes.GetDescendants()
                         where
                         a.TemplateID.ToString().Equals("{7E32B7AE-808C-4406-8DA3-49C3D104A2F1}")
                         orderby
                         a.Statistics.Created descending
                         select a;
            }
            else if (SortType == "Popular")
            {

                itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants().Skip(skip).Take(9).ToArray()
                                        where
                                        a.TemplateID.ToString().Equals(SG50Class.str_PGStory_Template_ID)
                                        orderby
                                        a.Fields["PopularCount"].ToString() descending
                                        select a;

                nitems = from a in itmPress.Axes.GetDescendants()
                         where
                         a.TemplateID.ToString().Equals("{7E32B7AE-808C-4406-8DA3-49C3D104A2F1}")
                         orderby
                         a.Fields["PopularCount"].ToString() descending
                         select a;

            }
            //IEnumerable<Item> itmIEnumPressReleases = from a in itmPress.Axes.GetDescendants().Skip(skip).Take(9).ToArray()
            //                                          where
            //                                          a.TemplateID.ToString().Equals("{7E32B7AE-808C-4406-8DA3-49C3D104A2F1}")
            //                                          select a;

            //IEnumerable<Item> nitems = from a in itmPress.Axes.GetDescendants()
            //                           where
            //                           a.TemplateID.ToString().Equals("{7E32B7AE-808C-4406-8DA3-49C3D104A2F1}")
            //                           select a;

            returndata = "<div id='totitems' style='display:none'>" + nitems.Count() + "</div>";
            returndata += "<div id='curitems' style='display:none'>" + itmIEnumPressReleases.Count() + "</div>";

            // Response.Write(cntr+"pagescroll");

            // IEnumerable<Item> itmIEnumPressReleases2 = itmIEnumPressReleases.Select((x, i) => new { Item = x, Index = i }).GroupBy(x => x.Index/groupSize, x => x.Item);
            foreach (Item a in itmIEnumPressReleases)
            {

                DataRow drrepEvents;
                drrepEvents = dtrepEvents.NewRow();

                drrepEvents["ImageText"] = a.Fields["Pioneers Description"];
                drrepEvents["item"] = a;
                //drrepEvents["href"] = LinkManager.GetItemUrl(a).ToString();"
                drrepEvents["href"] = "/SG50/Pioneer Generation" + LinkManager.GetItemUrl(a).ToString().Replace("/SG50", "").Replace("/en", "").Replace("/Pioneer Generation", "");

                drrepEvents["Id"] = a.ID;
                drrepEvents["PName"] = a.Fields["Pioneers Name"];

                Sitecore.Collections.ChildList ccc = web.GetItem(a.ID).GetChildren();

                string curntItem = a.ID.ToString();

                string path = a.Paths.Path.ToString();
                Item ImageItemHumbnail = web.GetItem(path + "/Images/Thumbnail");
                string url = string.Empty;
                if (ImageItemHumbnail != null)
                {
                    Sitecore.Data.Fields.ImageField image = ImageItemHumbnail.Fields["Image"];

                    if (image.MediaItem != null)
                    {
                        MediaUrlOptions options = new MediaUrlOptions();
                        options.BackgroundColor = System.Drawing.Color.White;
                        url = StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem, options));
                        //drrepEvents["ImageUrl"] = url;// stringArray[ImagesCount.Count - 1];
                    }
                }
                if (url != "")
                {
                    drrepEvents["ImageUrl"] = url;
                }
                else
                {
                    drrepEvents["ImageUrl"] = "/SG50/images/PG/logo.png";
                }

                dtrepEvents.Rows.Add(drrepEvents);

            }

            if (SortType != "Recent")
            {
                repStoryList.DataSource = dtrepEvents;
                repStoryList.DataBind();
            }
            else if (SortType == "Recent" && skip <= 9)
            {
                repStoryList.DataSource = dtrepEvents;
                repStoryList.DataBind();
            }

            if (!IsPostBack)
            {
                listItems = new List<string>();
            }
        }

        #region Load More Campgian User Stories

        [WebMethod]
        public static string LoadMoreStories(string skipCount, string nextCount)
        {
            try
            {
                Item UserItem = web.GetItem(userStoriesParentItemId);
                if (UserItem != null)
                {
                    StringBuilder sbBannerText = new StringBuilder();
                    if (System.Convert.ToInt32(skipCount) != UserItem.GetChildren().Count)
                    {
                        IEnumerable<Item> items = UserItem.GetChildren().Skip(System.Convert.ToInt32(skipCount)).Take(System.Convert.ToInt32(nextCount));
                        if (items != null && items.Count() > 0)
                        {
                            foreach (var item in items)
                            {
                                if (item.Versions.Count > 0)
                                {
                                    sbBannerText.Append("<li>");
                                    sbBannerText.Append("<div class='Gallery-box'>");
                                    string BannermediaUrl = string.Empty;
                                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)item.Fields["Thumb Image"]);
                                    if (imgField != null && !string.IsNullOrEmpty(imgField.Src))
                                    {
                                        BannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                    }
                                    sbBannerText.Append("<img src='" + BannermediaUrl + "' alt='No Image' /><div class='gtooltip'>");
                                    if (!string.IsNullOrEmpty(item["Content"]))
                                        sbBannerText.Append("<span>" + item["Content"] + "</span><div class='name'>");
                                    if (!string.IsNullOrEmpty(item["Name"]))
                                        sbBannerText.Append(item["Name"] + "</div>");
                                    //sbBannerText.Append("<a href='" + item["Button URL"] + "'>" + item["Button Text"] + "</a></div></li>");
                                    string url = string.Empty;
                                    url = LinkManager.GetItemUrl(item).ToString();
                                    sbBannerText.Append("<a href='" + url + "'>" + item["Button Text"] + "</a></div></div>");
                                }
                            }
                            return sbBannerText.ToString();
                        }
                        else return string.Empty;
                    }
                    else { return string.Empty; }
                }
                else return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region #SG50 Time Capsule

        /* Getting Time Capsule Items in Load More Button Click and li tag click */
        [WebMethod]
        public static string LoadMoreTimeCapsuleItems(string skipCount, string nextCount, string categoryType)
        {
            try
            {
                Item timeCapsuleFolder = web.GetItem(timeCapsuleItemsFolderId);
                if (timeCapsuleFolder != null)
                {
                    StringBuilder sbTimeCapusuleItems = new StringBuilder();
                    if (System.Convert.ToInt32(skipCount) != timeCapsuleFolder.GetChildren().Count)
                    {
                        IEnumerable<Item> timeCapsuleItems = null;

                        if (categoryType.Contains(strAll))
                            timeCapsuleItems = timeCapsuleFolder.Axes.GetDescendants().Where(x => x.TemplateID.ToString() == timeCapsuleItemTemplateId || (x.TemplateID.ToString() == suggestNewItemTemplateId && x.Fields["Is_Active"].Value == 1.ToString())).OrderBy(x => x.Name).ToList().Skip(System.Convert.ToInt32(skipCount)).Take(System.Convert.ToInt32(nextCount));
                        else if (categoryType.Contains("New"))
                            timeCapsuleItems = web.GetItem(suggestedItemsFolderId).GetChildren().Where(x => x.Fields["Is_Active"].Value == 1.ToString()).Select(x => x).Skip(System.Convert.ToInt32(skipCount)).Take(System.Convert.ToInt32(nextCount));
                        else
                            timeCapsuleItems = timeCapsuleFolder.GetChildren().Where(x => x.TemplateID.ToString() != suggestedItemsFolderTemplateId && categoryType.Contains(x.Fields["Category"].ToString())).Select(x => x).ToList().Skip(System.Convert.ToInt32(skipCount)).Take(System.Convert.ToInt32(nextCount));

                        if (timeCapsuleItems != null && timeCapsuleItems.Count() > 0)
                        {
                            foreach (var item in timeCapsuleItems)
                            {
                                if (item.Versions.Count > 0)
                                {
                                    sbTimeCapusuleItems.Append("<li>");
                                    sbTimeCapusuleItems.Append("<div class='boxcontainer'>");
                                    string BannermediaUrl = string.Empty;
                                    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)item.Fields["Image"]);
                                    if (imgField != null && !string.IsNullOrEmpty(imgField.Src))
                                    {
                                        BannermediaUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                                    }
                                    sbTimeCapusuleItems.Append("<div class='thumbIcon'><img src='" + BannermediaUrl + "' /></div>");
                                    if (!string.IsNullOrEmpty(item["Title"]))
                                        sbTimeCapusuleItems.Append("<h4>" + item["Title"] + "</h4>");
                                    if (!string.IsNullOrEmpty(item["Description"]))
                                        sbTimeCapusuleItems.Append("<div class='caption'>" + item["Description"] + "</div>");
                                    sbTimeCapusuleItems.Append("<div class='votesblock'><div class='votes'><a href='javascript:void(0)'>&nbsp; </a><div class='votecount'>" + item["votes"] + " likes</div></div><div class='grabItem'><a href='javascript:void(0)' id='ShowVote'><img id='imgIwnatThisIn' alt='Image' src='/SG50/images/Capsule/iwantthis.png'></a></div><div id='divItemDetails' style='display: none'><input type='hidden' value='" + item.ID + "' id='itemId'><span id='itemName'>" + item.Name + "</span></div></div></div></li>");
                                }
                            }
                            return sbTimeCapusuleItems.ToString();
                        }
                        else return string.Empty;
                    }
                    else { return string.Empty; }
                }
                else return string.Empty;
            }
            catch (Exception exSub1)
            {
                throw;
            }
        }

        /* Add New Item to the Gallery of Items */
        [WebMethod]
        public static string SuggestNewItem(string personName, string eMail, string itemName, string description)
        {
            try
            {
                if (!string.IsNullOrEmpty(personName) && !string.IsNullOrEmpty(eMail) && !string.IsNullOrEmpty(itemName))
                {
                    TemplateItem suggestNewItemTemplate = master.GetItem("/sitecore/templates/SG50/SuggestTimeCapsuleItem");
                    Item timeCapsuleItemsFolder = master.GetItem(suggestedItemsFolderId);
                    string newItemName = itemName + "-" + DateTime.Now.ToString("yyMMdd") + "-" + DateTime.Now.ToString("hhmmss");
                    Item newForm = null;
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        newForm = timeCapsuleItemsFolder.Add(newItemName, suggestNewItemTemplate);
                        newForm.Editing.BeginEdit();
                        newForm.Fields["Name"].Value = personName;
                        newForm.Fields["EMail"].Value = eMail;
                        newForm.Fields["Title"].Value = itemName;
                        newForm.Fields["votes"].Value = 0.ToString();
                        newForm.Fields["Button Text"].Value = voteCaptionForNewItem;
                        newForm.Fields["Category"].Value = newItemCategory;
                        newForm.Fields["Is_Active"].Value = 0.ToString();
                        newForm.Fields["Description"].Value = description;
                        newForm.Editing.EndEdit();
                        var item = newForm;

                        if (item != null)
                        {
                            var source = master;
                            var target = Sitecore.Data.Database.GetDatabase("web");
                            var options = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now) { RootItem = item, Deep = true };
                            var publisher = new Publisher(options);
                            publisher.PublishAsync();
                            return 1.ToString();
                        }
                    }
                    return 0.ToString();
                }
                else { return 0.ToString(); }
            }
            catch (Exception ex)
            {
                return ex.InnerException.ToString();
            }
        }

        /* Vote for an Existing Item */
        [WebMethod]
        public static int VoteForExistingItem(string name, string eMail, string description, string parentItemId)
        {
            try
            {
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(eMail) && !string.IsNullOrEmpty(description))
                {
                    Item newForm = null;
                    Sitecore.Data.Items.TemplateItem template = master.GetItem("/sitecore/templates/SG50/VoteForAnItem");
                    Item parentItem = master.GetItem(parentItemId);
                    string itemName = name + DateTime.Now.ToString("yyMMdd") + "-" + DateTime.Now.ToString("hhmmss");
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        newForm = parentItem.Add(itemName.ToString(), template);

                        newForm.Editing.BeginEdit();
                        newForm.Fields["Name"].Value = name;
                        newForm.Fields["EMail"].Value = eMail;
                        newForm.Fields["Description"].Value = description;
                        newForm.Editing.EndEdit();

                        var item = newForm;
                        if (item != null)
                        {
                            var source = master;
                            var target = Sitecore.Data.Database.GetDatabase("web");
                            var options = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now) { RootItem = item, Deep = true, };
                            var publisher = new Publisher(options);
                            publisher.PublishAsync();
                            return 1;
                        }
                        else { return 0; }
                    }
                }
                else { return 0; }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /* function to Like an Item and Increment the Likes Count 1*/
        [WebMethod]
        public static bool LikeItem(string itemId, string currentNoOfLikes)
        {
            try
            {
                if (!string.IsNullOrEmpty(itemId))
                {
                    Item likedItem = master.GetItem(itemId);
                    string newLikesCount = string.Empty;
                    if (!string.IsNullOrEmpty(currentNoOfLikes))
                        newLikesCount = (System.Convert.ToInt32(currentNoOfLikes.Replace("likes", string.Empty).Trim()) + 1).ToString();
                    else
                        newLikesCount = (0 + 1).ToString();

                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        likedItem.Editing.BeginEdit();
                        if (!listItems.Contains(itemId + " " + HttpContext.Current.Session.SessionID))
                        {
                            listItems.Add(itemId + " " + HttpContext.Current.Session.SessionID);
                            likedItem.Fields["Votes"].Value = newLikesCount.ToString();
                            likedItem.Editing.EndEdit();
                            var item = likedItem;
                            if (item != null)
                            {
                                var source = master;
                                var target = Sitecore.Data.Database.GetDatabase("web");
                                var options = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now) { RootItem = item, Deep = false };
                                var publisher = new Publisher(options);
                                publisher.PublishAsync();
                                return true;
                            }
                            else
                            {
                                listItems.Remove(itemId + " " + HttpContext.Current.Session.SessionID); return false;
                            }
                        }
                        else return false;
                    }

                }
                else { return false; }
            }
            catch (Exception msg)
            {
                throw;
            }
        }

        /* Get Item Count as per the Category */
        [WebMethod]
        public static string GetItemsCountCategory(string itemCat)
        {
            try
            {
                Item timeCapsuleFolder = web.GetItem(timeCapsuleItemsFolderId), suggestedItemsFolderItem = web.GetItem(suggestedItemsFolderId);
                if (!string.IsNullOrEmpty(itemCat))
                {
                    IEnumerable<Item> timeCapsuleItems = null;
                    if (itemCat.Contains(strAll))
                        timeCapsuleItems = timeCapsuleFolder.Axes.GetDescendants().Where(x => x.TemplateID.ToString() == timeCapsuleItemTemplateId || (x.TemplateID.ToString() == suggestNewItemTemplateId && x.Fields["Is_Active"].Value == 1.ToString())).ToList();
                    //timeCapsuleItems = timeCapsuleFolder.GetChildren().Where(x => x.TemplateID.ToString() != "{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}").Select(x => x);
                    else if (itemCat.Contains(newItemCategory))
                        timeCapsuleItems = suggestedItemsFolderItem.GetChildren().Where(x => x.Fields["Category"].Value == newItemCategory && x.Fields["Is_Active"].Value == 1.ToString()).Select(x => x);
                    else
                        timeCapsuleItems = timeCapsuleFolder.GetChildren().Where(x => x.TemplateID.ToString() != suggestedItemsFolderTemplateId && itemCat.Contains(x.Fields["Category"].ToString())).Select(x => x).ToList();
                    return timeCapsuleItems.Count().ToString();
                }
                else { return string.Empty; }
            }
            catch (Exception) { throw; }
        }

        #endregion

        #region Get Gallery items for Mobile Device

        [WebMethod]
        public static List<MobileApps> MobileAPPS()
        {
            try
            {
                List<MobileApps> allApps = new List<MobileApps>();
                IEnumerable<Item> Pulsechild = web.GetItem("{72FA46BB-27D4-4ED3-95A7-293BF6125834}").GetChildren();
                Sitecore.Collections.ChildList AppsCount = web.GetItem("{72FA46BB-27D4-4ED3-95A7-293BF6125834}").GetChildren();
                int count = 1;

                foreach (Item a in Pulsechild)
                {
                    MobileApps app = new MobileApps();
                    app.SNO = count.ToString();
                    app.Title = a.Fields["Title"].ToString();
                    app.Description = a.Fields["Description"].ToString();
                    app.Version = a.Fields["version"].ToString();

                    Sitecore.Data.Fields.ImageField mainImgField = ((Sitecore.Data.Fields.ImageField)a.Fields["Image"]);
                    if (mainImgField != null && !string.IsNullOrEmpty(mainImgField.Src) && mainImgField.MediaItem != null)
                    {
                        string mainimgSrc = mainImgField.Src;
                        if (!string.IsNullOrEmpty(mainimgSrc))
                        {
                            app.ImageSrc = mainimgSrc;
                        }
                    }
                    count++;
                    app.AndroidURL = a.Fields["Android APP URL"].ToString();
                    app.iPhoneUrl = a.Fields["Iphone APP URL"].ToString();
                    allApps.Add(app);
                }
                return allApps;
            }
            catch (Exception)
            {
                return new List<MobileApps>();
            }
        }


        #endregion

    }


    #region Models

    #region Gallery Landing Page Models

    public class MobileApps
    {
        public string SNO { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Version { get; set; }
        public string ImageSrc { get; set; }
        public string AndroidURL { get; set; }
        public string iPhoneUrl { get; set; }
    }

    #endregion

    #endregion

}





