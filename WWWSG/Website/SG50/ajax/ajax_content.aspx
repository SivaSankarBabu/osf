﻿<%@ Page Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"  Inherits="PG.ajax_content" CodeFile="~/SG50/ajax/ajax_content.aspx.cs"%>
 <div class="custom-container">  
 
 <ul class="sortlist-container">
  		<asp:Repeater ID="repStoryList" runat="server">
                        <ItemTemplate>
                            <li>
                                <div class="img">
                                    <asp:Image runat="server" ID="repImg" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ImageUrl") %>' />
                                    <div class="caption">
                                        <p>
                                            <sc:Text ID="Text1" Field="Pioneers Description" runat="server" Item='<%# DataBinder.Eval(Container.DataItem, "item") %>' /><span><%# DataBinder.Eval(Container.DataItem, "PName") %></span>
                                        </p>
                                        <%--<a class="sg-button-sm" href="#">more</a>--%>
                                        <asp:HyperLink ID="hf" Text="more" CssClass="sg-button-sm" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "href") %>'></asp:HyperLink>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
  
  </ul>
 
 <div class="clearfix"></div>
 </div>


<%=returndata%>