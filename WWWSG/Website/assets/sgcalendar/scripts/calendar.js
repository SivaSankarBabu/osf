/// <reference path="../js/jquery-3.4.0.js" />
'use strict';

var SGF = window.SGF || {};
var ViewmoreClick = false, mobileMonClick = false;

SGF.calendar = {
    isInit: false,
    init: function init() {
        SGF.calendar.initCustomSelect();
        SGF.calendar.initSubmitEventPopup();
        SGF.calendar.initMonthFilter();
        SGF.calendar.initFilter();
        SGF.calendar.initMonthFilterMobile();
        SGF.calendar.initEventDetailPopup();
        SGF.calendar.initHomeSlider();
        SGF.calendar.initWhatsappUrl();
        SGF.calendar.initLazyload();
        $(window).on('load resize', function () {
            SGF.calendar.initEvalHeight();
        });
        SGF.calendar.isInit = true;
    },

    initWhatsappUrl: function initWhatsappUrl() {
        $(window).on('load resize', function () {
            var whatsappIcons = $('[data-action="share/whatsapp/share"]');
            var isMobile = isMobileDevice();
            whatsappIcons.each(function (index, icon) {
                $(icon).attr('href', $(icon).attr(isMobile ? 'data-mobile-href' : 'data-desktop-href'));
            });
        });
    },

    initLazyload: function initLazyload() {
        //window.lazyLoad = new LazyLoad();
    },


    initFilter: function initFilter() {
        $('.btn-filter').on('click', function (e) {
            e.preventDefault();
            $(this).parents('.section-filter').toggleClass('active');
            $('.filter-bg').toggleClass('active');
        });

        $('.filter-bg').on('click', function (e) {
            e.preventDefault();
            $('.section-filter').removeClass('active');
            $(this).removeClass('active');
        });
    },

    initCustomSelect: function initCustomSelect() {
        function initSelect() {
            if ($(window).width() > 767 && $('select').length) {
                $('select:visible').niceSelect();
                if ($('select:visible').length) {
                    $(document).on('click.nice_select', '.nice-select .option:not(.disabled)', function (e) {
                        var $option = $(this);
                        var $dropdown = $option.closest('.nice-select');
                        var current = $option.data('value');
                        var first = $dropdown.prev('select').find('option:first-child').val();

                        if (current !== first) {
                            $dropdown.addClass('active');
                        } else {
                            $dropdown.removeClass('active');
                        }
                    });
                }
            } else {
                if ($('select').length) {
                    $('select').niceSelect('destroy');
                }
            }
        }

        initSelect();

        $(window).on('resize', function () {
            initSelect();
        });
    },

    initSubmitEventPopup: function initSubmitEventPopup() {
        $('#submitEventGuides').hide();
        $('.block-event.block-add .btn-add, .section-head .link').click(function () {
            $('#submitEventGuides').fadeIn(300);
            $('body').addClass('hideScrool');
        });
        $('.submitEventHeader a').click(function () {
            $('#submitEventGuides').fadeOut(300);
            $('body').removeClass('hideScrool');
        });
        $('#submitEventGuides').on('click', function (e) {
            if (e.target == this) {
                $('#submitEventGuides').fadeOut(300);
                $('body').removeClass('hideScrool');
            }
        });
    },

    initMonthFilter: function initMonthFilter() {
        if ($('.section-calendar').length) {
            $('.section-calendar').on('click', '.block-event.month .btn-action', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).toggleClass('collapsed');
                $(this).find('.text').html($(this).is('.collapsed') ? 'collapse' : 'expand');
                //
                var wrap = $(this).parents('.month');
                var target = wrap.attr('data-month');
                var list = wrap.siblings('[data-month="' + target + '"]');
                list.toggleClass('hidden');
                //
                SGF.calendar.initEvalHeight();
                if ($(this).is('.collapsed') && !ViewmoreClick && !mobileMonClick) {
                    console.log('GA Tracking ' + $(this).parent().parent().find('.wrap-info .month-large').text());
                    var monContent = $(this).parents('.inner').find('.wrap-info');
                    console.log(monContent.find('.month-large').text() + '_' + monContent.find('.year-large').text());
                    ga('send', 'event', { eventCategory: 'SG Event Listing', eventAction: 'Expand', eventLabel: monContent.find('.month-large').text() + '_' + monContent.find('.year-large').text() });

                }
                else { console.log('Now no tracking'); }
            });

            $('.section-calendar').on('click', '.block-event.month', function (e) {
                e.preventDefault();
                $(this).find('.btn-action').trigger('click');

            });
        }
    },

    initMonthFilterMobile: function initMonthFilterMobile() {
        var slider = null;

        function initSlider() {
            if (!slider) {
                if ($('.wrap-mobile-month:visible')) {
                    if ($('.wrap-mobile-month:visible .list :first').children().length > 2) {
                        var display = 3;
                        var width = $('.wrap-mobile-month:visible').width() / display;
                        slider = $('.wrap-mobile-month:visible .list').bxSlider({
                            //auto: ($('.wrap-mobile-month:visible .list').length < 3) ? false : true,
                            minSlides: 3, maxSlides: display, moveSlides: display, slideWidth: width, controls: true, pager: false,
                            prevText: '<i class="fa fa-caret-left"></i>',
                            nextText: '<i class="fa fa-caret-right"></i>'
                        });
                    }
                }
            } else {
                if ($(window).width() > 767) {
                    slider.destroySlider();
                    slider = null;
                } else {
                    slider.reloadSlider();
                }
            }
        }

        if ($('.wrap-mobile-month:visible').length > 0) {
            initSlider();
        }

        $(window).on('resize', function () {
            initSlider();
        });

        //handle click month on slider
        $('.wrap-mobile-month').on('click', '.item a', function (e) {
            mobileMonClick = true;
            e.preventDefault();
            $('.item a').removeClass('active');
            $(this).addClass('active');
            var target = $(this).attr('data-month');
            var list = $('.wrap-calendar .block-event').filter('[data-month="' + target + '"]');
            $('.wrap-calendar .block-event').filter(':not(.block-add)').filter(':not(.block-more)').filter(':not(.month)').addClass('hidden');
            if (list.length) {
                $('.wrap-calendar .block-event.month').filter('[data-month="' + target + '"]').find('.btn-action').trigger('click');
                mobileMonClick = false;
            }
        });

        //handle sticky
        $(window).on('scroll', function () {
            if ($('.wrap-mobile-month:visible').length > 0) {
                var top = $(window).scrollTop();
                var sticky = $('.wrap-mobile-month');
                var anchor = $('.wrap-calendar').offset().top - sticky[0].clientHeight - $('header')[0].clientHeight;
                if (top > anchor && !sticky.is('.sticky')) {
                    sticky.addClass('sticky');
                    $('.wrap-calendar').addClass('anchor');
                }
                if (top < anchor && sticky.is('.sticky')) {
                    sticky.removeClass('sticky');
                    $('.wrap-calendar').removeClass('anchor');
                }
            }
        });
    },

    initEventDetailPopup: function initEventDetailPopup() {
        if ($('.section-calendar').length) {
            $('.section-calendar').on('click', '.block-event a.inner', function (e) {
                e.preventDefault();

                $("#eventDetail").find('.wrap-thumb .thumb').attr('src', '');

                $("#eventDetail").fadeIn(300); $("body").addClass("hideScrool");
                var wrap = $(this).parents('.block-event'), isDisable = wrap.is('.disable'), imagePath = wrap.attr('data-img'), altText = wrap.attr('data-alt');
                Get_Event_Details(wrap, '#eventDetail');

                if (isDisable) {
                    $("#eventDetail").find('.eventDetailPopupContainer').addClass('disable');
                } else {
                    $("#eventDetail").find('.eventDetailPopupContainer').removeClass('disable');
                }
                if (imagePath) {
                    //					alert(imagePath);
                    if (wrap.hasClass('disable')) {
                        $("#eventDetail").find('.wrap-thumb .thumb').addClass('gray-img').attr({ 'src': imagePath, 'alt': altText });
                    }
                    else { $("#eventDetail").find('.wrap-thumb .thumb').removeClass('gray-img').attr({ 'src': imagePath, 'alt': altText }); }
                }
            });

            $('.eventDetailHeader a, .eventDetailHeaderMobile .close').on('click', function () {
                $('#eventDetail').fadeOut(300);
                $('body').removeClass('hideScrool');
            });

            $('#eventDetail').on('click', function (e) {
                if (e.target == this) {
                    $('#eventDetail').fadeOut(300);
                    $('body').removeClass('hideScrool');
                }
            });
        }

        /* Get event details in Popup */
        function Get_Event_Details(srcCtrl, targetCtrl) {
            if ($(srcCtrl).length > 0 && $(targetCtrl).length > 0) {

                var _objTar = $(targetCtrl), _objSrc = $(srcCtrl), hideinfo = _objSrc.find('.hide-event-info')
                _objTar.find('.event-type').html(_objSrc.find('.event-type').html());
                _objTar.find('.event-title').html(_objSrc.find('.event-title').html());
                _objTar.find('.place').html('').html(_objSrc.find('.place').html() + "<a href='" + hideinfo.find('.maps-link').val() + "' target='_blank' class='link'>View on Google Maps</a>").find('a.link').attr('onclick', "ga('send', 'event', { eventCategory: 'SG Event Detail', eventAction: 'View on google map', eventLabel: '" + _objSrc.find('.event-title').text() + "'})");
                _objTar.find('.content p').html(hideinfo.find('.detail-desc').val());

                if (hideinfo.find('.website-url').val() != '' && hideinfo.find('.website-url').val() != 'javascript:void(0)')
                    _objTar.find('.content a').attr({ 'href': hideinfo.find('.website-url').val(), 'onclick': "ga('send', 'event', { eventCategory: 'SG Event Detail', eventAction: 'URL click', eventLabel: '" + _objSrc.find('.event-title').text() + "'})" }).text(hideinfo.find('.website-url').val());

                if (_objSrc.find('.tag').length > 0) {
                    _objTar.find('.tag').text(_objSrc.find('.tag').text());
                } else { _objTar.find('.tag').hide(); }

                var tarFD = _objTar.find('.from-date'), tarTD = _objTar.find('.to-date'), srcFD = _objSrc.find('.from-date'), srcTD = _objSrc.find('.to-date');

                tarFD.find('.dow').text(srcFD.find('.dow').text()); tarFD.find('.dom').text(srcFD.find('.dom').text()); tarFD.find('.year.show-small').text(srcFD.find('.year.show-small').text());
                tarFD.find('.year.hide-small').text(srcFD.find('.year.hide-small').text())

                if (srcTD.length) {
                    tarTD.show();
                    tarTD.find('.dow').text(srcTD.find('.dow').text()); tarTD.find('.dom').text(srcTD.find('.dom').text()); tarTD.find('.show-small.year').text(srcTD.find('.show-small.year').text());
                    tarTD.find('.hide-small.year').text(srcTD.find('.hide-small.year').text());
                } else { tarTD.hide(); }

                if (_objSrc.find('.time').length > 0) _objTar.find('.time').show().html(_objSrc.find('.time').html()); else _objTar.find('.time').hide();

                _objTar.find('.note').html(hideinfo.find('.note-title').val());
                _objTar.find('.desc').html(hideinfo.find('.note-con').val());
                _objTar.find('.link.terms').attr({ 'href': hideinfo.find('.terms-url').val(), 'target': '_blank' }).html(hideinfo.find('.terms-title').val());
                _objTar.find('.desc a.link').attr('href', 'mailto:' + hideinfo.find('.mail-to').val());

                if (_objTar.find('.eventDetailFooter').text().trim() != '') _objTar.find('.eventDetailFooter').show(); else _objTar.find('.eventDetailFooter').hide();

                var socTitle, socDesc, socImg;

                if (hideinfo.find('.soc-share-title').val().trim() != '' && hideinfo.find('.soc-share-title').val().trim() != typeof (undefined))
                    socTitle = hideinfo.find('.soc-share-title').val();
                else {
                    socTitle = _objSrc.find('.event-title').text();
                }

                if (hideinfo.find('.soc-share-desc').val().trim() != '' && hideinfo.find('.soc-share-desc').val().trim() != typeof (undefined))
                    socDesc = hideinfo.find('.soc-share-desc').val();
                else {
                    socDesc = hideinfo.find('.detail-desc').val();
                }

                if (hideinfo.find('.soc-share-img').val().trim() != '' && hideinfo.find('.soc-share-img').val().trim() != typeof (undefined))
                    socImg = hideinfo.find('.soc-share-img').val();
                else {
                    socImg = _objSrc.attr('data-img');
                }


                $('#metaImg').attr('content', $('.host-name').val() + socImg);


                _objTar.find('.facebook').attr({ 'href': 'javascript:Facebook("' + socTitle + '","' + socDesc.replace(/"/g, '&quot;') + '","' + socImg + '","popup","' + hideinfo.find('.item-name').val() + '")' });
                _objTar.find('.mail').attr({ 'href': 'javascript:MailSend("' + socTitle + '","' + socDesc.replace(/"/g, '&quot;') + '","popup","' + hideinfo.find('.item-name').val() + '")', 'onclick': "ga('send', 'event', { eventCategory: 'SG Event Detail', eventAction: 'Share Email', eventLabel: '" + _objSrc.find('.event-title').text() + "'})" })
                _objTar.find('.whatsapp').attr({ 'href': 'javascript:WhatsApp("' + socDesc + '","popup","' + hideinfo.find('.item-name').val() + '")', 'onclick': "ga('send', 'event', { eventCategory: 'SG Event Detail', eventAction: 'Share Whatsapp', eventLabel: '" + _objSrc.find('.event-title').text() + "'})" })

                var pagetype = _objSrc.attr('data-page');

                if (pagetype == 'landing')
                    ga('send', 'event', { eventCategory: 'SG Homepage', eventAction: 'Calendar click', eventLabel: _objSrc.find('.event-title').text() })
                else
                    ga('send', 'event', { eventCategory: 'SG Event Listing', eventAction: 'Click', eventLabel: _objSrc.find('.event-title').text() })

                ga('send', 'pageview', '/sg-calendar/event-detail/' + _objSrc.find('.event-title').text());

                //alert('ga(set, Event Type,' +_objSrc.find('.cat-type').val()+')');
                //alert(_objSrc.find('.item-event-type').val())
                //alert(_objSrc.find('.cat-type').val())
                //alert(_objSrc.find('.audi-type').val())
                // alert(_objSrc.find('.item-period-type').val())

                var item_cat = _objSrc.find('.cat-type').val(), audiType = _objSrc.find('.audi-type').val(), itemeventType = _objSrc.find('.item-event-type').val(), period = _objSrc.find('.item-period').val();

                //if (item_cat != null && item_cat != '' && item_cat != typeof (undefined)) {
                //    //alert('C_' + item_cat)
                //    ga('set', 'dimension1', 'C_' + item_cat);
                //}
                //if (audiType != null && audiType != '' && audiType != typeof (undefined)) {
                //    //alert('A_' + audiType)
                //    ga('set', 'dimension2', 'A_' + audiType);
                //}
                //if (itemeventType != null && itemeventType != '' && itemeventType != typeof (undefined)) {
                //    //alert('T_' + itemeventType);
                //    ga('set', 'dimension3', 'A_' + itemeventType);
                //}
                //if (period != null && period != '' && period != typeof (undefined)) {
                //    //alert('P_' + period);
                //    ga('set', 'dimension4', 'A_' + period);
                //}

                var filters = '';

                if (item_cat != null && item_cat != '' && item_cat != typeof (undefined)) {
                    //console.log(item_cat);
                    filters += 'C_' + item_cat + ', ';
                } else filters += 'C_0' + ', ';

                if (audiType != null && audiType != '' && audiType != typeof (undefined)) {
                    //console.log(item_cat);
                    filters += 'A_' + audiType + ', ';
                } else filters += 'A_0' + ', ';

                if (itemeventType != null && itemeventType != '' && itemeventType != typeof (undefined)) {
                    // console.log(itemeventType);
                    filters += 'T_' + itemeventType + ', ';
                } else filters += 'T_0' + ', ';

                if (period != null && period != '' && period != typeof (undefined)) {
                    // console.log(period);
                    filters += 'P_' + period;
                } else filters += 'P_0';

                //alert(filters);
                ga('set', 'dimension1', filters);

            } else { return false; }
        }
    },

    initHomeSlider: function initHomeSlider() {
        if ($('.page-event-listing').length) {
            return;
        }
        var slider = null;

        function initSlider() {
            if (!slider) {
                if ($('.wrap-calendar').length && $(window).width() < 767) {
                    var width = $(window).width() - 50;
                    width = width > 280 ? 280 : width;
                    slider = $('.wrap-calendar').bxSlider({
                        minSlides: 1, maxSlides: 1, moveSlides: 1, controls: false, slideWidth: width, infiniteLoop: false, pager: true
                    });
                }
            } else {
                if ($(window).width() > 767) {
                    slider.destroySlider();
                    slider = null;
                } else {
                    var width = $(window).width() - 50;
                    width = width > 280 ? 280 : width;
                    slider.reloadSlider({
                        minSlides: 1, maxSlides: 1, moveSlides: 1, controls: false, slideWidth: width, infiniteLoop: false, pager: true
                    });
                }
            }

            var height = $('.block-event:not(.block-more)')[0].clientHeight;
            $('.block-event.block-more').css({ height: height });
        }

        $(window).on('load', function () {
            if ($('.wrap-calendar').length > 0) {
                initSlider();
            }
        });

        var lastWidth = 0;
        $(window).on('resize', function () {
            var currentWidth = $(window).width();
            if (currentWidth != lastWidth) {
                initSlider();
            }
            lastWidth = currentWidth;
        });
    },

    initEvalHeight: function initEvalHeight() {
        var isIE = detectIE();
        if ($('.wrap-calendar').length) {
            var list = $('.block-event').filter(':not(.hidden):not(.month):not(.comming):not(.block-more):not(.block-add)');
            var rect = list.get(0);
            if (rect) {
                var stdHeight = rect.getBoundingClientRect().height;
                $('.block-event.month').css({ height: stdHeight });
                $('.block-event.comming').css({ height: stdHeight });
                $('.block-event.block-more').css({ height: stdHeight });
                $('.block-event.block-add').css({ height: stdHeight });
                // fix height for ie
                if (isIE) {
                    list.css({ height: '' });
                    setTimeout(function () {
                        list.css({ height: stdHeight });
                    }, 50);
                }
            }
            else {
                var width = $('.wrap-calendar').width() / 4;
                var ratioHeight = width * 1.3322222222222222; //; //1.3322222222222222
                ratioHeight = ratioHeight < 340 ? 340 : ratioHeight;
                $('.block-event.month').css({ height: ratioHeight });
                $('.block-event.comming').css({ height: ratioHeight });
                $('.block-event.block-more').css({ height: ratioHeight });
                $('.block-event.block-add').css({ height: ratioHeight });
            }
        }
        //}
        //evalHeight();


    }
};

$(function () {
    SGF.calendar.init();
    $(window).on('resize', function () {
        SGF.calendar.initEvalHeight();
    });
});
function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}
//# sourceMappingURL=calendar.js.map

function isMobileDevice() {
    return typeof window.orientation !== "undefined" || navigator.userAgent.indexOf('Mobi') !== -1;
};


$(window).on('load', function () {

    /* GAtrack for Prev Next in Liting page */
    function GATrackPrevNext(ctrlId) {
        if ($(ctrlId).length > 0) {
            $(ctrlId).click(function () {
                if ($(this).is('.bx-prev')) {
                    console.log('GA Track Prev')
                    ga('send', 'event', { eventCategory: 'SG Event Listing', eventAction: 'Top bar click', eventLabel: 'left arrow' })
                } else {
                    console.log('GA Track Next'); ga('send', 'event', { eventCategory: 'SG Event Listing', eventAction: 'Top bar click', eventLabel: 'right arrow' })
                }
            });
        } else { console.log('Control not found'); return false; }
    }

    GATrackPrevNext('.bx-controls-direction a');

})

var clickCount = 0, start = 3, eventFire = 0;

function ShowNextMonths(ctrlId) {

    if ($(ctrlId).length > 0) {
        clickCount = 1;
        $(ctrlId).click(function () {
            var ctrl = $('.block-event.month'); ViewmoreClick = true;
            for (var index = start * clickCount; index < start * (clickCount + 1) ; index++) {
                ctrl.eq(index).show();
                if (index + 1 == ctrl.length) { $(ctrlId).hide(); }
            }
            if (eventFire == 0) {
                if (ctrl.eq(eventFire + 1).find('.btn-action').is('.collapsed')) {
                    ctrl.eq(eventFire + 1).find('.btn-action').trigger('click');
                }
            }
            else {
                ctrl.eq(eventFire).find('.btn-action').trigger('click');
            }
            ctrl.eq(start * clickCount).find('.btn-action').trigger('click');
            ViewmoreClick = false; eventFire = start * clickCount; clickCount++;
        })
    } else { console.log('Contorl not found'); return false; }

}

ShowNextMonths('.block-more');

