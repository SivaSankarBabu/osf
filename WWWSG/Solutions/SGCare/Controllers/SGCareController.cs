﻿using SGCare.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Controllers;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SGCare.Controllers
{
    public class SGCareController : SitecoreController
    {
        Database web = Database.GetDatabase("web");
        Database master = Database.GetDatabase("master");

        SGCareDAL objDal = new SGCareDAL();

        #region #SGCares Before Ravamp

        [HttpGet]
        public ActionResult Header()
        {
            HeadContent headCon = new HeadContent();

            try
            {
                Item item = web.GetItem(Constants.sgCareItemId);

                if (item != null)
                {
                    headCon.Logo = CommonFun.GetImage(item, "Brand Logo");
                    headCon.ProgramesMenu = Convert.ToString(item.Fields["Our Programmes MenuTitle"]);
                    headCon.StoriesMenu = Convert.ToString(item.Fields["The Stories MenuTitle"]);
                    headCon.Kindnessmenu = Convert.ToString(item.Fields["Act Of Kindness Menu Title"]);
                    headCon.ActivityMenu = Convert.ToString(item.Fields["Ground-Up Activity Menu Title"]);
                    headCon.CorporatePartnerShip = Convert.ToString(item.Fields["Corporate Title"]);
                    headCon.LogoAndGuidelinesText = Convert.ToString(item.Fields["Logo and Guidelines Text"]);
                    headCon.ShareText = Convert.ToString(item.Fields["Social Share Text"]);
                    headCon.GuidelinesFile = CommonFun.LinkUrl(item, "Logo and GuidelinesFile");
                    headCon.SocData = objDal.GetSocialSahreData(item);
                }

                return View(headCon);
            }
            catch (Exception ex) { return View(headCon); throw ex; }
        }

        void SetMetaTags(HeadContent obj, string desc)
        {
            obj.MetaDescription = desc;
        }

        [HttpGet]
        public ActionResult SGCareLanding()
        {
            SGCareLandData _objSGCare = new SGCareLandData();

            try
            {
                Item _conItem = Sitecore.Context.Item;


                _objSGCare.WhatsHappening = new SGCareDAL().GetWHData(Constants.eventsItemId);
                _objSGCare.Events = new SGCareDAL().GetEvents(Constants.eventsItemId, 5, string.Empty);
                _objSGCare.Categories = objDal.GetCategories(Constants.pillarsItemId);
                _objSGCare.Stories = objDal.GetStories(Constants.storiesItemId, 3, null, string.Empty);
                _objSGCare.Partners = objDal.GetPartners(Constants.partnersItemId);
                _objSGCare.Movements = objDal.GetMovements();
                _objSGCare.PartnerData = objDal.GetPartnerData();

                _objSGCare.Quote = objDal.GetQuotes("Quote Image 1", "Quote Image 2", _conItem);
                _objSGCare.DeskMastheadBanner = CommonFun.GetImage(_conItem, "Desktop Masthead Image");
                _objSGCare.MobMastheadBanner = CommonFun.GetImage(_conItem, "Mobile Masthead Image");
                _objSGCare.MasterHeadVideoUrl = Convert.ToString(_conItem.Fields["Masthead Video URL"]);
                _objSGCare.StoriesTitle = Convert.ToString(web.GetItem(Constants.storiesItemId).Fields["Story Title"]);
                _objSGCare.StoriesDesc = Convert.ToString(web.GetItem(Constants.storiesItemId).Fields["Story Description"]);
                _objSGCare.GetStartedCTA = Convert.ToString(_conItem.Fields["Get Started CTA"]);
                _objSGCare.GetStartedCTAUrl = CommonFun.LinkUrl(_conItem, "Get Started CTA Url");

                _objSGCare.MeterTitle = Convert.ToString(_conItem.Fields["Meter Title"]);
                _objSGCare.ActionPledgedText = Convert.ToString(_conItem.Fields["Action Pledged Text"]);
                _objSGCare.MeterDescription = Convert.ToString(_conItem.Fields["Meter Description"]);
                _objSGCare.ActofKindnessText = Convert.ToString(_conItem.Fields["Act Of Kindness Text"]);
                _objSGCare.VolunteerforCauseText = Convert.ToString(_conItem.Fields["Volunteer For a Cause Text"]);
                _objSGCare.GroupUpActivityText = Convert.ToString(_conItem.Fields["Group-Up Activity Text"]);
                _objSGCare.InvolvedMastheadBanner = CommonFun.GetImage(_conItem, "Get Involved Image");
                _objSGCare.InvolvedMasterHeadVideoUrl = Convert.ToString(_conItem.Fields["Get Involved Video Url"]);
                _objSGCare.PledgenowImage = CommonFun.GetImage(_conItem, "PledgeImage");

                _objSGCare.ActofKindnessPledgeimage = CommonFun.GetImage(_conItem, "Act Of Kindness Pledge Image");
                _objSGCare.VolunteerforCausePledgeImage = CommonFun.GetImage(_conItem, "Volunteer For a Cause Pledge Image");
                _objSGCare.GroupUpActivityPledgeImage = CommonFun.GetImage(_conItem, "Group-Up Activity Pledge Image");

                Item masterSGCares = master.GetItem(Constants.sgCareItemId);

                if (masterSGCares != null)
                    _objSGCare.PledgeCount = !string.IsNullOrEmpty(Convert.ToString(masterSGCares.Fields["Pledge Count"])) ? Convert.ToInt32(Convert.ToString(masterSGCares.Fields["Pledge Count"])) : 0;
                else
                    _objSGCare.PledgeCount = 0;

                return View(_objSGCare);
            }
            catch (Exception msg) { return View(_objSGCare); throw msg; }
        }

        [HttpGet]
        public ActionResult Footer()
        {
            FooterContent _fooer = new FooterContent();

            try
            {
                Item _item = web.GetItem(Constants.sgCareItemId);

                _fooer.FooterLogo = CommonFun.GetImage(_item, "Footer Brand Logo");
                _fooer.Copyrights = Convert.ToString(_item.Fields["Copyright Content"]);
                _fooer.SocialMediaContent = Convert.ToString(_item.Fields["Social Media Icons"]);
                _fooer.StickyTitle = Convert.ToString(_item.Fields["Title"]);
                _fooer.StickyDesc = Convert.ToString(_item.Fields["Description"]);
                _fooer.PledgeNowImage = CommonFun.GetImage(_item, "Pledge image");
                _fooer.ConfigDetails = objDal.GetConfigDetails();

                return View(_fooer);
            }
            catch (Exception msg) { return View(_fooer); throw msg; }
        }

        [HttpGet]
        public ActionResult Stories()
        {
            Stories _objStories = new Stories();

            try
            {
                Item conItem = Sitecore.Context.Item;

                string itemCat = CommonFun.Encode_Decode(Convert.ToString(Request.QueryString["cat"]), Constants.decode);
                _objStories.ListCategories = objDal.GetCategories(Constants.categoriesItemId);

                _objStories.ListStories = objDal.GetStories(Constants.storiesItemId, 4, null, string.Empty);
                _objStories.ListDeskImg = CommonFun.GetImage(conItem, "Desktop Masthead");
                _objStories.ListMobImg = CommonFun.GetImage(conItem, "Mobile Masthead");

                if (!string.IsNullOrEmpty(itemCat) && itemCat != Constants.AllCats)
                {
                    _objStories.PageCount = CommonFun.GetPageCount(web.GetItem(Constants.storiesItemId).GetChildren().Where(item => ((LookupField)item.Fields["Story Category"]).TargetItem != null && Convert.ToString(((LookupField)item.Fields["Story Category"]).TargetItem.Name) == itemCat).ToList().Count, "S");
                }
                else
                    _objStories.PageCount = CommonFun.GetPageCount(web.GetItem(Constants.storiesItemId).GetChildren().ToList().Count, "S");

                _objStories.QueryString = CommonFun.Encode_Decode(Convert.ToString(Request.QueryString["cat"]), Constants.decode);
                _objStories.AllCategories = new SGCareDAL().GetCTA(web.GetItem(Constants.categoriesItemId), "All Categories");


                return View(_objStories);
            }
            catch (Exception mg) { return View(_objStories); throw mg; }
        }

        [HttpGet]
        public ActionResult StoryDetails()
        {
            try
            {
                Item _item = Sitecore.Context.Item;
                Story _objStory = new Story();

                if (_item != null)
                {
                    _objStory = objDal.GetStoryDetails(_item);
                    ViewBag.RelatedStories = objDal.GetStories(Constants.storiesItemId, 3, _item, string.Empty);

                    Item _configItem = web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");
                    if (_configItem != null)
                    {
                        _objStory.FBAppId = Convert.ToString(_configItem.Fields["Facebook AppID"]);
                        _objStory.HostName = Convert.ToString(_configItem.Fields["Host Name"]);
                    }
                    //_objStory.Metatags = Convert.ToString("<meta property=\"fb:app_id\" content=\"" + _objStory.FBAppId + "\"/><meta property=\"og:image\" content=\"" + _objStory.HostName + _objStory.SocShareImage + "\"/>");
                    // Session["Metatags"] = Convert.ToString("<meta property=\"fb:app_id\" content=\"" + _objStory.FBAppId + "\"/><meta property=\"og:image\" content=\"" + _objStory.HostName + _objStory.SocShareImage + "\"/>");
                }
                return View(_objStory);
            }
            catch (Exception msg) { throw msg; }
        }

        [HttpGet]
        public ActionResult Programmes()
        {
            Programmes _objProgs = new Programmes();

            try
            {
                Item conItem = Sitecore.Context.Item;

                _objProgs.ListDeskImg = CommonFun.GetImage(conItem, "Desk Banner Image");
                _objProgs.ListMobImg = CommonFun.GetImage(conItem, "Mob Banner Image");

                string itemCat = CommonFun.Encode_Decode(Convert.ToString(Request.QueryString["cat"]), Constants.decode);
                _objProgs.ListProgrammes = objDal.GetProgrammes(Constants.programmesItemId, null, 6, itemCat);
                _objProgs.Stories = objDal.GetStories(Constants.storiesItemId, 3, null, string.Empty);
                _objProgs.ListCategories = objDal.GetCategories(Constants.categoriesItemId);
                _objProgs.ListTime = objDal.GetTimeList(Constants.TimeItemId);
                _objProgs.ListType = objDal.GetTypeList(Constants.TypeId);

                if (!string.IsNullOrEmpty(itemCat) && itemCat != Constants.AllCats)
                {
                    _objProgs.PageCount = CommonFun.GetPageCount(web.GetItem(Constants.programmesItemId).GetChildren().Where(item => ((LookupField)item.Fields["Programme Filter"]).TargetItem != null && Convert.ToString(((LookupField)item.Fields["Programme Filter"]).TargetItem.Fields["Text"]) == itemCat).ToList().Count, "P");
                }
                else { _objProgs.PageCount = CommonFun.GetPageCount(web.GetItem(Constants.programmesItemId).GetChildren().Count, "P"); }

                _objProgs.QueryString = CommonFun.Encode_Decode(Convert.ToString(Request.QueryString["cat"]), Constants.decode);
                _objProgs.AllCategories = new SGCareDAL().GetCTA(web.GetItem(Constants.categoriesItemId), "All Categories");

                _objProgs.Logo1 = CommonFun.GetImage(conItem, "LogoFirst");
                _objProgs.Logo2 = CommonFun.GetImage(conItem, "LogoSecond");

                _objProgs.CTAUrl1 = CommonFun.LinkUrl(conItem, "CTA Url1");
                _objProgs.CTAUrl2 = CommonFun.LinkUrl(conItem, "CTA Url2");

                return View(_objProgs);
            }
            catch (Exception) { return View(_objProgs); }
        }

        [HttpGet]
        public ActionResult ProgramDetails()
        {
            Programme _objProg = new Programme();

            try
            {
                Item _item = Sitecore.Context.Item;

                if (_item != null)
                {
                    ViewBag.Programmes = objDal.GetProgrammes(Constants.programmesItemId, _item, 3, string.Empty);
                    _objProg = objDal.GetProgramDetails(_item);
                    Item _configItem = web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");

                    if (_configItem != null)
                    {
                        _objProg.FBAppId = Convert.ToString(_configItem.Fields["Facebook AppID"]);
                        _objProg.HostName = Convert.ToString(_configItem.Fields["Host Name"]);
                    }

                    //_objProg.Metatags = Convert.ToString("<meta property=\"fb:app_id\" content=\"" + _objProg.FBAppId + "\"/><meta property=\"og:image\" content=\"" + _objProg.HostName + _objProg.SocShareImage + "\"/>");
                    // Session["Metatags"] = Convert.ToString("<meta property=\"fb:app_id\" content=\"" + _objProg.FBAppId + "\"/><meta property=\"og:image\" content=\"" + _objProg.HostName + _objProg.SocShareImage + "\"/>");
                }
                return View(_objProg);
            }
            catch (Exception msg) { return View(_objProg); }
        }


        [HttpGet]
        public ActionResult MetaData()
        {

            MetaContent metaCon = new MetaContent();

            new SGCareDAL().GetMetaInfo(metaCon, Sitecore.Context.Item);
            List<string> templateIds = new List<string>() { "{7A6B6F0B-3416-428E-AFEA-E16F41980213}", "{9668605B-6A03-49D7-AB8E-53996A0BD413}", "{02A6D268-37BA-4A4E-B7B0-6CAC59F58C05}", "{D60EB27C-6F53-4F1E-8CBF-7512B5E3097B}" };

            Item _configItem = web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}"), sgCareItem = web.GetItem(Constants.sgCareItemId);

            if (_configItem != null)
                metaCon.FBAppId = _configItem["Facebook AppID"];

            Item _conItem = Sitecore.Context.Item;

            List<string> rootItemTemps = new List<string>() { "{0D8D77B6-7F9F-4641-BC0D-77C8E8D7E00E}", "{51536251-CC70-4B0D-AA71-025EB7CC776E}", "{8F553DC8-26FF-4F9A-839E-4347ADF63FED}",
            "{63471076-8A3E-4E81-8961-7B43CDD28B94}","{0039F7E9-370F-48B7-ADD5-67CCB23A474E}"};

            if (rootItemTemps.IndexOf(_conItem.TemplateID.ToString()) >= 0)
            {
                if (sgCareItem != null)
                {
                    if (sgCareItem.Fields["Social Share Title"] != null)
                    {
                        string title = Convert.ToString(sgCareItem.Fields["Social Share Title"]);

                        if (!string.IsNullOrEmpty(title))
                            metaCon.OGTitle = title;
                        else
                            metaCon.OGTitle = sgCareItem.Name.Trim();
                    }

                    if (sgCareItem.Fields["Social Share Description"] != null)
                    {
                        string desc = Convert.ToString(sgCareItem.Fields["Social Share Description"]);

                        if (!string.IsNullOrEmpty(desc))
                            metaCon.OGDec = desc;
                        else
                            metaCon.OGDec = Convert.ToString(sgCareItem.Fields["Meter Description"]);
                    }

                    if (sgCareItem.Fields["Social Share Image"] != null)
                    {
                        ImageField img = sgCareItem.Fields["Social Share Image"];

                        if (img != null)
                        {
                            MediaItem mItem = img.MediaItem;

                            if (mItem != null)
                            {
                                string imgSrc = MediaManager.GetMediaUrl(mItem);
                                metaCon.OGImage = _configItem["Host Name"] + imgSrc;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(metaCon.OGImage))
                    {
                        if (sgCareItem.Fields["Desktop Masthead Image"] != null)
                        {
                            ImageField img = sgCareItem.Fields["Desktop Masthead Image"];

                            if (img != null)
                            {
                                MediaItem mItem = img.MediaItem;

                                if (mItem != null)
                                {
                                    string imgSrc = MediaManager.GetMediaUrl(mItem);
                                    metaCon.OGImage = _configItem["Host Name"] + imgSrc;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                string imgField = _conItem.ID.ToString() == "{9668605B-6A03-49D7-AB8E-53996A0BD413}" ? "Desktop Image" : "Desktop Masthead";
                string descField = _conItem.ID.ToString() == "{7A6B6F0B-3416-428E-AFEA-E16F41980213}" ? "Description" : "Short Description";

                if (_conItem != null)
                {
                    string title = Convert.ToString(_conItem.Fields["Social Share Title"]), desc = Convert.ToString(_conItem.Fields["Social Share Description"]),
                        img = Convert.ToString(CommonFun.GetImage(_conItem, "Social Share Image")), banner = Convert.ToString(CommonFun.GetImage(_conItem, imgField));

                    if (!string.IsNullOrEmpty(title))
                        metaCon.OGTitle = title;
                    else metaCon.OGTitle = _conItem.Name.Trim();

                    if (!string.IsNullOrEmpty(desc))
                        metaCon.OGDec = desc;
                    else metaCon.OGDec = Convert.ToString(_conItem.Fields[descField]).Length > 100 ? Convert.ToString(_conItem.Fields[descField]).Substring(0, 100) : Convert.ToString(_conItem.Fields[descField]);

                    if (!string.IsNullOrEmpty(img))
                        metaCon.OGImage = _configItem["Host Name"] + img;
                    else metaCon.OGImage = _configItem["Host Name"] + banner;
                }
            }
            return View(metaCon);
        }

        #endregion

        #region #SG Cares Revamp Methods

        /* Get Act of Kindness Data and list of items */
        [HttpGet]
        public ActionResult ActofKindness()
        {
            string itemId = Constants.actofKindnessRootItemId;

            try
            {
                ActofKindnessDTO _obj = new ActofKindnessDTO();

                if (!string.IsNullOrEmpty(itemId))
                {
                    Item _item = web.GetItem(itemId);

                    if (_item != null)
                    {
                        List<ActofKindnessItem> _objItems = new SGCareDAL().GetTiles(_item, "Tiles");

                        _obj.DeskMastheadImg = CommonFun.GetImage(_item, "Desktop Masthead");
                        _obj.MobMastHeadImg = CommonFun.GetImage(_item, "Mobile Masthead");
                        _obj.IntroImg = CommonFun.GetImage(_item, "Logo");
                        _obj.ItemCount = _item.GetChildren().Count - 8;
                        _obj.LandingTiles = _objItems;
                        _obj.AllExceptTiles = new SGCareDAL().GetTiles(_item, "Other");
                        _obj.CTAUrl = CommonFun.LinkUrl(_item, "CTA Url");

                        _obj.Item = _item;
                    }
                }

                return View(_obj);
            }
            catch (Exception msg) { throw msg; }
        }

        /* Get Act of Kindness Item Details */
        [HttpGet]
        public ActionResult ActofKindnessDetails()
        {
            try
            {
                Item _conItem = Sitecore.Context.Item;

                ActofKindnessItem obj = new ActofKindnessItem();

                if (_conItem != null)
                {
                    obj.DeskBanner = CommonFun.GetImage(_conItem, "Desktop Masthead");
                    obj.MobileBanner = CommonFun.GetImage(_conItem, "Mobile Masthead");
                    obj.CarouselItems = new SGCareDAL().GetCarouselBanners(_conItem, "Carousel", 5, new string[5] { "Carousel Video URL1", "Carousel Video URL2", "Carousel Video URL3", "Carousel Video URL4", "Carousel Video URL5" });

                    Item rootItem = web.GetItem(Constants.actofKindnessRootItemId);

                    if (rootItem != null)
                        obj.RelatedStories = new SGCareDAL().RelatedItems(rootItem, 3, _conItem.ID.ToString());

                    obj.SocData = objDal.GetSocialSahreData(_conItem);
                }

                return View(obj);
            }
            catch (Exception msg) { throw msg; }
        }

        /* Get Ground UP Activity Data and list of items */
        [HttpGet]
        public ActionResult GroundUpActivity()
        {
            string itemId = Constants.groundUpActivityRootItemId;

            try
            {
                GroundUpActivity _obj = new GroundUpActivity();

                if (!string.IsNullOrEmpty(itemId))
                {
                    Item _rootItem = web.GetItem(itemId);

                    if (_rootItem != null)
                    {
                        List<ActofKindnessItem> _objItems = new SGCareDAL().GetTiles(_rootItem, "Tiles");

                        _obj.DeskMastheadImg = CommonFun.GetImage(_rootItem, "Desktop Masthead");
                        _obj.MobMastHeadImg = CommonFun.GetImage(_rootItem, "Mobile Masthead");
                        _obj.IntroImg = CommonFun.GetImage(_rootItem, "Logo");
                        _obj.ItemCount = _rootItem.GetChildren().Count - 8;
                        _obj.LandingTiles = _objItems;
                        _obj.AllExceptTiles = new SGCareDAL().GetTiles(_rootItem, "Other");
                        _obj.CTAUrl = CommonFun.LinkUrl(_rootItem, "CTA Url");
                        _obj.Item = _rootItem;
                    }
                }

                return View(_obj);
            }
            catch (Exception msg) { throw msg; }
        }

        /* Get Ground UP Activity Item Details */
        [HttpGet]
        public ActionResult GroundupActivityDetails()
        {
            try
            {
                Item _conItem = Sitecore.Context.Item;

                ActofKindnessItem obj = new ActofKindnessItem();

                if (_conItem != null)
                {
                    obj.DeskBanner = CommonFun.GetImage(_conItem, "Desktop Masthead");
                    obj.MobileBanner = CommonFun.GetImage(_conItem, "Mobile Masthead");
                    obj.CarouselItems = new SGCareDAL().GetCarouselBanners(_conItem, "Carousel", 5, new string[5] { "Carousel Video URL1", "Carousel Video URL2", "Carousel Video URL3", "Carousel Video URL4", "Carousel Video URL5" });

                    Item rootItem = web.GetItem(Constants.groundUpActivityRootItemId);

                    if (rootItem != null)
                        obj.RelatedStories = new SGCareDAL().RelatedItems(rootItem, 3, _conItem.ID.ToString());

                    obj.SocData = objDal.GetSocialSahreData(_conItem);
                }

                return View(obj);
            }
            catch (Exception msg) { throw msg; }
        }

        #endregion


    }
}




