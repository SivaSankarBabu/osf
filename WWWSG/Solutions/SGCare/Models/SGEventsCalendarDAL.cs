﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Configuration;


namespace SGCare.Models
{
    public class SGEventsCalendarDAL
    {
        Database web = Factory.GetDatabase("web");

        /* Get Menu Items */
        public List<string> Get_Menu(Item item, string[] fldNames)
        {
            List<string> _menu = new List<string>();

            if (item != null && fldNames.Length > 0)
            {
                for (int index = 0; index < fldNames.Length; index++)
                {
                    _menu.Add(Convert.ToString(item.Fields[fldNames[index]]));
                }
                return new List<string>();
            }

            else { return new List<string>(); }
        }
    }
}