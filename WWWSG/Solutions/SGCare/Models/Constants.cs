﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGCare.Models
{
    public static class Constants
    {
        public static string eventsItemId = "{5F2A3EE4-3F03-4E2A-9023-846577F1490C}",
           programmesItemId = "{F427959C-9710-4F0B-A047-066F8010DFAA}", storiesItemId = "{CEEF3D85-830A-4CEE-8A12-6058F65D0189}",
           partnersItemId = "{C54C1D11-52CA-4E4B-90F1-2A3E716F9996}", sgCareItemId = "{C24A1BAA-3ED0-4AA4-BD68-A6A7E8EA7299}",
           categoriesItemId = "{E81E4477-9CA2-4917-A389-C05343A9EE85}", AllCats = "all categories", encode = "EN", decode = "De",
           actofKindnessRootItemId = "{58C9FC0E-FFB7-4E37-ACDE-70B003A8A28A}", groundUpActivityRootItemId = "{D54F137F-BB60-4A53-B95B-81CD522DFEB2}",
           TimeItemId = "{9B22718C-0ABB-4972-BC9C-B2F1214D5B64}", TypeId = "{F395E551-96DD-4307-AF95-7330D6E79971}",
           pillarsItemId = "{D177187A-0281-43DB-A75F-32731895350C}";

        public static int PageSize = 6, StoriesPageSize = 4;
    }


    public static class SGEventsConstants
    {
        public static string sgItemId = "{BAEBC2BC-71C9-4AA9-AA95-C58A8B0DEBF9}", AllCats = "all categories", encode = "EN", decode = "De";
        public static int PageSize = 6, StoriesPageSize = 4;
        public static string[] Menu = { "Futured Stories Menu Text", "Singapore Cares Menu Text", "SGFuture Menu Text", "OSF Menu Text", "Our SG Menu Text" };
    }
}
