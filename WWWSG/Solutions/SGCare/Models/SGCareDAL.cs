﻿using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SGCare.Models
{
    public class SGCareDAL
    {
        #region #SGCares Data Access Layer

        Database web = Database.GetDatabase("web");

        /* Get Events */
        public List<Event> GetEvents(string _eventsItmId, int count, string catName)
        {
            try
            {
                if (!string.IsNullOrEmpty(_eventsItmId))
                {
                    List<Item> items = null;

                    if (!string.IsNullOrEmpty(catName))
                        items = web.GetItem(_eventsItmId).GetChildren().Where(x => ((LookupField)x.Fields["Event Category"]).TargetItem != null && Convert.ToString(((LookupField)x.Fields["Event Category"]).TargetItem.Name) == catName).Take(count).ToList();
                    else
                        items = web.GetItem(_eventsItmId).GetChildren().Take(count).ToList();

                    return items.Select(eve => new Event
                    {
                        Name = Convert.ToString(_Get_Event_Name(eve.Fields["Event Name"].ToString(), 93)),
                        Date = _Get_Date(((DateField)eve.Fields["Event Date"]).DateTime.Date),
                        Location = Convert.ToString(eve.Fields["Event Location"]),
                        Url = CommonFun.LinkUrl(eve, "Event Url")
                    }).Take(count).ToList();
                }
                return new List<Event>();
            }
            catch (Exception msg) { return new List<Event>(); throw msg; }
        }

        /* Get Partners */
        public List<Partner> GetPartners(string _partnersItemId)
        {
            try
            {
                if (!string.IsNullOrEmpty(_partnersItemId))
                {
                    return web.GetItem(_partnersItemId).GetChildren().Select(item => new Partner
                    {
                        Image = CommonFun.GetImage(item, "Image"),
                        PartnerUrl = CommonFun.LinkUrl(item, "Link")
                    }).ToList();
                }
                return new List<Partner>();
            }
            catch (Exception msg) { return new List<Partner>(); throw msg; }
        }

        /* Get Movemenets and pareapared list */
        public List<Movement> GetMovements()
        {
            Movement _move = null;
            List<Movement> _movements = new List<Movement>();

            Item item = web.GetItem(Constants.partnersItemId);

            for (int index = 1; index <= 3; index++)
            {
                _move = new Movement();

                _move.ThumbImage = CommonFun.GetImage(item, "Moment" + index);
                _move.URL = CommonFun.LinkUrl(item, "Moment" + index + " URL");

                _movements.Add(_move);
            }

            return _movements;
        }

        /* Get What's happening data */
        public WhatHappening GetWHData(string whatsHappeningItemId)
        {
            WhatHappening _objWH = new WhatHappening();

            try
            {
                if (!string.IsNullOrEmpty(whatsHappeningItemId))
                {
                    Item _item = web.GetItem(whatsHappeningItemId);

                    _objWH.Title = Convert.ToString(_item.Fields["Whats Happening Title"]);
                    _objWH.ViewmoreCTA = Convert.ToString(_item.Fields["View More CTA"]);
                    _objWH.ViewmoreCTAUrl = CommonFun.LinkUrl(_item, "View More CTA URL");
                }

                return _objWH;
            }
            catch (Exception msg) { return _objWH; throw msg; }
        }

        /* Get Partners Data */
        public PartnerData GetPartnerData()
        {
            try
            {
                Item _item = web.GetItem(Constants.partnersItemId);
                if (_item != null)
                {
                    return new PartnerData()
                    {
                        PartnerTitle = Convert.ToString(_item.Fields["Partners Title"]),
                        ExpandText = Convert.ToString(_item.Fields["Expand Text"]),
                        MovementTitle = Convert.ToString(_item.Fields["A Moment Title"]),
                    };
                }
                else { return new PartnerData(); }
            }
            catch (Exception msg) { return new PartnerData(); throw msg; }
        }

        /* Get Quotes */
        public Quote GetQuotes(string fldOne, string fldTwo, Item _item)
        {
            Quote _objQuote = new Quote();

            try
            {
                _objQuote.QuoteImg1 = CommonFun.GetImage(_item, fldOne);
                _objQuote.QuoteImg2 = CommonFun.GetImage(_item, fldTwo);

                return _objQuote;
            }
            catch (Exception msg) { return _objQuote; throw msg; }
        }

        /* Get Categories */
        public List<Category> GetCategories(string _catsItemId)
        {
            try
            {
                if (!string.IsNullOrEmpty(_catsItemId))
                {
                    return web.GetItem(new ID(_catsItemId)).GetChildren().Select(item => new Category
                    {
                        Text = Convert.ToString(item.Fields["Text"]),
                        Value = Convert.ToString(item.Name.Trim()),
                        CatImage = CommonFun.GetImage(item, "Image Thumbnail"),
                        VideoUrl = Convert.ToString(item.Fields["Video Url"]),
                        ShortDesc = Convert.ToString(item.Fields["Short Description"]),
                        CategoryIcon = CommonFun.GetImage(item, "Icon"),
                        CTA = Convert.ToString(item.Fields["CTA Text"]),
                        GetStartedCTA = Convert.ToString(item.Fields["Get Started CTA"]),
                        GetStartedUrl = CommonFun.LinkUrl(item, "Get Started CTA Url"),
                        StoriesItem = web.GetItem(Constants.programmesItemId),
                        Url = _catsItemId == Constants.pillarsItemId ? CommonFun.LinkUrl(item, "Url") : string.Empty,
                        CatItem = item
                    }).ToList();
                }
                else { return new List<Category>(); }
            }
            catch (Exception ex) { return new List<Category>(); throw ex; }
        }

        //Get Time List
        public List<Time> GetTimeList(string timeId)
        {
            try
            {
                if (!string.IsNullOrEmpty(timeId))
                {
                    return web.GetItem(new ID(timeId)).GetChildren().Select(item => new Time
                    {
                        Text = Convert.ToString(item.Fields["Text"]),
                        Value = Convert.ToString(item.Fields["Value"]),
                        TimeItem = item
                    }).ToList();
                }
                else { return new List<Time>(); }
            }
            catch (Exception ex) { return new List<Time>(); throw ex; }
        }

        //Get Type List
        public List<Type> GetTypeList(string TypeId)
        {
            try
            {
                if (!string.IsNullOrEmpty(TypeId))
                {
                    return web.GetItem(new ID(TypeId)).GetChildren().Select(item => new Type
                    {
                        Text = Convert.ToString(item.Fields["Text"]),
                        Value = Convert.ToString(item.Fields["Value"]),
                        TypeItem = item
                    }).ToList();
                }
                else { return new List<Type>(); }
            }
            catch (Exception ex) { return new List<Type>(); throw ex; }
        }

        /* Get Programmes */
        public List<Programme> GetProgrammes(string _progItemId, Item contextItem, int count, string catName)
        {
            try
            {
                if (!string.IsNullOrEmpty(_progItemId))
                {
                    List<Item> _progs = new List<Item>();

                    if (contextItem != null)
                    {
                        string _catName = Convert.ToString(((LookupField)contextItem.Fields["Programme Filter"]).TargetItem.Name);

                        if (!string.IsNullOrEmpty(_catName))
                            _progs = web.GetItem(_progItemId).GetChildren().Where(item => (item.ID.ToString() != contextItem.ID.ToString() && ((LookupField)item.Fields["Programme Filter"]).TargetItem != null) && Convert.ToString(((LookupField)item.Fields["Programme Filter"]).TargetItem.Name) == _catName).OrderByDescending(x => ((DateField)x.Fields["Date"]).DateTime).Take(count).ToList();
                        else
                            return new List<Programme>();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(catName) && catName != Constants.AllCats)
                            _progs = web.GetItem(_progItemId).GetChildren().Where(item => ((LookupField)item.Fields["Programme Filter"]).TargetItem != null && Convert.ToString(((LookupField)item.Fields["Programme Filter"]).TargetItem.Name) == catName).ToList();
                        else
                            _progs = web.GetItem(_progItemId).GetChildren().ToList();

                        _progs = _progs.Take(count).ToList();
                    }

                    if (_progs != null && _progs.Count > 0)
                    {
                        return _progs.OrderByDescending(x => ((DateField)x.Fields["Date"]).DateTime).Select(_prog => new Programme
                        {
                            ThumbnailImage = CommonFun.GetImage(_prog, "Thumbnail Image"),
                            DeskThumbImage = CommonFun.GetImage(_prog, "Desktop Image"),
                            MobThumbImage = CommonFun.GetImage(_prog, "Desktop Image"),
                            Title = Convert.ToString(_prog.Fields["Name of Programme"]),
                            ShortDesc = Convert.ToString(_prog.Fields["Short Description"]).Length <= 200 ? Convert.ToString(_prog.Fields["Short Description"]) : CommonFun.TruncateAtWord(Convert.ToString(_prog.Fields["Short Description"]), 200),
                            CategoryIconPath = CommonFun.GetImage(((LookupField)_prog.Fields["Programme Filter"]).TargetItem, "Icon"),
                            CTA = Convert.ToString(_prog.Fields["CTA"]),
                            RelatedStory = CommonFun.GetItemPath(_prog, "Related Story"),
                            StoryCat = GetStoryTitle(((InternalLinkField)_prog.Fields["Related Story"]).Path),
                            ItemPath = LinkManager.GetItemUrl(_prog),
                            Type = ((LookupField)_prog.Fields["Type"]).TargetItem != null ? Convert.ToString(((LookupField)_prog.Fields["Type"]).TargetItem.Name) : string.Empty,
                            Time = ((LookupField)_prog.Fields["Time"]).TargetItem != null ? Convert.ToString(((LookupField)_prog.Fields["Time"]).TargetItem.Name) : string.Empty,
                            StoryName = ((LookupField)_prog.Fields["Related Story"]).TargetItem != null ? Convert.ToString(((LookupField)_prog.Fields["Related Story"]).TargetItem.Name) : string.Empty,
                            Item = _prog,
                            ItemName = _prog.Name
                        }).ToList();
                    }
                }
                return new List<Programme>();
            }
            catch (Exception msg) { return new List<Programme>(); throw msg; }
        }

        /* Get Programme Details */
        public Programme GetProgramDetails(Item _item)
        {
            Programme _prog = new Programme();

            try
            {

                _prog.ItemPath = LinkManager.GetItemUrl(_item);
                _prog.DeskThumbImage = CommonFun.GetImage(_item, "Desktop Image");
                _prog.MobThumbImage = CommonFun.GetImage(_item, "Mobile Image");
                _prog.FindOutMoreCTAUrl = CommonFun.LinkUrl(_item, "Find Out How URL");
                _prog.SocShareTitle = Convert.ToString(_item.Fields["Social Share Title"]);
                _prog.SocShareDesc = Convert.ToString(_item.Fields["Social Share Content"]);
                //_prog.SocShareImage = CommonFun.GetImage(_item, "Social Share Image");
                _prog.SocShareImage = CommonFun.GetImage(_item, "Social Share Image");

                if (string.IsNullOrEmpty(_prog.SocShareImage))
                    _prog.SocShareImage = _prog.DeskThumbImage;

                _prog.FindOutHowUrl = CommonFun.LinkUrl(_item, "Find Out How URL");

                Item tarItem = ((LookupField)_item.Fields["Programme Filter"]).TargetItem;

                if (tarItem != null)
                {
                    _prog.CategoryIconPath = CommonFun.GetImage(tarItem, "Icon");
                    _prog.ProgramCategory = Convert.ToString(tarItem.Name);
                }
                else
                    _prog.CategoryIconPath = _prog.ProgramCategory = string.Empty;

                //_prog.Events = GetEvents(Constants.eventsItemId, 3, _prog.ProgramCategory.ToString());

                return _prog;
            }
            catch (Exception msg) { return _prog; throw msg; }
        }

        /* Get Stories */
        public List<Story> GetStories(string _storiesItemId, int count, Item conItem, string catName)
        {
            try
            {
                if (!string.IsNullOrEmpty(_storiesItemId))
                {
                    List<Item> stories = null;

                    if (catName == string.Empty)
                    {
                        if (count > 0 && conItem == null)
                            stories = web.GetItem(new ID(_storiesItemId)).GetChildren().ToList().OrderByDescending(x => ((DateField)x.Fields["Date"]).DateTime).Take(count).ToList();

                        else
                        {
                            if (conItem != null)
                            {
                                string _catName = ((LookupField)conItem.Fields["Story Category"]).TargetItem != null ? Convert.ToString(((LookupField)conItem.Fields["Story Category"]).TargetItem.Name) : string.Empty;

                                if (!string.IsNullOrEmpty(_catName))
                                    stories = web.GetItem(new ID(_storiesItemId)).GetChildren().Where(item => (item.ID.ToString() != conItem.ID.ToString() && ((LookupField)item.Fields["Story Category"]).TargetItem != null) && Convert.ToString(((LookupField)item.Fields["Story Category"]).TargetItem.Name) == _catName).OrderByDescending(x => ((DateField)x.Fields["Date"]).DateTime).Take(count).ToList();
                                else
                                    return new List<Story>();
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(catName) && catName != Constants.AllCats)
                            stories = web.GetItem(_storiesItemId).GetChildren().Where(item => ((LookupField)item.Fields["Story Category"]).TargetItem != null && Convert.ToString(((LookupField)item.Fields["Story Category"]).TargetItem.Name) == catName).ToList();
                        else
                            stories = web.GetItem(_storiesItemId).GetChildren().Take(count).ToList();
                    }

                    if (stories != null && stories.Count > 0)
                    {
                        return stories.OrderByDescending(x => ((DateField)x.Fields["Date"]).DateTime).Select(item => new Story
                        {
                            /* Image Panel Data */
                            ThumbnailImage = CommonFun.GetImage(item, "Desktop Masthead"),
                            VideoUrl = CommonFun.LinkUrl(item, "Video Url"),
                            GetStartedCTA = Convert.ToString(item.Fields["Get Started CTA"]),
                            GetStartedUrl = CommonFun.LinkUrl(item, "Get Started CTA Url"),

                            /* Content Panel Data */
                            CategoryIconPath = CommonFun.GetImage(((LookupField)item.Fields["Story Category"]).TargetItem, "Icon"),
                            Title = Convert.ToString(item.Fields["Title"]),
                            ShortDesc = Convert.ToString(item.Fields["Description"]).Length <= 200 ? Convert.ToString(item.Fields["Description"]) : CommonFun.TruncateAtWord(Convert.ToString(item.Fields["Description"]), 200),
                            CTA = System.Convert.ToString(item.Fields["CTA"]),
                            ItemPath = LinkManager.GetItemUrl(item),
                            Item = item,
                            ItemName = item.Name
                        }).ToList();
                    }
                    else { return new List<Story>(); }
                }
                else { return new List<Story>() { }; }
            }
            catch (Exception msg) { return new List<Story>(); throw msg; }
        }

        /* Get Story Details */
        public Story GetStoryDetails(Item item)
        {
            try
            {
                Story _story = new Story();

                _story.ItemPath = LinkManager.GetItemUrl(item);
                _story.DeskThumbImage = CommonFun.GetImage(item, "Desktop Masthead");
                _story.MobThumbImage = CommonFun.GetImage(item, "Mobile Masthead");
                _story.CategoryIconPath = CommonFun.GetImage(((LookupField)item.Fields["Story Category"]).TargetItem, "Icon");
                _story.Title = Convert.ToString(item.Fields["Title"]);
                _story.Author = Convert.ToString(item.Fields["Author"]);
                _story.TopContent = Convert.ToString(item.Fields["Introduction"]);
                _story.BottomContent = Convert.ToString(item.Fields["Bodycopy"]);
                _story.CarouseItems = GetCarouselBanners(item, "Carousel", 5, new string[5] { "Carousel Video URL1", "Carousel Video URL2", "Carousel Video URL3", "Carousel Video URL4", "Carousel Video URL5" });
                _story.SocShareTitle = Convert.ToString(item.Fields["Social Share Title"]);
                _story.SocShareDesc = Convert.ToString(item.Fields["Social Share Description"]);
                _story.SocShareImage = CommonFun.GetImage(item, "Social Share Image");

                if (string.IsNullOrEmpty(_story.SocShareImage))
                    _story.SocShareImage = _story.DeskThumbImage;

                _story.FindOutHowUrl = CommonFun.LinkUrl(item, "Find Out How URL");

                return _story;
            }
            catch (Exception msg) { return new Story(); throw msg; }
        }

        /* Get Carousel Bannres from Tree list field in Story item */
        public List<Carousel> GetCarouselBanners(Item item, string fldName, int count, string[] videoFlds)
        {
            List<Carousel> carousels = new List<Carousel>();

            if (!string.IsNullOrEmpty(fldName) && videoFlds.Length >= 0 && count > 0)
            {
                carousels.AddRange(GetYoutubeVideos(videoFlds, item));

                MultilistField mlf = ((MultilistField)item.Fields[fldName]);

                if (mlf != null)
                {
                    Carousel car = null;

                    foreach (ID id in mlf.TargetIDs)
                    {
                        if (carousels.Count < count)
                        {
                            car = new Carousel();
                            car.ItemImage = Sitecore.Resources.Media.MediaManager.GetMediaUrl(web.GetItem(id));
                            car.ItemType = "Image";
                            carousels.Add(car);
                        }
                    }
                }
            }

            return carousels;
        }

        /* Get Event Name from item and trim it to 93 Characters */
        string _Get_Event_Name(string name, int length)
        {
            if (!string.IsNullOrEmpty(name))
            {
                return name.Length > length ? name.Substring(0, length) + "...." : name.Substring(0, name.Length);
            }
            else { return string.Empty; }
        }

        /* Get date */
        DateTime _Get_Date(DateTime dateTime)
        {
            if (dateTime != Convert.ToDateTime(new DateTime(0001, 01, 01)))
            {
                return dateTime;
            }
            else { return new DateTime(); }
        }

        /* Get Story Title from item path */
        public string GetStoryTitle(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                return Convert.ToString(web.GetItem(path).Fields["Title"]);
            }
            return string.Empty;
        }

        /* Get Story Categories CTA form Categories root item */
        public string GetCTA(Item item, string fildName)
        {
            if (item != null && !string.IsNullOrEmpty(fildName))
            {
                return Convert.ToString(item.Fields[fildName]);
            }
            else { return string.Empty; }
        }

        public void GetMetaInfo(MetaContent obj, Item item)
        {
            if (!string.IsNullOrEmpty(item.TemplateID.ToString()))
            {
                switch (item.TemplateID.ToString())
                {
                    // Home Page 
                    case "{0D8D77B6-7F9F-4641-BC0D-77C8E8D7E00E}": { SetMetaTags(obj, "{C24A1BAA-3ED0-4AA4-BD68-A6A7E8EA7299}"); break; }

                    /* Story Listing and details */
                    case "{0039F7E9-370F-48B7-ADD5-67CCB23A474E}":
                    case "{7A6B6F0B-3416-428E-AFEA-E16F41980213}": SetMetaTags(obj, "{CEEF3D85-830A-4CEE-8A12-6058F65D0189}"); break;

                    /* Program Listing and details */
                    case "{63471076-8A3E-4E81-8961-7B43CDD28B94}":
                    case "{9668605B-6A03-49D7-AB8E-53996A0BD413}": SetMetaTags(obj, "{F427959C-9710-4F0B-A047-066F8010DFAA}"); break;

                    /* Act of kindness Listing and details */
                    case "{51536251-CC70-4B0D-AA71-025EB7CC776E}":
                    case "{02A6D268-37BA-4A4E-B7B0-6CAC59F58C05}": SetMetaTags(obj, "{58C9FC0E-FFB7-4E37-ACDE-70B003A8A28A}"); break;

                    /* Ground Up Activity Listing and details */
                    case "{8F553DC8-26FF-4F9A-839E-4347ADF63FED}":
                    case "{D60EB27C-6F53-4F1E-8CBF-7512B5E3097B}": SetMetaTags(obj, "{D54F137F-BB60-4A53-B95B-81CD522DFEB2}"); break;

                    default: break;
                }
            }
        }

        void SetMetaTags(MetaContent obj, string itemId)
        {
            if (!string.IsNullOrEmpty(itemId))
            {
                Item item = web.GetItem(itemId);

                if (item != null)
                {
                    obj.MetaTitle = Convert.ToString(item.Fields["Meta Title"]);
                    obj.MetaDescription = Convert.ToString(item.Fields["Meta Description"]);
                    obj.MetaKeywords = Convert.ToString(item.Fields["Meta Keywords"]);
                    obj.ConItem = item;
                }
                else
                    obj.MetaTitle = obj.MetaDescription = obj.MetaKeywords = string.Empty;
            }
        }

        /* Build */
        List<Carousel> GetYoutubeVideos(string[] videoFlds, Item item)
        {
            List<Carousel> _carousel = new List<Carousel>();
            Carousel _objCarousel = null;

            if (videoFlds.Length > 0 && item != null)
            {
                foreach (string fild in videoFlds)
                {
                    string url = Convert.ToString(item.Fields[fild].ToString());

                    if (!string.IsNullOrEmpty(url))
                    {
                        _objCarousel = new Carousel();

                        _objCarousel.ItemImage = GetYoutubeImgPath(url);
                        _objCarousel.Url = url;
                        _objCarousel.ItemType = "Video";

                        _carousel.Add(_objCarousel);
                    }
                }
            }
            return _carousel;
        }

        /* Get Youtube image, if we pass youtube url */
        string GetYoutubeImgPath(string youtubeUrl)
        {
            int LastIndex = youtubeUrl.LastIndexOf("/", StringComparison.OrdinalIgnoreCase);

            if (LastIndex != -1)
            {
                return "https://i.ytimg.com/vi" + youtubeUrl.Substring(LastIndex) + "/hqdefault.jpg";
            }
            return string.Empty;
        }

        #endregion

        #region #SGCares Revamp Methods by Suryam

        /* return list of Act of Kindness items */
        public List<ActofKindnessItem> GetTiles(Item _rootItem, string type)
        {
            try
            {
                string[] tilesFiledNames = new string[8] { "Main Story", "2nd Story", "3rd Story", "4th Story", "5th Story", "6th Story", "7th Story", "8th Story" };

                List<Item> items = new List<Item>(), contentItems = new List<Item>(); List<ID> tilesIDs = new List<ID>();

                if (_rootItem != null)
                {
                    if (type == "Tiles" || type == "Other")
                    {
                        for (int index = 0; index < tilesFiledNames.Length; index++)
                        {
                            InternalLinkField internalField = ((InternalLinkField)_rootItem.Fields[tilesFiledNames[index]]);

                            Item _item = null;

                            if (internalField != null)
                                _item = internalField.TargetItem;

                            if (_item != null)
                            {
                                items.Add(_item); tilesIDs.Add(_item.ID);
                            }
                        }
                    }

                    if (type == "Other")
                        items = _rootItem.GetChildren().Where(x => tilesIDs.IndexOf(x.ID) < 0).ToList();

                    contentItems = items;

                    return contentItems.Select(x => new ActofKindnessItem
                    {
                        TileforBox1_2 = Convert.ToString(CommonFun.GetImage(x, "Image Thumbnail for Box 1 or 2")),
                        TileforBox3 = Convert.ToString(CommonFun.GetImage(x, "Image Thumbnail for Box3")),
                        ShortDesc = Convert.ToString(x.Fields["Short Description"]),
                        Title = Convert.ToString(x.Fields["Article Title"]),
                        Readmore = Convert.ToString(x.Fields["CTA Text"]),
                        Item = x,
                    }).ToList();
                }

                return new List<ActofKindnessItem>() { };
            }
            catch (Exception msg) { throw msg; }
        }

        /* Get Realted items for Act of Kindness and Ground up activity based on order by date created */
        public List<ActofKindnessItem> RelatedItems(Item item, int count, string itemId)
        {
            try
            {
                if (item != null && count > 0 && !string.IsNullOrEmpty(itemId))
                {
                    return item.GetChildren().Where(x => x.ID.ToString() != itemId).OrderByDescending(x => x.Statistics.Created).Take(count).Select(x => new ActofKindnessItem
                    {
                        TileforBox3 = Convert.ToString(CommonFun.GetImage(x, "Image Thumbnail for Box 1 or 2")),
                        Title = Convert.ToString(x.Fields["Article Title"]),
                        Readmore = Convert.ToString(x.Fields["CTA Text"]),
                        Item = x,
                    }).ToList();
                }
                return new List<ActofKindnessItem>();
            }
            catch (Exception msg) { throw msg; }


        }

        /* Get Social Sahre Content */
        public SocialShare GetSocialSahreData(Item item)
        {
            try
            {
                SocialShare obj = new SocialShare();
                string imgField = item.ID.ToString() == Constants.sgCareItemId ? "Desktop Masthead Image" : "Desktop Masthead";
                string descField = item.ID.ToString() == Constants.sgCareItemId ? "Meter Description" : "Introduction";

                if (item != null)
                {
                    string title = Convert.ToString(item.Fields["Social Share Title"]), desc = Convert.ToString(item.Fields["Social Share Description"]),
                        img = Convert.ToString(CommonFun.GetImage(item, "Social Share Image")), banner = Convert.ToString(CommonFun.GetImage(item, imgField));

                    if (!string.IsNullOrEmpty(title))
                        obj.SocTitle = title;
                    else obj.SocTitle = item.Name.Trim();

                    if (!string.IsNullOrEmpty(desc))
                        obj.SocDesc = desc;
                    else obj.SocDesc = Convert.ToString(item.Fields[descField]).Length > 100 ? Convert.ToString(item.Fields[descField]).Substring(0, 100) : Convert.ToString(item.Fields[descField]);

                    if (!string.IsNullOrEmpty(img))
                        obj.SocImg = img;
                    else obj.SocImg = banner;

                    return obj;
                }
                return new SocialShare();

            }
            catch (Exception msg)
            {
                throw msg;
            }
        }

        /* Get Config Detials */
        public ConfigDetails GetConfigDetails()
        {
            try
            {
                ConfigDetails objConfigDetails = new ConfigDetails();

                Item _configItem = web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");

                if (_configItem != null)
                {
                    objConfigDetails.FBAppId = Convert.ToString(_configItem.Fields["Facebook AppID"]);
                    objConfigDetails.HostName = Convert.ToString(_configItem.Fields["Host Name"]);

                    return objConfigDetails;
                }
                return new ConfigDetails();
            }
            catch (Exception msg) { throw msg; }
        }

        #endregion

    }
}
