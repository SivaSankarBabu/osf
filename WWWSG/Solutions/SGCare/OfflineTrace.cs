﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Events;
using Sitecore.Publishing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace SGCare
{
    public class OfflineTrace
    {
        static string TraceLogFile = HttpContext.Current.Server.MapPath("\\SGCare\\OfflineCounter\\Counter.txt");
        //static string TraceLogFile = "E:\\SIVA SANKAR BABU\\PROJECTS\\MCCYOSF\\Website\\SGCare\\OfflineCounter\\Counter.txt"; //Local
        //static string TraceLogFile = "F:\\websites\\MCCYNEW\\Website\\SGCare\\OfflineCounter\\Counter.txt"; //DEVT
        Database web = Database.GetDatabase("web");
        public void OnPublishEnd(object sender, EventArgs args)
        {
            try
            {
                Publisher publisher = Event.ExtractParameter(args, 0) as Publisher;
                var lan = publisher.Options.Language.Name;

                if (lan == "en")
                {
                    string sgCareItemId = "{C24A1BAA-3ED0-4AA4-BD68-A6A7E8EA7299}";

                    var rootItemId = publisher.Options.RootItem.ID.ToString();

                    if (rootItemId.ToString() == sgCareItemId)
                    {
                        string strauthor = Sitecore.Context.User.Name;
                        Item item = web.GetItem(rootItemId);
                        string CounterVal = Convert.ToString(item.Fields["Offline Pledge Count"]);
                        if (!string.IsNullOrEmpty(CounterVal))
                            WriteLine("Author: " + strauthor + " and Offline Count: " + CounterVal);
                    }

                }
            }
            catch (Exception ex)
            {

            }

        }
        public static void WriteLine(string Message)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n", DateTime.Now, Message);
            File.AppendAllText(TraceLogFile, finalMessage);
        }
    }
}