'use strict';

var SGF = window.SGF || {};

SGF.main = {
    init: function init() {
        SGF.main.initNav();
        SGF.main.initModal();
        SGF.main.initSlider();
        SGF.main.initHomeSlider();
        SGF.main.initExpand();
        SGF.main.initStickSocial();
        SGF.main.initToTop();
        SGF.main.initMobileArticleHover();
        SGF.main.initCustomSelect();
        SGF.main.initMasonryLayout();
        SGF.main.initPaging();
        SGF.main.initRelated();
        SGF.main.initSidebar();
        SGF.main.initPartners();
    },

    initRelated: function initRelated() {
        if ($('.block-relate').length) {
            $.each($('.block-relate .item__info'), function (i, v) {
                $(v).css({ height: $(v).siblings('.item__img').height() });
            });

            $(window).on('resize', function () {
                $.each($('.block-relate .item__info'), function (i, v) {
                    $(v).css({ height: $(v).siblings('.item__img').height() });
                });
            });
        }
    },

    initSidebar: function initSidebar() {
        if ($('.side-bar').length) {
            (function () {
                var setSidebar = function setSidebar() {
                    var breakpoint = 767;
                    var width = $(window).width();
                    var sidebar = $('.side-bar');
                    if (width > breakpoint) {
                        var neo = $('.article-content').parent();
                        var currentTop = $(window).scrollTop() + $('#page-header').height();
                        if (currentTop > neo.offset().top) {
                            if (sidebar.not('.sticky')) {
                                sidebar.addClass('sticky');
                            }
                            var height = sidebar.height();
                            // var height = $(window).height();
                            // var gap = $(window).scrollTop() + height - (neo.offset().top + neo.height());
                            // var gap = $(window).scrollTop() + height - $('#page-footer').offset().top + 20;
                            var gap = $(window).scrollTop() + height - $('#page-footer').offset().top + 100;
                            if (gap > 0) {
                                sidebar.css({ top: -(gap - 70) });
                            } else {
                                sidebar.css({ top: '' });
                            }
                        } else {
                            sidebar.removeClass('sticky').css({ top: '' });
                        }
                    } else {
                        sidebar.removeClass('sticky').css({ top: '' });
                    }
                };

                setSidebar();

                $(window).on('resize', function () {
                    setSidebar();
                });

                $(window).on('scroll', function () {
                    setSidebar();
                });
            })();
        }
    },

    initPartners: function initPartners() {
        if ($('.section-partners').length && $('.btn-toggle').length) {
            $('.section-partners').on('click', '.btn-toggle', function (e) {
                e.preventDefault();
                var wrap = $('.wrap-collapse-content');
                var height = wrap.find('.inner')[0].clientHeight;
                var isExpand = $(this).is('.expand');

                // Tween
                var to = isExpand ? height : 0;
                wrap.css({ height: to });
                $('.btn-toggle').toggleClass('active');
            });

            $(window).on('resize', function () {
                var wrap = $('.wrap-collapse-content');
                if (wrap.height() > 0) {
                    var height = wrap.find('.inner')[0].clientHeight;
                    wrap.css({ height: height });
                }
            });
        }
    },

    initExpand: function initExpand() {
        $('.expand').on('click', function (e) {
            e.preventDefault();
            $('.content--most-recent.collapse').toggle();
        });

        if ($('.section-events-calendar').length) {
            $('.section-events-calendar').on('click', '.heading a', function (e) {
                if ($(this).is('.collapsed')) {
                    $('.content--most-recent').find('.active').removeClass('active');
                } else {
                    $('.content--most-recent').find('li').first().addClass('active');
                }
            });
        }
    },

    initNav: function initNav() {
        $('.icon-nav').off('click').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $('.wrap-nav').toggleClass('active');
            $('.nav-fade').toggleClass('show');
        });

        $('.nav-fade').off('click').on('click', function () {
            if ($('.icon-nav').is('.active')) {
                $('.icon-nav').removeClass('active');
                $('.wrap-nav').removeClass('active');
                $(this).removeClass('show');
            }
        });

        $(window).on('scroll', function () {
            if ($('.icon-nav').is('.active')) {
                $('.icon-nav').removeClass('active');
                $('.wrap-nav').removeClass('active');
                $('.nav-fade').toggleClass('show');
            }
        });
    },

    initModal: function initModal() {
        if ($('.button-watch-video').length) {
            $('.button-watch-video').off('click').on('click', function (e) {
                e.preventDefault();
                var target = $(this).attr('href');
                $(target).modal();
                var iframe = $(target).find('iframe');
                iframe.attr('src', iframe.data('src'));

                $(target).off('hidden.bs.modal').on('hidden.bs.modal', function () {
                    iframe.attr('src', '');
                });
            });
        }

        if ($('.play').length) {
            $('.play').off('click').on('click', function (e) {
                e.preventDefault();
                var target = $(this).attr('href'), srcIframe = $(this).attr('data-src'), gsText = $(this).attr('data-gs-cta'), gs_url = $(this).attr('data-gs-url'), gs_Videoname = $(this).attr('data-gs-vtext');
                $(target).modal();
                var iframe = $(target).find('iframe'); iframe.attr('src', srcIframe);
                var desCtrl = $(target);
                if (gsText != null && gsText != '') {
                    desCtrl.find('.get-started:eq(0)').show();
                    desCtrl.find('.get-started a').attr('href', gs_url).text(gsText);

                    desCtrl.find('.get-started a').off('click').on('click', function () {
                        e.preventDefault();
                        ga('send', 'event', { eventCategory: 'SGCares Homepage', eventAction: 'Get started', eventLabel: gs_Videoname })
                    })
                } else {
                    desCtrl.find('.get-started:eq(0)').hide();
                }
                desCtrl.find('.button__wrapper:eq(1)').off('click').on('click', function () {
                    e.preventDefault();
                    ga('send', 'event', { eventCategory: 'SGCares Homepage', eventAction: 'Close', eventLabel: gs_Videoname })
                })

                $(target).off('hidden.bs.modal').on('hidden.bs.modal', function () {
                    iframe.attr('src', '');
                });
            });
        }
    },

    initSlider: function initSlider() {
        if ($('.list-quote').length) {
            if ($('.list-quote:visible').length && $('.list-quote:visible .item-quote').length > 1) {
                var bxQuote = $('.list-quote:visible').bxSlider({
                    controls: false,
                    easing: 'ease-in-out',
                    auto: true,
                    pause: 5500
                });

                $('.list-quote:visible').data('slider', bxQuote);

                $('.list-quote .item-link').off('click').on('click', function (e) {
                    e.preventDefault();
                    if (!bxQuote) {
                        bxQuote = $('.list-quote:visible').data('slider');
                    }
                    if (bxQuote) {
                        bxQuote.goToNextSlide();
                    }
                });
            }
        }

        if ($('.list-article-thumb:visible').length && $('.list-article-thumb:visible .item').length > 1) {
            $('.list-article-thumb:visible').bxSlider({
                adaptiveHeight: true
            });
        }

        if ($('.list-project').length) {
            var ww = $(window).width();
            var ms = ww > 767 ? 3 : 1;
            var sw = Math.round(ww / ms);
            $('.list-project').bxSlider({
                maxSlides: ms,
                slideWidth: sw,
                moveSlides: 1,
                infiniteLoop: true,
                pager: false
            });
        }

        if ($('.list-img li').length > 1) {
            $('.list-img').bxSlider({
                maxSlides: 1,
                moveSlides: 1,
                infiniteLoop: true,
                pager: true,
                controls: false
            });
        }
    },

    initHomeSlider: function initHomeSlider() {
        if ($('.mobile-list').length && $('.news--three-col').length) {
            (function () {
                var topSlide = function topSlide() {
                    var slide;
                    if ($(window).width() < 768) {
                        $('.content.content--expand-collapse').addClass('in').css({ height: '' }).attr('aria-expanded', 'true');
                        $('.button--read-more').removeClass('collapsed').attr('aria-expanded', 'true');
                        slide = $('.mobile-list:visible').bxSlider({
                            pager: false,
                            adaptiveHeight: true
                        });
                        $('.mobile-list:visible').data('slide', slide);
                    } else {
                        slide = $('.mobile-list:visible').data('slide');
                        if (slide) {
                            slide.destroySlider();
                            $('.mobile-list:visible').data('slide', null);
                        }
                        //
                        if ($('.content--expand-collapse').is('.in')) {
                            $('.button--read-more').parent().hide();
                            $('.button--read-more').parents('.content--expand-collapse').find('.button__wrapper').show();
                        }
                    }
                };

                var bottomSlide = function bottomSlide() {
                    var slide;
                    if ($(window).width() < 768) {
                        slide = $('.news--three-col .content').bxSlider({
                            pager: false,
                            adaptiveHeight: true
                        });
                        $('.news--three-col .content').data('slide', slide);
                    } else {
                        slide = $('.news--three-col .content').data('slide');
                        if (slide) {
                            slide.destroySlider();
                            $('.news--three-col .content').data('slide', null);
                        }
                    }
                };
                // topSlide();


                bottomSlide();
                $('.button--read-more').on('click', function (e) {
                    $('.button--read-more').parent().show();
                    $(this).parent().hide();
                });

                //
                $(window).on('resize', function () {
                    // topSlide();
                    bottomSlide();
                });
            })();
        }
    },

    initStickSocial: function initStickSocial() {
        if ($('.block-social').length) {
            var obj = $('.block-social');
            var neo = $('.article-content');
            var top = neo.offset().top;
            var left = neo.offset().left - 50;
            var gap = $('#page-header').height();
            left = $(window).width() > 767 ? left : 0;

            $(window).on('scroll', function () {
                var wintop = $(window).scrollTop() + gap;

                if (wintop > top && obj.not('sticky')) {
                    obj.addClass('sticky');
                    obj.css({ left: left, top: gap });
                } else {
                    obj.removeClass('sticky');
                    obj.css({ left: '', top: '' });
                }

                if (wintop > neo && obj.not('out')) {
                    obj.addClass('out');
                    obj.fadeOut();
                } else {
                    obj.removeClass('out');
                    obj.fadeIn();
                }
            });

            $(window).on('resize', function () {
                if (window.callback) {
                    clearTimeout(window.callback);
                }
                window.callback = setTimeout(function () {
                    SGF.main.initStickSocial();

                    // trigger calc now
                    top = neo.offset().top;
                    left = neo.offset().left - 65;
                    gap = $('#page-header').height();
                    left = $(window).width() > 767 ? left : 0;
                    var wintop = $(window).scrollTop() + gap;
                    if (wintop > top && obj.not('sticky')) {
                        obj.addClass('sticky');
                        obj.css({ left: left, top: gap });
                    } else {
                        obj.removeClass('sticky');
                        obj.css({ left: '', top: '' });
                    }
                }, 200);
            });
        }
    },

    initMobileArticleHover: function initMobileArticleHover() {
        if ($(window).width() < 992 && $('.block-projects').length) {
            $('.block-projects .project-item').on('touchstart', function (e) {
                $('.block-projects .project-item').removeClass('hover');
                $(this).addClass('hover');
                e.stopPropagation();
            });
            $(document).on('touchstart', function () {
                $('.block-projects .project-item').removeClass('hover');
            });
        }
    },

    initToTop: function initToTop() {
        if ($('.btn-back-top').length) {
            $('.btn-back-top').off('click').on('click', function (e) {
                e.preventDefault();
                $('body,html').animate({ scrollTop: 0 }, 500);
            });
            $(window).on('scroll', function () {
                var scrollTop = $(window).scrollTop();
                var height = window.innerHeight;
                var position = scrollTop + height;
                var limitTop = $('#page-footer').offset().top;

                if (position > height * 1.2 && position < limitTop) {
                    $('.btn-back-top').css({ 'position': 'fixed' });
                } else {
                    $('.btn-back-top').css({ 'position': '' });
                }
            });
        }
    },

    initCustomSelect: function initCustomSelect() {
        $('select').niceSelect();
    },

    initMasonryLayout: function initMasonryLayout() {
        var width = $('.news--grid .grid').width() / 3 - 100;
        var $grid = $('.grid').masonry({
            // options
            columnWidth: '.grid-sizer',
            itemSelector: '.grid-item'
        });

        // layout Masonry after each image loads
        $grid.imagesLoaded().progress(function () {
            $grid.masonry('layout');
        });
    },

    initPaging: function initPaging() {
        if ($('.wrap-paging').length) {
            $('.wrap-paging').on('click', '.page-link', function (e) {
                e.preventDefault();
                if ($(this).is('.prev') || $(this).is('.next')) {
                    var wrap = $('.wrap-paging .wrap-inner');
                    var left = parseInt(wrap.attr('data-left')) || 0;
                    var width = wrap[0].scrollWidth;
                    var move = wrap.parent().width();
                    left = left + ($(this).is('.next') ? -move : move);
                    left = left > 0 ? 0 : left < -(width - move) ? -(width - move) : left;
                    wrap.css({ left: left });
                    wrap.attr('data-left', left);
                    return;
                }
                if (!$(this).is('.active')) {
                    $(this).addClass('active').siblings('.active').removeClass('active');
                }
            });
        }
    }
};

$(function () {
    SGF.main.init();
});
//# sourceMappingURL=main.js.map
