﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using sgfuture.SGFuture_Revamp.sublayouts;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public partial class ProjectShowcaseDetailsSublayout : System.Web.UI.UserControl
    {
        private static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        Item itmcontext = Sitecore.Context.Item;
        SGFutureClass objUtilities = new SGFutureClass();
        CommonMethods cmObj = new CommonMethods();
        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Logger.WriteLine("Project Showcase Details Form is started");
                GetProjectDetails();
                GetInspiringProjects("{7F755F74-00C9-4604-8DA2-248ACFA90967}");
                string targetUrl = objUtilities.GetItemLink(itmcontext, "Link URL");
                if (!string.IsNullOrEmpty(targetUrl))
                {
                    lnkUrl.Text = lnkUrl.NavigateUrl = targetUrl;
                }
                else
                {
                    lnkUrl.Text = string.Empty;

                }

                lnkGetStarted.NavigateUrl = objUtilities.GetItemLink(itmcontext, "Get Started URL");
                lnkVolunter.NavigateUrl = objUtilities.GetItemLink(itmcontext, "Be a Volunteer URL");

                if (string.IsNullOrEmpty(Convert.ToString(itmcontext.Fields["Bottom Description"])))
                    divBottomDes.Visible = false;
                ProjectName.Value = itmcontext.Name;
                //SocialSharing();
            }
        }

        public void GetInspiringProjects(string p)
        {
            try
            {
                DataTable dtProjects = new DataTable();
                DataRow drProjects;
                dtProjects.Columns.AddRange(new DataColumn[6] { new DataColumn("href", typeof(string)), new DataColumn("BgImage", typeof(string)), new DataColumn("ProjectTitle", typeof(string)), new DataColumn("Date", typeof(string)), new DataColumn("Id", typeof(string)), new DataColumn("ItemName", typeof(string)) });
                List<Item> projectItems = web.GetItem(p).GetChildren().Where(x => x.ID.ToString() != itmcontext.ID.ToString()).ToList();
                if (projectItems != null && projectItems.Count > 0)
                {
                    foreach (Item item in projectItems)
                    {
                        drProjects = dtProjects.NewRow();
                        drProjects["href"] = Convert.ToString(LinkManager.GetItemUrl(item));
                        drProjects["BgImage"] = GetImagePath(item, "Thumb Image", "Image");
                        drProjects["ProjectTitle"] = Convert.ToString(item.Fields["Project Title"]);
                        drProjects["Date"] = Convert.ToString(item.Fields["Project Number"]);
                        drProjects["ItemName"] = item.Name;
                        drProjects["Id"] = Convert.ToString(item.ID);
                        dtProjects.Rows.Add(drProjects);

                    }
                    rptInspiringProjects.DataSource = dtProjects;
                    rptInspiringProjects.DataBind();
                }
            }


            catch (Exception ex)
            {
                //Logger.WriteException(ex, "Exception Occurred " + ex.InnerException);
                throw ex;
            }
        }

        //private void SocialSharing()
        //{
        //    Item itemconfiguration = SG50Class.web.GetItem(SG50Class.str_Configuration_Template_ID);
        //    string HostName = itemconfiguration["Host Name"];
        //    DataTable dtSGFuture = new DataTable();
        //    dtSGFuture.Columns.Add("FaceBookTitle", typeof(string));
        //    dtSGFuture.Columns.Add("FaceBookContent", typeof(string));
        //    dtSGFuture.Columns.Add("FaceBookImage", typeof(string));
        //    dtSGFuture.Columns.Add("HostName", typeof(string));

        //    DataRow drOSF;
        //    //try
        //    //{
        //    string SocialTitle = string.Empty;
        //    string SocialContent = string.Empty;
        //    string TwitterContent = string.Empty;
        //    string noHTML = string.Empty;
        //    string inputHTML = string.Empty;

        //    drOSF = dtSGFuture.NewRow();
        //    drOSF["HostName"] = HostName;

        //    if (!string.IsNullOrEmpty(itmcontext.Fields["Social Share Title"].ToString()))
        //    {
        //        drOSF["FaceBookTitle"] = cmObj.BuildString(itmcontext.Fields["Social Share Title"].ToString(), CommonMethods.faceBook);
        //    }
        //    else
        //    {
        //        drOSF["FaceBookTitle"] = cmObj.BuildString(itmcontext.Fields["Title"].ToString(), CommonMethods.faceBook);
        //    }

        //    if (!string.IsNullOrEmpty(itmcontext.Fields["Social Share Content"].ToString()))
        //    {
        //        drOSF["FaceBookContent"] = cmObj.BuildString(itmcontext.Fields["Social Share Content"].ToString(), CommonMethods.faceBook); //+ " " + hostName + LinkManager.GetItemUrl(itmContext);
        //    }
        //    drOSF["FaceBookImage"] = HostName + GetImagePath(itmcontext, "Social Share Image", "Image");

        //    dtSGFuture.Rows.Add(drOSF);
        //    repsocialSharingTop.DataSource = dtSGFuture;
        //    repsocialSharingTop.DataBind();

        //}

        public string GetImagePath(Item item, string colName, string imagePathOrAlt)
        {
            try
            {
                if (!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Image")
                {
                    ImageField img = (ImageField)item.Fields[colName];
                    if (img != null)
                    {
                        MediaItem mItem = img.MediaItem;
                        if (mItem != null)
                            return Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                        else { return string.Empty; }
                    }
                    else { return string.Empty; }
                }
                else if ((!string.IsNullOrEmpty(imagePathOrAlt) && imagePathOrAlt == "Alt"))
                    return ((ImageField)item.Fields[colName]).Alt.ToString();
                else { return string.Empty; }
            }
            catch (Exception exception)
            {
                //Logger.WriteException(exception, "Exception Occurred " + exception.InnerException);
                throw exception;
            }
        }

        string GetMediaImagePath(Item item, string colName)
        {
            ImageField imgBckgrnd = (ImageField)item.Fields[colName];
            if (imgBckgrnd != null)
            {
                MediaItem mItem = imgBckgrnd.MediaItem;
                if (mItem != null)
                {
                    return Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                }
                else { return string.Empty; }
            }
            else { return string.Empty; }
        }



        public void GetProjectDetails()
        {
            try
            {
                DataTable dtSlide = new DataTable();
                StringBuilder sbGallery = new StringBuilder();
                StringBuilder sbQuotes1 = new StringBuilder();
                StringBuilder sbQuotes2 = new StringBuilder();

                ImageField desktopBanner, mobileBanner;
                desktopBanner = ((ImageField)itmcontext.Fields["Project Desktop Image"]);
                mobileBanner = ((ImageField)itmcontext.Fields["Project Mobile Image"]);
                string url = string.Empty;
                if (desktopBanner != null && desktopBanner.MediaItem != null)
                {
                    url = Sitecore.Resources.Media.MediaManager.GetMediaUrl(desktopBanner.MediaItem);
                }


                if (itmcontext.Fields["Gallery"] != null && !itmcontext.Fields["Gallery"].Equals(""))
                {
                    Sitecore.Data.Fields.MultilistField mlGallery = itmcontext.Fields["Gallery"];
                    if (mlGallery != null)
                    {
                        int GalleryCount = 0;
                        foreach (ID id in mlGallery.TargetIDs)
                        {
                            DataRow drSlide = dtSlide.NewRow();
                            MediaItem item = Sitecore.Context.Database.GetItem(id.ToString());
                            if (item != null && GalleryCount < 5)
                            {
                                sbGallery.Append("<li class='item'> <figure class='wrap-thumb'>");
                                sbGallery.Append("<img class='thumb' src='" + Sitecore.Resources.Media.MediaManager.GetMediaUrl(item) + "'></figure>");
                                sbGallery.Append("<figcaption class='thumb-desc'> " + item.Alt + " </figcaption></li>");
                                GalleryCount++;
                            }
                        }
                    }
                }
                Slideshow.InnerHtml = sbGallery.ToString();


                DataTable dtQuotes = new DataTable();
                dtQuotes.Columns.AddRange(new DataColumn[4] { new DataColumn("QuoteName", typeof(string)), new DataColumn("QuoteDesignation", typeof(string)), new DataColumn("Quotation", typeof(string)), new DataColumn("Id", typeof(string)) });
                DataRow drQuotes;
                List<Item> quoteItems = itmcontext.GetChildren().OrderByDescending(a => a.Statistics.Created).Take(2).ToList();
                int quotecount = 0;
                if (quoteItems != null && quoteItems.Count > 0)
                {
                    foreach (Item item in quoteItems)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(item.Fields["Name"])) && !string.IsNullOrEmpty(Convert.ToString(item.Fields["Designation"])) && !string.IsNullOrEmpty(Convert.ToString(item.Fields["Quote"])))
                        {
                            drQuotes = dtQuotes.NewRow();
                            drQuotes["QuoteName"] = Convert.ToString(item.Fields["Name"]);
                            drQuotes["QuoteDesignation"] = Convert.ToString(item.Fields["Designation"]);
                            drQuotes["Quotation"] = Convert.ToString(item.Fields["Quote"]);
                            dtQuotes.Rows.Add(drQuotes);
                            quotecount++;
                        }
                        else if (!string.IsNullOrEmpty(Convert.ToString(item.Fields["Name"])) || !string.IsNullOrEmpty(Convert.ToString(item.Fields["Designation"])) || !string.IsNullOrEmpty(Convert.ToString(item.Fields["Quote"])))
                        {
                            drQuotes = dtQuotes.NewRow();
                            drQuotes["QuoteName"] = Convert.ToString(item.Fields["Name"]);
                            drQuotes["QuoteDesignation"] = Convert.ToString(item.Fields["Designation"]);
                            drQuotes["Quotation"] = Convert.ToString(item.Fields["Quote"]);
                            dtQuotes.Rows.Add(drQuotes);
                            quotecount++;
                        }

                    }

                    rptQuotes.DataSource = dtQuotes;
                    rptQuotes.DataBind();
                }
                else
                {
                    divQuote.Visible = false;
                }

                if (quotecount == 0)
                    divQuote.Visible = false;


                //if (!string.IsNullOrEmpty(itmcontext.Fields["Social Share Title"].ToString()))
                //    hdnSocialShareTitle.Value = cmObj.BuildString(itmcontext.Fields["Social Share Title"].ToString(), CommonMethods.faceBook);
                //else
                //    hdnSocialShareTitle.Value = cmObj.BuildString(itmcontext.Fields["Project Title"].ToString(), CommonMethods.faceBook);

                divSocialSahreDescription.InnerHtml = cmObj.BuildString(itmcontext.Fields["Social Share Content"].ToString(), CommonMethods.faceBook);
                //  hdnSocialShareImage.Value = new SGFutureClass().GetImagePath(itmcontext, "Social Share Image", "Image");
                //Item itemconfiguration = web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");
                 // hdnHostName.Value = itemconfiguration["Host Name"];
                //hdnEventURL.Value = itemconfiguration["Host Name"] + "/" + LinkManager.GetItemUrl(itmcontext).ToString();
            }
            catch (Exception msg)
            {

                //Logger.WriteException(msg, "Exception Occurred " + msg.InnerException);

            }
        }
    }
}
