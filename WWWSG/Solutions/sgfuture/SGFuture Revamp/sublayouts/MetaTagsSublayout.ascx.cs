﻿using System;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public partial class MetaTagsSublayout : System.Web.UI.UserControl
    {

        public static Database web = Sitecore.Configuration.Factory.GetDatabase("web");
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            if (!IsPostBack)
            {
                AddMetaTag();
            }


        }

        public void AddMetaTag()
        {

            if ("{12BAFE5B-E786-456D-B4D2-E4CF98A81ACB}" == Convert.ToString(Sitecore.Context.Item.ID))
            {
                SGFuturelitMetaTag.Text += "<meta property=\"Title\" content=\"SGfuture I Creating a meaningful future for all Singaporeans\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"description\" content=\"Fellow Singaporeans, just like us, are sharing ideas and working together to shape our future. Be part of the change.\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"keywords\" content=\"SGfuture,Superhero Me,Our Singapore Fund,Volunteer,Projects,MCCY,Ministry of Culture, Community and Youth,Minister Grace Fu,Minister Chan Chun Sing,Engagement sessions\" />";

            }
            else if ("{9F0037AF-4418-4A27-A844-CC636C29B9A7}" == Convert.ToString(Sitecore.Context.Item.ID))
            {

                SGFuturelitMetaTag.Text += "<meta property=\"Title\" content=\"About SGfuture I The next chapter of the nation’s story\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"description\" content=\"SGfuture signalled the start of a journey in which Singaporeans would work together to co-create greater success beyond SG50.\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"keywords\" content=\"SGfuture,SG50,SGfuture committee,Minister Grace Fu,Minister Chan Chun Sing,Engagement sessions,A caring community,A cleaner, greener and smarter home,A secure and resilient nation,A learning people\" />";


            }
            else if ("{7F755F74-00C9-4604-8DA2-248ACFA90967}" == Convert.ToString(Sitecore.Context.Item.ID) || "{11D5739C-9833-44ED-9DFB-FD599ED5D2C3}" == Convert.ToString(Sitecore.Context.Item.Template.ID))
            {

                SGFuturelitMetaTag.Text += "<meta property=\"Title\" content=\"SGfuture project showcase I Turning ideas into action\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"description\" content=\"Read the stories behind the projects and be inspired to start your own.\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"keywords\" content=\"SGfuture,Projects,Singaporeans,Superhero Me,Community,LearnSG Seed Fund,National Reading Movement,Volunteer,Our Singapore Fund,MCCY\" />";

            }
            else if ("{0E35F486-1BDC-4586-87FD-14413610B816}" == Convert.ToString(Sitecore.Context.Item.ID) || "{9FA4743A-AF24-46D3-AA09-51F52EED7B6B}" == Convert.ToString(Sitecore.Context.Item.Template.ID))
            {

                SGFuturelitMetaTag.Text += "<meta property=\"Title\" content=\"SGfuture engagement sessions I Ideas for the future of our nation\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"description\" content=\"Singaporeans got together to discuss their ideas for the future of our nation. See what happens when we come together as one.\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"keywords\" content=\"SGfuture,SG50,SGfuture committee,Minister Grace Fu,Minister Chan Chun Sing,Engagement sessions,A caring community,A cleaner, greener and smarter home,A secure and resilient nation,A learning people\" />";
            }
            else if ("{D2F60819-B30B-43BC-B5CA-FC606C422826}" == Convert.ToString(Sitecore.Context.Item.ID))
            {

                SGFuturelitMetaTag.Text += "<meta property=\"Title\" content=\"SGfuture report I Be inspired by the possibilities\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"description\" content=\"Our future depends on what we make of it together. Everyone can be part of this. Will you join us?\" />";
                SGFuturelitMetaTag.Text += "<meta property=\"keywords\" content=\"SGfuture report,SG50,SGfuture committee,Minister Grace Fu,Minister Chan Chun Sing,Engagement sessions,A caring community,A cleaner, greener and smarter home,A secure and resilient nation,A learning people\" />";
            }

            if (Sitecore.Context.Item.Fields["Social Share Image"] != null)
            {
                Sitecore.Data.Fields.ImageField img = Sitecore.Context.Item.Fields["Social Share Image"];
                if (img != null)
                {
                    MediaItem mItem = img.MediaItem;
                    if (mItem != null)
                    {
                        string imgSrc = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                        Item itemconfiguration = web.GetItem("{7BDF653A-75C4-4C6B-96C4-EA749EC0FBF5}");
                        SGFuturelitMetaTag.Text += "<meta property=\"fb:app_id\" content=\"" + itemconfiguration["Facebook AppID"] + "\"/>";
                        SGFuturelitMetaTag.Text += "<meta property=\"og:image\" content=\"" + itemconfiguration["Host Name"] + imgSrc + "\"/>";
                    }
                }



            }

            if (Sitecore.Context.Item.Fields["Social Share Title"] != null)
            {
                SGFuturelitMetaTag.Text += "<meta property=\"og:title\" content=\"" + Sitecore.Context.Item.Fields["Social Share Title"].ToString() + "\"/>";
            }

            //if (Sitecore.Context.Item.Fields["Social Share Content"] != null)
            //{
            //    SGFuturelitMetaTag.Text += "<meta property=\"og:description\" content=\"" + Sitecore.Context.Item.Fields["Social Share Content"].ToString() + "\"/>";
            //}


        }
    }
}