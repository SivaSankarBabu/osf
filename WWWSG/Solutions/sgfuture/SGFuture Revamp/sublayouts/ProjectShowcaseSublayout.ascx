﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProjectShowcaseSublayout.ascx.cs" Inherits="sgfuture.SGFuture_Revamp.sublayouts.ProjectShowcaseSublayout" %>
<main class="page page-projects">
			<div class="container-fluid">
				<!-- begin section hero -->
				<section class="section-hero text-center">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-10 col-sm-push-1">
								<h2 class="title">
									<sup class="number">02</sup>
									<span class="text">
                                        <sc:Text ID="txtPSTitle" runat="server" Field="Project Title" DataSource="{7F755F74-00C9-4604-8DA2-248ACFA90967}" />
									</span>
								</h2>
								<p class="desc">
									<sc:Text ID="txtPSDesc" runat="server" Field="Project Description" DataSource="{7F755F74-00C9-4604-8DA2-248ACFA90967}"/>
								</p>
							</div>
						</div>
					</div>
				</section>
				<!-- end section hero -->

				<!-- begin section main -->
				<section class="section-main">
					<div class="block-projects">
						<div class="row">
                            <asp:Repeater ID="rptProjectShowcase" runat="server">
                                <ItemTemplate>
							<div class="col-xs-6 col-sm-3 no-padding">
								<article class="project-item" id="pject1">
									<figure class="wrap-thumb">
										<img class="thumb" src="<%# DataBinder.Eval(Container.DataItem, "ListThumbImage") %>" />
									</figure>
									<div class="fade"></div>
									<figcaption class="wrap-info">
										<%--<span class="project-no"><%#DataBinder.Eval(Container.DataItem,"ProjectNumber") %></span>--%>
										<span class="project-date"><%#DataBinder.Eval(Container.DataItem,"ProjectNumber") %></span>
										<h2 class="project-title"><%# DataBinder.Eval(Container.DataItem,"ProjectTitle") %> </h2>
                                        <input type="hidden" class="hdnItemName" value="<%# DataBinder.Eval(Container.DataItem,"Name") %>" />
										<div class="project-desc-wrap">
											<p class="project-desc"> <%# DataBinder.Eval(Container.DataItem,"ProjectShortDescription") %> </p>
											<span class="project-link"><%# DataBinder.Eval(Container.DataItem,"ReadMore") %></span>
										</div>
									</figcaption>
									<div class="wrap-line">
										<div class="line line-ver"></div>
										<div class="line line-hoz"></div>
										<div class="cross cross-top"></div>
										<div class="cross cross-bottom"></div>
										<div class="cross-bg"></div>
									</div>
									<a class="wrap-link" href='<%#DataBinder.Eval(Container.DataItem,"href") %>'></a>
								</article>
							</div>	
                                    </ItemTemplate>
                            </asp:Repeater>						
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">     
                            <div class="wrap-paging">
                                <asp:LinkButton ID="lnkbFirst" CssClass="pager first" runat="server" OnClick="lnkbFirst_Click">&lt;&lt;</asp:LinkButton>
                                <asp:LinkButton ID="lnkbPrevious" CssClass="pager prev" runat="server" OnClick="lnkbPrevious_Click">&lt;</asp:LinkButton>
								<asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnPage" runat="server" Enabled='<%# DataBinder.Eval(Container.DataItem,"Enable") %>' CssClass='<%#DataBinder.Eval(Container.DataItem,"class") %>'
                                            CommandName="Page" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ID") %>' ForeColor="White" Font-Bold="True">
                                            <%#DataBinder.Eval(Container.DataItem,"ID") %>
                                        </asp:LinkButton>
                                    </ItemTemplate>
								</asp:Repeater>
                                <asp:LinkButton ID="lnkbNext" CssClass="pager next" runat="server" OnClick="lnkbNext_Click">&gt;</asp:LinkButton>
                                <asp:LinkButton ID="lnkbLast" CssClass="pager last" runat="server" OnClick="lnkbLast_Click">&gt;&gt;</asp:LinkButton>							
							</div>                       
						</div>
					</div>
				</section>
				<!-- end section main -->

			</div>
       <a class="btn-back-top" href="javascrip:;"></a>

		</main>

<script type="text/javascript">
    $(function () {

        $('.project-item').click(function () {
            var Title = $(this).find('.hdnItemName').val();
            ga('send', 'event', {
                eventCategory: 'SGFuture Homepage',
                eventAction: 'Click',
                eventLabel: Title
            });
        });

        $(".pager").click(function () {
            var buttonvalue = $(this).html();
            if (buttonvalue == "&gt;")
                buttonvalue = "Next";
            else if (buttonvalue == "&gt;&gt;")
                buttonvalue = "Last";
            else if (buttonvalue == "&lt;")
                buttonvalue = "Previous";
            else if (buttonvalue == "&lt;&lt;")
                buttonvalue = "Begin";
            ga('send', 'event', {
                eventCategory: 'Project showcase - Pagination',
                eventAction: 'Click',
                eventLabel: buttonvalue
            });
        });

        /* Truncate item description, If it is more than 35 chars only in mobile divices */
        function TrimDesc_Devices(ctrl) {
            if ($(ctrl).length > 0) {
                $(ctrl).each(function () {
                    if ($(this).text().length > 35) {
                        $(this).text($(this).text().toString().substr(0, 35) + '...');
                    }
                });
            } else { return false; }
        }

        /* Truncate item Title, If it is more than 25 chars only in mobile divices */
        function TrimTitle_Devices(ctrl) {
            if ($(ctrl).length > 0) {
                $(ctrl).each(function () {
                    if ($(this).text().length > 25) {
                        $(this).text($(this).text().toString().substr(0, 25) + '...');
                    }
                });
            } else { return false; }
        }

        if (/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            TrimDesc_Devices('.project-desc-wrap p'), TrimTitle_Devices('.project-item h2');
        }

    });

</script>
