﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Configuration;
using System.Web;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public static class Logger
    {
        static string ErrorLogFile, TraceLogFile;

        static Logger()
        {
            string fpath = HttpContext.Current.Server.MapPath("\\SGFuture Revamp\\TraceLog\\");

            ErrorLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_ErrorLogFile.txt";
            TraceLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_TraceLogFile.txt";

        }

        private static string GetExceptionMessage(Exception ex)
        {
            string retValue = string.Format("Message: {0}\r\nStackTrace: {1}", ex.Message, ex.StackTrace);
            if (ex.InnerException != null)
            {
                retValue = retValue + string.Format("\r\n\r\nInner Exception: {0}", GetExceptionMessage(ex.InnerException));
            }
            return retValue;
        }

        public static void WriteException(Exception ex, string AdditionalInfo)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n\r\nAdditional Info: {2}\r\n{3}\r\n\r\n", DateTime.Now, GetExceptionMessage(ex), AdditionalInfo, new string('-', 60));

            File.AppendAllText(ErrorLogFile, finalMessage);
        }

        public static void WriteLine(string Message)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n", DateTime.Now, Message);

            File.AppendAllText(TraceLogFile, finalMessage);
        }
    }
}