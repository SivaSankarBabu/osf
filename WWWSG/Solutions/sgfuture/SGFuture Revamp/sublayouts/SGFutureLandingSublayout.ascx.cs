﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using Sitecore.Data.Fields;
using Sitecore.Resources.Media;
using sgfuture.SGFuture_Revamp.sublayouts;

namespace sgfuture.SGFuture_Revamp.sublayouts
{
    public partial class SGFutureLandingSublayout : System.Web.UI.UserControl
    {
        private static Database web = Database.GetDatabase("web");
        Item itemContext = Sitecore.Context.Item;
        SGFutureClass objUtilities = new SGFutureClass();
        private void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Logger.WriteLine("1.SGFuture Landing form Process Started");

                GetQuotations("{A132358F-9F12-41E5-B960-918BC3FBB905}");
                ShowHideMaintenanceMessage(Sitecore.Context.Item);
                hdnVideo.Value = Convert.ToString(itemContext.Fields["Youtube URL"]);
                hdnoverlayImage.Value = GetMediaImagePath(itemContext, "Banner Overlay Image");
                hdnPastEngagement.Value = GetMediaImagePath(itemContext, "Past Engagement Background Image");
                hdnOverlayImageformobile.Value = GetMediaImagePath(itemContext, "Banner Overlay Image for Mobile");
                hdnPastEngagementforMobile.Value = GetMediaImagePath(itemContext, "Past Engagement Background Image for Mobile");
                lnkUrl.NavigateUrl = objUtilities.GetItemLink(itemContext, "Get started Button Link");
                lnkUrl1.NavigateUrl = objUtilities.GetItemLink(itemContext, "Be a Volunteer Link");

                BindFeaturedNews();
            }
        }

        public void GetQuotations(string itemId)
        {
            try
            {
                DataTable dtQuotes = new DataTable();
                dtQuotes.Columns.AddRange(new DataColumn[5] { new DataColumn("QuoteImage", typeof(string)), new DataColumn("QuoteName", typeof(string)), new DataColumn("QuoteDesignation", typeof(string)), new DataColumn("Quotation", typeof(string)), new DataColumn("Id", typeof(string)) });
                DataRow drQuotes;
                List<Item> quoteItems = web.GetItem(itemId).GetChildren().OrderByDescending(a => a.Statistics.Created).Take(2).ToList();
                if (quoteItems != null && quoteItems.Count > 0)
                {
                    foreach (Item item in quoteItems)
                    {
                        drQuotes = dtQuotes.NewRow();
                        if (!string.IsNullOrEmpty(GetMediaImagePath(item, "Quote Image")))
                            drQuotes["QuoteImage"] = GetMediaImagePath(item, "Quote Image");
                        else
                            drQuotes["QuoteImage"] = "/SGFuture Revamp/assets/images/dummy/avatar-default.jpg";

                        drQuotes["QuoteName"] = Convert.ToString(item.Fields["Name"]);
                        drQuotes["QuoteDesignation"] = Convert.ToString(item.Fields["Designation"]);
                        drQuotes["Quotation"] = Convert.ToString(item.Fields["Quote"]);
                        dtQuotes.Rows.Add(drQuotes);
                    }

                    rptQuotes.DataSource = dtQuotes;
                    rptQuotes.DataBind();
                }
                else
                {
                    divQuote.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //Logger.WriteException(ex, "Exception Occurred " + ex.InnerException);

            }
        }

        string GetMediaImagePath(Item item, string colName)
        {
            ImageField imgBckgrnd = (ImageField)item.Fields[colName];
            if (imgBckgrnd != null)
            {
                MediaItem mItem = imgBckgrnd.MediaItem;
                if (mItem != null)
                {
                    return Sitecore.Resources.Media.MediaManager.GetMediaUrl(mItem);
                }
                else { return string.Empty; }
            }
            else { return string.Empty; }
        }

        /* Show or Hide Maintenance Message based on Status */
        public void ShowHideMaintenanceMessage(Item item)
        {
            divMaintenance.Visible = !string.IsNullOrEmpty(item.Fields["Maintenance Text"].Value) ? true : false;
        }

        void BindFeaturedNews()
        {
            repFeaturedNews.DataSource = objUtilities.GetLandingTiles();
            repFeaturedNews.DataBind();
        }
    }
}