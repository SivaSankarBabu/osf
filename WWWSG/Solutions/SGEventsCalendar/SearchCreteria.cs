﻿using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGEventsCalendar
{
    public class SearchCreteria
    {
        private static readonly Database masterDB;

        static SearchCreteria()
        {
            masterDB = Factory.GetDatabase("master");
        }

        public static string get(string name)
        {
            Item commonText = masterDB.GetItem("/sitecore/content/Lucene Search Settings/common text/" + name);
            return commonText == null ? null : commonText["text"];
        }
    }
}