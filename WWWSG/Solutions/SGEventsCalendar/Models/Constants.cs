﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGEventsCalendar.Models
{
    public static class Constants
    {
        public static string rootItemId = "{BAEBC2BC-71C9-4AA9-AA95-C58A8B0DEBF9}",
           bannersItemId = "{ADFFB130-6A77-4C94-9297-6E460C194248}", eventsItemId = "{0CB3EA7B-3E71-4264-A974-4301F03ED693}",
           categoryTypes = "{6C118459-8634-454D-8D1B-9AED71468521}", eventTypes = "{20D739A9-7C68-472C-85A7-3EC52ECD6165}", audiTypes = "{706644A6-2D5D-4A0D-B050-F4EC01E875CC}",
           brandAssetsItemId = "{6A8E67C2-D023-4DAF-89DB-C04AAEBA296B}", storiesItemId = "{BE043EA9-BB60-49B4-95AF-9E1A3E5CF21A}", aboutsUsItemId = "{BD4642D2-EDCB-4B73-B1C6-D795D856718E}",
           AllCats = "all categories", encode = "EN", decode = "De",
          jsVoid = "javascript:void(0)";


        public static int PageSize = 6, StoriesPageSize = 4;
        public static string[] EventsTiles = new string[] { "Event1", "Event2", "Event3", "Event4", "Event5", "Event6", "Event7" }, StoriesTiles = new string[] { "Main Featured story", "2nd featured story", "3rd featured story", "4th featured story", "5th featured story", "6th featured story", "7th featured story", /* "8th featured story", "9th featured story" */ };
    }

}