﻿using System;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Events;
using Sitecore.SecurityModel;
using System.Collections;
using System.Collections.Generic;
using Sitecore.Data.Fields;
using System.Configuration;
using System.IO;

namespace SGEventsCalendar.Models
{
    public class ItemSavedHandler
    {

        public void OnItemSaved(object sender, EventArgs args)
        {
            DateTime startDate = new DateTime();

            Logger.WriteLine("1");
            Item savedItem = Event.ExtractParameter(args, 0) as Item;

            List<string> templateIds = new List<string>() { "{10ADBEF4-75F0-4E8E-9187-3DB1F2D1B03A}" };

            if (savedItem != null && savedItem.Database.Name.ToLower() == "master" && savedItem.TemplateID == ID.Parse(templateIds[0]))
            {
                Logger.WriteLine("2");
                Item parentItem = savedItem.Parent;

                startDate = ((DateField)savedItem.Fields["Event Start Date"]).DateTime;

                Logger.WriteLine(startDate.ToString());

                string monthYear = string.Empty;

                Logger.WriteLine("M and y " + monthYear);
                Logger.WriteLine("Parnet item name " + parentItem.Name.Trim());

                if (startDate != null && parentItem != null)
                {
                    monthYear = startDate.ToString("yyyy-MM");
                    string man = startDate.Year + "-" + startDate.Month;

                    Logger.WriteLine("monthYear " + monthYear + " man " + man);

                    if (parentItem.Name.Trim() == monthYear)
                    {
                    }
                    else
                    {
                        Message("Event Start Date Should in the year - " + parentItem.Name.Trim().Split('-')[0] + " month should be - " + parentItem.Name.Trim().Split('-')[1], savedItem, startDate);
                        Logger.WriteLine("Parent item anme and start date month year not name");

                    }
                }
                else
                {
                    Message("Event Start  Date Should in the year - " + parentItem.Name.Trim().Split('-')[0] + " month should be - " + parentItem.Name.Trim().Split('-')[1], savedItem, startDate);
                    Logger.WriteLine("Start date or parent item is null");

                }
            }

        }

        void Message(string msg, Item item, DateTime date)
        {
            Sitecore.Context.ClientPage.ClientResponse.Alert(string.Format(msg));
            if (msg != "Success")
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    item.Editing.BeginEdit();
                    item.Fields["Event Start Date"].Value = Sitecore.DateUtil.ToIsoDate(date);
                    item.Editing.EndEdit();
                }
            }
        }
    }

    public static class Logger
    {
        static string ErrorLogFile, TraceLogFile;

        static Logger()
        {
            string fpath = ConfigurationManager.AppSettings["telogpath"].ToString();

            ErrorLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_ErrorLogFile.txt";
            TraceLogFile = fpath + string.Format("{0:dd-MMM-yyyy}", System.DateTime.Now) + "_TraceLogFile.txt";

        }

        private static string GetExceptionMessage(Exception ex)
        {
            string retValue = string.Format("Message: {0}\r\nStackTrace: {1}", ex.Message, ex.StackTrace);
            if (ex.InnerException != null)
            {
                retValue = retValue + string.Format("\r\n\r\nInner Exception: {0}", GetExceptionMessage(ex.InnerException));
            }
            return retValue;
        }

        public static void WriteException(Exception ex, string AdditionalInfo)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n\r\nAdditional Info: {2}\r\n{3}\r\n\r\n", DateTime.Now, GetExceptionMessage(ex), AdditionalInfo, new string('-', 60));

            File.AppendAllText(ErrorLogFile, finalMessage);
        }

        public static void WriteLine(string Message)
        {
            string finalMessage = string.Format("{0:dd-MMM-yyyy hh:mm:ss.fff}: {1}\r\n", DateTime.Now, Message);

            File.AppendAllText(TraceLogFile, finalMessage);
        }
    }

}