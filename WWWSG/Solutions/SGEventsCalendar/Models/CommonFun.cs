﻿using System;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using System.Text;
using System.Text.RegularExpressions;

namespace SGEventsCalendar.Models
{
    public class CommonFun
    {
        /* Get url based on type of link from sitecore admin */
        public static String LinkUrl(Item item, string fldName)
        {
            if (item != null && !string.IsNullOrEmpty(fldName))
            {
                LinkField _objLF = (LinkField)item.Fields[fldName];

                switch (_objLF.LinkType.ToLower())
                {
                    case "internal":
                        return _objLF.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(_objLF.TargetItem) : "javascript:void(0)";
                    case "media":
                        return _objLF.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(_objLF.TargetItem) : "javascript:void(0)";
                    case "external":
                    case "mailto":
                    case "javascript":
                        return _objLF.Url;
                    case "anchor":
                        return !string.IsNullOrEmpty(_objLF.Anchor) ? "#" + _objLF.Anchor : "javascript:void(0)";
                    default:
                        return _objLF.Url;
                }
            }
            return string.Empty;
        }

        /* Get Image Path from sitecore */
        public static string[] GetImage(Item item, string fldName)
        {
            if (item != null && !string.IsNullOrEmpty(fldName))
            {
                ImageField imgFld = ((ImageField)item.Fields[fldName]);

                if (imgFld != null)
                {
                    MediaItem mediaItem = imgFld.MediaItem;
                    return new string[2] { mediaItem != null ? MediaManager.GetMediaUrl(mediaItem) : string.Empty, 
                        imgFld.Alt.Trim() == string.Empty ? (mediaItem != null ? mediaItem.Name : string.Empty) : imgFld.Alt.Trim() };
                }
            }
            return new string[2];
        }

        /* Get Item Path */
        public static string GetItemPath(Item item, string fieldName)
        {
            if (item != null)
            {
                InternalLinkField link = ((InternalLinkField)item.Fields[fieldName]);

                if (link != null)
                {
                    Item tarItem = link.TargetItem;

                    if (tarItem != null)
                    {
                        return Convert.ToString(LinkManager.GetItemUrl(tarItem));
                    }
                }
            }
            return string.Empty;
        }

        /* Encode and Decode String*/
        public static string Encode_Decode(string _text, string type)
        {
            if (!string.IsNullOrEmpty(_text))
            {
                if (type == Constants.encode)
                {
                    var _value = Encoding.UTF8.GetBytes(_text);
                    return Convert.ToBase64String(_value);
                }
                else if (type == Constants.decode)
                {

                    if ((_text.Length % 4 == 0) && Regex.IsMatch(_text, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None))
                    {
                        var _value = Convert.FromBase64String(_text);
                        return Encoding.UTF8.GetString(_value);
                    }
                    return Constants.AllCats;
                }
                else { return string.Empty; }
            }
            else { return string.Empty; }
        }

        /* Truncate word */
        public static string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length <= length)
                return input;
            int iNextSpace = input.LastIndexOf(" ", length);
            return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        /* Get File Path from File field */
        public static string GetFilePath(Item item, string fldName)
        {
            if (item != null && !string.IsNullOrEmpty(fldName))
            {
                FileField file = ((FileField)item.Fields[fldName]);
                if (file != null)
                {
                    MediaItem media = file.MediaItem;

                    if (media != null)
                    {
                        return MediaManager.GetMediaUrl(media);
                    }
                }
                return Constants.jsVoid;
            }
            else { return Constants.jsVoid; }
        }
    }
}