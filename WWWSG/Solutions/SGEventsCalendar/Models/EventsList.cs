﻿using SGEventsCalendar.Controllers;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Pipelines.GetLookupSourceItems;
using System;
using System.Linq;

namespace SGEventsCalendar.Models
{
    public class EventsList
    {
        public void process(GetLookupSourceItemsArgs args)
        {
            Database _web = Database.GetDatabase("web");
            try
            {
                SGEventslogger.WriteLine("zEvents List Pipe line started.");

                if (!args.Source.Equals("/sitecore/content/SG/events-calendar")) { return; }

                var index = ContentSearchManager.GetIndex("sitecore_master_index");

                using (var context = index.CreateSearchContext())
                {
                    var result = _web.GetItem("{0CB3EA7B-3E71-4264-A974-4301F03ED693}").GetChildren().
                      Where(x => (((DateField)x.Fields["Event Start Date"]).DateTime.Date != new DateTime(0001, 01, 01)
                          && ((DateField)x.Fields["Event Start Date"]).DateTime.Date <= DateTime.Now.Date
                          && ((DateField)x.Fields["Event End Date"]).DateTime.Date >= DateTime.Now.Date
                          && ((DateField)x.Fields["Event End Date"]).DateTime.Date != new DateTime(0001, 01, 01)) ||
                          ((DateField)x.Fields["Event Start Date"]).DateTime >= DateTime.Now.Date && ((DateField)x.Fields["Event End Date"]).DateTime >= DateTime.Now.Date
                          && ((DateField)x.Fields["Event Start Date"]).DateTime.Month == DateTime.Now.Month && ((DateField)x.Fields["Event Start Date"]).DateTime.Year == DateTime.Now.Year
                          ).Select(x => x).ToArray();

                    args.Result.Clear();
                    args.Result.AddRange(result);

                    SGEventslogger.WriteLine("result is:" + result.Length);
                    SGEventslogger.WriteLine("Events List Pipe line end.");
                }
                args.AbortPipeline();
            }
            catch (Exception ex)
            {
                SGEventslogger.WriteException(ex, "Events List");
            }
        }
    }
}