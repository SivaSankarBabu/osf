﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace SGEventsCalendar.Models
{
    public class Header
    {
        public string Logo { get; set; }
        public string LogoAlt { get; set; }
        public string GuidelinesLogo { get; set; }
        public string GuidelinesLogoAlt { get; set; }

        public string GuidelinesCTA { get; set; }
        public List<string> Menu { get; set; }
    }

    public class Home
    {
        public string PageTitle { get; set; }
        public string PageShortDesc { get; set; }
        public string PageBackImage { get; set; }
        public string PageBackImageAlt { get; set; }
        public string EventsTitle { get; set; }
        public string EventsDesc { get; set; }
        public string StoriesTitle { get; set; }
        public string RevisitTitle { get; set; }
        public string RevisitDesc { get; set; }
        public string RevisitDeskImg { get; set; }
        public string RevisitDeskImgAlt { get; set; }
        public string RevisitMobImg { get; set; }
        public string RevisitMobImgAlt { get; set; }
        public string RevisitUrl { get; set; }
        public bool IsVisibleSG50 { get; set; }
        public List<Banner> Banners { get; set; }
        public List<SGEvent> Events { get; set; }
        public List<Story> Stories { get; set; }
    }

    public class Banner
    {
        public string DeskImage { get; set; }
        public string DeskImageAlt { get; set; }

        public string MobImage { get; set; }
        public string MobImageAlt { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }

        public Item item { get; set; }
    }

    public class SGEvent
    {
        public OveryLayContent OveryLayContent { get; set; }
        public string IntroTitle { get; set; }
        public string IntroContent { get; set; }
        public string TandCContent { get; set; }
        public string TandCLink { get; set; }

        public string ThumbImg { get; set; }
        public string ThumbImgAlt { get; set; }
        public string DetailLargeImg { get; set; }
        public string DetailLargeImgAlt { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string Category { get; set; }
        public string Type { get; set; }
        public string AudienceType { get; set; }
        public string EntryType { get; set; }

        public string Location { get; set; }
        public string Url { get; set; }
        public string PurchaseTicketURL { get; set; }
        public string GoogleMapLink { get; set; }

        public string NoteTitle { get; set; }
        public string NoteContent { get; set; }
        public string TermsTitle { get; set; }
        public string EventDetailsMail { get; set; }
        public string TermsCondExternalLink { get; set; }
        public NewEvent NewEvent { get; set; }
        public string GetPost { get; set; }

        public int PeriodStartMonth { get; set; }
        public int PostStartMonth { get; set; }
        public int PeriodStartYear { get; set; }

        public List<Category> Categories { get; set; }
        public List<Category> EventTypes { get; set; }
        public List<Category> AudienceTypes { get; set; }
        public List<SGEvent> ListEvents { get; set; }
        public List<SGEvent> ListItems { get; set; }

        public string SocShareTitle { get; set; }
        public string SocShareDesc { get; set; }
        public string SocShareImg { get; set; }
        public string ConItemTempId { get; set; }
        public string ConItemId { get; set; }

        public Item item { get; set; }

        public bool AllDay { get; set; }
        public string typeEvent { get; set; }
    }

    public class Story
    {
        public string ThumbImg { get; set; }
        public string ThumbImgAlt { get; set; }

        public string WideImage { get; set; }
        public string WideImageAlt { get; set; }

        public string DeskBanner { get; set; }
        public string DeskBannerAlt { get; set; }

        public string MobBanner { get; set; }
        public string MobBannerAlt { get; set; }

        public string Title { get; set; }
        public bool IsEventDateHide { get; set; }
        public DateTime EventDate { get; set; }
        public string ReadMoreCTA { get; set; }
        public string YoutubeURL { get; set; }
        public string ExternalLink { get; set; }
        public List<string> Gallery { get; set; }
        public List<string> Categories { get; set; }
        public string ShortDesc { get; set; }
        public string TopDesc { get; set; }
        public string BottomDesc { get; set; }
        public string QuoteText { get; set; }
        public string QuoteAuthor { get; set; }
        public string SocTitle { get; set; }
        public string SocShareDesc { get; set; }
        public string SocShareImg { get; set; }
        public List<Carousel> CarouselItems { get; set; }
        public string HostName { get; set; }
        public string FBAppId { get; set; }
        public Item item { get; set; }
    }

    public class Footer
    {
        public string FooterContent { get; set; }
        public DownloadLogoGuideLines GuideLines { get; set; }
        public string FBAppId { get; set; }
        public string HostName { get; set; }
        public string FilePath { get; set; }
    }

    public class AboutUs
    {
        public DownloadLogoGuideLines GuideLines { get; set; }
        public string ContactInfo { get; set; }
        public string Feedback { get; set; }
    }

    public class DownloadLogoGuideLines
    {
        public string Logo { get; set; }
        public string LogoAlt { get; set; }
        public string Title { get; set; }
        public string ShortDesc { get; set; }
        public string FullDesc { get; set; }
        public string CTAText { get; set; }
        public string filePath { get; set; }
    }

    public class Carousel
    {
        public string ItemImage { get; set; }
        public string ImgAltText { get; set; }
    }

    public class Category
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string AllOption { get; set; }
        public Item Item { get; set; }
    }

    public class NewEvent
    {
        public string TermsUrl { get; set; }
        public string TermsConditionsCTA { get; set; }
        public string NewBlockTitle { get; set; }
    }

    public class MetaContent
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string FBAppId { get; set; }
        public string HostName { get; set; }
        public string FBID { get; set; }
        public string OGImage { get; set; }
        public string PageTitle { get; set; }
    }

    public class OveryLayContent
    {
        public string OverlayTitle { get; set; }
        public string OverlayContent { get; set; }
        public string OverlayDescription { get; set; }
        public string Email { get; set; }
        public string CTAText { get; set; }
    }

    public class SearchResults
    {
        public string SearchText { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Item Item { get; set; }
        public int ItemCount { get; set; }

    }


}
